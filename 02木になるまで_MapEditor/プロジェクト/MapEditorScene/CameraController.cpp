#include "CameraController.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "../Engine/Model.h"

//コンストラクタ
CameraController::CameraController(GameObject * parent)
	: GameObject(parent, "CameraController")
{
	//回転度数
	SPEED_ = 15.0f;
	standardPixX_ = 100.0f;
	standardPixY_ = 100.0f;	//１００ピクセルで　0.01回転する
				//前回と今回とで、割合を取り、
				//割合＊SPEEDにて、　回転量とする

}

//初期化
void CameraController::Initialize()
{


}

//更新
void CameraController::Update()
{


	//カメラの移動
	MoveCamera();


}

void CameraController::MoveCamera()
{
	XMVECTOR move = XMVectorSet(
		Input::IsKey(DIK_D) - Input::IsKey(DIK_A),
		Input::IsKey(DIK_UP) - Input::IsKey(DIK_DOWN),
		Input::IsKey(DIK_W) - Input::IsKey(DIK_S), 0.f);
	const float SPEED = 0.2f;

	XMVECTOR moveRotate = XMVectorSet(Input::IsKey(DIK_RETURN) - Input::IsKey(DIK_LSHIFT), Input::IsKey(DIK_RIGHT) - Input::IsKey(DIK_LEFT), 0.f, 0.f);

	move *= SPEED;
	moveRotate *= SPEED;

	/*
	if (Input::IsKey(DIK_W))
	{
	move.vecZ += SPEED;
	}
	if (Input::IsKey(DIK_S))
	{
	move.vecZ -= SPEED;
	}
	if (Input::IsKey(DIK_A))
	{
	move.vecX -= SPEED;
	}
	if (Input::IsKey(DIK_D))
	{
	move.vecX += SPEED;
	}
	*/


	////回転
	//transform_.rotate_ += moveRotate;
	////範囲を超えたときに切り詰める
	//	//カメラのX回転を90度以上にしてしまうと、
	//	//カメラの方向が逆になってしまうので、
	//	//超えてしまったときに、範囲内に切り詰める
	//TruncateRotate();

	//transform_.position_ = transform_.position_ + move;
	////移動範囲を切り詰める
	//TruncatePosition();



	////回転込み
	////Y軸回転と、X軸回転
	//{
	//	//２つの回転ベクトルを掛けて
	//	//Y軸回転とX軸回転がされた行列が完成する
	//	//ただし、回転の行列はTransform.cppを見るとわかるが、掛ける順番が決まっており、順番通りに掛けないと思い通りの回転の仕方をしない。結果を出せない
	//	//matZ * matX * matY
	//	XMMATRIX mat =
	//		XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX))
	//		* XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));


	//	//カメラ位置を確定
	//	//XMVECTOR moveVec = XMVectorSet(0.0f, 10.0f, -10.0f, 0.0f);
	//	//カメラの上下を変えるならば、→角度をそろえるために高さは０にしておく
	//	XMVECTOR moveVec = XMVectorSet(0.0f, 0.0f, -5.0f, 0.0f);
	//	//カメラの位置（ベクトル）も、コントローラーの回転具合分回転させる
	//	moveVec = XMVector3TransformCoord(moveVec, mat);


	//	//transform_.position_ += moveVec;

	//	//移動範囲を切り詰める
	//	TruncatePosition();


	//	//コントローラー位置と回転行列を掛けたカメラ位置ベクトルを足す
	//	Camera::SetPosition(
	//		transform_.position_ + moveVec);

	//	//カメラの焦点
	//	Camera::SetTarget(transform_.position_);

	//}


	{
		//移動は、
		//親ごと、移動させる
			//今回は、
			//SkyBoxという背景のクラスがあり、
			//その背景のクラスの親を、自身の親になっている。
			//なので、背景は、親が移動時に、自身も移動するようにしたいので、
			//親も同時に移動させる



		pParent_->transform_.position_ = pParent_->transform_.position_ + move;

		//移動範囲を切り詰める
		TruncatePosition();

		//		XMVECTOR camVec = Camera::GetPosition();
		Camera::SetPosition(pParent_->transform_.position_ - XMVectorSet(0, 0, -2, 0));

		//Camera::SetTarget(transform_.position_ + XMVectorSet(0, 0, 2, 0));
	}
	{
		//回転
		//回転は、子供クラスである、自身だけ
			//SkyBoxも回転させたくない
		transform_.rotate_ += moveRotate;
		//範囲を超えたときに切り詰める
			//カメラのX回転を90度以上にしてしまうと、
			//カメラの方向が逆になってしまうので、
			//超えてしまったときに、範囲内に切り詰める
		TruncateRotate();



		//回転行列
		XMMATRIX mat = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX))
			* XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));

		XMVECTOR targetDis = XMVectorSet(0.f, 0.f, 5.f, 0.f);






		//回転後の位置
		//現在の回転量を求めて、
		//回転したときのベクトル位置を求める
		targetDis = XMVector3TransformCoord(targetDis, mat);

		Camera::SetTarget(pParent_->transform_.position_ + targetDis);
	}

	/*Inputクラスからマウスの移動量を取得*/
	//移動量に伴って、回転

	//{
	//	//マウスの移動量を取得
	//	XMVECTOR moveMouseCursor = Input::GetMouseMove();



	//	//if (moveMouseCursor.vecX != 0 && moveMouseCursor.vecY != 0)
	//	{
	//		//カメラの回転
	//		//マウスの移動によって、回転
	//		//XMVECTOR sub = mouseCursor - beforePixPos_;
	//		//基準の移動ピクセルをもとに、
	//			//割合を求める
	//			//基準ピクセルをもとに、どの程度進むのか


	//		float perX;
	//		if (moveMouseCursor.vecX != 0)
	//		{
	//			//基準のピクセル値から
	//				//移動量のピクセルの割合を求める
	//				//どれだけ移動したのか
	//			perX = moveMouseCursor.vecX / standardPixX_;
	//			//０との除算をすると、
	//			//答えにinfという値が入ってしまいエラーの原因になるので、0の場合を除く計算とする
	//		}
	//		else
	//		{
	//			perX = 0;
	//		}
	//		float perY;
	//		if (moveMouseCursor.vecY != 0)
	//		{
	//			perY = moveMouseCursor.vecY / standardPixY_;
	//		}
	//		else
	//		{
	//			perY = 0;
	//		}


	//		transform_.rotate_.vecX += SPEED_ * (perY);
	//		//今回は移動量をもらうので、前回より下に言っていたらー　となるので、
	//		//ーを回転方向と合わせるために、変える必要がない


	//		transform_.rotate_.vecY += SPEED_ * (perX);


	//		//回転
	//		XMMATRIX matX = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX));
	//		XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));





	//		XMVECTOR targetDis = XMVectorSet(0.f, -1.f, 10.f, 0.f);

	//		//回転を掛ける
	//		targetDis = XMVector3TransformCoord(targetDis, matX * matY);

	//		Camera::SetTarget(transform_.position_ + targetDis);


	//	}
	//}
}

void CameraController::TruncatePosition()
{
	const float MAX_POS_X = 100.f;
	const float MIN_POS_X = 0.f;
	const float MAX_POS_Y = 20.f;
	const float MIN_POS_Y = 0.0f;
	const float MAX_POS_Z = 100.f;
	const float MIN_POS_Z = -10.f;

	//X
	if (pParent_->transform_.position_.vecX < MIN_POS_X)
	{
		pParent_->transform_.position_.vecX = MIN_POS_X;
	}
	else if (pParent_->transform_.position_.vecX > MAX_POS_X)
	{
		pParent_->transform_.position_.vecX = MAX_POS_X;
	}

	//Y
	if (pParent_->transform_.position_.vecY < MIN_POS_Y)
	{
		pParent_->transform_.position_.vecY = MIN_POS_Y;
	}
	else if (pParent_->transform_.position_.vecY > MAX_POS_Y)
	{
		pParent_->transform_.position_.vecY = MAX_POS_Y;
	}

	//Z
	if (pParent_->transform_.position_.vecZ < MIN_POS_Z)
	{
		pParent_->transform_.position_.vecZ = MIN_POS_Z;
	}
	else if (pParent_->transform_.position_.vecZ > MAX_POS_Z)
	{
		pParent_->transform_.position_.vecZ = MAX_POS_Z;
	}
}

void CameraController::TruncateRotate()
{
	const float MAX_ROT_X = 15.5f;
	const float MIN_ROT_X = -10.5f;

	//X
	if (transform_.rotate_.vecX < MIN_ROT_X)
	{
		transform_.rotate_.vecX = MIN_ROT_X;
	}
	else if (transform_.rotate_.vecX > MAX_ROT_X)
	{
		transform_.rotate_.vecX = MAX_ROT_X;
	}
}

//描画
void CameraController::Draw()
{
}

//開放
void CameraController::Release()
{
}