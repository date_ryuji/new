#include "WindowProcedure.h"
#include "../Engine/Input.h"

//メニューの操作によって、恩恵を受けたいクラス
#include "../MapEditorScene/MapEditManager.h"
#include "../Engine/SceneManager.h"



namespace WindowProcedure
{
	//全てのゲームオブジェクトの親
	RootJob* pRootJob_ = nullptr;
}




void WindowProcedure::Initialize()
{
}

void WindowProcedure::Release()
{
}

void WindowProcedure::SetRootJob(RootJob * pRootJob)
{
	pRootJob_ = pRootJob;
}


//★ウィンドウプロシージャ（何かあった時によばれる関数）
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
//誰の			何が起きたか
//リザルトを返す	CALLBACK関数=何かが起こったら、条件で勝手に呼ばれる
					//普通なら、呼ぶ先が、条件によって、勝手に呼んでくれる
//HWND=WindowHandle	（これらの関数は、Windoｗ側の関数）
	//ハンドル、自分がどっちに動きたいか、
		//Windowの識別番号だと思って、→ウィンドウがたくさん出る中で、それを識別番号（WindowHandle→ウィンドウの番号とHandleが付いたら、番号と）
	//何かあったときに呼ばれるが、今回のhWndには、何かあったウィンドウが入る（ゲームだとウィンドウは１つしかないので、そのウィンドウ）
//UINT=unsigned int(符号なしの整数)	の略
	//→引数には、何が起きたかが入る（番号で、）
		//→キーが押された、６００など決められた番号が入る
		//
//W,LParamは、そのメッセージごとの起こった処理が入る（マウスが動いたならマウスがどのように動いたかなど）
//その時によって、使われ方が異なる
	//なんのキーが押されたかなど


{

	switch (msg)

	{
	//何かが起こった
	case WM_COMMAND : 
		//LOWORD(wParam)にクリックしたメニューのコントロールが入ってkる(ダイアログ時に、第3引数から、LOWOR(第３引数)をして、どのコントロールなのか取得した時と同じ要領で)
		//つまり誰だか、わかるということ。
		switch (LOWORD(wParam))
		{
		//「保存」メニューが押されたとき
		case MENU_SAVE:
		{
			//case文の中で変数を作ろうとすると怒られる
			//その時は、caseを中かっこ｛｝で囲むと、使う範囲が決められるので怒られない。

			//ゲームオブジェクトから探すとして、
			//RootJobを呼ばないといけない。
			if (WindowProcedure::pRootJob_)//ルートジョブが確保されていれば、（nullptrでなければ）
			{
				if (WindowProcedure::pRootJob_)//ルートジョブが確保されていれば、（nullptrでなければ）
				{

					//メッセージの受取先のクラスを確保して
					MapEditManager* pMapEditmanager = (MapEditManager*)WindowProcedure::pRootJob_->FindObject("MapEditManager");
					if (pMapEditmanager)
					{
						//保存関数を呼び出す
						pMapEditmanager->CreateHeightMapImage();
					}

				}


			}

		return 0;	//WM_COMMAND

		}
		//新規作成
		case MENU_NEW:
		{
			//マップ作成シーンへ飛ぶ

			//Rootから、シーンマネージャーを探して、
				//シーンを切り替えてもらう
				//同じシーンから、
				//シーンマネージャーでチェンジシーンとしても、切り替え先が同じなので、新たなシーンになることはない。
			if (WindowProcedure::pRootJob_)//ルートジョブが確保されていれば、（nullptrでなければ）
			{
				if (WindowProcedure::pRootJob_)//ルートジョブが確保されていれば、（nullptrでなければ）
				{
					//メッセージの受取先のクラスを確保して
					SceneManager* pSceneManager = (SceneManager*)WindowProcedure::pRootJob_->FindObject("SceneManager");
					if (pSceneManager)
					{
						//シーン切り替え
						pSceneManager->ChangeScene(SCENE_ID_CREATE_MAP);
					}
				}


			}

		
		}

		}

		


		//（ゲームの）ウィンドウ側の何かが起こったとき
		//ウィンドウ側のマウスが動いたとき
	case WM_MOUSEMOVE:
		/*
			マウスのポジションはまず、
			マウスのデバイスから取得できないというのを前提として
			Window側から、マウスのポジションを取得しないといけない

			ゲームウィンドウのマウスが動いたとき
		*/
		//Inputの名前空間の関数へゲームのウィンドウのマウスの位置を送る
		Input::SetMousePosition(LOWORD(lParam), HIWORD(lParam));
		//WORD型から、LOWORD（上位バイトのみ抽出）、HIWORD（下位バイトのみ抽出）
		//IParamには、その時、起こったときの変化について書かれている
		//そのIParamの前半に、ｘ座標、後半にｙ座標をが保存されている
			//→そのIPramから前半を取得をLOWORDの関数で、後半の取得をHIWORDで取得するようにした
		return 0;

	case WM_DESTROY:
		//名前で、番号を取得
			//いちいち番号で覚えていられないので、名前で指定
		//定義に移動で、

		//様々な名前がある中で、
			//→ｍｓｇで、何かがされたかが、入っているので、その時の処理を描く

		PostQuitMessage(0);  //プログラム終了
							//ウィンドウを閉じる＝プログラム終了するとはならないので、
							//ウィンドウズを閉じた→プログラムを終了にしないといけない

		//上記をコメントアウトすると→ウィンドウは閉じられても、→プログラムは終わらない（こっそり裏で動く）

		return 0;

		/*
	case WM_CLOSE:
		return 0;
			//これで、WM_CLOSEをついかしてみて、呼ばれたら、return0を返す
			//WM_CLOSEは、×ボタンが押されたら　呼ばれるもの
			//→この時、今までは、デフォルトとして、→設定していたので、自動でデフォルトのウィンドウの消去をすることを行ってくれていた
			//なので、これが使えるのは、×ボタンが押されたとき、→本当に消しますか？という警告を出したり
			*/
	}
	//本来であれば、このスイッチに、この処理がされたときとmsgを網羅するものを書かなければいけないが、
		//→書いていられない↓

	return DefWindowProc(hWnd, msg, wParam, lParam);
	//いちいち、このmsgで呼ばれたとき→何をすると書いていられない
	//すべてのメッセージに対する処理を書いていられない、→なので、重要メッセージ以外はデフォルトの作業をしてもらう
	//→そんな時、デフォルトの作業
	//→例えば、罰が押されたら、閉じるなどデフォルトをしてほしいときは、
		//→デフォルトで、この作業をしてくださいと、してくれる

}
