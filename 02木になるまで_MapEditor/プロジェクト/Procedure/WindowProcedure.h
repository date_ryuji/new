#pragma once
#include <Windows.h>
#include <string>

//GameObjectクラスを継承したクラス
//すべてのObjectの一番上の親となるオブジェクト（オブジェクトは親をたどるとRootObjectに必ずたどり着く→たどり着かないオブジェクトは、GameObjectの家系にオブジェクトが作られていない）
#include "../Engine/RootJob.h"
//メニューID管理ヘッダ
#include "../resource.h"


//ウィンドウプロシージャ（ウィンドウに対して何かあった時に呼ばれる関数）
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);



enum MENU_TYPE
{
	//SAVE
	MENU_TYPE_SAVE = 0,
	//NEW
	MENU_TYPE_NEW ,


	MENU_MAX,

};


namespace WindowProcedure
{
	//初期化
	void Initialize();

	//解放
	void Release();

	//RootJobをセット
	void SetRootJob(RootJob* pRootJob);

}
