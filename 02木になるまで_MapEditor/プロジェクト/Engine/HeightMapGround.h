#pragma once
#include "PolygonGroup.h"



/*
・ハイトマップ画像を読み込み
・そのハイトマップ画像の　幅＊高さ　のピクセルを取得し、
・その幅＊高さ分、頂点を取得
・頂点一つ一つの高さを、　画像の1ピクセルの色情報から、高さを確定する。（（０，０）のピクセルの色が、(1,1,1)ならば、該当する頂点のY座標の高さを、あらかじめ決めて置いた最高Y座標位置の値に、
	（500，0）のピクセル色が、(0,0,0)ならば、あらかじめ決めて置いた、最低Y座標の高さにするなど、画像のピクセルの色から、頂点の高さを決める。）



*/


class HeightMapGround : public PolygonGroup
{
private : 
	//コンスタントバッファ
	//シェーダーに渡す情報（シェーダー内のコンスタントバッファに変数として持っている情報に渡す情報を、構造体として取得しておく（渡す情報をまとめておく））
	//シェーダーのコンスタントバッファと同じ順番になるようにする。
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;	//ワールド行列＊ビュー行列＊プロジェクション行列
		XMMATRIX	matNormal;	//回転のためのワールド行列（移動行列なし）
		XMMATRIX	matWorld;	//移動＊回転＊拡大　行列

		XMFLOAT4 camPos;	//カメラポジション
							//シェーダーの反射のため

		BOOL isTexture;	//テクスチャが張られているか
	};

	//頂点情報
		//現段階ではポジションだけの頂点情報（のちに法線）
	struct VERTEX
	{
		XMVECTOR position;	//位置情報（頂点の位置）
		XMVECTOR uv;		//UV情報	（頂点のテクスチャの座標）
		XMVECTOR normal;		//法線情報	（頂点の陰）

		//コンストラクタ
		VERTEX();
	};



	//各頂点ごとの頂点情報を入れる配列
	VERTEX* pVertices_;

	//インデックス情報を登録しておく
	//動的確保した配列
	int* pIndex_;


	//頂点情報を受け渡し
	ID3D11Buffer *pVertexBuffer_;
	//頂点の順番→インデックス情報を持っておく情報
	ID3D11Buffer *pIndexBuffer_;
	//コンスタントバッファ→渡すため
	ID3D11Buffer *pConstantBuffer_;
	//上記の情報があれば、→FBX,頂点情報、インデックス情報を受け取れば、
		//→Quadクラスト同じように大量に三角形を作れば表示は可能なはず

	//FBXを読み込むので、あらかじめわからないので、
	//メンバとして宣言しておく
	int vertexCount_;	//頂点数（FBXファイルより取得）
	int polygonCount_;	//ポリゴン数（FBXファイルより取得）
	int materialCount_;	//マテリアルの個数（マテリアルのLambert１、２などを入れるための数）（ノードに存在するマテリアルの個数）

	//インデックス数
	int indexCountEachMaterial_;	



protected : 
	//地面の幅
	int width_;
	//地面の奥行き（高さ）
	int depth_;

	//三角形のサイズ(横に並ぶときの間隔になる)
	const float SCALE_;

	//ローカル座標における
	//最高Y座標位置と　最低Y座標位置を決める
	//最高
	const float MAX_Y_;
	//最低
	const float MIN_Y_;



	//頂点情報（Y座標を除く）作成
	//戻値：エラーの有無
	HRESULT InitVertex();
	//頂点情報　Y座標の作成
	//引数：テクスチャの情報構造体のポインタ
	//戻値：エラーの有無
	HRESULT InitVertexY(D3D11_MAPPED_SUBRESOURCE* hMappedres);
	//頂点情報から頂点バッファーを作成
	HRESULT CreateVertexBuffer();

	//インデックス情報の作成
	HRESULT InitIndex();
	//インデックス情報からインデックスバッファーを作成
	HRESULT CreateIndexBuffer();
	//コンスタントバッファの作成
	HRESULT CreateConstantBuffer();

	//テクスチャのロード
	HRESULT LoadTexture();
	Texture* pTexture_;

public:

	//コンストラクタ
	//引数：なし
	//戻値：なし
	HeightMapGround();

	//デストラクタ
	~HeightMapGround() override;


	//ハイトマップ画像からモデルの作成を行う（引数のファイル名より）
	//引数：fileName ファイル名(ハイトマップの画像ファイル)
	//戻値：なし
	HRESULT Load(std::string fileName, SHADER_TYPE thisShader) override;

	//モデルの描画を行う
	//引数：&transform	描画時の変形情報（移動、拡大、回転）
	//戻値：なし
	HRESULT    Draw(Transform& transform) override;
	//ポインタの解放処理
	//引数：なし
	//戻値：なし
	void    Release() override;

	//レイキャスト
	//レイと、モデルとの衝突判定
	//引数：レイキャスト用の構造体ポインタ
	//戻ち：なし
	void RayCast(RayCastData* rayData) override;

	//引数ベクトルと衝突する面の法線ベクトルを帰す
		//レイキャストと同様の方法で、衝突する面を調べて、その面を作るベクトルから法線ベクトルを帰す
		//もしかすると、実際に衝突している地点から延びる法線を出さないと、正確にならない？
	//引数：面との衝突判定を行うベクトルのスタート地点
	//引数：面との衝突判定を行うベクトルの方向
	//戻値：衝突した面の法線ベクトル
	XMVECTOR NormalVectorOfCollidingFace(XMVECTOR& start, XMVECTOR& dir) override;


	//コンスタントバッファを渡す
	//オブジェクトクラス内から
	//シェーダーに渡すコンスタントバッファを管理するため
	//引数：コンスタントバッファのポインタ
	ID3D11Buffer* GetConstantBuffer() override;

};

