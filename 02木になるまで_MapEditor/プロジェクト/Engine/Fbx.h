#pragma once
#include "PolygonGroup.h"
#include <fbxsdk.h>


#pragma comment(lib, "LibFbxSDK-MT.lib")
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")


//３Dモデルの描画を行った
//→Quadのクラスとやることは変わらず
//→頂点情報を受け取って、（位置、法線）
//→頂点情報の三角形の形成（インデックス情報）をもらって
//→それを組み合わせることができれば、→FBXの形を形成することが可能である
	//→頂点数、ポリゴン数は、FBXの形でまちまちなので、→FBXを読み込んでから出ないとわからない→FBXから取得する形に



class Fbx : public PolygonGroup
{
private : 

	//コンスタントバッファ
	//シェーダーに渡す情報（シェーダー内のコンスタントバッファに変数として持っている情報に渡す情報を、構造体として取得しておく（渡す情報をまとめておく））
	//シェーダーのコンスタントバッファと同じ順番になるようにする。
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;	//ワールド行列＊ビュー行列＊プロジェクション行列
		XMMATRIX	matNormal;	//回転のためのワールド行列（移動行列なし）
		XMMATRIX	matWorld;	//移動＊回転＊拡大　行列

		XMFLOAT4 camPos;	//カメラポジション
							//シェーダーの反射のため

		XMFLOAT4 diffuseColor;	//マテリアルの色（テクスチャが使用されていない、部分に使用されているマテリアルの色を保存しておく変数）
								//MATERIALにて使用されている、FLOAT4の型を使用して、型を合わせる



		/*フォンシェーディングのためのマテリアル情報*/
		XMFLOAT4 ambientColor;	//環境光の色

		XMFLOAT4 supecularColor;	//ハイライトの色

		float shininess;			//光沢度


		BOOL isTexture;	//テクスチャが張られているか
		//フラグは、Ｃ＋＋側で普通に登録
						//ただのフラグだが、C++でのbool型のフラグの取り方と、HLSLシェーダーファイルでのbool型のフラグの取り方では違いがある
						/*
							C++は、boolの１バイト（８ビット）における、先頭１ビットにフラグの値０，１を登録
								→そして、取得するときも、bool型の８ビットから、先頭の１ビットに入っていることがわかっているので、
								→先頭１ビットからフラグ状況を受け取っている（残りの７ビットは使用しない）

							HLSLは、逆にboolの８ビットから
								→お尻の１ビットからフラグの０，１を取得する

							見るところが違うため→boolをそのまま使用することは不可能
								→ちなみにbool型ではなく、int型であれは、きちんとフラグとしてtrue,false入れたところと、HLSLで見るところは一緒
									→なので、int型でフラグをとってしまえば、その場で解決はする

							〇boolで何個も作成して、フラグ管理に用いるのはもったいない
								→フラグに使用するのが、１ビットならば、
								→１バイトの中には、１ビットが８つ→８つのフラグが管理できるということでは？
								→以下の「他の解決策として」へ
						*/

						/*
							その他の解決策として
							BYTEの型を使用する（BYTE=unsigned char）

							詳しく、参考例
							Input.cppのIsKey関数を参照


							★論理演算にて、ビットの計算を使用してフラグに使用されているビットの位置を指定して取得する
							取り出す時に、逆ならば、→先頭の１ビットからビットを取得する様に書けばいい
								→登録に使用、先頭の１ビットは、2真数にしたときの、0オリジンで、7桁目
								→10進では、128、16進では0x80（これらと、論理和にて、登録）
								→お尻の１ビットから取り出す。2進数では、1桁目。
　　							→10進では、1、16進では、〜〜〜
								それを論理積にて、フラグが立っているかの確認

						*/
						//本当の原因は
							//コンスタントバッファに渡すデータ、コンスタントバッファのポインタのデータサイズが16の倍数でないからエラーになる、データがうまく渡せない。
							//詳細は　../Shader/BaseShader.cpp CreateConstantBuffer1()にて



	};

	//頂点情報
		//現段階ではポジションだけの頂点情報（のちに法線）
	struct VERTEX
	{
		XMVECTOR position;	//位置情報（頂点の位置）
		XMVECTOR uv;		//UV情報	（頂点のテクスチャの座標）
		XMVECTOR normal;		//法線情報	（頂点の陰）

		//接線
		//FBXに接線と従法線を入れてもいいが
			//接線が求まれば、　法線と接線とで従法線を求めることもできる
		//https://www.gamedev.net/tutorials/programming/graphics/how-to-work-with-fbx-sdk-r3582/
			//UVの取り方を変えた
			//頂点情報の並び方がいろいろ種類がある。
				//MAYAの出力の仕方で、チェックのありなしで、　データの格納方法が変わってくる
				//同友情報で、同友ルールで格納されているのか、　を調べて、　その方法によって、取得方法を変える方式を上で取っている。　なので、本来であれば、そのように、様々な方法に対応したエンジンを作るが、
				//今回は出力方法を固定にしているので、　調べて、それに合った方法をとらないようにする。
					//詳しくは上記のサイトをチェック
		XMVECTOR tangent;

		//コンストラクタ
		VERTEX();
	};



	//マテリアル
		//今後、テクスチャのほかにも、マテリアルそのものの色も取得しておく
	struct MATERIAL
	{
		Texture*	pTexture;	//テクスチャのマテリアル
		/*ノーマルマップ*/
		Texture*	pNormalTexture;	//ノーマルマップ用のテクスチャ
										//RGBによって、でこぼこの表現

		XMFLOAT4	diffuseColor;	//テクスチャの貼られていないマテリアルの色


		//FBXに情報として持たせている情報を構造体メンバに取得し
		//それをコンスタントバッファに渡して、
		//ハイライトづくりなどを行い、MAYAで作成したオブジェクトに近い表現にする。
		XMFLOAT4	ambientColor;	//環境光
		XMFLOAT4	specularColor;		//speclar スペキュラー
										//シェーダーのフォンの計算における　ks（鏡面反射率） * is（ハイライトの色）
		float	shininess;	//shininess ハイライトの広がり具合


		//float		diffuse;		//LN
		//float		transparency;	//diffuseColor.w
		//float reflectivety;			//R
		//float reflectedPower;			//V




		//light
			//シェーダーにて
		//ambient
			//ambientColor //FBXより取得
		//id
			//diffuseColor //FBXより取得
		//LN
			//シェーダーにて	//light と　normal（法線）
		//diffuse
			//シェーダーにて	//LN * id
		//R
			//シェーダーにて								//ライトの反射ベクトル
		//ks
			//										//鏡面反射率、ハイライトの強さ
		//shininess
			//shininess		//Phongマテリアル使用時取得
		//is
			//										//ハイライトの色
		//specular
			//

		//コンストラクタ
		MATERIAL();

	};

	//各頂点ごとの頂点情報を入れる配列
	VERTEX* pVertices_;

	//マテリアルごとの
	//インデックス情報を入れる２次元配列
		//ポインタのポインタ　＝　ポインタのポインタに複数個のポインタ動的確保　（ppIndex = new int*[5]）
		//ポインタのポインタ[0] = ポインタのポインタを複数個に分けた、その「０」に、複数個要素動的確保  (ppIndex[0] = new int[10]) 
		//ポインタのポインタで、　ポインタを複数個確保することになるので、　その、確保した、複数個分解放してから、ポインタのポインタを解放しないといけない
	int** ppIndex_;


	//頂点情報を受け渡し
	ID3D11Buffer *pVertexBuffer_;
	//頂点の順番→インデックス情報を持っておく情報
	//ポインタを複数持たないといけないので、→ポインタのポインタ＊＊
	ID3D11Buffer **ppIndexBuffer_;
	//コンスタントバッファ→渡すため
	ID3D11Buffer *pConstantBuffer_;
	//上記の情報があれば、→FBX,頂点情報、インデックス情報を受け取れば、
		//→Quadクラスト同じように大量に三角形を作れば表示は可能なはず

	//マテリアルの構造体ポインタ
	MATERIAL* pMaterialList_;	//マテリアル数分の構造体を取得する
								//ノードに存在するマテリアルの個数分要素を動的確保



	//FBXを読み込むので、あらかじめわからないので、
	//メンバとして宣言しておく
	int vertexCount_;	//頂点数（FBXファイルより取得）
	int polygonCount_;	//ポリゴン数（FBXファイルより取得）
	int materialCount_;	//マテリアルの個数（マテリアルのLambert１、２などを入れるための数）（ノードに存在するマテリアルの個数）

	//マテリアルごとのインデックス情報の個数を保存する配列
	//マテリアル数分、要素を取得
	int *indexCountEachMaterial_;	//動的に要素を確保するためにポインタ（materialCount_という変数で要素を確保するためにポインタを使う）



protected : 
	
	//頂点バッファ作成
	//引数：FBXモデルのノードのメッシュ
	//戻値：エラーの有無
	HRESULT InitVertex(fbxsdk::FbxMesh * mesh);
	//インデックスバッファ作成
	//引数：FBXモデルのノードのメッシュ
	//戻値：エラーの有無
	HRESULT InitIndex(fbxsdk::FbxMesh * mesh);

	//コンスタントバッファ作成
	//引数：FBXモデルのノードのメッシュ
	//戻値：エラーの有無
	HRESULT IntConstantBuffer();

	//FBX使用のマテリアルの作成
	//引数：FBXモデルのノード（メッシュではない→メッシュにはマテリアルは存在しない）
	//戻値：エラーの有無
	HRESULT InitMaterial(fbxsdk::FbxNode* pNode);

	//ノーマルマップのテクスチャが存在するときに、情報を取得する
	HRESULT LoadNormalMapTexture(int i, FbxSurfaceMaterial* pMaterial);
	//通常のテクスチャの情報を取得する
	HRESULT LoadDiffuseTexture(int i, FbxSurfaceMaterial* pMaterial);


public:

	//コンストラクタ
	//引数：なし
	//戻値：なし
	Fbx();

	//デストラクタ
	~Fbx() override;


	//FBXファイルのロードを行う（引数のファイル名より）
	//引数：fileName ファイル名
	//戻値：なし
	HRESULT Load(std::string fileName, SHADER_TYPE thisShader) override;
	
	//FBXモデルの描画を行う
	//引数：&transform	描画時の変形情報（移動、拡大、回転）
	//戻値：なし
	HRESULT    Draw(Transform& transform) override;
	//ポインタの解放処理
	//引数：なし
	//戻値：なし
	void    Release() override;

	//レイキャスト
	//レイと、FBXとの衝突判定
	//引数：レイキャスト用の構造体ポインタ
	//戻ち：なし
	void RayCast(RayCastData* rayData) override;

	//引数ベクトルと衝突する面の法線ベクトルを帰す
		//レイキャストと同様の方法で、衝突する面を調べて、その面を作るベクトルから法線ベクトルを帰す
		//もしかすると、実際に衝突している地点から延びる法線を出さないと、正確にならない？
	//引数：面との衝突判定を行うベクトルのスタート地点
	//引数：面との衝突判定を行うベクトルの方向
	//戻値：衝突した面の法線ベクトル
	XMVECTOR NormalVectorOfCollidingFace(XMVECTOR& start, XMVECTOR& dir) override;


	//コンスタントバッファを渡す
	//オブジェクトクラス内から
	//シェーダーに渡すコンスタントバッファを管理するため
	//引数：コンスタントバッファのポインタ
	ID3D11Buffer* GetConstantBuffer() override;
};

