#pragma once

#include <d3d11.h>
#include <string>
#include "Transform.h"
#include "Direct3D.h"



//３Dモデルの描画を行った
//→Quadのクラスとやることは変わらず
//→頂点情報を受け取って、（位置、法線）
//→頂点情報の三角形の形成（インデックス情報）をもらって
//→それを組み合わせることができれば、→FBXの形を形成することが可能である
	//→頂点数、ポリゴン数は、FBXの形でまちまちなので、→FBXを読み込んでから出ないとわからない→FBXから取得する形に

//プロトタイプ宣言のように
//クラスの前方宣言
class Texture;

//レイキャスト用構造体
struct RayCastData
{
	XMVECTOR	start;	//レイ発射位置
	XMVECTOR	dir;	//レイの向きベクトル

	XMVECTOR	normal;	//レイが衝突した面の法線ベクトル（つまり、面に対して、垂直なベクトル）

	float		dist;	//衝突点までの距離
	BOOL        hit;	//レイが当たったか

	int			polygonNum;	//衝突したポリゴンの番号



	//コンストラクタ
	RayCastData();

};


class PolygonGroup
{
protected : 

	
	//自身のシェーダー
	SHADER_TYPE thisShader_;

public:

	//コンストラクタ
	//引数：なし
	//戻値：なし
	PolygonGroup();

	//デストラクタ
	virtual ~PolygonGroup();



	//FBXファイルのロードを行う（引数のファイル名より）
	//引数：fileName ファイル名
	//戻値：なし
	virtual HRESULT Load(std::string fileName, SHADER_TYPE thisShader) = 0;


	//FBXモデルの描画を行う
	//引数：&transform	描画時の変形情報（移動、拡大、回転）
	//戻値：なし
	virtual HRESULT    Draw(Transform& transform);
	
	
	//ポインタの解放処理
	//引数：なし
	//戻値：なし
	virtual void    Release();

	//レイキャスト
	//レイと、FBXとの衝突判定
	//引数：レイキャスト用の構造体ポインタ
	//戻ち：なし
	virtual void RayCast(RayCastData* rayData);

	//引数ベクトルと衝突する面の法線ベクトルを帰す
		//レイキャストと同様の方法で、衝突する面を調べて、その面を作るベクトルから法線ベクトルを帰す
		//もしかすると、実際に衝突している地点から延びる法線を出さないと、正確にならない？
	//引数：面との衝突判定を行うベクトルのスタート地点
	//引数：面との衝突判定を行うベクトルの方向
	//戻値：衝突した面の法線ベクトル
	virtual XMVECTOR NormalVectorOfCollidingFace(XMVECTOR& start, XMVECTOR& dir);



	//シェーダーを切り替える
	//戻値：切り替え先のシェーダータイプ
	virtual void ChangeShader(SHADER_TYPE shaderType) { thisShader_ = shaderType; };

	//現在のシェーダーを返す
	//戻値：シェーダータイプ
	virtual SHADER_TYPE GetMyShader() { return thisShader_; };
	//コンスタントバッファを渡す
	//オブジェクトクラス内から
	//シェーダーに渡すコンスタントバッファを管理するため
	//引数：コンスタントバッファのポインタ
	virtual ID3D11Buffer* GetConstantBuffer();
};