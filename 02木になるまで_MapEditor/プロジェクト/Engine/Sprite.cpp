#include "Sprite.h"
#include "Camera.h"
#include "Global.h"

//カメラも必要なし、合成行列も作らないので、
	//カメラからの見え方など、関係なし

//#include "Direct3D.h"
//cppでインクルードしたが、ヘッダでインクルードしないと
//頂点バッファの変数を作るために



Sprite::Sprite():
	thisShader_(SHADER_2D)		//デフォルトシェーダーのタイプをセット
{
	//初期化（ポインタの）
	pVertexBuffer_ = nullptr;	//頂点バッファ
	pIndexBuffer_ = nullptr;
	pTexture_ = nullptr;

	vertex_ = 6;
}

//初期化して、　表示する、
//おなじみの関数を作っておく
	//Updateは今回なし

Sprite::~Sprite()

{
	//ポインタで取得していても、newで動的確保はしていないので、　→解放もなし


}


//頂点情報の初期化（頂点バッファー作成まで）
HRESULT Sprite::InitVertex()
{
	// 頂点情報
	//構造体で、　位置、UV
	//２Dの場合、ワールドの位置で座標をとっても、
		//カメラの位置、などなどのビュー行列、プロジェクション行列変換を行っていない
		//→そのため、３Dのように、ワールドの位置と考えて、ベクトルを取得しても、それはプロジェクションにおける座標になってしまう（カメラのしすいだいを、ギュッとH２＊W２＊D１）
	//そのため、画像における位置というのは、プロジェクションの位置をそのまま、スクリーンへと変換した位置になる（プロジェクションの位置を、スクリーンサイズで表示させる）
	//
	VERTEX vertices[] =

	{


		{XMVectorSet(-1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)},	// 四角形の頂点（左上）

	{XMVectorSet(1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f)},	// 四角形の頂点（右上）

	{XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f)},	// 四角形の頂点（右下）

	{XMVectorSet(-1.0f, -1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)},	// 四角形の頂点（左下）		



	};
	//DirectXの名前空間のXMVECTORを使い、ベクター型で頂点情報を入れる、配列を取得


	//ここの、頂点１番目に書かれているものが、頂点０として扱われる
		//→これをインデックス情報に入れるときに、どの頂点でさんかっけいが作られているかを表すときの順番にナウr

	// 頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;

	bd_vertex.ByteWidth = sizeof(vertices);

	bd_vertex.Usage = D3D11_USAGE_DEFAULT;

	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	bd_vertex.CPUAccessFlags = 0;

	bd_vertex.MiscFlags = 0;

	bd_vertex.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA data_vertex;

	data_vertex.pSysMem = vertices;

	/*
	HRESULT hr;
	//HRESUTLの型に、→バッファーを作成したときにエラーを返さないか、その結果をもらう
		//HRESULTに、成功と、失敗とともに、どんな失敗をしたのか、　
	hr =
	Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);
			//頂点バッファを作ります
			*/


			//HRESULT型で、結果を受けるCreateBufferという関数などを使用したとき
			//→それにより、正しいエラー処理
			//失敗したかの判断
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		//エラーがわからないので、→エラーをWindowsの処理で、表示させる
		//メッセージボックス（上にウィンドウが出てくる）
		MessageBox(nullptr, "頂点バッファ作成失敗", "エラー", MB_YESNO);
		//メインのメッセージ、タブ、　OKボタン(他にも種類がある)
		//→★→これにより、失敗したところで、メッセージを出してもらう

		return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//→エラーの時、呼び出し先がそのエラーにより、帰ってきたエラーという表示により、プロジェクトを終了させるなど、処理を行わせる
						//呼び出し先では、一切責任は負わない（あくまで、Mainの一番上で、終了などの、そのエラーによる処理を行う）

	}
	//関数を作るとき、HRESULT型にしておいたほうが、結果を知れてよいことが多い
	//HRESULT型は、以上のように、エラー判断受け取り→それにより、呼び出し元でエラーを受けたときの処理


	//pDeviceを使いたいのだが、このpDeviceは、DirectX.cppに書かれているものなので、
		//取得できない（ヘッダしかインクルードしていないので、）
	//その時に、使うのが、extern
		//Direct３Dにて、pDeviceというポインタをしゅとくしている　（監督）
		//→そこで、初期などをしているものを、このクラスの中でほしい
		//→だが、.cppにて、初期化しているポインタが最終的には欲しい、
		//→ヘッダだけをインクルードしても、インクルードするというのは、あくまで、そこに書いてあることを、インクルード先のソースでコピーして貼り付けているに過ぎない
			//→となると、.cppにて初期化しているものは適用されない

		//★→cppにて、初期化したものを使いたいのだ、共通したものを使いたいのだ
		//★→インクルードするヘッダにて、extern Class *class;	と、externとして定義しておく

		//→するとどうなるのか・→externとして定義したものは、どこかで宣言するよ→となる、その宣言したものが、ヘッダの中のclassに入る。
		//→他クラスで、ヘッダをインクルードして、classを呼び込むと、→どこかで宣言したclassが、（つまり、初期化されて作られたものが）対象になる
		//→★★共通の変数、クラスを使うことが可能になる



	//インデックス情報
	//メンバのインデックス情報を宣言
	//→インデックス情報では、各モデルごとに頂点は違うため、オーバーライドの関数内でそれぞれ作成してもらう
	index = new int[vertex_] { 0, 2, 3, 0, 1, 2};	//時計回りに、頂点をとる

	return S_OK;
}

//ここで、初期化の処理内で、CreateBufferなどの、HRESULT型の処理がある。その時、エラーが出たとき
//引数：ファイルのパス
HRESULT Sprite::Initialize(std::string fileName, SHADER_TYPE thisShader)

{
	thisShader_ = thisShader;

	//配列要素をはじめに宣言し、それを後に編集することは、できない
	//→そのため、あらかじめ、宣言しておいた形での更新ではなく、
		//→かつ、関数の戻り値で取得する形でもなく（indexの一つの配列のために新しいオーバーライドの関数を作るわけにもいかないので、InitVertexの関数に入れてしまおうと考えた（しかし、InitVertexの戻り値は、すでに決まっているので、））
	//



	if (FAILED(InitVertex()))
	{
		MessageBox(nullptr, "モデルの頂点情報の初期化失敗", "エラー", MB_YESNO);
		return E_FAIL;
	}



	//int index[] = { 0,2,3, 0,1,2, 0,4,1 };
									//頂点、三角形の頂点のつながりを書く（どの頂点と、どの頂点と、どの頂点で、三角形ができているのか）
									//最初は、２つの三角形だkえ{ 0,2,3, 0,1,2 }
	//星形　int index[] = { 2,1,0, 3,4,5 };
				//三角形にした時の最終頂点数　＝６

	//以下の、メモリのサイズを指定しているので、
		//→配列におけるメモリのサイズを取得しなくてはいけない
	//→ここでindexの配列のサイズを取得するとき
		//→配列の型は　int →　そして、sizeofというのは、intの型のサイズまで取得できる
	//→となれば、int を　頂点数分取得
	//sizeof(int) * vertex_
	//intのサイズ　＊　頂点数分	→　intの配列の要素数分のサイズ

	//配列の要素数を取得したい
		//インデックス情報を取得した後に、その数分だけ、要素数を頂点数として取得できれば良いが、、、

	//ポインタをそのまま、Sizeofでメモリの容量を受け取るときは、
		//→ポインタを入れると、→pointerのサイズが出てくる
	byte vertexSize = (byte)(sizeof(int) * (vertex_));


	//三角形を一つ増やす
		///→頂点を必要なだけ増やして、
		//→その頂点のつながりで、三角形を作る→三角形の頂点のつながりを、上記のindexに入れているため→そのindexに、もう一つの三角形の頂点順番を入れる
	//頂点情報と、インデックス情報を変更しても、三角形は表示されない
		//→Drawの描画時にも、最終的な三角形の頂点集を指定しないと描画はできない


	// インデックスバッファを生成する

	D3D11_BUFFER_DESC   bd;

	bd.Usage = D3D11_USAGE_DEFAULT;

	bd.ByteWidth = vertexSize;	//サイズを、indexのサイズ分だけ、（インデックス情報の入っているサイズだけ）

	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

	bd.CPUAccessFlags = 0;

	bd.MiscFlags = 0;



	D3D11_SUBRESOURCE_DATA InitData;

	InitData.pSysMem = (void*)index;	//作成したインデックス情報を渡す
										//インデックスのint型のポインタを、メモリーに渡す？
										//送るときに、（void*）出ないと送れないとエラーを返されるため、(void*)でキャスト
	InitData.SysMemPitch = 0;

	InitData.SysMemSlicePitch = 0;

	//成功したかの判断
	//失敗したら、呼び出し元の関数にエラーを返す
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
	}
	//Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
		//インデックスバッファ（領域）を作成



	//あとは、カメラが動くかもしれないので、
		//→後から、コンスタントバッファと、合わせ得られるように
	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//成功したかの判断
	//失敗したら、呼び出し元の関数にエラーを返す
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
	}
	// コンスタントバッファの作成
	//Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_);


	//テクスチャのロード
	TextureLoad(fileName);


	return S_OK;	//S_　HRESULTの結果が、成功したとき、→最後まで来るので、
				//最後まで来たら、OKですよと、返す（エラーの時、HRESULTの型のところで、エラーですと返している→その時、このInitializeを読んでいる関数に、送られる→それにより、その関数が、処理を行う）
}

HRESULT Sprite::TextureLoad(std::string fileName) {
	//ファイルのロード
	//テクスチャのロード
	pTexture_ = new Texture;
	if (FAILED(pTexture_->Load(fileName)))
	{
		MessageBox(nullptr, "ファイルの読み込みを失敗", "エラー", MB_OK);
		SAFE_DELETE(pTexture_);
		return E_FAIL;
	}
	//ロード時

	return S_OK;
}

HRESULT Sprite::TextureLoad(ID3D11Texture2D* pTexture) {
	//ファイルのロード
	//テクスチャのロード
	pTexture_ = new Texture;
	if (FAILED(pTexture_->Load(pTexture)))
	{
		MessageBox(nullptr, "ファイルの読み込みを失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	//ロード時

	return S_OK;
}

//テクスチャを受け取り、
//そのテクスチャから、　テクスチャクラスでDirect3Dて創ったテクスチャからサンプラーなどを作り、
	//描画などができる体制を作ることができる。
HRESULT Sprite::Initialize(ID3D11Texture2D* pTexture, SHADER_TYPE thisShader)
{
	thisShader_ = thisShader;


	//配列要素をはじめに宣言し、それを後に編集することは、できない
	//→そのため、あらかじめ、宣言しておいた形での更新ではなく、
		//→かつ、関数の戻り値で取得する形でもなく（indexの一つの配列のために新しいオーバーライドの関数を作るわけにもいかないので、InitVertexの関数に入れてしまおうと考えた（しかし、InitVertexの戻り値は、すでに決まっているので、））
	//



	if (FAILED(InitVertex()))
	{
		MessageBox(nullptr, "モデルの頂点情報の初期化失敗", "エラー", MB_YESNO);
		return E_FAIL;
	}



	//int index[] = { 0,2,3, 0,1,2, 0,4,1 };
									//頂点、三角形の頂点のつながりを書く（どの頂点と、どの頂点と、どの頂点で、三角形ができているのか）
									//最初は、２つの三角形だkえ{ 0,2,3, 0,1,2 }
	//星形　int index[] = { 2,1,0, 3,4,5 };
				//三角形にした時の最終頂点数　＝６

	//以下の、メモリのサイズを指定しているので、
		//→配列におけるメモリのサイズを取得しなくてはいけない
	//→ここでindexの配列のサイズを取得するとき
		//→配列の型は　int →　そして、sizeofというのは、intの型のサイズまで取得できる
	//→となれば、int を　頂点数分取得
	//sizeof(int) * vertex_
	//intのサイズ　＊　頂点数分	→　intの配列の要素数分のサイズ

	//配列の要素数を取得したい
		//インデックス情報を取得した後に、その数分だけ、要素数を頂点数として取得できれば良いが、、、

	//ポインタをそのまま、Sizeofでメモリの容量を受け取るときは、
		//→ポインタを入れると、→pointerのサイズが出てくる
	byte vertexSize = sizeof(int) * (vertex_);


	//三角形を一つ増やす
		///→頂点を必要なだけ増やして、
		//→その頂点のつながりで、三角形を作る→三角形の頂点のつながりを、上記のindexに入れているため→そのindexに、もう一つの三角形の頂点順番を入れる
	//頂点情報と、インデックス情報を変更しても、三角形は表示されない
		//→Drawの描画時にも、最終的な三角形の頂点集を指定しないと描画はできない


	// インデックスバッファを生成する

	D3D11_BUFFER_DESC   bd;

	bd.Usage = D3D11_USAGE_DEFAULT;

	bd.ByteWidth = vertexSize;	//サイズを、indexのサイズ分だけ、（インデックス情報の入っているサイズだけ）

	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

	bd.CPUAccessFlags = 0;

	bd.MiscFlags = 0;



	D3D11_SUBRESOURCE_DATA InitData;

	InitData.pSysMem = (void*)index;	//作成したインデックス情報を渡す
										//インデックスのint型のポインタを、メモリーに渡す？
										//送るときに、（void*）出ないと送れないとエラーを返されるため、(void*)でキャスト
	InitData.SysMemPitch = 0;

	InitData.SysMemSlicePitch = 0;

	//成功したかの判断
	//失敗したら、呼び出し元の関数にエラーを返す
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
	}
	//Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
		//インデックスバッファ（領域）を作成



	//あとは、カメラが動くかもしれないので、
		//→後から、コンスタントバッファと、合わせ得られるように
	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//成功したかの判断
	//失敗したら、呼び出し元の関数にエラーを返す
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
	}
	// コンスタントバッファの作成
	//Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_);


	//テクスチャのロード
	//ここで、　D3D11型のテクスチャを渡して、　初期化
	TextureLoad(pTexture);


	return S_OK;	//S_　HRESULTの結果が、成功したとき、→最後まで来るので、
				//最後まで来たら、OKですよと、返す（エラーの時、HRESULTの型のところで、エラーですと返している→その時、このInitializeを読んでいる関数に、送られる→それにより、その関数が、処理を行う）
}


//HRESULT Sprite::Draw(XMMATRIX& worldMatrix)
HRESULT Sprite::Draw(Transform transform)
//回転の行列を受け取る（ワールド行列→ベクトルに行列をかけて、回転の度合いをかけたベクトルにしたときのように）
//ワールド行列で、移動、回転、拡大などなど　それぞれの行列を受け取る（→ワールド行列を、回転、移動２つの行列を送りたいなら、→あらかじめ２つの行列をかけてそれを送れば、２つの行列の変化を受けた行列になるので、（２つあるときは、→あらかじめ２つで掛け算をしてしまい、それを送るう））

{
	//Transformの計算
	transform.Calclation();

	//シェーダーを選択
	Direct3D::SetShaderBundle(thisShader_);
		//２Dのシェーダーを使用するために、Direct3D内の、シェーダーをセットする、関数へ、enumのタイプを扱ってあげる
		//Direct3Dの、enumの値をセットして、送ってあげる 



	
	CONSTANT_BUFFER cb;
	
	//拡大の行列として、テクスチャのサイズにする、サイズに拡大行列

		//スクリーンのサイズと、テクスチャのサイズとで、拡大行列
		//拡大の行列に扱う、値というのは、ｘ、ｙ、ｚでの値である→なので、　ｚは×
			//ｘ、ｙのそれぞれの拡大率を出し、その値を、拡大行列に変換する関数へ入れる
			//８００分の５１２　で　０．〜という値が出る→つまり、その値を関数へ入れれば、縮小になるので、
			//ｘは横なのでWidth　、　ｙは、縦なのでHeight
			//テクスチャサイズ／スクリーンサイズ

//	Texture tex;	//テクスチャサイズを入手す	//すでに、textureをLoadしている、ポインタを取得している（Textureのそのものを、作成しても、Loadしていないので、画像のサイズは出てこない）
	//XMMATRIX adjustScale = XMMatrixScaling(pTexture_->GetWidth() / Direct3D::scrWidth, tex.GetHeight() / Direct3D::scrHeight, 1.0f);
	//float texWidht = (float)pTexture_->GetWidth() / Direct3D::scrWidth;	//floatでキャストして、float型の計算とする
	//float texHeight = (float)pTexture_->GetHeight() / Direct3D::scrHeight;
	
	float texWidth = (float)pTexture_->GetWidth();	//ピクセル１のサイズが求められたので、テクスチャそのままのサイズを取得→後は、これを１ピクセルのサイズで拡大させればよい（ウィンドウにおける１ピクセルをテクスチャのサイズ分拡大（ウィンドウにおけるテクスチャのサイズを出せる））
	float texHeight = (float)pTexture_->GetHeight();

	//テクスチャのもともとのサイズに拡大、縮小する行列
	XMMATRIX adjustScale = XMMatrixScaling(
		texWidth , 
		texHeight, 
		1.0f);	//拡大行列を作成する
																	
				//横幅の拡大率、縦幅の拡大率、ｚ幅１．０ｆ

	//ウィンドウにおける１＊１のサイズに縮小、拡大させる行列
	//デフォルトでウィンドウサイズに表示されるのを
	//→ウィンドウサイズの１ピクセルの大きさに縮小する行列にする
		//1.0f / ウィンドウ幅
		//１ピクセル分の大きさに一度縮小して、そのピクセルをテクスチャサイズ、に拡大させればよい
		//１＊１のサイズを求めることができた→このサイズ分、テクスチャを拡大すれば→ウィンドウサイズ

	XMMATRIX miniScale = XMMatrixScaling(
		1.0f / Direct3D::scrWidth,
		1.0f / Direct3D::scrHeight,
		1);


	//ワールド行列のみ送る
	//位置などが、存在する、ワールド行列を送る
	//Transformで、world行列を取得することができるが、
		//→そのままワールド行列を掛けてしまうと、回転の時に、ウィンドウサイズをもとに回転などがされる(横長のウィンドウサイズなら、斜めに回転させたとき、斜めに伸びる)
		//掛ける順番を考える必要がある（Transformのワールド行列をそのまま使えない）
		//移動、回転、行列の順番を考えて、それぞれadjustScaleに掛けなければならない
		//拡大→回転→移動
	//×順番を考えて, ajustScaleをそれぞれ順番に掛けなくてはいけないが、→まず、最初に、adjustScaleをそれぞれの行列に掛けて、それを最後に順番道理に掛ける？
	
	/*★******************************************************************/
	//１ピクセルのサイズ（１＊１のサイズ）とtextureサイズ　とワールドそれぞれ
	//順番を考えればテクスチャそのままのサイズで回転などが行えるはず
	//textureそのままのサイズで、拡大したくて、(adjustScale * matScale_)
	//textureそのままのサイズで、回転させたい、（adjustScale * matScale_ * matRotate_）
	//その行列をウィンドウサイズの１＊１のサイズで拡大（ウィンドウにおける１＊１行列でテクスチャで拡大、回転させた行列を拡大）
	//最後に移動

	cb.matW = XMMatrixTranspose(adjustScale * transform.matScale_  *  transform.matRotate_ * miniScale *  transform.matTranslate_);
	/*★******************************************************************/
	//cb.matW = XMMatrixTranspose(adjustScale * transform.matScale_ * transform.matRotate_ * transform.matTranslate_);
	//cb.matW *= XMMatrixTranspose(adjustScale * transform.matRotate_);
	//cb.matW *= XMMatrixTranspose(adjustScale * transform.matTranslate_);
	//cb.matW = XMMatrixTranspose(adjustScale * transform.GetWorldMatrix());	//	位置を示すワールド行列と、拡大行列をかける



	D3D11_MAPPED_SUBRESOURCE pdata;



	//成功したかの判断（エラーチェック）
	//失敗したら、呼び出し元の関数にエラーを返す
	if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "コンスタントバッファのマップの書き込み失敗", "エラー", MB_YESNO);

		return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
	}
	//Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);	// GPUからのデータアクセスを止める
						//Mapとして、書き込むために呼び込む
	memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	// データを値を送る
					//配列にあるデータなどを、他の領域に●っとコピー
					//memcpy_s (コピー先、コピー先のサイズ、コピー元　、コピー元のサイズ)
					//コンスタントバッファに先ほど作ったものを書き込む
						//コピー元の先頭アドレスを入れたい、void 型のアドレス→アドレスであって、特にどの型でもないので、voidの、アドレスの型にキャストしている
						//構造体で作った、cbのアドレスを、→入れる

	//テクスチャのサンプラーの取得
	ID3D11SamplerState* pSampler = pTexture_->GetSampler();	//テクスチャクラスあkら、サンプラーを取得
	Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);
	//シェーダーへの橋渡しのビューを取得
	ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
	Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);


	Direct3D::pContext->Unmap(pConstantBuffer_, 0); 	//再開
						//書き込んだので、最後に、Unmapにて、　音汁
						//コンスタントバッファに送る



						//頂点バッファ（頂点を持っておくための領域（確保場所））
		//複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
	UINT stride = sizeof(VERTEX);	//1頂点のサイズを指定する、→位置のみであれば、XMVECTORの構造体であったが、
								//位置とUVで構造体をとるので、その構造体のサイズで
	UINT offset = 0;
	Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

	// インデックスバッファーをセット
	stride = sizeof(int);
	offset = 0;
	Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	//コンスタントバッファ
	Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
	Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用


	//描画
	//★頂点の個数を表す、→ここの頂点の数を、最終的な三角形の頂点の数
	//★この情報を追加しないと、頂点による、三角形を描画してくれない（たとえ頂点情報、インデックス情報を変更しても。。。）
	Direct3D::pContext->DrawIndexed(vertex_, 0, 0);	//頂点が６つなので、６つ


	//成功
	return S_OK;

}
//


void Sprite::Release()

{
	pTexture_->Release();
	SAFE_DELETE(pTexture_);
	SAFE_DELETE(index);	//インデックス情報を使用する、関数内にて、宣言し、使い終わった後に開放してしまうと、エラーになってしまったので、すべての作業が押さった後に、呼び出すようにめんばへんすうにした
	//ヘッダにて、確保したポインタを、逆順にリリース
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);


	//ID3D11から始まる、型→Releaseが用意されているので、→それで解放
	//頂点バッファなどを、確保するために、使った、型を開放


}


void Sprite::ChangeShader(SHADER_TYPE shaderType)
{
	//２D専用のシェーダーであることを確認
	bool exists = false;
	/*if (shaderType == SHADER_2D)
	{
		exists = true;
	}
	else if (shaderType == SHADER_KUROSAWA_MODE_2D)
	{
		exists = true;
	}*/

	switch (shaderType)
	{
	case SHADER_2D:
	case SHADER_KUROSAWA_MODE_2D:
		exists = true;
		break;
	default:
		break;

	}

	//専用シェーダーであるはずだ
	assert(exists == true);


	thisShader_ = shaderType;
}


void Sprite::HitCollision(ImageCollision * imageCollision)
{
	//引数にてもらったTransfromを用いて
	//Transformのpositionは、
		//プロジェクション行列になっている。
		//そのため、そのプロジェクションをスクリーン座標にする
		//ｘ1が　スクリーン横幅の/2 = scrWidth / 2.0f になるので、　現在のpositionをスクリーン座標に当てはめる//Transformのpositionを、その計算式にあて、求める

	//Transformにて受け取っている、プロジェクションは、
		//以下にて使用する、Direct3Dの値をスクリーンサイズとしてプロジェクション座標を出しているので、
		//プロジェクションをスクリーン座標に直したときに、相違は生まれないはず。

	//だが、その上でも、　座標に差が出てしまう。

	//その差はどこから生まれてしまうのか、調べる


	//マウスのポジションでもらえるマウスポジションが、
		//おそらく、×ボタンや、タイトルなどを含むバーの分の余分なピクセルを含んだ、スクリーン座標となっていないことが原因と考える
		//つまり、GetMousePosにてもらう、
			//ポジションを、　その余分なサイズ分足した位置にするべき。





	int scrWidth = Direct3D::scrWidth;
	int scrHeight = Direct3D::scrHeight;


	//画像のTransform値から、画像の原点となるピクセルを求める
	XMVECTOR imagePix = GetPixelToWorldConversion(
		imageCollision->trans->position_.vecX,
		imageCollision->trans->position_.vecY);

	float width = pTexture_->GetWidth();
	float height = pTexture_->GetHeight();

	//原点の位置から、
	//四角形を作る頂点の4頂点のピクセル位置を求める
	XMVECTOR scrVertex[] =
	{
		//左上
		XMVectorSet(imagePix.vecX - (width / 2.0f) , imagePix.vecY - (height / 2.0f) , 0.f , 0.f),
		//右上
		XMVectorSet(imagePix.vecX + (width / 2.0f) , imagePix.vecY - (height / 2.0f) , 0.f , 0.f),
		//右下
		XMVectorSet(imagePix.vecX + (width / 2.0f) , imagePix.vecY + (height / 2.0f) , 0.f , 0.f),
		//左下
		XMVectorSet(imagePix.vecX - (width / 2.0f) , imagePix.vecY + (height / 2.0f) , 0.f , 0.f),
	};




	//画像の原点ピクセルの
		//原点（０，０）までの移動値
		//原点に移動し、移動してから、　拡大、回転を行う。
		//そのうえで、　移動値分戻し、　元の座標に戻す。
	float moveValueX = imagePix.vecX;
	float moveValueY = imagePix.vecY;


	//以下に、単純に、奥行きが０
		//回転、拡大がされていない時のスクリーン座標を求める
	for (int i = 0; i < 4; i++)
	{
		//float x = scrVertex[i].vecX;
		//float y = scrVertex[i].vecY;

		//スクリーン範囲外判定
			//回転、拡大を考えずに、範囲外であれば、範囲内に切り詰めるというやり方をして計算をしているが、
			//これは、回転などを行ったうえで、切り詰めてしまうと、　範囲が変わってしまう。
				//そのため、回転、拡大を行ったら、　切り詰める作業はしない
				//切り詰めずに、範囲を求める
		//x =  WithinRange((int)x , scrWidth , 0);
		//y = WithinRange((int)y, scrHeight, 0);

		//座標を拡大行列を用いて拡大
		//座標を回転行列を用いて回転
			//移動は、移動値を用いてスクリーンピクセル位置を求めているため、　考えずに、頂点を、拡大、回転させる
			//順番は　scale * rotate
		//上記の方法では、
			//単純に値を小さくしてしまう
		//そのため、原点を画像のピクセル位置として、それをもとに回転をさせれば、
			//https://imagingsolution.blog.fc2.com/blog-entry-111.html
			//任意点から周りの回転移動
		//画像のピクセルの原点を、（０，０）に移動したとして、
			//その原点で、　拡大、回転を行い、それぞれの頂点の位置を求める。
			//そして、　それぞれの頂点を、元の座標分（移動した分）戻す作業を行う。
		//http://www.charatsoft.com/develop/otogema/page/05d3d/rotscale.html
		//�@
		//頂点を、移動
			//頂点の原点を、（０，０）に移動することで、
			//頂点も同様に移動する
		scrVertex[i] = scrVertex[i] - XMVectorSet(moveValueX, moveValueY, 0.f, 0.f);
		//上記の計算により、
		//原点（０，０）から見た
		//座標となる


	//�A
	//原点（0，0）を原点とした座標に
		//拡大行列、回転行列を掛けて　座標を求める
		scrVertex[i] = XMVector3TransformCoord(
			scrVertex[i],
			imageCollision->trans->matScale_ * imageCollision->trans->matRotate_);
		//プロジェクション座標を
			//ビューポート行列を使って、計算するとき、
			//−１〜１の範囲を超えているときに、ピクセルに直したりすると、
			//スクリーン座標を出すことはできない。
		//だが、　今回は、ビューポート行列などは使わないため、
			//範囲外のピクセル位置を示されても、衝突判定の計算を行う上では問題ないと考える


		//�B
		//移動した分戻す
		scrVertex[i] = scrVertex[i] + XMVectorSet(moveValueX, moveValueY, 0.f, 0.f);



		//scrVertex[i] = XMVectorSet(x, y, 0.f, 0.f);
	}


	//各頂点を使い、
	//三角形2つとして、頂点と三角形の内外判定を行う


	//四角形を　
		//SV0 , SV1 , SV3 で一つの三角形
		//SV1 , SV2 , SV3 で一つの三角形
	bool collision = PointInTriangle(
		imageCollision->scrPointPos,
		scrVertex[0],
		scrVertex[1],
		scrVertex[3]);
	if (collision)
	{
		imageCollision->hit = true;
		return;
	}

	collision = PointInTriangle(
		imageCollision->scrPointPos,
		scrVertex[1],
		scrVertex[2],
		scrVertex[3]
	);
	if (collision)
	{
		imageCollision->hit = true;
		return;
	}



	//// 点が四角形の中にあるかチェック
	//if (imageCollision->scrX >= imagePix.vecX &&
	//	imageCollision->scrX <= imagePix.vecX + width &&
	//	imageCollision->scrY >= imagePix.vecY &&
	//	imageCollision->scrY <= (imagePix.vecY + height))
	//{
	//	imageCollision->hit = true;
	//}
	//else
	//{
	//	imageCollision->hit = false;
	//}




}

float Sprite::WithinRange(int value, int max, int min)
{
	//範囲より大きい
	if (value > max)
	{
		return (float)max;
	}
	//範囲より小さい
	else if (value < min)
	{
		return (float)min;
	}

	return (float)value;
}

float Sprite::Sign(XMVECTOR point1, XMVECTOR point2, XMVECTOR point3)
{
	return (point1.vecX - point3.vecX) *
		(point2.vecY - point3.vecY) -
		(point2.vecX - point3.vecX) *
		(point1.vecY - point3.vecY);
}

bool Sprite::PointInTriangle(XMVECTOR point, XMVECTOR v1, XMVECTOR v2, XMVECTOR v3)
{
	//https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
	//http://www.thothchildren.com/chapter/5b267a436298160664e80763

	//外積
	//https://mathtrain.jp/gaiseki#:~:text=%EF%BC%88%E5%86%85%E7%A9%8D%E3%83%BC1%EF%BC%89%EF%BC%9A%E3%82%B9%E3%82%AB%E3%83%A9%E3%83%BC,%E5%9E%82%E7%9B%B4%E3%81%AA%E3%83%99%E3%82%AF%E3%83%88%E3%83%AB%E3%81%AE%E3%81%93%E3%81%A8%E3%80%82



	//外積を用いて、
	//三角形の内外を求める

	//外積を行ったときに正負が三点で一致すると、三角形は内側にあると判断される
	bool b1, b2, b3;

	b1 = Sign(point, v1, v2) < 0.0f;
	b2 = Sign(point, v2, v3) < 0.0f;
	b3 = Sign(point, v3, v1) < 0.0f;

	return ((b1 == b2) && (b2 == b3));
}

XMVECTOR Sprite::GetMyTextureSize()
{
	return XMVectorSet(pTexture_->GetWidth(), pTexture_->GetHeight(), 0.f, 0.f);
}



XMVECTOR Sprite::GetPixelToWorldConversion(float x, float y)
{
	//ワールドをピクセルに直す
	//引数にて渡されるワールドとしての値は、
		//２Dにおいてはプロジェクション座標として扱われる。
		//すなわち、　ｘ１は、スクリーンにおける　scrWidthの値になる（つまり右端）
					//ｘ０は、　　　　　　　　　　0					(つまり左端)

	//それをもとに、
	//引数にて渡されたワールド（もといプロジェクション）をスクリーンにおけるピクセル値に直す

	float scrWidth = Direct3D::scrWidth;
	float scrHeight = Direct3D::scrHeight;
	////上記のw,hが、プロジェクション座標における（0,0,z）になる
	//	//画面の中心の位置となる。
	//	//プロジェクションは、世界を直方体に圧縮したもの。　（2*2*1）の世界の広さに圧縮したもの
	//	
	//	//1	 = scrWidth
	//	//0  = scrWidth / 2.0f
	//	//-1 = 0
	//	

	//	//scrWidth * (x + 1.0f) = 仮にｘが１なら　scrWidth * (1.f * 1.f)
	//	//scrWidth * 2.0f - scrWidth = scrWidth 

	//	//引数のｘ　＝　プロジェクションの基準となる値を＋１して、　−の値をなくし
	//	//その値を　スクリーンサイズに掛ける。　そうすることで、０〜２の値が取得可能

	//	//そして、最後に、scrWidth ＝　１のサイズ分、引く
	//	//そうすることで、　１は　２になった後に　−１　=　１の時のサイズ
	//					//0.5 は　1.5になった後に　−１ = 0.5の時のサイズを出すことが可能となる


	////↓
	////０であった場合　値が０になってしまうので、
	////scrWidth + (scrWidth * x)とすれば、
	//	//結果、０〜２の値になり、　そのピクセルの値から　-1分のピクセルを引けばよい
	////↓
	////引数値をもとに、
	//	//scrWidth * xを行て、
	//		//どのぐらい進むのかを求める
	//		//それを　scrWidth から引くことで、　差分を求める。

	////上記を移動値として、
	//	//scrWidth に足すことで、右端（scrWidth ）からどのぐらい移動するのか、その移動量をもとに移動させる
	//float pixX = scrWidth / (scrWidth * x);
	//pixX = scrWidth - pixX;

	////Yも同様に求める
	//float pixY = scrHeight - (scrHeight * (y * -1.f));
	//pixY = scrHeight - pixY;
	////上記により、　−のない、　０〜scrWidth の値に切り詰める


	////切り詰めた方法と逆の手順で
	////ピクセルをワールドに切り詰めたのは、
	//	//ピクセルのほうの広さをワールドに合わせた

	//	//今回もピクセルのほうをワールドに広げればよい？

	//

	//最高値
	//−１〜１を　０〜２に切り詰めたら
	//最高値は２になる

	//なので、
	//引数の値は　-1~1になるので、　それを　０〜２の範囲にして、
		//最高値と割合をとる

		//その割合を、　スクリーン座標と計算する
	float maxX = 2.0f;
	float maxY = 2.0f;

	float percentageX = (x + 1.0f) / maxX;
	float percentageY = ((y*-1.0f) + 1.0f) / maxY;

	float pixX = scrWidth * percentageX;
	float pixY = scrHeight * percentageY;





	//上記において、
	//−１〜１におけるピクセル（スクリーン）座標を取得可能
	return XMVectorSet((float)pixX, (float)pixY, 0.f, 0.f);


}
