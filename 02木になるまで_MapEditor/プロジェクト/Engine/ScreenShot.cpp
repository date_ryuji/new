#include "ScreenShot.h"
#include "../Direct3D11/d3dx11.h"	//Direct3DX11
#include "Global.h"
//#include "Engine/Input.h"



using namespace std;


//スクリーンショットを適用するための
//プログラム
/*
	適用方法（概要）＜スクショ実行から、ファイル出力手前まで＞
		：スクショクラスのインスタンスを生成、スクショ関数を呼び込む
		：スクリーンショットを作成する関数が呼ばれたときに、
		　画像ファイルをAssetsフォルダ下に作成するプログラム
		：スワップチェーン（表と裏のバッファ（画面情報を書き込んでいる画用紙）を入れ替える人）
		　から、バックバッファの情報をもらうことによって、バックバッファの画面情報がもらえる。
		 　その画面情報をDirect３DのTexture2D型のテクスチャへ情報を渡して、バックバッファの画面情報で、テクスチャを作成する。
		：テクスチャから、画像保存形式（JPEG,PNG）を選んで、ファイルを生成する。


	適用方法＜スクショ画像ファイルへ保存まで（保存までの操作など）＞
		/****

		仮段階で、スクショ生成がわかるようにメッセージボックスなどで表示している。

		****

		：スクショ制作を「開始」することをWindows上のMessageBox機能を使用して表示。
		：スクショを生成。
		：スクショ制作を「完了」したことをWindows上のMessageBox機能を使用して表示
	


	検証済み結果（バックバッファへのアクセスについて）
		：画像W*H値　：：　バッファに登録したサイズにより決まる。（ウィンドウサイズを手動で変更しても適用されない。）

		：Update途中で、バックバッファを画像化させても、
		　問題なくバックバッファ情報（画像情報）を読み込める
		：現在表示しているフロントバッファと、裏で書き込みを行うバックバッファ
		　そのバックバッファを白紙にするのは、Drawの段階。
		　であれば、Update時点でバックバッファを読み込んでも、問題なく描画されたままのバッファ状態を読み込める


	参考サイト
		：MaverickProject ■Direct3D 11.0　スクリーンショット作成
		　http://maverickproj.web.fc2.com/d3d11_04.html
		：HatenaBlog DirectX11の初期化
		　http://eorf.hatenablog.com/entry/2016/08/27/120814
		：Gameつくろー！その４ charとUnicodeとワイド文字をごっちゃにしないために
		　http://marupeke296.com/CPP_charUnicodeWideChar.html
		：Microsoft ●D3DX11SaveTextureToFile
		　https://docs.microsoft.com/ja-jp/previous-versions/direct-x/ee416901(v=vs.85)?redirectedfrom=MSDN
		：Qiita C++におけるファイルの存在確認
		　https://qiita.com/takahiro_itazuri/items/e999ae24ab34b2756b04
		：UsefullCode.net　TCHARとかLPCTSTR、LPTSTRって何？？？
		　https://www.usefullcode.net/2006/11/tcharlpctstrlptstr.html
*/



//コンストラクタ
ScreenShot::ScreenShot():
	ROOT_DIRECTORY_("Assets/ScreenShot"),
	MAX_FILE_COUNT_(10)
{

}

//デストラクタ
ScreenShot::~ScreenShot()
{

}




// スナップショット作成
HRESULT ScreenShot::CreateScreenShot()
{
	//エラー処理の結果を入れる変数
	HRESULT hr;


	//ファイルの保存先パス
	//ファイルまでのフルのパスを入れておくtchar型の文字列(ディレクトリからファイル名までのパス)
	TCHAR path[256];



	// 初期化が完了してないときはスクリーンショットを作成しないようにする。
	//Direct3Dにて使用している、pDevice、pDeviceContextを使用
	if (Direct3D::pDevice == nullptr || Direct3D::pContext == nullptr)
	{
		hr = E_FAIL;	//エラー結果を受ける変数へ、エラーであることを入れる（なんのエラーかは、関係なしに、とにかくエラーだ）
		//メッセージボックスとして、小さいウィンドウを表示しエラーを知らせる
		MessageBox(nullptr, "デバイスコンテキスト、デバイスのポインタ取得失敗", "スクショ作成失敗", MB_OK);
		return hr;
	}

	//スクリーンショット開始を宣言
	MessageBox(nullptr, "スクリーンショットの作成開始", "作成中", MB_OK);


	/**スクリーンショットの保存先の確保**************************************************************************/
	// メンバにて指定したルートのフォルダがありますか？
		//あるなら、何もしない
		//ないなら、新たに作成
	//関数場所：Shlwapi.h
	//引数：LPCSTR型(const char*) スクショを保存するルートのパス
	if (::PathFileExists((LPCSTR)ROOT_DIRECTORY_) == false)
	{
		// ないので ScreenShotフォルダを作成する
		//関数場所：fileapi.h
		//引数：LPCSTR型　スクショを保存するルートのパス
		if (::CreateDirectory((LPCSTR)ROOT_DIRECTORY_, NULL) == FALSE)
		{
			hr = E_FAIL;	//エラー
			//ディレクトリ（フォルダ）作成に失敗
			MessageBox(nullptr, "スクショ専用ディレクトリ作成失敗", "スクショ作成失敗", MB_OK);
			return hr;//エラーを返す
		}
	}

	//ファイルの番号を示す。（ファイルの番号が、MAXを超えない間、ファイル番号のファイルが存在しない場合ファイル作成へ移る）
	int i = 0;
	// ファイルを作成しすぎてディスクがパンクすることがないようにするため、ファイルの作成可能数を限定する。
	while (i < MAX_FILE_COUNT_)
	{
		//保存先のパスを作成（ディレクトリからファイルまで）
		//pathの文字列に、各文字列を合わせた文字列を代入
			//第一引数：文字列を代入する変数
			//第二引数：表示形式を指定するフォーマット
			//第三引数：フォーマットの指定子
		_stprintf_s(path, _T("%s\\%02d.jpg"), ROOT_DIRECTORY_, i);
		//path = %s(ファイル保存のルートディレクトリ名) + %02d(ファイル名);
		//パス = 文字列(const char * = string) + ２桁の１０進数(int)


		// ファイルがありますか
			//なかったら、while抜けるを抜ける（iの番号確定）
		if (::PathFileExists((LPCSTR)path) == FALSE) break;

		i++;
	}
	//繰り返しを抜けて
	// スクリーンショットの作成可能数をオーバーしているため作成できません。
	if (i >= MAX_FILE_COUNT_)
	{
		hr = E_FAIL;
		//エラーメッセージの表示
		MessageBox(nullptr, "スクショ作成数オーバー", "スクショ作成失敗", MB_OK);
		return hr;
	}



	//バックバッファにて書き込まれた画像情報をもらうTexture2D型クラスのポインタで確保
		//＝スクショとして保存する画像の情報を受け取るインスタンス
	ID3D11Texture2D* pBackBuffer = nullptr; // バッファのアクセスに使うインターフェイス(バッファのスクリーン情報を2Dテクスチャとして扱う)



	/**スクリーンの画面のバッファを画像化**************************************************************************/
	// バックバッファを描画ターゲットに設定（テクスチャ情報として読み込むもの）
					// スワップチェインから最初のバックバッファを取得する

	hr = Direct3D::GetSwapChain1()->GetBuffer(
		0,							// とってくるバックバッファの番号（０番目のバックバッファ（バックバッファは１つしか使わない））
		__uuidof(ID3D11Texture2D),	// バッファにアクセスするインターフェイス
		(LPVOID*)&pBackBuffer		// バッファを受け取る変数
	);
	if (FAILED(hr)) 
	{ 
		//エラーメッセージの表示
		MessageBox(nullptr, "スワップチェインから、バックバッファの画像情報取得失敗", "スクショ作成失敗", MB_OK); 
		return hr; 
	}


	// ファイル出力
	// D3DX11SaveTextureToFile（d3dx11.h）
	//第一引数：デバイスコンテキスト
	//第二引数：ファイル作成するデータ（テクスチャ）
	//第三引数：データ形式（JPEG）
	//第四引数：保存先パス
	hr = D3DX11SaveTextureToFile(Direct3D::pContext, pBackBuffer, D3DX11_IFF_JPG, (LPCSTR)path);
	if (FAILED(hr)) 
	{
		//エラーメッセージの表示
		MessageBox(nullptr, "バックバッファの画像情報を画像ファイル化すること失敗", "スクショ作成失敗", MB_OK);
		return hr; 
	}

	//スクリーンショットができたことを知らせる
	MessageBox(nullptr, "スクリーンショットの作成終了", "完了", MB_OK);


	//解放
	SAFE_RELEASE(pBackBuffer);
		//参照カウント方式で、
		//バッファーのポイントをもらったので、
		//ここで解放させてしまう（もらったことで、見えないところのカウントが一つ上がったので、そのカウントを下げる）



	//エラーがなく、最後までたどり着いたことをエラー結果変数へ代入
	hr = S_OK;

	//エラー結果がなかったことを返す
	return hr;
}

HRESULT ScreenShot::CreateScreenShot(ID3D11Texture2D * pBuffer , const TCHAR* rootPath, const TCHAR * fileName)
{
	//エラー処理の結果を入れる変数
	HRESULT hr;

	// 初期化が完了してないときはスクリーンショットを作成しないようにする。
	//Direct3Dにて使用している、pDevice、pDeviceContextを使用
	if (Direct3D::pDevice == nullptr || Direct3D::pContext == nullptr)
	{
		hr = E_FAIL;	//エラー結果を受ける変数へ、エラーであることを入れる（なんのエラーかは、関係なしに、とにかくエラーだ）
		//メッセージボックスとして、小さいウィンドウを表示しエラーを知らせる
		MessageBox(nullptr, "デバイスコンテキスト、デバイスのポインタ取得失敗", "スクショ作成失敗", MB_OK);
		return hr;
	}

	//スクリーンショット開始を宣言
	MessageBox(nullptr, "スクリーンショットの作成開始", "作成中", MB_OK);



	/**スクリーンショットの保存先の確保**************************************************************************/
	// メンバにて指定したルートのフォルダがありますか？
		//あるなら、何もしない
		//ないなら、新たに作成
	//関数場所：Shlwapi.h
	//引数：LPCSTR型(const char*) スクショを保存するルートのパス
	if (::PathFileExists((LPCSTR)rootPath) == false)
	{
		// ないので ScreenShotフォルダを作成する
		//関数場所：fileapi.h
		//引数：LPCSTR型　スクショを保存するルートのパス
		if (::CreateDirectory((LPCSTR)rootPath, NULL) == FALSE)
		{
			hr = E_FAIL;	//エラー
			//ディレクトリ（フォルダ）作成に失敗
			MessageBox(nullptr, "スクショ専用ディレクトリ作成失敗", "スクショ作成失敗", MB_OK);
			return hr;//エラーを返す
		}
	}


	//ファイルの保存先パス
	//ファイルまでのフルのパスを入れておくtchar型の文字列(ディレクトリからファイル名までのパス)
	TCHAR createPath[256];


	//保存先のパスを作成（ディレクトリからファイルまで）
	//pathの文字列に、各文字列を合わせた文字列を代入
		//第一引数：文字列を代入する変数
		//第二引数：表示形式を指定するフォーマット
		//第三引数：フォーマットの指定子
	_stprintf_s(createPath, _T("%s/%s.png"), rootPath, fileName);
	//path = %s(ファイル保存のルートディレクトリ名) + %02d(ファイル名);
	//パス = 文字列(const char * = string) + ２桁の１０進数(int)


	// ファイル出力
	// D3DX11SaveTextureToFile（d3dx11.h）
	//第一引数：デバイスコンテキスト
	//第二引数：ファイル作成するデータ（テクスチャ）
	//第三引数：データ形式（PNG）
	//第四引数：保存先パス
	hr = D3DX11SaveTextureToFile(Direct3D::pContext, pBuffer, D3DX11_IFF_PNG, (LPCSTR)createPath);
	if (FAILED(hr))
	{
		//エラーメッセージの表示
		MessageBox(nullptr, "バックバッファの画像情報を画像ファイル化すること失敗", "スクショ作成失敗", MB_OK);
		return hr;
	}


	//スクリーンショット作成終了を宣言
	MessageBox(nullptr, "スクリーンショットの作成終了", "終了", MB_OK);


	//エラーがなく、最後までたどり着いたことをエラー結果変数へ代入
	hr = S_OK;


	return hr;
}


