/*
*
* スクリーンショットの機能と、画像ファイル化を行うクラス
* 作者名：伊達龍二
* 制作日：2020/09/30〜
* その他記述すべきことがある場合の記述
*		：実装形式	：	GameObject型のオブジェクトとして生成させる。（Instantiateの関数）
						ほかのGameObjectと同じように、生成し、解放するようにする。
						子供リストにスクショのインスタンスを追加すれば、スクショ生成キーにて生成可能。
					：Updateにて、スクショ実行キーの受付
*		：保存できる画像ファイル上限数	：：１０（変更可能）
*		：画像ファイルを保存するフォルダ：：Assets/ScreenShot/
*		：画像ファイルの名前			：：連番の番号

		

*/


//標準でのDirectXでのインクルードがエラーとなる（d3dx11.hなどのソース）
//（現段階での対処）：必要なDirect３Dの、ソースをプロジェクトフォルダにコピーしておく

#pragma once
#include <Shlwapi.h>	//PathFaileExits
#include <iostream>
#include <tchar.h>	//＿T、_stの関数を用いるため

#include "Direct3D.h"	//Direct3Dにて使用しているスワップチェーンのポインタを扱うためにインクルード


//リンカ
#pragma comment(lib, "Direct3D11/d3dx11.lib")
#pragma comment(lib, "Shlwapi.lib")




class ScreenShot
{
private:


	//スクショ画像を保存する、ルートになるディレクトリ名(標準のルートパス下に作成（この段階でのルートパス：Assets/）)
		//charの配列で文字列を表したいので、const char*にして、文字列を扱えるようにする（現在char型で文字列を扱うときは、constをつける）
	const TCHAR* ROOT_DIRECTORY_;
	//TCHAR型　＝　ワイド文字か、標準の文字かを指定せずに、どちらも対応できる型を使用する
	//Unicodeが定義されていないときには、TCHAR=char,
	//Unicodeが定義されているときには、　TCHAR=WCHAR

	//作成できる画像ファイル数指定
	const int MAX_FILE_COUNT_;




public:

	//コンストラクタ
	ScreenShot();
	//デストラクタ
	~ScreenShot();



	//スクリーンショットを作成する関数
	//現在描画している、バッファー（スワップチェインにて入れ替えているバッファー部分）のテクスチャを取得して、画像化する
	//指定ファイルパスへスクリーンショットを作成する関数
	//引数：なし
	//戻値：hr エラー結果（エラーがない場合：S_OK,エラーの場合：E_FAIL（どのようなエラーであっても））
	HRESULT CreateScreenShot();

	//スクリーンショットを作成する関数
	//指定ファイルパスへスクリーンショットを作成する関数
	//引数テクスチャバッファーのスクリーンショットを作成する
	//引数：テクスチャのポインタ
	//引数：ルートディレクトリのパス	：("Assets/〜〜")	<最後のフォルダに/　を付ける必要なし>
	//引数：ファイル名					：("〜〜")			<最後に	.png(拡張子)　を付ける必要なし>
	//戻値：hr エラー結果（エラーがない場合：S_OK,エラーの場合：E_FAIL（どのようなエラーであっても））
	HRESULT CreateScreenShot(ID3D11Texture2D* pBuffer , const TCHAR *rootPath , const TCHAR * fileName);



};


