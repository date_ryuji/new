#pragma once
#include "Direct3D.h"
#include <DirectXMath.h>

using namespace DirectX;



//-----------------------------------------------------------
//カメラ
//-----------------------------------------------------------
namespace Camera
{
	//extern宣言（後で絶対に正しく宣言するよ）
	extern XMVECTOR position_;	//カメラ位置	


	//初期化（プロジェクション行列作成）
	void Initialize();

	//更新（ビュー行列作成）
	void Update();

	//視点（カメラの位置）を設定
	void SetPosition(const XMVECTOR& position);
	void SetPosition(float x , float y , float z);	//SetPositionの、ｘ、ｙ、ｚだけを受け取る関数を持っておく
						//名前が同じで、引数が違う（オーバーロー度）

	//視点を取得
	XMVECTOR GetPosition();
	//焦点を取得
	XMVECTOR GetTarget();

	//焦点（見る位置）を設定
	void SetTarget(XMVECTOR target);
	void SetTarget(float x, float y , float z);

	//画角（視野の広さ）を設定
	void SetAngle(float angleValue);
	void AddAngleForZoomOut(float addValue);
	void SubAngleForZoomIn(float subValue);


	//ビュー行列を取得
	XMMATRIX GetViewMatrix();

	//プロジェクション行列を取得
	XMMATRIX GetProjectionMatrix();
};


//ｃｐｐの中であれば、Cameraに、追加でnamespace Cameraとすることはできるが、
//★→ヘッダの中で、２つ目のCameraと定義すると→多重定義となって、エラーをおこす

