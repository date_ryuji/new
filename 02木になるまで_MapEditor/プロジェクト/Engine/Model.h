#pragma once
//Fbxファイルを扱う
	//疑似フライウェイトパターンを扱う
#include <string>
#include <vector>
#include <Shlwapi.h>	//パスが存在するか//PathFileExists関数を使用するため
#include "Transform.h"
#include "PolygonGroup.h"
#include "Direct3D.h"

#include "../Shader/CommonDatas.h"



//ライブラリ
#pragma comment(lib , "Shlwapi.lib")

//シェーダーのタイプを示すenum
	//プロトタイプ宣言
enum SHADER_TYPE;



//Model名前空間にて
//扱われるポリゴン群のタイプ
enum POLYGON_GROUP_TYPE;





//３Dオブジェクトのデータのロード、描画、Transformのセット
//１シーンにおける３Dオブジェクトのモデルデータを保存しておく動的配列を持つ
namespace Model
{
	//１シーンにて扱うモデルデータ
	//構造体の作成
		//使用するモデルを保存しておくもの（ロードは１回しかしないとは別の話）
		//使用するモデル１つ１つのデータを持っておくもの
	/*
		必要な要素

		・FBXファイル　１つのモデルデータに対してモデルは一つ（球モデルとなる、Fbxファイル）(すでにロードしたものが存在するなら、ここには、すでにあるFbxのアドレスをもらう)


		・Transform	そのモデルのTransform(モデル１個１個のTransform)

		・fileName	すでにファイルをロードしていたかは、ファイル名にて判断するとする。（同じファイル名がすでに存在していたらロードしない（すでに同じファイル名のFbxがロードされている場合、すでに存在するモデルデータのFbxのアドレスを自身のModelDataのFbxポインタに持つ））
	
	
	*/
	struct ModelData
	{
		Transform transform;
		std::string fileName;

		POLYGON_GROUP_TYPE thisPolygonGroup;
		SHADER_TYPE thisShader;
		
		PolygonGroup* pPolygonGroup;
		


		//構造体もコンストラクタが使用可能
		ModelData();
	};
	//Modelをインクルードされた先にて、
	//使用数Fbxファイルのデータを持っておく構造体
	//インクルード先でLoadされたモデルを保存しておくので、何個になるかわからない
	/*
		モデルのロードは、Bulletクラスが消されても
		球のモデルデータはなくしたくない

		消すタイミングはシーンの切り替え時

		vectorは、頭からお尻まで入れられた順に並ぶ前提（見るのが容易）（途中で抜かれることがないので、配列の添え字指定のように見れる）
					途中で抜かれることを考慮しない

		listは、途中で抜いたりが簡単（for文の途中で抜いても、次の人のアドレスをイテレータに渡してあげるだけ）
	
		★なので、今回は消すのはシーン切り替えの最後の段階
			//→なので、vector型を使用
		★使い分けよう
	*/
	//namespaceだと、どこからでもアクセスできる（保守性の面で高くない）
		//インクルードをする際は.hのみ。アクセスしたくないのであれば、ｃｐｐに（変に、外部から変更されると困る）

	


	//ロード
	//モデルのFBXをロードして、ロードを完了したらモデル番号になる値を返して、
		//ロードできなかったらー１を返す
	//引数：fileName ロードするモデルファイル名
	//引数：Fbxに適用するシェーダータイプ
	//戻値：handle モデルを読み込んだら、そのモデルの番号。失敗したらー１
	int Load(std::string fileName, POLYGON_GROUP_TYPE thisPolygonGroup = FBX_POLYGON_GROUP , SHADER_TYPE thisShader = SHADER_NORMAL);

	//モデルデータを新規に追加する
	//引数：モデルデータ
	//戻値：モデル番号
	int AddModelData(ModelData modelData);

	//引数データをコピーする
	//引数：コピー先
	//引数：コピー元
	void AllDataCopy(ModelData* pCopyTo , ModelData* pOriginal);





	//指定されたモデルデータのTransfromをセットする
	//トランスフォームの位置、サイズ、拡大率を受け取る
	//引数：handle トランスフォームを変更するモデル番号
	//引数：transform セットするトランスフォーム(&　基本型以外のデータを受け取るときは＆を使用)
	//戻値：なし
	void SetTransform(int handle , Transform& transform);

	//指定されたモデルデータのFbxの描画を行う
	//引数：handle 描画するモデル番号
	//戻値：なし
	void Draw(int handle);

	//クラス全体の解放処理
	void Release();

	//モデルデータの全消去
		//モデルデータは、シーンが切り替えられたときに行えばよい（１っ回１っ回クラスオブジェクトが解放と同時にFbxファイルなどのデータが解放されても困る（せっかくモデルデータを保存しておいた意味がなくなる））
		//１つ１つの球をロードする際に、毎回ロードしたら困るのでモデルデータを持っておく動的配列を宣言した。（１つの球が解放時に、モデルデータを動的配列から消したら。意味ない）
	void ReleaseAllModelData();
	
	//指定された要素の解放
		//FBXファイルなどは、すでにロードしているものを自分のFBXのデータとして所有している可能性もあるため、
		//→その場合は、解放しないなどの処理を行う
	//引数：解放するモデルデータ配列の添え字
	//戻値：なし
	void ReleaseOneData(int handle);


	//レイキャストによる、レイとFBXとの衝突判定
		//引数のモデル番号により、特定のモデルとの衝突判定
		//第二引数にて、レイキャストのデータをもらい、それを衝突判定時のデータとして使用する。
	void RayCast(int handle, RayCastData* rayData);

	//引数ベクトルと、Fbxと衝突した面の法線ベクトルを帰す
	//引数：Fbxハンドル
	//引数：面との衝突判定を行うベクトルのスタート地点
	//引数：面との衝突判定を行うベクトルの方向
	//戻値：衝突した面の法線ベクトル
	XMVECTOR NormalVectorOfCollidingFace(int handle, XMVECTOR& start, XMVECTOR& dir);



	//シェーダーを切り替える
	//引数：モデルハンドル
	//引数：切り替え先のシェーダータイプ
	void ChangeShader(int handle , SHADER_TYPE shaderType);

	//現在のシェーダーをもらう
	//引数：モデルハンドル
	SHADER_TYPE GetShader(int handle);



};

