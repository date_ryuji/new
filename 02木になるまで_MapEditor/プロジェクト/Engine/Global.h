//いろいろなクラスから呼ばれる可能性のあるマクロ関数などをまとめておくヘッダ


#pragma once

//ポインタの中身の解放と、ポインタ自身の解放
#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
//マクロ
//→deleteの際に、ポインタが、領域のアドレスを覚えているときに、そのアドレスを忘れさせる
	//→これによって、deleteした後に、

//配列ポインタの解放
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){delete[] p; p = nullptr;}

//Release
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}
//Releseも同じく
//ポインタに使用されているポインタのReleaseを呼んであげる。



//XMVECTORにおける、X成分、Y成分、Z成分にアクセスを容易にする定数
//本来であれば、positionのベクター型のｘ成分にアクセスするには、
	//m128_f32[0]を指定しなくてはいけない→だが、１年生のように名称で管理したい
	//m128_32f[0]を名称で管理できるように名前で作っておく→#defineの定数でvecXとして取得
#define vecX m128_f32[0]
#define vecY m128_f32[1]
#define vecZ m128_f32[2]
#define vecW m128_f32[3]	//４次元目の値　長さを求める時や、方向を求めるときに４時限目に値が入っていると想定通りに動かないこともあるので、０で初期化をするのが理想


