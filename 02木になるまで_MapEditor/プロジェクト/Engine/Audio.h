#pragma once
#include <windows.h>
#include <string>


namespace Audio
{
	//オーディオの準備・初期化
	void Initialize();

	//ロード関数
		//新たにオーディオファイルをロードしたいときに呼び込む
	//引数：オーディオファイル名
	//引数：ソースボイスの作成数（連続して音を鳴らすときに、ソースボイス一人では再生が終わるまで連続で鳴らす作業ができないため、音を鳴らす演奏者を複数用意する。その用意数。＝１により、引数にて指定されなかった場合、自動で１が入る）
	//戻値：オーディオファイルのハンドル番号
	int Load(std::string fileName , int create = 1);

	
	
	//再生
	//引数：再生させたいオーディオのハンドル番号
	void Play(int hundle);

	//停止
	//引数：停止させたいオーディオのハンドル番号
	void Stop(int hundle);

	//無限ループを設定
	//引数：ハンドル番号
	void InfiniteLoop(int handle);
	//無限ループを止める
	//引数：ハンドル番号
	void NotInfiniteLoop(int handle);

	//音量変更
	//引数：ハンドル番号
	//引数：音量の割合　（0.0f ~ 1.0f）(0% ~ 100%)
	HRESULT SetVolume(int handle , float volume);

	
	//解放
	void Release();


	////データ群の中から指定のデータを解放
	////引数：解放させたいデータを示す添え字
	//void ReleaseOneData(int handle);



	//オーディオデータの全消去
		//オーディオデータのみ解放（シーン切り替え時などに使用する）
	void ReleaseAllAudioData();

};

