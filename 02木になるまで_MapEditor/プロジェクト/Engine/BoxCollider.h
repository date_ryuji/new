#pragma once
#include "Collider.h"


class BoxCollider : public Collider
{
private :
	//原点を中心
	XMVECTOR size_;
	


public : 
	//コンストラクタ
	//引数：コライダーの離れ具合
	//引数：ｘｙｚ方向へのサイズ（ｘ２→原点からx方向へ１、−x方向へ１の長さ）
	//引数：デバック用のワイヤーフレームを描画するか
	BoxCollider(XMVECTOR position , XMVECTOR size, bool drawWireFrame = false);
	//デストラクタ
	~BoxCollider() override;


	//衝突判定実行
		//引数のコライダーのタイプを調べ、
		//自身（四角形の箱）と　相手（箱OR球）の当たり判定を呼び込む（Colliderクラスに計算式は存在するので、その関数を呼び込む）
	bool IsHit(Collider* target) override;


	//自身のサイズ返す
	XMVECTOR GetSize();

	//自身のサイズを、ワイヤーフレームモデルへわたす
	void SetWireFrameSize() override;


};

