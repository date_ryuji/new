#include "HeightMapGround.h"

#include "Texture.h"
#include "Camera.h"
#include "../Shader/BaseShader.h"


HeightMapGround::HeightMapGround():
	PolygonGroup::PolygonGroup(),
	SCALE_(0.5f),
	MAX_Y_(10.0f),
	MIN_Y_(0.0f)
{
}

HeightMapGround::~HeightMapGround()
{
}

HRESULT HeightMapGround::Load(std::string fileName, SHADER_TYPE thisShader)
{
	//自身の使用シェーダーの登録
	thisShader_ = thisShader;

	{

	/*
	�@
		引数にて示された、
		画像ファイルから、
		ハイトマップの画像をロード。
	�A
		その画像から、
		縦横比をもらう。
	�B
		その比率をもとに、
		頂点数を比率に等しくなるように確保。
		そして、決められたサイズで、頂点のローカル座標を決めていく。
	�C
		頂点のY座標を、
		画像のピクセルごとに、色情報を取得し、
		その色情報をもとに、白に近いほど、あらかじめ決めた、最高Y座標位置に近く、
						　	黒に近いほど、あらかじめ決めた、最低Y座標位置に近く、していく


		これで、基本的な、頂点情報は作成完了。
		あとは、コンスタントバッファとか、いろいろ作成していく。


	*/


	/*�@**************************************************************************/
	//テクスチャのバッファを入れておくポインタ
	ID3D11Texture2D* pTextureBuffer;

	//ハイトマップの画像
	Texture* pHeightMap = new Texture;

	//バッファーの情報を入れておく構造体
	D3D11_MAPPED_SUBRESOURCE hMappedres;

	//バッファーを取得
	//テクスチャを読み込んで、そのバッファーを引数ポインタに登録
	if (FAILED(pHeightMap->GetTextureBuffer(fileName, &pTextureBuffer, &hMappedres)))
	{
		MessageBox(nullptr, "ハイトマップのテクスチャバッファー取得失敗", "エラー", MB_YESNO);
		pHeightMap->Release(); SAFE_DELETE(pHeightMap);
		return E_FAIL;
	}

	/*�A**************************************************************************/
	//画像サイズを取得
	width_ = pHeightMap->GetWidth();
	depth_ = pHeightMap->GetHeight();

	//テクスチャクラスの解放
	pHeightMap->Release();	//何もテクスチャクラスには情報を残していないが、一応解放
	SAFE_DELETE(pHeightMap);

	/*�B**************************************************************************/
	//Y座標を除いた
	//頂点情報の確保、確立
	InitVertex();

	//Y座標をヘイトマップから読み取って、Y座標を確保、確立
	InitVertexY(&hMappedres);
	SAFE_RELEASE(pTextureBuffer);


	//バッファー作成
	CreateVertexBuffer();



	}


	//インデックス情報の作成
	InitIndex();

	//コンスタントバッファ作成
	CreateConstantBuffer();

	//テクスチャのロード
	LoadTexture();







	return S_OK;
}


HRESULT HeightMapGround::InitVertex()
{
	//単純に、三角ポリゴンを並べるマップを作ってみる

	//頂点数の確保
		//Width＊Depth分だけ確保
		//そのため、頂点は、画像のピクセル数と同じ数にする。
		//→頂点から作られる、三角ポリゴンを敷き詰めた、マス。→これは、頂点数より1つ少ないマスができるということになる。
			//→頂点を5つ置いたら、頂点と頂点の間のスペースは4つになる。ということ。
	vertexCount_ = (width_) * (depth_);

	//頂点座標(頂点情報などを持つ構造体を頂点ごと取得)
	pVertices_ = new VERTEX[vertexCount_];


	//UV座標
	//var uv = new Vector2[((width + 1) * 2 + 1) * (height + 1) + width + 1];

	//インデックス情報数
		//0行目と1行目で、
			//Width - 1 分だけ、▲ポリゴン * 2ができる
			//▲ポリゴンは3頂点なので
		//Width - 1 * 2 * 3 = 一行のインデックス情報の数
		//上記を＊height - 1分

		//絵にかくと分かりやすいが、　縦横　Width ＊Depth分頂点を書いて、インデックスが何個必要なのか
	indexCountEachMaterial_ = ((width_ - 1)* 2 * 3) * (depth_ - 1);
	//インデックス情報
	pIndex_ = new int[indexCountEachMaterial_];


	//頂点をカウントするカウンター
	int p = 0;

	//頂点を、横にまず、並べて、
	//そして、から次の行に同じ数だけ並べて、を繰り返す

	//頂点確保
	for (int i = 0; i < depth_; i++)
	{
		for (int k = 0; k < width_; k++)
		{
			//1頂点ずつ、まずは、横並びに等間隔に頂点の取得
			//その行の頂点を確保
			pVertices_[p].position.vecX = k * SCALE_;
			pVertices_[p].position.vecZ = i * SCALE_;
			pVertices_[p].position.vecY = 0;	//後に改めておこなう


			//確認用
			XMVECTOR xmxm = pVertices_[p].position;

			p++;
		}
	}
	return S_OK;

}

HRESULT HeightMapGround::InitVertexY(D3D11_MAPPED_SUBRESOURCE * hMappedres)
{


	//１ピクセル事、
	//データを取得する
	int size = 4;	//rgbaのピクセル情報をバッファーに格納しているので、４ずつ
	//テクスチャのピクセルデータにアクセスするためのポインタ
	//０〜２５５の値が取れるように
		//unsigned char 型でデータを取得する。
	unsigned char* dest = static_cast<unsigned char*>(hMappedres->pData);

	//上記のポインタにて示される1つ1つのデータ（unsigned char型）の
	//に入る値が、０　なら　最低。　２５５なら最高という。
		//RGB値を０〜２５５で取得したときの色値が返ってくる
		//その値をもとに、RGBAの値を取得し、1ピクセルの色を取得
		//その色をもとに、黒に近いなら、最低Y座標へ。白に近いなら、最高Y座標へ。

		//https://teratail.com/questions/213854


	//頂点情報の配列の添え字を表すカウンター
	int p = 0;

	for (int v = 0; v < depth_; v++)
	{
		for (int u = 0; u < width_; u++)
		{
			////RGBA
			////をそれぞれ格納する領域
			//unsigned char c[4];

			////RGBAの４回回す
			//for (int i = 0; i < size; i++)
			//{
			//	//destは
			//	//RGBAの4バイトにアクセスするポインタである。
			//	//そのため、destに最初に入ってくるのは、RGBAのRの色情報（０〜２５５）

			//	//dest ++ ;　をしたら、次は、ポインタが回って、1バイト進んで、　Bの色情報（０〜２５５）
			//	//ト順番で確保できる。

			//	//その情報を、4バイトの配列に格納して、
			//	//1ピクセルの色を確立させる


			//	// R G B A のどれか１つを入れる変数
			//	unsigned char color;
			//	//destが示している1バイトを
			//	//コピー
			//	memcpy(&color, dest, 1);

			//	// R G B A のどれかを、順番に格納
			//	//色情報をコピー
			//	c[i] = color;


			//	//ポインタを一つ進める
			//	dest++;


			//}

			////RGBが同様の値である、（(0,0,0) , (1,1,1) , (0.2 , 0.2 , 0.2 )など）
			////白黒の表現であるならば、
			//	//→これは、Rだけを取得して、それをRGBの値として共通の値として用いて計算させる


			//RGBAのR部分だけを取得する
			unsigned char rColor;

			//destが示している1バイトを
				//コピー
			memcpy(&rColor, dest, 1);

			//RGBA分先に進めて、次にRを見たいので、
				//現在見ているポインタ（R）から４つ進める
			/*for (int i = 0; i < size; i++)
			{
				dest++;
			}*/
			dest += 4;
			




			/*
			�@Rの値を取得する
			�Aその値が、２５５と割って、どの程度の割合か
			�BY座標の最高値と最低値の間の、�Aの割合にて示されるY座標を求める(線形補完)（XMVECTORLerpのようなもの）
				//最高値の割合を仮に１，最低値の割合を仮に０としたとき、　�Aの割合0.5があったとき、 最高値の持っている値と最低値の持っている値　を線で結んだとき、�Aの割合の0.5の位置（値）を求める
			�Cその座標を、その頂点のY座標とする。


			*/

			//1バイト（unsigned char）の最高値
			const int UC_MAX = 255;
			//1バイト（unsigned char）の最低値
			const int UC_MIN = 0;

			//�@
			int r = (int)(rColor);

			//�A
			float per;
			if (r == 0)
			{
				//０との割り算はできないので
				per = 0.0f;
			}
			else
			{
				//割合を取得
				per = r / (float)(UC_MAX);
			}


			//�B
			float between = MIN_Y_ + per * (MAX_Y_ - MIN_Y_);


			//�C登録
			pVertices_[p].position.vecY = between;

			//頂点を進める
			p++;

			int aaa = 0;



		}


	}
	//上記で、
	//RGBAの値を取得できるので、
		//その色から、
		// 0000 から　1111の間でのそのピクセル色の割合を取得する

		// rgb の合計が　0なら、黒。　rgb の合計が 3なら、白。　などといった判断の仕方で、
		//どのぐらいの間にいるのかを割合で取得。

	//それによって、出現の高さを取得する。

	return S_OK;
}

HRESULT HeightMapGround::CreateVertexBuffer()
{
	//頂点情報を
	//頂点バッファに登録（バッファに登録することになるので、ここで、頂点情報は、いらなくなっているのだが、、（頂点情報が変わらない限り。））
	// 頂点データ用バッファの設定
	//バッファに渡す情報の取得
	D3D11_BUFFER_DESC bd_vertex;


	//bd_vertex.ByteWidth = sizeof(vertices);
	//VERTEXという、構造体を、頂点数分サイズの確保
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;

	bd_vertex.Usage = D3D11_USAGE_DEFAULT;

	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	bd_vertex.CPUAccessFlags = 0;

	bd_vertex.MiscFlags = 0;

	bd_vertex.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA data_vertex;

	data_vertex.pSysMem = pVertices_;

	////メンバ変数に保存
	//data_vertex_ = new D3D11_SUBRESOURCE_DATA;
	////ここに保存を行って、更新の時は取り出し、更新するというやり方を行う。
	//	//だが、これだとメンバ変数として持っておくことと変わらない。（どっかに動的確保したもののアドレスを持っているだけなので、、、、　これなら、メンバ変数として持っておくほうが幾分見やすい）
	//data_vertex_->pSysMem = vertices;

	//頂点バッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		//エラーの原因のウィンドウ表示
		MessageBox(nullptr, "頂点バッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーですと返す。
	}

	return S_OK;


}

HRESULT HeightMapGround::InitIndex()
{
	//インデックス情報の登録


//イメージは、
//仮にWidth=5 , height = 5
 //一行に頂点は　５　、　それが５行　となるように配置
	

 //0行目から見て行って、
	//0行目と、1行目と三角形の作り方を考える
//それが終わったら、次は
	//1行目と、2行目の三角形の作り方と遷移する

//つまり、0行目には、
	// 頂点番号　０〜４
//		  1行目には、
	// 頂点番号　５〜９
// 〜

//★敷き詰めた頂点からインデックスによって、三角ポリゴンを作る。
 //0行目の。０番目の頂点は、
	//1行目の。５と６番目の頂点と1つの三角形
//0行目の。1番目の頂点は、
	//1行目の。６と７番目の〜
//上記を　３番目まで続ける。（k < width - 1）

//1行目の。1番目(つまり６)の頂点は
	//0行目の。０と１番目の頂点と1つの三角形。
//上記を　４番目まで続ける。　(k < width)


//上記を　高さ分繰り返す

/*
0行目で、右に斜辺が向いている直角三角形を作り、

1行目で、左に斜辺が向いている直角三角形を作り、

その連続を行う

*/


	int p = 0;

	//ポリゴンの枚数を数える変数
	//int polygonCounter = 0;

	//ポリゴンの枚数を確保
	polygonCount_ = indexCountEachMaterial_ / 3;
	//ポリゴン情報を、ポリゴン数分確保
		//インデックス情報は3つで1枚のポリゴンなので
		//　インデックス情報数 / 3 すれば、ポリゴン数になる
	//pPolygonInfo_ = new PolygonInfo[polygonCount_];

	//高さは、頂点をひとつ減らした数分だけ繰り返す
	//理由は、　一回のforにて、　現在のiの行と、その下の行を見るので、 i　を配列の添え字限界まで回すと、範囲外になる
	for (int i = 0; i < depth_ - 1; i++)
	{
		//i = 0
		//0行目と、1行目を使った、三角形を作るインデックス情報の作成

		//0行目の三角形のインデックス情報
		for (int k = 0; k < (width_ - 1); k++)
		{
			//時計回りにインデック情報は登録しないといけないので、
			//順番は逆にしている。

			//ここで注意するのが、
				//ｘ（０，０）から、ｘ（５，５）仮　に向かって頂点を埋めるということ
				//つまり、左手前から右奥へ頂点を埋めるということ

			//例
			//[p + 0]　＝　０行目の０番目
			//[p + 1]　＝　１行目の０番目
			//[p + 2]　＝　１行目の１番目
			//計算説明
			//[p + 0]　＝　(行番目 * １行の頂点数) + 現在の列
			//[p + 1]　＝　(行番目 * １行の頂点数) + 現在の列 + (1行の頂点数)
			//[p + 2]　＝　(行番目 * １行の頂点数) + 現在の列 + (1行の頂点数 + 1)
			/*
			
			+ (行番目 * １行の頂点数) = 行における　頂点番号開始位置を示す
			+ k = 頂点番号＋ｋ　でその行における　現在見ている頂点番号
			+ width = 次の行
			
			*/
			pIndex_[p + 0] = (i * width_) + k;
			pIndex_[p + 1] = (i * width_) + k + (width_);
			pIndex_[p + 2] = (i * width_) + k + (width_ + 1);

			////ポリゴン情報をセット
			////ポリゴン番号をセット。
			//	//０〜
			//	//インデックス情報が欲しいときは、ポリゴン番号＊３　をした頂点から3つが、
			//	//そのポリゴンを作るインデックス情報となる。
			//pPolygonInfo_->polygonNum = polygonCounter;
			//polygonCounter++;

			//追加したインデックス情報分カウント
			p = p + 3;
		}

		//1行目の三角形のインデックス情報
		for (int k = 1; k < width_; k++)
		{
			//時計回りにインデック情報は登録しないといけないので、
			//順番は逆にしている。


			//例
			//[p + 0]　＝　０行目の１番目
			//[p + 1]　＝　０行目の０番目
			//[p + 2]　＝　１行目の１番目
			//計算説明
			//[p + 0]　＝　(行番目 * １行の頂点数) + 現在の列
			//[p + 1]　＝　(行番目 * １行の頂点数) + 現在の列 - 1
			//[p + 2]　＝　(行番目 * １行の頂点数) + 現在の列 + (1行の頂点数)
			/*

			+ (行番目 * １行の頂点数) = 行における　頂点番号開始位置を示す
			+ k = 頂点番号＋ｋ　でその行における　現在見ている頂点番号
			+ width = 次の行
			- 1 = 頂点１つ左

			*/
			pIndex_[p + 0] = (i * (width_)) + k;
			pIndex_[p + 1] = (i * (width_)) + k - 1;
			pIndex_[p + 2] = (i * (width_)) + k + (width_);

			////ポリゴン情報をセット
			////ポリゴン番号をセット。
			//pPolygonInfo_->polygonNum = polygonCounter;
			//polygonCounter++;


			//追加したインデックス情報分カウント
			p = p + 3;
		}


	}

	//インデックスバッファ作成
	CreateIndexBuffer();


	return S_OK;
}

HRESULT HeightMapGround::CreateIndexBuffer()
{

	// インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(int) * (indexCountEachMaterial_);	//サイズを、indexのサイズ分だけ、（インデックス情報の入っているサイズだけ）
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;


	//バッファーの格納情報
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex_;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	//インデックスバッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。
	}

	return S_OK;
}

HRESULT HeightMapGround::CreateConstantBuffer()
{

	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//コンスタントバッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。
	}

	return S_OK;
}

HRESULT HeightMapGround::LoadTexture()
{
	//ファイルのロード
	//テクスチャのロード
	pTexture_ = new Texture;
	if (FAILED(pTexture_->Load("Assets/HeightMap/Grass.jpg")))
	{
		MessageBox(nullptr, "ファイルの読み込みを失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	//ロード時
	return S_OK;
}



HRESULT HeightMapGround::Draw(Transform & transform)
{

	//シェーダーを選択
	Direct3D::SetShaderBundle(thisShader_);

	//シェーダーファイルに渡すための情報をまとめる
	CONSTANT_BUFFER cb;
	cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix()* Camera::GetViewMatrix() * Camera::GetProjectionMatrix());

	//ワールド行列
		//法線の回転のために
		//ライトは固定なのに、、陰も一緒に回っていた。
			//それを回転状況を考慮して、法線を回転させた。
	cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));	//



	//ワールド行列を入れる
		//ワールド行列を渡して、
		//頂点からカメラへの向きというのを取得するために、頂点に掛けてやらないといけない（移動、回転、拡大を込みしたワールド位置にするために）
		cb.matWorld = XMMatrixTranspose(transform.GetWorldMatrix());


	//カメラの位置を入れる
					//XMVECTOR型をFLOAT4型に入れることはできない。
					//変換する関数があったが、忘れてしまったので、1つずつFLOAT４型に変換して代入
	cb.camPos = XMFLOAT4(Camera::position_.vecX, Camera::position_.vecY, Camera::position_.vecZ, 0);


	//テクスチャあり
	if (pTexture_ != nullptr)
	{
		//nullptrでない　＝　テクスチャが存在する
		//「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
			//サンプラーは、どこに貼り付けますか？などの情報
		//hlsl : g_sampler(s0)
		ID3D11SamplerState* pSampler = pTexture_->GetSampler();	//テクスチャクラスから、サンプラーを取得
		//ここにおける第一引数が何番目のサンプラーなどか、
		Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);
		//シェーダーへの橋渡しのビューを取得
		//hlsl : g_texture(t0)
		ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
			//シェーダーに作った変数に渡している
		Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);


		cb.isTexture = TRUE;	//コンスタントバッファのテクスチャ有無にtrueを立てる
	}
	//テクスチャなし
	else
	{
		//nullptrである　＝　テクスチャが存在しない
		//　＝　マテリアルそのものの色を使用しないといけない
		cb.isTexture = FALSE;

	}


	//自身の使用する
	//シェーダー独自の
	//コンスタンバッファや、テクスチャの受け渡しを行う
	Direct3D::GetShaderClass(thisShader_)->ShaderConstantBuffer1View();





	D3D11_MAPPED_SUBRESOURCE pdata;
	//シェーダーなどのGPUでしか、触れることのできないものに
	//CPUから触れる準備
	if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))
	{
		MessageBox(nullptr, "コンスタントバッファのマップの書き込み失敗", "エラー", MB_YESNO);

		return E_FAIL;	//エラーですと返す。
	}
	//メモリにコピー
	memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	// データを値を送る


	{
		//頂点バッファをセット（頂点を持っておくための領域（確保場所））
			//複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		UINT stride = sizeof(VERTEX);	//1頂点のサイズを指定する、→位置のみであれば、XMVECTORの構造体であったが、
									//位置とUVで構造体をとるので、その構造体のサイズで
		UINT offset = 0;
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

		// インデックスバッファーをセット
		stride = sizeof(int);
		offset = 0;
		Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

		//コンスタントバッファをセット
		Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
		Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用

	}

	Direct3D::pContext->Unmap(pConstantBuffer_, 0); 	//再開
						//書き込んだので、最後に、Unmapにて、　閉じる
						//コンスタントバッファに送る


	//描画
	//★頂点の個数を表す、→ここの頂点の数を、最終的な三角形の頂点の数
	//★つまり、インデックス情報の頂点の数
	Direct3D::pContext->DrawIndexed(indexCountEachMaterial_, 0, 0);




	return S_OK;

}

void HeightMapGround::Release()
{

	//ポインタ宣言順と逆に解放
	pTexture_->Release();
	SAFE_DELETE(pTexture_);
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pVertices_);
	SAFE_RELEASE(pVertexBuffer_);


}

void HeightMapGround::RayCast(RayCastData * rayData)
{
}

XMVECTOR HeightMapGround::NormalVectorOfCollidingFace(XMVECTOR & start, XMVECTOR & dir)
{
	return XMVECTOR();
}

ID3D11Buffer * HeightMapGround::GetConstantBuffer()
{
	return nullptr;
}

HeightMapGround::VERTEX::VERTEX() :
	position(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	uv(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	normal(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}
