#include "DebugWireFrame.h"
#include "../Engine/Transform.h"
#include "../Engine/Model.h"

#include "../Shader/CommonDatas.h"


DebugWireFrame::DebugWireFrame():
	hModel_(-1),enable_(false),
	transform_(new Transform)
{
}

DebugWireFrame::~DebugWireFrame()
{
	delete transform_;
}

void DebugWireFrame::SetParentTransform(Transform * pTrans)
{
	transform_->pParent_ = pTrans;
}

void DebugWireFrame::Load(int type)
{
	switch (type)
	{
		//Sphere
	case 0:
		hModel_ = Model::Load("Assets/DebugCollider/DebugSphereCollider.fbx" , FBX_POLYGON_GROUP ,   SHADER_DEBUG_WIRE_FRAME);
		break;
		//Box
	case 1:
		hModel_ = Model::Load("Assets/DebugCollider//DebugBoxCollider.fbx", FBX_POLYGON_GROUP, SHADER_DEBUG_WIRE_FRAME);
		break;
	default:
		break;
	}
	assert(hModel_ != -1);

}

void DebugWireFrame::SetPosition(XMVECTOR & position)
{
	transform_->position_ = position;
}

void DebugWireFrame::SetScale(XMVECTOR & scale)
{
	transform_->scale_ = scale;
}

void DebugWireFrame::SetScale(float radius)
{
	//半径として表すため、
	// 半径　０．５ｆ→　サイズ１．０ｆ　となる。

	//そして、球は、ｘｙｚがともに等しいため、Transform.scale は、ｘｙｚ共通の値を入れる

	//transform_->scale_ = XMVectorSet(radius * 2.0f, radius * 2.0f, radius * 2.0f, 0.f);
	transform_->scale_ = XMVectorSet(radius, radius, radius, 0.f);

}


void DebugWireFrame::DrawWireFrame(bool drawWireFrame)
{
	enable_ = drawWireFrame;
}

void DebugWireFrame::Draw()
{
	if (enable_)
	{
		//Transformを計算し、セットする
		transform_->Calclation();
		Model::SetTransform(hModel_, *transform_);

		//描画
		Model::Draw(hModel_);
	}
}

XMVECTOR DebugWireFrame::NormalVectorOfCollidingFace(XMVECTOR & start, XMVECTOR & dir)
{
	return Model::NormalVectorOfCollidingFace(hModel_ , start, dir);
}
