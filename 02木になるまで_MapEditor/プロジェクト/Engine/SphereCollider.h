//当たり判定を行うクラス

#pragma once
#include "Collider.h"
#include <DirectXMath.h>


using namespace DirectX;


//コライダーの大きさを保有しておくクラス
//半径が一定の球
class SphereCollider : public Collider
{
private :
	
	//コライダーのサイズ（半径の長さ）
	float radius_;

public : 
	

	//コンストラクタ
	//引数：コライダーの離れ具合
	//引数：サイズ（半径）
	//引数：デバック用のワイヤーフレームを描画するか
	//戻値：なし
	SphereCollider(XMVECTOR position , float size , bool drawWireFrame = false);

	//デストラクタ
	~SphereCollider();



	//衝突判定を行う
		//オーバーライド関数
	//引数：相手のコライダー情報　ポインタ
		//ここでは、相手が、どのコライダータイプなのかは考えない
	bool IsHit(Collider* target)override;


	//コライダーのサイズ（半径）を返す
	//引数：なし
	//戻値：自身のコライダーの半径
	float GetRadiusSize(){ return radius_;}

	//自身のサイズを、ワイヤーフレームモデルへわたす
	void SetWireFrameSize() override;


};

