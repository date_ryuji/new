#include "PolygonGroupFactory.h"

//シェーダーの継承元クラス
#include "PolygonGroup.h"
//シェーダーの継承先クラス
#include "Fbx.h"
#include "HeightMapGround.h"
#include "HeightMapCreater.h"


PolygonGroup* PolygonGroupFactory::CreatePolygonGroupClass(POLYGON_GROUP_TYPE type)
{
	switch (type)
	{
	case FBX_POLYGON_GROUP : 
		return new Fbx;
	case HEIGHT_MAP_POLYGON_GROUP:
		return new HeightMapGround;
	case HEIGHT_MAP_CREATER_POLYGON_GROUP : 
		return new HeightMapCreater;



	default:
		return nullptr;

	}

	return nullptr;
}
