#pragma once
#include "GameObject.h"

/*
	ゲームプログラムの構造

	WinMain(メッセージループ)　
	→　RootJob(GameObject継承)　
		→ SceneManager(GameObject継承)　
		→　各Scene(GameObject継承)　
		→　各シーン内オブジェクト(GameObject継承)

*/

/*
	大本の親、（WinMainにて呼ぶ）
		→ゲーム内のオブジェクトは、（シーンも含む）
		→これを一番上の親とする（すべてのオブジェクトは元をたどると一番上の親にはRootJobがいる。（これがGameObjectのFindなどの機能を使用するとき、すべてのオブジェクトのUpdateを呼ぶなどに便利になる））


	この下に、子供を作る
	→すると、Updateが呼ばれたら、さらに、その子供のUpdateが呼ばれる


	なので、Mainでは、このRootJobだけをUpdateなどを呼ぶようにすれば、(正確にはUpdateSub)
		→RootJobの子供の、シーン（シーンマネージャー）のUpdate、
		　→シーンマネージャーのUpdateが呼ばれれば、その子供のシーンのUpdateが呼ばれる
			→シーンのUpdateが呼ばれれば、子供の各オブジェクト（プレイヤー）が呼ばれる


*/

/*
	RootJobですること
	★SceneManagerのインスタンスを自分の子供にする
	（これだけ）

	※SceneManager下に各Scene などが、書かれるので、RootJobはSceneManagerに指示するだけ
	※RootJobの子供はSceneManagerのみ
*/


//GameObjectを継承したので

//純粋仮想関数で定義したUpdateなどをオーバーライドする
	//クラス名クリック→クイックアクションとリファクタリングにて純粋仮想関数の宣言

class RootJob :public GameObject
{
public :
	//コンストラクタ
	RootJob();
	//デストラクタ
	~RootJob();


	// GameObject を介して継承されました
		//Virtualは、あっても、なくても問題はない（Virtualついてても問題は起こらない）
		//このクラスをさらに継承するときは、必要
		//overrideもあっても、なくても同じ（だが、きちんとオーバーライドしていることを目で分かるように）
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

