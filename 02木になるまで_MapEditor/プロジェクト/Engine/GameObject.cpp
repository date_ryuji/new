#include "GameObject.h"
#include "Global.h"			//SAFE_DELETEなどのマクロ関数を所有しているヘッダ
							//どこでも使うようなマクロを持っておく

#include "Model.h"			//Fbxのシェーダーを切り替える
							//自身で所有しているモデルハンドル番号群に、シェーダーを切り替えさせる処理をさせるため。

//#include "Direct3D.h"		//SHADER_TYPEのenum値を取得するため
							//Model.hのプロトタイプ宣言にて存在するため必要なし


#include "Collider.h"


//オーバーロード
//引数なしのコンストラクタが呼ばれたときに、何も要素を持たない引数にして、引数ありのコンストラクタを呼ぶ
//引数なしのコンストラクタ
	//親もない、名前もない
	//→これは、親もない、名前もないという引数で、引数ありのコンストラクタを呼んでやればいい
	//→これをすれば、GameObjectの引数ありのコンストラクタを呼べるので、→同じことをできる
GameObject::GameObject()
	:GameObject(nullptr , "")
{


}

//引数ありのコンストラクタが呼ばれたとき
	//引数に親のGameObjectのポインタ、
	//      自身の名前
//それぞれ初期化
GameObject::GameObject(GameObject * parent, const std::string & name)
	:pParent_(parent),
	objectName_(name),
	isDead_(false)
{
	//親の子供リストに追加
		//→これはInstantiate時に行っている


	//ベクターの初期化（クリア）
	collDatas_.clear();

	//リストの初期化（クリア）（宣言部には書けないので、）
	childList_.clear();

	changeShaderFbxList_.clear();

}

//GameObjectをオブジェクトを消すとき、子供を消さないといけない
//何を消せばいいのか
	//自分が死んだとき
		//その子供は消すのか？＝消す
		//→自分が親だったら、その親が消えたら、子供を消さないといけない
	//親は消すのか＝消さない
		//子供が消えても、親は消えない（プレイシーンの子供の敵が消えても、プレイシーンは消えない）
//自分のReleaseは、自身のオブジェクトの中で動的確保したものなどをReleaseするものなので、
//自身が消えるときにデストラクタで子供の解放を行わなければいけない。
GameObject::~GameObject()
{
	//すべての子供の解放
		//一番最初は、RootJobのReleaseSubにて、子供のReleaseSubが終わったら、AllChildrenKillを呼ぶようにする
			//呼ぶタイミングを考えれば、全オブジェクトの消去ができる
			//AllChildrenKillで行うのは、自身の子供だけの消去。
			//ReleaseSubで、自身の子供のReleaseSubが呼び終わったタイミングであれば、子供を削除のタイミングとしては良いタイミングと思われる。
	//AllChildrenKill();

	//コライダー情報の解放（自身のコライダー）
	AllColliderKill();

	
}

//すべての子供の解放を行う
	//デストラクタ
	//SceneManagerは、シーンを切り替えるときに、自身の子供を全消去を行う
void GameObject::AllChildrenKill()
{
	//子供のReleaseをすべて呼んであげないと
	//そして、Releaseを呼んだら、delete
	//リストをすべて回すためのイテレータ

	//イテレータの型名：std::list<GameObject*>::iterator
	//長くなるので、autoで（自動型）

	//リストの最初から、最後まで、
	//リストを指してくれる、イテレータを回す
	for (auto itr = childList_.begin(); itr != childList_.end();
		itr++)
	{
		//イテレータのさしている子供のReleaseを呼ぶ
		//イテレータのさしているアドレスにある、GameObject
		//(*itr)->Release();
			//すでにReleaseSubで呼んでいるので、ここでは呼ばない。
			//子供だけを解放
				//子供の子供は、子供のReleaseSubが終わったタイミングで自身の子供を解放している
					//1子供→2子供→3子供→4子供
					//　　　　↑　　↑
					//		　↑	4子供の解放（3は自身の解放を行わず）
					//　　　　3子供の解放

					//そして最後の１はRootJobのクラスになるので、
						//RootJobの解放は、Mainにて行う。


		//子供のdelete
		SAFE_DELETE(*itr);


	}

	//動的確保したポインタを解放したら
	//リストの中身をクリアする
	childList_.clear();
}

//コライダー情報の解放（自身のコライダー）
void GameObject::AllColliderKill()
{
	//配列要素分回して

	for (int i = 0; i < collDatas_.size(); i++)
	{
		//解放
		SAFE_DELETE(collDatas_[i]);
	
	}
	collDatas_.clear();

}

void GameObject::Enter()
{
	myStatus_.entered = true;
}

void GameObject::Leave()
{
	myStatus_.entered = false;
}

void GameObject::Visible()
{
	myStatus_.visible = true;
}

void GameObject::Invisible()
{
	myStatus_.visible = false;
}

void GameObject::EnterChildren()
{
	myStatus_.enteredChildren = true;
}

void GameObject::LeaveChildren()
{
	myStatus_.enteredChildren = false;
}

void GameObject::VisibleChildren()
{
	myStatus_.visibleChidren = true;
}

void GameObject::InvisibleChildren()
{
	myStatus_.visibleChidren = false;
}




void GameObject::ChangeParent(GameObject * pAfterParent)
{
	//現在の親のリストから、自身のオブジェクトを削除する
	pParent_->RemoveOneChild(this);
	//親を更新
	pParent_ = pAfterParent;
	//更新後の親のリストに自身を追加
	pParent_->AddOneChild(this);

}
void GameObject::RemoveOneChild(GameObject * pChild)
{
	//自身の子供リストから引数オブジェクトを探し
	//削除する
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		//イテレータの示すオブジェクトと同様のポインタの時
		if ((*itr) == pChild)
		{
			//リストから削除
			childList_.erase(itr);
			return;	//関数終了　抜ける
		}
	}
}

void GameObject::AddOneChild(GameObject * pChild)
{
	//自身の子供リストに引数オブジェクトを追加
	childList_.push_back(pChild);
}

//衝突時にされる処理
//オーバーライドされる関数
void GameObject::OnCollision(GameObject * pTarget)
{
}

////自身を消去するフラグを立てる（フレームの最後に自身が消去されるフラグを立てる）
//void GameObject::KillMe()
//{
//	//実際に自分の消去を行う（自身の消去と、自身の子供の消去を呼び出す）
//	//のは、自身の親のクラスである。（自分で、自分を消去するのは怖い）
//
//	//そして、KillMeから親の子供リストから直接、フレーム途中に消されると、Updateに行う処理もろもろが（消されるオブジェクト以外のところ）消去前のオブジェクトを参照しようとしてエラーになる可能性も
//
//
//	//なので、フラグを立てて、フレームのUpdateなども終わった後に、消去するようにする
//	isDead_ = true;
//
//
//}


//自分の子供から引数にもらった名前のオブジェクトがいるか探す
	//子供の子供も網羅する
GameObject * GameObject::FindChildObject(std::string name)
{
	//子供から引数の名前と同じ名前の人がいたら返す（自身の子供なので、自身の子供リストから）
	for (auto itr = childList_.begin(); itr != childList_.end();
		itr++)
	{
		//イテレータは、各要素を指しているもの
		//イテレータの中身を見たいなら* をつける

		//イテレータの示す子供（GameObject*型）の持つ名前と引数の名前が等しいなら
		if ((*itr)->objectName_ == name)
		{
			//子供のポインタは
			return (*itr);
		}
		//子供が該当のオブジェクトでなかった(returnされなかった)
		//その子供の子供も、さらに探すために（その子供の子供も）
		
			//再帰（自分で自分の関数を呼ぶ）
				//itrの指しているオブジェクトの子供から探したい（itrの指しているオブジェクトもGameObjectなので、当然FindChildObjectが存在する。）
				//(*itr)->FindChildObject(name);
				//見つかったら、return でその子供のオブジェクトが帰ってくる（FindChildObjectより）
				//だが、現段階では、仮に子供の子供リストの中で見つけたとしても、Findから返り値が帰ってきても返せない
				//だが、見つからなかったら、forを抜けてnullを返す可能性もあるため、ここで一気に返してはいけない
			
			GameObject* pObj = (*itr)->FindChildObject(name);
				//探しているオブジェクトがなかったら、nullptrが帰ってくる
				//探しているオブジェクトだったら、その探しているポインタが入ってくる

			//FindChildOjbectによって返ってきた値が
			if (pObj != nullptr)
			{
				//ヌルでないならば、
				//探していたオブジェクト見つかったということなので、ポインタを返す
				return pObj;
			}
				
				

		/*
			PlaySceneからのFindChildObject（"B"）
			Bが欲しい


			PlayScene →　Player　→　A
								　
					　→　Player１→　B

			PlayScene forにてPlayerを見て、Bではない
			→(子供)PlayerのFindをよぶ
			↓
			（子供）Playerが
			Aを見て、Bではない
			→（子供）AのFindを呼ぶ
			↓
			（子供）Aが
			子供いないので、（for抜けて）
			→nullptrを返す
			↓
			Playerがnullptrを受け取って、
			次の子供がいない（for抜けて）
			→nullptr返す
			↓
			PlaySceneがnullptrを受け取って、FindChildによる結果がnullのため、
			forが回って、PlaySceneの次の子供へ
			→(子供)Player1を呼ぶ
			↓



		*/

		
	}

	//子供が見つからなかったのでnullptrを返す
	return nullptr;
}

//自分の親がいなかったら自身がRootJobオブジェクトなので、
//自身のRootJobのポインタを返す
GameObject * GameObject::GetRootJob()
{
	//自分の親を参照して
	//親がいなかったら、
	if (pParent_ == nullptr)
	{
		//自身がRootJobである。
		//→自身のポインタを返す
		return this;
	}
	//自身の親がいたら
	else
	{
		//（再帰）親のGetRootJobを呼ぶ
		return pParent_->GetRootJob();
	}
	//ゲームオブジェクトである以上、（誰かから親子付されてゲームオブジェクトとして出現した以上）
	//RootJobが一番上にいないということはあり得ない
	//一番上に到達したら→それがRootJob
	//なので、GetRootJobを呼んだところに戻って、必ず最後にはRootJobのポインタが帰ってくる
	/*
		（A）親が
			→nullptrでなかったら
		（A）親がいるので
			→（B）親のGetRootJob()
		（B）親がいないので自身がRootJob
			→（B）関数の戻り値で自身のポインタ返す
		 (A)　


	
	*/
}


//全オブジェクトの中から、指定の名前を持つオブジェクトを探す
GameObject * GameObject::FindObject(std::string name)
{
	//全オブジェクトの中から、引数の名前を持つオブジェクトを探したい
		//全オブジェクトの中から探す＝RootJobからFindChildObjectを呼べばいい
		//＝RootJobを探すことができれば、　RootJobのFindChildObject呼ぶだけで、
		//＝全オブジェクトの中から引数の名前を持つオブジェクトを探せる（ここでnullptrが帰ってくるなら、全オブジェクトの中にオブジェクトが存在しないということになる）
	return GetRootJob()->FindChildObject(name);
}

//自身の所有するコライダー配列（ベクター）に
//当たり判定を登録
void GameObject::AddCollider(Collider * addCollider)
{
	//自身の所有しているコライダー配列（ベクター）にコライダーを追加
	collDatas_.push_back(addCollider);

	//コライダーに
	//コライダーをセットしているゲームオブジェクトを知らせる
	addCollider->SetMyGameObject(this);
}

//大本のWinMainにて、DrawSubを読んであげることで、
	//その下の子供のDrawSubを呼んで→さらに子供のDrawSub呼んでと続くので、すべての子供のDrawを網羅できる
//自身のDrawと
//子供のDrawSubを呼んであげる
void GameObject::DrawSub()
{
	//自分のDrawを呼ぶ
		//自身が描画をしてよい状態にあるとき限定
	if (myStatus_.visible)
	{
		Draw();

		//自身のコライダーリストにある、コライダーを描画
		//デバック用として、ワイヤーフレームとして描画許可がされているものを描画
		DrawCollider();
	}

	//自身の子供がが描画をしてよい状態にあるとき限定
	if (myStatus_.visibleChidren)
	{
		//自分のDrawが終わった後に
		//子供全員のDrawSubを呼ぶ
		for (auto itr = childList_.begin();
			itr != childList_.end(); itr++)
		{
			//イテレータにて、リストの全要素を回して
			//指定された子供のDraw"Sub"関数を呼ぶ
				//Subのほうを呼ぶことで、→子供は自身のDrawとともに、さらに、子供のDrawSubを行うようになる
				//子供のクラスでは、Drawしか書かないが、実際は親からDrawSubのほうが呼ばれて、→その中でDrawが呼ばれて、子供のDrawSubが呼ばれる（これだと、子供をすべてDraw呼ぶことができる）
			(*itr)->DrawSub();
		}
	}

}

void GameObject::UpdateSub()
{
	//自分のUpdateを呼ぶ
	//自身が更新をしてよい状態にあるとき限定
	if (myStatus_.entered)
	{
		///自身のUpdateが終わった後に当たり判定
		//Update内で、衝突した相手との検証を行うので、
		//ここで、衝突相手を特定する。
		//どこでCollisionを呼ぶかは、衝突して、
		//・衝突後、移動後に移動分だけ戻すのか
		//・衝突する前に、移動先が衝突する位置なのか
		//このどちらで判断させるかにもよってくる。
		///★今回は→移動してから、移動分だけ戻すという方式をとる衝突判定をとる。（移動後に自身の位置がほかのコライダーと衝突しているか）
		//当たり判定の検証（自身のコライダーと、ほか全体のコライダーとの当たり判定）
		Collision();

		Update();
	}


	//親が動けば、子も動く
	//親が動いている時点で、子供も動いている
		//なので、子供のUpdateSubが呼ばれる前に、親のTransformの計算がされている必要がある
	//Transformの行列計算
	transform_.Calclation();





	//自身の子供が更新をしてよい状態にあるとき限定
	if (myStatus_.enteredChildren)
	{
		//自分のUpdateが終わった後に
		//子供全員のUpdateSubを呼ぶ
		for (auto itr = childList_.begin();
			itr != childList_.end(); itr++)
		{
			//イテレータにて、リストの全要素を回して
			//指定された子供のDraw"Sub"関数を呼ぶ
				//Subのほうを呼ぶことで、→子供は自身のDrawとともに、さらに、子供のDrawSubを行うようになる
				//子供のクラスでは、Drawしか書かないが、実際は親からDrawSubのほうが呼ばれて、→その中でDrawが呼ばれて、子供のDrawSubが呼ばれる（これだと、子供をすべてDraw呼ぶことができる）
			(*itr)->UpdateSub();
		}
	}



	/*消去フラグの立っている子供の消去******************/
		/*
			UpdaeSubにて
			すべてのこどもの
			UpdateSubを呼んだあと(Updateが終わる前に消してはいけない)
		
		*/
	//すべての子供をループして、
	//isDeadのフラグが立っているかの確認
	//イテレータにて、リストの全要素を回して
	for (auto itr = childList_.begin();
		itr != childList_.end();)
	{
		//消すフラグが立っていたら、
		if ((*itr)->isDead_ == true)
		{
			//子供のReleaseSub（子供のすべての解放）
			(*itr)->ReleaseSub();

			//Releaseを読んだら、SAFE_DELETEでポインタの解放
			SAFE_DELETE(*itr);

			//子供リストの中から削除
			itr = childList_.erase(itr);
			/*
				リストの要素を消すとき
				イテレータの削除

				eraseでその場で消してしまうとダメ。

				A,B,C,D
				Aさんは、Bさんの電話番号を知っている（他を知らない）
				Bさんは、Cさんの電話番号を知っている（他を知らない）
			

				回覧板を渡す（その回覧板をイテレータとして）
				イテレータのbeginによって、Aさんに回覧板（イテレータ）回る
				イテレータ＋＋によって、　Bさんに回覧板

				Bさんが途中でいなくなるとする。
					→Bさんがいなくなって、回覧板を持っている状態でいなくなる

				CさんにBさんが持っていた回覧板を渡さなければいけない（BさんだったイテレータがCさんにわたる）

				消えたときCさんがBさんの位置のイテレータの位置に位置することになる
					→なので、for分で回った時に、Cさんがやらずに、次に行ってしまうことになる

				それを防ぐために、
				→イテレータを消したら、イテレータBさんの位置がCさんになる
					→★なので、次に回していけない

				だったら、イテレータを消さなかったら、イテレータを回して（itr++）
				消したらイテレータを回さない(×)
			
			*/
			//イテレータを消されたら、戻り値として
			//消した次の人のイテレータが入ってくる
				//それを次のイテレータとする


		}
		else
		{
			//イテレータが消されなかったらイテレータを回す
			itr++;
		
		}
	}

	
}

void GameObject::ReleaseSub()
{
	//自分のUpdateを呼ぶ
	Release();

	//自分のUpdateが終わった後に
	//子供全員のUpdateSubを呼ぶ
	for (auto itr = childList_.begin();
		itr != childList_.end(); itr++)
	{
		//イテレータにて、リストの全要素を回して
		//指定された子供のDraw"Sub"関数を呼ぶ
			//Subのほうを呼ぶことで、→子供は自身のDrawとともに、さらに、子供のDrawSubを行うようになる
			//子供のクラスでは、Drawしか書かないが、実際は親からDrawSubのほうが呼ばれて、→その中でDrawが呼ばれて、子供のDrawSubが呼ばれる（これだと、子供をすべてDraw呼ぶことができる）
		(*itr)->ReleaseSub();
	}

	//子供の解放
		//delete処理
	AllChildrenKill();

}

//全オブジェクトのコライダーと、自身の全コライダーとの衝突判定
	//全オブジェクトとの総当たり
void GameObject::Collision()
{

	//総当たりのため、全オブジェクトを見るためにRootJobから
		//RootJobから、子供がいるなら
		//→その子供のコライダーとの衝突判定、
		//→子供がいるなら、その子供との衝突判定
	//自身のコライダーを送って、
	//再帰的に、もらったコライダーと呼ばれた側の持っているコライダーとで、衝突判定
		//衝突していたら、
		//→trueが帰ってくる
		//→だが、これでは、１つのコライダーにあたった、時点で帰ってきてしまう
		//→１つのコライダーとの接触でよいのであれば、問題は無いが、→総当たりに行うとは、そうゆうことではない気がする
		//→★衝突のコライダーに接触しているときに、片方の衝突はなかったことになるので、注意

	//自身の所有するコライダー数分回す
	for (int i = 0; i < collDatas_.size(); i++)
	{
		//RootJobから総当たりで
		//自身（Collisionを呼んでいるオブジェクト）と他コライダーとで、衝突しているかをもらう（RootJobから見れば、全オブジェクトと総当たりに判定できる）

		//親のポジションなども考慮した
		//ポジションを設定
		/*transform_.Calclation();
		XMVECTOR myPos = XMVector3TransformCoord(transform_.position_, transform_.GetWorldMatrix());
		*/

		//コライダーに自身のオブジェクトの位置を送り保存してもらう
			//→コライダーの位置を、オブジェクトの位置＋コライダーの離れ具合でとるので、
		collDatas_[i]->SetObjectPos(transform_.position_);


		//RootJobから、全オブジェクトのコライダーとと自身のコライダーとの衝突判定
			//RootJobのCollisionChildObjectを呼ぶ
		//衝突していた場合、衝突した相手のGameObject*がもらえる
		GameObject* pCollTarget = GetRootJob()->CollisionChildObject(collDatas_[i]);


		//nullptrでないなら
		if (pCollTarget != nullptr)
		{
			//衝突しているので、
			//衝突の相手を送って、衝突時の処理を行う
			OnCollision(pCollTarget);

		}
		//nullptrなら、衝突していないので何もしない。
	}





	//返り値で衝突していると判定が来たら
	//自身の衝突されたときに呼ぶ関数を呼びこむ（引数に当たった先のコライダーを渡して）

}
void GameObject::DrawCollider()
{
	//自身の所有するコライダー数分回す
	for (int i = 0; i < collDatas_.size(); i++)
	{
		//コライダーのワイヤーフレーム描画
			//描画は、Updateのタイミングで行うと、
			//BeginDrawにて、クリアされてしまうので、
			//DrawSubの中で、自身の、コライダーを描画する処理を行う
		collDatas_[i]->Draw();
	}
}

//再帰を行って、自身の子供に引数のコライダーと当たっているオブジェクト(オブジェクトの持つコライダーからすべて調べる)を探す
GameObject* GameObject::CollisionChildObject(Collider* collider)
{
	//自身の子供の数分回して
	//衝突の計算を行って、
		//衝突していたら、trueが返されるので
		//trueだったら、衝突したオブジェクトを返す
	for (auto itr = childList_.begin(); itr != childList_.end();
		itr++)
	{
		//"子供"の所有するコライダー分回す
			//示している子供の
		for (int i = 0; i < (*itr)->collDatas_.size(); i++)
		{

			//引数のコライダーと衝突判定を行う子供のコライダーが同じでない（ポインタなのでアドレスが同じ）
			// &&
			//引数にてもらったコライダー（当たり判定の元となる自身）と
			//イテレータで示している子供のコライダー（ターゲット）と衝突しているか
				//自身が当たり判定の関数を呼んで、引数にターゲットを入れる
					//→順番が逆でも、結局やっていることは変わらない。
					//→すべての自身以外のコライダーと当たり判定を行うようにする
			if (collider != (*itr)->collDatas_[i] &&
				collider->IsHit((*itr)->collDatas_[i])
				)
			{
				//衝突しているなら、子供のオブジェクトを返す
				return (*itr);
			
			}

		
		}
		//returnされずに、forを抜けた　＝　”子供のコライダーには”衝突するものはなかった
		//→子供のさらに子供のコライダーから探す
		//↓


		//”子供”の所有するコライダーと目的のコライダーと衝突をしなかった場合
		//（再帰）その子供の子供に同じ衝突判定を呼ぶ込む処理を行う（子供のCollChildObで、子供リストのコライダーと再帰的に判定）
		GameObject* pCollTarget =
			(*itr)->CollisionChildObject(collider);

		//戻り値がnullptrでないなら→衝突者が見つかった
		//→返す
		if (pCollTarget != nullptr)
		{
			//衝突したコライダーを持つオブジェクトを返す
			return pCollTarget;
		}
		//nullptrなら見つからなかった
		//→そうでないならfor回す
	}


	//引数のコライダーと、相手のコライダーが同じ場合
	//自分自身となってしまうので、
	//当然接触してしまう。
	//この場合は、接触判定を行わないように。

	//上記の判定では、自分の他のコライダーとも衝突判定が起きる




	return	nullptr;
}

void GameObject::PushBackChild(GameObject * pChild)
{
	//子供リストに追加
	childList_.push_back(pChild);


	{
		//新たに宣言したオブジェクト
		//の親のTransformを入れてあげる
		//Instantiateは、必ずオブジェクト宣言時に通るので、その時に、
		//自身のTransform内の親のTransformに親のTransformをセット。(参照渡しでアドレスをセット)
		pChild->transform_.pParent_ = &pChild->pParent_->transform_;		//Transformの関数内にて、親のTransformを考慮した行列の計算を行う→親のTransformが変更されたら、自身も変更する。
			//pParentは、GameObjectのこのクラスのparentになるので、
			//新規に作成したオブジェクトの親を指定したいときは、→p->pParent_としないといけない
	}

}

void GameObject::ChangeMyFbxShader(SHADER_TYPE type, bool childrenToo)
{
	//リスト内のモデルハンドル番号を使用して
	//Model.cppにアクセスし、
		//シェーダーを切り替える
	auto itr = changeShaderFbxList_.begin();
	for (itr; itr != changeShaderFbxList_.end(); itr++)
	{
		Model::ChangeShader((*itr), type);
	}


	if (childrenToo)
	{
		//子供も再帰的に切り替えさせるフラグが立っていたら
		//子供の同じ関数を呼ぶ（再帰）
		for (auto itr = childList_.begin();
			itr != childList_.end(); itr++)
		{
			//同様のType　かつ　同様のchildrenToo(再帰のため、)
			(*itr)->ChangeMyFbxShader(type, childrenToo);
		}
	}


}

void GameObject::AddChangeShaderFbx(int handle)
{
	auto itr = changeShaderFbxList_.begin();
	for (itr; itr != changeShaderFbxList_.end(); itr++)
	{
		if ((*itr) == handle)
		{
			//モデルのハンドル番号が、
				//別モデルで同じになることはあり得ないので、
				//この場合は、２回呼んでしまったということになる。
				//何もせずに帰る
			return;

		}
	}

	changeShaderFbxList_.push_back(handle);
}

void GameObject::SubChangeShaderFbx(int handle)
{
	auto itr = changeShaderFbxList_.begin();
	for (itr; itr != changeShaderFbxList_.end(); itr++)
	{
		if ((*itr) == handle)
		{
			//イテレータを使用して削除
			changeShaderFbxList_.erase(itr);
			return;

		}
	}

}
