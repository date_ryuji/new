#pragma once
#include <string>
//文字をテキストファイルに書き込むクラス


//文字の区切りタイプ
enum DELIMITER_CHAR
{
	DELIMITER_COMMA = 0,	//カンマ
	DELIMITER_NEW_LINE,		//改行

	MAX_DELIMITER,

};

class TextWriter
{
private : 
	//書き込み文字（保存用）
	std::string text_;

	//区切り文字タイプ
	DELIMITER_CHAR type_;
	//区切り文字
	char delimiter_;

private :
	//区切り文字の選定
		//引数タイプから、区切り文字の選定
	//引数：区切り文字タイプ
	//戻値：区切り文字
	char GetDelimiterChar(DELIMITER_CHAR type);

	//指定要素番目に文字列を追加
	//引数：追加文字
	//引数：文字列をついかする要素番目
	void AddString(std::string str, int suffix);

	//書き込み文字の合計文字数（合計バイト数）取得
	//戻値：文字数
	int GetStringLength();

	//文字列と、区切り文字を結合して返す
	//引数：結合文字
	//戻値：結合後の文字
	std::string GetCombineStrAndDelimiter(std::string str);

public : 

	//コンストラクタ
	TextWriter(DELIMITER_CHAR type);
	//デストラクタ
	~TextWriter();

	//末に文字追加
	//引数：追加文字列
	//引数：文字追加時　区切り文字を代入するか
	void PushBackString(std::string str , bool isDelimiter);

	//先頭に文字追加
	//引数：追加文字列
	//引数：文字追加時　区切り文字を代入するか
	void PushFrontString(std::string str, bool isDelimiter);

	//区切り文字の追加
	void PushBackDelimiter();

	//改行
	void PushBackNewLine();

	//文字書き込み終了
		//改行して終了させる
	void PushEnd();


	//書き込み実行
	//引数：書き込み先のファイルパス
	void FileWriteExcecute(std::string filePath);


};

