#include "SphereCollider.h"
#include "Global.h"

//コンストラクタによる初期化
SphereCollider::SphereCollider(XMVECTOR position, float size, bool drawWireFrame):
	Collider::Collider(position, SPHERE_COLLIDER , NO_FUNCTION, drawWireFrame),
	radius_(size)
{
	SetWireFrameSize();
}

SphereCollider::~SphereCollider()
{
}

bool SphereCollider::IsHit(Collider* target)
{
	bool hit = false;

	//相手のコライダータイプをもらう

	//そのタイプにより
	//衝突判定の計算式を持つ関数を呼び分ける
	switch (target->GetMyType())
	{
	case SPHERE_COLLIDER:
		//衝突判定
		//球と球
		//自身とターゲット
		hit = IsHitSphereToSphere(this, (SphereCollider*)target);

		//ターゲットは継承の親クラスのCollider方なので、
			//相手が球だとわかったら、
			//球型にキャストする

		break;

	case BOX_COLLIDER:
		//衝突判定
		//箱と球
		//ターゲットと箱
		hit = IsHitBoxToSphere((BoxCollider*)target, this);
		break;


	default:
		hit = false;
		break;
	}


	return hit;
}

void SphereCollider::SetWireFrameSize()
{
	//サイズからワイヤーフレームに使用するサイズを変更
		//親のColliderクラスにある関数から、
		//float型から、サイズを変更する関数を呼び出す
	Collider::SetWireFrameSize(radius_);
}
