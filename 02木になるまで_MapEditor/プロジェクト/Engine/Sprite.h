#pragma once
//Sprite = 画像

//★★画像であっても、２Dのポリゴンを表示させるだけ★★★★
//Quadの３D情報のクラスを、そのままコピーしてきて、
	//３Dにおける、２Dにいらない情報→法線など、（合成行列）ビュー行列などなど　を、削除していく


//Main.cppから、Spriteを呼べばいい
	//その時、位置などを、行列で送ってもらう（ワールドぎゅれつ）


#pragma once

#include <DirectXMath.h>
#include "Direct3D.h"
#include "Texture.h"
#include "Transform.h"	//Transformクラスの、移動、回転、拡大行列を使用するため
using namespace DirectX;


//#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}
//解放


struct ImageCollision
{
	////x
	////画像と衝突判定を行う際の衝突判定ｘ位置
	//int scrX;

	////y
	//int scrY;

	//画像と衝突判定を行う際の衝突判定位置
	XMVECTOR scrPointPos;


	//衝突したかの判定
	bool hit;

	//ワールドのTransformを取得
	Transform* trans;


	ImageCollision() :
		hit(false)
	{};


};



class Sprite
{
private : 
	//自身のシェーダー
	SHADER_TYPE thisShader_;

protected:
	//Quadでも、Spriteでも、こぴーしたので、　同じ構造体を使用している→なので、　→クラスの中に入れればいい
			//だが、継承先でも、Quadの構造体はしようしたいので、　継承先でも自由に使えるようにprotectedへ
	struct CONSTANT_BUFFER
	{
		//合成行列いらず	（カメラのどの位置などないので）

		XMMATRIX	matW;	//ワールド行列
	};
	//シェーダーにて、受け取るもののために、構造体で取得
		//現在は、一つしかもっていないが、

	//頂点情報
	//頂点に、位置と、UVがひつようなので、　構造体で取得

	struct VERTEX
	{
		XMVECTOR position;
		XMVECTOR uv;

		//法線いらず
	};


	//継承先で使用

	ID3D11Buffer *pVertexBuffer_;	//頂点バッファ
	ID3D11Buffer *pIndexBuffer_;	//インデックスバッファ
	ID3D11Buffer *pConstantBuffer_;	//コンスタントバッファ

		//ポインタのアスタリスクの場所
		//Class *class1;
		//Class* class2;		//どちらも同じ、だが、どっちかにするなら、書き方は共通した書き方をする
	Texture*	pTexture_;


	int vertex_;	//	頂点数
					//継承先ごとで、違う頂点数
	int *index;

	XMVECTOR GetPixelToWorldConversion(float x, float y);


public:

	Sprite();

	~Sprite();

	//継承先でオーバーライドする関数
	HRESULT Initialize(std::string fileName, SHADER_TYPE thisShader);//Spriteの初期化（モデル描画のための初期化）
	HRESULT Initialize(ID3D11Texture2D* pTexture, SHADER_TYPE thisShader);//Spriteの初期化（モデル描画のための初期化）
	virtual HRESULT InitVertex();//頂点情報の初期化

	virtual HRESULT TextureLoad(std::string fileName);
	HRESULT TextureLoad(ID3D11Texture2D* pTexture);

	//オーバーライドの必要はないため、Virtualなし
	//HRESULT Draw(XMMATRIX& worldMatrix);	//モデル描画
	HRESULT Draw(Transform transform);		//Transform型を引数としたDraw関数（Transformよりそれぞれ、行列を取得する）
	void Release();

	//シェーダーを切り替える
	//戻値：切り替え先のシェーダータイプ
	void ChangeShader(SHADER_TYPE shaderType);

	//現在のシェーダーを返す
	//戻値：シェーダータイプ
	SHADER_TYPE GetMyShader() { return thisShader_; };


	//画像と衝突判定
	//引数：衝突判定のためのデータ群
	void HitCollision(ImageCollision* imageCollision);

	//最高値、最低値　を超えない、下回らない値であることを確認し、
		//超えていた場合：最高値に切り詰める
		//下回った　場合：最低値に切り詰める
		//間の値の　場合：そのまま
			//を返す
	//引数：確認する値
	//引数：最高値
	//引数：最低値
	//戻値：結果値（↑参照（2行目〜4行目））
	float WithinRange(int value, int max, int min);

	//外積を求める
	//引数：座標１
	//引数：座標２
	//引数：座標３
	//戻値：外積の結果値
	float Sign(XMVECTOR point1, XMVECTOR point2, XMVECTOR point3);

	//三角形と点の内外判定
	//引数：点の座標
	//引数：三角形の頂点１
	//引数：三角形の頂点２
	//引数：三角形の頂点３
	bool PointInTriangle(XMVECTOR point, XMVECTOR v1, XMVECTOR v2, XMVECTOR v3);

	//自身のテクスチャの画像サイズを取得
	XMVECTOR GetMyTextureSize();

};


