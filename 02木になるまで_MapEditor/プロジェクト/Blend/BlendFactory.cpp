#include "BlendFactory.h"

//ブレンドの継承元クラス
#include "BaseBlend.h"
//ブレンドの継承先クラス
#include "AlphaBlendForAlphaExpression.h"
#include "SubBlendForGravityMagicExpression.h"


BaseBlend* BlendFactory::CreateBlendClass(BLEND_TYPE type)
{
	switch (type)
	{
	case BLEND_ALPHA_FOR_ALPHA:
		return new AlphaBlendForAlphaExpression;
	case BLEND_SUBTRACT_FOR_GRAVITY_MAGIC:
		return new SubBlendForGravityMagicExpression;

	default:
		return nullptr;

	}

	return nullptr;
}
