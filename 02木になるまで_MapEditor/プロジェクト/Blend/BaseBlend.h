#pragma once
#include <d3d11.h>	//Direct３Dの略
#include "../Engine/Direct3D.h"


class BaseBlend
{
protected:
	//ブレンド　をコンパイルするための構造体
		//シェーダーのように、必要な時にセットして切り替える
	struct BlendInfo
	{
		//ブレンドの基本情報
			//セットの段階では使用しない
			//初期化の段階で、Stateに情報を渡している
		//D3D11_BLEND_DESC* pBlendDesc;

		//透過
		ID3D11BlendState* pBlendState;
		//青の半透明の板があって、
		//赤いタマがあったら、　紫に見える。
			//つまり、色が混ざっているか、いないかで透明になっているように、見える。
		//透明なら、後ろの色を透過させた、　後ろの色と合わせた色にすれば、透過しているように見える

	}blendInfo_;
	//構造体情報の初期化関数
	void InitBlendInfo();

	//ブレンド状態を登録
	//引数：ブレンド状態の設定
	HRESULT CreateBlendState(D3D11_BLEND_DESC* blendDesc);


public :
	//コンストラクタ
	BaseBlend();

	//デストラクタ
	virtual ~BaseBlend();

	//ブレンド（混ぜ具合）の初期化
	virtual HRESULT Initialize();

	//自身のブレンド（混ぜ具合）をセットする
	void SetBlend();




};

