#include "ShaderFactory.h"

//シェーダーの継承元クラス
#include "../Shader/BaseShader.h"
//シェーダーの継承先クラス
#include "../Shader/ChocolateFashionShader.h"
#include "../Shader/EnvironmentShader.h"
#include "../Shader/FogShader.h"
#include "../Shader/NormalShader.h"
#include "../Shader/PointLightPlusWorldLightShader.h"
#include "../Shader/PointLightShader.h"
#include "../Shader/Simple2DShader.h"
#include "../Shader/Simple3DShader.h"
#include "../Shader/TestShader.h"
#include "../Shader/ToonShader.h"
#include "../Shader/WaterShader.h"
#include "../Shader/WetBallShader.h"

#include "../Shader/SkyBoxShader.h"
#include "../Shader/Blurred2DShader.h"

#include "../Shader/KurosawaModeShader.h"
#include "../Shader/KurosawaMode2DShader.h"
#include "../Shader/ToonOutLineShader.h"

#include "../Shader/DebugWireFrameShader.h"

#include "../Shader/HeightMapShader.h"


BaseShader* ShaderFactory::CreateShaderClass(SHADER_TYPE type)
{
	switch (type)
	{
	case SHADER_3D:
		return new Simple3DShader;
	case SHADER_2D:
		return new Simple2DShader;
	case SHADER_TEST:
		return new TestShader;
	case SHADER_NORMAL:
		return new NormalShader;
	case SHADER_TOON:
		return new ToonShader;
	case SHADER_WATER:
		return new WaterShader;
	case SHADER_WETBALL:
		return new WetBallShader;
	case SHADER_ENVI:
		return new EnvironmentShader;
	case SHADER_CHOCO_FASHION:
		return new ChocolateFashionShader;
	case SHADER_FOG:
		return new FogShader;
	case SHADER_POINT_LIGHT:
		return new PointLightShader;
	case SHADER_POINT_LIGHT_PLUS_WORLD_LIGHT:
		return new PointLightPlusWorldLightShader;
	case SHADER_SKY_BOX :
		return new SkyBoxShader;
	case SHADER_BLURRED_2D:
		return new Blurred2DShader;
	case SHADER_OUTLINE:
		return new ToonOutLineShader;
	case SHADER_KUROSAWA_MODE:
		return new KurosawaModeShader;
	case SHADER_KUROSAWA_MODE_2D:
		return new KurosawaMode2DShader;
	case SHADER_DEBUG_WIRE_FRAME:
		return new DebugWireFrameShader;
	case SHADER_HEIGHT_MAP:
		return new HeightMapShader;



	default : 
		return nullptr;
	
	}

	return nullptr;
}
