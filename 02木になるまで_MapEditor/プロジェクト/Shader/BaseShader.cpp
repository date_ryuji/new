#include <d3dcompiler.h>
//シェーダーコンパイルのための、関数群があるヘッダは
//ライブラリの下にて定義しなければ、
//未定義となってしまう
//そして、　ヘッダの下であっても、　ヘッダ内にインクルードしただけでは、未定義のままになる（正しくは、型指定がされていない。となる（よく、ｃｐｐの方にクラスをインクルードして、そのクラス型をヘッダなどで使うと起こる事例））
	//そのため、cppにインクルードを書く


#include "BaseShader.h"
#include "../Engine/Global.h"
#include "../Engine/Texture.h"	//CubeMapのテクスチャからシェーダーリソースビューを取得する関数を呼ぶために必要。
	//BaseShaderにて、テクスチャの関数を呼び込めるようにするために、インクルードするが、
	//継承先であっても、参照することができるのか、
	//BaseShaderの関数内で完結させるので、BaseShaderの関数を呼んでいるので、BaseShaderのｃｐｐでは、インクルードされているので、参照は可能。であるならば、継承先であっても、呼び込むのはBaseShaderの関数なので、参照は可能と考える


//コンストラクタにて、構造体の初期化
BaseShader::BaseShader()
{
	InitShaderInfo();
}

BaseShader::~BaseShader()
{
	SAFE_RELEASE(shaderInfo_.pRasterizerState);
	SAFE_RELEASE(shaderInfo_.pVertexLayout);
	SAFE_RELEASE(shaderInfo_.pPixelShader);
	SAFE_RELEASE(shaderInfo_.pVertexShader);
}

//継承先にオーバーライドする
HRESULT BaseShader::Initialize()
{
	//シェーダーファイル名
	//ワイド文字（全文字　2バイト）
	const wchar_t shaderFileName[] = L"Assets/Shader/NormalMap.hlsl";
	
	//頂点シェーダーの作成（コンパイル）
	{
		//頂点シェーダーへのコンパイルデータへのポインタ
		ID3DBlob *pCompileVS = nullptr;	//コンパイルデータへのポインタ


		//頂点シェーダーのために、シェーダーファイルをコンパイル
			//この時、ポインタである、pCompleVSは、nullptrなので、　何も入っていないからのポインタを渡している
			//つまり、からのポインタをコピーして渡していると考える。

			//となると、関数先で、仮に、ポインタへアドレスが入っても、
				//それは、コピー先のポインタに、アドレスが入っているだけ、　引数にて渡したもの自体に入るものではない。
			//つまり、引数にて渡したポインタに対して、コピーではなくて、ポインタ自体を参照したい。
			//＝参照渡しにてポインタを渡してやって、　ポインタを、外部からも直接メモリへ参照できるようにする。（D3DCompileFromFile()に渡している引数のポインタに＆をつけている理由は、同様の理由。　ポインタそのままを渡しても、ポインタがコピーされるだけで、元の「ポインタの」アドレスを指していないので、ポインタのアドレス部が変わっても、関数呼び出し元のポインタには影響を及ぼさなくなる）
		if (FAILED(VSCompileFromShaderFile(&pCompileVS, shaderFileName)))
		{
			MessageBox(nullptr, "頂点シェーダーへのシェーダーファイルコンパイル失敗", "エラー", MB_OK); return E_FAIL;
		};
		//頂点シェーダーの作成
		if (FAILED(CreateVertexShader(pCompileVS)))
		{
			MessageBox(nullptr, "頂点シェーダーへの作成失敗", "エラー", MB_OK); return E_FAIL;
		};

		//★★頂点インプットレイアウト(3D用（光のための法線）)★★
			//→頂点シェーダーに渡す情報群をここで設定
		//頂点の情報
		//1頂点に、位置、法線、色（etc...）　の情報を持たせる
		//Release前に、
		D3D11_INPUT_ELEMENT_DESC layout[] = {

			//TestShader.hlslファイルの
			//頂点
			//第５引数：メモリに書き込む位置（１つ目は０）
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置

			//UV
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標

			//法線
			//第５引数：メモリに書き込む位置（３つ目はXMVECTORの構造体サイズの２つ分先から書き始める）
			{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線

			//接線
			{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 3 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//接線


			//ノーマルマップを実装するために、
				//接線の追加
					//FBXから情報をそれぞれ渡す
			//従法線も取得するのであれば、　この下に一行追加
				//今回は接線をもとめたら、
					//法線と接線とで従法線をもらうようにする


		};

		//頂点インプットレイアウト情報の確定
		//頂点インプットの情報群をもとに、
		//頂点インプットに渡す情報の確定
		//第３引数：頂点インプットレイアウトにおける引数の個数
		if (FAILED(CreateInputLayout(pCompileVS, layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)))))
		{
			MessageBox(nullptr, "頂点インプットレイアウトの情報確定失敗", "エラー", MB_OK); return E_FAIL;
		};


		//コンパイラ用のデータを解放
		SAFE_RELEASE(pCompileVS);
		
	}

	// ピクセルシェーダの作成（コンパイル）
	{
		//ピクセルシェーダーへのコンパイルデータへのポインタ
		ID3DBlob *pCompilePS = nullptr;

		//ピクセルシェーダーのために、シェーダーファイルをコンパイル
		if (FAILED(PSCompileFromShaderFile(&pCompilePS, shaderFileName)))
		{
			MessageBox(nullptr, "ピクセルシェーダーへのシェーダーファイルコンパイル失敗", "エラー", MB_OK); return E_FAIL;
		};

		//ピクセルシェーダーの作成
		if (FAILED(CreatePixelShader(pCompilePS)))
		{
			MessageBox(nullptr, "ピクセルシェーダーの作成失敗", "エラー", MB_OK); return E_FAIL;
		}

		//コンパイラ用のデータを解放
		SAFE_RELEASE(pCompilePS);


		//ラスタライザ作成
			//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
			//→そのピクセルごと、ピクセル化みたいなもの
		D3D11_RASTERIZER_DESC rdc = {};

		rdc.CullMode = D3D11_CULL_BACK;
		//カリングモード　の略
		//★裏のポリゴンを表示しますか、表示しませんか
			//FRONT:　表いらない
			//BACK :　裏いらない
			//NONE :　いらないもの　なし（両面表示）

		rdc.FillMode = D3D11_FILL_SOLID;
		//★中身を塗りつぶすか
			//SOLID		：中身を塗りつぶす
			//WIER_FRAME：ワイヤーフレーム		//枠だけなので、三角形にて表現しているので、三角形のつながりとなる
												//当たり判定表示、取り合えず表示されているのか知りたいときなど

		rdc.FrontCounterClockwise = FALSE;
		//カウンター　反撃
		//時計回りを表とするか、反時計回りを表とするのか（FBXのインデックス情報に用いる）
			//TRUE  : 反時計回りを　表　とします
			//FALSE : 時計回りを　　表　とします
				/*
					FBX作成時に、面を三角化して、
					△ポリゴンだけで面を表現できる、その面を描画する際に、時計回りで頂点を読み込むのかなどを決める
				
				*/
		
		//ラスタライザ情報の確定
			//作成した情報を渡し、シェーダーに対するラスタライザの確定
		if (FAILED(CreateRasterizerState(&rdc)))
		{
			MessageBox(nullptr, "ラスタライザの情報確定失敗", "エラー", MB_OK); return E_FAIL;
		};


	}


	return S_OK;
}

void BaseShader::InitShaderInfo()
{
	shaderInfo_.pVertexShader = nullptr;	//頂点シェーダー
	shaderInfo_.pPixelShader = nullptr;		//ピクセルシェーダー
	shaderInfo_.pVertexLayout = nullptr;	//頂点インプットレイアウト	
	shaderInfo_.pRasterizerState = nullptr;	//ラスタライザー

}

HRESULT BaseShader::VSCompileFromShaderFile(ID3DBlob** pCompileVS, const wchar_t * shaderFileName)
{
	//頂点シェーダーの関数を指定し、シェーダーファイルをコンパイル
	return CompileFromShaderFile(pCompileVS, shaderFileName, "VS", FUNCTION_VERTEX);

}

HRESULT BaseShader::PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t * shaderFileName)
{

	//ピクセルシェーダーの関数を指定し、シェーダーファイルをコンパイル
	return CompileFromShaderFile(pCompilePS, shaderFileName, "PS", FUNCTION_PIXEL);


}
//
//HRESULT BaseShader::VSCompileFromShaderFile(ID3DBlob ** pCompileVS, const wchar_t * shaderFileName, const std::string functionName)
//{
//	//シェーダーファイルからコンパイルを行う
//		//引数にて、ポインタのアドレスをもらっているので、
//		//関数呼び出し元のポインタ（アドレスを格納している部分のアドレス（ポインタ自体のアドレス））を直接書き換えることになる。
//	//if (FAILED(D3DCompileFromFile(shaderFileName, nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))	//ここでpCompleVSが、普通のポインタであるならば、nullptrが入っているアドレスを渡してあげる必要があった、だが、今回はポインタのポインタとシテ、nullptrの入るポインタのアドレスを受け取っているので、ポインタのポインタを引数として渡す
//	if (FAILED(D3DCompileFromFile(shaderFileName, nullptr, nullptr, functionName.c_str(), "vs_5_0", NULL, 0, pCompileVS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dシェーダファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
//	//ファイルをコンパイルする　ファイル名				名前	グラボのバージョン		コンパイルするポインタ
//	//グラボのバージョンは、特に難しいことをしないのであれば、小さい値でOK
//	//L"Simple~"	Lは？
//	//→メモにて
//	//
//	assert(pCompileVS != nullptr);
//
//	return S_OK;
//}
//
//HRESULT BaseShader::PSCompileFromShaderFile(ID3DBlob ** pCompilePS, const wchar_t * shaderFileName, const std::string functionName)
//{
//	//シェーダーファイルからコンパイルを行う
//		//引数にて、ポインタのアドレスをもらっているので、
//		//関数呼び出し元のポインタ（アドレスを格納している部分のアドレス（ポインタ自体のアドレス））を直接書き換えることになる。
//	//if (FAILED(D3DCompileFromFile(shaderFileName, nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))	//ここでpCompleVSが、普通のポインタであるならば、nullptrが入っているアドレスを渡してあげる必要があった、だが、今回はポインタのポインタとシテ、nullptrの入るポインタのアドレスを受け取っているので、ポインタのポインタを引数として渡す
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
//	if (FAILED(D3DCompileFromFile(shaderFileName, nullptr, nullptr, functionName.c_str(), "ps_5_0", NULL, 0, pCompilePS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "シェーダーファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	assert(pCompilePS != nullptr);
//
//	return S_OK;
//}

HRESULT BaseShader::CompileFromShaderFile(ID3DBlob** pCompile, 
	const wchar_t * shaderFileName, const std::string functionName, FUNCTION_TYPE type)
{
	//シェーダーファイルからコンパイルを行う
		//引数にて、ポインタのアドレスをもらっているので、
		//関数呼び出し元のポインタ（アドレスを格納している部分のアドレス（ポインタ自体のアドレス））を直接書き換えることになる。
	//if (FAILED(D3DCompileFromFile(shaderFileName, nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))	//ここでpCompleVSが、普通のポインタであるならば、nullptrが入っているアドレスを渡してあげる必要があった、だが、今回はポインタのポインタとシテ、nullptrの入るポインタのアドレスを受け取っているので、ポインタのポインタを引数として渡す
	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
	if (FAILED(D3DCompileFromFile(shaderFileName, nullptr, nullptr, functionName.c_str(), GetGraphicVersion(type).c_str(), NULL, 0, pCompile, NULL)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, GetErrorMessage(type).c_str(), "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
	}
	assert(pCompile != nullptr);

	return S_OK;
}

HRESULT BaseShader::CreateVertexShader(ID3DBlob * pCompileVS)
{
	//pVertexShaderを作成（頂点シェーダー）
	//シェーダーバンドルのSHADER_TESTに入れてね
	if (FAILED(Direct3D::pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL,
		&shaderInfo_.pVertexShader)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "３Dコンパイルしたシェーダを代入失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ

	}
	//pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &pVertexShader);
	//コンパイルしたもの（pCompileVS）を入れる（クリエイト）
	return S_OK;

}

HRESULT BaseShader::CreateInputLayout(ID3DBlob * pCompileVS, D3D11_INPUT_ELEMENT_DESC* layout , size_t size)
{
	size_t base = sizeof(layout);
	size_t pattarn = sizeof(D3D11_INPUT_ELEMENT_DESC);
	//size_t size = (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC));

	//配列の先頭アドレスを仮に、ポインタとして、受け取ったとして、
		//そのポインタを、sizeofしても、ポインタのサイズしか出てこず、
		//配列のサイズは出てこない
	//その理由は、	
		//単一のポインタとして受け取っているので、
		//その単一のポインタのサイズしか出てこない
	//なので、引数で、単一のポインタではなく、　配列のポインタとして受け取る、つまり、array[]という形で、配列のポインタとして先頭アドレスを受け取る
		//→×
	//サイズを、引数にて送ってもらって、そのサイズをもとに、関数の呼び出しを行うようにする





	if (FAILED(Direct3D::pDevice->CreateInputLayout(layout, size, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(),
		&shaderInfo_.pVertexLayout)))
		//頂点インプットレイアウトの情報を２つにしたので、　上記の数も２にしなければエラーになる
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "頂点インプットレイアウトの情報を確定失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ

	}

	return S_OK;
}

HRESULT BaseShader::CreatePixelShader(ID3DBlob* pCompilePS)
{
	if (FAILED(Direct3D::pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL,
		&shaderInfo_.pPixelShader)))
	{
		//エラー処理
			//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "ピクセルシェーダーの作成失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ

	};
	return S_OK;

}

HRESULT BaseShader::CreateRasterizerState(D3D11_RASTERIZER_DESC * rdc)
{
	if (FAILED(Direct3D::pDevice->CreateRasterizerState(rdc, &shaderInfo_.pRasterizerState)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "ラスタライザの作成失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
	}
	//pDevice->CreateRasterizerState(&rdc, &pRasterizerState);

	return S_OK;

}



void BaseShader::SetShader()
{
	Direct3D::pContext->VSSetShader(shaderInfo_.pVertexShader, NULL, 0);	//頂点シェーダー

	Direct3D::pContext->PSSetShader(shaderInfo_.pPixelShader, NULL, 0);	//ピクセルシェーダー

	Direct3D::pContext->IASetInputLayout(shaderInfo_.pVertexLayout);	//頂点インプットレイアウト

	Direct3D::pContext->RSSetState(shaderInfo_.pRasterizerState);		//ラスタライザー
}

void BaseShader::ShaderConstantBuffer1View()
{
}

////BaseShader内の構造体なので、　BaseShaderのスコープ
////ShaderInfoなので、		　　ShaderInfoのスコープ
////継承先でコンストラクタにアクセスすることができない可能性もある
//BaseShader::ShaderInfo::ShaderInfo()
//{
//}

HRESULT BaseShader::CreateConstantBuffer1(ID3D11Buffer ** ppConstantBuffer1, UINT size)
{
	//コンスタントバッファ作成
//バッファーの確保しておくサイズは、16の倍数でないといけない。
	D3D11_BUFFER_DESC cb;
	//cb.ByteWidth = sizeof(CONSTANT_BUFFER_1);	//コンスタントバッファは１６の倍数でないといけないらしい
												//なので、構造体のサイズが１６の倍数でないと、エラーになる
													//float1つの型であると、4の倍数で、１６の倍数にはならないので、　ここであえて、１６の値を入れると、エラーにはならない
			//�@なので、コンスタントバッファの構造体のサイズを１６の倍数にする。
			//�Aあるいは、ここで、偽のサイズとして、　16の倍数の値を入れることでエラーを回避することが可能である。

	//�A
	cb.ByteWidth = size;	//float １つ　：４バイト
						//16バイトを超えることはないので、一番近い16の倍数として１６の値を、バッファーの格納領域、？みたいなところに渡す。



	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//コンスタントバッファを作成する
	//Deviceに引数としてバッファを作成してもらう
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, ppConstantBuffer1)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	}

	return S_OK;
}

void BaseShader::ShaderConstantBuffer1View(ID3D11Buffer ** ppConstantBuffer1, void * cb1, UINT size)
{
	//コンスタントバッファに情報を与える
//この時、
//自身がLoad時に設定しているシェーダーファイルに、コンスタントバッファ２つめ（セットの第一引数の番号に該当する）があることが前提


//コンスタントバッファ（登録（メンバ内のコンスタントバッファを））
	//第一引数にて、
	//シェーダーファイルにて、作成した番号を入れて、
		//1番目のコンスタントバッファの値を代入すると指定する

//ピクセルでのみ、
	//今回のコンスタントバッファの値を使うが
	//一応、頂点シェーダーでも使うことになった時のため、　または、例外を起こさないために、両方へ渡しておく
	Direct3D::pContext->VSSetConstantBuffers(1, 1, ppConstantBuffer1);	//頂点シェーダー用	
	Direct3D::pContext->PSSetConstantBuffers(1, 1, ppConstantBuffer1);	//ピクセルシェーダー用




	//コンスタントバッファの受け渡し。
	D3D11_MAPPED_SUBRESOURCE pdata;

	//★コンスタントバッファに変更を加えたので、この時点での変更を保存するために開く(更新を送ってあげる)
		//★→コンスタントバッファで開いて、メモリに保存、書き込みを行う
		//コンスタントバッファを開く（マップ）
	if (FAILED(Direct3D::pContext->Map((*ppConstantBuffer1), 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "コンスタントバッファのマップの書き込み失敗", "エラー", MB_YESNO);


		assert(0);

		//return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
	}

	//int size = sizeof(cb);


	//Mapとして、書き込むために呼び込む（変更をした、コンスタントバッファを書き込む）
	//メモリーをコピー
	memcpy_s(pdata.pData, pdata.RowPitch, cb1, size);	// データを値を送る
					//配列にあるデータなどを、他の領域にまるっとコピー
					//memcpy_s (コピー先、コピー先のサイズ、コピー元　、コピー元のサイズ)
					//コンスタントバッファに先ほど作ったものを書き込む
						//コピー元の先頭アドレスを入れたい、void 型のアドレス→アドレスであって、特にどの型でもないので、voidの、アドレスの型にキャストしている
						//構造体で作った、cbのアドレスを、→入れる



	//最後にコンスタントバッファを閉じる
	//編集を行った、コンスタントバッファを閉じる（Unマップ）
	Direct3D::pContext->Unmap((*ppConstantBuffer1), 0); 	//再開
						//書き込んだので、最後に、Unmapにて、　閉じる
						//コンスタントバッファに送る


}

void BaseShader::SetCubeMapTexture(UINT suffix)
{
	//テクスチャの橋渡しを送る
	ID3D11ShaderResourceView* pSRV = Direct3D::pCubeTex->GetSRV();
	//シェーダーに作った変数に渡している
		//hlsl : 例　：　g_normalTexture(t1)
		//第一引数に、(tn) のnを代入
	Direct3D::pContext->PSSetShaderResources(suffix, 1, &pSRV);
}

const std::string BaseShader::GetGraphicVersion(FUNCTION_TYPE type)
{
	switch (type)
	{
	case FUNCTION_VERTEX:
		return "vs_5_0";
	case FUNCTION_PIXEL:
		return "ps_5_0";
	default : 
		return " ";
	}
	return " ";
}

const std::string BaseShader::GetErrorMessage(FUNCTION_TYPE type)
{
	switch (type)
	{
	case FUNCTION_VERTEX:
		return "頂点シェーダー関数をもとに、シェーダーファイルのコンパイル失敗";
	case FUNCTION_PIXEL:
		return "ピクセルシェーダー関数をもとに、シェーダーファイルのコンパイル失敗";
	default:
		return "シェーダーファイルのコンパイル失敗";
	}
	return " ";
}


