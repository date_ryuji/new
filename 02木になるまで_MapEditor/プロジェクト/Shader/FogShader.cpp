
#include "FogShader.h"
#include "../Engine/Direct3D.h"	//コンテキストへシェーダーの情報を渡すためにインクルード


FogShader::FogShader() :
	BaseShader::BaseShader()	//継承元の親のコンストラクタを呼び出す
{
}

FogShader::~FogShader()
{
	BaseShader::~BaseShader();
	SAFE_RELEASE(pConstantBuffer_);
}

HRESULT FogShader::Initialize()
{
	//シェーダーファイル名
	//ワイド文字（全文字　2バイト）
	const wchar_t shaderFileName[] = L"Assets/Shader/Fog.hlsl";



	//頂点シェーダーの作成（コンパイル）
	{
		//頂点シェーダーへのコンパイルデータへのポインタ
		ID3DBlob *pCompileVS = nullptr;	//コンパイルデータへのポインタ


			//頂点シェーダーのために、シェーダーファイルをコンパイル
		if (FAILED(VSCompileFromShaderFile(&pCompileVS, shaderFileName)))
		{
			MessageBox(nullptr, "頂点シェーダーへのシェーダーファイルコンパイル失敗", "エラー", MB_OK); return E_FAIL;
		};
		//頂点シェーダーの作成
		if (FAILED(CreateVertexShader(pCompileVS)))
		{
			MessageBox(nullptr, "頂点シェーダーへの作成失敗", "エラー", MB_OK); return E_FAIL;
		};

		//★★頂点インプットレイアウト(3D用（光のための法線）)★★
			//→頂点シェーダーに渡す情報群をここで設定
		//頂点の情報
		//1頂点に、位置、法線、色（etc...）　の情報を持たせる
		//Release前に、
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			//FogShader.hlslファイルの
		//頂点
		//第５引数：メモリに書き込む位置（１つ目は０）
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置

		//UV
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標

		//法線
		//第５引数：メモリに書き込む位置（３つ目はXMVECTORの構造体サイズの２つ分先から書き始める）
		{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線

		//接線
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 3 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//接線


		//ノーマルマップを実装するために、
			//接線の追加
				//FBXから情報をそれぞれ渡す
		//従法線も取得するのであれば、　この下に一行追加
			//今回は接線をもとめたら、
				//法線と接線とで従法線をもらうようにする
		};

		//頂点インプットレイアウト情報の確定
		//頂点インプットの情報群をもとに、
		//頂点インプットに渡す情報の確定
		//第３引数：頂点インプットレイアウトにおける引数の個数
		if (FAILED(CreateInputLayout(pCompileVS, layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)))))
		{
			MessageBox(nullptr, "頂点インプットレイアウトの情報確定失敗", "エラー", MB_OK); return E_FAIL;
		};


		//コンパイラ用のデータを解放
		SAFE_RELEASE(pCompileVS);

	}

	// ピクセルシェーダの作成（コンパイル）
	{
		//ピクセルシェーダーへのコンパイルデータへのポインタ
		ID3DBlob *pCompilePS = nullptr;

		//ピクセルシェーダーのために、シェーダーファイルをコンパイル
		if (FAILED(PSCompileFromShaderFile(&pCompilePS, shaderFileName)))
		{
			MessageBox(nullptr, "ピクセルシェーダーへのシェーダーファイルコンパイル失敗", "エラー", MB_OK); return E_FAIL;
		};
		//ピクセルシェーダーの作成
		if (FAILED(CreatePixelShader(pCompilePS)))
		{
			MessageBox(nullptr, "ピクセルシェーダーの作成失敗", "エラー", MB_OK); return E_FAIL;
		}
		//コンパイラ用のデータを解放
		SAFE_RELEASE(pCompilePS);



		//ラスタライザ作成
			//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
			//→そのピクセルごと、ピクセル化みたいなもの
		D3D11_RASTERIZER_DESC rdc = {};

		rdc.CullMode = D3D11_CULL_BACK;
		//カリングモード　の略
		//★裏のポリゴンを表示しますか、表示しませんか
			//FRONT:　表いらない
			//BACK :　裏いらない
			//NONE :　いらないもの　なし（両面表示）

		rdc.FillMode = D3D11_FILL_SOLID;
		//★中身を塗りつぶすか
			//SOLID		：中身を塗りつぶす
			//WIER_FRAME：ワイヤーフレーム		//枠だけなので、三角形にて表現しているので、三角形のつながりとなる
												//当たり判定表示、取り合えず表示されているのか知りたいときなど

		rdc.FrontCounterClockwise = FALSE;
		//カウンター　反撃
		//時計回りを表とするか、反時計回りを表とするのか（FBXのインデックス情報に用いる）
			//TRUE  : 反時計回りを　表　とします
			//FALSE : 時計回りを　　表　とします
				/*
					FBX作成時に、面を三角化して、
					△ポリゴンだけで面を表現できる、その面を描画する際に、時計回りで頂点を読み込むのかなどを決める

				*/

				//ラスタライザ情報の確定
					//作成した情報を渡し、シェーダーに対するラスタライザの確定
		if (FAILED(CreateRasterizerState(&rdc)))
		{
			MessageBox(nullptr, "ラスタライザの情報確定失敗", "エラー", MB_OK); return E_FAIL;
		};


	}

	//シェーダーのコンスタントバッファ2つ目に渡すための、コンスタントバッファを初期化
	if (FAILED(BaseShader::CreateConstantBuffer1(&pConstantBuffer_, 16)))
	{
		MessageBox(nullptr, "コンスタントバッファ１を送るためのポインタの作成失敗", "エラー", MB_OK); return E_FAIL;
	};

	return S_OK;
}


HRESULT FogShader::VSCompileFromShaderFile(ID3DBlob** pCompileVS, const wchar_t * shaderFileName)
{
	return BaseShader::VSCompileFromShaderFile(pCompileVS, shaderFileName);
}

HRESULT FogShader::PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t * shaderFileName)
{
	return BaseShader::PSCompileFromShaderFile(pCompilePS, shaderFileName);
}



//HRESULT FogShader::CreateConstantBuffer1()
//{
//	//コンスタントバッファ作成
//	//バッファーの確保しておくサイズは、16の倍数でないといけない。
//	D3D11_BUFFER_DESC cb;
//	//cb.ByteWidth = sizeof(CONSTANT_BUFFER_1);	//コンスタントバッファは１６の倍数でないといけないらしい
//												//なので、構造体のサイズが１６の倍数でないと、エラーになる
//													//float1つの型であると、4の倍数で、１６の倍数にはならないので、　ここであえて、１６の値を入れると、エラーにはならない
//			//�@なので、コンスタントバッファの構造体のサイズを１６の倍数にする。
//			//�Aあるいは、ここで、偽のサイズとして、　16の倍数の値を入れることでエラーを回避することが可能である。
//
//	//�A
//	cb.ByteWidth = 16;	//float １つ　：４バイト
//						//16バイトを超えることはないので、一番近い16の倍数として１６の値を、バッファーの格納領域、？みたいなところに渡す。
//
//
//
//	cb.Usage = D3D11_USAGE_DYNAMIC;
//	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//	cb.MiscFlags = 0;
//	cb.StructureByteStride = 0;
//
//	//コンスタントバッファを作成する
//	//Deviceに引数としてバッファを作成してもらう
//	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
//	{
//		//エラーを、どこでしたのかを知らせるためのメッセージボックス
//		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_YESNO);
//		return E_FAIL;
//	}
//
//	return S_OK;
//}


void FogShader::SetConstantBuffer1Data(CONSTANT_BUFFER_1 * pConstantBuffer1)
{
	ConstantBuffer1_.backGroundColor = pConstantBuffer1->backGroundColor;
}

void FogShader::ShaderConstantBuffer1View()
{
	//シェーダーのコンスタントバッファに渡すための処理関数を呼び出す
	BaseShader::ShaderConstantBuffer1View(
		&pConstantBuffer_,
		(void*)(&ConstantBuffer1_),
		sizeof(ConstantBuffer1_));
	
	//CubeMapのテクスチャを渡す
	//第１引数の値をわたす
	SetCubeMapTexture(2);
	/*
	//シェーダーに作った変数に渡している
		//hlsl : 例　：　g_normalTexture(t1)
		//第一引数に、(tn) のnを代入
	Direct3D::pContext->PSSetShaderResources(2, 1, &pSRV);
	*/


	////コンスタントバッファに情報を与える
	////この時、
	////自身がLoad時に設定しているシェーダーファイルに、コンスタントバッファ２つめ（セットの第一引数の番号に該当する）があることが前提


	////コンスタントバッファ（登録（メンバ内のコンスタントバッファを））
	//	//第一引数にて、
	//	//シェーダーファイルにて、作成した番号を入れて、
	//		//1番目のコンスタントバッファの値を代入すると指定する

	////ピクセルでのみ、
	//	//今回のコンスタントバッファの値を使うが
	//	//一応、頂点シェーダーでも使うことになった時のため、　または、例外を起こさないために、両方へ渡しておく
	//Direct3D::pContext->VSSetConstantBuffers(1, 1, &pConstantBuffer_);	//頂点シェーダー用	
	//Direct3D::pContext->PSSetConstantBuffers(1, 1, &pConstantBuffer_);	//ピクセルシェーダー用




	////コンスタントバッファの受け渡し。
	//D3D11_MAPPED_SUBRESOURCE pdata;

	////★コンスタントバッファに変更を加えたので、この時点での変更を保存するために開く(更新を送ってあげる)
	//	//★→コンスタントバッファで開いて、メモリに保存、書き込みを行う
	//	//コンスタントバッファを開く（マップ）
	//if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))
	//{
	//	//エラーを、どこでしたのかを知らせるためのメッセージボックス
	//	MessageBox(nullptr, "コンスタントバッファのマップの書き込み失敗", "エラー", MB_YESNO);


	//	assert(0);

	//	//return E_FAIL;	//エラーですと返す。
	//					//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

	//					//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
	//}

	////int size = sizeof(cb);


	////Mapとして、書き込むために呼び込む（変更をした、コンスタントバッファを書き込む）
	////メモリーをコピー
	//memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&ConstantBuffer1_), sizeof(ConstantBuffer1_));	// データを値を送る
	//				//配列にあるデータなどを、他の領域にまるっとコピー
	//				//memcpy_s (コピー先、コピー先のサイズ、コピー元　、コピー元のサイズ)
	//				//コンスタントバッファに先ほど作ったものを書き込む
	//					//コピー元の先頭アドレスを入れたい、void 型のアドレス→アドレスであって、特にどの型でもないので、voidの、アドレスの型にキャストしている
	//					//構造体で作った、cbのアドレスを、→入れる



	////最後にコンスタントバッファを閉じる
	////編集を行った、コンスタントバッファを閉じる（Unマップ）
	//Direct3D::pContext->Unmap(pConstantBuffer_, 0); 	//再開
	//					//書き込んだので、最後に、Unmapにて、　閉じる
	//					//コンスタントバッファに送る


}

FogShader::CONSTANT_BUFFER_1::CONSTANT_BUFFER_1() :
	backGroundColor(XMVectorSet(0.f , 0.f, 0.f, 0.f))
{

}

