#pragma once
#include "../Engine/GameObject.h"


class SphereCollider;
class BoxCollider;


//■■シーンを管理するクラス
class Bullet : public GameObject
{
	int hModel_;

	SphereCollider* pSpColl_;
	BoxCollider* pBoxColl_;

	//前回の座標を保存しておく
	//壁に衝突したときに、移動分を保存しておく必要がある
		//前回の座標に戻すことで、
		//壁ずり方向に移動を滑らかに行う
	XMVECTOR beforePos_;


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Bullet(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
	
	//衝突時実行関数
	void OnCollision(GameObject* pTarget) override;
};