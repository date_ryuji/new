#include "NormalCube.h"
#include "../Engine/Model.h"


//コンストラクタ
NormalCube::NormalCube(GameObject * parent)
	: GameObject(parent, "NormalCube")
{
}

//初期化
void NormalCube::Initialize()
{
	hModel_ = Model::Load("Assets/CubeMap/Boll1.fbx" , FBX_POLYGON_GROUP, SHADER_NORMAL);
	assert(hModel_ != 0);

	transform_.position_.vecZ = 5.0f;

}

//更新
void NormalCube::Update()
{



}

//描画
void NormalCube::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void NormalCube::Release()
{
}