#include "MoveButton.h"


MoveButton::MoveButton(GameObject * parent) :
	Button(parent),
	SPEED_X_(2),
	SPEED_Y_(4),
	codeX_(1),
	codeY_(1),
	currentX_(0),
	currentY_(0)
{
}

MoveButton::~MoveButton()
{
}


/*
void Button::Initialize()
{
	//ボタンのロード
	Load("" , ON_BUTTON);
	Load("", OFF_BUTTON);
}
void Button::Update()
{
	//ボタンの接触判定
	UpdateButtonForOnContact();
}
void Button::Draw()
{
	//ボタン描画
	DrawButton();
}
void Button::Release()
{
	//ボタンクラスの動的確保した要素を解放
	ReleaseButton();
}
*/
void MoveButton::Initialize()
{
	

	//ボタンのロード
	Load("Assets/SampleScene/MoveButton_ON.png", ON_BUTTON);
	Load("Assets/SampleScene/MoveButton_OFF.png", OFF_BUTTON);

	//移動
	SetPixelPosition(currentX_, currentY_);


}

void MoveButton::Update()
{
	//ボタンの接触判定
	UpdateButtonForOnContact();
	int minX = 0 , minY = 0;
	int maxX = 500 , maxY = 300;

	//移動
	if (currentX_ < minX || currentX_ > maxX)
	{
		//反転
		codeX_ *= -1;
	}
	if (currentY_ < minY || currentY_ > maxY)
	{
		//反転
		codeY_ *= -1;
	}

	//移動値計算
	currentX_ += SPEED_X_ * codeX_;
	currentY_ += SPEED_Y_ * codeY_;

	//セット
	SetPixelPosition(currentX_, currentY_);



}

void MoveButton::Draw()
{
	//ボタン描画
	DrawButton();
}

void MoveButton::Release()
{
	//ボタンクラスの動的確保した要素を解放
	ReleaseButton();
}



void MoveButton::OnContactButton()
{
	//ボタンの押下
	//ButtonPress();

	//スイッチ式ON,OFF
	//押した瞬間に一回実行
	ButtonPressOneDown();

	//スイッチ式ON,OFF
	//離した瞬間に一回実行
	//ButtonPressOneUp();

}