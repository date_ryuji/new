#pragma once
#include "../Engine/GameObject.h"


//SceneManagerが、初めにロードする（SceneManagerが動作するためのはじめに読み込むシーン）
//何もしないシーン


//■■シーンを管理するクラス
class NormalCube : public GameObject
{
	int hModel_;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	NormalCube(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};