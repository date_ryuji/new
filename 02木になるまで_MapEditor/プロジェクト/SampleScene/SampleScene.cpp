#include "SampleScene.h"

//360度背景
#include "../Engine/SkyBox.h"
//テキスト
#include "../Engine/Texts.h"
//カメラ
#include "../Engine/Camera.h"
#include "../Engine/Image.h"
//当たり判定
#include "../Engine/SphereCollider.h"	//球コライダー
#include "../Engine/BoxCollider.h"		//箱コライダー


//子供オブジェクトクラス
#include "NormalCube.h"
//#include "Controller.h"

//ボタン
#include "StartButton.h"
#include "MoveButton.h"

//当たり判定実装オブジェクト
#include "Enemy.h"

#include "Player.h"


//ヘイトマップ
#include "Ground.h"

//
#include "../MapEditorScene/HeightMapObject.h"




//コンストラクタ
SampleScene::SampleScene(GameObject * parent)
	: GameObject(parent, "SampleScene")
{
}

//初期化
void SampleScene::Initialize()
{


	//透過の処理を行うとき、
	//出現の順番を考えなくてはいけない　→　正しくは、描画の順番
	//Instantiateする順番を考えないと、
	//透過がうまくいかない　→このうまくいかないというのは、　
	//仮に　Water　a = 0.5f
		//  Ground a = 1.0f の順番で描画した場合、
	//Waterの透過部分の下に、Groundのオブジェクトが仮にあったとしても、
		//Groundの色を込みした透過にはならず、		
		//背景の色を映してしまう。

	//そのため、　Waterの下に、Groundの色を載せたいのであれば、
		//Ground , Waterの順番で描画をしなくてはいけない。
		//Waterの段階で、　今ある描画の色に自身の色を重ねる（αを含んだ計算）をするため、　のちの描画するピクセルが、　その透過のピクセルよりも遠ければ、　Zバッファの関係で映らなくなる。
		//そうすれば、　Groundの色が反映されないのは当然

	//pController_ = (Controller*)Instantiate<Controller>(this);
	pPlayer_ = (Player*)Instantiate<Player>(this);
	//Instantiate<SkyBox>((GameObject*)pController_);
	Instantiate<SkyBox>((GameObject*)pPlayer_);
	Instantiate<NormalCube>(this);
	Instantiate<Enemy>(this);

	//Instantiate<Ground>(this);

	Instantiate<HeightMapObject>(this);


	Instantiate<StartButton>(this);
	Instantiate<MoveButton>(this);



	//テキスト初期化
	pTexts_ = new Texts;
	pTexts_->Initialize();

}

//更新
void SampleScene::Update()
{



}

//描画
void SampleScene::Draw()
{
	/*int , float ,double , char , std::string から、出力したいデータを選んでDrawを呼ぶ*/
	const float drawText = -100.0234f;	//floatなどは、　小数点以下の値が、正確なものにならない
								//仮に、constにしても、	0.3 = 0.29999~~~という値になる		（浮動小数点型を使用したときに、　丸め誤差として、2進数の近似値で表現するほかないということ）
								//実際、左記の値は、100.0234 = 100.02399となる
	double b = 100.0234f;

	std::string s = std::to_string(drawText);

	//TextのDraw実行
	//float型
	//pTexts_->DrawTex<float>(drawText, TEXT_COLOR_BLUE, 100, 100);

	//char型
	//pTexts_->DrawTex<std::string>("X :", TEXT_COLOR_VIOLET, 100, 100);
	pTexts_->DrawTex<char>('X', TEXT_COLOR_VIOLET, 100, 100);
	//現在PosX
	//pTexts_->DrawTex<float>(pController_->transform_.position_.vecX, TEXT_COLOR_BLUE, 130, 100);
	pTexts_->DrawTex<float>(pPlayer_->transform_.position_.vecX, TEXT_COLOR_BLUE, 130, 100);
	//char型
	pTexts_->DrawTex<char>('Y', TEXT_COLOR_VIOLET, 300, 100);
	//現在PosY
	pTexts_->DrawTex<float>(pPlayer_->transform_.position_.vecY, TEXT_COLOR_BLUE, 330, 100);
	//char型
	pTexts_->DrawTex<char>('Z', TEXT_COLOR_VIOLET, 500, 100);
	//現在PosZ
	pTexts_->DrawTex<float>(pPlayer_->transform_.position_.vecZ, TEXT_COLOR_BLUE, 530, 100);


	////int型
	//pTexts_->DrawTex<int>((int)drawText, TEXT_COLOR_VIOLET, 300, 100);
	////double型
	//pTexts_->DrawTex<double>((double)drawText, TEXT_COLOR_VIOLET, 500, 100);
	////char型
	//pTexts_->DrawTex<char>('a', TEXT_COLOR_VIOLET, 700, 100);
	////string型
	//pTexts_->DrawTex<std::string>(s, TEXT_COLOR_VIOLET, 900, 100);


}

//開放
void SampleScene::Release()
{
}