#include "Ground.h"
#include "../Engine/Model.h"
#include "../Shader/CommonDatas.h"


//コンストラクタ
Ground::Ground(GameObject * parent)
	: GameObject(parent, "Ground")
{
}

//初期化
void Ground::Initialize()
{
	//モデルのロード
	//ヘイトマップ専用
	hModel_ = Model::Load("Assets/HeightMap/HeightMapTex.png", HEIGHT_MAP_POLYGON_GROUP, SHADER_HEIGHT_MAP);
	assert(hModel_ != -1);




}

//更新
void Ground::Update()
{



}

//描画
void Ground::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

}

//開放
void Ground::Release()
{
}