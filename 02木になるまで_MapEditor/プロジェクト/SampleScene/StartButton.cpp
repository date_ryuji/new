#include "StartButton.h"
#include "../Engine/Input.h"
#include "Bullet.h"


StartButton::StartButton(GameObject * parent):
	Button(parent)
{
}

StartButton::~StartButton()
{
}


/*
void Button::Initialize()
{
	//ボタンのロード
	Load("" , ON_BUTTON);
	Load("", OFF_BUTTON);
}
void Button::Update()
{
	//ボタンの接触判定
	UpdateButtonForOnContact();
}
void Button::Draw()
{
	//ボタン描画
	DrawButton();
}
void Button::Release()
{
	//ボタンクラスの動的確保した要素を解放
	ReleaseButton();
}
*/
void StartButton::Initialize()
{
	//ボタンのロード
	Load("Assets/SampleScene/StartButton_ON.png", ON_BUTTON);
	Load("Assets/SampleScene/StartButton_OFF.png", OFF_BUTTON);

	//サイズの可変
	//500pix * 500pix
	SetPixelScale(500, 500);
}

void StartButton::Update()
{
	//ボタンの接触判定
	UpdateButtonForOnContact();
}

void StartButton::Draw()
{
	//ボタン描画
	DrawButton();
}

void StartButton::Release()
{
	//ボタンクラスの動的確保した要素を解放
	ReleaseButton();
}



void StartButton::OnContactButton()
{
	//ボタンの押下
	ButtonPress();

	//if (Input::IsMouseButtonDown(0))
	//{
	//	Instantiate<Bullet>(this);
	//}

	//スイッチ式ON,OFF
	//押した瞬間に一回実行
	//ButtonPressOneDown();

	//スイッチ式ON,OFF
	//離した瞬間に一回実行
	//ButtonPressOneUp();

}