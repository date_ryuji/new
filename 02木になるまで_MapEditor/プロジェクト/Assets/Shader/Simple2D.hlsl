//2Dの、カメラや、影が必要のないシェーディング
//Direct3D.cppにおいての、頂点シェーダ、ピクセルシェーダ
						//Simple3Dのvsという関数を使って、などと、コンパイルを行わせていた





//2Dにおいて、テクスチャ、サンプラーいる
//───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
Texture2D	g_texture : register(t0);	//テクスチャー
								//テクスチャを複数使うときに、ｔ０の0の番号が変化する

SamplerState	g_sampler : register(s0);	//サンプラー


//カメラの位置を計算する、ワールド・ビュー・などはいらない、カメラの位置など関係ないので
//───────────────────────────────────────
 // コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────

cbuffer global

{


	//ここで、行列を受け取っているので、これとは別に、
//法線を回転させるために、回転行列を受け取らなければいけない
	float4x4	matW;	//ワールド行列を受け取り、回転行列を受け取る



};
//→行列の掛け算
//→掛け算は、どの順番で書けても、おなじなので、
//ここで、　あらかじめ、すべての行列をかけたものを入れる（それを、最初のローカル座標に掛ける）



//Position,UVは必要
//明るさ　光のほうを向いていたら、明るいなど、→光の情報には関係ないので、いらない
//───────────────────────────────────────

// 頂点シェーダー出力＆ピクセルシェーダー入力データ構造体

//───────────────────────────────────────

struct VS_OUT

{

	float4 pos		: SV_POSITION;	//位置
	float2 uv		: TEXCOORD;//UV


};
//VS_OUTというのが、ピクセルシェーダーに対して、情報を与える構造体である
	//→戻り値として渡す、VS＿OUT　UVを入れてやって



//───────────────────────────────────────

// 頂点シェーダ

//───────────────────────────────────────
//VS = V~ Shader

VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD)
//posとuvだけ

{

	//ピクセルシェーダーへ渡す情報

	VS_OUT outData;

	//matWに、必要なワールド行列が入ってくるので、その情報を受け取りポジションに掛ける
	outData.pos = mul(pos, matW);

	//UV　＝テクスチャの座標（テクスチャにおけるこのピクセルはこの位置）
	outData.uv = uv;


	//まとめて出力
	return outData;

}



//───────────────────────────────────────

// ピクセルシェーダ
	//★ピクセルの色などを管理する。
	//→そのため、テクスチャを張ったときの、UV情報なども、どのテクスチャのUV位置を貼り付けるのかも持つ
//───────────────────────────────────────

float4 PS(VS_OUT inData) : SV_Target
{
	//通常ピクセル色
	float4 color = (g_texture.Sample(g_sampler, inData.uv));

	//color.a = 0.5f;	//アルファ値を指定すると、　全体のテクスチャ部分のアルファ値が変わるので、　全体に対してアルファ値を替える処理は行えない

		//ピクセル、→テクスチャの色そのまま出ればいい
		//３Dにおける、★★影（明るさ）などの情報をすべて撤去すればいい
return color;	//テクスチャのそのものの色
								//*inData.colorは、明るさなどを変えるときに使用したので、いらない
								//テクスチャそのものの色

}



float4 PS_BlurredColor(VS_OUT inData) :SV_Target
{
	//通常ピクセル色
//float4 color = (g_texture.Sample(g_sampler, inData.uv));

//2マス以上用の色とで平均を取る際には、上下左右2マス先を取るだけでなく、1マス右上なども必要になる
	//つまり、十字が広がるのではなく、
	//ダイヤのような形で広がっていくイメージが、平均を取るのに適している
//マスを広げる範囲
		//５なら5マス先まで上下左右のピクセルを広げて、　かつ、ひし形のように、範囲を広げていく
		//その位置のピクセル色を足して、足した数分割ることで、平均の色を取得する
int loop = 5;
int sumColor = 0;
float4 defaultColor = (g_texture.Sample(g_sampler, inData.uv));	//通常出力カラー
float4 blurredColor = (g_texture.Sample(g_sampler, inData.uv));	//初期のカラーを、通常のピクセル色にする
sumColor += 1;
float widthPix = (1.0 / 800.f);
float heightPix = (1.0 / 600.f);
for (int i = 1; i <= loop; i++)
{

		float4 up = g_texture.Sample(g_sampler, inData.uv + float2(0, heightPix * (float)i));
		float4 down = g_texture.Sample(g_sampler, inData.uv - float2(0, heightPix * (float)i));
		float4 right = g_texture.Sample(g_sampler, inData.uv + float2(widthPix * (float)i, 0));
		float4 left = g_texture.Sample(g_sampler, inData.uv - float2(widthPix * (float)i, 0));
		blurredColor =
			blurredColor + up + down + right + left;


		sumColor += 4;



}



//斜め位置色も足すのだが、
		//数が大きくなるほど、
		//塗る数がおおくなるため、再帰などの処理が必要になる
		//g_texture.Sample(g_sampler, inData.uv - float2(0, (1.0 / 600.f) * (float)i)) +
		//g_texture.Sample(g_sampler, inData.uv - float2(0, (1.0 / 600.f) * (float)i)) +

		//＋Y　上に伸ばしたピクセルから見て、
			//�@右下
			//�@＿１：＋X方向に　1 ~ (i - 1)までのピクセル　の色を足して
			//�@＿２：- Y方向に　(i - 1) ~ 1までのピクセル　の色を足して

			//�A左下
			//�A＿１：- X方向に　-1 ~ -(i - 1)までのピクセル　の色を足して
			//�A＿２：- Y方向に　(i - 1) ~ 1までのピクセル　の色を足して

		//- Y　↓に伸ばしたピクセルから見て、
			//�B右上
			//�B＿１：＋X方向に　1 ~ (i - 1)までのピクセル　の色を足して
			//�B＿２：＋Y方向に　-(i - 1) ~ -1までのピクセル　の色を足して

			//�C左上
			//�C＿１：- X方向に　-1 ~ -(i - 1)までのピクセル　の色を足して
			//�C＿２：＋Y方向に　-(i - 1) ~ -1までのピクセル　の色を足して

		//これをそれぞれ色を足すことが出来れば、
		//ひし形上に広がるピクセルの合計を取ることが可能
		//X
for (int x = 1; x <= loop - 1; x++)
{
	//�@＿１
		// X は　forの要素
		// Y は　i - 1
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(widthPix * (float)x, heightPix * (float)(i - 1))
			);

	//�A＿１
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(-(widthPix * (float)x), heightPix * (float)(i - 1))
			);

	//�B＿１
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(widthPix * (float)x, -(heightPix * (float)(i - 1)))
			);

	//�C＿１
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(-(widthPix * (float)x), -(heightPix * (float)(i - 1)))
			);

	sumColor += 4;

}


//Y
for (int y = 1; y <= loop - 1; y++)
{
	//�@＿２
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(widthPix * (float)(i - 1), heightPix * (float)y)
			);

	//�A＿２
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(-(widthPix * (float)(i - 1)), heightPix * (float)y)
			);


	//�B＿２
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(widthPix * (float)(i - 1), -(heightPix * (float)y))
			);

	//�C＿２
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(-(widthPix * (float)(i - 1)), -(heightPix * (float)y))
		);

	sumColor += 4;

}


return blurredColor / sumColor;

/*
//ピクセルをボケさせる
	//一ピクセルを基準に、そのピクセルの上下左右のピクセルの色をもらって、
	//そのピクセル色を足して、計算した合計ピクセル数で割る（平均をとる）
	float4 color = (g_texture.Sample(g_sampler, inData.uv) +
		g_texture.Sample(g_sampler, inData.uv + float2(1.0 / 800 , 0)) +
		g_texture.Sample(g_sampler, inData.uv - float2(1.0 / 800 , 0)) +
		g_texture.Sample(g_sampler, inData.uv + float2(0, 1.0 / 600)) +
			g_texture.Sample(g_sampler, inData.uv - float2(0, 1.0 / 600))) / 5;


*/

}




//ぼかしたいなら、
//そのピクセルの　周囲のピクセルの色を足して割る（平均）をとることができれば、
//ぼかすことが可能

//右隣りのドットを見るには、
//スクリーンがW８００＊H６００ならば、
	//右は　８００分の１　足した位置


	//上下左右の大きさを大きくして、その合計を足して、足した数分割るとすれば、
		//大きい数合計して、割れば、　大きなボケを作れる

	//Yは足さずに、Xだけを足してやる。
		//右横に延びているようなものもできる。

	//きちんとぼかすなら、
	//重みをとる
	/*

	より使いたい色を大きい値で取っておいて、
	合計、　で大きい色のピクセル色が出るように。




	*/

	//一部分モザイクも、可能。
	//一部分をSpriteとして取得して、その部分だけモザイク処理も可能


//色がある程度暗かったら
//黒と返すとして、
//Mainに普通に描画して
//黒色の部分は黒くなる。

//その画像を、
	//普通のピクセルで描画するとして、
	//そのピクセルと、加算合成をすると、

	//暗いところはそのままで、　明るいところは、黒くした一方のピクセルで、そのままの明るさが残っているので、
		//明るいところをより強調する表現ができる。




//------------------------------------------------------------------------------
//ピクセルシェーダー
//黒澤（白黒）モード

//------------------------------------------------------------------------------
float4 PS_KUROSAWA(VS_OUT inData) : SV_Target
{

	float4 color = g_texture.Sample(g_sampler, inData.uv);	//テクスチャのそのものの色

//白黒に切り詰める
//最終的な色のRGB　は　０〜１の値である。
//つまりRGBの合計が3に近ければ白、　0に近ければ黒にすればよい
float sum = color.r + color.g + color.b;

if (sum > 2.5f)
{
	return float4(1, 1, 1, 1);
}
else if (sum > 2.0f)
{
	return float4(0.7f, 0.7f, 0.7f, 1);
}
else if (sum > 1.5f)
{
	return float4(0.5f, 0.5f, 0.5f, 1);
}
else if (sum > 1.0f)
{
	return float4(0.3f, 0.3f, 0.3f, 1);
}
else
{
	return float4(0, 0, 0, 1);
}


}







