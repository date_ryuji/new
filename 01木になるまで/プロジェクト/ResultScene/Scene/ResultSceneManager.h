#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/SceneManagerParent.h"	//シーンマネージャー　抽象クラス

//クラスのプロトタイプ宣言
class CountTimer;
//enum clas のプロトタイプ宣言
enum class RESULT_SCENE_INPUT_ALGORITHM;
enum class RESULT_SCENE_INPUT_TYPE;



//リザルトシーンにおけるUIGroup（UI群）のタイプ
	//詳細：リザルトシーン内にて管理する常駐のUIGroup
	//　　：外部からUIGroupの登録する際に使用する
enum class RESULT_SCENE_UI_GROUP_TYPE
{
	RESULT_UI_GROUP_TITLE = 0,	//タイトル画像グループ

	RESULT_UI_GROUP_MAX,		//MAX
};



//リザルトシーンにおける連携や仲介が必要なオブジェクトクラス
	//詳細：連携を行う際にアクセス先のオブジェクトクラス
	//　　：外部からオブジェクトの登録する際に使用する
enum class RESULT_SCENE_COOP_OBJECTS
{
	RESULT_COOP_GAME_INPUTER = 0,	//ユーザー入力受け付けクラス

	RESULT_COOP_MAX,				//MAX
};





/*
	クラス詳細：リザルトシーンのマネージャークラス（シーン内オブジェクトとの橋渡しオブジェクト）
	クラスレベル：サブクラス（スーパークラス（SceneManagerParent））
	使用クラス　：ResultScene
	クラス概要（詳しく）
				：マネージャークラス
				：シーン内の常駐UIGroupを登録し、表示、非表示
				：シーンの各オブジェクトの連携（橋渡し）を行うクラス
				：詳しくは、SceneManagerParentを参照

*/
class ResultSceneManager : public SceneManagerParent
{
//protected メンバ変数、ポインタ、配列
protected:
	//時間計測クラス
		//詳細：GameSceneで用いる時間計測タイマー（カウントダウン）のクラスに、
		//　　：カウントアップの機能も持たせて、そのクラスで、時間を計測する
	CountTimer* pCountTimer_;

//public ゲームループメソッド
public:
	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）	
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	ResultSceneManager(GameObject* parent);


	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~ResultSceneManager() override;

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;


	//UIGroupの個数を取得
		//詳細：自身の所有しているUIGroup群（ppUIGroups_）内のUIGroupの個数を取得
		//  　：○○_SCENE_UI_GROUP_TYPE::○○_UI_GROUP_MAX(enum)を返す。（○○　＝　各シーン名）
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	int GetUIGroupCount() override;



//public メソッド
public:


	/*連携処理系***************************************************************/

	//シーンを切り替える
		//詳細：選択シーンへ切り替え
		//　　：SCENE_ID::SCENE_ID_TITLEへ切り替え
		//連携先：SceneChanger
		//連携元：ResultSceneInputStrategy
		//引数：なし
		//戻値：なし
	void ChangeScene();


};



