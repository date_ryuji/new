#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/SceneInputStrategyParent.h"	//入力行動ストラテジークラス　抽象クラス

//クラスのプロトタイプ宣言
class ResultSceneManager;
class ResultSceneInputStrategy;
class SceneUserInputter;
//構造体のプロトタイプ宣言
struct CreateInputStrategyInfo;
//enum class プロトタイプ宣言
enum class RESULT_SCENE_INPUT_TYPE;
enum class RESULT_SCENE_UI_GROUP_TYPE;


//リザルトシーンのデバイス入力　押下時のアルゴリズム
	//詳細：デバイス（キーボード、マウス）押下際の行動アルゴリズムを記述したクラスを識別するタイプ
enum class RESULT_SCENE_INPUT_ALGORITHM
{
	RESULT_INPUT_ALGORITHM_CHANGE_SCENE = 0,		//シーンを切り替え

	RESULT_INPUT_ALGORITHM_NULL,					//行動なし
													//キー入力による行動をなし	
													//行動がセットされているときは、その行動を破棄する役割

	RESULT_INPUT_ALGORITHM_MAX,						//MAX
};



/*
	クラス詳細	：各デバイス入力を行った際の行動クラス　作成クラス（シーン専用）
	使用デザインパターン：FactoryMethod
	使用クラス	：ResultScene
	クラスレベル：サブクラス（スーパークラス（FactoryForSceneInputStrategyParent））
	クラス概要（詳しく）
				：RESULT_SCENE_INPUT_ALGORITHMに登録した行動アルゴリズムタイプにて識別し、
				　専用クラスを作成する

*/
class FactoryForResultSceneInputStrategy : public FactoryForSceneInputStrategyParent
{
//private メソッド
private:

	//ストラテジークラスを生成する
		//詳細：生成するストラテジークラスを引数タイプより識別し、
		//　　：ストラテジータイプを生成して返す
		//引数：行動クラス識別ID(enum)
		//引数：シーンマネージャー
		//引数：入力情報
	ResultSceneInputStrategy* CreateInputStrategy(
		RESULT_SCENE_INPUT_ALGORITHM type , 
		ResultSceneManager* pSceneManager , 
		CreateInputStrategyInfo* pCreateInfo);


//public メソッド
public:
	//コンストラクタ
		//引数：なし
		//戻値：なし
	FactoryForResultSceneInputStrategy();


	//行動クラスのインスタンスを生成する関数
		//詳細：引数タイプ（TITLE_SCENE_INPUT_ALGORITHM(enum)）にて生成クラスを識別し、行動クラスのインスタンスを作成、インスタンスを返す
		//　　：親である、行動クラス生成の実行を呼び出す
		//　　：以下関数の中で、
		//　　：行動クラスの作成
		//　　：行動クラスをキータイプとともに、UserInputterに登録　の２つを行っている。
		//　　：引数へ該当情報を渡すだけで、キー入力の操作を確定できる
		//引数：行動アルゴリズム識別タイプ（TITLE_SCENE_INPUT_ALGORITHM(enum)）
		//引数：シーンマネージャー（行動アルゴリズムにて各シーンオブジェクトの連携が必要である場合、シーンマネージャーを利用するため、シーンマネージャーに呼び込むメソッドを用意しておく。そのメソッド呼び込みのために取得）
		//引数：入力クラス（ユーザー入力受付クラス）
		//引数：入力情報（入力受付方法+入力デバイスタイプ+入力コード）
		//戻値：なし
	void CreateResultSceneInputStrategy(
		RESULT_SCENE_INPUT_ALGORITHM type, ResultSceneManager* pSceneManager,
		SceneUserInputter* pSceneUserInputter,
		CreateInputStrategyInfo* pCreateInfo);

};




/*
	クラス詳細	：デバイス入力時の実行行動を実装した行動ストラテジークラス
	使用クラス	：ResultScene
	クラスレベル：スーパークラス（スーパークラス（SceneInputStrategyParent））
	クラス概要（詳しく）
				：行動実行が確定した場合、行動アルゴリズムを記述するメソッドを持つクラス
				　継承先においても、メソッドをオーバーライドすることで、各継承先で個別の処理を記述可能。
				  デバイス入力時の行動を変えることができる

*/
class ResultSceneInputStrategy : public SceneInputStrategyParent
{
//protected メソッド
protected:
	//継承元クラスを各シーンごとの継承先型に変換
		//詳細：ResultSceneManager型にキャストして返す
		//引数：シーンマネージャークラス
		//戻値：ResultSceneManager型にキャストしたシーンマネージャークラス
	ResultSceneManager* TransResultSceneManager(SceneManagerParent* pSceneManager);


//public メソッド
public:
	//コンストラクタ
		//詳細：スプラッシュシーンマネージャーの取得
		//引数：シーンマネージャークラス
		//引数：入力情報（入力受付方法+入力デバイスタイプ+入力コード）
		//戻値：なし
	ResultSceneInputStrategy(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo);


	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void AlgorithmInterface() = 0;
};



