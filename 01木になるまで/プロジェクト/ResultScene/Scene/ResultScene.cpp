#include "ResultScene.h"							//ヘッダ
#include "../../Engine/GameObject/UIGroup.h"		//UIGroup　シーン内UI群
#include "../../Engine/Scene/SceneUIManager.h"		//シーンのUIマネージャー
#include "../../Engine/Scene/SceneUserInputter.h"	//ユーザー入力受付クラス　キー、マウス入力受け取りと受け取り時の行動呼び出しクラス
#include "../../CommonData/GameDatas.h"				//ゲーム内情報定義
#include "../../Engine/Text/Texts.h"				//テキスト描画クラス
#include "../../Engine/Csv/CsvReader.h"				//Csvファイル読み取りクラス
#include "ResultSceneManager.h"						//リザルトシーンマネージャー
#include "ResultSceneInputStrategy.h"				//ユーザー入力時の行動実行クラス


//コンストラクタ
//SceneParentのコンストラクタ呼び込み
	//引数：親オブジェクト
	//引数：シーン名のオブジェクト名（シーン識別情報群(Project/CommonDatas/SceneType.h)よりシーンオブジェクト名を取得する（シーンをID識別しオブジェクト名取得））
ResultScene::ResultScene(GameObject * parent)
	: SceneParent::SceneParent(parent, SCENE_OBJECT_NAME[(int)SCENE_ID::SCENE_ID_RESULT]),
	pTexts_(nullptr),
	percentage_(0)
{
}

//初期化
void ResultScene::Initialize()
{
	//ゲームの背景色を切り替える
		//背景色の変化
	//背景色取得
	GameDatas::SetGameBackGroundColor(BACKGROUND_COLOR_BLACK);


	//リザルトシーンクラスのマネージャー
		//詳細：SceneParentを継承しているため、
		//　　：継承元関数から、シーンマネージャークラスを作成させる
		//制限　　：シーンマネージャークラスは、GameObject型継承のオブジェクト作成関数である、Instantiateでの作成は推奨されない。
		//制限理由：制限をかけるのは、シーンマネージャーが作成されたことを伝えなくてはいけないクラスが存在するためである。
	ResultSceneManager* pSceneManager = 
		(ResultSceneManager*)CreateSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_RESULT);

	//UI作成
	CreateSceneUIGroups(pSceneManager);

	//ユーザー入力の作成
	CreateInitUserInputter(pSceneManager);



	//CSVReaderにて、必要要素を読み込む
	CsvReader* pCsvReader = new CsvReader;
	//ロード
	pCsvReader->Load("Assets/Scene/OutputData/ResultScene/Percentage.csv");

	//要素の読み込み（緑の豊かさ（繁殖率）を数値で読み取り）
	percentage_ = pCsvReader->GetValueInt(0, 0);
	//解放（データ読みとり、ヘッダに保存したので、ファイルデータを解放）
	SAFE_DELETE(pCsvReader);


	//テキスト描画クラス動的確保
	pTexts_ = new Texts;
	//初期化
	pTexts_->Initialize();

}

//更新
void ResultScene::Update()
{
}

//描画
void ResultScene::Draw()
{
	//テキストのフォントサイズ変更（単位：pix）
	pTexts_->SetFontSize(100);
	//文字列テキスト出力
	pTexts_->DrawStringTex("繁殖率" , TEXT_COLOR_BLACK, 200, 100);
	
	//テキストのフォントサイズ変更（単位：pix）
	pTexts_->SetFontSize(200);
	//繁殖率を桁によって、
	//出力位置を変更
	{
		int oneHundred = percentage_ / 100;
		pTexts_->DrawTex<int>(oneHundred, TEXT_COLOR_BLACK, 350, 250);
	}
	if (percentage_ >= 100)
	{
		//１００を超える場合
			//１００を引き
			//１０の位の値を取得
		int ten = (percentage_ - 100) / 10;
		pTexts_->DrawTex<int>(ten, TEXT_COLOR_BLACK, 500, 250);
	}
	else
	{
		//１００を超えない場合
			//１０で割り、１０の位の値を求める
		int ten = percentage_ / 10;
		pTexts_->DrawTex<int>(ten, TEXT_COLOR_BLACK, 500, 250);
	}
	{
		//１０とのあまりを出すことで、
		//１０で割り切れなかった時のあまり　＝　１の位を取得
		int one = percentage_ % 10;
		pTexts_->DrawTex<int>(one, TEXT_COLOR_BLACK, 650, 250);
	}

	//pTexts_->DrawTex<int>(percentage_, TEXT_COLOR_BLACK, 500, 250);


	//テキストのフォントサイズ変更（単位：pix）
	pTexts_->SetFontSize(100);
	//文字列テキスト出力
	pTexts_->DrawTex<char>('%', TEXT_COLOR_BLACK, 850, 450);


}

//解放
void ResultScene::Release()
{
	//解放
	SAFE_DELETE(pTexts_);
}

//UIGroupへUi画像の追加
int ResultScene::AddStackUI(UIGroup * pUIGroup, const std::string& FILE_NAME)
{
	//引数UIGroupに追加する
	return pUIGroup->AddStackUI(FILE_NAME);
}
//シーンの常駐UIGroupを作る
void ResultScene::CreateSceneUIGroups(ResultSceneManager * pSceneManager)
{
	//UIGroupの作成

	//所属ロゴUI群
	{
		//UIGroup作成
		//インスタンス生成
		UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

		//UIGroupをSceneMnagerにセットする
		//SPLASH_UI_GROUP_AFFILIATION
		//所属ロゴUI群
		pSceneManager->AddUIGroup((int)RESULT_SCENE_UI_GROUP_TYPE::RESULT_UI_GROUP_TITLE, uiGroup);

		//UI画像
		{

			//背景画像
			{
				int hStart = AddStackUI(uiGroup, "Assets/Scene/Image/ResultScene/BackGround.png");

				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

				//サイズの可変
				pSceneUIManager->SetPixelScale(hStart, 1300, 800);

				for (int i = 0; i < 2; i++)
				{
					//画像の奥行きを一段階下げる
						//ほか画像より奥に描画することで、奥行き関係を管理する
				pSceneUIManager->LowerOneStep(hStart);
				
				}

			}		

			//マウス左クリックロゴ　
			{
				int hClick = AddStackUI(uiGroup, "Assets/Scene/Image/TitleScene/MouseLeftButtonClick.png");

				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

				pSceneUIManager->LowerOneStep(hClick);

				//サイズの可変
				pSceneUIManager->SetPixelScale(hClick, 300, 300);
				//位置の移動
				pSceneUIManager->SetPixelPosition(hClick, 1100, 500);
			}
			//スタートロゴ　
			{
				int hStart = AddStackUI(uiGroup, "Assets/Scene/Image/TitleScene/StartButton.png");

				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

				//サイズの可変
				pSceneUIManager->SetPixelScale(hStart, 500, 400);
				//位置の移動
				pSceneUIManager->SetPixelPosition(hStart, 850, 650);
			}


		}
		//UIGroupの描画
		pSceneManager->VisibleSceneUIGroup((int)RESULT_SCENE_UI_GROUP_TYPE::RESULT_UI_GROUP_TITLE);
	}

}

//ユーザー入力の作成
void ResultScene::CreateInitUserInputter(ResultSceneManager* pSceneManager)
{
	//ユーザー入力設定
	{
		//ユーザー入力作成用のファクトリークラスの作成
		// リザルトシーン専用であることを確認
		//ResultSceneInputStrategyより
		FactoryForResultSceneInputStrategy* pFactory = new FactoryForResultSceneInputStrategy();

		//ユーザー入力クラス作成
		SceneUserInputter* pSceneUserInputter =
			(SceneUserInputter*)Instantiate<SceneUserInputter>(this);

		//入力クラスをゲームマネージャーに登録
		{
			//シーンマネージャーへ
			//自身のインスタンスを知らせる
			pSceneManager->AddCoopObject(
				(int)RESULT_SCENE_COOP_OBJECTS::RESULT_COOP_GAME_INPUTER, (GameObject*)this);
		}


		//追加
		//マウス入力と入力による行動を追加
		//行動：RESULT_INPUT_ALGORITHM_CHANGE_SCENE
		{
			//入力情報の作成
			CreateInputStrategyInfo* pCreateInfo = new CreateInputStrategyInfo(
				KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_DOWN,
				INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_MOUSE,
				(int)MOUSE_CODE::MOUSE_BUTTON_LEFT);


			//入力追加
			//入力情報と他もろもろの情報を渡す。
			pFactory->CreateResultSceneInputStrategy(
				RESULT_SCENE_INPUT_ALGORITHM::RESULT_INPUT_ALGORITHM_CHANGE_SCENE,
				pSceneManager,
				pSceneUserInputter,
				pCreateInfo);

		}


		//生成クラスの消去
		SAFE_DELETE(pFactory);

	}
}
