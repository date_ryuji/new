#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/SceneParent.h"	//シーンオブジェクト　抽象クラス

//クラスのプロトタイプ宣言
class UIGroup;
class TitleSceneManager;

/*
	クラス詳細	：タイトルシーン　
	クラスレベル：サブクラス（スーパークラス（SceneParent））
	クラス概要（詳しく）
				：タイトル表示クラス

*/
class TitleScene : public SceneParent
{
//private メンバ変数、ポインタ、配列
private:

	//音楽ハンドル
		//詳細：シーン内BGM
		//　　：ループ
	int hAudio_;


//private メソッド
private : 
	//UI画像作成処理
		//詳細：UIGroupへUI画像のロードを担うメソッド
		//引数：UIGroup
		//引数：画像ファイル名
		//戻値：UIGroupへのハンドル
	int AddStackUI(UIGroup* pUIGroup, const std::string& FILE_NAME);


	//シーンのUIGroupを作る
		//詳細：UIGroup（SceneManagerにて管理する）を作成、SceneManagerに登録する
		//　　：SceneManagerにてUIGroupの管理ができるようにする
		//引数：タイトルシーンマネージャー
		//戻値：なし
	void CreateSceneUIGroups(TitleSceneManager * pSceneManager);

	//シーンのユーザー入力を作成
		//詳細：デバイスによる入力の定義と、入力時の行動を定義することで、ユーザー入力受付準備を行う
		//引数：タイトルシーンマネージャー
		//戻値：なし
	void CreateInitUserInputter(TitleSceneManager* pSceneManager);


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	TitleScene(GameObject* parent);

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	
	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;
};