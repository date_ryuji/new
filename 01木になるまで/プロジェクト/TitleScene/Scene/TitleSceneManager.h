#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/SceneManagerParent.h"	//シーンマネージャー　抽象クラス

//クラスのプロトタイプ宣言
class CountTimer;
//enum class のプロトタイプ宣言
enum class TITLE_SCENE_INPUT_ALGORITHM;
enum class TITLE_SCENE_INPUT_TYPE;



//タイトルシーンにおけるUIGroup（UI群）のタイプ
	//詳細：タイトルシーン内にて管理する常駐のUIGroup
	//　　：外部からUIGroupの登録する際に使用する
enum class TITLE_SCENE_UI_GROUP_TYPE
{
	TITLE_UI_GROUP_TITLE = 0,	//タイトル画像グループ

	TITLE_UI_GROUP_MAX,			//MAX

};



//タイトルシーンにおける連携や仲介が必要なオブジェクトクラス
	//詳細：連携を行う際にアクセス先のオブジェクトクラス
	//　　：外部からオブジェクトの登録する際に使用する
enum class TITLE_SCENE_COOP_OBJECTS
{
	TITLE_COOP_GAME_INPUTER = 0,	//ユーザー入力受け付けクラス

	TITLE_COOP_MAX,					//MAX
};



/*
	クラス詳細：タイトルシーンのマネージャークラス（シーン内オブジェクトとの橋渡しオブジェクト）
	クラスレベル：サブクラス（スーパークラス（SceneManagerParent））
	使用クラス　：TitleScene
	クラス概要（詳しく）
				：マネージャークラス
				：シーン内の常駐UIGroupを登録し、表示、非表示
				：シーンの各オブジェクトの連携（橋渡し）を行うクラス
				：詳しくは、SceneManagerParentを参照

*/
class TitleSceneManager : public SceneManagerParent
{
//public ゲームループメソッド
public:
	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）	
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	TitleSceneManager(GameObject* parent);


	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~TitleSceneManager() override;

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;


	//UIGroupの個数を取得
		//詳細：自身の所有しているUIGroup群（ppUIGroups_）内のUIGroupの個数を取得
		//  　：○○_SCENE_UI_GROUP_TYPE::○○_UI_GROUP_MAX(enum)を返す。（○○　＝　各シーン名）
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	int GetUIGroupCount() override;


//public メソッド
public:


	/*連携処理系***************************************************************/
	
	//シーンを切り替える
		//詳細：選択シーンへ切り替え
		//　　：SCENE_ID::SCENE_ID_GAMEへ切り替え
		//連携先：SceneChanger
		//連携元：TitleSceneInputStrategy
		//引数：なし
		//戻値：なし
	void ChangeScene();


};



