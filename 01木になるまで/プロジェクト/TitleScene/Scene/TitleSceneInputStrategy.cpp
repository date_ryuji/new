#include "TitleSceneInputStrategy.h"	//ヘッダ
#include "TitleSceneManager.h"			//シーンマネージャー


/*行動クラス（シーンごとのシーン名SceneInputStrategyを継承したクラス）宣言*******************************************************************************************************/
	//詳細：Factoryクラス（行動クラス生成クラス）にてインスタンスの動的確保を行うため、Factoryの実装よりも前に記述
	//詳細：行動クラスを複数作ることになるため、行動クラスごとにクラスファイルを作成すると、クラスファイル量が多くなる。
	//　　：→上記を避けるために、一つのクラスに行動クラスをまとめる。（メソッドやメンバも少ないため、ひとくくりにしてしまう。）
/*****************************************************************/
/*シーン切り替え　アルゴリズム*/
/*TITLE_INPUT_ALGORITHM_CHANGE_SCENE*/
class ChangeSceneForTitleScene : public TitleSceneInputStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//引数：入力情報（入力デバイス、入力コード　など）
		//戻値：なし
	ChangeSceneForTitleScene(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo);


	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/



/*行動クラス生成クラス　実装*******************************************************************************************************/
/*Factory*/
//コンストラクタ
FactoryForTitleSceneInputStrategy::FactoryForTitleSceneInputStrategy()
{
}

//行動クラスのインスタンスを生成する関数
void FactoryForTitleSceneInputStrategy::CreateTitleSceneInputStrategy(
	TITLE_SCENE_INPUT_ALGORITHM type, TitleSceneManager* pSceneManager, 
	SceneUserInputter* pSceneUserInputter , 
	CreateInputStrategyInfo* pCreateInfo)
{
	//適切なシーンで生成されているかの判断
	//条件：現在のシーンマネージャーが引数ポインタと同様でない
	if (!IsExistsSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_TITLE , pSceneManager))
	{
		//処理終了
		return;
	}


	//行動クラス（生成クラスを引数typeより識別）を生成
	TitleSceneInputStrategy* pInputStrategy =
		CreateInputStrategy(type, pSceneManager, pCreateInfo);


	//条件：行動クラスを生成できたなら
	if (pInputStrategy != nullptr)
	{
		//行動クラスをユーザー入力受付クラスに登録
			//詳細：入力情報（入力デバイス、入力コード）と、その入力時の行動クラスを登録し、
			//　　：入力時の行動実行準備を行う
		RegistrationStrategyInSceneUserInputter(
			pSceneUserInputter, pInputStrategy, pCreateInfo);
	}
	
	return;
}

//ストラテジークラスを生成する
//パターン：FactoryMethod
TitleSceneInputStrategy* FactoryForTitleSceneInputStrategy::CreateInputStrategy(TITLE_SCENE_INPUT_ALGORITHM type, TitleSceneManager* pSceneManager, CreateInputStrategyInfo* pCreateInfo)
{
	switch (type)
	{
	case TITLE_SCENE_INPUT_ALGORITHM::TITLE_INPUT_ALGORITHM_CHANGE_SCENE:
	{
		//行動クラス生成関数を呼び出す
			//必要引数を渡し
			//該当クラスをテンプレートとして渡し、渡したテンプレートの型でインスタンスを生成
		return CreateStrategy<ChangeSceneForTitleScene>(pSceneManager, pCreateInfo);
	}break;

	default:
		return nullptr;

	}
	return nullptr;
}
/**************************************************************/


/*シーンごとのユーザー入力時の行動クラス　親クラス　実装*******************************************************************************************************/
//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込みにより初期化
TitleSceneInputStrategy::TitleSceneInputStrategy(
	SceneManagerParent* pSceneManager,
	CreateInputStrategyInfo* pCreateInfo) : 

	SceneInputStrategyParent(pSceneManager , pCreateInfo)
{

}

//継承元クラスを各シーンごとの継承先型に変換
TitleSceneManager* TitleSceneInputStrategy::TransTitleSceneManager(SceneManagerParent* pSceneManager)
{
	return (TitleSceneManager*)pSceneManager;
}

/*****************************************************************/


/*行動クラス（シーンごとのシーン名SceneInputStrategyを継承したクラス）実装*******************************************************************************************************/

/*****************************************************************/
/*シーン切り替え　アルゴリズム*/
/*TITLE_INPUT_ALGORITHM_CHANGE_SCENE*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
ChangeSceneForTitleScene::ChangeSceneForTitleScene(
	SceneManagerParent* pSceneManager,
	CreateInputStrategyInfo* pCreateInfo) :

	TitleSceneInputStrategy(pSceneManager, pCreateInfo)
{
}

//デバイス入力時　実行する行動
void ChangeSceneForTitleScene::AlgorithmInterface()
{
	//タイトルシーンマネージャーへ変形
	TitleSceneManager* pTitleSceneManager = TransTitleSceneManager(pSceneManager_);
	//シーン切り替えを呼び出す
	pTitleSceneManager->ChangeScene();
}
/*****************************************************************/
