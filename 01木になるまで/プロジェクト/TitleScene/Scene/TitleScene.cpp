#include "TitleScene.h"								//ヘッダ
#include "../../Engine/GameObject/UIGroup.h"		//UIGroup　シーン内UI群
#include "../../Engine/Scene/SceneUIManager.h"		//シーンのUIマネージャー
#include "../../Engine/Scene/SceneUserInputter.h"	//ユーザー入力受付クラス　キー、マウス入力受け取りと受け取り時の行動呼び出しクラス
#include "../../CommonData/GameDatas.h"				//ゲーム内情報定義

#include "../../Engine/Audio/Audio.h"				//音源再生クラス

#include "TitleSceneManager.h"						//タイトルシーンのマネージャークラス
#include "TitleSceneInputStrategy.h"				//ユーザー入力時の行動実行クラス
/*
	ユーザー入力時の行動実行クラス

	Project/Engine/Scene/SceneUserInputter.hにて、
	シーン内の入力受付デバイスとそのキーコードを登録
	その時に、入力されたときに起こす行動も登録。

	その際の行動クラスが上記のクラスである。


	※各シーンごとに行動クラスの継承元クラスを持ち、行動ごとにオーバーライドする。
	　継承された行動クラスを識別する列挙型のIDも持つ。
	 （上記のIDを継承された行動クラス作成の際に用いる）
*/



//コンストラクタ
//SceneParentのコンストラクタ呼び込み
	//引数：親オブジェクト
	//引数：シーン名のオブジェクト名（シーン識別情報群(Project/CommonDatas/SceneType.h)よりシーンオブジェクト名を取得する（シーンをID識別しオブジェクト名取得））
TitleScene::TitleScene(GameObject * parent)
	: SceneParent::SceneParent(parent, SCENE_OBJECT_NAME[(int)SCENE_ID::SCENE_ID_TITLE]),
	hAudio_(-1)
{
}

//初期化
void TitleScene::Initialize()
{
	//ゲームの背景色を切り替える
		//背景色の変化
	//背景色取得
	GameDatas::SetGameBackGroundColor(BACKGROUND_COLOR_BLACK);



	//タイトルシーンクラスのマネージャー
		//詳細：SceneParentを継承しているため、
		//　　：継承元関数から、シーンマネージャークラスを作成させる
		//制限　　：シーンマネージャークラスは、GameObject型継承のオブジェクト作成関数である、Instantiateでの作成は推奨されない。
		//制限理由：制限をかけるのは、シーンマネージャーが作成されたことを伝えなくてはいけないクラスが存在するためである。
	TitleSceneManager* pSceneManager = 
		(TitleSceneManager*)CreateSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_TITLE);

	//UI作成
	CreateSceneUIGroups(pSceneManager);

	//ユーザー入力の作成
	CreateInitUserInputter(pSceneManager);



	//音楽ロード
		//MainBGMのロード
	hAudio_ = Audio::Load("Assets/Scene/Sound/TitleScene/夢いっぱいの箱.wav");
	//警告
		//ロードできたかの判定
	assert(hAudio_ != -1);
	//音楽のループ設定
	Audio::InfiniteLoop(hAudio_);
	//音楽の再生
	Audio::Play(hAudio_);

}

//更新
void TitleScene::Update()
{
}

//描画
void TitleScene::Draw()
{
}

//解放
void TitleScene::Release()
{
	//音楽停止
	Audio::Stop(hAudio_);
}

//引数UIGroupへ引数パスからロードできる画像を登録
int TitleScene::AddStackUI(UIGroup * pUIGroup, const std::string& FILE_NAME)
{
	//引数UIGroupに追加する
	return pUIGroup->AddStackUI(FILE_NAME);
}

//シーンの常駐UIGroupを作る
void TitleScene::CreateSceneUIGroups(TitleSceneManager * pSceneManager)
{
	//UIGroupの作成

	//所属ロゴUI群
	{
		//UIGroup作成
		//インスタンス生成
		UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

		//UIGroupをSceneMnagerにセットする
		//SPLASH_UI_GROUP_AFFILIATION
		//所属ロゴUI群
		pSceneManager->AddUIGroup((int)TITLE_SCENE_UI_GROUP_TYPE::TITLE_UI_GROUP_TITLE, uiGroup);

		//UI画像
		{
			//背景ロゴ　
			//ロードと追加
			{
				int hBackGround = AddStackUI(uiGroup, "Assets/Scene/Image/TitleScene/Title.png");

				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

				for (int i = 0; i < 2; i++)
				{
					//奥行き設定
						//奥行きを一段階下げる
					pSceneUIManager->LowerOneStep(hBackGround);
				}

				//サイズの可変
				pSceneUIManager->SetPixelScale(hBackGround, 1300, 770);
			}
			//スタートロゴ　
			{
				int hStart = AddStackUI(uiGroup, "Assets/Scene/Image/TitleScene/StartButton.png");

				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();
				//奥行き設定
						//奥行きを一段階下げる
				pSceneUIManager->LowerOneStep(hStart);

				//サイズの可変
				pSceneUIManager->SetPixelScale(hStart, 500, 400);
				//位置の移動
				pSceneUIManager->SetPixelPosition(hStart, 600, 500);
			}
			//タイトル名ロゴ　
			{
				int hTitle = AddStackUI(uiGroup, "Assets/Scene/Image/TitleScene/TitleName.png");

				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

				//サイズの可変
				pSceneUIManager->SetPixelScale(hTitle, 800, 600);
				//位置の移動
				pSceneUIManager->SetPixelPosition(hTitle, 500, 300);
			}
			//マウス左クリックロゴ　
			{
				int hClick = AddStackUI(uiGroup, "Assets/Scene/Image/TitleScene/MouseLeftButtonClick.png");

				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

				//サイズの可変
				pSceneUIManager->SetPixelScale(hClick, 300, 300);
				//位置の移動
				pSceneUIManager->SetPixelPosition(hClick, 1000, 500);
			}

		}
		//UIGroupの描画
		pSceneManager->VisibleSceneUIGroup((int)TITLE_SCENE_UI_GROUP_TYPE::TITLE_UI_GROUP_TITLE);
	}
	
}


//ユーザー入力の作成
void TitleScene::CreateInitUserInputter(TitleSceneManager* pSceneManager)
{
	//ユーザー入力設定
	{
		//ユーザー入力作成用のファクトリークラスの作成
		// タイトルシーン専用であることを確認
		//TitleSceneInputStrategyより
		FactoryForTitleSceneInputStrategy* pFactory = new FactoryForTitleSceneInputStrategy();

		//ユーザー入力クラス作成
		SceneUserInputter* pSceneUserInputter =
			(SceneUserInputter*)Instantiate<SceneUserInputter>(this);

		//入力クラスをゲームマネージャーに登録
		{
			//シーンマネージャーの取得
			//シーンチェンジャーを経由して
			//引数指定のシーンのマネージャークラスを取得
			//pSceneManager = GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_TITLE);
			//シーンマネージャーへ
			//自身のインスタンスを知らせる
			pSceneManager->AddCoopObject((int)TITLE_SCENE_COOP_OBJECTS::TITLE_COOP_GAME_INPUTER, (GameObject*)this);
		}


		//追加
		//マウス入力と入力による行動を追加
		//行動：TITLE_INPUT_ALGORITHM_CHANGE_SCENE
		{
			//入力情報の作成
			CreateInputStrategyInfo* pCreateInfo = new CreateInputStrategyInfo(
				KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_DOWN,
				INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_MOUSE,
				(int)MOUSE_CODE::MOUSE_BUTTON_LEFT);


			//入力追加
			//入力情報と他もろもろの情報を渡す。
			pFactory->CreateTitleSceneInputStrategy(
				TITLE_SCENE_INPUT_ALGORITHM::TITLE_INPUT_ALGORITHM_CHANGE_SCENE,
				pSceneManager,
				pSceneUserInputter,
				pCreateInfo);

			//入力情報の削除
			//SAFE_DELETE(pCreateInfo);
		}

		/*
		//テスト
		//キー入力と入力行動を登録
		//キー入力と入力による行動を追加
		//行動：TITLE_INPUT_ALGORITHM_CHANGE_SCENE
		{
		//入力情報の作成
		CreateInputStrategyInfo* pCreateInfo = new CreateInputStrategyInfo(
		KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_DOWN,
		INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_KEY,
		DIK_RETURN);


		//入力追加
		//入力情報と他もろもろの情報を渡す。
		pFactory->CreateTitleSceneInputStrategy(
		TITLE_SCENE_INPUT_ALGORITHM::TITLE_INPUT_ALGORITHM_CHANGE_SCENE,
		pSceneManager,
		pSceneUserInputter,
		pCreateInfo);

		//入力情報の削除
		//SAFE_DELETE(pCreateInfo);
		}

		//消去
		//登録したキーコードを削除
		//削除をする際は、
		//SceneUserInputterを直接経由して削除する
		//キー入力Returnを削除
		//ただし、キー入力を追加したそのフレームに同時に、消去依頼をすると、
		//まだ、常駐のマップに追加されていないキーを削除しようとするため、エラーとなる

		//消去と追加を同フレームに行われた場合、
		//消去を優先する
		//そのため、フレーム内追加を宣言した場合、次のフレームで追加処理がされるため、保存領域に追加をスタックしておく
		//そのため、追加スタック分からも、消去依頼のキーを消去する
		pSceneUserInputter->RequestSetEmptyKeyCode(DIK_RETURN);
		*/

		//生成クラスの消去
		SAFE_DELETE(pFactory);

	}
}

