#include "TitleSceneManager.h"					//ヘッダ
#include "../../Engine/GameObject/UIGroup.h"	//シーン内画像、ボタンを合わせたUI群
#include "../../Engine/Scene/SceneChanger.h"	//シーン切り替えクラス


//コンストラクタ
TitleSceneManager::TitleSceneManager(GameObject * parent) :
	SceneManagerParent::SceneManagerParent(parent, "TitleSceneManager")
{
}
//デストラクタ
TitleSceneManager::~TitleSceneManager()
{
}

//初期化
void TitleSceneManager::Initialize()
{
	/*シーンマネージャー定義時の必須処理*******************************************************************/
		//詳細：マネージャーとしての機能を持たせるための前提処理、初期化
		//　　：（条件：リソースが存在する場合必須）
		//　　：リソースとは、この場合、連携オブジェクト群における連携オブジェクトが存在するか
		//　　：　　　　　　　　　　　　UIGroup軍におけるUIGroupが存在するか。
		//　　：上記のリソースが一つも存在しない場合に呼び込む必要はない
	
	//連携オブジェクト群のポインタを保存しておく配列の初期化、生成
	//継承先である、自身のクラスで宣言した、連携オブジェクトを指定するEnum値をもとに、連携オブジェクトの数を引数として渡す
	//関数先にて、引数分の連携オブジェクトを入れる枠、要素を確保してくれる
	NewArrayForCoopObjects((int)TITLE_SCENE_COOP_OBJECTS::TITLE_COOP_MAX);
	//UIオブジェクト群のポインタを保存しておく配列の初期化、生成
	NewArrayForUIGroups((int)TITLE_SCENE_UI_GROUP_TYPE::TITLE_UI_GROUP_MAX);

	/************************************************************************************************************/

}
//更新
void TitleSceneManager::Update()
{
}
//描画
void TitleSceneManager::Draw()
{
}
//解放
void TitleSceneManager::Release()
{
	//シーン内連携オブジェクトの解放
		//解放といっても、オブジェクトはGameObject型であるため、Deleteはしない。マネージャークラス内にて管理しているオブジェクトを登録する配列ポインタの解放
	DeleteArrayForCoopObjects();
	//UIオブジェクト群の解放
	DeleteArrayForUIGroups();
}

//シーン管理の常駐UIGroupの個数を返す
int TitleSceneManager::GetUIGroupCount()
{
	return (int)TITLE_SCENE_UI_GROUP_TYPE::TITLE_UI_GROUP_MAX;
}

//シーン切り替え
void TitleSceneManager::ChangeScene()
{
	//シーン切り替え
	SceneChanger* pSceneChanger = (SceneChanger*)FindObject("SceneChanger");
	pSceneChanger->ChangeScene(SCENE_ID::SCENE_ID_GAME);
}
