#include "TestScene.h"		//ヘッダ

//コンストラクタ
//SceneParentのコンストラクタ呼び込み
	//引数：親オブジェクト
	//引数：シーン名のオブジェクト名（シーン識別情報群(Project/CommonDatas/SceneType.h)よりシーンオブジェクト名を取得する（シーンをID識別しオブジェクト名取得））
TestScene::TestScene(GameObject * parent)
	: SceneParent::SceneParent(parent, SCENE_OBJECT_NAME[(int)SCENE_ID::SCENE_ID_TEST])
{
}

//初期化
void TestScene::Initialize()
{
}

//更新
void TestScene::Update()
{
}

//描画
void TestScene::Draw()
{	
}

//解放
void TestScene::Release()
{
}

