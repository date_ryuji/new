#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/SceneParent.h"	//シーンオブジェクト　抽象クラス

/*
	クラス詳細	：テストシーン　
	クラスレベル：サブクラス（スーパークラス（SceneParent））
	クラス概要（詳しく）
				：何もしないシーン
				：シーンのサンプル
				：SceneParent継承
				　SceneParentによって、
				　Sceneに、仮に、SceneManagerが存在した場合、そのSceneManagerの作成、管理をしやすくするための継承元である
				　そして、視覚的にも、シーンは以下のクラスを継承することで、ほかのオブジェクトとは、差分を付けることが可能である

*/
class TestScene : public SceneParent
{
//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	TestScene(GameObject* parent);

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;
};