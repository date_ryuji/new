#include "TestObject.h"		//ヘッダ




//コンストラクタ
//GameObjectのコンストラクタ呼び込み
TestObject::TestObject(GameObject* parent)
	: GameObject(parent, "TestObject")
{
}

//初期化
void TestObject::Initialize()
{
}

//更新
void TestObject::Update()
{
}

//描画
void TestObject::Draw()
{
}

//解放
void TestObject::Release()
{
}