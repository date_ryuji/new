#include "HelpImagesButtonStrategy.h"		//ヘッダ
#include "../../CommonData/GlobalMacro.h"	//共通マクロ
#include "../Scene/GameSceneManager.h"		//ゲームシーンマネージャー
#include "HelpImages.h"						//ヘルプ画像群


/*行動クラス（シーンごとのシーン名HelpImageButtonStrategyを継承したクラス）宣言*******************************************************************************************************/
	//詳細：Factoryクラス（行動クラス生成クラス）にてインスタンスの動的確保を行うため、Factoryの実装よりも前に記述
	//詳細：行動クラスを複数作ることになるため、行動クラスごとにクラスファイルを作成すると、クラスファイル量が多くなる。
	//　　：→上記を避けるために、一つのクラスに行動クラスをまとめる。（メソッドやメンバも少ないため、ひとくくりにしてしまう。）
/*****************************************************************/
/*次へ　アルゴリズム*/
/*HELP_BUTTON_ALGORITHM_NEXT*/
class NextStrategy : public HelpImagesButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//引数：HelpImages
		//戻値：なし
	NextStrategy(HelpImages* pHelpImages);
	
	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*戻る　アルゴリズム*/
/*HELP_BUTTON_ALGORITHM_BACK*/
class BackStrategy : public HelpImagesButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//引数：HelpImages
		//戻値：なし
	BackStrategy(HelpImages* pHelpImages);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*終了　アルゴリズム*/
/*HELP_BUTTON_ALGORITHM_ENDING*/
	//ESCによる、メニュー表示時の終了ボタン
class EndingStrategy : public HelpImagesButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//引数：HelpImages
		//戻値：なし
	EndingStrategy(HelpImages* pHelpImages);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*終了+ゲームシーンに伝える　アルゴリズム*/
/*HELP_BUTTON_ALGORITHM_ENDING_PLUS_TELL_GAME*/
	//ゲームシーンにおける
	//スタート時に表示する終了ボタン
class EndingPlusTellGameStrategy : public HelpImagesButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//引数：HelpImages
		//戻値：なし
	EndingPlusTellGameStrategy(HelpImages* pHelpImages);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/




/*行動クラス生成クラス　実装*******************************************************************************************************/
/*Factory*/
//コンストラクタ
FactoryForHelpImagesButtonStrategy::FactoryForHelpImagesButtonStrategy()
{
}

//ストラテジークラスを生成の委託
//パターン：FactoryMethod
HelpImagesButtonStrategy* FactoryForHelpImagesButtonStrategy::CreateHelpImagesButtonStrategy(
	HELP_IMAGES_BUTTON_ALGORITHM type, HelpImages* pHelpImages)
{
	switch (type)
	{
	case HELP_IMAGES_BUTTON_ALGORITHM::HELP_BUTTON_ALGORITHM_NEXT:
		//インスタンスを生成して
			//その戻値のポインタを、親のクラスでキャストして返す
		return (HelpImagesButtonStrategy*)(new NextStrategy(pHelpImages));
	case HELP_IMAGES_BUTTON_ALGORITHM::HELP_BUTTON_ALGORITHM_BACK:
		return (HelpImagesButtonStrategy*)(new BackStrategy(pHelpImages));
	case HELP_IMAGES_BUTTON_ALGORITHM::HELP_BUTTON_ALGORITHM_ENDING:
		return (HelpImagesButtonStrategy*)(new EndingStrategy(pHelpImages));
	case HELP_IMAGES_BUTTON_ALGORITHM::HELP_BUTTON_ALGORITHM_ENDING_PLUS_TELL_GAME:
		return (HelpImagesButtonStrategy*)(new EndingPlusTellGameStrategy(pHelpImages));

	default:
		return nullptr;
		break;
	};



	return nullptr;
}
/*****************************************************************/

/*シーンごとのユーザー入力時の行動クラス　親クラス　実装*******************************************************************************************************/
//コンストラクタ
HelpImagesButtonStrategy::HelpImagesButtonStrategy(HelpImages* pHelpImages) :
	pHelpImages_(pHelpImages)
{
}
//コンストラクタ
HelpImagesButtonStrategy::HelpImagesButtonStrategy() : 
	pHelpImages_(nullptr)

{
}
/*****************************************************************/


/*行動クラス（シーンごとのシーン名HelpImagesButtonStrategyを継承したクラス）実装*******************************************************************************************************/

/*****************************************************************/
/*次へ　アルゴリズム*/
/*HELP_BUTTON_ALGORITHM_NEXT*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み
NextStrategy::NextStrategy(HelpImages* pHelpImages):
	HelpImagesButtonStrategy::HelpImagesButtonStrategy(pHelpImages)
{
}

//実行行動関数
void NextStrategy::AlgorithmInterface()
{
	//表示画像を次の画像へ切り替える　行動関数を呼び込む
	pHelpImages_->GoNextImage();
}
/*****************************************************************/

/*****************************************************************/
/*戻る　アルゴリズム*/
/*HELP_BUTTON_ALGORITHM_BACK*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み
BackStrategy::BackStrategy(HelpImages* pHelpImages):
	HelpImagesButtonStrategy::HelpImagesButtonStrategy(pHelpImages)
{
}

//実行行動関数
void BackStrategy::AlgorithmInterface()
{
	//表示画像を前の画像へ切り替える　行動関数を呼び込む
	pHelpImages_->GoBackImage();
}
/*****************************************************************/

/*****************************************************************/
/*終了　アルゴリズム*/
/*HELP_BUTTON_ALGORITHM_ENDING*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み
EndingStrategy::EndingStrategy(HelpImages* pHelpImages):
	HelpImagesButtonStrategy::HelpImagesButtonStrategy(pHelpImages)
{
}

//実行行動関数
void EndingStrategy::AlgorithmInterface()
{
	//画像表示を終了
	pHelpImages_->GoEndingImage();
}
/*****************************************************************/

/*****************************************************************/
/*終了＋終了を知らせる　アルゴリズム*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み
EndingPlusTellGameStrategy::EndingPlusTellGameStrategy(HelpImages* pHelpImages):
	HelpImagesButtonStrategy::HelpImagesButtonStrategy(pHelpImages)
{
}

//実行行動関数
void EndingPlusTellGameStrategy::AlgorithmInterface()
{
	//ゲームシーンマネージャーを探す
		//GameObjectの標準機能、シーンのマネージャークラスを探す処理を呼び込み取得する
		//この処理の時点で、戻値にnullptrが返ってくれば、存在しないことになるため、何も行動しない
	GameSceneManager* pSceneMnager = (GameSceneManager*)pHelpImages_->GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);

	//存在したならば
	if (pSceneMnager != nullptr)
	{
		//時間停止終了を呼び込む
		pSceneMnager->EndingDrawHelpImages();
	}

	//画像表示を終了
	pHelpImages_->GoEndingImage();
}
/*****************************************************************/

