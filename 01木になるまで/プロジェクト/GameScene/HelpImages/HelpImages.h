#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス

//クラスのプロトタイプ宣言
class Button;
class UIGroup;
class SceneUIManager;
//enum class のプロトタイプ宣言
enum class HELP_IMAGES_BUTTON_ALGORITHM;

/*
	クラス詳細：ヘルプ画像群を扱うクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：ヘルプ画像（ゲームの操作説明、ゲームルールなどが記述された画像）を読み込み、
				　プレイヤーの操作によって、(画像を進めるなどのボタンを押す)
				  複数枚の画像を切り替え、
				  プレイヤーに画像を参照してもらうヘルプ機能を作るクラス

				：ヘルプ画像と、次へ、戻る、終わるボタンを持たせる
				：ボタンを使い、見るヘルプ画像を動的に変化させる

*/
class HelpImages : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 
	//ロード画像数
	int loadImageNum_;
	//背景画像数
	int backGroundLoadImageNum_;
	//現在参照している画像ハンドル
	int currentImageNum_;


	//UIGroup
		//詳細：HelpImages
	UIGroup* pUIGroup_;

	//UIGroup
		//詳細：BackGround	//背景用
	UIGroup* pBackGround_;

//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	HelpImages(GameObject* parent);

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//ヘルプ画像をロード
		//詳細：ロードとともに、画像をUIGroupに追加する
		//　　：そのため、追加順が、画像の遷移順となる
		//引数：ヘルプ画像ファイル名
		//戻値：なし
	void AddLoadImage(const std::string& FILE_NAME);
	//背景画像をロード
		//引数：背景画像ファイル名
		//戻値：なし
	void AddLoadBackGroundImage(const std::string& FILE_NAME);

	//ボタンを追加
		//詳細：クラス表示中、常に描画され続けるボタンクラス
		//　　：ボタンそれぞれに、機能を持たせる
		//引数：ボタンインスタンス
		//引数：ボタンに持たせる行動タイプ
		//戻値：なし
	void AddButton(Button* pButton , HELP_IMAGES_BUTTON_ALGORITHM type);


	//初期ヘルプ画像の描画
		//引数：なし
		//戻値：なし
	void StartVisibleImage();


	//ヘルプ画像を遷移（次へ）
		//詳細：次へ　ボタン（次への行動を持つボタン）が押下された際に
		//　　；現在表示されているヘルプ画像の次の画像を表示する
		//　　：次の画像がある場合：次の画像へ遷移
		//　　：次の画像がない場合：何もしない
		//引数：なし
		//戻値：なし
	void GoNextImage();


	//ヘルプ画面を遷移（戻る）
		//詳細：戻る　ボタン（戻る　行動を持つボタン）が押下された際に
		//　　：現在表示されているヘルプ画像の前の画像を表示する
		//　　：前の画像がある場合：前の画像へ遷移
		//　　：前の画像がない場合：何もしない
		//引数：なし
		//戻値：なし
	void GoBackImage();

	//ヘルプ画像表示終了（終了）
		//詳細：終了　ボタン（終了の行動を持つボタン）が押下された際に
		//　　：自身のクラスを削除して、ヘルプ画像終了処理を行う
		//引数：なし
		//戻値：なし
	void GoEndingImage();
	
	//引数SceneUIManagerにて管理している全UIのPos,Sizeを引数値にセットする
		//引数：シーンマネージャークラス
		//引数：設置位置X
		//引数：設置位置Y
		//戻値：なし
	void SetAllPos(SceneUIManager* pSceneUIManager, int x, int y);
		//引数：シーンマネージャークラス
		//引数：設置サイズX
		//引数：設置サイズY
		//戻値：なし
	void SetAllSize(SceneUIManager* pSceneUIManager, int x, int y);
public : 
	//HelpImage全体の画像の配置位置を設定
		//引数：設置X
		//引数：設置Y
	void SetPosition(int x, int y);
	//HelpImage全体の画像のサイズを設定
		//引数：サイズX
		//引数：サイズY
	void SetSize(int x, int y);
	
	//HelpImage背景画像の配置位置を設定
		//引数：設置X
		//引数：設置Y
	void SetBackGroundPosition(int x, int y);
	
	//HelpImage背景画像のサイズを設定
		//引数：設置X
		//引数：設置Y
	void SetBackGroundSize(int x, int y);

	//UIGroup（HelpImages）を取得
		//引数：なし
		//戻値：UIGroup（HelpImages）
	UIGroup* GetUIGroup();
	//UIGroup（背景用）を取得
		//引数：なし
		//戻値：UIGroup（BackGround）
	UIGroup* GetBackGroundUIGroup();


};