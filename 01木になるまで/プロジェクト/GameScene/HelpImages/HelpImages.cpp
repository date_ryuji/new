#include "HelpImages.h"									//ヘッダ
#include "../../CommonData/GlobalMacro.h"				//共通マクロ
#include "../../Engine/Button/Button.h"					//ボタンクラス
#include "../../Engine/GameObject/UIGroup.h"			//UIGroup群
#include "../../Engine/Scene/SceneButtonManager.h"		//シーンボタンマネージャー
#include "../../Engine/Scene/SceneUIManager.h"			//シーンUIマネージャー
#include "HelpImagesButtonStrategy.h"					//HelpImageのボタン押下時の行動をまとめた行動クラス


//コンストラクタ
HelpImages::HelpImages(GameObject* parent)
	: GameObject(parent, "HelpImages"),
	pUIGroup_(nullptr),
	pBackGround_(nullptr),

	loadImageNum_(0),
	currentImageNum_(0),
	backGroundLoadImageNum_(0)
{
}

//初期化
void HelpImages::Initialize()
{
	//UIGroup作成
		//インスタンス生成
	//HelpImageのヘルプ用画像群
	pUIGroup_ = (UIGroup*)Instantiate<UIGroup>(this);
	//HelpImageの背景用画像群
	pBackGround_ = (UIGroup*)Instantiate<UIGroup>(this);
}

//更新
void HelpImages::Update()
{
}

//描画
void HelpImages::Draw()
{
}

//解放
void HelpImages::Release()
{
}

//HelpImageの画像追加
void HelpImages::AddLoadImage(const std::string& FILE_NAME)
{
	//画像をロード
	// 	   現在所有する画像数をカウントする
	loadImageNum_++;

	//UIGroupに引数画像名にて　画像UIを追加する
	pUIGroup_->AddStackUI(FILE_NAME);
}

//HelpImageの背景用画像追加
void HelpImages::AddLoadBackGroundImage(const std::string& FILE_NAME)
{
	//UIGroupに引数画像名にて　画像UIを追加する
	pBackGround_->AddStackUI(FILE_NAME);

	//描画
		//背景は常に描画し続ける
	pBackGround_->VisibleSceneUI(backGroundLoadImageNum_);
	//前後関係を作る
		//背景用なので、後ろへ下げる
	SceneUIManager* pUIManger = pBackGround_->GetSceneUIManager();
	pUIManger->LowerOneStep(backGroundLoadImageNum_);


	//背景画像数をカウント
	backGroundLoadImageNum_++;
}

//ボタン追加
void HelpImages::AddButton(Button* pButton, HELP_IMAGES_BUTTON_ALGORITHM type)
{

	////ボタンクラスごとの
	////ボタンが押下されたときの、
	////行動アルゴリズムを取得する

	//ボタン押下時のアルゴリズムを所有している、
	//アルゴリズム作成クラス
	FactoryForHelpImagesButtonStrategy* pFactory = new FactoryForHelpImagesButtonStrategy;

	//ボタン押下時の行動をセットする
	{
		//作成
		HelpImagesButtonStrategy* pButtonStrategy = pFactory->
			CreateHelpImagesButtonStrategy(type, this);

		//UIGroupから経由して、
		//UIGroupの所有するボタンクラス群へ
		//インスタンスをセットする
		//ボタンのインスタンスと、行動ストラテジーのアルゴリズム
		pUIGroup_->AddStackButton(pButton, pButtonStrategy);
	}

	//削除
	SAFE_DELETE(pFactory);

	//ButtonManagerを取得
	SceneButtonManager* pSceneButtonManager = pUIGroup_->GetSceneButtonManager();

	//ボタンマネージャーを
		//ボタンクラスにセットさせる
	pButton->SetSceneButtonManager(pSceneButtonManager);


	//★pSceneButtonManager->AddStackを終えたのちに描画、非描画を行う必要がある
	//ボタン非描画
	//デフォルトにて非描画
	pSceneButtonManager->InvisibleSceneButton(pButton);

}

//初期ヘルプ画像の描画
void HelpImages::StartVisibleImage()
{
	//ヘルプ画像1枚目の描画
	pUIGroup_->VisibleSceneUI(currentImageNum_);

	//ボタンの全描画
	pUIGroup_->VisibleInvertSceneButton();

}

//ヘルプ画像を遷移（次へ）
void HelpImages::GoNextImage()
{
	//画像番号をカウントアップ後の番号が
	//	存在するならばカウントアップ
	//条件：画像番号＋１　が　ロード画像合計数と同じならば
	if (currentImageNum_ + 1 == loadImageNum_)
	{
		//遷移できる画像がないため
		//何もしない
		return;
	}

	//現在の画像を非描画
	pUIGroup_->InvisibleSceneUI(currentImageNum_);

	//画像番号をカウントアップ
	currentImageNum_++;

	//カウントアップ後の画像を描画
	pUIGroup_->VisibleSceneUI(currentImageNum_);
}

//ヘルプ画面を遷移（戻る）
void HelpImages::GoBackImage()
{
	//次への逆

	//画像番号をカウントダウン後の番号が
	//	存在するならばカウントダウン
	//条件：画像番号 - 1　が　ー１と同じならば
	if (currentImageNum_ - 1 == -1)
	{
		//遷移できる画像がないため
		//何もしない
		return;
	}

	//現在の画像を非描画
	pUIGroup_->InvisibleSceneUI(currentImageNum_);


	//画像番号をカウントアップ
	currentImageNum_--;

	//カウントアップ後の画像を描画
	pUIGroup_->VisibleSceneUI(currentImageNum_);


}

//ヘルプ画像表示終了（終了）
void HelpImages::GoEndingImage()
{
	//UiGroupの削除
		//GameObject型であるため、
		//自身で消去処理を行う必要なし

	//自身を消去
	this->KillMe();
}

//HelpImage全体の画像の配置位置を設定
void HelpImages::SetPosition(int x, int y)
{
	//SceneUImanagerを取得し
		//位置を変更
	SceneUIManager* pSceneUIManager = pUIGroup_->GetSceneUIManager();
	
	SetAllPos(pSceneUIManager, x, y);

}

//HelpImage全体の画像のサイズを設定
void HelpImages::SetSize(int x, int y)
{
	//SceneUImanagerを取得し
		//サイズを変更
	SceneUIManager* pSceneUIManager = pUIGroup_->GetSceneUIManager();

	SetAllSize(pSceneUIManager, x, y);

}

//HelpImage背景画像の配置位置を設定
void HelpImages::SetBackGroundPosition(int x, int y)
{
	//SceneUImanagerを取得し
		//位置を変更
	SceneUIManager* pSceneUIManager = pBackGround_->GetSceneUIManager();

	SetAllPos(pSceneUIManager, x, y);
}

//HelpImage背景画像のサイズを設定
void HelpImages::SetBackGroundSize(int x, int y)
{
	//SceneUImanagerを取得し
	//サイズを変更
	SceneUIManager* pSceneUIManager = pBackGround_->GetSceneUIManager();

	SetAllSize(pSceneUIManager, x, y);

}

//引数SceneUIManagerにて管理している全UIのSizeを引数値にセットする
void HelpImages::SetAllSize(SceneUIManager* pSceneUIManager, int x, int y)
{
	int max = pSceneUIManager->GetUINum();
	for (int i = 0; i < max; i++)
	{
		pSceneUIManager->SetPixelScale(i, x, y);
	}
}

//引数SceneUIManagerにて管理している全UIのPosを引数値にセットする
void HelpImages::SetAllPos(SceneUIManager* pSceneUIManager, int x, int y)
{
	int max = pSceneUIManager->GetUINum();
	for (int i = 0; i < max; i++)
	{
		pSceneUIManager->SetPixelPosition(i, x, y);
	}
}

//UIGroupの取得
UIGroup* HelpImages::GetUIGroup()
{
	return pUIGroup_;
}
//背景用UIGroupの取得
UIGroup* HelpImages::GetBackGroundUIGroup()
{
	return pBackGround_;
}

