#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/ButtonStrategyParent.h"	//ボタン入力行動ストラテジークラス　抽象クラス

//クラスのプロトタイプ宣言
class HelpImages;
class HelpImagesButtonStrategy;

/*
	//ヘルプ画像
	//種類：
	//次へ：次の画像へ遷移するHelpImagesのクラスの関数を呼び込む
	//戻る：上記の戻るVer
	//終了：HelpImagesのクラスを削除する

*/
//HelpImagesのボタン　押下時のアルゴリズム
	//詳細：ボタン（UI画像へ　マウスクリックの判定を付けたクラス）
	//　　：押下際の行動アルゴリズムを記述したクラスを識別するタイプ
enum class HELP_IMAGES_BUTTON_ALGORITHM
{
	HELP_BUTTON_ALGORITHM_NEXT = 0,		//HelpImage画像次へ
	HELP_BUTTON_ALGORITHM_BACK ,		//HelpImage画像戻る
	HELP_BUTTON_ALGORITHM_ENDING ,		//HelpImage画像終了(HelpImagesのインスタンス削除)
	HELP_BUTTON_ALGORITHM_ENDING_PLUS_TELL_GAME,	//HelpImage画像終了＋ゲームシーンマネージャーに終了を伝える
													//GameSceneMnager存在時　限定
	HELP_BUTTON_ALGORITHM_MAX ,			//MAX

};



/*
	クラス詳細	：ボタン入力を行った際の行動クラス　作成クラス（シーン専用）
	使用デザインパターン：FactoryMethod
	使用クラス	：GameScene->HelpImages
	クラス概要（詳しく）
				：HELP_IMAGES_BUTTON_ALGORITHMに登録した行動アルゴリズムタイプにて識別し、
				　専用クラスを作成する

*/
class FactoryForHelpImagesButtonStrategy
{
public:
	//コンストラクタ
		//引数：なし
		//戻値：なし
	FactoryForHelpImagesButtonStrategy();

	//行動クラスのインスタンスを生成する関数
		//詳細：引数タイプ（HELP_IAMGES_BUTTON_ALGORITHM(enum)）にて生成クラスを識別し、行動クラスのインスタンスを作成、インスタンスを返す
		//　　：親である、行動クラス生成の実行を呼び出す
		//　　：以下関数の中で、
		//　　：行動クラスの作成
		//引数：行動アルゴリズム識別タイプ（HELP_IAMGES_BUTTON_ALGORITHM(enum)）
		//引数：シーンマネージャー（行動アルゴリズムにて各シーンオブジェクトの連携が必要である場合、シーンマネージャーを利用するため、シーンマネージャーに呼び込むメソッドを用意しておく。そのメソッド呼び込みのために取得）
		//戻値：行動クラスのインスタンス
	HelpImagesButtonStrategy* CreateHelpImagesButtonStrategy(HELP_IMAGES_BUTTON_ALGORITHM type, HelpImages* pHelpImages);
};



/*
	クラス詳細	：ボタン入力時の実行行動を実装した行動ストラテジークラス
	使用クラス	：GameScene->HelpImages
	クラスレベル：スーパークラス（スーパークラス（ButtonStrategyParent））
	クラス概要（詳しく）
				：行動実行が確定した場合、行動アルゴリズムを記述するメソッドを持つクラス
				　継承先においても、メソッドをオーバーライドすることで、各継承先で個別の処理を記述可能。
				  ボタン入力時の行動を変えることができる


*/
class HelpImagesButtonStrategy : public ButtonStrategyParent
{
//protected メンバ変数、ポインタ、配列
protected:

	//行動時の行動呼び出し先の関数を持つのポインタ
	HelpImages* pHelpImages_;

//public メソッド
public:

	//コンストラクタ
		//引数：なし
		//戻値：なし
	HelpImagesButtonStrategy();

	//コンストラクタ
		//詳細：ヘルプ画像のマネージャーの取得
		//引数：HelpImages
		//戻値：なし
	HelpImagesButtonStrategy(HelpImages* pHelpImages);


	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void AlgorithmInterface() = 0;
};

