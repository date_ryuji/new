#include "MoveButton.h"	//ヘッダ


//コンストラクタ
MoveButton::MoveButton(GameObject * parent) :
	Button(parent, "StartButton"),
	SPEED_X_(2),
	SPEED_Y_(4),
	codeX_(1),
	codeY_(1),
	currentX_(0),
	currentY_(0)
{
}

//デストラクタ
MoveButton::~MoveButton()
{
}

//初期化
void MoveButton::Initialize()
{
	//ボタンのロード
	//ON時のボタン画像ロード
	Load("Assets/SampleScene/MoveButton_ON.png", BUTTON_STATUS::BUTTON_STATUS_ON);
	//OFF時のボタン画像ロード
	Load("Assets/SampleScene/MoveButton_OFF.png", BUTTON_STATUS::BUTTON_STATUS_OFF);

	//ボタン位置移動
	SetPixelPosition(currentX_, currentY_);

}
//更新
void MoveButton::Update()
{
	//独自のUpdate
		//今回におけるボタンが移動するような場合は、別途ボタンクラスを作成する必要がある

	//ボタンの接触判定
	UpdateButtonForOnContact();

	//ボタン移動用の変数
	int minX = 0 , minY = 0;
	int maxX = 500 , maxY = 300;

	//移動
	//条件：X座標が最小値より小さい
	//　　：||
	//　　：X座標が最大値より大きい
	if (currentX_ < minX || currentX_ > maxX)
	{
		//反転
		codeX_ *= -1;
	}
	//条件：Y座標が最小値より小さい
	//　　：||
	//　　：Y座標が最大値より大きい
	if (currentY_ < minY || currentY_ > maxY)
	{
		//反転
		codeY_ *= -1;
	}

	//移動値計算
	currentX_ += SPEED_X_ * codeX_;
	currentY_ += SPEED_Y_ * codeY_;

	//ボタン位置移動
	SetPixelPosition(currentX_, currentY_);
}

//描画
void MoveButton::Draw()
{
	//ボタン描画
	DrawButton();
}

//解放
void MoveButton::Release()
{
	//ボタンクラスの動的確保した要素を解放
	ReleaseButton();
}

//ボタン押下時の処理
void MoveButton::OnContactButton()
{
	//ボタンの押下
	//ButtonPress();

	//スイッチ式ON,OFF
	//押した瞬間に一回実行
	ButtonPressOneDown();

	//スイッチ式ON,OFF
	//離した瞬間に一回実行
	//ButtonPressOneUp();

}