#pragma once
//システム内　標準ヘッダ(usr/)
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス


//クラスのプロトタイプ宣言
class GameSceneManager;




//ナンバープレートを適用させるオブジェクト
	//詳細：オブジェクトごとの配置位置の微調整に使用する
enum class NUMBER_PLATE_TYPE
{
	NUMBER_PLATE_TREE = 0,	//木
	NUMBER_PLATE_ENEMY ,	//敵

	NUMBER_PLATE_MAX,		//MAX
};


/*
	クラス詳細：ナンバープレートクラス　該当番号を常に表示し続けるクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：親オブジェクトを原点として、
				　常に一定の範囲離れながら、常にプレイヤーの方向を向き続けるプレート

				　該当番号から示される番号を常に描画し続ける

				　各敵、各木の連番番号を視認できるように作られる

*/
class NumberPlate : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 

	//あくまで平面の”ポリゴンモデル”
	//平面（枠）（番号を描画する枠）
		//のモデルハンドル
	int hFrame_;

	//番号の種別
		//詳細：木の番号であれば、：木
		//　　：敵の番号であれば、：敵　と表示　モデルハンドル
	int hType_;

	//ゲームシーンマネージャー
		//詳細：プレイヤーの位置を知っているクラス
		//　　：どこにいるかわからないプレイヤークラスのポインタをFind（走査）するより、シーンマネージャーから直接聞いた方が速いと考えた
	GameSceneManager* pSceneManager_;

	//番号描画のための画像へのハンドル
		//詳細：番号は一桁であるとは限らない
		//　　：その場合、2桁ならば、２つの平面を用意し、１つを１の位、１つを１０の位として使用させる
		//　　：そのため、ハンドル数は可変である必要がある
	std::vector<int> hNumbers_;
		
//private メソッド
private : 

	//番号をロード
		//引数：ロード番号
		//戻値：なし
	void LoadNumber(int number);
	//自身をプレイヤーの方向をもとに回転させる
		//引数：なし
		//戻値：なし
	void UpdateRotate();


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	NumberPlate(GameObject* parent);

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;
//public メソッド
public:
	
	//初期の番号のロード
		//詳細：番号の種別を受け取り、描画のモデルを確保する
		//引数：描画オブジェクトのタイプ
		//引数：番号
		//戻値：なし
	void LoadNumberAndType(NUMBER_PLATE_TYPE type  , int number);

	//番号の更新
		//引数：更新　描画番号　
		//戻値：なし
	void UpdateNumber(int number);

};