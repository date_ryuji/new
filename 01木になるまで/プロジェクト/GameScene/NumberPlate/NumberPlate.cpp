#include "NumberPlate.h"						//ヘッダ
#include "../../Engine/DirectX/Direct3D.h"		//Direct3D シェーダー取得
#include "../../Engine/Model/Model.h"			//モデル
#include "../../Engine/Algorithm/Math.h"		//ベクトルのなす角の計算を持っている名前空間
#include "../Scene/GameSceneManager.h"			//ゲームシーンマネージャー


//コンストラクタ
NumberPlate::NumberPlate(GameObject* parent)
	: GameObject(parent, "NumberPlate"),
	hFrame_(-1),
	hType_(-1),
	pSceneManager_(nullptr)
{
	//可変配列のクリア
	hNumbers_.clear();
}

//初期化
void NumberPlate::Initialize()
{
	//ロード
	//枠の確保
		//番号が何番であろうと、
		//タイプが何であろうと、枠はかわらない
	hFrame_ = Model::Load("Assets/Scene/Image/GameScene/NumberPlate/Frame.png" , POLYGON_GROUP_TYPE::PLANE_POLYGON_GROUP , SHADER_TYPE::SHADER_PLANE);
	//警告
	assert(hFrame_ != -1);


	//初期サイズの可変
	transform_.scale_ = XMVectorSet(1.0f, 0.5f, 1.0f, 0.f);
	

	//シーンマネージャー取得
		//現在確保されているシーンマネージャーが、引数指定のシーンマネージャーであれば、取得可能
	pSceneManager_ = (GameSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);

}


//更新
void NumberPlate::Update()
{
	//常にプレイヤーの方向を向くように回転
	UpdateRotate();
}


//描画
void NumberPlate::Draw()
{
	Transform transform = transform_;
	//初期値回転量の加算
		//ーZ方向を標準とするため、180度プラスで加算させる(+Z方向に向いていたものを、ーZ方向に向かせる)
	transform.rotate_.vecY += 180.f;

	//現在の回転量から
		//奥行きの方向を求める
		//初期段階ではZ方向の値を変動して、画像の奥行きを管理していたが、
			//画像が回転をすると、奥行きの方向がZ方向ではなくなってくる。そのため、奥行きの移動方向を変えるために、現在の回転量から、奥行き方向を求める（本来の奥行き方向のベクトルを、現在の回転量をもとにした回転行列で掛ける）
	XMVECTOR defaultDirZ = XMVectorSet(0.f, 0.f, -1.f, 0.f);
	XMVECTOR defaultDirX = XMVectorSet(-1.f, 0.f, 0.f, 0.f);

	//Y軸の回転量からY軸の回転行列を取得
	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(transform.rotate_.vecY));

	//回転後の位置
		//現在の回転量を求めて、
		//回転したときのベクトル位置を求める
	defaultDirZ = XMVector3TransformCoord(defaultDirZ, matY);
	defaultDirX = XMVector3TransformCoord(defaultDirX, matY);

	//描画
	Model::SetTransform(hFrame_, transform);
	Model::Draw(hFrame_);

	//条件：タイプが‐1でない
		if (hType_ != -1)
		{
			//サイズ設定
			transform.scale_ = XMVectorSet(0.5f, 0.5f, 0.5f, 0.f);

			//奥行を少し上げる
			transform.position_ += defaultDirZ * 0.05f;
			transform.position_ += defaultDirX * 1.f;

			//描画
			Model::SetTransform(hType_, transform);
			Model::Draw(hType_);

		}
		//条件：ナンバープレートが1つ以上
		if (hNumbers_.size() > 0)
		{
			//ナンバープレートのサイズを指定
				//ナンバープレートの数（番号の桁数）によって、サイズを可変
				//1つの場合左記（0.3f, 0.3f, 0.3f, 0.f）を1で掛ける
				//2つの場合左記（0.3f, 0.3f, 0.3f, 0.f）を2で掛ける
					//番号の数に応じてサイズを縮小する。縮小をしなければ、描画の際に、ナンバープレートが被ってしまう。
			transform.scale_ = XMVectorSet(0.3f, 0.3f, 0.3f, 0.f) / (float)hNumbers_.size();


			//奥行を少し上げる
			//transform.position_.vecZ -= 0.05f;
			transform.position_ += defaultDirZ * 0.05f;
			//初期X
			//transform.position_.vecX += 1.75f;
			transform.position_ += defaultDirX * -1.75f;


			//１の位が、一番最後に登録されているので、	
				//１の位から取得
			//初期値：番号群
			//条件：0以下
			//増減：ー１
			for (int i = (int)hNumbers_.size() - 1; i >= 0; i--)
			{
			
				
				Transform trans = transform;
				//設置位置の調整
				//trans.position_.vecX = trans.position_.vecX + (0.1f * i);

				//描画
				Model::SetTransform(hNumbers_[i], trans);
				Model::Draw(hNumbers_[i]);

				//次の値をーX方向へずらす
				//transform.position_.vecX += 0.25f;
				transform.position_ += defaultDirX * 0.25f;
			}

			

		}
}

//解放
void NumberPlate::Release()
{
}



//常にプレイヤーの方向を向くように回転
void NumberPlate::UpdateRotate()
{
	//プレイヤーのワールド座標を取得
		//ワールド座標であることに注意
		//GameObjectから、アクセスできるTransform　はあくまでも親からのローカル座標である
	XMVECTOR playerWorldPos = pSceneManager_->GetPlayerWorldPos();

	//内積を求める（２つのベクトルによる、一番近い内積（小さい角度のほうの内積））
		//内積の計算では、角度を求めて、角度の小さいほうを出す
		//そのため、回転量を固定値でセットしようとすると、本来は、270度位の位置なのに、80度の回転になってしまう。
		//上記を避けるために、常に小さい角度の回転でも済むように、

		//現在の方向を、現在の回転量をもとに求める。
		//現在の回転をY軸回転で回転。→そのうえで、現在向いている方向のベクトルを求められる。そのベクトルと、位置ベクトルから、プレイヤーの位置ベクトルまでの差のなす角を求める

		//�@へ



	//現在の移動方向をもとに、
	//移動方向へ体を回転させたい

	//�@デフォルトの方向。（０，０，-１）
	//�A進行方向。（ｘ，ｙ，ｚ）＜現在のベクトルからプレイヤーのベクトルまでの差ベクトル＞
	//�B上記のベクトルの単位ベクトルを出し、�@と�Aでなす角を求める
	//�Cその角度を、transform_.roate.yにセットする


	//�@
	XMVECTOR defaultDir = XMVectorSet(0.f, 0.f, -1.f, 0.f);
	////Y軸の回転量からY軸の回転行列を取得
	//XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));

	////回転後の位置
	//	//現在の回転量を求めて、
	//	//回転したときのベクトル位置を求める
	//defaultDir = XMVector3TransformCoord(defaultDir, matY);
	//defaultDir = XMVector3Normalize(defaultDir);

	//親のY軸回転分、ベクトルを回転させる
		//自身の中の回転だけであれば、考慮する必要はないが、
		//ワールド行列の、なにも回転を受けていない状態での回転量にしてしまっているので、親が回転してしまったら、ずれが生じてしまう
	//Y軸の回転量からY軸の回転行列を取得
	pParent_->transform_.Calclation();
	XMMATRIX mat = pParent_->transform_.matRotate_;
	//mat *= matY;


	//回転後の位置
		//現在の回転量を求めて、
		//回転したときのベクトル位置を求める
	defaultDir = XMVector3TransformCoord(defaultDir, mat);
	defaultDir = XMVector3Normalize(defaultDir);




	//�A
		//最終の目的位置 - 現在位置 = 現在位置から目的位置までのベクトル
		//上記の正規化
	XMVECTOR myWorldPos = this->GetWorldPos();
	//Y座標をそろえる
	myWorldPos.vecY = 0.f;
	playerWorldPos.vecY = 0.f;

	XMVECTOR moveDir = XMVector3Normalize(myWorldPos - playerWorldPos);

	//�B　�C
	//なす角を求める計算を外部に任せる
	transform_.rotate_.vecY = Math::GetAngleWithin360Degree(moveDir, defaultDir, NORMAL_DIR::NORMAL_DIR_Y);

}


//初期の番号のロード
void NumberPlate::LoadNumberAndType(NUMBER_PLATE_TYPE type, int number)
{
	//親のTransformによって、任意の位置にセットするには、各々位置を調整する必要がある
		//その調整値
		//（子供は、親のTransformをうけて、自身のローカルTransformで位置を調整する。そのため、子供Aの親がScale(1.f,1.f,1.f)の時の子供Ａのサイズと。子供Ｂの親がScale(5.f,5.f,5.f)の時の子供Bのサイズでは子供のScaleが同じでもサイズは違う）


	//初期位置
	const XMVECTOR INIT_POS[(int)NUMBER_PLATE_TYPE::NUMBER_PLATE_MAX] =
	{
		//木
		XMVectorSet(5.0f, 5.0f, -5.0f, 0.f) , 
		//敵
		XMVectorSet(3.0f, 2.0f, -3.0f, 0.f) 

	};
	transform_.position_ = INIT_POS[(int)type];
	//初期サイズ
	const XMVECTOR INIT_SCALE[(int)NUMBER_PLATE_TYPE::NUMBER_PLATE_MAX] =
	{
		//木
		XMVectorSet(2.f,1.0f , 1.f, 0.f),
		//敵
		XMVectorSet(1.f,0.5f , 1.f, 0.f),
	};
	transform_.scale_ = INIT_SCALE[(int)type];



	//引数タイプによって、
	//番号の種類により選択されるロゴを判別する
	const std::string TYPE_FILE_NAME[(int)NUMBER_PLATE_TYPE::NUMBER_PLATE_MAX] =
	{
		//木
		"Assets/Scene/Image/GameScene/GameMap/Logo_DefaultTree.png",
		//敵
		"Assets/Scene/Image/GameScene/GameMap/Logo_Ghost.png",

	};

	//タイプのロード
	hType_ = Model::Load(TYPE_FILE_NAME[(int)type], 
		POLYGON_GROUP_TYPE::PLANE_POLYGON_GROUP, SHADER_TYPE::SHADER_PLANE);
	//警告
	assert(hType_ != -1);


	//番号ロード
	LoadNumber(number);

}

//番号の更新
void NumberPlate::UpdateNumber(int number)
{
	//現在の番号を解放
	hNumbers_.clear();

	//番号を更新
	LoadNumber(number);
}

//番号をロード
void NumberPlate::LoadNumber(int number)
{
	//引数番号が
	//０より小さくない判断する
	if (number < 0)
	{
		//０より小さいとき処理終了
		return;
	}


	//ファイルパス
	const std::string NUMBER_FILE_NAME_ROOT = "Assets/Scene/Image/GameScene/NumberPlate/Number";
	const std::string EXTENSION = ".png";

	//引数にてもらった
	//番号を１の位、１０の位、１００の位でそれぞれ値を登録する

	//100の位
	int targetNumber = 100;
	if (number >= targetNumber)
	{
		std::string place100 = std::to_string(number / targetNumber);

		hNumbers_.push_back(Model::Load(NUMBER_FILE_NAME_ROOT + place100 + EXTENSION, POLYGON_GROUP_TYPE::PLANE_POLYGON_GROUP, SHADER_TYPE::SHADER_PLANE));
		assert(hNumbers_[hNumbers_.size() - 1] != -1);

		number -= targetNumber;
	}
	//10の位
	targetNumber = 10;
	if (number >= targetNumber)
	{
		std::string place10 = std::to_string(number / targetNumber);

		hNumbers_.push_back(Model::Load(NUMBER_FILE_NAME_ROOT + place10 + EXTENSION, POLYGON_GROUP_TYPE::PLANE_POLYGON_GROUP, SHADER_TYPE::SHADER_PLANE));
		assert(hNumbers_[hNumbers_.size() - 1] != -1);

		number -= targetNumber;
	}
	//1の位
	{
		std::string place1 = std::to_string(number % 10);

		hNumbers_.push_back(Model::Load(NUMBER_FILE_NAME_ROOT + place1 + EXTENSION, POLYGON_GROUP_TYPE::PLANE_POLYGON_GROUP, SHADER_TYPE::SHADER_PLANE));
		assert(hNumbers_[hNumbers_.size() - 1] != -1);
	}

}

