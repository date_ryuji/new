#include "CountTimer.h"							//ヘッダ
#include "../../Engine/Algorithm/Timer.h"		//フレーム時間計測クラス
#include "../../CommonData/GlobalMacro.h"		//共通マクロ


//コンストラクタ
CountTimer::CountTimer(GameObject* parent) : 
	GameObject::GameObject(parent , "CountTimer")
{
	//可変配列のクリア
	pTimers_.clear();
}

//初期化
void CountTimer::Initialize()
{
}
//更新
void CountTimer::Update()
{
	//基本的にカウントアップ、カウントダウンのどちらも計算する
		//そのため、きちんと計算開始を行うには、まず、スタートの関数を呼び出す必要がある、

	//複数計測に対応しているため、
		//それぞれの計測を行う
	//条件：登録したタイマー数
	for (int i = 0; i < pTimers_.size(); i++)
	{
		//条件：タイマーの計測許可されているか
		if (pTimers_[i]->permit)
		{
			//タイマーの計測開始


			//タイマー：カウントアップ
			//条件：経過時間が終了時間以下
			if (pTimers_[i]->endTimeForCountUp >= pTimers_[i]->elpasedTime)
			{
				//デルタタイム
					//フレームの経過時間を計測
				pTimers_[i]->elpasedTime += Timer::GetDelta();
			}
			else
			{
				//終了時間で更新
				pTimers_[i]->elpasedTime = pTimers_[i]->endTimeForCountUp;
			}



			//タイマー：カウントダウン
			//条件：残り時間が終了時間以上
			if (pTimers_[i]->endTimeForCountDown <= pTimers_[i]->remaingTime)
			{
				//デルタタイム
					//フレームの経過時間を計測
				pTimers_[i]->remaingTime -= Timer::GetDelta();
			}
			else
			{
				//終了時間で更新
				pTimers_[i]->remaingTime = pTimers_[i]->endTimeForCountDown;
			}

		}

	}

}
//描画
void CountTimer::Draw()
{
}
//解放
void CountTimer::Release()
{
	//条件：登録したタイマー数
	for (int i = 0; i < pTimers_.size(); i++)
	{
		//解放
		SAFE_DELETE(pTimers_[i]);
	}
}

//タイマーの計測開始
//タイマー：カウントダウン
int CountTimer::StartTimerForCountDown(float startTime, float endTime)
{
	//新規タイマーの作成
	TimerInfo* pNewTimer = new TimerInfo;

	pNewTimer->remaingTime = startTime;
	pNewTimer->endTimeForCountDown = endTime;


	//リストに登録して、
	pTimers_.push_back(pNewTimer);
	//リストの要素数 - 1 = にて、タイマーへのハンドルを返す
	return (int)pTimers_.size() - 1;

}
//タイマーの計測開始
//タイマー：カウントアップ
int CountTimer::StartTimerForCountUp(float startTime, float endTime)
{
	//新規タイマーの作成
	TimerInfo* pNewTimer = new TimerInfo;

	pNewTimer->elpasedTime = startTime;
	pNewTimer->endTimeForCountUp = endTime;


	//リストに登録して、
	pTimers_.push_back(pNewTimer);
	//リストの要素数 - 1 = にて、タイマーへのハンドルを返す
	return (int)pTimers_.size() - 1;
}

//タイマーの計測再計測
//タイマー：カウントダウン
void CountTimer::ReStartTimerForCountDown(int handle, float startTime, float endTime)
{
	//ハンドル番号から、
		//時間をセットしなおす
		//リセットとは違い、リセットは、すべての要素を０にするが、
		//ReStartは、計測のし直しになる。（最初から、カウントアップ、カウントダウンを行う）

	pTimers_[handle]->remaingTime = startTime;
	pTimers_[handle]->endTimeForCountDown = endTime;

	//自フレームから、
		//タイマーをリスタートして、計測を開始することが可能となる

}

//タイマーの計測再計測
//タイマー：カウントアップ
void CountTimer::ReStartTimerForCountUp(int handle, float startTime, float endTime)
{
	pTimers_[handle]->elpasedTime = startTime;
	pTimers_[handle]->endTimeForCountUp = endTime;
}

//指定タイマーの残り時間取得
//タイマー：カウントダウン
float CountTimer::GetRemaingTimer(int handle)
{
	return pTimers_[handle]->remaingTime;
}

//指定タイマーの経過時間取得
//タイマー：カウントアップ
float CountTimer::GetElpasedTime(int handle)
{
	return pTimers_[handle]->elpasedTime;
}

//指定タイマーの現在の終了時間を取得
//タイマー：カウントダウン
float CountTimer::GetEndTimerForCountDown(int handle)
{
	return pTimers_[handle]->endTimeForCountDown;
}

//指定タイマーの現在の経過時間を取得
//タイマー：カウントアップ
float CountTimer::GetEndTimerForCountUp(int handle)
{
	return pTimers_[handle]->endTimeForCountUp;
}

//指定タイマーの計測の終了したかを判断
//タイマー：カウントダウン
bool CountTimer::EndOfTimerForCountDown(int handle)
{
	return pTimers_[handle]->remaingTime <= pTimers_[handle]->endTimeForCountDown;
}

//指定タイマーの計測の終了したかを判断
//タイマー：カウントアップ
bool CountTimer::EndOfTimerForCountUp(int handle)
{
	return pTimers_[handle]->elpasedTime >= pTimers_[handle]->endTimeForCountUp;
}

//タイマー計測拒否
void CountTimer::NotPermittedTimer(int handle)
{
	pTimers_[handle]->permit = false;
}

//タイマー計測許可
void CountTimer::PermitTimer(int handle)
{
	pTimers_[handle]->permit = true;
}

//タイマーの計測許可されているか
bool CountTimer::IsPermitTimer(int handle)
{
	return pTimers_[handle]->permit;
}

//指定タイマーのリセット
void CountTimer::ResetTimer(int handle)
{

	//セットしなおす専用の関数へ
		//スタート、エンドを０にセットする
	ReStartTimerForCountDown(handle, 0.0f, 0.0f);
	ReStartTimerForCountUp(handle, 0.0f, 0.0f);

}

//タイマー情報構造体のコンストラクタ
CountTimer::TimerInfo::TimerInfo():
	permit(true),	//初期から、タイマーの計測を許可する
	elpasedTime(0.f),
	remaingTime(0.f),
	endTimeForCountUp(0.f),
	endTimeForCountDown(0.f)
{
}
