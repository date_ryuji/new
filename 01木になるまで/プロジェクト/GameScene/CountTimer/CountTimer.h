#pragma once
//システム内　標準ヘッダ(usr/)
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス
//時間の計測を請け負うクラス
	//Updateを行って、時間を加算する必要がある。
	//自身でUpdateを行って、そのタイミングで加算が出来れば、管理が楽なので、GameObject型を継承する



/*
	クラス詳細：時間計測を担うクラス　指定時間のカウントアップタイマー、指定時間のカウントダウンタイマー
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：SplashScene , TitleScene , GameScene , GameOverScene , WinningScene , ResultSceneなど
	クラス概要（詳しく）
				：Update処理内にて、設定された開始時間から、終了時間までの時間計測を行う
				：カウントアップとカウントダウンの2つの時間計測を行って、計測終了などを受け取ることが可能となる

*/
class CountTimer : public GameObject
{
//private 構造体
private : 
	/*
		構造体詳細	：1つのタイマーにおける計測に必要な情報群
		構造体概要（詳しく）
					：経過時間、終了時間などを所有することで、動的な時間計測と。計測終了を受けとる

	*/
	struct TimerInfo
	{
		//計測停止のフラグ
			//詳細：（計測を止めている場合は、時間計測を行わない）
			//　　：タイマー許可、非許可
		bool permit;
		//経過時間
			//タイマー：カウントアップ
			//詳細：タイマー（カウントアップ）実行時の、毎フレームの経過時間を加算する変数
		float elpasedTime;		
		//残り時間
			//タイマー：カウントダウン
			//詳細：タイマー（カウントダウン）実行時の、残り時間（引数終了時間から、経過時間をカウントダウンした際の、残りのｓ）
		float remaingTime;
		//終了時間
			//タイマー：カウントアップ
		float endTimeForCountUp;
		//終了時間
			//タイマー：カウントダウン
		float endTimeForCountDown;

		//コンストラクタ
		TimerInfo();
	};

	//計測中のタイマー群
	std::vector<TimerInfo*> pTimers_;


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	CountTimer(GameObject* parent);


	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//タイマー（カウントダウン）の開始
		//タイマー：カウントダウン
		//引数：開始時間（最大値、開始値）
		//引数：終了時間（最小値、終了値）
		//戻値：ハンドル番号（このクラスにおける、計測タイマーの個数番目（０オリジン））
	int StartTimerForCountDown(float startTime , float endTime);
	//タイマー（カウントアップ）の開始
		//タイマー：カウントアップ
		//引数：開始時間（最小値、開始値）
		//引数：終了時間（最大値、終了値）
		//戻値：ハンドル番号（このクラスにおける、計測タイマーの個数番目（０オリジン））
	int StartTimerForCountUp(float startTime, float endTime);
	//計測しなおし
		//タイマー：カウントダウン
		//詳細：すでに確保している、タイマーを使い、計測を１から再開する
		//　　：そのため、すでにあるタイマーにアクセスするために、ハンドル番号を第一引数にセットする
		//引数：ハンドル（タイマーの番号）
		//引数：開始時間（最小値、開始値）
		//引数：終了時間（最大値、終了値）
	void ReStartTimerForCountDown(int handle , float startTime, float endTime);
	//計測しなおし
		//タイマー：カウントアップ
		//詳細：すでに確保している、タイマーを使い、計測を１から再開する
		//　　：そのため、すでにあるタイマーにアクセスするために、ハンドル番号を第一引数にセットする
		//引数：ハンドル（タイマーの番号）
		//引数：開始時間（最小値、開始値）
		//引数：終了時間（最大値、終了値）
	void ReStartTimerForCountUp(int handle, float startTime, float endTime);

	//現在の残り時間を取得
		//タイマー：カウントダウン
		//引数：ハンドル（指定なし＝０）
		//戻値：残り時間
	float GetRemaingTimer(int handle = 0);
	//現在の終了時間を取得
		//タイマー：カウントダウン
		//引数：ハンドル（指定なし＝０）
		//戻値：終了時間
	float GetEndTimerForCountDown(int handle = 0);
	//現在の経過時間を取得
		//タイマー：カウントアップ
		//引数：ハンドル（指定なし＝０）
		//戻値：経過時間
	float GetElpasedTime(int handle = 0);
	//現在の終了時間を取得
		//タイマー：カウントアップ
		//引数：ハンドル（指定なし＝０）
		//戻値：終了時間
	float GetEndTimerForCountUp(int handle = 0);

	//タイマーの計測の終了したかを判断
		//タイマー：カウントダウン
		//詳細：”残り時間”　が　”カウントダウン終了時間”　以下である
		//引数：ハンドル（指定なし＝０）
		//戻値：計測終了かのフラグ
	bool EndOfTimerForCountDown(int handle = 0);

	//タイマーの計測の終了したかを判断
		//タイマー：カウントアップ
		//詳細：”現在時間”　が　”カウントアップ終了時間”　以上である
		//引数：ハンドル（指定なし＝０）
		//戻値：計測終了かのフラグ
	bool EndOfTimerForCountUp(int handle = 0);


	//タイマーの計測禁止
		//詳細：タイマーのストップ
		//　　：現在の計測の時間を残したまま、タイマーを停止する
		//引数：ハンドル（指定なし＝０）
		//戻値：なし
	void NotPermittedTimer(int handle = 0);
	//タイマーの計測許可
		//詳細：タイマーの開始(ストップしたものを再開)
		//引数：ハンドル（指定なし＝０）
		//戻値：なし
	void PermitTimer(int handle = 0);
	//タイマーが計測許可されているか
		//引数：ハンドル（指定なし＝０）
		//戻値：計測許可（許可:true , 拒否：false）
	bool IsPermitTimer(int handle = 0);


	//タイマーリセット
		//詳細：指定タイマーのメンバ変数をすべてリセット
		//引数：ハンドル（指定なし＝０）
		//戻値：なし
	void ResetTimer(int handle = 0);






};

