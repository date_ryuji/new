#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス

//クラスのプロトタイプ宣言
class GameSceneManager;
class BoxCollider;
class SphereCollider;
class CameraController;
//enum class のプロトタイプ宣言
enum class MOVE_DIRECTION;



/*
	クラス詳細：プレイヤークラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：プレイヤーオブジェクト操作に関するクラス


*/
class Player : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 
	//レイキャストが実行され、Y軸方向への落下が行われたか
	bool isFallY_;

	//Y方向への落下距離
	float fallValueY_;

	//前回の地面とのレイキャスト実行からのレイキャストを実行していないフレーム数
	unsigned int elapsedFrames_;
	
	//箱コライダー
	BoxCollider* pBoxColl_;
	//球体コライダー
	SphereCollider* pSphereColl_;

	//前回の座標
	XMVECTOR beforePos_;

	//前回、レイキャストが行われていた時の最終座標
	XMVECTOR beforeAtTheTimeOfRayCast_;

	//シーンマネージャークラス
	GameSceneManager* pSceneManager_;
	//カメラのコントローラークラス
		//詳細：カメラのコントロールと同時に、プレイヤーの移動を担う
	CameraController* pCameraController_;

//private メソッド
private :

	//プレイヤーが地面以外に移動してしまったときの戻し作業
		//引数：なし
		//戻値：なし
	void CoordReturnProcessing();


	//地面に添わせる
		//詳細：更新された地面までの落下距離分、自身の座標を下げて、
		//　　：地面のポリゴンの位置と同様の高さにする
		//引数：なし
		//戻値：なし
	void AlongTheGround();

	//レイキャスト実行
		//詳細：地面とのレイキャスト実行　プレイヤーの座標を地面に沿わせる
		//引数：なし
		//戻値：なし
	void RayCastGround();


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	Player(GameObject* parent);

	//初期化
				//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
				//レベル：オーバーライド
				//引数：なし
				//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;


	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

	//コライダー衝突時に呼ばれる関数
		//詳細：コライダー同士の３D衝突判定にて、衝突した場合、衝突の基準コライダー（コライダー１と衝突するコライダーを探していた場合、コライダー１が基準のコライダー）を持つGameObjectの以下関数を呼び込む
		//レベル：オーバーライド
		//引数：自身のコライダー衝突した相手のポインタ
		//戻値：なし
	void OnCollision(GameObject* pTarget) override;

//public メソッド
public : 

	//プレイヤー専用の木群を作成
		//詳細：プレイヤーが生成させた専用の木オブジェクトを作成
		//引数：なし
		//戻値：なし
	void CreateTreeOwnedByPlayer();


	//プレイヤーの移動
		//詳細：移動のキー入力が実行されて、移動の実行、カメラコントローラーにてカメラの操作と親オブジェクトの座標も移動させる
		//引数：移動方向
		//戻値：なし　
	void MovePlayer(MOVE_DIRECTION moveType);


	//落下距離を更新
		//引数：落下距離
		//戻値：なし
	void SetFallValueY(float fallValueY , bool isHit);

	//プレイヤーのY軸回転量を取得
		//詳細：プレイヤーの回転というより、カメラの回転量
		//　　：プレイヤー自身は実際には回転をせず、カメラの回転でプレイヤーの視点移動を示している。
		//　　：そのため、カメラコントローラーインスタンスを持っているプレイヤーに、カメラのY軸回転量を聞いて渡す
		//引数：なし
		//戻値：なし
	float GetPlayerRotateY();


};