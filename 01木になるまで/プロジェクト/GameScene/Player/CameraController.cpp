#include "CameraController.h"					//ヘッダ
#include "../../Engine/DirectX/Input.h"			//デバイス入力　受け付けクラス
#include "../../Engine/DirectX/Camera.h"		//カメラ
#include "../../Engine/Model/Model.h"			//モデル
#include "../../Engine/DirectX/Direct3D.h"		//Direct3D　


//コンストラクタ
CameraController::CameraController(GameObject * parent)
	: GameObject(parent, "CameraController"),
	moveVec_(XMVectorSet(0.f, 0.f, 0.f, 0.f)) , 
	beforeMouseCursor_(XMVectorSet(0.f , 0.f, 0.f, 0.f))
{
	//回転度数
	SPEED_ = 0.05f;
	standardPixX_ = 35.0f;
	standardPixY_ = 35.0f;	
				//standardPix〇_ ピクセルで　１度　回転する
				//前回と今回とで、割合を取り、
				//前回のピクセルと、今回のピクセルの差ピクセルを求めて、
					//その差を上記の標準と割ることで、回転度数を求める

				//★マウス感度を上げ下げを行うときは、
				//　上記の値を変更すればよい
				//★マウス感度を上げたいときは　値を小さく
				//★マウス感度を下げたいときは　値を大きく
}

//初期化
void CameraController::Initialize()
{
	//初期のマウス位置を保存
	//beforeMouseCursor_ = Input::GetMouseCursorPosition();
		//この位置がきちんと指定されていないと、シーン切り替えで移動してきたとき、
		//視点が勝手に移動し続ける現象が起きる
	beforeMouseCursor_ = XMVectorSet((float)(Direct3D::scrWidth / 2), (float)(Direct3D::scrHeight / 2), 0.f, 0.f);
}

//更新
void CameraController::Update()
{
	//カメラの移動(キーボード)
	//MoveCameraByKeyBoard();
	
	//カメラの移動(マウス)
	MoveCameraByMoveMouse();
}



//描画
void CameraController::Draw()
{
}


//解放
void CameraController::Release()
{
}


//カメラの移動
void CameraController::MoveCameraByKeyBoard()
{
	//キー入力受付
	XMVECTOR moveRotate = XMVectorSet(
		(float)(Input::IsKey(DIK_RETURN) - Input::IsKey(DIK_LSHIFT)),
		(float)(Input::IsKey(DIK_RIGHT) - Input::IsKey(DIK_LEFT)),
		0.f, 0.f);

	//キー入力値をスピードで掛けて、移動量を計算
	moveRotate *= SPEED_;

	//位置座標移動
	MovePosition();

	{
		//回転
		//回転は、子供クラスである、自身だけ
			//SkyBoxも回転させたくない
		transform_.rotate_ += moveRotate;
		//範囲を超えたときに切り詰める
			//カメラのX回転を90度以上にしてしまうと、
			//カメラの方向が逆になってしまうので、
			//超えてしまったときに、範囲内に切り詰める
		//TruncateRotate();

		//回転行列
		XMMATRIX mat = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX))
			* XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
		//マウス焦点の離れ具合
		XMVECTOR targetDis = XMVectorSet(0.f, 0.f, 5.f, 0.f);

		//回転後の位置
		//現在の回転量を求めて、
		//回転したときのベクトル位置を求める
		targetDis = XMVector3TransformCoord(targetDis, mat);
		//カメラの焦点
		Camera::SetTarget(pParent_->transform_.position_ + targetDis);
	}


}

//カメラの移動
void CameraController::MoveCameraByMoveMouse()
{
	//位置座標移動
	MovePosition();

	/*Inputクラスからマウスの移動量を取得*/
	//移動量に伴って、回転
	MoveView();

}

//カメラの回転
void CameraController::MoveView()
{
	{
		//マウスの移動量を取得
		//移動量にてマウスを移動させることで、
		//→ウィンドウから外れていても、視点を動かすことが可能

		//移動量のため、そのまま、基準のピクセル値と割合を取れば、回転量を求めることができる
		XMVECTOR mouseMove = Input::GetMouseMove();

		{
			//前回のポジションと
			//差分をとり、
			//その差ピクセルを、標準の移動ピクセルとで、割合をとり、
			//その割合を、移動スピードとする

			//この時、
			//どの方向に移動したら、どのように回転するのか。
			//→　＋Yへ、マウスが移動→　Y座標のピクセルが - → X軸回転の回転値(rotate_)が -
			//→　-Yへ、マウスが移動→　Y座標のピクセルが + → X軸回転の回転値(rotate_)が +
			//=　計算において　、　Yピクセルが +になったら、回転値が-になればよいので、
			//=　before - current

			//→　+Xへ、マウスが移動→ X座標のピクセルが + → Y軸回転の回転値(rotate_)が +
			//→　-Xへ、マウスが移動→　X座標のピクセルが + → Y軸回転の回転値(rotate_)が -
			//=　計算において　、　Xピクセルが +になったら、回転値が+になればよいので、
			//=　current - before 

			/*XMVECTOR move = XMVectorSet(
			currentMouseCursor.vecY - beforeMouseCursor_.vecY,
			currentMouseCursor.vecX - beforeMouseCursor_.vecX,
			0.f, 0.f);*/
		}


		//上記の計算では、
		//それぞれ、差のピクセル数が入っているため、
		//それを、1度回転の基準の移動ピクセル数と割って、
		//今回の移動量（度）を求める
		//Xの回転値(vecX)に入っているのは、Yピクセル移動による回転値（そのため、Y座標のスタンダードピクセルと割る）
		//Yの回転値(vecY)に入っているのは、Xピクセル移動による回転値（そのため、X座標のスタンダードピクセルと割る）
		XMVECTOR move;
		move.vecX = mouseMove.vecY / (float)standardPixY_;
		move.vecY = mouseMove.vecX / (float)standardPixX_;



		//回転
		//回転は、子供クラスである、自身だけ
		//SkyBoxも回転させたくない
		transform_.rotate_ += move;
		//範囲を超えたときに切り詰める
		//カメラのX回転を90度以上にしてしまうと、
		//カメラの方向が逆になってしまうので、
		//超えてしまったときに、範囲内に切り詰める
		TruncateRotate();



		//回転行列
		XMMATRIX mat = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX))
			* XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));

		//カメラ焦点の離れ具合
		XMVECTOR targetDis = XMVectorSet(0.f, 0.f, 5.f, 0.f);

		//回転後の位置
		//現在の回転量を求めて、
		//回転したときのベクトル位置を求める
		targetDis = XMVector3TransformCoord(targetDis, mat);

		//カメラの焦点セット
		Camera::SetTarget(pParent_->transform_.position_ + targetDis);
	}
}

//カメラの移動
void CameraController::MovePosition()
{

	{
		//移動は、
		//親ごと、移動させる
		//今回は、
		//SkyBoxという背景のクラスがあり、
		//その背景のクラスの親を、自身の親になっている。
		//なので、背景は、親が移動時に、自身も移動するようにしたいので、
		//親も同時に移動させる


		//回転行列
		XMMATRIX mat = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX))
			* XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));

		//回転行列にて変換
		XMVECTOR move = XMVector3TransformCoord(moveVec_, mat);
		//Y座標の高さを０へ
		move.vecY = 0.0f;


		pParent_->transform_.position_ = pParent_->transform_.position_ + move;


		//移動範囲を切り詰める
		//TruncatePosition();

		//親のプレイヤーオブジェクトより、
			//一定位置上げる
		XMVECTOR cameraMoveVec = XMVectorSet(0, 3, 0, 0);


		//回転後の位置
		//現在の回転量を求めて、
		//回転したときのベクトル位置を求める
		cameraMoveVec = XMVector3TransformCoord(cameraMoveVec, mat);



		//XMVECTOR camVec = Camera::GetPosition();
		Camera::SetPosition(pParent_->transform_.position_ + cameraMoveVec);

		//Camera::SetTarget(transform_.position_ + XMVectorSet(0, 0, 2, 0));
	}

	//移動値を初期化
	InitMoveVec();
}

//移動値の制限を掛ける
void CameraController::TruncatePosition()
{
	//移動限界値の定義
	static constexpr float MAX_POS_X = 10.0f;
	static constexpr float MIN_POS_X = MAX_POS_X * -1.0f;
	static constexpr float MAX_POS_Y = MAX_POS_X / 2.0f;
	static constexpr float MIN_POS_Y = MAX_POS_Y * -1.0f;
	static constexpr float MAX_POS_Z = MAX_POS_X;
	static constexpr float MIN_POS_Z = MAX_POS_Z * -1.0f;

	//移動限界値を超えている場合　
	//最高、最低値で割り切る
	//X
	if (pParent_->transform_.position_.vecX < MIN_POS_X)
	{
		pParent_->transform_.position_.vecX = MIN_POS_X;
	}
	else if (pParent_->transform_.position_.vecX > MAX_POS_X)
	{
		pParent_->transform_.position_.vecX = MAX_POS_X;
	}

	//Y
	if (pParent_->transform_.position_.vecY < MIN_POS_Y)
	{
		pParent_->transform_.position_.vecY = MIN_POS_Y;
	}
	else if (pParent_->transform_.position_.vecY > MAX_POS_Y)
	{
		pParent_->transform_.position_.vecY = MAX_POS_Y;
	}

	//Z
	if (pParent_->transform_.position_.vecZ < MIN_POS_Z)
	{
		pParent_->transform_.position_.vecZ = MIN_POS_Z;
	}
	else if (pParent_->transform_.position_.vecZ > MAX_POS_Z)
	{
		pParent_->transform_.position_.vecZ = MAX_POS_Z;
	}
}

//回転値の制限を掛ける
void CameraController::TruncateRotate()
{
	//回転限界値
	static constexpr float MAX_ROT_X = 10.5f;
	static constexpr float MIN_ROT_X = -50.5f;

	//回転限界値を超えている場合　
	//最高、最低値で割り切る
	//X
	if (transform_.rotate_.vecX < MIN_ROT_X)
	{
		transform_.rotate_.vecX = MIN_ROT_X;
	}
	else if (transform_.rotate_.vecX > MAX_ROT_X)
	{
		transform_.rotate_.vecX = MAX_ROT_X;
	}
}

//移動初期化
void CameraController::InitMoveVec()
{
	//移動値を初期化
	moveVec_ = XMVectorSet(0.f, 0.f, 0.f, 0.f);
}

//プレイヤー移動値を加算
void CameraController::AddMovePlayerValue(MOVE_DIRECTION moveType)
{
	//移動スピード
	static constexpr float MOVE_SEED_ = 0.1f;

	//移動の固定値
	//移動方向ごとの移動量
	static  const XMVECTOR moveValue_[(int)MOVE_DIRECTION::MOVE_DIR_MAX] =
	{
		//W
		XMVectorSet(0.f , 0.f, 1.0f * MOVE_SEED_ , 0.f),
		//A
		XMVectorSet(-1.0f * MOVE_SEED_  , 0.f, 0.f, 0.f),
		//S
		XMVectorSet(0.f , 0.f, -1.0f * MOVE_SEED_ , 0.f),
		//D
		XMVectorSet(1.0f * MOVE_SEED_  , 0.f, 0.f, 0.f),
	};

	//移動値を加算する
	moveVec_ += moveValue_[(int)moveType];

}

