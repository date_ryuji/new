#include "Player.h"										//ヘッダ
#include "../../Engine/DirectX/Camera.h"				//カメラ
#include "../../Engine/Collider/BoxCollider.h"			//箱コライダー　当たり判定
#include "../../Engine/Collider/SphereCollider.h"		//球コライダー　当たり判定
#include "../Scene/GameSceneManager.h"					//地面とプレイヤーの当たり判定実行時
														//地面の情報を橋渡ししてくれるもの
#include "../Scene/GameScene.h"							//GroupOfTreeオブジェクトの親オブジェクト指定のために使用する
#include "../Tree/GroupOfTree.h"						//プレイヤーの所有する
														//動的に生成、管理する木クラス群
#include "CameraController.h"							//カメラコントローラー


//コンストラクタ
Player::Player(GameObject* parent)
	: GameObject(parent, "Player"),
	pSceneManager_(nullptr),
	pCameraController_(nullptr),

	pBoxColl_(nullptr),
	pSphereColl_(nullptr),

	isFallY_(true) , 
	fallValueY_(0.0f),
	beforePos_(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	elapsedFrames_(0),
	beforeAtTheTimeOfRayCast_(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
	
}

//初期化
void Player::Initialize()
{
	{
		//シーンマネージャーの取得
		//シーンチェンジャーを経由して
			//引数指定のシーンのマネージャークラスを取得
		pSceneManager_ = (GameSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);
		{
			//下記の処理を上記で行っている、
				//文字列で指定すると、
				//クラス名変更になった際に、変更する箇所が多くなってしまう
				//（SceneMnagerクラスは、多くのクラスで参照するため、、Findで文字列で探している箇所が多いと、クラス名が変更になったとき、手直しで直す部分が多くなる
				//（Enumなどであれば、「名前の変更」で全ソースに適用できるが、　文字列は、名前の変更を行っても、全てのソースに対応できない。そのため、修正の際に時間とミスが出てしまう。
				//其れを避けるために、Enumで、指定する,修正のしやすい、形に変更させた））
			////シーンマネージャークラスを探す
			//pSceneManager_ = (GameSceneManager*)FindObject("GameSceneManager");
		}


		//自身をセットする
		pSceneManager_->AddCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_PLAYER, this);


	}

	//プレイヤー位置をカメラ位置に設置
		//カメラコントローラーとの差をなくすため
	transform_.position_ = Camera::GetPosition();
	//Y座標を初期化
	transform_.position_.vecY = 0.0f;

	{
		//カメラのY座標を０に、
		//カメラ操作する前に初期値を固定
		Camera::SetPosition(transform_.position_);
		//カメラコントローラー作成
		pCameraController_ = (CameraController*)Instantiate<CameraController>(this);
	}

	{
		//当たり判定追加
			//箱コライダーの作成
			//引数：付加オブジェクトからのローカル座標
			//引数：コライダーサイズ
			//引数：コライダー描画フラグ
		pBoxColl_ = new BoxCollider(XMVectorSet(0.f, 2.f, 0.f, 0.f), XMVectorSet(3.0f, 5.0f, 3.0f, 0.f), VISIBLE_COLLIDER);
	

		//コライダーを
		//オブジェクト自身のコライダーリストへ追加
		AddCollider(pBoxColl_);
		
		//コライダーの性能を壁ずり実行のコライダーに変更
			//壁ずりを実行する側のコライダー
			//コライダーがあった場合、自身の方が押し出される
		pBoxColl_->SetColliderFunctionType(COLLIDER_FUNCTION_ALONG_THE_WALL);
	}
}

//プレイヤー専用の木群を作成
void Player::CreateTreeOwnedByPlayer()
{
	/*
		//プレイヤーの所有する木オブジェクト群クラスのインスタンス生成
		//と共に、シーンマネージャーに登録して
		//管理下に置く

		//ここで、あくまでも、GroupOfTreeの、おやは、どのGroupOfTreeでも、共通にしなくてはいけない。
		//→共通といかなくても、静的なオブジェクトにする必要がある
		//→そのうえで、
		//→共通のオブジェクトを親にしないと、
		//→GroupOfTreeのTransformの可変によって、木オブジェクトの大きさ、位置が変わってしまうので、
		//静的オブジェクトかつ、共通オブジェクトを親にする必要がある。

		//サンプルシーンを親とするため、ゲット関数で取得する
		//→だが、ここで問題となるのは、
		//プレイヤーというオブジェクトは、シーンのオブジェクトのInitializeにて、初期化、生成されている、
		//すなわち、シーンのInitializeの中で、以下の処理がされるということ、
		//GetCurrentScene内にて、まだ、シーンのオブジェクトがないと判断されたとき、全オブジェクトから、シーンオブジェクトを探す処理が始まるので注意
	*/

	//木群を生成
	GroupOfTree* pGroup = (GroupOfTree*)Instantiate<GroupOfTree>(GetCurrentScene(SCENE_ID::SCENE_ID_GAME));
	//初期化
		//詳細：Initializeにて　→//pSceneManager_->AddSampleSceneObj(SAMPLE_SCENE_TREE_OWNED_BY_PLAYER, (GameObject*)pGroup);
		//引数：プレイヤー専用木を示すタイプ(enum)
	pGroup->Initialize(GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);
}

//更新
void Player::Update()
{
	//地面との当たり判定実行
	RayCastGround();
	
	//プレイヤーが地面以外に移動してしまったときの戻し作業
	CoordReturnProcessing();

	//前回の座標として、現在の座標を登録
		//プレイヤーのUpdate終了後
		//カメラコントローラーにて、
		//プレイヤーの座標を変化させられる
		//その前に、座標を登録
			//そうすれば、地面による座標移動は登録されて、キー移動のみ座標を戻す
	beforePos_ = transform_.position_;

}

//描画
void Player::Draw()
{
}

//解放
void Player::Release()
{
}


//コライダーとの衝突判定時処理
void Player::OnCollision(GameObject* pTarget)
{
	//衝突時、どのコライダーにもかかわらず、
	//そのコライダーから吐き出される処理を実行する
	{
		//壁ずりの前に
		//移動量を戻す
			//→前回の移動（衝突判定のタイミングから考えると、前回のフレームのUpdateの時の座標、→この処理が終了後に、今回のフレームのUpdateが呼ばれる）
		transform_.position_ = beforePos_;

		//壁ずりの実行
		pBoxColl_->AlongTheWall();

		//壁ずり実行後の座標を前回の座標に登録
		beforePos_ = transform_.position_;

		//壁ずりが実行されたため、計測フレームを初期化
		elapsedFrames_ = 0;
		//レイキャストが実行されたときに更新する座標も同様に前回の座標で更新
		beforeAtTheTimeOfRayCast_ = beforePos_;
	}
}

//プレイヤーが地面以外に移動してしまったときの戻し作業
void Player::CoordReturnProcessing()
{
	//レイキャストが一定フレーム行われなかったら
	//地面とのレイキャストが行われていた、座標へ戻す（戻し作業）
	//壁の当たり判定、地面の範囲を超えてしまったときの、救済措置
	static const int UPDATE_COUNT_MAX = 60;
	//条件：レイキャストがUPDATE_COUNT_MAX回以上、UPDATE_COUNT_MAXフレーム以上行われなかったら、
	//　　：&&
	//　　：落下がされていない
	if (elapsedFrames_ >= UPDATE_COUNT_MAX && (!isFallY_))
	{
		//座標を
		//レイキャストが行われていた座標に戻す
		transform_.position_ = beforeAtTheTimeOfRayCast_;

		elapsedFrames_ = 0;
	}
}

//地面とのレイキャスト実行
void Player::RayCastGround()
{
	//レイキャスト実行
	pSceneManager_->RayCastForPlayerAndGround();

	//地面に沿わせる
	AlongTheGround();
}

//レイキャスト実行後
//地面にY座標を沿わせる
void Player::AlongTheGround()
{
	//落下値分を落下させる
	transform_.position_.vecY -= fallValueY_;

	//条件：レイキャストの実行までの経過フレームが0回であったとき
	//地面のレイキャストが行われたことになるので、
	//その座標を登録する
		//レイキャスト実行後の座標を登録する
	if (elapsedFrames_ == 0)
	{
		beforeAtTheTimeOfRayCast_ = transform_.position_;
	}
}

//落下距離を更新
void Player::SetFallValueY(float fallValueY, bool isHit)
{
	//レイキャストが実行されたか
	//Y軸方向への落下が実行されたかのフラグ代入
	isFallY_ = isHit;


	//レイキャストによる落下が0の場合、落下がされていないフレーム計測を行う
	//条件：落下値が0の場合
	if (fallValueY == 0.0f)
	{
		elapsedFrames_++;
	}
	else
	{
		//経過フレームを初期化
		elapsedFrames_ = 0;
	}

	//メンバの落下値を更新
	fallValueY_ = fallValueY;

}

//プレイヤーのY軸回転量を取得
float Player::GetPlayerRotateY()
{
	//プレイヤーの所有する
	//カメラコントローラーを直接回転させ、その回転にカメラを合わせていル
		//そのため、オブジェクトのY軸回転量を取得
	return pCameraController_->transform_.rotate_.vecY;
}

//プレイヤーの移動
	//外部から移動を宣言され、
	//カメラコントローラーに移動宣言→プレイヤーのワールド座標も含めた、カメラの移動を担う（FPS視点限定）
void Player::MovePlayer(MOVE_DIRECTION moveType)
{
	pCameraController_->AddMovePlayerValue(moveType);
}
