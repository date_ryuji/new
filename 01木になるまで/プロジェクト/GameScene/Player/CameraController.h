#pragma once
#include "../../Engine/GameObject/GameObject.h"

//カメラ移動方向
	//詳細：移動方向を示す識別子
enum class MOVE_DIRECTION
{
	MOVE_DIR_W = 0,	//W
	MOVE_DIR_A,		//A
	MOVE_DIR_S,		//S
	MOVE_DIR_D,		//D

	MOVE_DIR_MAX,	//MAX
};



/*
	クラス詳細：カメラを操作するクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene->Player
	クラス概要（詳しく）
				：カメラのマウス操作、キーボード操作に対応するカメら操作
				　カメラの位置座標、カメラの焦点座標の移動、制限を掛けることが可能になる

*/
class CameraController : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 
	/*マウスによる移動***************************************************/
	//マウスによる視点移動の時、使用する前回のピクセル位置
		//詳細：前回のマウスの位置ピクセル
		//　　：マウスによる視点移動の際に、マウスピクセルの移動量から、マウス移動を行う
	XMVECTOR beforeMouseCursor_;

	//基準移動スピード
		//詳細：カメラの回転量
	float SPEED_;

	//基準移動ピクセル
	float standardPixX_;
	float standardPixY_;

	//プレイヤー移動値	
	XMVECTOR moveVec_;

//private メソッド
private : 
	//カメラの移動
		//詳細：キーボードによるカメラ移動
		//引数：なし
		//戻値：なし
	void MoveCameraByKeyBoard();
	//カメラの移動
		//詳細：マウスによるカメラ移動
		//引数：なし
		//戻値：なし
	void MoveCameraByMoveMouse();

	//カメラの回転
		//引数：なし
		//戻値：なし
	void MoveView();
	//カメラの移動
		//引数：なし
		//戻値：なし
	void MovePosition();
	//移動値の制限を掛ける
		//詳細：移動の限界値を設定し、限界値を超える場合、限界値を代入する
		//引数：なし
		//戻値：なし
	void TruncatePosition();
	//回転値の制限を掛ける
		//詳細：回転の限界値を設定し、限界値を超える場合、限界値を代入する
		//引数：なし
		//戻値：なし
	void TruncateRotate();

	//移動初期化
		//引数：なし
		//戻値：なし
	void InitMoveVec();



//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	CameraController(GameObject* parent);

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//プレイヤー移動値を加算
		//引数：移動方向
		//戻値：なし
	void AddMovePlayerValue(MOVE_DIRECTION moveType);







};