#include "Tree.h"									//ヘッダ
#include "../../CommonData/GlobalMacro.h"			//共通マクロ
#include "../../Engine/Csv/CsvReader.h"				//Csv読み取りクラス
#include "../../Engine/Algorithm/Timer.h"			//タイマークラス
#include "../../Engine/Collider/BoxCollider.h"		//箱コライダー　当たり判定クラス
#include "../Scene/GameSceneManager.h"				//シーンマネージャー
#include "../NumberPlate/NumberPlate.h"				//番号プレート
#include "GroupOfTree.h"							//木群オブジェクト
#include "TreeStrategy.h"							//木タイプごとの行動クラス
#include "FallenLeaves.h"							//落ち葉クラス
#include "EffectTree.h"								//エフェクト


//コンストラクタ
Tree::Tree(GameObject * parent)
	: LSystemTree::LSystemTree(parent, "Tree"),
	

	damageReceived_(0),
	//isErase_(false),
	pGroupOfTree_(nullptr),
	pNumberPlate_(nullptr),


	typeFlag_(false),
	pTreeStrategy_(nullptr),
	isEraseTreeStrategy_(false) ,

	pColliderSizes(nullptr) , 
	myTreeType_(TREE_TYPES::TREE_TYPE_MAX) , 
	pBoxColl_(nullptr) , 
	parentType_(GAME_SCENE_COOP_OBJECTS::GAME_COOP_BACK_GROUND_TREE)
{
}

//初期化
void Tree::Initialize()
{
}

//初期化
	//木のタイプごとの初期化
void Tree::Initialize(TREE_TYPES type , bool addEffect)
{
	//自身の木のタイプを設定
	myTreeType_ = type;



	//自身のタイプにおける
	//LSYSTEMの生成ルールと
	//使用テクスチャを選択

	{
		//ルール群
		std::string ruleStr[(int)TREE_TYPES::TREE_TYPE_MAX] =
		{
			GetRandomRule() ,
			GetRule("Assets/Scene/InputText/GameScene/LSystem/LSystemDummy.txt"),
			GetRule("Assets/Scene/InputText/GameScene/LSystem/LSystemBind.txt"),
			GetRule("Assets/Scene/InputText/GameScene/LSystem/LSystemEater.txt"),

		};
		//テクスチャ群
		std::string texStr[(int)TREE_TYPES::TREE_TYPE_MAX] =
		{
			"Assets/Scene/Image/GameScene/TreeTexture/Tree_Default.png",
			"Assets/Scene/Image/GameScene/TreeTexture/Tree_Dummy.png",
			"Assets/Scene/Image/GameScene/TreeTexture/Tree_Bind.png",
			"Assets/Scene/Image/GameScene/TreeTexture/Tree_Eater.png",

		};



		//LSYSTEMのルール
		//const std::string RULE = "F[-F[*F][/ F]]";
		//テクスチャのファイルパス
		const std::string TEXTURE_FILE_NAME = texStr[(int)myTreeType_];
		//ルールタイプをテキストから読み取る
		const std::string RULE = ruleStr[(int)myTreeType_];



		//継承元のInitialize()
		//引数：LSYSTEMのルール
		//引数：ソースモデルのテクスチャ
		LSystemTree::Initialize(RULE, TEXTURE_FILE_NAME);
	}




	//描画位置を変えているだけで
		//実際には、Transoformを動かさないので、
		//親のTransformから離れ具合を表現しているコライダーの方式ではできない
	//そのため、木を追加時に、
		//新たにコライダーを作成して、
		//その時に、原点からの離れ具合を登録して、コライダーを作る
		//そのため、コライダーが多くなるので注意


	{
		/*
		//落ち葉のテクスチャ群
		std::string fallenLeavesTex[(int)TREE_TYPES::TREE_TYPE_MAX] =
		{
			"Assets/Scene/Image/GameScene/TreeTexture/Leaf/Green.png",
			"Assets/Scene/Image/GameScene/TreeTexture/Leaf/WhiteGreen.png",
			"Assets/Scene/Image/GameScene/TreeTexture/Leaf/Purple.png",
			"Assets/Scene/Image/GameScene/TreeTexture/Leaf/Red.png",

		};
		*/
		//落ち葉のモデル群
		std::string fallenLeavesTex[(int)TREE_TYPES::TREE_TYPE_MAX] =
		{
			"Assets/Scene/Model/GameScene/TreeSource/Leaf/Leaf.fbx",
			"Assets/Scene/Model/GameScene/TreeSource/Leaf/LeafWhiteGreen.fbx",
			"Assets/Scene/Model/GameScene/TreeSource/Leaf/LeafPurple.fbx",
			"Assets/Scene/Model/GameScene/TreeSource/Leaf/LeafRed.fbx",

		};


		//落ち葉クラスへのテクスチャロード
		this->LoadFallenLeaves(fallenLeavesTex[(int)myTreeType_]);
	}

	{
		//ロードするエフェクト用のモデルのパス
		std::string effectFile[(int)TREE_TYPES::TREE_TYPE_MAX] =
		{
			"Assets/Scene/Model/GameScene/TreeSource/EffectDefaultTree.fbx",
			//"",
			"Assets/Scene/Model/GameScene/TreeSource/EffectDummyTree.fbx",
			"Assets/Scene/Model/GameScene/TreeSource/EffectBindTree.fbx",
			//"Assets/Scene/Model/GameScene/TreeSource/Frame.fbx",
			"Assets/Scene/Model/GameScene/TreeSource/EffectEaterTree.fbx",

		};

		//条件：エフェクトを追加するならば
		if (addEffect)
		{
			//エフェクトの追加

			//エフェクトクラス作成
			EffectTree* pEffect = (EffectTree*)Instantiate<EffectTree>(this);
			pEffect->Load(effectFile[(int)myTreeType_]);


			//木ごとのコライダーサイズを宣言
			{
				//動的確保
				pColliderSizes = new XMVECTOR[(int)TREE_TYPES::TREE_TYPE_MAX];


				pColliderSizes[(int)TREE_TYPES::TREE_TYPE_DEFAULT] = XMVectorSet(10.0f, 15.0f, 10.0f, 0.f);
				pColliderSizes[(int)TREE_TYPES::TREE_TYPE_DUMMY] = XMVectorSet(10.0f, 15.0f, 10.0f, 0.f);
				pColliderSizes[(int)TREE_TYPES::TREE_TYPE_BIND] = XMVectorSet(15.0f, 15.0f, 15.0f, 0.f);
				pColliderSizes[(int)TREE_TYPES::TREE_TYPE_EATER] = XMVectorSet(15.0f, 15.0f, 15.0f, 0.f);
			}

			//コライダーサイズごとに
			//エフェクトのサイズを可変する
				//コライダーサイズを視認できるように、エフェクトクラスを作っているため、コライダーサイズごとにサイズを可変
				//ただし、コライダーのサイズは、Tree自体のTransformを受けての、ローカルのサイズになるので、
					//コライダーサイズにそのまま合わせて、そのままの値のままエフェクトを拡大すればよいわけではないので注意
			pEffect->SetScaleWithColliderSize(pColliderSizes[(int)myTreeType_]);
		}
	}

}


//更新
void Tree::Update()
{
	//継承元のUpdate()
	LSystemTree::Update();

	//条件：時間停止していない場合
	if (!(IsStopTime()))
	{
		//条件：消去モーションが立っているか
		if (IsErase())
		{
			//自身が消去されるかの判定
			//消去モーションが終了したか
			JudgeKillMe();
		}
		else
		{
			//立っていない場合
			//	消去判定の計算を行う
			
			//蓄積ダメージの計算
			//DamageReceived();
			//死亡判定
			IsStartEraseMotion();
			//木ごとの行動クラス実行
			ExecutionTreeStrategy();
		}
	}
}

//描画
void Tree::Draw()
{
	//親のDraw()
		//基本的に、Tree単体でDrawを行うことはない、
		//→基本的に、Treeの描画は、GroupOfTreeにて管理する
		//LsystemのDrawには、何も処理は書かれていない。（描画用の専用の関数が存在する）
	//LSystemTree::Draw();

}

//解放
void Tree::Release()
{
	//行動クラス削除
	SAFE_DELETE(pTreeStrategy_);
	SAFE_DELETE_ARRAY(pColliderSizes);

	//親のRelease()
	LSystemTree::Release();

	//条件：存在するならば
	if (pGroupOfTree_ != nullptr)
	{
		//自身が解放したことを伝える
		pGroupOfTree_->ReleaseATree(this);
	}

}

//衝突時の行動
void Tree::OnCollision(GameObject* pTarget)
{
	//通常の木であれば、
	//消去モーション実行中

	//条件：通常の木タイプ
	//　　：||
	//　　：消去フラグが立っている
	if (myTreeType_ == TREE_TYPES::TREE_TYPE_DEFAULT || IsErase())
	{
		return;
	}

	//条件：衝突相手が敵であるならば
	if (pTarget->objectName_ == "Enemy")
	{
		//条件：木のタイプごとの行動クラスが生成されていないならば
		if (pTreeStrategy_ == nullptr)
		{
			//行動クラスの作成を行う

			//生成クラスの作成
			TreeStrategyFactory* pFactory = new TreeStrategyFactory;
			//行動クラスの作成
			pTreeStrategy_ = pFactory->CreateTreeStrategy(myTreeType_, this, pTarget);

			//生成クラスの解放
			SAFE_DELETE(pFactory);
		}

	}
}

//ルールをランダムで選択し返す
std::string Tree::GetRandomRule()
{
	//読み取るファイル数
	static constexpr unsigned int FILE_COUNT = 7;

	//ファイルパスのルート
	static const std::string RULE_PATH_ROOT = "Assets/Scene/InputText/GameScene/LSystem/LSystem";

	//今回の拡張で用いるルールの確定
	//1オリジンの　FILE_COUNTまで
	int random = 1 + rand() % (FILE_COUNT - 1);

	//CsvReaderを使用して、テキスト文字を受け取る
	CsvReader* pCsvReader = new CsvReader;

	//ファイルをロード
	pCsvReader->Load(RULE_PATH_ROOT + std::to_string(random) + ".txt");

	//ルールの文字列を受け取る
	const std::string RULE = pCsvReader->GetString(0, 0);

	//解放
	SAFE_DELETE(pCsvReader);

	return RULE;
}

//引数ファイルパスにて示されたテキストを読み込みルールを返す
std::string Tree::GetRule(const std::string& FILE_NAME)
{
	//CsvReaderを使用して、テキスト文字を受け取る
	CsvReader* pCsvReader = new CsvReader;

	//ファイルをロード
	pCsvReader->Load(FILE_NAME);

	//ルールの文字列を受け取る
	const std::string RULE = pCsvReader->GetString(0, 0);

	//解放
	SAFE_DELETE(pCsvReader);

	return RULE;
}

//消去フラグが立っている場合
bool Tree::IsErase()
{
	return treeStatus_.isErase;
}

//自身が消去されるかの判定
void Tree::JudgeKillMe()
{
	//条件：消去モーションが終了していたら
	if (EndEraseMotion())
	{
		//消去する
		this->KillMe();
	}
}

//蓄積ダメージの計算
void Tree::DamageReceived()
{
	//ダメージ値分　ＨＰを減少
	treeStatus_.trunkHP -= damageReceived_;
	treeStatus_.rootHP -= damageReceived_;

	//ダメージ値の初期化
	damageReceived_ = 0;
}

//消滅判定
void Tree::IsStartEraseMotion()
{
	//条件：消去フラグが立っているとき
	//　　：幹　根　どちらか一方が倒されたら
	if (IsDead())
	{
		//削除

		//消去モーションの実行
		//＆＆
		//木タイプごとの行動クラスも削除
		ForciblyEraseMe();
	}
}

//自身がなくなっているか
bool Tree::IsDead()
{
	return treeStatus_.rootHP <= 0 || treeStatus_.trunkHP <= 0;
}

//木ごとの行動クラスの実行
void Tree::ExecutionTreeStrategy()
{
	//条件：削除フラグが立っていたら削除
	if (isEraseTreeStrategy_)
	{
		//削除
		SAFE_DELETE(pTreeStrategy_);
		//フラグを下ろす
		isEraseTreeStrategy_ = false;

		//処理終了
		return;
	}

	//条件：行動クラスが存在していたら
	//呼び込みと同時に実行
	if (pTreeStrategy_ != nullptr)
	{
		//行動関数呼び込み
		//引数：フレーム経過時間
		pTreeStrategy_->AlgorithmInterface(Timer::GetDelta());
	}
}

//自身の番号プレートのロード	
void Tree::LoadNumberPlate(int handle)
{
	//現在のシーンが
		//ゲームシーンであるかの確認
		//ゲームシーンでければ、ナンバープレートは必要ない
	//条件：現在のシーンIDから、オブジェクトを取得できたなら
	if (GetCurrentScene(SCENE_ID::SCENE_ID_GAME) == nullptr)
	{
		//取得できなく、
			//nullptrであったら
			//関数終了
		return;
	}

	//初期ナンバープレートのロード
	pNumberPlate_ = (NumberPlate*)Instantiate<NumberPlate>(this);
	//ナンバープレートの該当モデルのロード
	pNumberPlate_->LoadNumberAndType(NUMBER_PLATE_TYPE::NUMBER_PLATE_TREE, handle);

}

//木ごとの行動クラスの削除実行
void Tree::EraseTreeStrategy()
{
	isEraseTreeStrategy_ = true;
}

//強制的に指定オブジェクトを捕縛させる
void Tree::ForciblyBindEnemy(GameObject* pTarget)
{
	//GroupOfTreeの同関数の呼び込み
	pGroupOfTree_->ForciblyBindEnemy(pTarget);
}

//捕縛解除
void Tree::ForciblyUnBindEnemy(GameObject* pTarget)
{
	//GroupOfTreeの同関数の呼び込み
	pGroupOfTree_->ForciblyUnBindEnemy(pTarget);
}

//自身の木のタイプを渡す
TREE_TYPES Tree::GetMyTreeType()
{
	return myTreeType_;
}

//表示用の自身番号として適切なオリジンの番号を渡す
int Tree::GetUpdatedMyNumber()
{
	//木の番号を更新する
			//木の番号は、0オリジンで管理させる
			//敵の番号のオリジンと合わせる

		//だが、この際、
			//プレイヤーの持っている木は、プレイヤーが立てた木以外にも、初期として木を持っている
			//そのため、ここで伝える番号は、その木があって、次の木の番号となってしまう。
			//ゲーム中は、その、プレイヤーが立てていない木は存在しないも同然なので、　番号を考えないようにする必要がある。

		//そのため、差として　ー１　を行う
	const int SUB_EXTRA = 1;
	//Update後の番号
	const int UPDATED_NUMBER = treeStatus_.number - SUB_EXTRA;

	return UPDATED_NUMBER;
}

//強制的に自身（木オブジェクト）消去する
void Tree::ForciblyEraseMe()
{
	//強制的に消去実行
	//消去モーションの実行
			//継承元（描画のモデル情報を持つ）クラスにシェーダーの切り替えを宣言
			//消去モーション実行のためのシェーダー切替えを宣言
			//消去モーションは、シェーダーの変更によって、実現する
			//消去モーションの実行後→解放
	StartEraseMotion();
	//消去モーション実行のフラグを立てる
	treeStatus_.isErase = true;

	//木のタイプごとの行動クラスの消去フラグを立てる
	isEraseTreeStrategy_ = true;
	//消去のタイミングで
	//条件：行動クラスが存在していたら
	if (pTreeStrategy_ != nullptr)
	{
		//木削除時の解放処理を呼び出す
			//消去モーション実行のタイミングで呼び込みたい
		pTreeStrategy_->ReleaseWhenATreeKnockedDown();
	}
}

//蓄積ダメージ加算
bool Tree::AddDamageAndIsDead(int damage)
{
	//ダメージ値加算
	damageReceived_ += damage;

	//ダメージを与える
	DamageReceived();

	//消滅判定を受け取る
	//もうすでに消滅していない
	//かつ
	//消滅した
	if (!IsErase() && IsDead())
	{
		return true;
	}

	return false;
}

//自身のHPと番号をセットする
void Tree::InitTreeStatus(int trunkHP, int rootHP, int number)
{
	treeStatus_.rootHP = trunkHP;
	treeStatus_.trunkHP = rootHP;
	//自身の木の番号を更新
	UpdatedMyTreeNumber(number);
}

//自身の木オブジェクトを所有するクラスをセット
void Tree::SetMyGroupClass(GroupOfTree * pGroupOfTree , GAME_SCENE_COOP_OBJECTS parentType)
{
	pGroupOfTree_ = pGroupOfTree;
	parentType_ = parentType;
}

//自身の木オブジェクト番号（GroupOfTreeにおける自身（Tree）へのインスタンスハンドル）を更新する
void Tree::UpdatedMyTreeNumber(int number)
{
	treeStatus_.number = number;

	//条件：木の番号が存在するとき
	if (treeStatus_.number != -1)
	{
		//木の番号を更新する
		//更新先の番号を取得
		const int UPDATED_NUMBER = GetUpdatedMyNumber();

		//条件：pNumberPlate_が確保されていないなら
		if (pNumberPlate_ == nullptr)
		{
			//ロードする
			//確保から始める
			LoadNumberPlate(UPDATED_NUMBER);
		}
		else
		{
			//木の番号を更新する
			pNumberPlate_->UpdateNumber(UPDATED_NUMBER);
		}
	}

}

//新しいコライダーの作成
void Tree::AddNewCollider(Transform& trans)
{

	//コライダー追加の処理の前に
		//コライダーを付けるオブジェクトの、
			//�@座標を、描画位置と同じ位置にする必要がある
			//�A或いは、オブジェクトを移動せずに、離れ具合で調整する必要がある

	//�A
	XMVECTOR vec = this->transform_.position_ - trans.position_;


	//当たり判定の追加
		//箱コライダーの作成
	//引数：離れ具合（）
	//引数：サイズ（１０）
	BoxCollider* pColl = new BoxCollider(XMVectorSet(0.f, 0.f, 0.f, 0.f) , pColliderSizes[(int)myTreeType_], VISIBLE_COLLIDER);
	//メンバに登録
	pBoxColl_ = pColl;

	//コライダーを
	//オブジェクト自身のコライダーリストへ追加
	AddCollider(pColl);

}

//当たり判定によるコライダーによる、消去の対象になるか
bool Tree::IsTargetToBeErased()
{
	return parentType_ == GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER;
}

//自身のコライダーの所有しているデバック描画用のワイヤーフレームのモデル番号を与える
int Tree::GetMyColliderModelHandle()
{
	return GetColliderModelHandleFromGameObject();
}

//自身のコライダーの所有しているデバック描画用のワイヤーフレームのモデルの描画時のTransformにセットさせる
void Tree::SetMyColliderTransformWhenDrawing()
{
	SetColliderTransformDrawingFromGameObject();
}

//自身の情報をセット（ポインタ）して返す
void Tree::SetTreeInfomation(TreeInfo * pTreeInfo)
{
	//pTreeInfo->rootHP = rootHP_;
	//pTreeInfo->trunkHP = trunkHP_;
}

//木のパーツを成長させる
bool Tree::GrowParts(TREE_PARTS part)
{
	//各成長
	//条件：パーツが幹
	//　　：&&
	//　　：成長可能回数を超えていない
	if (part == TREE_PARTS::TREE_TRUNK && treeStatus_.numberOfTrunkGrowths < MAX_NUMBER_TRUNK)
	{
		treeStatus_.trunkHP += GROW_VALUE_;
		return true;
	}
	//条件：パーツが根
	//　　：&&
	//　　：成長可能回数を超えていない
	else if(part == TREE_PARTS::TREE_ROOT && treeStatus_.numberOfRootGrowths < MAX_NUMBER_ROOT)
	{
		treeStatus_.rootHP += GROW_VALUE_;
		return true;
	}

	//成長していない
	return false;
}

//引数敵オブジェクトが、敵スポーンに存在するか
bool Tree::IsExistsTarget(GameObject* pTarget)
{
	//Treeの親クラス
		//GroupOfTreeを経由し、Spawnに存在するかの確認
	return pGroupOfTree_->IsExistsTarget(pTarget);
}

//引数敵オブジェクトを強制的に削除する
void Tree::EraseEnemy(GameObject* pTarget)
{
	pGroupOfTree_->EraseEnemy(pTarget);
}

//コンストラクタ
Tree::TreeStatus::TreeStatus():
	rootHP(-1),number(-1),trunkHP(-1),
	numberOfTrunkGrowths(-1),
	numberOfRootGrowths(-1),
	elpasedTime(0.f),
	isErase(false)
{
}
