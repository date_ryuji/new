#pragma once

//クラスのプロトタイプ宣言
class Tree;
class TreeStrategy;
class GameObject;
//enum class のプロトタイプ宣言
enum class TREE_TYPES;

/*
	クラス詳細	：木のタイプごとの行動クラス生成クラス　ファクトリークラス
	使用デザインパターン：FactoryMethod
	使用クラス	：GameScene->GroupOfTree->Tree
	クラス概要（詳しく）
				：木オブジェクトのコライダーと、対象となる敵オブジェクトのコライダーが衝突した際、
				　対象となる敵オブジェクトへ向けた毎フレームの行動実行アルゴリズムを記述したクラスを生成する
				：木ごとに動的処理を分けることができるため、Tree（木オブジェクト）単体で複数パターンの処理を担わせることができる。
				　行動を分離した

*/
class TreeStrategyFactory
{
//public メソッド
public:
	//コンストラクタ
		//引数：なし
		//戻値：なし
	TreeStrategyFactory();
	
	//行動クラス生成関数
		//引数：木のタイプ
		//引数：Treeポインタ
		//引数：行動を起こすターゲットポインタ
		//戻値：木ごとの行動クラス
	TreeStrategy* CreateTreeStrategy(TREE_TYPES type, Tree* pTree, GameObject* pTarget);

};



/*
	クラス詳細	：対象となった敵オブジェクトとの衝突時に行う　敵への行動ストラテジークラス
	使用クラス	：GameScene->GroupOFTree->Tree
	クラス概要（詳しく）
				：木オブジェクトと敵オブジェクトが衝突した際、
				　初めて木オブジェクトごとの行動クラスが生成される。（木オブジェクトのタイプごとに生成されるクラスが異なる）
				：生成後、毎フレーム行動クラスのAlgorithmInterface処理が呼び込まれ、対象の敵オブジェクトへの動的処理が開始される
				：継承先においても、メソッドをオーバーライドすることで、各継承先で個別の処理を記述可能。
				  デバイス入力時の行動を変えることができる

				：対象となる敵オブジェクトが消滅する。或いは、自身の親クラスであるTreeオブジェクトが消滅した場合
				　行動終了となり、
				  行動クラスは消滅する。

*/
class TreeStrategy
{
//protected メンバ変数、ポインタ、配列
protected : 
	//ターゲットとなる敵オブジェクト
	GameObject* pTarget_;
	//親クラスであるTreeオブジェクト
	Tree* pTree_;

//protected メソッド
protected : 

	//Treeを経由してターゲットなる敵オブジェクト（pTarget_）の存在確認
		//詳細：対象となる敵オブジェクトが存在するかを調べてもらう
		//　　：存在していたら、行動実行
		//　　：存在していなかったら、行動終了。とともに終了処理（ReleaseWhenATreeKnockedDown）
		//引数：対象となる敵オブジェクト(pTarget_)
		//戻値：存在するか
	bool IsExistsTarget(GameObject* pTarget);

//public メソッド
public : 

	//コンストラクタ
		//引数：自身を付加しているTreeオブジェクトポインタ
		//引数：対象となる敵オブジェクト
		//戻値：なし
	TreeStrategy(Tree* pTree , GameObject* pTarget);

	//解放時の処理
		//詳細：ターゲットとなる敵オブジェクトへ木の行動クラスが解放されたことを宣言
		//　　：自身の親に解放されることを宣言
		//　　：　　：バインドの木などで、
		//　　：　　：敵をバインド状態にしたまま退場すると、敵は行動不能を保ちつづけてしまう。
		//　　：　　：そのため、自身が解放される際は、バインドした敵のバインドを解除する。その際にデストラクタ。あるいは、解放時のReleaseを用意しておく
		//　　：　　：デストラクタは、ほかのポインタが削除されてしまっている場合があるため、経由できない可能性もある
		//　　：　　：Releaseもしかり。
		//　　：　　：そのため、木が倒された際限定で、呼ぶ処理を作成する
		//　　：　　：倒されたときにだけ呼びようにし、そのほかの自身の削除などは視野に入れない。
		//　　：　　：そのほかの削除であれば、自身のメンバなども解放されるので、解放されれば関係ない。
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void ReleaseWhenATreeKnockedDown() = 0; 


	//行動実行
		//詳細：ターゲットとなる敵オブジェクトへの動的処理を行う
		//　　：基本的には、毎フレーム呼び込まれる（例外在り）
		//　　：例外：時間が停止されている場合
		//レベル：純粋仮想関数
		//引数：経過時間
		//戻値：なし
	virtual void AlgorithmInterface(float elpasedTime) = 0;


	//自身消去時の行動
		//詳細：自身の消去依頼
		//　　：Tree自体の消去依頼
	virtual void ExecutionErase() = 0;
};




