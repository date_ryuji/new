#include "EaterTreeMouth.h"					//ヘッダ
#include "../../Engine/Model/Model.h"		//モデル


//コンストラクタ
EaterTreeMouth::EaterTreeMouth(GameObject* parent)
	: GameObject(parent, "EaterTreeMouth"),
	hModel_(-1),
	hWithGround_(-1),

	code_(1.0f),
	isCompleted_(false),
	MOVE_POS_SPEED_(0.f),
	ROTATE_SPEED_(0.f),
	EXPANSION_SPEED_(0.f),
	elpasedTIme_(0.f),
	totalElpasedTime_(0.f),
	targetPos_(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	moveDir_(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}

//初期化
void EaterTreeMouth::Initialize()
{
	//モデルのロード
	hModel_ = Model::Load("Assets/Scene/Model/GameScene/TreeSource/Eater.fbx",
		POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_TEST);
	//警告
	assert(hModel_ != -1);

	//地面エフェクト用のモデルのロード
		//地面エフェクト用は地面エフェクトとして使うのでなく、上記モデルに付加するモデルとして使用する
	hWithGround_ = Model::Load("Assets/Scene/Model/GameScene/TreeSource/EaterWithGround.fbx" , 
		POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP , SHADER_TYPE::SHADER_SCROLL);
	//警告
	assert(hWithGround_ != -1);


	//Transformのセット
	transform_.position_.vecY = 10.f;
	transform_.rotate_.vecX = -120;
	transform_.scale_.vecY = 0.8f;

}



//更新
void EaterTreeMouth::Update()
{

	//捕食場所まで移動
	//条件：移動完了フラグが立っていなかったら
	if (!isCompleted_)
	{
		//目的位置まで移動
		Navigation();

		//合計経過時間の計測
		totalElpasedTime_ += elpasedTIme_;

		//条件：合計経過時間が捕食時間を超えていたら
		if (totalElpasedTime_ >= PREDATION_TIME_)
		{
			//移動完了フラグを立てる
			isCompleted_ = true;
		}
	}
	
	//計算終了の
	//経過時間の初期化
	elpasedTIme_ = 0.f;

}

//捕食オブジェクトの移動ナビゲーション
void EaterTreeMouth::Navigation()
{
	//捕食時間をかけて移動
	{
		//スピード＊経過時間によって
			//移動量を出す
			//移動方向は、
			//現在位置から、目的位置まで

		//移動方向＊移動量
		transform_.position_ += moveDir_ * MOVE_POS_SPEED_ * elpasedTIme_;

	}

	//捕食時間をかけて回転
	{
		//回転スピード＊経過時間
			//回転量を出し、回転させる
		transform_.rotate_.vecY += ROTATE_SPEED_ * elpasedTIme_;
	}
	
	//捕食時間をかけて拡大、縮小
	{
		//拡大量
		const float EXPANSION_VALUE = EXPANSION_SPEED_ * elpasedTIme_;

		//拡大スピード＊経過時間
			//拡大量を出し、拡大させる
		transform_.scale_ += XMVectorSet(EXPANSION_VALUE, EXPANSION_VALUE, EXPANSION_VALUE, 0.f);
	}

}

//描画
void EaterTreeMouth::Draw()
{
	//描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
	Model::SetTransform(hWithGround_, transform_);
	Model::Draw(hWithGround_);

}

//解放
void EaterTreeMouth::Release()
{
}

/*
//捕食オブジェクトの移動
void EaterTreeMouth::MoveUpOrDown()
{
	//条件：
	if (transform_.position_.vecY > 13.f && code_ == 1.0f)
	{
		code_ = -1.f;
	}
	else if (transform_.position_.vecY < 10.f && code_ == -1.0f)
	{
		code_ = 1.f;
	}

	//移動値
	static constexpr float SPEED = 0.1f;

	//上下移動
	transform_.position_.vecY += SPEED * code_;
}
*/

//捕食完了しているか
bool EaterTreeMouth::IsPredationCompleted()
{
	//条件：捕食完了フラグが立っているか
	return isCompleted_;
}

//引数目的地へのナビゲーションを開始する
void EaterTreeMouth::StartNavigation(XMVECTOR targetPos)
{
	//移動完了フラグを下ろす
	isCompleted_ = false;
	//目標位置をセット
	targetPos_ = targetPos;


	{
		//移動方向の更新
		moveDir_ = targetPos - this->GetWorldPos();

		//現在位置と目的位置までの長さを求めて、
			//その長さを道のりとして、時間と割り、スピードを求める
		XMVECTOR length = XMVector3Length(moveDir_);

		//移動方向を正規化
		moveDir_ = XMVector3Normalize(moveDir_);


		//長さと時間から
		//スピードを求める
		MOVE_POS_SPEED_ = length.vecX / PREDATION_TIME_;
	}
	{
		//回転スピードを求める

		//�@現在向いている方向と、
		//移動方向との2つのベクトルのなす角を出したい
		//�A内積で、角度を出す。
			//この角度が、最終的に回転する量。

		//�Bその回転量から、
			//時間とで割り。回転スピードを求める

		//�@
		//現在向いている方向
		XMVECTOR frontVec = XMVectorSet(0.f, 0.f, -1.f, 0.f);
		//移動方向
		XMVECTOR moveVec = XMVector3Normalize(moveDir_);

		//�A
				//２つのベクトルの内積のACosの値が、２つのベクトルのなす角となる
			//内積を求める（２つのベクトルによる、一番近い内積（小さい角度のほうの内積））
		float dot = XMVector3Dot(frontVec, moveVec).vecX;


		//２つのベクトルのなす角
			//結果　＝　ラジアン
			//度に直す(計算の際に、最終的にラジアンに変化させるが、でバック時にわかりやすくするために、度にする)
		//float angle = acos(dot) * (180 / 3.14);	//←計算式
		float angle = XMConvertToDegrees((float)acos((double)dot));

		//�B
		ROTATE_SPEED_ = angle / PREDATION_TIME_;

	
	}
	{
		//拡大量から拡大スピードを求める

		//拡大量
		static constexpr float MAX_EXPANSITON = 0.5f;

		//拡大スピードを求める
		EXPANSION_SPEED_ = MAX_EXPANSITON / PREDATION_TIME_;

	}

}

//経過時間更新
void EaterTreeMouth::UpdateElpasedTime(float elpasedTime)
{
	elpasedTIme_ = elpasedTime;
}

//ナビゲーション目的位置の座標取得
XMVECTOR EaterTreeMouth::GetTargetPos()
{
	return targetPos_;
}
