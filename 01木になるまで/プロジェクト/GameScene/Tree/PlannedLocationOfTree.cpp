#include "PlannedLocationOfTree.h"			//ヘッダ
#include "../../Engine/Csv/CsvReader.h"		//ルール読み取り用
#include "../../CommonData/GlobalMacro.h"	//共通マクロ
#include "../../Engine/Model/Model.h"		//モデルクラス


//コンストラクタ
PlannedLocationOfTree::PlannedLocationOfTree(GameObject* parent) 
	: LSystemTree::LSystemTree(parent, "PlannedLocationOfTree") , 
	hPlate_(-1)
{
}

//初期化
void PlannedLocationOfTree::Initialize()
{
	//木モデルの作成
	CreateTree();

	//プレート作成
	CreatePlate();
}

void PlannedLocationOfTree::Update()
{
}

void PlannedLocationOfTree::Draw()
{
	//木描画
	//描画を自ら管理する
	this->DrawBranchesMakeTree(transform_);

	{
		//プレート用のTransform宣言
		Transform trans = transform_;
		//位置調整
		trans.position_ += XMVectorSet(0.f, 2.f, 0.f, 0.f);
		//サイズ調整
		trans.scale_ = XMVectorSet(1.f, 2.f, 1.f, 0.f);
		//位置セット
		Model::SetTransform(hPlate_, trans);
		//描画
		Model::Draw(hPlate_);

	}

}

void PlannedLocationOfTree::Release()
{
	//親のRelease()
	LSystemTree::Release();
}


//プレート番号を作成する
void PlannedLocationOfTree::CreatePlate()
{
	//プレート専用の画像をセットして
	//プレートクラスの作成
	hPlate_ = Model::Load(
		"Assets/Scene/Image/GameScene/Signboard/PlannedLocationOfTree.png",
		POLYGON_GROUP_TYPE::PLANE_POLYGON_GROUP,
		SHADER_TYPE::SHADER_PLANE);
	//警告
	assert(hPlate_ != -1);


}

//木を作成する
void PlannedLocationOfTree::CreateTree()
{
	//LSYSTEMのモデル用意

	//LSYSTEMのルール
	//固定
	//ファイル名
	static const std::string RULE_FILE_NAME = "Assets/Scene/InputText/GameScene/LSystem/LSystem1.txt";

	//ルール読み取りのCsvReaderのインスタンス生成
	CsvReader* pCsvReader = new CsvReader();
	//ロード
	pCsvReader->Load(RULE_FILE_NAME);
	//ルール読み取り
	std::string RULE = pCsvReader->GetString(0, 0);

	//テクスチャファイル名
	static const std::string TEXTURE_FILE_NAME = "Assets/Scene/Image/GameScene/TreeTexture/Tree_Default.png";


	//継承元のInitialize()
	//引数：LSYSTEMのルール
	//引数：ソースモデルのテクスチャ
	LSystemTree::Initialize(RULE, TEXTURE_FILE_NAME);


	//枝を生成
	//この枝生成を呼びこまないと、最初の枝ができないので注意
	//後のLSYSTEMを実行できない
	this->CreateTheFirstOne();

	//解放
	SAFE_DELETE(pCsvReader);


	//立地予定木のためのシェーダー（ワイヤーフレーム）に切り替える
	ChangeShader();


	//LSYSTEMを指定回数進めて、枝を生成
	//LSYSTEMの最高回数
	//2回進める
	static constexpr int LSYSTEM_COUNTER = 2;
	//条件：LSYSTEM実行回数分
	for (int i = 0; i < LSYSTEM_COUNTER; i++)
	{
		//LSYSTEMの実行宣言
		this->ExecutionLsystem();
	}

}

//立地予定木のためのシェーダー（ワイヤーフレーム）に切り替える
void PlannedLocationOfTree::ChangeShader()
{
	//立地予定の木の描画のために専用のシェーダーをセットする
		//モデルハンドルを持つのはLSYSTEMなので、
		//LSYSTEMへ、シェーダー変更を指示してもらう
		//SetPlannedLocationOfTreeShader();
	int hTree = this->GetBranchMakeATreeHandle();

	//シェーダーを切り替える
		//シェーダーの切替え
		//ワイヤーフレームへ
	Model::ChangeShader(hBranchesMakeTree_, SHADER_TYPE::SHADER_DEBUG_WIRE_FRAME);
}

//描画ピクセル位置から描画位置を取得する
void PlannedLocationOfTree::SetDrawPosition(XMVECTOR& setPos)
{
	transform_.position_ = setPos;
}

