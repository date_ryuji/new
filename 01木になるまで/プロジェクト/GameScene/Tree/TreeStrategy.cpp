#include "TreeStrategy.h"							//ヘッダ
#include "../../Engine/GameObject/GameObject.h"		//GameObject型
#include "../../Engine/Model/Model.h"				//モデル
#include "Tree.h"									//木オブジェクト
#include "EaterTreeMouth.h"							//イーター専用　イーターモデル
#include "BindTreeBinder.h"							//バインド専用　バインドモデル



/*行動クラス（シーンごとのシーン名TreeStrategyを継承したクラス）宣言*******************************************************************************************************/
	//詳細：Factoryクラス（行動クラス生成クラス）にてインスタンスの動的確保を行うため、Factoryの実装よりも前に記述
	//詳細：行動クラスを複数作ることになるため、行動クラスごとにクラスファイルを作成すると、クラスファイル量が多くなる。
	//　　：→上記を避けるために、一つのクラスに行動クラスをまとめる。（メソッドやメンバも少ないため、ひとくくりにしてしまう。）
/******************************************************************/
//BIND
class BindTreeStrategy : public TreeStrategy
{
//protected メンバ変数、ポインタ、配列
protected : 
	//バインド専用　バインドモデル（バインドのつる、ひもモデル）
	BindTreeBinder* pBindBinder_;

//public メソッド
public : 
	//コンストラクタ
		//引数：自身を付加しているTreeクラスオブジェクト
		//引数：ターゲットとなるオブジェクト
		//戻値：なし
	BindTreeStrategy(Tree* pTree, GameObject* pTarget);

	//自身のTreeが倒木時に呼び出される処理
		//詳細：解放処理
		//レベル：オーバーライド
		//引数：なし	
		//戻値：なし
	void ReleaseWhenATreeKnockedDown() override;

	//行動実行
		//詳細：基本的には、毎フレーム呼び込まれる（例外在り）
		//例外：時間が停止されている場合
		//レベル：オーバーライド
		//引数：経過時間
		//戻値：なし
	void AlgorithmInterface(float elpasedTime) override;

	//消去時の行動
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void ExecutionErase() override;
};
/******************************************************************/
/******************************************************************/
//EATER
class EaterTreeStrategy : public TreeStrategy
{
//protected メンバ変数、ポインタ、配列
protected:
	//イーター専用　イーターモデル（イーターの口モデル）
	EaterTreeMouth* pEaterMouth_;

//public メソッド
public:
	//コンストラクタ
		//引数：自身を付加しているTreeクラスオブジェクト
		//引数：ターゲットとなるオブジェクト
		//戻値：なし
	EaterTreeStrategy(Tree* pTree, GameObject* pTarget);

	//自身のTreeが倒木時に呼び出される処理
		//詳細：解放処理
		//レベル：オーバーライド
		//引数：なし	
		//戻値：なし
	void ReleaseWhenATreeKnockedDown() override;


	//行動実行
		//詳細：基本的には、毎フレーム呼び込まれる（例外在り）
		//例外：時間が停止されている場合
		//レベル：オーバーライド
		//引数：経過時間
		//戻値：なし
	void AlgorithmInterface(float elpasedTime) override;

	//消去時の行動
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void ExecutionErase() override;
};
/******************************************************************/


/******************************************************************/
//Treeを経由してターゲットなる敵オブジェクト（pTarget_）の存在確認
bool TreeStrategy::IsExistsTarget(GameObject* pTarget)
{
	//Treeを経由して
		//引数オブジェクトが敵スポーンに存在するかの確認
	return pTree_->IsExistsTarget(pTarget);
}
//コンストラクタ
TreeStrategy::TreeStrategy(Tree* pTree, GameObject* pTarget):
	pTree_(pTree),
	pTarget_(pTarget)
{
}
//解放時の処理
	//レベル：仮想関数
void TreeStrategy::ReleaseWhenATreeKnockedDown()
{
}
/******************************************************************/


/*行動クラス生成クラス　実装*******************************************************************************************************/
/*Factory*/
//コンストラクタ
TreeStrategyFactory::TreeStrategyFactory()
{
}

//行動クラス生成関数
//パターン：FactoryMethod
TreeStrategy* TreeStrategyFactory::CreateTreeStrategy(TREE_TYPES type, Tree* pTree, GameObject* pTarget)
{
	switch (type)
	{
	case TREE_TYPES::TREE_TYPE_BIND:
		return (TreeStrategy*)(new BindTreeStrategy(pTree , pTarget));
	case TREE_TYPES::TREE_TYPE_EATER:
		return (TreeStrategy*)(new EaterTreeStrategy(pTree, pTarget));
	default:
		return nullptr;
	}
}
/******************************************************************/


/******************************************************************/
//EATER
//コンストラクタ
EaterTreeStrategy::EaterTreeStrategy(Tree* pTree, GameObject* pTarget):
	TreeStrategy(pTree , pTarget),
	pEaterMouth_(nullptr)
{
	//イーター専用のモデルのロード
			//Treeの座標をもとに描画位置を決める
	pEaterMouth_ = (EaterTreeMouth*)Instantiate<EaterTreeMouth>(pTree_);
	//ナビゲーションの開始
	pEaterMouth_->StartNavigation(pTarget->GetWorldPos());

	//対象の敵のバインド開始
	//バインド中：ナビゲーション不可。攻撃不可。
	pTree_->ForciblyBindEnemy(pTarget_);
}

//自身のTreeが倒木時に呼び出される処理
	//レベル：オーバーライド
void EaterTreeStrategy::ReleaseWhenATreeKnockedDown()
{
	//対象の敵のバインド解除
		//自身の木が倒されたとき限定で呼び出すため、
		//ゲーム終了時の解放、などでは呼ばれることがない。
		//更に、自身の木が倒されたとき限定であるため、デストラクタなど必ず呼ばれるもので起こりえる、敵スポーンのポインタ消滅時に、アクセスするという事態は起こらない。

		//呼ばれるタイミング
		//消去モーションが呼ばれたタイミング→自身で倒木した。敵に倒木された。
	
	//ForciblyUnbind事態に、
		//引数オブジェクトが存在していたら、その対象へオブジェクトバインド解除を命じる
		//そのため、前段階に、引数オブジェクトが存在するかの判断は必要なし
	pTree_->ForciblyUnBindEnemy(pTarget_);
}

//行動実行
//毎フレーム処理
	//レベル：オーバーライド
void EaterTreeStrategy::AlgorithmInterface(float elpasedTime)
{
	//条件：対象が存在しない場合
		//対象が存在しない場合、考えられる原因：対象が消滅した
	if (!(IsExistsTarget(pTarget_)))
	{
		//消去行動実行
		ExecutionErase();
		//イーター専用のモデルクラスの削除
		//pEaterMouth_->KillMe();

	}
	else
	{
		//経過時間を更新
		pEaterMouth_->UpdateElpasedTime(elpasedTime);

		//ターゲットを移動させない
			//イーターの捕食目的地に更新する
		
		//条件：捕食のための移動が完了したら
		if(pEaterMouth_->IsPredationCompleted())
		{
			//処理終了

			//対象を削除
			//シーンマネージャーを経由して、
			//敵を削除
			//敵を削除するにも、手順が必要なので、
			pTree_->EraseEnemy(pTarget_);

		}
	}
}

//消去時の行動
	//レベル：オーバーライド
void EaterTreeStrategy::ExecutionErase()
{
	//自身のインスタンスを削除依頼
	pTree_->EraseTreeStrategy();
	//自身の木の削除依頼
	pTree_->ForciblyEraseMe();

}
/******************************************************************/

/******************************************************************/
//BIND
//コンストラクタ
BindTreeStrategy::BindTreeStrategy(Tree* pTree, GameObject* pTarget):
	TreeStrategy(pTree, pTarget),
	pBindBinder_(nullptr)
{
	//バインド用バインダー作成
	pBindBinder_ = (BindTreeBinder*)Instantiate<BindTreeBinder>(pTree_);
	//バインド開始位置として
		//ターゲットのワールド座標をセット
	pBindBinder_->SetStartBindPos(pTarget_->GetWorldPos());

	//対象の敵のバインド開始
		//バインド中：ナビゲーション不可。攻撃不可。
	pTree_->ForciblyBindEnemy(pTarget_);

}

//自身のTreeが倒木時に呼び出される処理
	//レベル：オーバーライド
void BindTreeStrategy::ReleaseWhenATreeKnockedDown()
{
	//対象の敵のバインド解除
	pTree_->ForciblyUnBindEnemy(pTarget_);

}

//行動実行
//毎フレーム処理
	//レベル：オーバーライド
void BindTreeStrategy::AlgorithmInterface(float elpasedTime)
{
	//条件：対象が存在するかの確認
	if (!(IsExistsTarget(pTarget_)))
	{
		//消去行動実行
		ExecutionErase();
	}
	else
	{
		//ターゲットのワールド座標と同様の位置にバインダーを移動
		pBindBinder_->SetWorldPos(pTarget_->GetWorldPos());

		/*
		//下記で座標位置を変更させても、
			//Enemy自身の座標変換、ナビゲーションがあると、
			//その処理で、移動位置が更新されてしまうため、
			//ナビゲーション中は、以下の処理が効かない。
			// 
			// そのため、バインド開始位置にセットしなおしたとしても意味なし、以下での移動自体が、無視されている
		//解決方法
			//ナビゲーションを止める
			//→だが、ナビゲーションを止めると、→新たな仕事が割り当てられてしまう。
			//→なので、強制的に止める、フラグなど　新しい何かが必要。
		*/


		/*
		//バインドフラグを立てる

		//★バインド対象を自身方向へ一定量引き寄せる
		//
		//何回かに一回　力を緩めて、あえて、進ませる
			//そして、再び開始するときには、移動力よりも強い力で引き寄せる

		//対象のワールド座標を求める
			//pTargetから参照できる座標は、親からのローカルのため、ワールド座標取得
		XMVECTOR targetPos = pTarget_->GetWorldPos();

		//自身のワールド座標を求める
		XMVECTOR myPos = pTree_->GetWorldPos();

		//差を求めて、
		//移動方向を出し
			//移動を行う
		//自身のベクトル　―　対象　＝　対象から自身へ延びるベクトル
		XMVECTOR moveVec = XMVector3Normalize(myPos - targetPos);

		//移動スピード
		static constexpr float SPEED = 0.5f;

		float randomCode;
		//ランダム方向に移動
		if (rand() % 2)
		{
			randomCode = 1.0f;
		}
		else
		{
			randomCode = -1.0f;
		}

		//引き寄せる
		pTarget_->transform_.position_ += moveVec * SPEED * randomCode;



		//ターゲットのワールド座標と同様の位置にバインダーを移動
		pBindBinder_->SetWorldPos(pTarget_->GetWorldPos());
		
		*/

	}

}
//消去時の行動
	//レベル：オーバーライド
void BindTreeStrategy::ExecutionErase()
{

	//自身のインスタンスを削除依頼
	pTree_->EraseTreeStrategy();
	//自身の木の削除依頼
	pTree_->ForciblyEraseMe();


}
/******************************************************************/

