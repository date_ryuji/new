#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Tree/LSystemTree.h"		//LSYSTEMオブジェクト　木オブジェクトの形を作るクラス

//クラスのプロトタイプ宣言
class BoxCollider;
class GroupOfTree;
class TreeStrategy;
class NumberPlate;
//構造体のプロトタイプ宣言
struct TreeInfo;
//enum class のプロトタイプ宣言
enum class GAME_SCENE_COOP_OBJECTS;


//木オブジェクトの部位
	//詳細：木オブジェクトを成長、攻撃をされる際、その攻撃部位の選定を行う際に使用する
enum class TREE_PARTS
{
	TREE_TRUNK = 0,		//木の部位：幹
	TREE_ROOT,			//木の部位：根

	TREE_MAX,			//MAX
};

//木の種類
	//詳細：木の見た目を変更する際に使用する
enum class TREE_TYPES
{
	TREE_TYPE_DEFAULT = 0,	//通常（効果：範囲を広げるだけ）
	TREE_TYPE_DUMMY ,		//ダミー（効果：偽物）
	TREE_TYPE_BIND ,		//バインド（効果：敵拘束）
	TREE_TYPE_EATER,		//イーター（効果：敵捕食）

	TREE_TYPE_MAX,			//MAX
};



/*
	クラス詳細：木オブジェクト
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene->GroupOfTree
	クラス概要（詳しく）
				：LSYSTEMにより形作られる作られる木オブジェクト
				：敵に攻撃されている場合、木へのダメージを受ける
				：GroupOｆTreeにて管理される木オブジェクト

				：当たり判定のコライダーを持たせることで、他オブジェクト（プレイヤー、敵）との当たり判定を持たせる。
				：木ごとの行動によって、動的処理を持たせることができる。行動の処理はTreeStrategy(class)に一任する
				：消去時　消去モーションを実行する（一定時間をかけてモデルのα値を０に）

*/
class Tree : public LSystemTree
{
//protected メンバ定数
protected : 
	//成長の際の成長値
	static constexpr int GROW_VALUE_ = 20;
	//成長上限回数
	//成長上限回数　幹
	static constexpr int MAX_NUMBER_TRUNK = 5;
	//成長上限回数　根
	static constexpr int MAX_NUMBER_ROOT = 5;



//protected 構造体
protected : 

	//木の情報
		//詳細：木のステータス
		//　　：番号など
	struct TreeStatus
	{
		//幹HP
		int trunkHP;
		//根HP
		int rootHP;

		//木の番号
		int number;

		//現成長回数（幹）
		int numberOfTrunkGrowths;
		//現成長回数（根）
		int numberOfRootGrowths;

		//前回成長したときからの経過時間
			//詳細：経過時間が一定時間以上たった際に、再び、成長が可能となる
			//　　：成長が可能かどうかを判断するのは、木オブジェクトからではなく、
			//　　：→GroupOｆTreeのクラスでUpdate時に、時間を計測するなどして、判定する→その際、GroupOfTreeeで時間計測を行えばよくなるので、ここで持っておく必要がなくなる	
		float elpasedTime;


		//消去モーションのフラグ
			//詳細：すでにHPが0になり、
			//　　：消去モーションに入ったことを知らせるフラグ
			//　　：この際には、敵の攻撃対象にならず、消滅モーションだけを行う
			//　　：消滅モーション実行後、自身をKillMeをして、親（GroupOfTreeなどの管理クラス）から削除を依頼
		bool isErase;

		//コンストラクタ
		TreeStatus();

	}treeStatus_;

//protected メンバ変数、ポインタ、配列
protected : 

	//自身の木のタイプごとに使用するフラグ
		//詳細：通常：使用しない
		//　　：バインド：バインド中か
		//　　：イーター：捕食中か
	bool typeFlag_;
	//行動実行クラスの削除フラグ
	bool isEraseTreeStrategy_;

	//蓄積ダメージ
		//詳細：次のフレームのUpdateにて、自身に受けるダメージ
	int damageReceived_;
	
	//自身の親である、木群オブジェクトクラスのタイプをセットさせる
		//詳細：GroupOfTreeのタイプを識別する
	GAME_SCENE_COOP_OBJECTS parentType_;

	//自身の木のタイプを保存
		//詳細：木のタイプごとに
		//　　：木へ当たった際に、起こす自身の行動を決定する
	TREE_TYPES myTreeType_;


	//箱コライダー
	BoxCollider* pBoxColl_;

	//自身を管理する
		//木群オブジェクトクラス
	GroupOfTree* pGroupOfTree_;

	//番号プレート
	NumberPlate* pNumberPlate_;


	//自身の木のタイプごとの行動実行クラス
	TreeStrategy* pTreeStrategy_;

	//タイプごとのコライダーサイズ
	XMVECTOR* pColliderSizes;

//protected メソッド
protected :

	//蓄積ダメージの計算
		//引数：なし
		//戻値：なし
	void DamageReceived();

	//消滅判定
		//詳細：消滅判定を行い、
		//　　：消去モーションを開始できるならば、開始する
		//引数：なし
		//戻値：なし
	void IsStartEraseMotion();

	//自身がなくなっているか
		//引数：なし
		//戻値：なし
	bool IsDead();


	//自身が消去されるかの判定
		//詳細：消去される場合、消去モーションの開始を宣言する
		//引数：なし
		//戻値：なし
	void JudgeKillMe();

	//ルールをランダムで選択し返す
		//引数：なし
		//戻値：ルールの文字列
	std::string GetRandomRule();

	//引数ファイルパスにて示されたテキストを読み込みルールを返す
		//引数：ファイル名
		//戻値：ルールの文字列
	std::string GetRule(const std::string& FILE_NAME);

	//木ごとの行動クラスの実行
		//引数：なし
		//戻値：なし
	void ExecutionTreeStrategy();

	//自身の番号プレートのロード	
		//引数：読み取り番号（プレートへ描画する番号）
		//戻値：なし
	void LoadNumberPlate(int handle);

//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	Tree(GameObject* parent);
	
	//木の種類を選択した初期化
		//引数：木のタイプ
		//引数：木オブジェクトへエフェクトを追加するかのフラグ
		//戻値：なし
	void Initialize(TREE_TYPES type , bool addEffect);

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

	//コライダー衝突時に呼ばれる関数
		//詳細：コライダー同士の３D衝突判定にて、衝突した場合、衝突の基準コライダー（コライダー１と衝突するコライダーを探していた場合、コライダー１が基準のコライダー）を持つGameObjectの以下関数を呼び込む
		//レベル：オーバーライド
		//引数：自身のコライダー衝突した相手のポインタ
		//戻値：なし
	void OnCollision(GameObject* pTarget) override;

//public メソッド
public : 

	//消去モーション中か
		//引数：なし
		//戻値：消去モーション中か
	bool IsErase();


	//強制的に自身（木オブジェクト）消去する
		//詳細：自身の消去に伴い、解放処理を呼び込む
		//引数：なし
		//戻値：なし
	void ForciblyEraseMe();


	//蓄積ダメージ加算
		//詳細：とともに、自身へダメージを計算
		//　　：その際、自身のダメージで、木のHPを０にすることができたら、trueを返す
		//引数：ダメージ値
		//戻値：ダメージにより、木オブジェクトのHPを0にしたかのフラグ
	bool AddDamageAndIsDead(int damage);

	//自身のHPと番号をセットする
		//引数：幹HP
		//引数：根HP
		//引数：木オブジェクト番号（GroupOfTreeにおける自身（Tree）へのインスタンスハンドル）
		//戻値：なし
	void InitTreeStatus(int trunkHP , int rootHP , int number);

	//自身の木オブジェクトを所有するクラスをセット
		//引数：木群
		//引数：連携オブジェクトタイプ（木群のタイプ）
		//戻値：なし
	void SetMyGroupClass(GroupOfTree* pGroupOfTree , GAME_SCENE_COOP_OBJECTS parentType);

	//自身の木オブジェクト番号（GroupOfTreeにおける自身（Tree）へのインスタンスハンドル）を更新する
		//詳細：ほかの木が倒された際に、
		//　　：動的に、自身の番号を更新する	
		//引数：更新先の木オブジェクト番号
		//戻値：なし
	void UpdatedMyTreeNumber(int number);

	//新しいコライダーの作成
		//引数：描画先のTransform（コライダーの設置位置）
		//戻値：なし
	void AddNewCollider(Transform& trans);


	//当たり判定によるコライダーによる、消去の対象になるか
		//詳細：動的に削除する木オブジェクトであるかを取得
		//　　：上記の対象となるオブジェクトは、親オブジェクトタイプ(parentType_)にて、該当するGroupOfTreeのタイプ（GAME_COOP_OBJECT_TYEP）を所有している
		//　　：そのため、親のオブジェクトタイプを比較に掛けて、該当する親オブジェクトタイプを所有しているかのフラグにて上記対象オブジェクトであるかの判別を行う
		//引数：なし
		//戻値：対象であるか
	bool IsTargetToBeErased();


	//自身のコライダーの所有しているデバック描画用のワイヤーフレームのモデル番号を与える
		//詳細：与えたモデル番号から、モデルにアクセスし、モデルとレイとの当たり判定を行う
		//　　：当たり判定とレイキャストを行うようにし、レイとの計算を行うポリゴン数を極力少なくする
		//引数：なし
		//戻値：なし
	int GetMyColliderModelHandle();

	//自身のコライダーの所有しているデバック描画用のワイヤーフレームのモデル
		//詳細：描画時のTransformにセットさせる
		//引数：なし
		//戻値：なし
	void SetMyColliderTransformWhenDrawing();


	//自身の情報をセット（ポインタ）して返す
		//引数：木情報（ポインタ）
		//戻値：なし
	void SetTreeInfomation(TreeInfo* pTreeInfo);

	//木のパーツを成長させる
		//詳細：パーツ（幹、根）から成長させるパーツを一つを選び、成長させる
		//　　：引数にて指定された、値から、成長部位を選択
		//引数：木のパーツ
		//戻値：成長できたか
	bool GrowParts(TREE_PARTS part);

	//自身の木のタイプを渡す
	//引数：なし
	//戻値：自身の木のタイプ
	TREE_TYPES GetMyTreeType();

	//表示用の自身番号として適切なオリジンの番号を渡す
		//詳細：とある条件によって、自身のメンバとして持っている番号を更新させて、その番号を返す
		//　　：詳細は関数先にて
		//引数：なし
		//戻値：適切な表示用の自身の番号
	int GetUpdatedMyNumber();


	/*TreeStrategyより*********************************************************************/
	//引数敵オブジェクトが、敵スポーンに存在するか
		//詳細：現在行動を起こしたい　敵オブジェクトが存在するかを調べてもらう
		//　　：存在していたら、行動実行
		//　　：存在していなかったら、行動終了。とともに終了処理
		//引数：ターゲット敵オブジェクト
		//戻値：存在するか
	bool IsExistsTarget(GameObject* pTarget);

	//引数敵オブジェクトを強制的に削除する
		//引数：削除オブジェクト
		//戻値：なし
	void EraseEnemy(GameObject* pTarget);
	//強制的に指定オブジェクトを捕縛させる
		//詳細：捕縛：行動不能、スタン
		//　　：捕縛が起こる条件：バインドの木より捕縛。イーター木より捕縛。
		//引数：捕縛対象のオブジェクト
		//戻値：なし
	void ForciblyBindEnemy(GameObject* pTarget);
	//捕縛解除
		//詳細：強制的に捕縛状態となるが、
		//　　：その捕縛が解除される場合がある。→ほかの敵に捕縛の木が切り落とされる
		//　　：その場合、捕縛を解除
		//引数：捕縛解除対象のオブジェクト
		//戻値：なし
	void ForciblyUnBindEnemy(GameObject* pTarget);


	//木ごとの行動クラスの削除実行
		//詳細：行動クラスからの呼び出しにより、実行されるため、このフレームでの削除は行わない
		//引数：なし
		//戻値：なし
	void EraseTreeStrategy();



};