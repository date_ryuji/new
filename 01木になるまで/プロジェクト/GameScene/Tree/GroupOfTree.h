#pragma once
//システム内　標準ヘッダ(usr/)
#include <list>		//リストを使用するためにインクルード（途中で中身を消去する可変長配列）
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス

//クラスのプロトタイプ宣言
class Tree;
class GameSceneManager;
class PlannedLocationOfTree;
//enum class のプロトタイプ宣言
enum class GAME_SCENE_COOP_OBJECTS;
enum class TREE_PARTS;

/*
	構造体詳細	：木情報　構造体
	構造体概要（詳しく）
				：木の情報群
				　木へのアクセス、木自体のステータスを担う情報

				  プレイヤーの所有する（プレイヤーが生成した）木一つ一つに持たせる情報
*/
struct TreeInfo
{
	//木番号
	int number;

	//残り幹HP
	int rootHP;
	//残り根HP
	int trunkHP;

	//コンストラクタ
	TreeInfo();

};


/*
	クラス詳細：木群　木オブジェクトをまとめて管理するクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：木オブジェクトのインスタンスの所有、描画を管理するクラス

				：木の設置ポリゴン位置（地面を作るポリゴンを番号で管理する。地面に立つ位置をポリゴン番号で管理する）
				　を木オブジェクトごとに管理しておく

				：LSYSTEMによって、作成した、木オブジェクト（Tree）を複数所有しておくクラス
				　データを何個も所有しないように、
				　共通データ（Treeのインスタンスは共通で使えるならば、インスタンスを共通して使う）を使い、
				　出現位置だけを変えて、管理させる方式をとるクラス


				：背景用の木群としても使用する
				　背景用である場合
				　※あくまで、動的に変化のないオブジェクトに限る。
				　※あくまで、管理するオブジェクトは、同様のオブジェクトであることに限る。
				　※木オブジェクトクラスを複数持つのではなく、
				　木のオブジェクトクラスは一つで、オブジェクトの管理するモデルをどこに描画するかのTransformを、複数所持しておく


*/
class GroupOfTree : public GameObject
{
//private メンバ定数
private : 
	//生成可能個数
		//詳細：プレイヤーの所有木グループ限定の　木生成可能個数
		//　　：StageDataにて管理する方がよさそう。ゲームルール、ステージ難易度を決めるような内容は外部でまとめておくことが理想
	static constexpr int MAXIMUM_NUMBER_ = 20;

	//木のサイズ
	static constexpr XMVECTOR DEFAULT_SCALE_ = { 1.0f, 1.0f, 1.0f, 0.0f };


//private 構造体
private:
	/*
	構造体詳細	：描画のTransformと対象の描画対象の木オブジェクトを登録しておく構造体
	構造体概要（詳しく）
				：木オブジェクトごとの描画位置、木オブジェクトが地面ポリゴンにおけるどの位置に生成されているか示す
	*/
	struct DrawTreeInfo
	{
		//描画のTransform
		Transform* pSourceTransform;

		//描画対象の木オブジェクトのポインタ
		Tree* pTreeSource;

		//初期設置時のポリゴン番号
		int polyNum;


		//コンストラクタ
		DrawTreeInfo();
	};


//private メンバ変数、ポインタ、配列
private : 

	//時間停止のフラグ
	//時間による行動、移動、攻撃、HP現象などなど様々な動的要因を停止する
	bool isStopTime_;

	//現在の生成個数
	int currentNumberOfTrees_;

	//木オブジェクトクラスのカウンター
	//木のオブジェクトクラスが生成されたらカウントアップする
	unsigned int treeNumCounter_;


	//自身のタイプ
	GAME_SCENE_COOP_OBJECTS myType_;

	//シーンマネージャー
	GameSceneManager* pSceneManager_;



	//立地予定の木オブジェクト
		//詳細：木を配置する前に、配置予定一のポリゴンに木を描画し、立地予定位置をプレイヤーに知らせるためのもの
	PlannedLocationOfTree* pPlannedLocation_;

	//描画対象の木オブジェクト
		//詳細：クラス内で共通のオブジェクトを使う場合に、GroupOfTreeのクラスが最初に定義されたときに必ず定義するオブジェクト
		//　　：★描画の対象のLSYSTEMが指定されていない場合、この初めに定義したLSYSTEMが描画の対象となる
		//　　：全て共通の形を持たせた木オブジェクトを描画させるので、
		//　　：クラスは一つだけ用いて、
		//　　：クラスは、GameObjectクラスなので、オブジェクトクラスのTransformをTransform群に登録されているTransformに動的に変化させて
		//　　：Drawを呼びこむ
		//　　：→そうすれば、　クラスは一つで、違う位置に、同様のモデルをDrawできる
	Tree* pTreeSources_;

	//描画対象の木オブジェクト群
		//詳細：登録された木オブジェクト群
		//　　：（上記のため、描画に使用するTreeは重複する場合がある）
	std::list<DrawTreeInfo*> pSourceTransforms_;

	//木インスタンス群
		//詳細：描画対象内に存在する木オブジェクトのポインタ群
		//　　：描画対象は、下記の木オブジェクト群の中から、木オブジェクトを選択して構造体に格納している
		//　　：リストに登録されているオブジェクトは唯一無二である
	std::list<Tree*> pListTreeSources_;

//private メソッド
private : 
	//時間が停止しているか
		//引数：なし
		//戻値：停止
	bool IsStopTime();
	//時間の停止（行動、移動、攻撃などの停止）
		//引数：なし
		//戻値：なし
	void StopTime();
	//時間の再開（行動、移動、攻撃などの再開）
		//引数：なし
		//戻値：なし
	void StartTime();


	//LSYSTEM実行可能な木へのLSYSTEM実行
		//詳細：管理しているすべての木オブジェクトを調べて、LSYSTEM可能である木オブジェクトを判別して、
		//　　：可能と判断された木オブジェクトのLSYSTEM実行
		//引数：なし
		//戻値：なし
	void ExecutionLSystem();

	//木オブジェクト作成
		//詳細：GroupOfTreeにて管理する初期Treeの作成
		//　　：GroupOfTreeは、必ず一つの木オブジェクトを持った状態で始めなくてはならない。そのため、木オブジェクト一つ目の作成
		//引数：なし
		//戻値：なし
	void CreateAFirstTree();

//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	GroupOfTree(GameObject* parent);
	//デストラクタ
		//レベル：オーバーライド	
		//引数：なし
		//戻値：なし
	~GroupOfTree() override;

	//初期化
		//自身のタイプを選択して、
		//セットする
		//引数：GroupOfTreeのタイプ（GAME_SCENE_COOP_OBJECTS(enum)）
		//戻値：なし
	void Initialize(GAME_SCENE_COOP_OBJECTS myType);


	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//時間の停止
		//詳細：自身の管理するすべての木オブジェクトへ
		//　　：時間の停止を宣言
		//　　：Treeのインスタンス全てへ、時間停止を宣言
		//引数：なし
		//戻値：なし
	void StopTimeAllTree();

	//時間の計測の再開
		//詳細：自身の管理するすべての木オブジェクトへ
		//　　：時間の計測再開を宣言
		//引数：なし
		//戻値：なし
	void StartTimeAllTree();

	//強制的に自身（木オブジェクト）消去する
		//引数：木オブジェクトへのハンドル番号
		//戻値：なし
	void ForciblyEraseTheTree(int handle);

	//木を生成できるか
		//引数：なし
		//戻値：木が生成できるか
	bool IsCreateTreeLimitExceeded();
	//残り生成可能個数
		//引数：なし
		//戻値：残り生成可能木本数
	int RemainingNumberOfCreateTree();
	//木オブジェクトの解放
		//詳細：引数オブジェクトを解放
		//　　：ほかのクラスで使われない前提のクラスを解放する
		//　　：引数オブジェクトを自身の木群から探して、存在する場合、解放を行う
		//引数：消去対象木オブジェクト
		//戻値：なし
	void ReleaseATree(Tree* pSource);


	
	//リストへ木のオブジェクトを追加
		//引数：追加のTransform
		//引数：木の立地ポリゴン番号
		//引数：追加するLsystemのSourceのインスタンスクラス（選択がない場合、nullptrとする）（必須：LsystemのポインタはInstantiateにて制作されて、親は、必ず自身のGroupOfTreeとする（理由は、解放時に、LsystemObjectの解放の手間をなくすため）（Instantiateで確保されたものであれば、解放は必ずされるので、問題ないが、LsystemObjectの親は、GroupOfTreeであると、確定されたほうが、解放のタイミングが追いやすい））
		//戻値：木オブジェクトへのハンドル
	int AddTreeObject(Transform& trans , int polyNum ,  Tree* pSource = nullptr);

	//オブジェクトを成長させる
		//詳細：外部から成長指示をされた場合、
		//　　：成長部位と、成長する木へのハンドルにて成長する木を判別する
		//引数：木へのハンドル
		//引数：成長さえる木のパーツ
		//戻値：成長できたか
	bool GrowTreeParts(int num , TREE_PARTS part);

	//LsystemObjectの初期設定
		//詳細：木オブジェクトとして、生成するときの初期設定もろもろを委託する
		//　　：ポインタのポインタ、つまり、ポインタの参照渡しをして、関数先で、ポインタのインスタンスを生成して、引数に代入してもらう
		//引数：木の種類（通常の木、イーター、バインド）
		//引数：木のポインタのポインタ
		//引数：木にエフェクトを付加するか（指定のない場合=true）
		//戻値：なし
	void InitTreeObject(int type , Tree** pSource , bool isAddEffectByType = true);

	//木の初期化
		//引数：木のポインタのポインタ
		//戻値：なし
	void InitStatus(Tree ** pSource);

	//木オブジェクトのScale値を拡大させる
		//詳細：木オブジェクトの動的成長を見せる
		//　　：サイズを大きくする
		//引数：成長値
		//戻値：なし
	void IncreaseScale(float increaseValue);

	//デフォルトのサイズを取得する
		//引数：なし
		//戻値：デフォルトサイズ値
	XMVECTOR GetDefultScale();

	//自身の管理する木オブジェクトと当たり判定を行う
		//詳細：詳しくは、木オブジェクトの所有するコライダーと衝突判定を行うことで、低ポリゴンのモデルとの衝突判定を行うことができる
		//引数：なし
		//戻値：当たり判定の結果（衝突をしていた場合、衝突した木の番号）
	TreeInfo RayCastToAllData();

	//自身の管理する木オブジェクトと当たり判定を行う
		//詳細：引数にて、レイを飛ばす座標をもらい
		//　　：その座標へ向けて、真上からレイを飛ばす、そのレイと衝突するオブジェクトがあるかの判定
		//引数：なし
		//戻値：当たり判定の結果（衝突した：True、衝突しなかった：False）
	bool RayCastToAllData(XMVECTOR targetWorldPos);


	//木が存在するポリゴン番号をすべて知る
		//詳細：引数にて渡した、ベクターへ、木が存在するポリゴン番号を代入してもらう
		//　　：ゲームマップを作る際に、木の存在するポリゴン番号を取得する。その際に使用する
		//引数：立地するポリゴン番号群
		//引数：木のタイプ群
		//戻値：なし
	void GetPolygonsWithTrees(std::vector<int>* polygonsWithTrees, std::vector<int>* treeTypes);

	/*TreeStrategyより*********************************************************************/
	//引数敵オブジェクトが、敵スポーンに存在するか
		//詳細：現在行動を起こしたい　敵オブジェクトが存在するかを調べてもらう
		//　　：存在していたら、行動実行
		//　　：存在していなかったら、行動終了。とともに終了処理
		//引数：ターゲット敵オブジェクト
		//戻値：存在するか
	bool IsExistsTarget(GameObject* pTarget);

	//引数敵オブジェクトを強制的に削除する
		//引数：削除オブジェクト
		//戻値：なし
	void EraseEnemy(GameObject* pTarget);
	//強制的に指定オブジェクトを捕縛させる
		//詳細：捕縛：行動不能、スタン
		//　　：捕縛が起こる条件：バインドの木より捕縛。イーター木より捕縛。
		//引数：捕縛対象のオブジェクト
		//戻値：なし
	void ForciblyBindEnemy(GameObject* pTarget);
	//捕縛解除
		//詳細：強制的に捕縛状態となるが、
		//　　：その捕縛が解除される場合がある。→ほかの敵に捕縛の木が切り落とされる
		//　　：その場合、捕縛を解除
		//引数：捕縛解除対象のオブジェクト
		//戻値：なし
	void ForciblyUnBindEnemy(GameObject* pTarget);


	/*木への攻撃***************************************************************************************/
	//引数ポリゴン番号に立っている、植わっている木があるならば、その木の番号を受け取る
		//詳細：その木の番号をもとに、木グループにアクセスして、該当木にダメージを与えるまで至る
		//引数：ターゲットとなる木がたっているポリゴン番号（敵のナビゲーションの最終目的ポリゴン番号）
		//引数：ターゲットへ与えるダメージ
		//引数：攻撃で木を倒したか（HPを０にしたか）
		//引数：倒した場合、その倒した木の番号
		//戻値：攻撃に成功したか（true : 攻撃成功。攻撃対象が存在する。 false:攻撃失敗。攻撃対象が存在しない）
	bool AttackSuccess(const int TARGET_POLY_NUM, const int DAMAGE, bool* isKill, int* treeNum);

	/*立地予定木関連****************************************************************************/
	//立地予定木の作成
		//詳細：立地予定の木を視認できるように
		//　　：立地予定ポリゴン位置にワイヤーフレームにかたどれられた木オブジェクトを秒差させる
		//引数：立地ポリゴン番号から求められた設置ワールド座標
		//戻値：なし
	void CreatePlannedLocationOfTree(XMVECTOR& setPos);

	//立地予定木の削除
		//引数：なし
		//戻値：なし
	void ErasePlannedLocationOfTree();


};
