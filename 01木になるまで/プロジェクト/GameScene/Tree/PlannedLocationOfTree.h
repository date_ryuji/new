#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Tree/LSystemTree.h"		//LSYSTEMオブジェクト　木オブジェクトの形を作るクラス

/*
	クラス詳細：立地予定の木オブジェクトクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene->GroupOfTree
	クラス概要（詳しく）
				：現在クリックされている地面位置に、今後木が立つことになるため、
				　その立地予定の位置に、仮として、ワイヤーフレームに囲まれている木を描画し、
				  立地予定の位置をプレイヤーに示す目的のクラス

				：あくまでも立地予定の木であるため、
				　描画と描画アニメーション以外の機能を持たない

				：当たり判定も消去判定も時間停止も必要なし

				：ただし、木のグラフィックを作成するには、
				　LSYSTEMの機能が必要であるため、LSYSTEMを継承する

*/
class PlannedLocationOfTree : public LSystemTree
{
//private メンバ変数、ポインタ、配列
private :
	//木の看板プレート
	int hPlate_;

//private メソッド
private : 
	//プレート番号を作成する
	//引数：なし
	//戻値：なし
	void CreatePlate();
	//木を作成する
		//引数：なし
		//戻値：なし
	void CreateTree();
	//シェーダーを切り替える
		//引数：なし
		//戻値：なし
	void ChangeShader();


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	PlannedLocationOfTree(GameObject* parent);

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;
//public メソッド
public : 

	//描画ピクセル位置から描画位置を取得する
		//引数：描画位置
		//戻値：なし
	void SetDrawPosition(XMVECTOR& setPos);

};
