#include "BindTreeBinder.h"				//ヘッダ
#include "../../Engine/Model/Model.h"	//モデル


//コンストラクタ
BindTreeBinder::BindTreeBinder(GameObject* parent)
	: GameObject(parent, "BindTreeBinder"),
	hModel_(-1),
	startBindPos_(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}

//初期化
void BindTreeBinder::Initialize()
{
	//モデルのロード
	hModel_ = Model::Load("Assets/Scene/Model/GameScene/TreeSource/Bind.fbx",
		POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_TEST);
	//警告
	assert(hModel_ != -1);
}

//更新
void BindTreeBinder::Update()
{
}

//描画
void BindTreeBinder::Draw()
{
	//描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//解放
void BindTreeBinder::Release()
{
}
//バインドスタート位置のセット
void BindTreeBinder::SetStartBindPos(XMVECTOR startBindPos)
{
	startBindPos_ = startBindPos;
}
//バインドスタート位置のゲット
XMVECTOR BindTreeBinder::GetStartBindPos()
{
	return startBindPos_;
}
