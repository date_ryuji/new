#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス

/*
	クラス詳細：バインド用の木　エフェクト（バインダー）
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：バインド用の木にバインダーエフェクトのモデルを付加させるクラス
				：木ごとのタイプ行動クラスから生成管理される
				：行動クラスから毎フレーム呼び込まれ、
				　任意の動きを行う

				：バインドのつる、ひも　バインドを行うモデル
				　敵モデルを拘束する


*/
class BindTreeBinder : public GameObject
{
//private メンバ変数、ポインタ、配列
private:

	//モデルハンドル
	int hModel_;

	//バインド開始位置
	XMVECTOR startBindPos_;


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	BindTreeBinder(GameObject* parent);

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//バインド開始位置をセット
		//引数：開始位置のセット
		//戻値：なし
	void SetStartBindPos(XMVECTOR startBindPos);
	
	//バインド開始位置をゲット
		//引数：なし
		//戻値：バインド開始位置
	XMVECTOR GetStartBindPos();



};