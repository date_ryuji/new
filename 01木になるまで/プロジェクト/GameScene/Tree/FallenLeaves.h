#pragma once
//システム内　標準ヘッダ(usr/)
#include <list>		//リストを使用するためにインクルード（途中で中身を消去する可変長配列）
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス


/*
	クラス詳細：落ち葉クラス　木オブジェクトから発生する落ち葉を再現するクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene->Tree
	クラス概要（詳しく）
				：落ち葉クラス
				  動的に落ち葉となる平面ポリゴンを作成し、
				  時間とともに、下へ落下させる
				  その平面を木オブジェクトとともに描画させることで、
				  その木から発生する落ち葉と視覚的に見せる



*/
class FallenLeaves : public GameObject
{
//private 構造体
private : 
	/*
	//基準位置
		//落ち葉の描画位置、
		//落ち葉の消去位置などなどを決める際の基準位置
		//だが、この位置は、親が木オブジェクトであるならば、その親の原点からのローカル座標を自身は所有しているため、
		//基準の位置を持たずとも、親からのローカルという時点で基準となる
	*/
	/*
		構造体詳細	：落ち葉ごとの描画位置
		構造体概要（詳しく）
					：落ち葉一枚のインスタンスごとの描画位置を格納する構造体

	*/	
	struct DrawInfo
	{
		//座標
		XMVECTOR pos;
		//回転値
		XMVECTOR rotate;

		//符号
			//+ : +1.f
			//- : -1.f
		XMVECTOR cord;

		//コンストラクタ
		DrawInfo(XMVECTOR setPos) :
			DrawInfo(setPos, XMVectorSet(0.f, 0.f, 0.f, 0.f), XMVectorSet(1.f, -1.f, 1.f, 0.f))
		{
		}

		DrawInfo(XMVECTOR setPos, XMVECTOR setRotate, XMVECTOR setCord) :
			pos(setPos),
			rotate(setRotate),
			cord(setCord)
		{
		}
	};


//private メンバ変数、ポインタ、配列
private : 

	//時間停止のフラグ
	//他のオブジェクトが止まっているときに、あえて、動き続けるのも、演出としても良いが、
	//それは不自然なので、時間停止を実現する
	bool isStopTime_;

	//落ち葉の描画に用いる平面ポリゴンのモデル番号
	int hModel_;

	//合計経過時間
		//新しい落ち葉を生成する際に使用する
	float totalElpasedTime_;


	//描画位置群
	std::list<DrawInfo> drawPoses_;




//private　メソッド
private : 
	//落下処理かつ回転
		//引数：なし
		//戻値：なし
	void FallenAndRound();

	//タイマーが動いているか
		//引数：なし
		//戻値：なし
	bool IsStopTimer();
	//落ち葉の作成が可能かの判断
		//詳細：作成可能であれば、作成。作成不可能であれば、何もしない。
		//引数：なし
		//戻値：なし
	void CanAddNewFallenLeave();
	//落ち葉の作成
		//引数：なし
		//戻値：なし
	void CreateFallenLeave();


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	FallenLeaves(GameObject* parent);

	//落ち葉のテクスチャを指定して描画先の平面のロード
		//引数：落ち葉のテクスチャ
		//戻値：なし
	void Load(const std::string& TEXTURE_FILE_NAME);

	//初期化
				//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
				//レベル：オーバーライド
				//引数：なし
				//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 
	//時間停止
		//引数：なし
		//戻値：なし
	void StopTimer();
	//時間計測再開
		//引数：なし
		//戻値：なし
	void StartTimer();

};
