#include "EffectTree.h"						//ヘッダ
#include "../../Engine/DirectX/Direct3D.h"	//Direct3D　シェーダークラス取得
#include "../../Engine/Model/Model.h"		//モデル
#include "../../Shader/ScrollShader.h"		//専用シェーダー（テクスチャUVの動的移動）



//コンストラクタ
EffectTree::EffectTree(GameObject* parent)
	: GameObject(parent, "EffectTree"),
	hModel_(-1),
	scrollPower_(0.f)
{
}

//初期化
void EffectTree::Initialize()
{
}

//ロード
void EffectTree::Load(const std::string& FBX_FILE_NAME)
{
	//条件：ファイル名がない場合
	if (FBX_FILE_NAME == "")
	{
		//処理終了
		return;
	}

	//ロード
	hModel_ = Model::Load(FBX_FILE_NAME, POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_SCROLL);
	//警告
	assert(hModel_ != -1);

	//初期サイズの可変
	transform_.scale_ = XMVectorSet(5.0f, 5.0f, 5.0f, 0.f);
}

//更新
void EffectTree::Update()
{
	//移動値を加算する
	scrollPower_ += SPEED_;

	//条件：移動値が１．０ｆ(UVにおける最高値)を超えた場合
	if (scrollPower_ >= 1.f)
	{
		//0で初期化
		scrollPower_ = 0.f;
	}
}

//描画
void EffectTree::Draw()
{
	//条件：モデルが存在する場合
	if (hModel_ != -1)
	{

		//コンスタントバッファ2つ目に代入
		ScrollShader* pScrollShader = (ScrollShader*)Direct3D::GetShaderClass(SHADER_TYPE::SHADER_SCROLL);

		//コンスタントバッファにセット
		ScrollShader::CONSTANT_BUFFER_1 cb1;
		cb1.scrollPower = scrollPower_;

		//セット
		pScrollShader->SetConstantBuffer1Data(&cb1);

		//描画
		Model::SetTransform(hModel_, transform_);
		Model::Draw(hModel_);

	}
}

//解放
void EffectTree::Release()
{
}

//自身のTransformサイズを引数コライダーサイズに沿わせる
void EffectTree::SetScaleWithColliderSize(XMVECTOR colliderSize)
{
	transform_.scale_.vecX = colliderSize.vecX / 5.f;
	transform_.scale_.vecZ = colliderSize.vecZ / 5.f;

}
