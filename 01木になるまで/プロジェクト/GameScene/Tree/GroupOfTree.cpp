#include "GroupOfTree.h"						//ヘッダ
#include "../../CommonData/GlobalMacro.h"		//共通マクロ
#include "../../Engine/Algorithm/RayCaster.h"	//レイキャスト実行クラス
#include "../Scene/GameSceneManager.h"			//シーンマネージャー
#include "Tree.h"								//木オブジェクトクラス
#include "PlannedLocationOfTree.h"				//立地予定木


//コンストラクタ
GroupOfTree::GroupOfTree(GameObject * parent)
	: GameObject(parent, "GroupOfTree"),
	isStopTime_(false),
	currentNumberOfTrees_(0),
	pTreeSources_(nullptr),
	treeNumCounter_(0),	//1オリジン
	pPlannedLocation_(nullptr) , 
	pSceneManager_(nullptr) , 

	myType_(GAME_SCENE_COOP_OBJECTS::GAME_COOP_BACK_GROUND_TREE)
{
	//可変配列のクリア
	pSourceTransforms_.clear();
	pSourceTransforms_.clear();
}

//デストラクタ
GroupOfTree::~GroupOfTree()
{
}

//初期化
void GroupOfTree::Initialize()
{
}


void GroupOfTree::Initialize(GAME_SCENE_COOP_OBJECTS myType)
{
	myType_ = myType;

	{
		//シーンマネージャーの取得
			//シーンチェンジャーを経由して
				//引数指定のシーンのマネージャークラスを取得
		pSceneManager_ = (GameSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);

		//自身をセットする
		pSceneManager_->AddCoopObject((int)myType_, this);
	}

	//LSystemの木オブジェクト1つを取得
	CreateAFirstTree();
}

//更新
void GroupOfTree::Update()
{
	//条件：時間停止でないとき
	if (!(IsStopTime()))
	{
		//木のLSYSTEM（枝生成）を実行
		ExecutionLSystem();

		//木のサイズの動的可変
		IncreaseScale(0.001f);
	}
}

//描画
void GroupOfTree::Draw()
{
	//条件：描画対象のイテレータの最後の要素ではない（Transformの可変長配列に登録されている分だけ）
		//詳細：可変長配列に登録されているオブジェクトのポインタでDrawを呼びこむ
		//　　：ここにおけるDrawとは、GameObjectにおけるDrawではなく、Lsystemのポインタが持っている、専用のDraw関数である。
		//　　：描画
		//　　：可変長配列に登録されている、Transformで、描画位置をセットして、
		//　　：任意の座標に描画させる
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end(); itr++)
	{
		//イテレータにて示される
		//構造体のオブジェクトポインタを参照
			//引数に、同構造体のTransformを送り、Transformの位置にDrawさせる
		(*itr)->pTreeSource->DrawBranchesMakeTree(*((*itr)->pSourceTransform));
		
		//輪郭の描画
			//輪郭におけるTransformの変化は関数先にて計算する
		(*itr)->pTreeSource->DrawOutLineForBranchesMakeTree(*((*itr)->pSourceTransform));

	}

}

//解放
void GroupOfTree::Release()
{
	//条件：描画対象のイテレータの最後の要素ではない（Transformの可変長配列に登録されている分だけ）
	//動的確保の全要素の解放
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end();)
	{
		//動的確保したTransformの解放
		SAFE_DELETE((*itr)->pSourceTransform);

		{
			//対象のオブジェクトポインタは解放をする必要がない
				//なぜならば、オブジェクトのポインタはGameObjectクラスを継承していて、
				//なおかつ、GroupOfTreeの子供として生成しているので、（仮に、他のオブジェクトが親だとしても、ここで解放してはいけない（なぜならば、その人が解放するときに、すでに存在しないところを解放しようとするから））
				//GroupOfTreeが解放されたら、解放されるようになる。
				//なので、ポインタの解放はせずに、ここでは、ポインタを使えないように、nullptrに変えるだけ
			(*itr)->pTreeSource = nullptr;
		}

		//構造体自体の解放
		SAFE_DELETE((*itr));

		//イテレータから
		//自身の解放
			//解放して、
			//次の要素のイテレータを返してもらう
		itr = pSourceTransforms_.erase(itr);

	}

	//リストを空に
	pSourceTransforms_.clear();
}

//時間停止をされているか
bool GroupOfTree::IsStopTime()
{
	return isStopTime_;
}
//時間停止
void GroupOfTree::StopTime()
{
	isStopTime_ = true;
}
//時間計測再開
void GroupOfTree::StartTime()
{
	isStopTime_ = false;
}

//木オブジェクト作成
void GroupOfTree::CreateAFirstTree()
{
	//条件：メンバTreeのインスタンスが生成されていない場合
	if (pTreeSources_ == nullptr)
	{
		//インスタンスの生成

		//pTreeSources_ = (Tree*)Instantiate<Tree>(this);
		
		//引数に渡したポインタに
			//生成したインスタンスを返してもらう、かつ、
			//ポインタの参照渡し　そして、ポインタのポインタで受け取り
		//LsystemObjectの初期化を行う
			//Transform値の調整などなど
			//描画のための設定を行ってもらう
		//引数：木のタイプ
		//引数：木のポインタのポインタ
		//引数：エフェクトを追加するか（この木はソースとなる木のため、エフェクトは追加しない）
		InitTreeObject((int)TREE_TYPES::TREE_TYPE_DEFAULT, &pTreeSources_ , false);
	}

}

//木オブジェクトの解放
void GroupOfTree::ReleaseATree(Tree * pSource)
{
	//条件：描画対象のイテレータの最後の要素ではない（Transformの可変長配列に登録されている分だけ）
	//引数オブジェクトと該当するオブジェクトを探し
	//解放する
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end();)
	{
		//条件：イテレータにて示されるオブジェクト　と　引数オブジェクトが同様の場合
		if ((*itr)->pTreeSource == pSource)
		{
			//動的確保したTransformの解放
			SAFE_DELETE((*itr)->pSourceTransform);
			
			{
				//対象のオブジェクトポインタは解放をする必要がない
					//なぜならば、オブジェクトのポインタはGameObjectクラスを継承していて、
					//なおかつ、GroupOfTreeの子供として生成しているので、（仮に、他のオブジェクトが親だとしても、ここで解放してはいけない（なぜならば、その人が解放するときに、すでに存在しないところを解放しようとするから））
					//GroupOfTreeが解放されたら、解放されるようになる。
					//なので、ポインタの解放はせずに、ここでは、ポインタを使えないように、nullptrに変えるだけ
				(*itr)->pTreeSource = nullptr;
			}

			//木オブジェクトが立っていた、ポリゴン番号を知らせて、
			//そのポリゴンから、範囲を広げた、草オブジェクトを縮小させる
			int polyNum = (*itr)->polyNum;
			//条件：ポリゴン番号が定義されている場合
			if (polyNum != -1)
			{
				//解放したポリゴン番号を
					//GameSceneManagerに伝える
					//シーンマネージャーの取得
				//シーンチェンジャーを経由して
					//引数指定のシーンのマネージャークラスを取得
				GameSceneManager* pSceneManager_ = (GameSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);
				pSceneManager_->StartShrinkingGrass(polyNum);
			}

			//構造体自体の解放
			SAFE_DELETE((*itr));

			//リストから解放
			itr = pSourceTransforms_.erase(itr);

			//カウンターのカウントダウン
			treeNumCounter_--;

			//ほかのクラスで使用している可能性もあるため、
				//一つ解放しても終わらない

			//プレイヤー専用の所有木オブジェクト数のカウントダウン
			currentNumberOfTrees_--;

		}
		else
		{
			//イテレータを回す
			itr++;
		}
	
	}

	//木番号を更新
		//詳細：全体の木オブジェクトクラスへ
		//　　：番号の更新を伝える
		//制限：0オリジン
	int num = 0;


	//条件：管理木オブジェクトのイテレータの最後の要素ではない（重複しないTreeオブジェクト）
	//どう繰り返しにて、木オブジェクトクラスが引数と同様のものを持っていたら、解放する
	for (auto itr = pListTreeSources_.begin(); itr != pListTreeSources_.end();)
	{
		//条件：イテレータにて示されるオブジェクト　と　引数オブジェクトが同様の場合
		if ((*itr) == pSource)
		{
			//リストから解放
			itr = pListTreeSources_.erase(itr);
		}
		else
		{

			//番号を伝える
				//詳細：番号が今後木オブジェクトへのアクセスハンドルとなる
				//　　：消去された分番号が繰り下がる
			(*itr)->UpdatedMyTreeNumber(num);

			//番号の更新
			num++;

			//イテレータの更新
			itr++;
		}
	}

}

//リストへ木のオブジェクトを追加
int GroupOfTree::AddTreeObject(Transform & trans , int polyNum ,  Tree* pSource)
{
	//引数Transformを
	//可変配列に追加

	//あくまで追加するといっても、
		//引数の値をそのまま代入するのではなく、
		//新しく動的確保した領域に、コピーさせる

	//サイズの取得
	size_t size = (sizeof(trans));

	//リストに登録する要素の動的確保
	Transform* transform = new Transform;


	//Transformの初期値を変更

	//コピー
	//引数： to
	//引数： original
	//引数： size
	memcpy(transform, &trans, size);


	//可変配列に登録する
	//構造体の確保
	DrawTreeInfo* pData = new DrawTreeInfo;
	//Transform	//ポインタのコピー
	pData->pSourceTransform = transform;
	pData->pSourceTransform->Calclation();

	//ポリゴン番号の更新
	pData->polyNum = polyNum;


	//条件：描画対象のオブジェクトとして引数ポインタのアドレスがnullptrの場合
	//　　：描画対象オブジェクトが示されていない場合
	if (pSource == nullptr)
	{
		//引数のpSourceがnullptrの場合
			//クラスのInitializeにて確保した
			//Lsystemのクラスインスタンス、オブジェクトを登録する
		pData->pTreeSource = pTreeSources_;
	}
	//条件：描画対象オブジェクトが示されている場合
	else
	{
		//引数にて、新しいオブジェクトが指定されたとき
		//そのポインタを登録
		pData->pTreeSource = pSource;


		//オブジェクトのTransofmrを描画位置にセットする
		pData->pTreeSource->transform_.position_ = pData->pSourceTransform->position_;


		//クラス内で確保しているオブジェクト以外の	
					//新たに確保されたオブジェクトだったら
				//木オブジェクトのソースに
				//新たな描画先が追加されたということで、
				//コライダーの追加を行う
				//実際、pTreeSource事態は移動せずに、Transformを移動して、描画を行っている、
				//そのため、描画先々に、新たなコライダーを必要とする
		pData->pTreeSource->AddNewCollider(*(pData->pSourceTransform));

		
		//木のオブジェクト個数をカウントアップ
		currentNumberOfTrees_++;

	}

	//コピー先の動的確保した要素を、可変配列にコピー
	pSourceTransforms_.push_back(pData);

	//ハンドル番号を返す
	return (int)pSourceTransforms_.size() - 1;

}

//オブジェクトを成長させる
bool GroupOfTree::GrowTreeParts(int num, TREE_PARTS part)
{
	//描画対象オブジェクトの初期イテレータ
	auto itr = pSourceTransforms_.begin();

	//引数指定番号の要素のパーツを成長させる
	//カウンター
	int i = 0;
	//条件：カウンターと引数番号が同じではない場合
	while (i != num)
	{
		itr++;
		i++;
	}

	//成長呼び込み
	return (*itr)->pTreeSource->GrowParts(part);
}

//LsystemObjectの初期設定
void GroupOfTree::InitTreeObject(int type, Tree** pSource, bool isAddEffectByType)
{
	//インスタンスの生成
		//引数にて渡された、ポインタのポインタ（つまり、ポインタのアドレス）によって、
		//関数呼び出し元の宣言したポインタに要素を入れる
		//つまり、ポインタの参照渡しの実装を行っている
	(*pSource) = (Tree*)Instantiate<Tree>(this);

	//自身の木の木のグループによって
	//エフェクトをつけるかつけないかのフラグを立てる
	bool isAddEffect = false;

	//条件：自身がプレイヤー専用木である場合
	//　　：&&
	//　　：初期段階のエフェクトを追加するかのフラグが立っている場合（立っていないときは、その木がソースの木であるとき）
	if (myType_ == GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER && isAddEffectByType)
	{
		//エフェクトを追加
		isAddEffect = true;
	}

	//初期化呼び込み
		//引数：通常の木
	(*pSource)->Initialize((TREE_TYPES)type , isAddEffect);

	//スケール値セット
	(*pSource)->transform_.scale_ = XMVectorSet(0.5f, 0.5f, 0.5f, 0.0f);

	//枝を生成
		//この枝生成を呼びこまないと、最初の枝ができないので注意
		//後のLSYSTEMを実行できない
	(*pSource)->CreateTheFirstOne();

	
	//描画のフラグを下ろす
		//描画は、こちらで任意のタイミングで行う
		//正しくは、GroupOfTreeの描画のタイミングで描画する。
		//そのため、ゲームループのDrawではDrawしないようにする
	//(*pSource)->Invisible();


	//木のステータスの選定
	InitStatus(pSource);

	//木オブジェクトクラスに
		//自身（木群を管理するオブジェクト）をセット
		//自身のシーンマネージャーにおけるタイプをセットする
	(*pSource)->SetMyGroupClass(this , myType_);

	//木オブジェクトを管理するリストへ自身オブジェクトを追加
	//リストに追加
	pListTreeSources_.push_back((*pSource));

}

//木の初期化
void GroupOfTree::InitStatus(Tree ** pSource)
{
	//番号のカウントアップ
		//初期値：０
		//オリジン：０
	treeNumCounter_++;

	//定数値　HP
	static constexpr int HP = 100;
	//木のステータスの選定
	//引数：幹HP
	//引数：根HP
	//引数：木の番号（木の合計数　ー　１（0オリジンにするためー１））
	(*pSource)->InitTreeStatus(HP ,HP ,  treeNumCounter_ - 1);

}

//木オブジェクトのScale値を拡大させる
void GroupOfTree::IncreaseScale(float increaseValue)
{
	//条件：描画対象のイテレータの最後の要素ではない（Transformの可変長配列に登録されている分だけ）
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end(); itr++)
	{
		//LSYSTEMの実行
			//ある程度の大きさの木になったとき
			//LSYSTEMを実行させる
			//ここにおける木の大きさは、ｘｙｚすべてが、同じ値であることが前提である
		
		//条件：登録Transformがデフォルトサイズを超えない間
		if ((*itr)->pSourceTransform->scale_.vecX  < DEFAULT_SCALE_.vecX)
		{
			//引数の値分足す
			(*itr)->pSourceTransform->scale_ += XMVectorSet(increaseValue, increaseValue, increaseValue, 0.0f);
		}

	}
}

//デフォルトのサイズを取得する
XMVECTOR GroupOfTree::GetDefultScale()
{
	return DEFAULT_SCALE_;
}

//自身の管理する木オブジェクトと当たり判定を行う
TreeInfo GroupOfTree::RayCastToAllData()
{
	//衝突判定前に
	//衝突判定結果を入れるフラグの初期化
	//pData->hit = false;

	//レイキャスト実行クラス
	RayCaster* pRayCaster = new RayCaster;

	//レイキャスト実行クラスに
		//現在のマウス位置をワールド座標にして
		//その座標を世界の終端まで伸ばした時のレイをベクトルでもらう
	//�@引数：マウスクリック位置をワールド座標に変換したベクトル
	//�A引数：マウスクリック位置のワールド座標から、カメラで見ている世界の終端までベクトルを伸ばした時の、ちょうど終端のワールド座標

	//�@
	XMVECTOR mousePosFront;
	//�A
	XMVECTOR mousePosBack;

	//取得
	pRayCaster->GetWorldMouseCursorPos(&mousePosFront, &mousePosBack);


	//衝突したかのフラグ
	bool hit = false;

	//衝突した木オブジェクトをみつけ
		//その木の情報を与える
	TreeInfo info;

	//オブジェクト群のリスト
		//最初の要素へのイテレータ
	auto itr = pSourceTransforms_.begin();

	//オブジェクトの番号
		//リストの何番目のデータかをもらう
		//0オリジン
	int number = 0;


	//全Treeオブジェクトを回し
		//引数レイキャストと当たり判定を行う
	//条件：描画対象のイテレータの最後の要素ではない（Transformの可変長配列に登録されている分だけ）
	for (itr; itr != pSourceTransforms_.end(); itr++)
	{
		//オブジェクトのポリゴン数が多くなってしまい、
			//かつ、当たり判定もおおよその範囲でよいので、
			//オブジェクトのキャラクターモデルではなく、
			//オブジェクトのコライダーのモデル（低ポリゴン）との当たり判定を行うようにする

		//Treeに関しては、どのように、Treeのコライダーを描画しているのか、
			//その描画位置を取得して、その位置へコライダーを移動させて、そのオブジェクト（コライダー）と当たり判定を行う



		//インスタンスをEnemy型に変換して取得
		Tree* pTree = (Tree*)(*itr)->pTreeSource;


		//Treeに、削除フラグ（消去モーション中）になっているとき
			//判定相手に加えない
		//条件：消去中でないとき
		if (!pTree->IsErase())
		{

			//コライダーのハンドル取得
			int handle = pTree->GetMyColliderModelHandle();

			//上記で取得したモデルのTransformをセットさせる
			pTree->SetMyColliderTransformWhenDrawing();


			//当たり判定実行
				//レイキャストは、引数のhandleにて、セットされているTransformを使用して変形をしてからレイキャストを行う
				//そのため、上記でSetMyColliderTransformWhenDrawing（）により、実際の描画時のTransformにする（SetTransformをするので、行列の計算済み）
			hit = pRayCaster->RayCastToVectorDirection(handle, mousePosFront, mousePosBack);

		}


		//条件：衝突したか
		if (hit)
		{
			//ヒットした場合
				//直ぐに処理を終了する
				//一番最初に当たったもの対象なので、
				//本来は、一番距離の短い相手を対象とするのが良いのだろうが、計算量がその分多くなるので要検討
			break;

		}

		//ナンバー
			//リストの番号を更新
		number++;

	}


	//条件：当たっていたら
	if (hit)
	{
		//当たった対象の情報を与える

		//当たった相手の情報格納
			//インスタンスをEnemy型に変換して取得
		Tree* pTree = (Tree*)(*itr);

		//Enemy番号をセット
		info.number = number;

		//各情報取得
		pTree->SetTreeInfomation(&info);

	}


	//衝突相手の情報を返す
	return info;

}

//自身の管理する木オブジェクトと当たり判定を行う
bool GroupOfTree::RayCastToAllData(XMVECTOR targetWorldPos)
{

	//レイキャスト実行クラス
	RayCaster* pRayCaster = new RayCaster;

	//レイキャストの方法は
	//�@レイキャストの開始位置、方向を決めて当たり判定を行う
	//�Aレイキャストの開始位置、終了位置を決めて、その両方を渡し、当たり判定の方向を関数先で決めてもらう
	//上記の２つの方法がある
		//今回は�Aを使う



	//レイキャスト開始位置
		//引数ワールド座標の１０m上から
	XMVECTOR startPos = targetWorldPos + XMVectorSet(0.f, 10.f, 0.f, 0.f);

	//レイキャスト終了位置
		//引数をそのまま使用

	//オブジェクト群のリスト
		//最初の要素へのイテレータ
	auto itr = pSourceTransforms_.begin();

	//全Treeオブジェクトを回し
		//引数レイキャストと当たり判定を行う
	//条件：描画対象のイテレータの最後の要素ではない（Transformの可変長配列に登録されている分だけ）
	for (itr; itr != pSourceTransforms_.end(); itr++)
	{
		//木オブジェクトのモデルと当たり判定を行うのではなく
		//木オブジェクトの所有するコライダーとの当たり判定を行う


		//インスタンスをTree型に変換して取得
		Tree* pTree = (Tree*)(*itr)->pTreeSource;



		//衝突したかのフラグ
		bool hit = false;

		//Treeに、削除フラグ（消去モーション中）になっているとき
			//判定相手に加えない
		//条件：消去中でないとき
		if (!pTree->IsErase())
		{
			//コライダーのハンドル取得
			int handle = pTree->GetMyColliderModelHandle();

			//上記で取得したモデルのTransformをセットさせる
			pTree->SetMyColliderTransformWhenDrawing();


			//当たり判定実行
				//レイキャストは、引数のhandleにて、セットされているTransformを使用して変形をしてからレイキャストを行う
				//そのため、上記でSetMyColliderTransformWhenDrawing（）により、実際の描画時のTransformにする（SetTransformをするので、行列の計算済み）
			hit = pRayCaster->RayCastToVectorDirection(handle, startPos, targetWorldPos);

		}


		//条件：衝突したか
		if (hit)
		{
			//ヒットした場合
				//衝突したことを知らせる
			return true;

		}

	}

	//衝突しなかった
	return false;
}

//引数敵オブジェクトが、敵スポーンに存在するか
bool GroupOfTree::IsExistsTarget(GameObject* pTarget)
{
	//シーンマネージャーの同様の関数を呼び込み
	return pSceneManager_->IsExistsTarget(pTarget);
}

//引数敵オブジェクトを強制的に削除する
void GroupOfTree::EraseEnemy(GameObject* pTarget)
{
	//シーンマネージャーの同様の関数を呼び込み
	pSceneManager_->EraseEnemy(pTarget);
}

//強制的に指定オブジェクトを捕縛させる
void GroupOfTree::ForciblyBindEnemy(GameObject* pTarget)
{
	//シーンマネージャーの同様の関数を呼び込み
	pSceneManager_->ForciblyBindEnemy(pTarget);
}

//捕縛解除
void GroupOfTree::ForciblyUnBindEnemy(GameObject* pTarget)
{
	//シーンマネージャーの同様の関数を呼び込み
	pSceneManager_->ForciblyUnBindEnemy(pTarget);
}

//木が存在するポリゴン番号をすべて知る
void GroupOfTree::GetPolygonsWithTrees(std::vector<int>* polygonsWithTrees, std::vector<int>* treeTypes)
{
	//引数リストを空にする
	polygonsWithTrees->clear();
	treeTypes->clear();


	//条件：描画対象のイテレータの最後の要素ではない（Transformの可変長配列に登録されている分だけ）
	//全オブジェクトを参照する
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end(); itr++)
	{
		//それぞれの木がたっているポリゴン登録
		polygonsWithTrees->push_back((*itr)->polyNum);
		//その木のタイプを取得する
		treeTypes->push_back((int)((*itr)->pTreeSource->GetMyTreeType()));
	}
}

//引数ポリゴン番号に立っている、植わっている木があるならば、その木の番号を受け取る
bool GroupOfTree::AttackSuccess(const int TARGET_POLY_NUM, const int DAMAGE, bool* isKill, int* treeNum)
{
	/*
		//�@引数TargetPolyNumにて示される番号が
			//木自身がたっているポリゴンと同じ番号であるならば、
			//その木を攻撃対象とする　
		//�Aその攻撃対象へ　引数 damage分ダメージを与える
		//�Bダメージで倒したならば、isKillへTrue。倒せなかったならば、isKillへFalse。
		//�C木の番号（GroupOfTreeにて管理している木自身の番号）を登録

		//�D�@〜�Cをへて、攻撃が成功したならば、True。攻撃が失敗したならば、Falseを返す。
	*/
	
	//条件：描画対象のイテレータの最後の要素ではない（Transformの可変長配列に登録されている分だけ）
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end(); itr++)
	{
		//�@
		if (TARGET_POLY_NUM == (*itr)->polyNum)
		{
			//条件：攻撃対象が攻撃可能な木であるかを判断し
			if ((*itr)->pTreeSource->IsTargetToBeErased())
			{
				//攻撃可能であるならば攻撃を行う

				//�A
				//�B
				//ダメージを与える
				(*isKill) = (*itr)->pTreeSource->AddDamageAndIsDead(DAMAGE);


				//�C
				//if ((*isKill))
				{
					//木の番号を代入
					(*treeNum) = (*itr)->pTreeSource->GetUpdatedMyNumber();
				}

			
				//�D
				return true;

			}
		}
	
	}

	//�D
	return false;
}


//立地予定木の作成
void GroupOfTree::CreatePlannedLocationOfTree(XMVECTOR& setPos)
{
	//既に存在している場合（可能性はないが　一応解放）
	//解放
	ErasePlannedLocationOfTree();

	//インスタンス生成
	pPlannedLocation_ = (PlannedLocationOfTree*)Instantiate<PlannedLocationOfTree>(this);

	//座標をセットする
	pPlannedLocation_->SetDrawPosition(setPos);

}

//立地予定木の削除
void GroupOfTree::ErasePlannedLocationOfTree()
{
	//条件：存在する場合
	if (pPlannedLocation_ != nullptr)
	{
		pPlannedLocation_->KillMe();
		pPlannedLocation_ = nullptr;
	}
}

//時間の停止
void GroupOfTree::StopTimeAllTree()
{
	//自身のクラスの時間停止のフラグを立てる
	StopTime();

	//条件：管理木オブジェクトのイテレータの最後の要素ではない（重複しないTreeオブジェクト）
	//全オブジェクトを参照し
		//時間停止の宣言を行う
	for (auto itr = pListTreeSources_.begin(); itr != pListTreeSources_.end(); itr++)
	{
		//時間停止を宣言
		(*itr)->StopTime();
	}
}

//時間の計測の再開
void GroupOfTree::StartTimeAllTree()
{
	//自身のクラスの時間計測再開のフラグを立てる
	StartTime();

	//条件：管理木オブジェクトのイテレータの最後の要素ではない（重複しないTreeオブジェクト）
	//全オブジェクトを参照し
		//時間計測再開の宣言を行う
	for (auto itr = pListTreeSources_.begin(); itr != pListTreeSources_.end(); itr++)
	{
		//時間計測再開を宣言
		(*itr)->StartTime();
	}
}

//強制的に自身（木オブジェクト）消去する
void GroupOfTree::ForciblyEraseTheTree(int handle)
{
	//リストの初期イテレータ
	auto itr = pSourceTransforms_.begin();

	//指定番号の要素を削除
	//カウンター
	int i = 0;
	//条件：カウンター　と　引数ハンドルが同様でない場合
	while (i != handle)
	{
		itr++;
		i++;
	}

	//条件：リストのイテレータの最後の要素ではない
	if (itr != pSourceTransforms_.end())
	{
		//削除呼び込み
		(*itr)->pTreeSource->ForciblyEraseMe();
	}
}

//木を生成できるか
bool GroupOfTree::IsCreateTreeLimitExceeded()
{
	//作成できるか
		//上限個数を超えていないか
	//条件：現在作成している木オブジェクトの個数が、生成上限個数を超えているか
	return MAXIMUM_NUMBER_ > currentNumberOfTrees_;
}

//残り生成可能個数
int GroupOfTree::RemainingNumberOfCreateTree()
{
	//上限個数　‐　現在の生成個数
	return MAXIMUM_NUMBER_ - currentNumberOfTrees_;
}

//LSYSTEM実行可能な木へのLSYSTEM実行
void GroupOfTree::ExecutionLSystem()
{
	
	//条件：描画対象のイテレータの最後の要素ではない（Transformの可変長配列に登録されている分だけ）
		//�@LSYSTEMが実行可能かどうかを調べる
		//�A実行可能の時に、一定上の大きさを超えたら、
		//�BLSYSTEM実行を呼び込み
		//※InitTreeObjectにて、初期サイズを決定(Treeオブジェクト自体のサイズ)
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end(); itr++)
	{
		//条件：実行可能か
		if ((*itr)->pTreeSource->ExecutableLSystem())
		{
			//実際にLSYSTEM実行するかのフラグ
			bool isLSystem = false;


			switch ((*itr)->pTreeSource->GetLSystemCounter())
			{
			case 0 :
			{
				if ((*itr)->pSourceTransform->scale_.vecX > 0.3f)
				{
					isLSystem = true;

				}
			}break;

			case 1:
			{
				if ((*itr)->pSourceTransform->scale_.vecX > 0.6f)
				{
					isLSystem = true;

				}
			}break;
			
			default:
				break;
			}

			//条件：LSYSTEM実行可能ならば
			if (isLSystem)
			{
				//実行
				(*itr)->pTreeSource->ExecutionLsystem();
			}
		}
	}
	
}

//コンストラクタ
GroupOfTree::DrawTreeInfo::DrawTreeInfo():
	pSourceTransform(nullptr),
	pTreeSource(nullptr),
	polyNum(-1)
{
}

//コンストラクタ
TreeInfo::TreeInfo():
	number(-1),
	rootHP(-1),
	trunkHP(-1)
{
}
