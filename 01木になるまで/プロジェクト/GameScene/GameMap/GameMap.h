#pragma once
//システム内　標準ヘッダ(usr/)
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
#include <windows.h>	//Windows機能
#include <d3d11.h>	//Direct3D　３D描画を担う　バッファー型やDirect3D用の型
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス


//クラスのプロトタイプ宣言
class GameSceneManager;
class UIGroup;


/*
	クラス詳細：ゲーム内マップを表示するクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：現在のマップ状態を読み込み
				　テクスチャに書き込み、描画を行うクラス

				：現在のマップが、塗られていれば、ポリゴンを緑
				　塗られていなければ、ポリゴンをオレンジ
				　色にピクセルを塗り、地面の１ポリゴン１ピクセルとして　テクスチャを生成する。
				　CPUにてテクスチャを作成する方法は、Engine/HeightMapCreater.cppにて行っている

				：木の設置位置、敵の位置もマップに反映させる


*/
class GameMap : public GameObject
{
//private メンバ定数
private : 
	//マップ画像の設置位置
	static constexpr int MAP_POS_X_ = 525;
	static constexpr int MAP_POS_Y_ = 400;

	//マップ画像のサイズ
	static constexpr int MAP_SIZE_X_ = 900;
	static constexpr int MAP_SIZE_Y_ = 500;


//private メンバ変数、ポインタ、配列
private : 

	//マップ画像のモデルハンドル
	int hImage_;
	//プレイヤーロゴのハンドル
		//詳細：プレイヤーロゴは動的に、移動、回転を行いたい。そのため、画像ハンドルを取得しておく
	int hPlayerImage_;

	//地面のマップポリゴン数
	int WIDTH_;
	int DEPTH_;

	//地面の合計ポリゴン
	int MAX_POLY_NUM_;



	//シーンマネージャー
	GameSceneManager* pSceneManager_;


	//敵アイコンの配置位置
	std::vector<XMVECTOR> enemyPoses_;
	//木アイコンの配置位置
	std::vector<XMVECTOR> treePoses_;

	//ゲームマップ画像登録用グループ
		//詳細：マップにて描画する、マップ画像、他アイコンを管理するクラス
		//　　：ここにおける画像軍は、このMapクラスが存在し、描画を続けている間だけ存在しておけばよいクラスであるため、
		//　　：必要処理が終了後、
		//　　：UIGroupを開放する
	UIGroup* pUIGroup_;

	//プレイヤー、敵、木のアイコングループ
	UIGroup* pLogoUIGroup_;


//private メソッド
private : 

	//敵アイコンの配置位置作成
		//詳細：現在存在する敵ごとの描画位置をセットする
		//引数：なし
		//戻値：なし
	void SetEnemiesIconPos();
	//プレイヤーのアイコン配置位置作成
		//引数：なし
		//戻値：なし
	void SetPlayerIconPos();
	//木の配置位置作成
		//詳細：現在存在する木ごとの描画位置をセットする
		//引数：なし
		//戻値：なし
	void SetTreesIconPos();

	//マップ用のテクスチャの作成
		//詳細：現在の地面ポリゴンの塗状態をマップ画像に反映させる
		//引数：テクスチャのバッファー（ポインタの参照渡しで渡されるため、ポインタのポインタで受け取る）
		//戻値：なし
	HRESULT CreateMapTexture(ID3D11Texture2D** pBuffer);

	//セット位置を計算する
		//詳細：ゲームマップとして地面ポリゴンを四角形のポリゴンで２D描画をする。
		//　　：その際の、地面におけるポリゴン番号が2Dスクリーン上で、どの位置に配置されるかの計算。
		//　　：ポリゴン番号をスクリーン座標のピクセルに変換したときの位置を返す 
		//引数：ポリゴン番号
		//引数：マップ画像の位置を考慮したX座標
		//引数：マップ画像の位置を考慮したY座標
		//戻値：なし
	void GetOriginMapXY(int polyNum , int& x , int& y);



//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	GameMap(GameObject* parent);

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//ゲームマップの作成
		//引数：なし
		//戻値：生成できたか
	HRESULT CreteMap();

	//ゲームマップの終了
		//引数：なし
		//戻値：なし
	void EndMap();

};