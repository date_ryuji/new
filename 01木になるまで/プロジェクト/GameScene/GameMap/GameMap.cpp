#include <vector>									//可変長配列
#include "GameMap.h"								//ヘッダ
#include "../../Engine/DirectX/Direct3D.h"			//Direct3D　バッファ型使用するため
#include "../../Engine/GameObject/UIGroup.h"		//UIGroup群
#include "../../Engine/Scene/SceneUIManager.h"		//シーンUIマネージャー
#include "../../Engine/Algorithm/ScreenShot.h"		//画像生成クラス
#include "../Scene/GameSceneManager.h"				//シーンマネージャー
#include "../Tree/Tree.h"							//木クラス　木のタイプ合計数を取得するため

//コンストラクタ
GameMap::GameMap(GameObject* parent)
	: GameObject(parent, "GameMap") , 
	hPlayerImage_(-1),
	hImage_(-1),

	pSceneManager_(nullptr),
	pUIGroup_(nullptr),
	pLogoUIGroup_(nullptr),

	WIDTH_(-1), 
	DEPTH_(-1) , 
	MAX_POLY_NUM_(-1)
{
}

//初期化
void GameMap::Initialize()
{
	{
		//シーンマネージャーの取得
		//シーンチェンジャーを経由して
			//引数指定のシーンのマネージャークラスを取得
		pSceneManager_ = (GameSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);

		//自身をセットする
		pSceneManager_->AddCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GAME_MAP, this);
	}
}

//更新
void GameMap::Update()
{
	//条件：UIGroupが存在する場合
	//プレイヤーのUIの動的移動処理を行う 
	//プレイヤーの回転量を取得する
		//プレイヤーの回転量を動的に取得して、
		//プレイヤーアイコンを回転量に合わせて回転させる。
			//プレイヤーの向いている方向に、画像である（矢印）が向くように回転させる
	if(pLogoUIGroup_ != nullptr)
	{
		//プレイヤーの回転量を取得
		float playerRotateY = pSceneManager_->GetPlayerRotateY();

		//画像をプレイヤーの回転量分回転させる
		//UIManagerを取得
		SceneUIManager* pSceneUIManager = pLogoUIGroup_->GetSceneUIManager();

		//回転
		pSceneUIManager->SetRotate(hPlayerImage_, XMVectorSet(0.f, 0.f, playerRotateY, 0.f));
		//画像はY軸と平行に貼り付けられているため、
			//その画像をY軸回転の回転を適用させる（要するに、Y軸回転しているオブジェクトを上から見ているように画像では回転させたい）
			//であるならば、Z軸方向が、元のY軸回転方向となるため、取得した回転量は、Z方向に掛ける（Z方向の回転とする）

	}
}

//描画
void GameMap::Draw()
{
}

//解放
void GameMap::Release()
{
}

//Mapの作成
HRESULT GameMap::CreteMap()
{
	//マップの地面情報を示す画像作成
	{
		//テクスチャのバッファー
		ID3D11Texture2D* pBuffer;

		//テクスチャのバッファー作成
			//ポインタのアドレスを渡して、
			//関数先にて、動的確保も行ってもらう（動的確保したポインタを引数のポインタへ）
		HRESULT hr = CreateMapTexture(&pBuffer);
		//エラーメッセージ
		ERROR_CHECK(hr, "テクスチャのバッファー作成失敗", "エラー");

		//テクスチャ書き込みを完了したので、
		//そのテクスチャを画像として描画する

		//UIGroupの作成
		//UIGroupの解放
		EndMap();

		//作成
		pUIGroup_ = (UIGroup*)Instantiate<UIGroup>(this);

		//上記で作成したテクスチャを描画対象の画像として追加する
		int handle = pUIGroup_->AddStackUI(pBuffer);


		//描画位置セット
		{
			//UIManagerを取得
			SceneUIManager* pSceneUIManager = pUIGroup_->GetSceneUIManager();

			////サイズの可変
			pSceneUIManager->SetPixelScale(handle, MAP_SIZE_X_, MAP_SIZE_Y_);
			////位置の移動
			pSceneUIManager->SetPixelPosition(handle, MAP_POS_X_, MAP_POS_Y_);

			//描画の奥行きを奥へ移動させる
			pSceneUIManager->LowerOneStep(handle);
		}
	}

	//マップ内配置UIの作成
	{
		//プレイヤー、敵、木のUIGroupを作成
		pLogoUIGroup_ = (UIGroup*)Instantiate<UIGroup>(this);

		//敵セット
		SetEnemiesIconPos();
		//プレイヤーセット
		SetPlayerIconPos();
		//木セット
		SetTreesIconPos();
	}

	//描画開始
	pUIGroup_->VisibleSceneUIAndButton();
	pLogoUIGroup_->VisibleSceneUIAndButton();

	//処理の成功
	return S_OK;
}

//UIGroupの初期化
void GameMap::EndMap()
{
	//条件：UIGroupが存在する場合
	if (pUIGroup_ != nullptr)
	{
		//UIGroupの削除
			//UIGroupの先にて、確保したテクスチャなどを所持しているので、
			//その解放も行ってくれる
		pUIGroup_->KillMe();
		pUIGroup_ = nullptr;
	}
	//条件：UIGroupが存在する場合
	if (pLogoUIGroup_ != nullptr)
	{
		pLogoUIGroup_->KillMe();
		pLogoUIGroup_ = nullptr;
	}

	//ハンドル番号の初期化
	hPlayerImage_ = -1;

}

//敵アイコン位置を確定する
void GameMap::SetEnemiesIconPos()
{

	//全木の立つポリゴン番号を取得し
	//その木のタイプも取得することで、
		//木を取得ポリゴン番号から示されるピクセル位置に描画させる

	//ポリゴン番号を取得するベクター
	std::vector<int> polygonsWithEnemies;


	//ポリゴン番号を取得
	pSceneManager_->GetPolygonsWithEnemies(&polygonsWithEnemies);

	//取得ポリゴンをもとに
	//全敵の設置位置を決めて描画対象とする
	for (int i = 0; i < polygonsWithEnemies.size(); i++)
	{
		//敵アイコンをロードする
			//複数のアイコンを管理するため
			//管理する数分、ロードを行う必要がある
				//そのため、繰り返し構文の中へ、ロードを入れる
		int hEnemy = pLogoUIGroup_->AddStackUI("Assets/Scene/Image/GameScene/GameMap/Logo_Ghost.png");


		int setPosX = 0;
		int setPosY = 0;

		//描画位置を決める
			//引数に、ポリゴン番号を入れ、敵の立っている場所をもとに描画ピクセル位置を取得
		GetOriginMapXY(polygonsWithEnemies[i], setPosX, setPosY);

		//座標をセット
		//描画位置セット
		{
			//UIManagerを取得
			SceneUIManager* pSceneUIManager = pLogoUIGroup_->GetSceneUIManager();

			////サイズの可変
			pSceneUIManager->SetPixelScale(hEnemy, 25, 25);

			////描画位置の微調整
			//	//画像ファイルの関係で、画像の中心が、正しい位置にいても、
			//	//画像のグラフィカルな部分で、位置調整を入れたい。
			//		//グラフィカルな部分では、もう少し上にいてもらいたいなど。　それらの微調整用の値
			//static constexpr int FINE_ADJUSTMENT_VALUE_X = -15;
			//static constexpr int FINE_ADJUSTMENT_VALUE_Y = -20;


			//位置の移動
			pSceneUIManager->SetPixelPosition(hEnemy, setPosX , setPosY);
		}

	}

}

//プレイヤーアイコン位置を確定する
void GameMap::SetPlayerIconPos()
{
	//プレイヤーを現在ポリゴン地にセットする

	//プレイヤーアイコンをロードする
	hPlayerImage_ = pLogoUIGroup_->AddStackUI("Assets/Scene/Image/GameScene/GameMap/Logo_Player.png");

	//現在のプレイヤーの立地するポリゴン番号を取得
	//仮　：　１
	//int polyNum = 1;

	//プレイヤーの地面とのレイキャストの情報を入れる変数
	bool isHit;
	float dist;
	int collPolyNum;
	//プレイヤーのたっているポリゴンを取得する
	pSceneManager_->RayCastForPlayerAndGround(isHit, dist, collPolyNum);



	int setPosX = 0;
	int setPosY = 0;

	//描画位置を決める
	GetOriginMapXY(collPolyNum, setPosX, setPosY);





	//座標をセット
		//描画位置セット
	{
		//UIManagerを取得
		SceneUIManager* pSceneUIManager = pLogoUIGroup_->GetSceneUIManager();

		////サイズの可変
		pSceneUIManager->SetPixelScale(hPlayerImage_, 25, 25);
		////位置の移動
		pSceneUIManager->SetPixelPosition(hPlayerImage_, setPosX, setPosY);
	}
	//描画位置
	//誤差
	// ポリゴン１に描画する際の描画ピクセル位置
	//intによる計算
			//X : 140（大きな誤差（グラフィカルに見て））
			//Y : 211（大きな誤差（グラフィカルに見て））
	//ポリゴン番号以外(float計算)
			//X : 118（多少の誤差（グラフィカルに見て））
			//Y : 149（誤差なし（グラフィカルに見て））
	//ポリゴン番号も(float計算)
			//X : 500
			//Y : 
	//大きくずれてしまう
		//X方向の移動が０になってしまう
		//float型にしたことによって、
		//割り切れてしまうので、X方向への移動が０になる。
		//int型であれば、計算の中で、割り切れることがないため、０になることはなかった。それが良い結果を生んでいたが、正確ではないとして、float型への変更を行ったが、逆に失敗した。

		//問題となるのは、
		//ORIGIN_MOVE_POS_X　という、（０，０）からの

	//★
	//ORIGINAL_POLY_NUM , ORIGINAL_MOVE_POS_X , ORIGINAL_MOVE_POS_Y　を int 型にする
	//他移動量などをfloat型にて計算
		//X : 106
		//Y : 150
		//ほか、ORIGINAL_POLY_NUM、、MOVE_POS_Xなどと、同様の計算を行っている変数もintとする
			//上記を踏まえると上から2つ目の要素と同じ計算結果になる。そのため、多少のずれは回避できない。

}

//木オブジェクトの位置を確定
void GameMap::SetTreesIconPos()
{
	//ロード画像
	const std::string FILE_NAME[(int)TREE_TYPES::TREE_TYPE_MAX] =
	{
		"Assets/Scene/Image/GameScene/GameMap/Logo_DefaultTree.png",
		"Assets/Scene/Image/GameScene/GameMap/Logo_DummyTree.png",
		"Assets/Scene/Image/GameScene/GameMap/Logo_BindTree.png",
		"Assets/Scene/Image/GameScene/GameMap/Logo_EaterTree.png",

	};



	//全木の立つポリゴン番号を取得し
	//その木のタイプも取得することで、
		//木を取得ポリゴン番号から示されるピクセル位置に描画させる

	//ポリゴン番号を取得するベクター
	std::vector<int> polygonsWithTrees;
	//木のタイプを取得するベクター
	std::vector<int> treeTypes;

	//ポリゴン番号を取得
	pSceneManager_->GetPolygonsWithTrees(&polygonsWithTrees, &treeTypes);


	//取得ポリゴンをもとに
	//全木の設置位置を決めて描画対象とする
	//条件：描画対象の木数分
	for (int i = 0; i < polygonsWithTrees.size(); i++)
	{
		//木のタイプから対応アイコン画像をロード
		int handle = pLogoUIGroup_->AddStackUI(FILE_NAME[treeTypes[i]]);


		int setPosX = 0;
		int setPosY = 0;

		//描画位置を決める
			//引数に、ポリゴン番号を入れ、木の立っている場所をもとに描画ピクセル位置を取得
		GetOriginMapXY(polygonsWithTrees[i], setPosX, setPosY);


		//座標をセット
		//描画位置セット
		{
			//UIManagerを取得
			SceneUIManager* pSceneUIManager = pLogoUIGroup_->GetSceneUIManager();

			////サイズの可変
			pSceneUIManager->SetPixelScale(handle, 25, 50);

			//描画位置の微調整
				//画像ファイルの関係で、画像の中心が、正しい位置にいても、
				//画像のグラフィカルな部分で、位置調整を入れたい。
					//グラフィカルな部分では、もう少し上にいてもらいたいなど。　それらの微調整用の値
			static constexpr int FINE_ADJUSTMENT_VALUE_X = -15;
			static constexpr int FINE_ADJUSTMENT_VALUE_Y = -20;

			////位置の移動
			pSceneUIManager->SetPixelPosition(handle, setPosX + FINE_ADJUSTMENT_VALUE_X, setPosY + FINE_ADJUSTMENT_VALUE_Y);
		}

	}

}

//マップのテクスチャを作成する
HRESULT GameMap::CreateMapTexture(ID3D11Texture2D** pBuffer)
{
	//テクスチャバッファへのCPUからの書き込み
	{
		/*
		//�@地面の総ポリゴン数を取得（Width,Depth）
		//�Aポリゴン数に応じた。総ポリゴンに等しいピクセルを持ったテクスチャを取得（�@のWidth,Depthからなるピクセル）
		//�B�@の１ポリゴンずつアクセスして、そのポリゴンが緑ポリゴンなら、緑。オレンジ（黒）ポリゴンなら、オレンジ。の色を登録するRGBとして宣言する。
		//�C�AのRGB値をピクセルに登録
		//�D上記を全ピクセル数分繰り返す
		*/


		//乗数のために加算する値
			//Width,Depthにて取得できる値は、必ず２の乗数になっているので、
			//２の乗数を１減らして、Widthとする。であれば、　その引く分を後から加算してやればよい。
		const int ADD_VALUE = 1;


		//�@
		//合計ポリゴン数を取得
			//ポリゴン数は
		MAX_POLY_NUM_ = pSceneManager_->GetTotalPolygonCountOfGrassLand();
		int width = 0;
		int depth = 0;

		pSceneManager_->GetVertexesWidthCountAndDepthCount(&width, &depth);

		//HeightMapGroundにて、ポリゴンを作る際の、Width,Depthと同様の値にする
			//下記の詳細は、HeightMapGroundにて
			//関数にてもらう、Width,Depthは、あくまでも、Witdh、Depthのポリゴンを作る頂点の数である。
			//地面は、その頂点をもとに、三角ポリゴンを作り、地面を作っている。
				//そのため、ポリゴン数を作るポリゴンのWidth,ポリゴンのDepthも計算が必要。

			//絵にかくとわかりやすい
			//Width,Depthの頂点があるとして、それをもとにポリゴンを作るとしたら、Widthには何ポリゴン並ぶのか、Depthには、何ポリゴン並ぶのか
			//Width　＝　Width - 1 (頂点５個の場合、４つ並ぶ)
			//Depth　＝　(Depth - 1) * 2 (頂点５個の場合、８個並ぶ)
		WIDTH_ = (width - 1);
		width = WIDTH_ + ADD_VALUE;
		//width = (width - 1);
		DEPTH_ = (depth - 1) * 2;
		depth = DEPTH_;

		//�A
		//テクスチャ生成
			//テクスチャのバッファー
		//ID3D11Texture2D* pBuffer;

		//テクスチャの設定を入れる構造体
		D3D11_TEXTURE2D_DESC texdec;
		texdec.Width = width;	//画像の横幅
		texdec.Height = depth;	//画像の縦幅
		texdec.MipLevels = 1;
		texdec.ArraySize = 1;
		//ここで、必ず、RGBAの4成分を確保することを設定する
			//4成分にしなければ、書き込むときに、ずれが生じてしまう
			//1バイトずつ、　R,G,B,Aと、4回回して、それぞれの成分を書き込ませるので、4成分にしないとsize、書き込むときのポインタがずれるので注意
		texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		texdec.SampleDesc.Count = 1;
		texdec.SampleDesc.Quality = 0;
		texdec.Usage = D3D11_USAGE_DYNAMIC;
		texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		texdec.MiscFlags = 0;
		//設定をもとにテクスチャの作成
		HRESULT hr = Direct3D::pDevice->CreateTexture2D(&texdec, nullptr, pBuffer);
		//エラーメッセージ
		ERROR_CHECK(hr, "テクスチャの生成失敗", "エラー");



		//CPUからテクスチャへアクセスすることを宣言
		//アクセスに用いる構造体
		D3D11_MAPPED_SUBRESOURCE hMappedres;

		//第3引数：書き込みを行う
		hr = Direct3D::pContext->Map((*pBuffer), 0, D3D11_MAP_WRITE_DISCARD, 0, &hMappedres);
		//エラーメッセージ
		ERROR_CHECK(hr, "コンテキストのマップ失敗", "エラー");


		//テクスチャのピクセルデータにアクセスするためのポインタ
		//０〜２５５の値が取れるように　1バイトずつ
		//unsigned char 型でデータを取得する。（unsigned = 符号なし、の char 1バイトずつ）
		unsigned char* dest = static_cast<unsigned char*>(hMappedres.pData);


		//�B
		//１オリジンにて、
		//ポリゴンごとのポリゴンが塗られているかの確認

		//２の乗数に合わせるためにある、ピクセルは、透明にする
		//そのためのポリゴン数カウンター
		int polyCounter = 1;

		//�C
		//�D
		for (int i = 1; i <= width * depth; i++)
		{


			//rgbの3項目にアクセスするための終了条件
			const int SIZE = 3;




			//端ならば
			if (i % width == 0)
			{
				//αまで
				for (int ccc = 0; ccc < (SIZE + 1); ccc++)
				{
					//透明なピクセルを入れる
					unsigned char color = 0;

					//ポインタを回す
					(*dest) = color;
					dest++;
				}
			}
			else
			{




				
				//RGB値を入れる変数
				// x  : r
				// y  : g
				// z  : b
				int rgb[SIZE] = { 0,0,0 };



				//１ポリゴンごとにアクセスして、
					//そのポリゴンが緑ポリゴンか調べる
				if (pSceneManager_->IsGreenPolygon(polyCounter))
					//if (pSceneManager_->IsGreenPolygon(i))
				{
					//RGB値（０〜２５５）の緑の値を代入
					//r : 0
					//g : 255
					//r : 0

					rgb[1] = 255;
				}
				else
				{
					//RGB値（０〜２５５）のオレンジの値を代入
					//r : 255
					//g : 150
					//r : 0
					rgb[0] = 255;
					rgb[1] = 150;

				}



				//上記のｒｇｂ値を１ピクセルに登録
					//aを除く
				for (int k = 0; k < SIZE; k++)
				{

					//RGB　の値を、unsigned char型で登録
					unsigned char commonColor = rgb[k];


					//ポインタにて示されるRGBの色情報を書き換える
					(*dest) = commonColor;
					//ポインタを回す
					dest++;
				}

				//Aの登録
					//α値　２５５　＝　１．０
				unsigned char alpha = 255;

				//ポインタを回す
				(*dest) = alpha;
				dest++;


				polyCounter++;

			}
		}



		//CPUからのアクセス終了
		Direct3D::pContext->Unmap((*pBuffer), 0);

	}
	/*
		テクスチャの横幅ピクセル


		//テクスチャの横幅は、
		//２の乗数でないと、おかしなことになる。
		//そのため、からでもいいので、２の乗数にて設定する必要がある。
		//画像にしてみればわかるが、
		//ピクセルサイズが、任意のサイズにならなくなる。

		//今回に関しては、１行空きができてしまう（透明なところができてしまう）

		//空きを左の列に来るように設定。

	
	*/

	//処理の結果
	return S_OK;

}


//ポリゴン番号をスクリーン座標のピクセルに変換したときの位置を返す
void GameMap::GetOriginMapXY(int polyNum , int& x, int& y)
{
	/*
		//※下記の計算にて、
		//　int型にして計算してしまうと、大きな誤差が出てしまうため、
		//　基本的にポリゴン番号以外は、float型に手計算する

		
		//※ポリゴン番号をもとに、
		//　設置位置を決める

		//※ポリゴン番号の合計ポリゴン番号の半分のポリゴン番号が、
		//　マップ画像における中心である
		//※上記を込みして、ポリゴン番号から示される座標を求める
		//　その座標へ、プレイヤーロゴなどをセットさせる


		//※半分のポリゴン番号が、
		//　マップ画像設置位置の座標となる
		//　上記をもとに、設置ポリゴン番号の座標を求める

		//※座標を求める際に、
		//　1ポリゴンの　描画時の横のピクセル差　を求める。
		//　1ポリゴンの　描画時の縦のピクセル差　を求める。
		//　上記の値で、設置するポリゴン番号をもとに、設置座標を求める


		//※描画時のサイズと
		//　縦横のポリゴン数を所持しているため、
		//　1ポリゴンの占有する長さと座標を求めることができる。

		//★//ORIGINAL_POLY_NUM , ORIGINAL_MOVE_POS_X , ORIGINAL_MOVE_POS_Y　を int 型にする
		//それ以外の値をfloat型にて管理
		//ほか、ORIGINAL_POLY_NUM、、MOVE_POS_Xなどと、同様の計算を行っている変数もintとする
	*/


	//1ポリゴンの横の長さ(1ポリゴンの移動量)
	float widthLength = (float)MAP_SIZE_X_ / (float)WIDTH_;

	//1ポリゴンの縦の長さ(1ポリゴンの移動量)
	float depthLength = (float)MAP_SIZE_Y_ / (float)DEPTH_;


	//原点となる全ポリゴンの半分のポリゴン番号をもとに
	//設置ポリゴンの差を求める

	//原点となるポリゴン番号
		//縦のポリゴン数半分、横のポリゴン数半分　移動した際のポリゴン番号
	const int ORIGINAL_POLY_NUM = (int)(((float)WIDTH_ * ((float)DEPTH_ / 2.f)) + ((float)WIDTH_ / 2.f));


	//符号
	float code;
	//符号量
	static constexpr float PLUS = 1.f;
	static constexpr float MINUS = -1.f;

	//原点となるポリゴン番号より多いか、少ないかの判定
	if (ORIGINAL_POLY_NUM > polyNum)
	{
		//符号をマイナスとする
		code = MINUS;
	}
	else
	{
		//符号をプラスとする
		code = PLUS;
	}


	//その差によって、
	//1ポリゴンの横、縦の差分、移動させる

	//差を求める
	//差ポリゴン
		//絶対値にて、正の値で差を求める
	float subPoly = (float)((abs(ORIGINAL_POLY_NUM - polyNum)));

	//差をもとに移動
		//差のポリゴンが
		//横のポリゴン数分移動していたら、ひとつ一行上に移動（縦移動）
		//横移動と、縦移動を計算して、設置座標を求める

	//しかし、
	//原点となるポリゴン座標は、
		//中心の位置のため、中途半端な横ポリゴン座標に位置している。
		//そのため、原点の中途半端な横ポリゴンの位置を移動してしまって、
			//横座標の位置移動を0にした状態から、残りの差分移動を行わせる。


	//原点座標ポリゴンから、
	//（０，０）のポリゴンから、横に何ポリゴン、縦に何ポリゴン移動しているかを求める
	//Y : WIDTH　何個分か
	//X : ORIGIN座標から、WIDTHを割ったときの余り
	const int ORIGIN_MOVE_POS_Y = (int)(ORIGINAL_POLY_NUM / (float)WIDTH_);
	const int ORIGIN_MOVE_POS_X = (int)(ORIGINAL_POLY_NUM - (ORIGIN_MOVE_POS_Y * (float)WIDTH_));


	//設置座標
		//初期値：原点
	float setPosX = MAP_POS_X_;
	float setPosY = MAP_POS_Y_;


	//移動量の差が
		//原点から、端までの移動量より小さい場合
		//以下の左端に移動させる計算はせず、
		//移動量の差を移動させる
	if (subPoly >= ORIGIN_MOVE_POS_X)
	{

		//上記のX分原点の座標から移動させる
		//　設置座標　＝　設置座標　＋　（（０，０）から原点座標が移動しているX成分　＊　描画時の1ポリの移動ピクセル量　＊　符号（どちらに移動するか））
		setPosX = setPosX + (ORIGIN_MOVE_POS_X * widthLength * code);

		//移動分差ポリゴンから減らす
		subPoly -= ORIGIN_MOVE_POS_X;
	}
	else
	{
		//移動
		setPosX = setPosX + (subPoly * widthLength * code);

		//移動分差ポリゴンから減らす
		subPoly = 0.f;
	}




	//移動量が
		//0の場合、移動が存在しないので、
		//以下の計算を行わない
	if (subPoly > 0.f)
	{


		//残り差から
		//残りの差が、縦に何個分、横に何個分移動するか求める
			//この際、マイナス方向に移動するか、プラス方向に移動するかで計算方法が変わってくるので注意
				//プラスは、上記の座標から、後のX移動成分を加算すればよいだけ。
				//マイナスは、上記の座標から、後のX移動成分を移動するには、
					//X方向に移動が必要　＝　ひとつ上段に移動して、更に、画像マップの右端から、左へ移動することが必要

		//Y方向の移動量
		const int SUB_MOVE_Y = (int)(subPoly / (float)WIDTH_);
		//X方向の移動量
		const int SUB_MOVE_X = (int)(subPoly - (SUB_MOVE_Y * (float)WIDTH_));





		//プラス方向の計算
		if (code == PLUS)
		{


			//Y方向の移動
				//Y方向のピクセル移動量の移動
			setPosY = setPosY + (depthLength * SUB_MOVE_Y);

			//X方向の移動
			//X方向の移動が０より多い場合
			if (SUB_MOVE_X > 0.f)
			{
				//X成分にー１すると、一つ上の行の端に移動する。
				//	//そのため、Y成分を一減らして、Xの移動成分を一減らす
				//Y方向に一行移動する

				setPosY = setPosY + (depthLength * 1.f);




				//一番左端に移動させる
					//次の行に言っているので、
					//次の行の左端へ
				setPosX = setPosX - (widthLength * (ORIGIN_MOVE_POS_X * 2.f));

				//端から
				//X成分の移動量を移動
					//Y方向に一行移動したため、X成分を１移動したということ。
					//そのため、移動量を1減らす
				setPosX = setPosX + (widthLength * (SUB_MOVE_X - 1.f));


			}

			//X方向の移動
			//setPosX = setPosX + (widthLength * SUB_MOVE_X);


		}
		//マイナス方向の計算
		else
		{
			//Y方向の移動
			setPosY = setPosY - (depthLength * SUB_MOVE_Y);

			//X方向の移動
			//X方向の移動が０より多い場合
			if (SUB_MOVE_X > 0.f)
			{
				//X成分にー１すると、一つ上の行の端に移動する。
				//	//そのため、Y成分を一減らして、Xの移動成分を一減らす
				//Y方向に一行移動する

				setPosY = setPosY - (depthLength * 1.f);




				//一番右端に移動させる
					//原点は、縦横の中心となっているため、原点から原点のX成分の移動量が求められているので、その分加算
					//現在は左端に移動しているはずなので、
					//そこから、原点からのX方向の移動量の2倍移動すれば、右端へ移動できる
				setPosX = setPosX + (widthLength * (ORIGIN_MOVE_POS_X * 2.f));

				//端から
				//X成分の移動量を移動
					//Y方向に一行移動したため、X成分を１移動したということ。
					//そのため、移動量を1減らす
				setPosX = setPosX - (widthLength * (SUB_MOVE_X - 1.f));


			}


		}

	}

	x = (int)setPosX;
	y = (int)setPosY;	

}

