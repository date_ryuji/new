#include "Enemy.h"									//ヘッダ
#include "../../Engine/Model/Model.h"				//モデル
#include "../../Engine/Collider/SphereCollider.h"	//球コライダー
#include "../../Engine/Collider/BoxCollider.h"		//箱コライダー
#include "../../Engine/Algorithm/Math.h"			//ベクトルのなす角取得計算関数
#include "../../Shader/FbxEraseMotionShader.h"		//専用シェーダー（FBX専用消去シェーダー）
#include "../NumberPlate/NumberPlate.h"				//自身の番号プレート
#include "../CountTimer/CountTimer.h"				//カウントタイマー
#include "EnemySpawn.h"								//敵オブジェクトのスポーン
#include "NavigationForEnemy.h"						//敵オブジェクトのナビゲーター
#include "EffectEnemy.h"							//敵オブジェクトのエフェクトモデルオブジェクト
#include "EnemyWeapon.h"							//敵オブジェクトの武器モデル


//コンストラクタ
Enemy::Enemy(GameObject * parent)
	: GameObject(parent, "Enemy"),
	isStopTime_(false),
	hMyNavigation_(-1),

	elpasedTimeForBeforeAttack_(0.f),
	elpasedTimeForRecivedDamage_(0.f), 
	elpasedTimeForFloating_(0.f),
	isEraseMotion_(false),
	isBind_(false),
	isAttack_(false),

	targetPolyNum_(-1),

	eraseSpeed_(1.0f),

	pEnemyWeapon_(nullptr),
	pEffect_(nullptr),
	pMySpawn_(nullptr),
	pMyNavigator_(nullptr),
	pNumberPlate_(nullptr),

	isNavigate_(false),
	pCountTimer_(nullptr),

	pEnemyInfo_(nullptr),

	startingPos_(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	nextDestinationPos_(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	goalPos_(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	lastUpdatePosY_(0.f),

	hModel_(-1),
	interpolationRate(0.f)
{
}

//初期化
void Enemy::Initialize()
{

	{
		//自身の武器描画
		pEnemyWeapon_ = (EnemyWeapon*)Instantiate<EnemyWeapon>(this);
		//インターバルセット
		pEnemyWeapon_->SetInterval(ATTACK_INTERVAL_);
	}


	{
		//自身の中心に	
			//エフェクトモデルの描画
			//エフェクトは、木のモデルに触れているときに描画を行うようにする
		pEffect_ = (EffectEnemy*)Instantiate<EffectEnemy>(this);
		{
			//描画フラグを下す
			pEffect_->Invisible();
		}
	}

	//ロード
	hModel_ = Model::Load("Assets/Scene/Model/GameScene/Enemy/Ghost.fbx", POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_TEST);
	//警告
	assert(hModel_ != -1);

	//箱コライダーの作成
	//引数：コライダー位置（付加オブジェクトからのローカル座標）
	//引数：コライダーサイズ
	//引数：コライダー描画のフラグ
	BoxCollider* pColl = new BoxCollider(XMVectorSet(0.f, 0.f, 0.f, 0.f), XMVectorSet(4.f, 4.f, 4.f, 0.f) , VISIBLE_COLLIDER);
	
	//コライダーを
	//オブジェクト自身のコライダーリストへ追加
	AddCollider(pColl);



	{

		//Enemyの初期ステータスのセット
		pEnemyInfo_ = new EnemyInfo;

		pEnemyInfo_->HP = 100;
		pEnemyInfo_->attackDamage = 20;
	}

}

//更新
void Enemy::Update()
{

	//条件：時間が停止されていない
		//時間停止時＝行動不可
		//時間非停止時＝行動許可
	if (!(IsStopTime()))
	{
		//条件：消去モーションが実行されていない
		if (!(IsEraseMotion()))
		{
			//条件：バインドされていない
			if (!(IsBind()))
			{
				//経路探索、移動
				MyNavigation();

				//浮遊モーション実行
				Floating();

				//オブジェクトの回転
				RotateMyBody();

				//攻撃を与える
				CauseDamage();

			}

			//被ダメージの間隔を超えたら
				//自身の立っているポリゴンの状況に応じてダメージを受ける
			ReceiveDamage();

			//消去判定
			JudgeExeEraseMotion();

		}
		//条件：消去モーションが実行されているとき
		else
		{
			//計測時間が過ぎていたら、
				//自身を削除
			JudgeKillMe();

		}

	}

}


//描画
void Enemy::Draw()
{

	//条件：消去モーションを実行している場合
		//コンスタントバッファ２つ目へ、必要情報を与える
	if (IsEraseMotion())
	{
		//シェーダーへの追加情報を与える
		//経過時間の取得
		float elpasedTime = pCountTimer_->GetElpasedTime();

		//経過時間をシェーダーに渡す
			//コンスタントバッファ2つ目に代入
		FbxEraseMotionShader* pFbxEraseMotion = (FbxEraseMotionShader*)Direct3D::GetShaderClass(SHADER_TYPE::SHADER_FBX_ERASE_MOTION);

		//コンスタントバッファにセット
		FbxEraseMotionShader::CONSTANT_BUFFER_1 cb1;
		cb1.elpasedTime = elpasedTime * eraseSpeed_;

		//セット
		pFbxEraseMotion->SetConstantBuffer1Data(&cb1);
	}

	//描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);


}

//解放
void Enemy::Release()
{
	//敵構造体の解放
	SAFE_DELETE(pEnemyInfo_);
}

//衝突時の処理
void Enemy::OnCollision(GameObject* pTarget)
{

	//条件：衝突相手が木である
	if (pTarget->objectName_ == "Tree")
	{

		//敵のエフェクトの
		//描画フラグをあげる
			//毎フレームUpdateにて、
			//描画のフラグを下されている
			//そのため、衝突を行っているときは、毎回描画のフラグを立てる
		pEffect_->Visible();
	}

}



//攻撃を与える
void Enemy::CauseDamage()
{
	//条件：攻撃が行えるフラグ（ナビゲーション終了時立つ）がたっている
	//　　：&&
	//　　：攻撃間隔を過ぎている
	if (IsPossibleToAttack())
	{

		//木を倒したとき使用する
			//木の番号を入れる変数
		int treeNum = -1;

		//攻撃の実行
		CauseDamage(pEnemyInfo_->attackDamage, &treeNum);

		//前回の攻撃からの経過時間を初期化
		InitAttackTime();

	}

}

//攻撃を与える
void Enemy::CauseDamage(int damage, int* treeNum)
{

	//攻撃して木を倒したかのフラグ
	bool isKill = false;

	//攻撃相手が存在しているかの確認
	// 	   //ターゲット先のポリゴン番号を渡して、そのポリゴン番号を持つ、木へ攻撃を与える
	//同じポリゴンにほかの木が存在しない前提
	//存在している場合、木グループに登録されている、その登録順に木が対象となる
	//とともに
	//攻撃実行
	bool isExistsTree = pMySpawn_->AttackSuccess(
		targetPolyNum_, damage, &isKill, treeNum);



	//条件：攻撃対象が存在しない場合
	//戻り値にて、Falseを受け取る
	//Falseの場合、攻撃を終了とする
	//攻撃を終了とし、新しいナビゲーション対象を受け取る準備
	if (!isExistsTree)
	{
		targetPolyNum_ = -1;
		//攻撃を止める
		StopAttack();

		//看板描画
		//攻撃対象が存在しなかったことを伝える
		pMySpawn_->AddTalkWhenNullTree(hMyNavigation_);

	}


	//条件：自身を消去するフラグが立っているとき
	if (isKill)
	{
		targetPolyNum_ = -1;
		//攻撃を止める
		StopAttack();


		//看板描画
		//対象の木を倒すことができたなら、
		//倒したことを、看板テキストに描画してもらう
		//敵情報看板にテキストとして、〜〜が木を倒したということを伝える

		//必要情報の整理
		//自身の番号

		//倒した木の番号
		pMySpawn_->AddTalkWhenTreeFall(hMyNavigation_, (*treeNum));
	}
}



void Enemy::Floating()
{
	//指定時間間隔で
		//浮遊モーションを行う
		//浮遊モーションは、上下にゆらゆらと移動するモーションである

		//指定時間を決め、その指定時間の半分を使用し、上昇。残り半分を使用し、下降。

	//上昇の際注意すること
	//敵オブジェクトは、ナビゲーションにて、自身の座標を移動させている
		//そのため、Y座標も同様に移動する。
		//つまり、ナビゲーション移動後の座標を原点とし、現在の経過時間による、Y座標を求め、設置する必要ある。　フレーム経過時間による加算ではなく、毎フレーム設置を行う必要がある。
			//加算にしてしまうと、原点位置から下に下がる場合、地面についてしまう。
			//なぜならば、加算先の座標は、これまで上昇下降を含めた座標ではなく、ナビゲーションによって、地面に立つように設定された座標であるため。

	//浮遊時間
	static constexpr float FLOATING_TIME = 2.f;


	//Y軸上昇値
		//指定時間の半分で下記量上昇し、残り半分を使用し、下記量下降する
	static constexpr float FLOATING_VALUE = 0.5f;

	//上記の上昇の際注意することを踏まえて
		//計算方法
	//上昇
		//現在の経過時間が浮遊時間の半分以下であった場合、
		//経過時間＊上昇スピードにて、上昇値を取得。現在座標＋上昇値を設定
	//下降
		//現在の経過時間が浮遊時間の半分以上であった場合、
		//経過時間 - (浮遊時間 / 2.f) * 上昇スピードにて、下降値を取得。
		//上昇の際の最高値を求める。
		//上記の座標から、下降値を減算。


	//条件：経過時間が浮遊時間を超えているか
	if (FLOATING_TIME < elpasedTimeForFloating_)
	{
		//経過時間を初期化し
		//何もせず終了
		elpasedTimeForFloating_ = 0.f;
		return;
	}

	//計算に用いる要素を求める
	//上昇スピード
		//計算において、浮遊時間の半分を掛けて、上昇値を上昇したいため、
		//結果を2倍にする必要がある。（あるいは、上昇値設定の段階で値を2倍にしておく）
	const float riseValue = (FLOATING_VALUE / FLOATING_TIME) * 2.f;

	//条件：上昇下降判断
	//　　：浮遊時間の半分より多い
	if ((FLOATING_TIME / 2.f) < elpasedTimeForFloating_)
	{
		//下降
		Descent(elpasedTimeForFloating_ , riseValue, FLOATING_TIME);
	}
	else
	{
		//上昇
		Rise(elpasedTimeForFloating_, riseValue);

	}


}

//上昇
void Enemy::Rise(const float ELPASED_TIME, const float SPEED)
{
	//経過時間＊上昇スピードにて、上昇値を取得
	float riseValue = ELPASED_TIME * SPEED;
	//現在座標＋上昇値を設定
		//毎フレームナビゲーションにて、地面に沿ったY座標となるので、
		//毎回合計経過時間による加算値を加算する形でよい

		//しかし、ナビゲーションが行われていない際に、常に上昇する形になるため、
		//変数へ、最期に更新した際のY座標をセットする
	transform_.position_.vecY = lastUpdatePosY_+ riseValue;
}

//下降
void Enemy::Descent(const float ELPASED_TIME, const float SPEED, const float FLOATING_TIME)
{
	//経過時間 - (浮遊時間 / 2.f) * 上昇スピードにて、下降値を取得。
	float descentValue = (ELPASED_TIME - (FLOATING_TIME / 2.f)) * SPEED;
	//上昇の際の最高値を求める。
	//最高値における上昇値
	float max = (FLOATING_TIME / 2.f) * SPEED;
	//上昇値＋現在値
	float maxPosY = lastUpdatePosY_ + max;

	//上記の座標から、下降値を減算。
		//減算後の座標をセット
	transform_.position_.vecY = maxPosY - descentValue;

}



//自身へのダメージ
void Enemy::ReceiveDamage()
{
	//黒ポリゴン　＝　１
	//緑ポリゴン　＝　５

	//条件：経過時間から被ダメージの間隔を超えているかの判断
	if (elpasedTimeForRecivedDamage_ >= TAKEN_DAMAGE_INTERVAL_)
	{

		//自身が立っているポリゴン番号を取得
		int polyNum = GetPolygonIStandingOn();

		//条件：ポリゴン番号がー１でない場合
		if (polyNum != -1)
		{
			//ポリゴン番号が、緑か、黒か調べる
			bool isGreen = pMySpawn_->IsGreenPolygon(polyNum);


			//ダメージ値
			//通常ＨＰ：１００
			//1回の通常ダメージ：１
			//1ステージの制限時間：　120 s
			//上記を踏まえたうえで、敵は通常●ｓ居てほしいか

			//�@●　＝　２０ｓ　としたとき、
			//�@1s で5回ダメージを受ける　＝　攻撃を行う間隔　0.2(攻撃間隔ｓ)
			//�A●　＝　４０ｓ
			//�A1s で2.5回ダメージを受ける　＝　攻撃を行う間隔　0.4（攻撃間隔ｓ）

			static constexpr int DAMAGE[2] =
			{
				1 ,	//通常、

				2 ,	//緑ポリゴン

			};


			//ダメージを受ける
				//boolの真偽値をintに置き換えて、　ダメージ値を判定する
				//０：通常時のダメージ
				//１：通常時のダメージ＊２倍のダメージ
			ReciveDamage(DAMAGE[isGreen]);


			//経過時間初期化
			InitRecivedDamageTime();
		}



	}

}
//自身へのダメージを受けとる時間の初期化
void Enemy::InitRecivedDamageTime()
{
	elpasedTimeForRecivedDamage_ = 0.0f;
}
//攻撃タイムの初期化
void Enemy::InitAttackTime()
{
	elpasedTimeForBeforeAttack_ = 0.0f;
}
//自身の回転（移動方向へ）
void Enemy::RotateMyBody()
{
	//条件：移動が終了し
		//攻撃を行っているならば、
		//回転値を固定する
	if (IsAttack())
	{
		static constexpr float SET_ROTATE_Y = 90.f;
		transform_.rotate_.vecY = SET_ROTATE_Y;
		return;
	}


	/*
	//現在の移動方向をもとに、
		//移動方向へ体を回転させたい

	//デフォルトの前方方向は、
		//-Z方向が前方方向である。(0,0,-1)
	//(0,0,1)方向へ進む場合、
		//Y軸回転で、１８０度回転したい

	//�@デフォルトの方向。（０，０，-１）
	//�A進行方向。（ｘ，ｙ，ｚ）
	//�B上記のベクトルの単位ベクトルとがなす角を求める
	//�Cその角度を、transform_.roate.yにセットする

	*/


	//�@
	XMVECTOR defaultDir = XMVectorSet(0.f, 0.f, -1.f, 0.f);
	//�A
		//最終の目的位置 - 現在位置 = 現在位置から目的位置までのベクトル
		//上記の正規化
	XMVECTOR moveDir = XMVector3Normalize(nextDestinationPos_ - transform_.position_);
	//XMVECTOR moveDir = XMVector3Normalize(goalPos_ - (transform_.position_ + XMVectorSet(0.5f,0.f,0.f,0.f)));


	//３６０度の回転量取得
	float angle = Math::GetAngleWithin360Degree(moveDir, defaultDir, NORMAL_DIR::NORMAL_DIR_Y);


	////�B
	//	//２つのベクトルの内積のACosの値が、２つのベクトルのなす角となる
	//	//内積を求める（２つのベクトルによる、一番近い内積（小さい角度のほうの内積））
	////float dot = XMVector3Dot(defaultDir, moveDir).vecX;
	//float dot = XMVector3Dot(moveDir, defaultDir).vecX;


	////２つのベクトルのなす角
	//	//結果　＝　ラジアン
	//	//度に直す(計算の際に、最終的にラジアンに変化させるが、でバック時にわかりやすくするために、度にする)
	////float angle = acos(dot) * (180 / 3.14);	//←計算式
	//float angle = XMConvertToDegrees(acos(dot));

	//�C
	//角度の差分回転させる
	//transform_.rotate_.vecY = -angle;
	transform_.rotate_.vecY = angle;



}
//消去モーション開始
void Enemy::StartEraseMotion()
{
	//消去モーション実行中のフラグを立てる
	isEraseMotion_ = true;
	//自身のスポーンに
		//自身が消去モーションに入った
		//つまり、管理対象から外れたことを知らせる
	SpawnRemoveMyData();

	//ナビゲーションから、自身のナビゲーションのための経路を持っているリスト
	//そのリストを解放する
		//ナビゲーション終了をもとに、自身には今後ナビゲーションが必要ない。
			//とともに、自身が消去後は、リストのリストが消された分、横にずれる必要がある。
			//その処理のために、ずらす前に、必要のないリストを削除
	pMyNavigator_->EraseMyNavigator(hMyNavigation_);

	//ナビゲーションは今後行わないので、
		//ナビゲーションをNullPtrで、使用できなくする
	pMyNavigator_ = nullptr;
	//ハンドルも解放
	UpdateMyNavigationHandle(-1);


	//スポーンを経由して(スポーンに行わせない理由は、　スポーンには、基本的にオブジェクトの所有だけ行ってほしい、ナビゲーション関連の処理などをスポーン内で行わせたくない
	//						だが、Enemyは、司令塔のオブジェクトを知らない。そのため、Spawnを経由して、司令塔へ処理を行うように知らせる)
	//ナビゲーションへアクセスするためのハンドル番号の更新を知らせる
	//全オブジェクトの持っている、
		//上記で、スポーンの範囲、スポーンのオブジェクトから外していることが必要。外してからでないと、番号を更新してと言って、消去される自身も含めて、番号が振りなおされる
			//それでは、振り直しの意味がない
	pMySpawn_->ReassigningMyNavigationHandle();


	//現段階でのシェーダーでは、
		//引数にて渡された終了時間で、きれいに透明度が０になる計算にはなっていないので注意
		//上記の実装を行うのであれば、TestShader.hlslのPS_EraseMotion()の修正が必要

	{
		//シェーダーを切り替える
			//Fbxモデル専用の、
			//消去モーションシェーダー
		//シェーダーの切替え
			//消去モーションを必要とするシェーダーに変換
				//各々のハンドル番号は、重複することはない。クラスが違う場合、同じハンドル番号になることはない。
				//仮に、同じFbxファイルを使用していたとしても、
					//そのため、シェーダータイプを各々のハンドル番号ごとに持たせているため、同じFbxでも、シェーダーを都度、ハンドル番号ごと管理することが可能である。
		Model::ChangeShader(hModel_, SHADER_TYPE::SHADER_FBX_ERASE_MOTION);


		//タイマーの確保
		pCountTimer_ = (CountTimer*)Instantiate<CountTimer>(this);


		//終了時間の設定
		static constexpr float endTime = 3.0f;

		//タイマーのセット
		pCountTimer_->StartTimerForCountUp(0.f, endTime);
	}


}
//消去モーションを開始ししているか
bool Enemy::IsEraseMotion()
{
	return isEraseMotion_;
}
//捕縛されているか
bool Enemy::IsBind()
{
	return isBind_;
}
//攻撃可能フラグを立てる
void Enemy::StartAttack()
{
	isAttack_ = true;


	//木を倒したとき使用する
			//木の番号を入れる変数
	int treeNum = -1;

	//看板描画
	//攻撃開始を宣言

	//攻撃対象の木番号を取得
		//ナビゲーション目的地のポリゴン番号から、
		//該当の木（その上記のポリゴン番号に立てられている木を探す）を見つけて、その木の番号を返す
	//攻撃実行関数を呼び込む
		//攻撃実行関数の、与えるダメージを０にして渡すことで、
		//ダメージ０を与えたときに、ダメージを与えた木の木番号を取得して、その敵を攻撃対象とする
	CauseDamage(0, &treeNum);

	//引数にて取得した番号に対して
	//攻撃を与えることを宣言する
	//条件：攻撃対象が存在する
	if (treeNum != -1)
	{
		pMySpawn_->AddTalkWhenStartAttack(hMyNavigation_, treeNum);
	}

}
//攻撃可能フラグを下ろす
void Enemy::StopAttack()
{
	isAttack_ = false;
}
//自身の死亡判定
void Enemy::JudgeExeEraseMotion()
{
	//条件：自身メンバのHPが、0以下であったならば
	if (pEnemyInfo_->HP <= 0.f)
	{
		//消去モーションを開始
		StartEraseMotion();
	}
}
//自身が消去対象か
void Enemy::JudgeKillMe()
{
	//条件：計測時間が過ぎていたら
	if (pCountTimer_->EndOfTimerForCountUp())
	{
		//自身を消去
		this->KillMe();
	}
}
//自身へダメージを受ける
void Enemy::ReciveDamage(const int DAMAGE)
{
	//引数ダメージを自身メンバのHPから減らす
	pEnemyInfo_->HP -= DAMAGE;
}
//攻撃できるか
bool Enemy::IsPossibleToAttack()
{
	//条件：前回の攻撃からの経過時間　が　攻撃間隔を超えているか
	return elpasedTimeForBeforeAttack_ >= ATTACK_INTERVAL_ && IsAttack();
}
//攻撃を受けるか
bool Enemy::IsPossibleToRecivedDamage()
{
	//条件：前回の攻撃を受けた経過時間　が　攻撃を受けた間隔を超えているか
	return elpasedTimeForRecivedDamage_ >= TAKEN_DAMAGE_INTERVAL_;
}

//経路探索　移動
void Enemy::MyNavigation()
{
	//線形補間の更新
	bool isEndingDestinationPos = false;


	//条件：ナビゲーションされているか
	if (isNavigate_)
	{
		//線形補間の割合をSPEED分上げる
		interpolationRate += SPEED_;

		//条件：割合が１．０ｆを超えたなら
		if (interpolationRate > 1.0f)
		{
			//1.0fに切り捨て
			interpolationRate = 1.0f;

			//更新のフラグを立てる
				//線形補間が終了したので、
				//経路先を更新するフラグを立てる
			isEndingDestinationPos = true;

		}

		//割合分移動
		//線形補間実行
		ExecuteLinearInterpolation();


	}
	//条件：線形補間が終了していたら
	if (isEndingDestinationPos)
	{
		//移動経路の更新

		//線形補間関連のメンバ変数の初期化
		InitVariableForLerp();


		//ナビゲーションにて、
		//何個先のルートを算出するか
			//ナビのルートを何個飛ばしで行うか

			//ルート算出のマス目が細かく、移動の際にカクカクしてしまうので、ルートをある程度スキップして、カクカクしすぎないように設定
			//だが、ルートをスキップしてしまったら、
				//ルート算出をした意味がないし、
			//かつ、障害物があったら、障害物のルートをスキップして、通り抜けるようになってしまうかもしれない
				//それだと困る。
		int skip = 1;

		//条件：ナビゲーションが存在しているか
		if (pMyNavigator_ != nullptr)
		{
			//条件：ルートスキップ数
			for (int k = 0; k < (skip + 1); k++)
			{
				//条件：更新対象が存在しないとき
				if (!pMyNavigator_->RemoveAndUpdateNextRoute(hMyNavigation_))
				{
					//ナビゲーション終了を示す
					isNavigate_ = false;
					//とともに
					//攻撃開始を宣言
					StartAttack();

					break;
				}
			}

			//条件：ナビゲーション中であるとき
			if (isNavigate_)
			{
				//目的位置の更新
					//前回の目標座標を次の開始地点とする
				startingPos_ = nextDestinationPos_;	

				//ナビゲーションから、
					//自身のナビゲーションハンドルにて示される次の目的地を更新
				nextDestinationPos_ = pMyNavigator_->GetNextDestination(hMyNavigation_);
			}


		}

	}
}
//時間が停止しているか
bool Enemy::IsStopTime()
{
	return isStopTime_;
}


//線形補完割合の初期化
void Enemy::InitVariableForLerp()
{
	//割合の初期化
	interpolationRate = 0.0f;
}

//ナビゲーションにおける線形補完を行う
void Enemy::ExecuteLinearInterpolation()
{
	//ベクトル同士の線形補間をサポートする関数を使用して
	//線形補間実行

	//引数：０の位置のベクトル
	//引数：１の位置のベクトル
	//引数：０〜１の線形補間の割合
	transform_.position_ = XMVectorLerp(startingPos_, nextDestinationPos_, interpolationRate);

	////位置に変更を加えると、固定の位置にセットされて固定されてしまう
	//	//おそらく、木オブジェクトが、任意の形になっていないことと、同様の原因と考えられる。
		//現在の座標＋Y座標を一定量上にあげる
		//このY座標を上げる処理は、モデル自体の初期位置が、Y座標が低く設定されているため、微調整のためにY座標を上げる
	transform_.position_.vecY += 1.0f;

	//最後に更新した際のY座標をセットする
	lastUpdatePosY_ = transform_.position_.vecY;

}

//自身をナビゲーションするポインタをセット
void Enemy::SetMySpawn(EnemySpawn * pMySpawn)
{
	pMySpawn_ = pMySpawn;
}
//自身をナビゲーションするポインタをセット
void Enemy::SetMyNavigator(int hMyNavigation, NavigationForEnemy* pMyNavigator)
{
	pMyNavigator_ = pMyNavigator;

	//ハンドル番号の更新
	UpdateMyNavigationHandle(hMyNavigation);
	
}
//自身のナンバープレートをロード
void Enemy::LoadNumberPlate(int handle)
{
	//初期ナンバープレートのロード
	pNumberPlate_ = (NumberPlate*)Instantiate<NumberPlate>(this);
	//ナンバープレートの該当モデルのロード
	pNumberPlate_->LoadNumberAndType(NUMBER_PLATE_TYPE::NUMBER_PLATE_ENEMY, handle);
}
//時間の停止（行動、移動、攻撃などの停止）
void Enemy::StopTime()
{
	isStopTime_ = true;
}
//時間の再開（行動、移動、攻撃などの再開）
void Enemy::StartTime()
{
	isStopTime_ = false;
}
//デルタタイムの更新
void Enemy::UpdateDeltaTime(float elpasedTime)
{
	elpasedTimeForBeforeAttack_ += elpasedTime;
	elpasedTimeForRecivedDamage_ += elpasedTime;
	elpasedTimeForFloating_ += elpasedTime;

	//武器に時間経過知らせる
	pEnemyWeapon_->UpdateElpasedTime(elpasedTime);

}
//スポーンに、自身のインスタンスを追加　呼び込み
void Enemy::SpawnAddMyData()
{
	pMySpawn_->AddObject(this);
}
//スポーンに、自身のインスタンスを解放　呼び込み
void Enemy::SpawnRemoveMyData()
{
	//スポーンから自身のインスタンスを削除、解放
	pMySpawn_->RemoveMyObject(this , hMyNavigation_);

	//条件：自身が立たされたポリゴン番号が存在する場合
	if (targetPolyNum_ != -1)
	{
		//消滅時に
		//攻撃対象、ナビゲーション対象である番号が残っている場合	
			//その番号を伝える
		//その木への攻撃が終わっていないことを示す
		pMySpawn_->StartNavigationForTellCommandTower(targetPolyNum_);
	}
}
//ポリゴン番号から目的位置までの経路探索開始
void Enemy::StartNavigation(int startNum, int targetNum)
{
	//条件：ナビゲーションが存在する場合
	if (pMyNavigator_ != nullptr)
	{
		//ターゲットとなる
		//ポリゴン番号が、木の立っているポリゴン番号となるため、
			//上記を攻撃時の対象として使う
			//そのため、メンバ変数に保存
		targetPolyNum_ = targetNum;


		//ナビゲーションの開始
			//スタートのポリゴン番号と、ゴールの番号をセットする
		//pMyNavigator_->StartNavigation(startNum, targetNum);

		//引数：自身のナビゲーションへのハンドル
		//引数：スタート地点の座標を入れる変数
		//引数：スタートポリゴン
		//引数：ゴールポリゴン
		pMyNavigator_->StartNavigation(
			hMyNavigation_ , 
			startingPos_ , goalPos_,
			startNum, targetNum);

		//ナビゲーションのフラグを立てる
		isNavigate_ = true;

		//各線形補間の変数の初期化
		InitVariableForLerp();


		////スタート位置
		//startingPos_ = pMyNavigator_->GetStartingPosition();
		
		//GetNextDestination似て、
		//経路のリストの一番上の要素を取得して、
			//その要素のワールド座標を取得する
					//何も行わずにGetNextDestinationを呼び込むと、startのポリゴンと同様の座標を取得することになる。
					//それは避けたいので、
					//一番目の要素、つまり、startの要素を削るために、　一度一番目の要素を削って、次の要素がリストから取得できるように処理を行う
			//上記の処理は、RemoveAndUpdateNextRoute にて担う
		pMyNavigator_->RemoveAndUpdateNextRoute(hMyNavigation_);
		

		//次の目的位置の更新
		//nextDestinationPos_ = pMyNavigator_->GetNextDestination();
		nextDestinationPos_ = pMyNavigator_->GetNextDestination(hMyNavigation_);

		


		//看板描画
		//ナビゲーション開始を知らせる
		
		//木を倒したとき使用する
			//木の番号を入れる変数
		int treeNum = -1;
		//０ダメージを与える攻撃を行い
			//攻撃対象の木番号を取得
		CauseDamage(0, &treeNum);
		//条件：木番号がー１でなかったら
		if (treeNum != -1)
		{
			//ナビゲーション先を知らせる
			pMySpawn_->AddTalkWhenStartNavigation(hMyNavigation_, treeNum);
		}
		
	}

}
//自身のコライダーの所有しているデバック描画用のワイヤーフレームのモデル番号を与える
int Enemy::GetMyColliderModelHandle()
{
	return GetColliderModelHandleFromGameObject();
}
//自身のコライダーの所有しているデバック描画用のワイヤーフレームのモデルのTransformをセットする
void Enemy::SetMyColliderTransformWhenDrawing()
{
	SetColliderTransformDrawingFromGameObject();
}
//必要情報を（Enemy自身の情報）引数にて渡された参照私の構造体に格納する
void Enemy::SetEnemyInfomation(EnemyInfo * pToEnemyInfo)
{
	//EnemyInfoの引数元の
		//最初の要素、番号だけを保存し、
		//あとは、すべてコピーさせる
	int saveNumber = pToEnemyInfo->number;

	//コピー
		//pEnemyInfo->HP = pEnemyInfo_->HP;
		//pEnemyInfo->attackDamage = pEnemyInfo_->attackDamage;
	memcpy(pToEnemyInfo, pEnemyInfo_, sizeof(EnemyInfo));

	//保存しておいた番号を代入
	pToEnemyInfo->number = saveNumber;

}
//仕事がないかの確認
bool Enemy::WantJob()
{
	//条件：移動中でない
	//　　：&&
	//　　：攻撃中でない
	return !IsMoveing() && !IsAttack();
}
//強制的に自身オブジェクトを削除
void Enemy::ForciblyErase()
{
	//消去モーション開始
	StartEraseMotion();
}
//敵の捕縛
void Enemy::BindEnemy()
{
	isBind_ = true;
}
//敵の捕縛の解除
void Enemy::UnBindEnemy()
{
	isBind_ = false;
}
//自身が立っているポリゴンを渡す
int Enemy::GetPolygonIStandingOn()
{

	int polyNum = -1;
	//条件：ナビゲーションが存在する場合
	if (pMyNavigator_ != nullptr)
	{

		//ダメージを受ける
			//現在のポリゴンをナビゲーションから調べる
			//そのポリゴンが、緑化、黒かをManagerを経由して調べる　ISGreen〜

			//応じて、ダメージを変える
		//ポリゴン番号取得
		polyNum = pMyNavigator_->GetNextPolygon(hMyNavigation_);

		//条件：ポリゴン番号　ー１である場合
		if (polyNum == -1)
		{
			//ー１の場合
				//現在の座標から、衝突する、ポリゴン番号を求める
			polyNum = pMySpawn_->RayCastForEnemyAndGround(this);
		}

	}


	return polyNum;

}
//ナビゲーションにアクセスするためのハンドルを更新
void Enemy::UpdateMyNavigationHandle(int handle)
{
	//ハンドルの更新
	hMyNavigation_ = handle;

	//条件：ナビゲーションハンドル　ー１でない
	if (hMyNavigation_ != - 1)
	{
		//条件：NumberPlate_が確保されていないなら
			//確保から始める
		if (pNumberPlate_ == nullptr)
		{
			//番号をロードする
			LoadNumberPlate(hMyNavigation_);
		}
		else
		{
			//ナビゲーションのハンドル番号と
			//自身のスポーンにおける番号は同じなので、
				//番号の更新をかける
				//自身の番号プレートの番号も更新する
			pNumberPlate_->UpdateNumber(hMyNavigation_);
		}
	}

}

