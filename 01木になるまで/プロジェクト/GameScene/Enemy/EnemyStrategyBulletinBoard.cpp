#include "EnemyStrategyBulletinBoard.h"			//ヘッダ
#include "../../CommonData/GlobalMacro.h"		//共通マクロ
#include "../../Engine/Text/Texts.h"			//テキスト
#include "../../Engine/GameObject/UIGroup.h"	//UIGroup群
#include "../../Engine/Scene/SceneUIManager.h"	//シーンUIマネージャー
#include "../../Engine/Audio/Audio.h"			//音楽クラス
#include "../Scene/GameSceneManager.h"			//シーンマネージャー
#include "../CountTimer/CountTimer.h"			//カウントタイマークラス


//コンストラクタ
EnemyStrategyBulletinBoard::EnemyStrategyBulletinBoard(GameObject* parent)
	: GameObject(parent, "EnemyStrategyBulletinBoard"),
	pSceneManager_(nullptr),
	isStopTime_(false),

	pTexts_(nullptr),
	pCommonTexts_(nullptr),
	pCountTimer_(nullptr),

	//pEnemySpawn_(nullptr),

	pCurrentTalkInfo_(nullptr),
	pUIForEachTalks_(nullptr),
	hAudioForEachTalks_(nullptr) , 
	pUIGroup_(nullptr)
{
	//可変配列のクリア
	pNextTalkInfos_.clear();
	visibleImageNums_.clear();
}

//デストラクタ
EnemyStrategyBulletinBoard::~EnemyStrategyBulletinBoard()
{
	//解放
	SAFE_DELETE_ARRAY(hAudioForEachTalks_);
	SAFE_DELETE_ARRAY(pUIForEachTalks_);
	SAFE_DELETE(pCurrentTalkInfo_);
	SAFE_DELETE(pTexts_);
}

//初期化
void EnemyStrategyBulletinBoard::Initialize()
{
	{
		//シーンマネージャーの取得
			//シーンチェンジャーを経由して
				//引数指定のシーンのマネージャークラスを取得
		pSceneManager_ = (GameSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);

		//SceneManagerに自身を登録する
			//敵、の情報、攻撃情報などを取得する必要があるため、連携オブジェクトとして登録する
		pSceneManager_->AddCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_BULLETIN_BOARD, (GameObject*)this);
	}

	//必要画像のロード
	CreateUIGroup();


	{
		//テキスト初期化
		pTexts_ = new Texts;
		pTexts_->Initialize();
		//フォントサイズの変更
		pTexts_->SetFontSize(25);
	}
	{
		//タイマー計測開始
		pCountTimer_ = (CountTimer*)Instantiate<CountTimer>(this);
		//テキスト描画間隔のセット
		pCountTimer_->StartTimerForCountUp(INTERVAL_START, INTERVAL_END);
	}


	//会話ごとの処理関数を関数ポインタとしてセットする
	SetFunctionPointaForEachTalks();

	//各会話タイプごとに
		//会話描画時の描画UIのタイプを、会話タイプをセット
	SetUIForEachTalks();

	//音源のロード
	SetAudioForEachTalks();

	//全UIの非描画
	InvisibleImage();

}

//更新
void EnemyStrategyBulletinBoard::Update()
{
	//一定時間終了後
		//現在のテキストを変更（スタックに存在するテキストで更新）
	//条件：カウントアップの計測が終了したら
	if (pCountTimer_->EndOfTimerForCountUp())
	{
		//現在再生中の音源を停止する
		StopAudioCurrentTalkInfo();

		//現在の会話情報を破棄して
		//スタックされている会話情報の先頭のデータを現在の会話情報とする
		TalkInfo* pNextTalkInfo = GetStackTopData();
	
		//現在の会話情報を削除
		DeleteCurrentTalkInfo();

		//全UIの非描画
		InvisibleImage();
		//現在の描画対象の画像番号を削除
		InitVisibleImageNums();

		//条件：取得したデータが空（ヌルポインタ）でないならば
		if (pNextTalkInfo != nullptr)
		{
			//更新
			UpdateCurrentTalkInfo(pNextTalkInfo);

			{
				//会話タイプによる、　描画対象のUI群を持つ可変配列を取得
				const std::vector<UI_TYPE_BULLETIN_BOARD> visibleImages
					= pUIForEachTalks_[pNextTalkInfo->talkType];


				//描画対象のUIを選択
					//会話タイプを取得し、
					//会話タイプごとの描画UIのタイプを登録しているので、
					//その登録しているUIの描画を宣言
				//条件：描画画像分
				for (int i = 0; i < visibleImages.size(); i++)
				{
					//描画先として登録
						//取得した、UI群の要素一つ一つにアクセスして、
						//描画する画像番号をもらい、描画先として登録する
					VisibleImage(visibleImages[i]);

				}

				//描画指示
					//描画対象、描画先として登録した、画像番号を実際に描画
				VisiblePermitImage();

				//次の描画対象と同様の添え字を持つ音源再生
				Audio::Play(hAudioForEachTalks_[pNextTalkInfo->talkType]);
			}

			//更新を終えたデータは、削除依頼
			EraseStackTopData();

			//ポインタを使えないようにnullptrへ
			pNextTalkInfo = nullptr;

		}
		//タイマーリスタート
		pCountTimer_->ReStartTimerForCountUp(0 , INTERVAL_START, INTERVAL_END);
	
	}


}

//会話中の音楽を停止
void EnemyStrategyBulletinBoard::StopAudioCurrentTalkInfo()
{
	//条件：現在のトーク情報が存在する場合
	if (pCurrentTalkInfo_ != nullptr)
	{
		//現在の音源を停止
		Audio::Stop(hAudioForEachTalks_[pCurrentTalkInfo_->talkType]);
	}
}

//描画
void EnemyStrategyBulletinBoard::Draw()
{
	/*
	//シーンマネージャーの描画許可が行われていたら
		//シーンマネージャーの時間停止の機能を下記は、シーンマネージャーを読み込んで時間停止の有無を読み込んでいる
		//だが、ほかの敵スポーンや、木のグループクラスは、時間停止が指示されたら、直接敵スポーンに知らせて、自らの時間停止の有無のフラグで認識するようにしている
			//上記2つの違いは、　
				//→自身のような一つのクラスに関係する時間停止の場合（敵スポーンのように、複数のオブジェクトにさらに時間停止を伝える必要がない）
					//→時間停止を指示されたタイミングでわざわざ自身のクラスを呼び出す必要がないので、シーンマネージャーにコード記述の必要がないという利点
					//→すべての時間停止を受けたいクラスを、シーンマネージャーに呼んでもらう二はいけない（なぜなら、自身のクラスをシーンマネージャーに知ってもらっている必要が出てきてしまう。其れよりは、自身がシーンマネージャーを知っていて、シーンマネージャーに時間停止していますか？と聞く方が、シーンマネージャーに余計な情報を持たず済む）
				//→敵スポーンのように複数の子供のクラスがいる時間停止の場合（スポーン→各敵オブジェクトへ伝えないといけない）
					//→都度Updateで、自身が行動できるか、シーンマネージャーに聞く必要があると、処理工数的に多くなるので、
					//→上記の場合は、時間停止となったときに、あらかじめ伝えて、今後シーンマネージャーに聞くという無駄な行動をなくす。

	//後述
	//SceneManagerから、時間停止を宣言してもらうことにした。
		//理由は、自身のクラスでタイマーを持ち、そのタイマーで計測をする
		//そのタイマーを止めるには、停止を宣言された時点で停止を宣言する必要がある。そのため、停止関数を作り、停止を宣言する形をとる

	*/


	//条件：時間が停止していないならば
	if (!IsStopTime())
	{
		//テキストにセットするテキスト描画の開始位置は、
			//文字、一文字目開始位置の座標に該当する
			//そのため、文の開始位置を揃えるならば、
				//同じx、同じｙにすると同様の開始位置からのテキストとなる。
			//行区切り（次の行に行くかなどの判断は、表示文字バイト数で判断？）
			

		//描画対象がいたら
		if (pCurrentTalkInfo_ != nullptr 
			&& pFunctionForEachTalks_[pCurrentTalkInfo_->talkType] != nullptr)
		{
			//現在の描画タイプを受け取り
				//その描画タイプによって、処理関数が書かれている関数を呼び込む
			//関数ポインタの配列にて、
				//現在描画するタイプを配列の添え字として、アクセスする関数を選択する
			(this->*pFunctionForEachTalks_[pCurrentTalkInfo_->talkType])();
		}

	}

}

//テキスト
//一行目の描画
void EnemyStrategyBulletinBoard::DrawTextFirstLine(const std::string& drawText)
{
	pTexts_->DrawStringTex(drawText, TEXT_COLOR_VIOLET, FIRST_LINE_X, FIRST_LINE_Y);
}

//テキスト
//二行目の描画
void EnemyStrategyBulletinBoard::DrawTextSecondLine(const std::string& drawText)
{
	pTexts_->DrawStringTex(drawText, TEXT_COLOR_VIOLET, SECOND_LINE_X, SECOND_LINE_Y);
}

//解放
void EnemyStrategyBulletinBoard::Release()
{
}

//引数構造体のベクター（可変長配列）に文字列を追加
void EnemyStrategyBulletinBoard::PushBackStr(TalkInfo* stackTalkInfo, const std::string& pushBackStr)
{
	//引数の構造体の
	//可変長配列の文字列軍に
		//引数文字列を追加
	stackTalkInfo->talkTexts.push_back(pushBackStr);
}

//会話のスタック
void EnemyStrategyBulletinBoard::AddStackTalkInfo(TalkInfo* stackTalkInfo)
{
	//スタックに追加
	pNextTalkInfos_.push_back((*stackTalkInfo));
}

//時間が停止しているか
bool EnemyStrategyBulletinBoard::IsStopTime()
{
	return isStopTime_;
}

//時間の停止（行動、移動、攻撃などの停止）
void EnemyStrategyBulletinBoard::StopTime()
{
	isStopTime_ = true;
	//タイマーを止める　計測禁止
	pCountTimer_->NotPermittedTimer();
	//UIGroupの描画停止(自身が管理するUIGroupの全UIを非描画)
	InvisibleImage();
}

//時間の再開（行動、移動、攻撃などの再開）
void EnemyStrategyBulletinBoard::StartTime()
{
	isStopTime_ = false;
	//タイマー計測許可
	pCountTimer_->PermitTimer();
	//描画対象の画像だけ描画
	VisiblePermitImage();
}

//UIGroupにおける　必要画像の作成、ロード
void EnemyStrategyBulletinBoard::CreateUIGroup()
{
	//読み込み画像ファイル名
	const std::string FILE_NAME[BULLETIN_BOARD_UI_MAX] =
	{
		//BULLETIN_BOARD_TEXT_FRAME
		"Assets/Scene/Image/GameScene/Frame/Frame_EnemyStrategyText.png",
		//BULLETIN_BOARD_ENEMY_ICON
		"Assets/Scene/Image/GameScene/EnemyBulletinBoard/EnemyLogo.png",
		//BULLETIN_BOARD_COMMAND_TOWER_ICON
		"Assets/Scene/Image/GameScene/EnemyBulletinBoard/EnemyKingLogo.png",

	};

	//画像の設置位置の宣言
	const XMVECTOR SET_POS[BULLETIN_BOARD_UI_MAX] =
	{
		//BULLETIN_BOARD_TEXT_FRAME
		XMVectorSet(500,100,0,0),
		//BULLETIN_BOARD_ENEMY_ICON
		XMVectorSet(100,100,0,0),
		//BULLETIN_BOARD_COMMAND_TOWER_ICON
		XMVectorSet(100,100,0,0),


	};
	//画像のサイズの宣言
	const XMVECTOR SET_SIZE[BULLETIN_BOARD_UI_MAX] =
	{
		//BULLETIN_BOARD_TEXT_FRAME
		XMVectorSet(700,100,0,0),
		//BULLETIN_BOARD_ENEMY_ICON
		XMVectorSet(100,100,0,0),
		//BULLETIN_BOARD_COMMAND_TOWER_ICON
		XMVectorSet(100,100,0,0),

	};


	//UIGroupのインスタンス生成
	pUIGroup_ = (UIGroup*)Instantiate<UIGroup>(this);
	//シーンUIManager取得
	SceneUIManager* pSceneUIManager = pUIGroup_->GetSceneUIManager();



	//条件：UIのMAX（enum）
	for (int i = 0; i < BULLETIN_BOARD_UI_MAX; i++)
	{
		//ロード
		pUIGroup_->AddStackUI(FILE_NAME[i]);

		//位置のセット
		pSceneUIManager->SetPixelPosition(i , (int)SET_POS[i].vecX, (int)SET_POS[i].vecY);
		//サイズのセット
		pSceneUIManager->SetPixelScale(i, (int)SET_SIZE[i].vecX, (int)SET_SIZE[i].vecY);
	}

}

//関数ポインタの登録
void EnemyStrategyBulletinBoard::SetFunctionPointaForEachTalks()
{
	{
		//初期化
		//条件：トークタイプMAX（enum）
		for (int i = 0; i < TALK_MAX; i++)
		{
			pFunctionForEachTalks_[i] = nullptr;
		}

		//enumタイプごとに呼び出す関数を設定
		//HELLO
		pFunctionForEachTalks_[HELLO] = &EnemyStrategyBulletinBoard::DrawTextHello;
		//GOOD_MOR
		pFunctionForEachTalks_[GOOD_MORNING] = &EnemyStrategyBulletinBoard::DrawTextGoodMorning;
		//GOOD_EVE
		pFunctionForEachTalks_[GOOD_EVENING] = &EnemyStrategyBulletinBoard::DrawTextGoodEvening;

	}
}

//会話ごとの描画UIタイプのセット
void EnemyStrategyBulletinBoard::SetUIForEachTalks()
{
	//可変長配列の動的確保
	//会話ごとなので、　会話のタイプの最高値数要素を　動的確保
	pUIForEachTalks_ = new std::vector<UI_TYPE_BULLETIN_BOARD>[TALK_MAX];

	//会話ごとに
	//UIタイプの登録
	{
		//HELLO
		pUIForEachTalks_[HELLO].push_back(BULLETIN_BOARD_UI_TEXT_FRAME);
		pUIForEachTalks_[HELLO].push_back(BULLETIN_BOARD_UI_ENEMY_ICON);
		//GOOD_MOR
		pUIForEachTalks_[GOOD_MORNING].push_back(BULLETIN_BOARD_UI_TEXT_FRAME);
		pUIForEachTalks_[GOOD_MORNING].push_back(BULLETIN_BOARD_UI_COMMAND_TOWER_ICON);
		//GOOD_EVE
		pUIForEachTalks_[GOOD_EVENING].push_back(BULLETIN_BOARD_UI_TEXT_FRAME);
		pUIForEachTalks_[GOOD_EVENING].push_back(BULLETIN_BOARD_UI_ENEMY_ICON);

	}

}

//会話ごとのSEタイプのセット（同時に、音楽のロード）
void EnemyStrategyBulletinBoard::SetAudioForEachTalks()
{
	//音楽をロードして、
	//会話ごとに、音楽のハンドル番号を登録しておく

	//音楽ファイルのファイルパス
	const std::string FILE_NAME[TALK_MAX] =
	{
		//HELLO
		"Assets/Scene/Sound/GameScene/EnemySE/message1.wav" , 
		//GOOD_MOR
		"Assets/Scene/Sound/GameScene/EnemySE/message2.wav" , 
		//GOOD_EVE
		"Assets/Scene/Sound/GameScene/EnemySE/message1.wav" , 

	};

	hAudioForEachTalks_ = new int[TALK_MAX];

	//ロードと、ハンドル番号の登録
	//条件：トークタイプMAX（enum）
	for (int i = 0; i < TALK_MAX; i++)
	{
		//ロード
		hAudioForEachTalks_[i] =  Audio::Load(FILE_NAME[i]);
		//ロードできたかの判断
		assert(hAudioForEachTalks_[i] != -1);
	}

	//Updateにて、
		//新しい会話が設定されたら、
		//そのタイミングで、音源再生

}

//スタックの先頭データを取得	
TalkInfo* EnemyStrategyBulletinBoard::GetStackTopData()
{
	//条件：イテレータの最終要素でない
	if (pNextTalkInfos_.begin() != pNextTalkInfos_.end())
	{
		//先頭データを取得
		auto itr = pNextTalkInfos_.begin();
		//先頭データの
			//イテレータにて示されるデータ部。そのアドレスを渡す
		return &(*itr);
	}
	
	//何もなし
	return nullptr;
}

//スタックの先頭データを削除
void EnemyStrategyBulletinBoard::EraseStackTopData()
{
	//条件：イテレータの最終要素でない
	if (pNextTalkInfos_.begin() != pNextTalkInfos_.end())
	{
		//先頭データを削除
		auto itr = pNextTalkInfos_.begin();
		//先頭データを削除
		pNextTalkInfos_.erase(itr);
	}
}

//現在描画中の画像番号を削除する
void EnemyStrategyBulletinBoard::InitVisibleImageNums()
{
	//クリア
	visibleImageNums_.clear();
}

//現在描画中と判断されている画像番号を使用し画像を描画させる
void EnemyStrategyBulletinBoard::VisiblePermitImage()
{
	//描画番号に登録された画像の描画
	//条件：イテレータにて示される最後の要素でない
	for (auto itr = visibleImageNums_.begin(); itr != visibleImageNums_.end(); itr++)
	{
		//画像番号を指定して、
			//画像を描画指示
		pUIGroup_->VisibleSceneUI((*itr));
	}
}

//描画する画像番号として登録
void EnemyStrategyBulletinBoard::VisibleImage(UI_TYPE_BULLETIN_BOARD uiType)
{
	//番号を、描画対象の画像番号としてリストに登録
	visibleImageNums_.push_back((int)uiType);
}

//全画像の非描画
void EnemyStrategyBulletinBoard::InvisibleImage()
{
	//UIGroupの全非描画
	//描画番号に登録された画像の描画
	pUIGroup_->InvisibleSceneUIAndButton();

}

//会話情報の更新（現在描画の会話の更新）
void EnemyStrategyBulletinBoard::UpdateCurrentTalkInfo(TalkInfo* nextTalkInfo)
{
	////現在の会話情報を削除
	//DeleteCurrentTalkInfo();
		//更新を呼ぶ時点で行うので心配なし

	//会話情報の動的生成
		//可変サイズのデータのコピーは、そのままサイズ分コピーで確保可能なのか？
		//動的確保時点で、可変長配列のサイズ分メモリを確保できるのか？→動的確保で、データが入る部分を確保した。そのサイズ以上の可変長配列のサイズは格納できるのか

		//上記の関係で、アクセスしてはいけないところもコピーによって上書きされると困る
		//実際、実行してみて、動かなかった。→文字列がうまくコピーできていなかった。（確か、文字列は、strcpyが必要だった。）
			//→なので、現段階では、手間だが、コピー元の可変配列に登録した要素を、一つ一つコピー先の可変配列にPushBackしていく形にする
	pCurrentTalkInfo_ = new TalkInfo(nextTalkInfo->talkType);

	//データのコピー
	//コピー元の可変配列に登録された文字列を
		//コピー先の可変配列にスタックする
	//条件：引数可変長配列のサイズ
	for (int i = 0; i < nextTalkInfo->talkTexts.size(); i++)
	{
		//追加
		PushBackStr(pCurrentTalkInfo_, nextTalkInfo->talkTexts[i]);
	}
}

//会話情報の削除（現在描画の会話の削除）
void EnemyStrategyBulletinBoard::DeleteCurrentTalkInfo()
{
	//現在の会話情報の解放
	//可変長配列を持っていたりと、サイズが可変なので、一度解放
	SAFE_DELETE(pCurrentTalkInfo_);
}

//こんにちは（テスト）　表示
void EnemyStrategyBulletinBoard::DrawTextHello()
{
	//可変長配列の要素への添え字
	int line = 0;

	//こんにちは表示
	//通常描画
	//DrawTextFirstLine(pCurrentTalkInfo_->talkText);
	DrawTextFirstLine(pCurrentTalkInfo_->talkTexts[line]);

}

//おはようございます（テスト）　表示
void EnemyStrategyBulletinBoard::DrawTextGoodMorning()
{
	//可変長配列の要素への添え字
	int line = 0;


	//通常描画
	//DrawTextFirstLine(pCurrentTalkInfo_->talkText);
	DrawTextFirstLine(pCurrentTalkInfo_->talkTexts[line]);
	//2行に分けるとき
		//pCurrentTalkInfoに登録された文字列は、一行なので、どこかで、文字列を分ける。あるいは、複数の文字列を登録できるようにする必要がある。

	//スタックの文字列として登録するときも、
		//何かの文字列によって、一行を複数文字列に分けるならば、　その記号となる単語が必要。
		//であれば、結合して、区切り文字で区切った。文字列を作る必要がある。

		//上記の文字列から、分割する関数も必要
		//→であれば、一つの文字列に結合するよりも、可変長配列で複数文字列を管理するほうがよさそう。（管理は楽）


	//添え字更新
	line = 1;

	//現在の会話情報に登録された
		//可変長配列の文字列の中から
		//２番目の要素を取得する
	//２番目の要素が存在するならば
		//添え字の０オリジンの値　＋　１　以上値をもっているならば
	if (pCurrentTalkInfo_->talkTexts.size() >= (line + 1))
	{
		DrawTextSecondLine(pCurrentTalkInfo_->talkTexts[line]);
	}

}

//こんばんは（テスト）　表示
void EnemyStrategyBulletinBoard::DrawTextGoodEvening()
{
	//可変長配列の要素への添え字
	int line = 0;

	{
		//通常描画
		DrawTextFirstLine(pCurrentTalkInfo_->talkTexts[line]);
	}
	{
		//添え字更新
		line = 1;

		//現在の会話情報に登録された
			//可変長配列の文字列の中から
			//２番目の要素を取得する
		//２番目の要素が存在するならば
			//添え字の０オリジンの値　＋　１　以上値をもっているならば
		if (pCurrentTalkInfo_->talkTexts.size() >= (line + 1))
		{
			DrawTextSecondLine(pCurrentTalkInfo_->talkTexts[line]);
		}
	}

}


//コンストラクタ
TalkInfo::TalkInfo() : TalkInfo(TALK_TYPE::HELLO)
{
}
//コンストラクタ
TalkInfo::TalkInfo(TALK_TYPE setType)
	:talkType(setType)
{
}
