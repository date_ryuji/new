#include "EnemySpawn.h"							//ヘッダ
#include "../../Engine/Model/Model.h"			//モデル
#include "../../Engine/Algorithm/RayCaster.h"	//レイ実行クラス
#include "../../Engine/Algorithm/Timer.h"		//タイマークラス
#include "../Scene/GameSceneManager.h"			//シーンマネージャークラス
#include "../CountTimer/CountTimer.h"			//カウントタイマークラス
#include "NavigationForEnemy.h"					//敵オブジェクトのナビゲーター(ナビゲーションAI)
#include "Enemy.h"								//敵オブジェクト
#include "EnemyCommandTower.h"					//敵の司令塔オブジェクト

//コンストラクタ
EnemySpawn::EnemySpawn(GameObject * parent) : 
	Spawn::Spawn(parent , "EnemySpawn"),
	pSceneManager_(nullptr),
	pCommandTower_(nullptr)
{
}

//デストラクタ
EnemySpawn::~EnemySpawn()
{
	//親クラスメソッドの解放を呼ぶ
	ReleaseAllData();
}

//初期化
void EnemySpawn::Initialize()
{
	{
		//シーンマネージャークラスを探す
		//シーンチェンジャーを経由して
				//引数指定のシーンのマネージャークラスを取得
		pSceneManager_ = (GameSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);


		//自身を登録する
		pSceneManager_->AddCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_SPAWN, (GameObject*)this);
	}

	//司令塔の作成
	{
		//インスタンス生成
		pCommandTower_ = (EnemyCommandTower*)Instantiate<EnemyCommandTower>(this);
		//自身（スポーン）のセット
		pCommandTower_->SetEnemySpawn(this);
	}

}

//更新
void EnemySpawn::Update()
{
}

//描画
void EnemySpawn::Draw()
{
}

//解放
void EnemySpawn::Release()
{
}

//リストから解放かつ解放時の処理を追加
void EnemySpawn::RemoveMyObject(GameObject* pGameObject, int enemyNum)
{
	//親の解放処理を呼び込み
	RemoveObject(pGameObject);

	//解放を看板テキストに表示してもらう
	AddTalkWhenReleaseEnemy(enemyNum);

}

//スポーン管理のオブジェクト作成
GameObject* EnemySpawn::AddNewInstance(GameObject * pParent)
{
	//引数を親として
	//Enemyクラスのインスタンスを生成
	GameObject* pEnemy = Instantiate<Enemy>(pParent);

	//リストへ追加
	AddObject(pEnemy);

	return pEnemy;
}

//木への攻撃実行
bool EnemySpawn::AttackSuccess(const int TARGET_POLY_NUM, const int DAMAGE, bool* isKill, int* treeNum)
{
	//ダメージを与える
	return pSceneManager_->AttackSuccess(TARGET_POLY_NUM , DAMAGE , isKill , treeNum);
}

//すべての敵データとのレイキャストを行う
EnemyInfo EnemySpawn::RayCastToAllData()
{
	//衝突判定前に
		//衝突判定結果を入れるフラグの初期化
	//pData->hit = false;

	//レイキャスト実行クラス
	RayCaster* pRayCaster = new RayCaster;

	//レイキャスト実行クラスに
		//現在のマウス位置をワールド座標にして
		//その座標を世界の終端まで伸ばした時のレイをベクトルでもらう
	//�@引数：マウスクリック位置をワールド座標に変換したベクトル
	//�A引数：マウスクリック位置のワールド座標から、カメラで見ている世界の終端までベクトルを伸ばした時の、ちょうど終端のワールド座標

	//�@
	XMVECTOR mousePosFront;
	//�A
	XMVECTOR mousePosBack;

	//取得
	pRayCaster->GetWorldMouseCursorPos(&mousePosFront , &mousePosBack);


	//衝突したかのフラグ
	bool hit = false;

	//衝突した敵オブジェクトをみつけ
		//その敵の情報を与える
	EnemyInfo info;

	//オブジェクト群のリスト
		//最初の要素へのイテレータ
	auto itr = objects_.begin();

	//オブジェクトの番号
		//リストの何番目のデータかをもらう
	int number = 0;


	//条件：全Enemyオブジェクトを回し
		//引数レイキャストと当たり判定を行う
	for (itr; itr != objects_.end(); itr++)
	{
		//オブジェクトのポリゴン数が多くなってしまい、
			//かつ、当たり判定もおおよその範囲でよいので、
			//オブジェクトのキャラクターモデルではなく、
			//オブジェクトのコライダーのモデル（低ポリゴン）との当たり判定を行うようにする
		
		//インスタンスをEnemy型に変換して取得
		Enemy* pEnemy = (Enemy*)(*itr);

		//コライダーのハンドル取得
		int handle = pEnemy->GetMyColliderModelHandle();

		//上記で取得したモデルのTransformをセットさせる
		pEnemy->SetMyColliderTransformWhenDrawing();


		//当たり判定実行
			//レイキャストは、引数のhandleにて、セットされているTransformを使用して変形をしてからレイキャストを行う
			//そのため、上記でSetMyColliderTransformWhenDrawing（）により、実際の描画時のTransformにする（SetTransformをするので、行列の計算済み）
		hit = pRayCaster->RayCastToVectorDirection(handle, mousePosFront, mousePosBack);



		//条件：衝突したか
		if (hit)
		{
			//ヒットした場合
				//直ぐに処理を終了する
				//一番最初に当たったもの対象なので、
				//本来は、一番距離の短い相手を対象とするのが良いのだろうが、計算量がその分多くなるので要検討
			break;

		}

		//ナンバー
			//リストの番号を更新
		number++;

	}


	//条件：当たっていたら
	//当たった対象の情報を与える
	if (hit)
	{
		//当たった相手の情報格納
			//インスタンスをEnemy型に変換して取得
		Enemy* pEnemy = (Enemy*)(*itr);

		//Enemy番号をセット
		info.number = number;


		//各情報取得
		pEnemy->SetEnemyInfomation(&info);

	}

	return info;
}

//自身の管理する敵オブジェクト全部へ　時間の停止宣言
void EnemySpawn::StopTimeAllObject()
{
	//自身のクラスの時間停止のフラグも立てる
	StopTime();

	//司令塔の停止フラグも立てる
	pCommandTower_->StopTime();


	//条件：イテレータの最後の要素でない
	//時間停止を宣言
	for (auto itr = objects_.begin(); itr != objects_.end(); itr++)
	{
		//オブジェクトをEnemy型へキャスト
			//キャストを行うため、
			//必ず、登録しているオブジェクトは、Enemy型である前提
			//AddNewInstanceにて、Enemy方以外を追加しないため、今回に関しては、問題なし
		Enemy* pEnemy = (Enemy*)(*itr);


		//時間停止を宣言
		pEnemy->StopTime();

	}
}

//自身の管理する敵オブジェクト全部へ　時間の再開宣言
void EnemySpawn::StartTimeAllObject()
{
	//自身のクラスの時間停止のフラグもおろす
	StartTime();
	//司令塔の停止フラグもおろす
	pCommandTower_->StartTime();


	//条件：イテレータの最後の要素でない
	//時間計測再開を宣言
	for (auto itr = objects_.begin(); itr != objects_.end(); itr++)
	{
		//オブジェクトをEnemy型へキャスト
		Enemy* pEnemy = (Enemy*)(*itr);

		//時間計測再開を宣言
		pEnemy->StartTime();

	}
}

//緑ポリゴンか調べる（マネージャーシーンへ）
bool EnemySpawn::IsGreenPolygon(int polyNum)
{
	return pSceneManager_->IsGreenPolygon(polyNum);
}

//引数のEnemyオブジェクトと地面とのレイキャスト行い衝突判定を行う
int EnemySpawn::RayCastForEnemyAndGround(Enemy* pEnemy)
{
	return pSceneManager_->RayCastForEnemyAndGround(pEnemy);
}

//強制的に敵を一体生成させる
void EnemySpawn::ForciblyCreateEnemy()
{
	//敵司令塔における生成手順を踏むため、	
	//敵司令塔に生成の処理一式を呼び出す
	pCommandTower_->ForciblyCreateEnemy();

}

//引数オブジェクトが敵スポーンに存在するか
bool EnemySpawn::IsExistsTarget(GameObject* pTarget)
{
	//条件：引数オブジェクトが自身インスタンス群に存在するかの確認
	if (FindMyObject(pTarget))
	{
		//見つかった場合
		//Trueを返す
			//存在している
		return true;

	};

	return false;

}

//強制的に引数オブジェクトを削除させる
void EnemySpawn::ForciblyEraseEnemy(GameObject* pTarget)
{
	//条件：引数オブジェクトが自身インスタンス群に存在するかの確認
	if (FindMyObject(pTarget))
	{
		//見つかった引数オブジェクトを
		//Enemy型でキャスト
		Enemy* pEnemy = (Enemy*)pTarget;
		//オブジェクトを削除
		pEnemy->ForciblyErase();

	}
}

//強制的に指定オブジェクトを捕縛させる
void EnemySpawn::ForciblyBindEnemy(GameObject* pTarget)
{
	//条件：引数オブジェクトが自身インスタンス群に存在するかの確認
	if (FindMyObject(pTarget))
	{
		//見つかった引数オブジェクトを
		//Enemy型でキャスト
		Enemy* pEnemy = (Enemy*)pTarget;
		//オブジェクトをバインドする
		pEnemy->BindEnemy();

	}
}

//捕縛解除
void EnemySpawn::ForciblyUnBindEnemy(GameObject* pTarget)
{
	//条件：引数オブジェクトが自身インスタンス群に存在するかの確認
	if (FindMyObject(pTarget))
	{
		//見つかった引数オブジェクトを
		//Enemy型でキャスト
		Enemy* pEnemy = (Enemy*)pTarget;
		//オブジェクトをバインド解除
		pEnemy->UnBindEnemy();
	}
}

//敵が存在するポリゴン番号をすべて知る
void EnemySpawn::GetPolygonsWithEnemies(std::vector<int>* polygonsWithEnemies)
{

	//条件：イテレータの最後の要素ではないとき
	//現在立っているポリゴン番号を知るには、
	//いったい、いったいを地面とのレイキャストでポリゴン番号を調べるより、
		//ナビゲーションにて、聞く。あるいは、敵自身に保存しておいて、聞く

	//敵のインスタンス一人一人に、自身の立っているポリゴン番号を取得
	for (auto itr = objects_.begin(); itr != objects_.end(); itr++)
	{
		//Enemy型へキャスト
		Enemy* pEnemy = (Enemy*)(*itr);
		//ポリゴン番号を登録
		polygonsWithEnemies->push_back(pEnemy->GetPolygonIStandingOn());
	}

}

//司令塔へナビゲーションへアクセスするためのハンドル番号の更新を行ってもらう
void EnemySpawn::ReassigningMyNavigationHandle()
{
	//司令塔の同様のハンドルを呼び込む
	pCommandTower_->ReassigningMyNavigationHandle();
}

//ナビゲーション先を指令塔へ伝える
void EnemySpawn::StartNavigationForTellCommandTower(int targetPolyNum)
{
	pCommandTower_->StartNavigation(targetPolyNum);
}

//木を倒した
void EnemySpawn::AddTalkWhenTreeFall(int enemyNum, int treeNum)
{
	//敵司令塔へ情報を渡して、
		//関数先にて、表示する文字列を作成、情報看板へ渡す
	pCommandTower_->AddTalkWhenTreeFall(enemyNum, treeNum);
}

//敵が倒れた
void EnemySpawn::AddTalkWhenReleaseEnemy(int enemyNum)
{
	//敵司令塔へ情報を渡して、
	//関数先にて、表示する文字列を作成、情報看板へ渡す
	pCommandTower_->AddTalkWhenReleaseEnemy(enemyNum);
}

//ナビゲーションを行っていたが対象の木が存在しなかった(目的地に木が存在しなかった)
void EnemySpawn::AddTalkWhenNullTree(int enemyNum)
{
	pCommandTower_->AddTalkWhenNullTree(enemyNum);
}

//攻撃開始を宣言
void EnemySpawn::AddTalkWhenStartAttack(int enemyNum, int treeNum)
{
	pCommandTower_->AddTalkWhenStartAttack(enemyNum, treeNum);
}

//敵のナビゲーションを知らせる
void EnemySpawn::AddTalkWhenStartNavigation(int enemyNum, int treeNum)
{
	pCommandTower_->AddTalkWhenStartNavigation(enemyNum, treeNum);
}
//ナビゲーション命令
	//該当のオブジェクトを、目的ピクセル位置へ移動させる
	//ナビゲーションの開始位置は、現在敵オブジェクトが存在するポリゴン番号
		//＝　現在敵オブジェクトの座標から、下へレイキャストを行って、衝突したポリゴン番号をスタート位置ポリゴンとする


//敵情報の構造体のコンストラクタ
EnemyInfo::EnemyInfo():
	EnemyInfo(-1,-1,-1 , ENEMY_TYPE::ENEMY_TYPE_MAX)
{
}

//敵情報の構造体のコンストラクタ
EnemyInfo::EnemyInfo(int num , int hp , int AD, ENEMY_TYPE type):
	number(num),
	HP(hp),
	attackDamage(AD),
	thisType(type)
{
}
