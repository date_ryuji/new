#include "EffectEnemy.h"						//ヘッダ
#include "../../Engine/DirectX/Direct3D.h"		//Direct3Dにおけるシェーダークラス取得
#include "../../Engine/Model/Model.h"			//モデル
#include "../../Shader/ScrollShader.h"			//専用シェーダークラス（画像テクスチャ動的移動シェーダー）

//コンストラクタ
EffectEnemy::EffectEnemy(GameObject* parent)
	: GameObject(parent, "EffectEnemy"),
	hModel_(-1),
	scrollPower_(0.f)
{
}


//初期化
void EffectEnemy::Initialize()
{
	//ロード
	hModel_ = Model::Load("Assets/Scene/Model/GameScene/Enemy/TransparentEffectEnemy.fbx", POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_SCROLL);
	//警告
	assert(hModel_ != -1);

	//サイズの可変
	transform_.scale_ = XMVectorSet(3.0f, 3.0f, 3.0f, 0.f);
}

//更新
void EffectEnemy::Update()
{
	//移動値を加算する
	scrollPower_ += SPEED_;

	//条件：移動値が１．０ｆ(UVにおける最高値)を超えた場合
	if (scrollPower_ >= 1.f)
	{
		//0で初期化
		scrollPower_ = 0.f;
	}

}

//描画
void EffectEnemy::Draw()
{
	//専用シェーダーへ情報を渡す
	//コンスタントバッファ2つ目に代入
	ScrollShader* pScrollShader = (ScrollShader*)Direct3D::GetShaderClass(SHADER_TYPE::SHADER_SCROLL);
	
	//コンスタントバッファにセット
	ScrollShader::CONSTANT_BUFFER_1 cb1;
	cb1.scrollPower = scrollPower_;

	//セット
	pScrollShader->SetConstantBuffer1Data(&cb1);

	//描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);


	//エフェクトモデルの描画
	//エフェクトは、木のモデルに触れているときに描画を行うようにする
	//毎フレーム
	//自身の描画後に、フラグを下す
	{
		//自身の描画フラグを下す
		this->Invisible();
	}

}

//解放
void EffectEnemy::Release()
{
}