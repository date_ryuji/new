#pragma once
#include "Spawn.h"	//スポーンクラスの抽象クラス


//クラスのプロトタイプ宣言
class GameSceneManager;
class NavigationForEnemy;
class Enemy;
class EnemyCommandTower;
class CountTimer;

//構造体のプロトタイプ宣言
struct RayCastData;



/*
	構造体詳細	：敵情報構造体
	構造体概要（詳しく）
				：敵ステータス、敵へのアクセスの際に参照する情報群

*/
struct EnemyInfo
{
	//敵タイプ
		//詳細：敵ごとのステータスを分ける際に使用する
	enum class ENEMY_TYPE
	{
		ENEMY_NORMAL,	//標準
		
		ENEMY_TYPE_MAX,	//MAX
	}thisType;


	//敵番号
		//詳細：EnemySpawnにて更新
	int number;

	//HP
		//詳細：Enemyから取得
	int HP;
	//攻撃力
		//詳細：Enemyから取得	
	int attackDamage;

	//コンストラクタ
	EnemyInfo();
	//コンストラクタ
	EnemyInfo(int num, int hp, int AD , ENEMY_TYPE type);
};



/*
	クラス詳細：敵クラス生成、管理、削除を担うクラス
	クラスレベル：サブクラス（スーパークラス（Spawn））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：敵オブジェクトのあくまで、生成、管理、消去を行いたい。
				　生成のタイミングはほかのクラスにて行う
				　生成が呼ばれ、その生成インスタンスを保存しておく

*/
class EnemySpawn : public Spawn
{
//private メンバ変数、ポインタ、配列
private : 

	//シーンマネージャー
	GameSceneManager* pSceneManager_;
	//敵の司令塔
	EnemyCommandTower* pCommandTower_;


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	EnemySpawn(GameObject* parent);


	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~EnemySpawn() override;

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//スポーン管理のオブジェクト作成
		//詳細：引数のオブジェクトを親としてインスタンスの生成
		//　　：インスタンス生成とスポーンへの追加
		//レベル：オーバーライド
		//引数：親オブジェクト
		//戻値：生成されたインスタンスオブジェクト
	GameObject* AddNewInstance(GameObject* pParent) override;

	//木への攻撃実行
		//詳細：引数ポリゴン番号（TARGET_POLY_NUM）に立っている、植わっている木があるならば、
		//　　：その木の番号を受け取る
		//　　：その木の番号をもとに、木グループにアクセスして、該当木にダメージを与えるまで至る
		//引数：ターゲットとなる木がたっているポリゴン番号（敵のナビゲーションの最終目的ポリゴン番号）
		//引数：ターゲットへ与えるダメージ
		//引数：攻撃で木を倒したか（HPを０にしたか）
		//引数：倒した場合、その倒した木の番号
		//戻値：攻撃に成功したか（true : 攻撃成功。攻撃対象が存在する。 false:攻撃失敗。攻撃対象が存在しない）
	bool AttackSuccess(const int TARGET_POLY_NUM, const int DAMAGE, bool* isKill, int* treeNum);


	//すべての敵データとのレイキャストを行う
		//詳細：当たった敵のオブジェクトを返す
		//　　：その敵のデータをもとに情報だけを送るだけでも良い
		//　　：敵のステータスや、状態などを構造体で送ってでもよい
		//引数：なし
		//戻値：衝突した敵情報
	EnemyInfo RayCastToAllData();


	//自身の管理するすべての敵オブジェクトへ時間の停止を宣言
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void StopTimeAllObject() override;

	//自身の管理するすべての敵オブジェクトへ時間の計測再開を宣言
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void StartTimeAllObject() override;

	//緑ポリゴンか調べる（マネージャーシーンへ）
		//引数：引数にて受け取った、ポリゴン番号が、緑ポリゴンかどうか調べる
		//　　：その真偽値を返す
		//引数：ポリゴン番号
		//戻値：緑ポリゴンか
	bool IsGreenPolygon(int polyNum);


	
	//引数のEnemyオブジェクトと地面とのレイキャスト行い衝突判定を行う
		//詳細：衝突したポリゴン番号を求める
		//引数：敵オブジェクト
		//戻値：衝突したポリゴン番号（未衝突時：-1）
	int RayCastForEnemyAndGround(Enemy* pEnemy);


	//強制的に敵を一体生成させる
		//引数：なし
		//戻値：なし
	void ForciblyCreateEnemy();

	//引数オブジェクトが敵スポーンに存在するか
		//詳細：現在行動を起こそうとしているオブジェクトが存在するかを調べてもらう
		//　　：存在していたら、行動実行
		//　　：存在していなかったら、行動終了。とともに終了処理
		//引数：ターゲットオブジェクト
		//戻値：存在するか
	bool IsExistsTarget(GameObject* pTarget);

	//強制的に引数オブジェクトを削除させる
		//引数：ターゲットオブジェクト
		//戻値：なし
	void ForciblyEraseEnemy(GameObject* pTarget);

	//強制的に指定オブジェクトを捕縛させる
		//詳細：捕縛：行動不能、スタン
		//　　：捕縛が起こる条件：バインドの木より捕縛。イーター木より捕縛。
		//引数：捕縛対象のオブジェクト
		//戻値：なし
	void ForciblyBindEnemy(GameObject* pTarget);

	//捕縛解除
		//詳細：強制的に捕縛状態となるが、
		//　　：そのバインドが解除される場合がある。→ほかの敵にバインドの木が切り落とされる
		//　　：その場合、バインドを解除
		//引数：捕縛解除対象のオブジェクト
		//戻値：なし
	void ForciblyUnBindEnemy(GameObject* pTarget);


	//敵が存在するポリゴン番号をすべて知る
		//詳細：引数にて渡した、ベクターへ、敵が存在するポリゴン番号を代入してもらう
		//　　：ゲームマップを作る際に、敵の存在するポリゴン番号を取得する。その際に使用する
		//引数：敵の存在するポリゴン群
		//戻値：なし
	void GetPolygonsWithEnemies(std::vector<int>* polygonsWithEnemies);

	//司令塔へナビゲーションへアクセスするためのハンドル番号の更新を行ってもらう
		//詳細：スポーンに登録されている順番で、連番に、ハンドル番号を振りなおす
		//　　：下記の関数が呼ばれるタイミングは、敵オブジェクトが削除された後、削除されると、ナビゲーションにある、その削除敵オブジェクトの経路ルートも同様に削除される。
		//　　：となると、削除された分、スタックは、横にずれるため、アクセスするためのハンドル番号も更新せざるを得ない
		//　　：そのため、全敵対象にハンドル番号の振り直しを行う
		//引数：なし
		//戻値：なし
	void ReassigningMyNavigationHandle();

	//ナビゲーション先を指令塔へ伝える
		//詳細：敵が、木を倒しきる前に、消滅してしまった
		//　　：その際に、攻撃対象としていた木が忘れ去られてしまうため、
		//　　：再び、攻撃対象となるように、倒しきれなかった番号を渡し、司令塔へ余裕のある敵オブジェクトに割り振り
		//引数：ナビゲーション先のポリゴン番号
		//戻値：なし
	void StartNavigationForTellCommandTower(int targetPolyNum);



	//リストから解放かつ解放時の処理を追加
		//引数：解放オブジェクト
		//引数：敵番号
		//戻値：なし
	void RemoveMyObject(GameObject* pGameObject, int enemyNum);

	/*敵の情報看板との会話生成メソッド*************************************************************************************/
	
		/*
			//敵情報看板に、テキスト情報の更新
			//情報看板と表記しているが、あくまでも、敵同士の会話を表現するものであるため、会話のような口調にする
			//ゲームシーンでその形を作るのはおかしく感じるので、ほかのクラス、あるいは、掲示板クラスにて行うべき

			//敵個人から
			//木を倒したなどの行動がある場合、
			//スポーンを経由して→敵司令塔→敵情報看板へ移動して
			//必要情報を渡す

		*/
		
	//木を倒した
		//引数：敵番号（スポーンにおける敵へのハンドル番号）
		//引数：木番号
		//戻値：なし
	void AddTalkWhenTreeFall(int enemyNum , int treeNum);

	//敵が倒れた
		//詳細：スポーン内に、Release時に呼ばれる関数があるため、その関数の中で、テキスト情報の更新を行う
		//　　：この際、どのように倒れたのか、情報を持っておきたい
		//　　：自然消滅(黒ポリゴン、緑ポリゴン関係なく、バインド含む)
		//　　：イーター
		//引数：敵番号（スポーンにおける敵へのハンドル番号）
		//戻値：なし
	void AddTalkWhenReleaseEnemy(int enemyNum);
			
	//ナビゲーションを行っていたが対象の木が存在しなかった(目的地に木が存在しなかった)
		//引数：敵番号（スポーンにおける敵へのハンドル番号）
		//戻値：なし
	void AddTalkWhenNullTree(int enemyNum);

	//攻撃開始を宣言
		//詳細：攻撃対象と自身の番号を渡して、
		//　　：攻撃開始を知らせる
		//引数：敵番号（スポーンにおける敵へのハンドル番号）
		//引数：木番号
		//戻値：なし
	void AddTalkWhenStartAttack(int enemyNum, int treeNum);


	//敵のナビゲーションを知らせる
		//詳細：敵のナビゲーション先の木と敵の番号を知らせる
		//引数：敵番号（スポーンにおける敵へのハンドル番号）
		//引数：木番号
		//戻値：なし
	void AddTalkWhenStartNavigation(int enemyNum, int treeNum);


};

