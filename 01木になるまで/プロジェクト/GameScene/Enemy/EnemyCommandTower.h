#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス

//クラスのプロトタイプ宣言
class Enemy;
class EnemySpawn;
class GameSceneManager;
class CountTimer;
class NavigationForEnemy;
class EnemyStrategyBulletinBoard;


/*
	クラス詳細：敵クラス全体の司令塔
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：敵の行動を指示するクラス
				：敵の生成タイミングを担う
				：現在とれる、最優先の行動を各敵に知らせ、行動を命令する
	
				：ゲーム全体のバランスを整える、ゲームメタAIが存在する場合、
				　そのメタAIの影響を大きく受ける。→敵に不利になるように行動選択をしろ。（※直接司令塔に指示するよりも、外的要因。地面における受けるダメージ値の調整　などを行うほうが公平か？）
				　敵の司令塔となっているが、敵とプレイヤーの状態に応じて、互いに戦況が均衡するように行動、命令を指示するクラス
	

				：行動に対する指示を
				　EnemyStrategyBulletinBoardに知らせ、
				　行動を明示的に示せるようにする。（ゲーム中の敵の会話をプレイヤーが参照できるように）


				//ゲームの遊び要素
				//案（余裕があったら）
				//この際に生じる、
				//敵への行動指示。その行動があまりにも、遠いものだったり、敵に不利になるものだったりすると、EnemyStrategyBulletinBoardに、特殊な会話を描画してもらう
				//「あまりにも、不利な行動」
				//「外部からの圧力を感じる。」　など、メタ的な会話をできるとよい。


*/
class EnemyCommandTower : public GameObject
{
//private メンバ定数
private:
	//生成間隔
	//生成間隔をカウントアップの終了時間として、終了時間以上の計測を行ったら、
	//初期化を行う
	static constexpr float TIME_CREATING = 20.0f;

//private メンバ変数、ポインタ、配列
private : 

	//時間停止のフラグ
		//詳細：時間による行動、移動、攻撃、HP現象などなど様々な動的要因を停止する
	bool isStopTime_;

	//敵スポーン
		//詳細：敵生成時、敵への指示時、など敵の情報を受け取る際に、現在の敵を所有している下記のクラスにアクセスし、
		//　　：各、生成指示。敵のインスタンス取得を行う
	EnemySpawn* pEnemySpawn_;

	//シーンマネージャークラス
	GameSceneManager* pSceneManager_;

	//敵生成間隔を管理するカウントタイマー
	CountTimer* pCountTimer_;

	//敵オブジェクトのナビゲーター(ナビゲーションAI)
	NavigationForEnemy* pNavigationForEnemy_;


	//敵の戦略掲示板
	EnemyStrategyBulletinBoard* pEnemyBulletinBoard_;


	//移動先のポリゴン番号軍
		//詳細：次に敵一人に割り当てるポリゴン番号
		//　　：ナビゲーション先のポリゴン番号
	std::list<int> listNextTargetPolyNum_;



//private メソッド
private : 

	//スポーンから指定敵を取得
		//引数：スポーン内の敵へのハンドル
		//戻値：敵オブジェクト
	Enemy* GetEnemy(int suffix);
	//スポーンにて扱う敵オブジェクト合計数を取得
		//引数：なし
		//戻値：敵オブジェクト合計数
	int GetObjectCount();

	//敵オブジェクトの時間計測を更新する
		//詳細：敵1体1体のデルタタイム更新		
		//引数：なし
		//戻値：なし
	void UpdateDeltaTime();

	//タイマーの開始(初期)
		//引数：なし
		//戻値：なし
	void StartCountTimer();
	//タイマーの再計測開始
		//引数：なし
		//戻値：なし
	void ReStartCountTimer();
	//タイマーの停止
		//引数：なし
		//戻値：なし
	void NotPermittedTimer();
	//タイマーの再開
		//詳細：再計測：カウントアップを初期化、０からカウントアップ
		//　　：再開：停止していたカウントアップを、再開し、状態を保ったまま、カウントアップ
		//引数：なし
		//戻値：なし
	void PermitTimer();

	//敵が出現判定
		//詳細：一定間隔で敵の出現判定 
		//引数：なし
		//戻値：なし
	void JudgementSpawnEnemy();

	//敵オブジェクトの初期化処理
		//引数：初期化する敵オブジェクト
		//戻値：なし
	void InitEnemyPointer(Enemy* pEnemy);

	//仕事を持たないオブジェクトを探す
		//引数：なし
		//戻値：なし
	void FindEnemyNotHaveAJob();


	//仕事を持たないオブジェクトに仕事を割り当てる
		//引数：仕事を持たないオブジェクト群
		//戻値：なし
	void JobAssignment(std::list<Enemy*>* noneWork);


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	EnemyCommandTower(GameObject* parent);

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;


//public メソッド
public:

	//EnemySpawnのセット
		//引数：敵スポーン
		//戻値：なし
	void SetEnemySpawn(EnemySpawn* pEnemySpawn);

	//ナビゲーション命令
		//詳細：該当のオブジェクトを、目的ピクセル位置へ移動させる
		//　　：ナビゲーションの開始位置は、現在敵オブジェクトが存在するポリゴン番号
		//　　：＝　現在敵オブジェクトの座標から、下へレイキャストを行って、衝突したポリゴン番号をスタート位置ポリゴンとする
		//　　：関数呼び出し時には、目的位置のポリゴンだけをもらい
		//　　：対象のオブジェクトは、敵スポーン内で、適切なオブジェクトを見つけ出して、そのオブジェクトに向かうように指示する
		//引数：なし
		//戻値：なし
	void StartNavigation(int goalPolyNum);


	//司令塔へナビゲーションへアクセスするためのハンドル番号の更新を行ってもらう
		//詳細：スポーンに登録されている順番で、連番に、ハンドル番号を振りなおす
		//　　：下記の関数が呼ばれるタイミングは、敵オブジェクトが削除された後、削除されると、ナビゲーションにある、その削除敵オブジェクトの経路ルートも同様に削除される。
		//　　：となると、削除された分、スタックは、横にずれるため、アクセスするためのハンドル番号も更新せざるを得ない
		//　　：そのため、全敵対象にハンドル番号の振り直しを行う
		//引数：なし
		//戻値：なし
	void ReassigningMyNavigationHandle();

	//時間が停止しているか
		//引数：なし
		//戻値：停止しているか
	bool IsStopTime();

	//自身のタイマーの停止
		//詳細：スポーン管理のオブジェクトは、
		//　　：スポーンに停止を宣言してもらう
		//　　：時間の停止（行動、移動、攻撃などの停止）
		//引数：なし
		//戻値：なし
	void StopTime();
	//自身のタイマーの再開
		//詳細：時間の再開（行動、移動、攻撃などの再開）
		//引数：なし
		//戻値：なし
	void StartTime();

	//強制的に敵を一体生成させる
		//引数：なし
		//戻値：なし
	void ForciblyCreateEnemy();


	/*敵の情報看板との会話生成メソッド*************************************************************************************/
		/*
			//敵オブジェクト、あるいは、自身クラスからテキストとして描画したい会話を作成
			//それを看板クラスへ渡し、テキストとして描画してもらう
		*/

	//木を倒した
		//引数：敵番号（スポーンにおける敵へのハンドル番号）
		//引数：木番号
		//戻値：なし
	void AddTalkWhenTreeFall(int enemyNum, int treeNum);

	//敵が倒れた
		//引数：敵番号（スポーンにおける敵へのハンドル番号）
		//戻値：なし
	void AddTalkWhenReleaseEnemy(int enemyNum);

	//ナビゲーションを行っていたが対象の木が存在しなかった(目的地に木が存在しなかった)
		//引数：敵番号（スポーンにおける敵へのハンドル番号）
		//戻値：なし
	void AddTalkWhenNullTree(int enemyNum);

	//攻撃開始を宣言
		//詳細：攻撃対象と自身の番号を渡して、
		//　　：攻撃開始を知らせる
		//引数：敵番号（スポーンにおける敵へのハンドル番号）
		//引数：木番号
		//戻値：なし
	void AddTalkWhenStartAttack(int enemyNum, int treeNum);

	//敵の生成を知らせる
		//引数：なし
		//戻値：なし
	void AddTalkWhenSpawnEnemy();

	//敵のナビゲーションを知らせる
		//詳細：敵のナビゲーション先の木と敵の番号を知らせる
		//引数：敵番号（スポーンにおける敵へのハンドル番号）
		//引数：木番号
		//戻値：なし
	void AddTalkWhenStartNavigation(int enemyNum , int treeNum);




};

