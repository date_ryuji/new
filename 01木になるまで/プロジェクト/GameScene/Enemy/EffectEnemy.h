#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス



/*
	クラス詳細：敵エフェクトクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene->EnemySpawn->Enemy
	クラス概要（詳しく）
				：敵のエフェクト用のモデルを所有するクラス
				：敵エフェクト実現のために、モデルのテクスチャを動的に移動させるシェーダー表現を実現させる

*/
class EffectEnemy : public GameObject
{
//private メンバ定数
private : 

	//シェーダーのUV移動スピード
	static constexpr float SPEED_ = 0.01f;

//private メンバ変数、ポインタ、配列
private : 
	//モデルハンドル
	int hModel_;

	//シェーダーのUV移動値
	float scrollPower_;

//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	EffectEnemy(GameObject* parent);

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

};