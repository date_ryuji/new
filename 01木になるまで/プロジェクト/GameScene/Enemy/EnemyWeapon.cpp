#include "EnemyWeapon.h"					//ヘッダ
#include "../../Engine/DirectX/Direct3D.h"	//Direct3Dにおけるシェーダークラス取得
#include "../../Engine/Model/Model.h"		//モデル
#include "../../Shader/ScrollShader.h"		//専用シェーダークラス（画像テクスチャ動的移動シェーダー）


//コンストラクタ
EnemyWeapon::EnemyWeapon(GameObject* parent)
	: GameObject(parent, "EnemyWeapon"),
	hModel_(-1),
	INTERVAL_(0.f),
	scrollPower_(0.f),
	elpasedTime_(0.f),
	totalElpasedTime_(0.f)
{

}

//初期化
void EnemyWeapon::Initialize()
{
	/*
	//テクスチャを移動させる
	//	移動のテクスチャと、ディフューズの色とが重なって、ディフューズのテクスチャの上を、テクスチャが移動しているようなかっこいいエフェクトができる
		//これは、予想をしていない実装なので、
		//なぜか起きてしまったことなので、
		// 本来任意に起こすのであれば、
		// ディフューズの色を取得
		// それとは別に、テクスチャを読み込む（Fbxとは関係なしに）、→そのテクスチャをシェーダーに渡して、ディフューズと、テクスチャを重ねる。（そのテクスチャを移動させる。）色の合成。
		// ディフューズと、テクスチャを重ねることが可能になる。
	//hModel_ = Model::Load("Assets/Scene/Model/GameScene/AxeWithEffect.fbx", FBX_POLYGON_GROUP, SHADER_SCROLL);
	*/

	//通常のシェーダーにて実装
		//しかし、Fbxの中に、透過の処理の画像で、モデルとは別でテクスチャが張られたモデルがある。
		//そのテクスチャを動的に移動させて、エフェクトのようにしたい。
		//現在のFbxのエンジンだと、テクスチャ部分と、ディフューズ部分が存在するモデルは、複数を満たす処理にでき	ない
		//→×　テクスチャ部分とディフューズ部分が存在するモデルでもそれぞれ実装できる
			//ディユーズの上で、別途スクロールするテクスチャを実装するならば、上記で書かれているように、テクスチャを別に用意して、そのテクスチャを移動するようにシェーダーで処理を書けばよい。
			//自身のFbxに存在するテクスチャを使用して、自身のディフューズに重ねるというやり方は、現段階ではできない。→なぜならば、テクスチャが存在しないときに、自身のディフューズにテクスチャを重ねるという処理ができないから。
			//テクスチャが存在しないのに、持ってくることは不可能。（本来は不可能だが、なぜか、前回に描画したテクスチャが使用されることがある。（つまり、ひとつ前にテクスチャ付きの木を描画したら、その木のテクスチャが使用されることがある））
	hModel_ = Model::Load("Assets/Scene/Model/GameScene/Enemy/AxeWithEffect.fbx", POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_SCROLL);
	//警告
	assert(hModel_ != -1);



	//座標変換
	transform_.position_ = XMVectorSet(-1.f, 0.f, -0.5f, 0.f);

	//サイズ変換
	transform_.scale_ = XMVectorSet(0.5f , 0.5f, 0.5f, 0.f);
}

//更新
void EnemyWeapon::Update()
{

	//移動値を加算する
	scrollPower_ += SPEED_;

	//条件：移動値が１．０ｆ(UVにおける最高値)を超えた場合
	if (scrollPower_ >= 1.f)
	{
		//0で初期化
		scrollPower_ = 0.f;
	}

	//経過時間に応じて回転
	RotateWeapon();

}

//描画
void EnemyWeapon::Draw()
{
	//専用シェーダーへ情報を渡す
	//コンスタントバッファ2つ目に代入
	ScrollShader* pScrollShader = (ScrollShader*)Direct3D::GetShaderClass(SHADER_TYPE::SHADER_SCROLL);

	//コンスタントバッファにセット
	ScrollShader::CONSTANT_BUFFER_1 cb1;
	cb1.scrollPower = scrollPower_;

	//セット
	pScrollShader->SetConstantBuffer1Data(&cb1);


	//描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);


}

//解放
void EnemyWeapon::Release()
{
}

//経過時間計測
void EnemyWeapon::UpdateElpasedTime(float elpasedTime)
{
	//フレームの経過時間を計測
	elpasedTime_ = elpasedTime;
	//合計経過時間を計測
	totalElpasedTime_ += elpasedTime;
}
//インターバル更新
void EnemyWeapon::SetInterval(const float INTERVAL)
{
	INTERVAL_ = INTERVAL;
}
//武器回転
void EnemyWeapon::RotateWeapon()
{
	//フレームの経過時間と、合計回転経過時間を別々に計測した場合
	//		//下記により、回転に掛ける時間の半分を使用し、行き。もう半分を使用して、戻り。を表現する
	//回転方向を求める


	//回転値　Z軸回転
	static constexpr float ROTATE_Z = 90.f;
	//回転値　Y軸回転
	static constexpr float ROTATE_Y = -90.f;



	//条件：合計経過時間が　インターバル以上ならば
	if (INTERVAL_ <= totalElpasedTime_)
	{
		//回転値を０にして終了

		//elpasedTime_ = 0.f;
		//合計経過時間の初期化
		totalElpasedTime_ = 0.f;


		//回転値を初期値にする
		transform_.rotate_.vecY = 0.f;
		transform_.rotate_.vecZ = 0.f;
		return;
	}

	//合計経過時間が
		//間隔の半分以下ならば　＋
		//間隔の半分以上ならば　ー

	//float code = 1.0f;

	//計算用　経過時間
	float calcElpased = elpasedTime_;
	//条件：合計経過時間が回転間隔の半分の時間経過していたら
	if (INTERVAL_ / 2.0f <= totalElpasedTime_)
	{

		//半分経過しているため、
			//回転方向を逆にするため、
			//値を―にする
		calcElpased *= -1.f;
	}



	{
		//フレームの経過時間によって、このフレームにおける回転値を求めて
			//回転させる

		//回転スピード
		const float SPEED_Z = ROTATE_Z / (INTERVAL_ / 2.0f);

		//Z
			//計算方法は、　スピードを求める（道のり、どのぐらいの時間で道のりを超えるか）
			//スピード　＊　経過時間
		float rotateZ = SPEED_Z * calcElpased;


		//回転量分、回転する
			//フレームにおける回転量なので、加算する
		transform_.rotate_.vecZ += rotateZ;
	}

	//以上をXも行う
	{
		//回転スピード
		const float SPEED_Y = ROTATE_Y / (INTERVAL_ / 2.0f);
		//Y
			//スピード　＊　経過時間
		float rotateY = SPEED_Y * calcElpased;


		//回転量分、回転する
		transform_.rotate_.vecY += rotateY;
	}


	//経過時間を初期化
	elpasedTime_ = 0.f;


	//フレームの経過時間と、合計回転経過時間を同じ変数に計測した場合
	/*
	//回転方向を求める

	//経過時間が　インターバル以上ならば
		//回転値を０にして終了
	if (INTERVAL_ <= totalElpasedTime_)
	{
		elpasedTime_ = 0.f;


		//回転値を初期値にする
		transform_.rotate_.vecY = 0.f;
		transform_.rotate_.vecZ = 0.f;
		return;
	}

	//経過時間が
		//間隔の半分以下ならば　＋
		//間隔の半分以上ならば　ー

	float code = 1.0f;

	//計算用　経過時間
	float calcElpased = elpasedTime_;

	if (INTERVAL_ / 2.0f <= elpasedTime_)
	{
		code = -1.0f;
		//間隔の半分以上経過しているので、
			//計算のために、経過時間を、間隔の半分引く
			//つまり経過時間半分からの経過時間となる
		calcElpased -= INTERVAL_ / 2.0f;
	}



	{
		//経過時間によって、回転値を求めて
			//回転させる

		//スピード
		const float SPEED_Z = ROTATE_Z / (INTERVAL_ / 2.0f);

		//Z
			//計算方法は、　スピードを求める（道のり、どのぐらいの時間で道のりを超えるか）
			//スピード　＊　経過時間
		float rotateZ = SPEED_Z * calcElpased * code;


		//適用
		transform_.rotate_.vecZ = rotateZ;
	}

	//以上をXも行う
	{
		//スピード
		const float SPEED_Y = ROTATE_Y / (INTERVAL_ / 2.0f);
		//Z
			//計算方法は、　スピードを求める（道のり、どのぐらいの時間で道のりを超えるか）
			//スピード　＊　経過時間
		float rotateY = SPEED_Y * calcElpased * code;


		//適用
		transform_.rotate_.vecY = rotateY;
	}
	*/

}
