#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス


//クラスのプロトタイプ宣言
class EffectEnemy;
class EnemySpawn;
class EnemyWeapon;
class NavigationForEnemy;
class NumberPlate;
class CountTimer;
	/*
		CountTImer使用目的
		//カウントタイマークラス
		//消去モーション用
		//消去モーション実行時は、時間計測を行っているスポーンクラスから、自身を解除する。
		//つまり、時間は、自身で管理する必要がある。
		//であれば、自身でタイマーを持つ。
		//インスタンスを持つのは、自身が削除されると判断されたとき、消去モーションに入る時なので、それまでの時間計測はスポーンから受ける
	
	*/

//構造体のプロトタイプ宣言
struct EnemyInfo;


/*
	クラス詳細：敵クラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene->EnemySpawn
	クラス概要（詳しく）
				：EnemySpawnより出現され、特定の位置への移動を指示され、
				　特定の敵のオブジェクトへの攻撃を行うクラス


*/
class Enemy : public GameObject
{
//private メンバ定数
private : 

	//攻撃間隔（ｓ）
	static constexpr float ATTACK_INTERVAL_ = 2.0f;
	//被ダメージ間隔（ｓ）
	//被ダメージの間隔で自身がダメージを受ける
		//通常ＨＰ：１００
		//1回の通常ダメージ：１
		//1ステージの制限時間：　120 s
		//上記を踏まえたうえで、敵は通常●ｓ居てほしいか

		//２０ｓ
		//1sで5回ダメージを受ける　＝　0.2
	//�@２０ｓ生きる
	//static constexpr float TAKEN_DAMAGE_INTERVAL_ = 0.2f;

	//�A４０ｓ生きる
	static constexpr float TAKEN_DAMAGE_INTERVAL_ = 0.4f;


	//補間の移動量、スピード
		//割合を進めるスピード
	//static constexpr float SPEED_ = 0.001f;
	static constexpr float SPEED_ = 0.02f;



//private メンバ変数、メソッド、配列
private : 
	//時間停止のフラグ
		//時間による行動、移動、攻撃、HP現象などなど様々な動的要因を停止する
	bool isStopTime_;
	//消去モーション実行
	bool isEraseMotion_;
	//バインド中フラグ
		//バインドを受けて、行動不能を示すフラグ
		//どの種類の木でも、木からの行動不能を指示された場合、問答無用で行動不能とする
	bool isBind_;
	//攻撃対象への攻撃中のフラグ
	bool isAttack_;
	//ナビゲーター（ナビゲーションAI）によってナビ方向が指定された
	bool isNavigate_;
		/*
			フラグ類

			//ナビゲーションを含めて、フラグが多くなってしまうため、
			//フラグを少なくする必要がある。
			//フラグではなく、必要なタイミングで呼び込む形にするなど。スポーンや、司令塔がいるため、そのクラスに行動を一任するなど様々な方法がある。
			//だが、自身の敵ごとにフラグを持たせて、共通のナビゲーションクラスを使っている現在の管理の方法が一番分かりやすい。
			//上記に、さらに、攻撃できるかのフラグも出てくるとさらに増えてしまう

			//各フラグ類は、構造体でまとめたい
			//そのフラグの真偽を確認する中で、
			//複数の真偽判定が必要なものは、一つの関数でその真偽を問うようにする

		*/


	//自身のモデルハンドル
	int hModel_;
	//ナビゲーションにおける自身のナビゲーションルートへアクセスするためのハンドル
	int hMyNavigation_;
	//攻撃対象となる木がたつポリゴン番号
	int targetPolyNum_;

	//消去モーションのスピード
	float eraseSpeed_;

	//未攻撃　経過時間加算変数（ｓ）
		//前回の攻撃から経過した時間
	float elpasedTimeForBeforeAttack_;
	//未被ダメージ　経過時間加算変数（ｓ）
		//前回の被ダメージから経過した時間
	float elpasedTimeForRecivedDamage_;
	//浮遊経過時間加算変数（ｓ）
	float elpasedTimeForFloating_;

	//移動の補間割合
	//開始位置ベクトルを０、目的位置ベクトルを１と置いたとき
	//０〜１へ線形補間をして、現在が、その、どの位置にいるのか、その補間の割合
	float interpolationRate;

	//最期にナビゲーションで更新された際のY座標
		//浮遊モーションの際に、上昇、下降位置を確定する際に使用する
	float lastUpdatePosY_;



	//自身のスポーン
	EnemySpawn* pMySpawn_;
	//自身のエフェクト
	EffectEnemy* pEffect_;
	//自身のナビゲーションAI
	NavigationForEnemy* pMyNavigator_;

	//自身の武器
	EnemyWeapon* pEnemyWeapon_;
	//自身の番号プレート
	NumberPlate* pNumberPlate_;

	//自身のステータス情報
	EnemyInfo* pEnemyInfo_;

	//カウントタイマークラス
	CountTimer* pCountTimer_;


	//自身の移動開始ベクトル
	XMVECTOR startingPos_;
	//自身の次の目的位置ベクトル
	XMVECTOR nextDestinationPos_;

	//最終目的地のベクトル
		//オブジェクト回転方向を求める際に使う
	XMVECTOR goalPos_;




//private メソッド
private : 

	//経路探索　移動
		//引数：なし
		//戻値：なし
	void MyNavigation();
	//時間が停止しているか
		//引数：なし
		//戻値：なし
	bool IsStopTime();
	//自身へのダメージ計算
		//引数：なし
		//戻値：なし
	void ReceiveDamage();
	//自身へのダメージを受けとる時間の初期化
		//引数：なし
		//戻値：なし
	void InitRecivedDamageTime();
	//自身の死亡判定
		//引数：なし
		//戻値：なし
	void JudgeExeEraseMotion();
	//自身へダメージを受ける
		//引数：引数ダメージを自身HPから削る
		//戻値：なし
	void ReciveDamage(const int DAMAGE);

	//攻撃できるか
		//詳細：自身の攻撃間隔分時間が経過しているかの判断
		//引数：なし
		//戻値：攻撃できる
	bool IsPossibleToAttack();
	//攻撃を受けるか
		//詳細：自身のダメージを受ける間隔分時間が経過しているかの判断
		//引数：なし
		//戻値：攻撃を受けるか
	bool IsPossibleToRecivedDamage();
	//浮遊モーション実行
	void Floating();
	//浮遊モーション（上昇）
		//引数：経過時間
		//引数：スピード
		//戻値：なし
	void Rise(const float ELPASED_TIME , const float SPEED);
	//浮遊モーション（下降）
		//引数：経過時間
		//引数：スピード
		//引数：浮遊時間
		//戻値：なし
	void Descent(const float ELPASED_TIME, const float SPEED , const float FLOATING_TIME);

	//自身の回転（移動方向へ）
		//引数：なし
		//戻値：なし
	void RotateMyBody();

	//消去モーション開始
		//引数：なし
		//戻値：なし
	void StartEraseMotion();
	//消去モーションを開始ししているか
		//引数：なし
		//戻値：開始しているか
	bool IsEraseMotion();
	//捕縛されているか
		//引数：なし
		//戻値：捕縛されているか
	bool IsBind();
	//自身が現在移動中であるかどうか
		//引数：なし
		//戻値：移動中であるか
	bool IsMoveing() { return isNavigate_; };
	//自身が攻撃中であるか
		//引数：なし
		//戻値：攻撃中であるか
	bool IsAttack() { return isAttack_; };
	//攻撃可能フラグを立てる
		//引数：なし
		//戻値：なし
	void StartAttack();
	//攻撃可能フラグを下ろす
		//引数：なし
		//戻値：なし
	void StopAttack();
	
	//攻撃を与える
	//引数：なし
	//戻値：なし
	void CauseDamage();
	//攻撃を与える
		//引数：与えるダメージ値
		//引数：攻撃を与える木の番号（ポインタ）
		//戻値：なし
	void CauseDamage(int damage, int* treeNum);


	//自身が消去対象か
		//引数：なし
		//戻値：なし
	void JudgeKillMe();

	//補完割合の初期化
		//引数：なし
		//戻値：なし
	void InitVariableForLerp();
	//ナビゲーションにおける線形補完を行う
		//引数：なし
		//戻値：なし
	void ExecuteLinearInterpolation();

	//攻撃タイムの初期化
		//引数：なし
		//戻値：なし
	void InitAttackTime();


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	Enemy(GameObject* parent);

	//初期化
				//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
				//レベル：オーバーライド
				//引数：なし
				//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;


	//コライダー衝突時に呼ばれる関数
		//詳細：コライダー同士の３D衝突判定にて、衝突した場合、衝突の基準コライダー（コライダー１と衝突するコライダーを探していた場合、コライダー１が基準のコライダー）を持つGameObjectの以下関数を呼び込む
		//レベル：オーバーライド
		//引数：自身のコライダー衝突した相手のポインタ
		//戻値：なし
	void OnCollision(GameObject* pTarget) override;

//public メソッド
public : 

	//自身をスポーンしたポインタをセット
		//引数：スポーンオブジェクトポインタ
		//戻値：なし
	void SetMySpawn(EnemySpawn* pMySpawn);
	
	//自身をナビゲーションするポインタをセット
		//引数：ナビゲーションの経路を取得するための自身のハンドル番号
		//引数：ナビゲーションポインタ
		//戻値：なし
	void SetMyNavigator(int hMyNavigation , NavigationForEnemy* pMyNavigator);

	//自身のナンバープレートをロード
		//引数：ナンバープレートの番号
		//戻値：なし
	void LoadNumberPlate(int handle);

	//時間の停止（行動、移動、攻撃などの停止）
		//引数：なし
		//戻値：なし
	void StopTime();
	//時間の再開（行動、移動、攻撃などの再開）
		//引数：なし
		//戻値：なし
	void StartTime();

	//デルタタイムの更新
		//詳細：フレームの経過時間を取得し、時間計測を行う。あくまでも自身の中でタイマーを持つのではなく、外部のスポーンなどからデルタタイムを取得して、時間を計測		
		//引数：経過時間
		//戻値：なし
	void UpdateDeltaTime(float elpasedTime);

	//スポーンに、自身のインスタンスを追加　呼び込み
		//引数：なし
		//戻値：なし
	void SpawnAddMyData();
	//スポーンに、自身のインスタンスを解放　呼び込み
		//引数：なし
		//戻値：なし
	void SpawnRemoveMyData();

	//ポリゴン番号から目的位置までの経路探索開始
		//引数：ナビゲーション開始位置
		//引数：ナビゲーション終了位置
		//戻値：なし
	void StartNavigation(int startNum ,  int targetNum);

	//自身のコライダーの所有しているデバック描画用のワイヤーフレームのモデル番号を与える
		//詳細：与えたモデル番号から、モデルにアクセスし、モデルとレイとの当たり判定を行う
		//　　：当たり判定とレイキャストを行うようにし、レイとの計算を行うポリゴン数を極力少なくする
		//引数：なし
		//戻値：なし
	int GetMyColliderModelHandle();

	//自身のコライダーの所有しているデバック描画用のワイヤーフレームのモデル
		//詳細：描画時のTransformにセットさせる
		//引数：なし
		//戻値：なし
	void SetMyColliderTransformWhenDrawing();

	//必要情報を（Enemy自身の情報）引数にて渡された参照私の構造体に格納する
		//引数：セットする構造体
		//戻値：なし
	void SetEnemyInfomation(EnemyInfo* pToEnemyInfo);


	//仕事がないかの確認
		//詳細：ナビゲーション先がない
		//　　：かつ
		//　　：攻撃中でない
		//引数：なし
		//戻値：仕事が欲しいか（ほしい、ない：true , ある：false）
	bool WantJob();


	//強制的に自身オブジェクトを削除
		//引数：なし
		//戻値：なし
	void ForciblyErase();

	//敵の捕縛
		//詳細：自身のナビゲーション、攻撃を停止する
		//　　：時間停止とは別。時間停止は、敵自身の行動全体を止めるモノ、だが、バインドは、敵の行動の一部を停止するもののため、共通して扱うことはできない
		//引数：なし
		//戻値：なし
	void BindEnemy();
	//敵の捕縛の解除
		//引数：なし
		//戻値：なし
	void UnBindEnemy();

	//自身が立っているポリゴンを渡す
		//引数：なし
		//戻値：自身がったているポリゴン
	int GetPolygonIStandingOn();

	//ナビゲーションにアクセスするためのハンドルを更新
		//引数：ナビゲーションハンドル
		//戻値：なし
	void UpdateMyNavigationHandle(int handle);



};