#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス


/*
	クラス詳細：敵武器クラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene->EnemySpawn->Enemy
	クラス概要（詳しく）
				：敵の武器用のモデルを所有するクラス
				：敵武器のエフェクト実現のために、モデルのテクスチャを動的に移動させるシェーダー表現を実現させる

*/
class EnemyWeapon : public GameObject
{
//private メンバ定数
private : 

	//UV移動スピード
	static constexpr float SPEED_ = 0.01f;

//private メンバ変数、ポインタ、配列
private : 
	//モデルハンドル
	int hModel_;

	//シェーダーのUV移動値
	float scrollPower_;
	//武器の回転経過時間
		//詳細：親である、敵から経過時間を取得し
		//　　：その時間に応じて、武器を一定時間で回転
		//　　：フレームにおける経過時間
	float elpasedTime_;
	//合計回転経過時間
		//詳細：回転に所要している時間
	float totalElpasedTime_;

	//敵が攻撃を行う間隔
		//詳細：回転方法は、
		//　　：その半分で、Z回転と、Y回転を行う
		//　　：半分経過後、元の座標へ戻す
		//　　：目標となる間隔
	float INTERVAL_;

//private メソッド
private : 

	//経過時間に応じて回転を行う
		//引数：なし
		//戻値：なし
	void RotateWeapon();

//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	EnemyWeapon(GameObject* parent);

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//時間経過を計測
		//引数：経過時間
		//戻値：なし
	void UpdateElpasedTime(float elpasedTime);

	//攻撃モーション開始
		//詳細：経過時間を計測して　応じて、回転を行う
		//　　：間隔取得
	void SetInterval(const float INTERVAL);



};