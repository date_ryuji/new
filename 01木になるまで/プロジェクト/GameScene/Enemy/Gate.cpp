#include "Gate.h"							//ヘッダ
#include "../../Engine/DirectX/Direct3D.h"	//Direct3D　シェーダークラス取得のため
#include "../../Engine/Model/Model.h"		//モデル
#include "../../Shader/GateShader.h"		//専用シェーダー（ゲート専用　UVテクスチャ移動シェーダー）


//コンストラクタ
Gate::Gate(GameObject * parent)
	: GameObject(parent, "Gate"),
	hModel_(-1),
	moveUVPower_(MIN_MOVE_UV_POWER_),
	type_(GATE_UV_MOVE_TYPE::GATE_UV_MOVE_DOWN)
{

}

//初期化
void Gate::Initialize()
{
	//ロード
	hModel_ = Model::Load("Assets/Scene/Model/GameScene/Gate/Gate.fbx", POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_GATE);
	//警告
	assert(hModel_ != -1);
}

//更新
void Gate::Update()
{
	//UPの時は、 1, DOWNのときは、　-1になるように
	//Gate＿UP = 0
	//Gate＿DOWN = 2
	//なので、 1 - Gate_UP = 1 , 1 - Gate_DOWN = -1
	moveUVPower_ = moveUVPower_ + (SPEED_ * (1 - (int)type_));

	//条件：UV移動値　が　UV移動値のMAX値よりも大きい場合
	if (moveUVPower_ > MAX_MOVE_UV_POWER_)
	{
		type_ = GATE_UV_MOVE_TYPE::GATE_UV_MOVE_DOWN;
	}
	//条件：UV移動値　が　UV移動値のMIN値よりも小さい場合
	if (moveUVPower_ < MIN_MOVE_UV_POWER_)
	{
		type_ = GATE_UV_MOVE_TYPE::GATE_UV_MOVE_UP;
	}
}

//描画
void Gate::Draw()
{
	//コンスタントバッファ2つ目に代入
	GateShader* pGateShader = (GateShader*)Direct3D::GetShaderClass(SHADER_TYPE::SHADER_GATE);

	//コンスタントバッファにセット
	GateShader::CONSTANT_BUFFER_1 cb1;
	cb1.moveUVPower = moveUVPower_;

	//セット
	pGateShader->SetConstantBuffer1Data(&cb1);

	//描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//解放
void Gate::Release()
{
}