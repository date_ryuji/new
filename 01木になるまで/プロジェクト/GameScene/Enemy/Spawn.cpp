#include "Spawn.h"	//ヘッダ


//コンストラクタ
Spawn::Spawn(GameObject * parent):
	Spawn(parent , "Spawn")
{
}
//コンストラクタ
	//継承先より呼ばれるコンストラクタ
Spawn::Spawn(GameObject * parent, const std::string name):
	GameObject(parent , name),
	isStopTime_(false)
{
	//可変配列のクリア
	objects_.clear();
}

//デストラクタ
Spawn::~Spawn()
{
}

//初期化
void Spawn::Initialize()
{
}

//更新
void Spawn::Update()
{
}

//描画
void Spawn::Draw()
{
}

//解放
void Spawn::Release()
{
}

//オブジェクトを自身管理のスポーンに追加
void Spawn::AddObject(GameObject * pGameObject)
{
	objects_.push_back(pGameObject);
}

//インスタンスの追加
	//レベル：オーバーライド
GameObject* Spawn::AddNewInstance(GameObject* pParent)
{
	return nullptr;
}
//時間が停止しているか
bool Spawn::IsStopTime()
{
	return isStopTime_;
}
//時間停止宣言
void Spawn::StopTime()
{
	isStopTime_ = true;
}
//時間計測宣言
void Spawn::StartTime()
{
	isStopTime_ = false;
}
//引数オブジェクトをスポーン管理オブジェクトから探す
bool Spawn::FindMyObject(GameObject* pTarget)
{
	//条件：イテレータの最後の要素ではない
	for (auto itr = objects_.begin(); itr != objects_.end(); itr++)
	{
		//条件：ポインタのアドレスが同じならば　
		if (pTarget == (*itr))
		{
			//見つかった
			return true;
		}
	
	}
	//見つからなかった
	return false;
}

//引数オブジェクトを解放
void Spawn::RemoveObject(GameObject * pGameObject)
{
	//条件：イテレータの最後の要素ではない
	for (auto itr = objects_.begin(); itr != objects_.end(); itr++)
	{
		//条件：イテレータと引数オブジェクトが同じ
		if ((*itr) == pGameObject)
		{
			//リストから解放
				//あくまで、ポインタをリストから消すだけ
				//Deleteはしない

				//なぜならば、保管しているオブジェクトのポインタは、
				//誰かがInstantiateしたものなので、
				//解放をしてしまったら、
				//誰かが、参照しているにもかかわらず、解放を行ってしまう
			objects_.erase(itr);
			//関数終了
			return;

		}
	}
}

//スポーン管理のオブジェクト全員へ時間停止を宣言
	//レベル：オーバーライド
void Spawn::StopTimeAllObject()
{
}
//スポーン管理のオブジェクト全員へ時間計測再開を宣言
	//レベル：オーバーライド
void Spawn::StartTimeAllObject()
{
}

//オブジェクト数取得
int Spawn::GetObjectCount()
{
	return (int)objects_.size();
}

//オブジェクトの座標変換
bool Spawn::SetPosition(GameObject* pGameObject, XMVECTOR setPosition)
{
	for (auto itr = objects_.begin(); itr != objects_.end(); itr++)
	{
		if ((*itr) == pGameObject)
		{
			//引数座標へセット
			(*itr)->transform_.position_ = setPosition;
			return true;
		}
	}
	return false;
}
//オブジェクトの座標変換
bool Spawn::SetPosition(const int suffix, XMVECTOR setPosition)
{
	//初期要素の取得
	auto itr = objects_.begin();

	//カウンター
	int i = 0;

	//引数suffix分イテレータを回す
	//条件：リストのイテレータの最後の要素ではない
	while (itr != objects_.end())
	{
		//条件：カウンターが引数番号と同様である
		if (i == suffix)
		{
			//引数番号目のオブジェクトに引数座標をセットする
			(*itr)->transform_.position_ = setPosition;
			//座標変換完了を返す
			return true;
		}

		//引数添え字番号と同様でなかった場合、
			//カウンターと、イテレータを回す
		i++;
		itr++;

	}
	//座標変換未完了を返す
	return false;
}

//オブジェクトを取得
GameObject* Spawn::GetSpawnObject(int suffix)
{
	//初期要素の取得
	auto itr = objects_.begin();

	//カウンター
	int i = 0;

	//引数suffix分イテレータを回す
	//条件：リストのイテレータの最後の要素ではない
	while (itr != objects_.end())
	{
		//条件：カウンターと引数番号が同様の場合
		if (i == suffix)
		{
			//オブジェクトポインタを返す
			return (*itr);
		}

		//引数添え字番号と同様でなかった場合、
			//カウンターと、イテレータを回す
		i++;
		itr++;

	}

	//オブジェクトを見つけることができなかった
	return nullptr;

}

//全てのデータを解放する
void Spawn::ReleaseAllData()
{
	//条件：イテレータの最後の要素ではない
	for (auto itr = objects_.begin(); itr != objects_.end(); )
	{
		//要素の解放
		itr = objects_.erase(itr);
	}

	//リストをクリアする
	objects_.clear();

}
