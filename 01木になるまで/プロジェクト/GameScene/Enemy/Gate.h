#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス

//ゲートUVタイプ
enum class GATE_UV_MOVE_TYPE
{
	GATE_UV_MOVE_UP = 0,
	GATE_UV_MOVE_DOWN = 2, 

};

/*
	クラス詳細：敵出現スポーン　グラフィック用クラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene->EnemySpawn
	クラス概要（詳しく）
				：敵が出現するスポーンを視認できるようにしたクラス
				：スポーンのモデルの未所有しておくクラス

*/
class Gate : public GameObject
{
//private メンバ定数
private : 
	//移動スピード
	static constexpr float SPEED_ = 0.01f;
	//UV移動のMAX値
	static constexpr float MAX_MOVE_UV_POWER_ = 2.0f;
	//UV移動の最低値
	static constexpr float MIN_MOVE_UV_POWER_ = 1.0f;


//private メンバ変数、ポインタ、配列
private : 
	//モデルハンドル
	int hModel_;

	//現在の移動方法
	GATE_UV_MOVE_TYPE type_;


	//シェーダーのUV移動値
	float moveUVPower_;
	
//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	Gate(GameObject* parent);

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

};