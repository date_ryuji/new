#include "NavigationForEnemy.h"							//ヘッダ
#include "../../CommonData/GlobalMacro.h"				//共通マクロ
#include "../../Engine/Algorithm/AStarAlgorithm.h"		//AStarアルゴリズム
#include "../Scene/GameSceneManager.h"					//シーンマネージャー

//コンストラクタ
NavigationForEnemy::NavigationForEnemy(int width, int depth, GameSceneManager* pGameSceneManager):
	pAStarAlgorithm_(nullptr),
	WIDTH_(width) , DEPTH_(depth),
	pGameSceneManager_(pGameSceneManager)
{
	//可変配列のクリア
	//リストのリストを空にする
	nextRoutes_.clear();
}

//デストラクタ
NavigationForEnemy::~NavigationForEnemy()
{
	//AStarアルゴリズムのインスタンス消去
	SAFE_DELETE(pAStarAlgorithm_);
}

//初期化
void NavigationForEnemy::Initialize()
{
	//AStarアルゴリズムのインスタンス生成
	pAStarAlgorithm_ = new AStarAlgorithm(WIDTH_ , DEPTH_);
}

//新しいナビゲーション先の追加
int NavigationForEnemy::AddNewList()
{
	//新しい経路先を追加して
	//追加した際の、そのリストへのハンドル番号を取得する
		//ただし、このハンドル番号は、動的に変化するため、
		//敵オブジェクトのスポーンに登録されている敵オブジェクトのハンドル番号と連動していると考えることが最適である。
		//つまり、敵オブジェクトが追加されれば、ナビゲーションのリストも追加されるし、敵オブジェクトが削除されれば、対応のナビゲーションのリストも削除されるので、それに伴って、リストへのハンドル番号も消されたところを詰めて連番で取り直す
			//取り直しの番号の振り分けは、簡易的である

	//リストの確保
		//中身は空
	std::list<Cell*>* pNewList = new std::list<Cell*>;
	pNewList->clear();

	//リストを追加する
	nextRoutes_.push_back(pNewList);

	//リストのサイズ - 1　にて、現在追加した最後の要素の要素番号ハンドルを返す
		//今後、リストにアクセスする際には、下記のハンドル番号でアクセスする
	return (int)nextRoutes_.size() - 1;
}

//引数ポリゴンをCell型で表される座標値に変換する
Cell NavigationForEnemy::GetCood(int polyNum)
{
	//ポリゴン番号（１オリジン）を
	//自クラスのマスのｘｙ値に変換して戻す


	//例
	/*
	
	・
	WIDTH = 5
	DEPTH = 6

	・
	WIDTH * DEPTH = 30

	・
	polyNum = 11


	int y = polyNum / WIDTH = 2
	int x = polyNum % WIDTH = 1

	y = y -1;
	x = x -1;

	*/

	//座標を登録する構造体
	Cell cell;

	//WIDTH数と割って、商　-　１（０オリジンのため）
	cell.y_ = (polyNum / WIDTH_);// - 1;
	//WIDTH数と割って、余り-　１（０オリジンのため）
	cell.x_ = (polyNum % WIDTH_); //- 1;


	return cell;
}

//引数の座標値をポリゴン番号にする
int NavigationForEnemy::GetPolygonNumber(Cell cell)
{
	//cood →　０オリジン
	//polyNum→　１オリジン

	return WIDTH_ * (cell.y_ + 1) + (cell.x_ + 1);
}

//AStar実行
void NavigationForEnemy::ExecutionAStarAlgorithm(int handle , 
	XMVECTOR& startPos, XMVECTOR& goalPos,
	Cell start, Cell target)
{
	//敵ごとのルートを登録するため、
	//1体の敵専用の
	//リストを取得（ハンドルにて指定）
	std::list<Cell*>* pMyList = GetNextRouteList(handle);

	//条件：該当リストが存在していなかったら
	if (pMyList == nullptr)
	{
		//関数終了
		return;
	}


	//ハンドルにて示された要素番目のリストを渡し
	//AStar法実行
		//専用のクラスに
		//AStar実行を呼び込む
		//引数のリストへ、実行時の、経路のセル値を登録してもらって、
		//返してもらう
			//登録された経路をたどることで、目的位置までの最短経路を求めることができる
	pAStarAlgorithm_->Exe(pMyList, start, target);
		

	//開始ポリゴンの位置を取得
	{
		//開始ポリゴンの
		//ワールド座標をもらって、
			//ナビゲーションの一番最初の開始位置を引数の参照渡しの変数へ渡す

		//開始位置のポリゴン番号を取得
		int polyNum = GetPolygonNumber(start);

		//ワールド座標を入れてもらう変数
		XMVECTOR worldNextPos;


		//ポリゴン番号から
			//そのポリゴンのワールド座標を取得
				//上記の変換の関数をシーンマネージャーに持たせてあるため、呼び込み
		pGameSceneManager_->GetWorldPosOfPolygonMakeGround(&worldNextPos, polyNum);

		//引数のベクトルへ渡す
		startPos = worldNextPos;
	}
	//終了ポリゴンの位置を取得
	{
		//終了位置のポリゴン番号を取得
		int polyNum = GetPolygonNumber(target);

		//ワールド座標を入れてもらう変数
		XMVECTOR worldNextPos;


		//ポリゴン番号から
			//そのポリゴンのワールド座標を取得
				//上記の変換の関数をシーンマネージャーに持たせてあるため、呼び込み
		pGameSceneManager_->GetWorldPosOfPolygonMakeGround(&worldNextPos, polyNum);

		//引数のベクトルへ渡す
		goalPos = worldNextPos;
	}
}

//引数にて示される経路へのハンドルから該当リスト（経路）を取得する
std::list<Cell*>* NavigationForEnemy::GetNextRouteList(int handle)
{
	//ハンドル番号番目のリストを取得し
	//要素を返す

	//カウンター
	int i = 0; 
	//イテレータ
	auto itr = nextRoutes_.begin();

	//条件：カウンターが引数ハンドルでない場合
	while (i != handle)
	{
		//条件：最後の要素でないか
		if (itr == nextRoutes_.end())
		{
			//最後の要素が示されたなら
				//空を返す（見つからなかった）
			return nullptr;
		}

		//カウンターを回す
		i++;
		//イテレータを回す
		itr++;


	}

	//イテレータにて示された
	//リストのポインタを渡す
	return (*itr);
}

//次の移動先に更新
bool NavigationForEnemy::RemoveAndUpdateNextRoute(int handle)
{
	//ハンドルにて示されるリストの取得
	std::list<Cell*>* pMyList = GetNextRouteList(handle);

	//条件：取得できなかった
		//本来は、取得できないことは、流れとしておかしい、
		// どこかの処理で間違って削除してしまっている
		// あるいは、アクセスするハンドル番号が間違っている
			//取得できない際、新たに、専用のナビゲーションを確保する処理は追加しない
			//理由は、番号更新の時、オブジェクトがスポーンに確保されている番号順、連番で番号を更新するので、その際にずれが生じてしまう。であれば、そのずれを修正するプログラムを書くより、何かの不具合で、ナビゲーションがないのであれば、ないこと自体がバグとして修正を行うようにする形へ
	if (pMyList == nullptr)
	{
		//ナビゲーションが終了していることを示す
		return false;
	}


	//ハンドルにて示されたリストが
	//条件：空でないなら
	if (!pMyList->empty())
	{
		//一番頭の要素を消去
		pMyList->erase(pMyList->begin());


		//すると、一番頭には、連番で次の要素が来る
		//条件：その要素が何もなく、空だったら、
			//false
			//その要素があるなら、
			//true
		if (pMyList->empty())
		{
			//空なら
			return false;
		}
		
		return true;
	}

	return false;
}


//ナビゲーション開始
void NavigationForEnemy::StartNavigation(int handle , 
	XMVECTOR& startPos, XMVECTOR& goalPos,
	int currentPolyNum, int targetPolyNum)
{
	//開始の前に
	//前回の経路を持っているかもしれないため
	//全データの解放
	ReleaseMyRoute(handle);


	//ナビゲーションのヒューリスティックコスト、合計コスト（AStarによる経路探索をするために必要なコスト類）を初期化
		//実行段階で必ず初期化は行うようにしている
		//そのため、外部から呼ぶ必要はない
	//pAStarAlgorithm_->InitHeuristicAndTotalCost();



	//AStar法によって
	//最短経路を求めて、次の目的地群リストに登録する
	ExecutionAStarAlgorithm(handle ,
		startPos ,  goalPos,
		GetCood(currentPolyNum), GetCood(targetPolyNum));



}

//次の目的地を取得
XMVECTOR NavigationForEnemy::GetNextDestination(int handle)
{
	////前回のポジションとして、
	////現段階における、次の目的位置座標を登録
	//currentPos_ = nextPos_;

	//リストに登録されている
	//一番頭の座標から示される、ｘｙ座標値をポリゴン番号に戻し、
		//そのポリゴン番号から、ポリゴンのワールド座標を取得する

	//ポリゴン番号を取得
		//リストの先頭要素のイテレータの中身として、Coodをもらい、それをポリゴン番号に変換
		//ポリゴン番号が　ー１であっても、以下のワールド座標をもらう処理先で、
		//ー１出会った時のエラー回避を行っている
	int polyNum = GetNextPolygon(handle);


	/*
	////ハンドルにて示されるリストの取得
	//std::list<Cell>* pMyList = GetNextRouteList(handle);

	////取得できなかった
	//if (pMyList == nullptr)
	//{
	//	//関数終了
	//	return;
	//}

	////リストに登録されている
	////一番頭の座標から示される、ｘｙ座標値をポリゴン番号に戻し、
	//	//そのポリゴン番号から、ポリゴンのワールド座標を取得する

	////ポリゴン番号を取得
	//	//リストの先頭要素のイテレータの中身として、Coodをもらい、それをポリゴン番号に変換
	//int polyNum = GetPolygonNumber(*(pMyList->begin()));
	*/

	

	//ワールド座標を入れてもらう変数
	XMVECTOR worldNextPos;

	//ポリゴン番号から
	//そのポリゴンのワールド座標を取得
		//上記の変換の関数をシーンマネージャーに持たせてあるため、呼び込み
	pGameSceneManager_->GetWorldPosOfPolygonMakeGround(&worldNextPos ,  polyNum);



	////次の目的位置座標を
	////メンバ変数に登録
	//nextPos_ = worldNextPos;


	//座標をわたす
	return worldNextPos;


}

//現在のナビゲーション情報の解放
void NavigationForEnemy::ReleaseMyRoute(int handle)
{
	//指定ハンドルにて示される
	//リストを取得し
		//そのリストを空にする（解放はしない）
	//ハンドルにて示されるリストの取得
	std::list<Cell*>* pMyList = GetNextRouteList(handle);

	//条件：取得できなかった
	if (pMyList == nullptr)
	{
		//関数終了
		return;
	}

	//経路の初期化
	pMyList->clear();
}

//ナビゲーション自体の解放
void NavigationForEnemy::EraseMyNavigator(int handle)
{
	//リストをリストから解放し

	//カウンター
	int i = 0;
	//イテレータ
	auto itr = nextRoutes_.begin();

	//条件：カウンターが引数ハンドルと同様でないとき
	while (i != handle)
	{
		//条件：最後の要素でないか
		if (itr == nextRoutes_.end())
		{
			//関数終了
			return;
		}

		//カウンターを回す
		i++;
		//イテレータを回す
		itr++;

	}

	//リストの要素をポインタで取得しておく
	std::list<Cell*>* pMyList = (*itr);

	//大元リストからイテレータにて解放
	nextRoutes_.erase(itr);

	//リストに登録された要素を解放（動的確保の解放）
	SAFE_DELETE(pMyList);
}

//次のポリゴン番号を取得
int NavigationForEnemy::GetNextPolygon(int handle)
{
	//指定ハンドルにて示されるリストを取得
	std::list<Cell*>* pMyList = GetNextRouteList(handle);

	//条件：取得できなかった
	if (pMyList == nullptr)
	{
		//関数終了
		return -1;
	}

	//リストの頭のCell値をポリゴン番号に変更して
	//返す
	auto itr = pMyList->begin();
	//条件：イテレータが最後の要素である場合
	if (itr == pMyList->end())
	{
		return -1;
	}

	//イテレータにて示されるポインタの中身
	return GetPolygonNumber((*(*itr)));
}

