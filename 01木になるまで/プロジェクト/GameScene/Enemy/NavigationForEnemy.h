#pragma once
//システム内　標準ヘッダ(usr/)
#include <list>				//リストを使用するためにインクルード（途中で中身を消去する可変長配列）
#include <DirectXMath.h>	//XMVECTOR型　3次元ベクトル　など3次元処理のソースをまとめたヘッダ

//スコープ省略
using namespace DirectX;


//クラスのプロトタイプ宣言
class GameSceneManager;
class AStarAlgorithm;
//構造体のプロトタイプ宣言
struct Cell;

/*
	クラス詳細：敵オブジェクトごとのナビゲーションを担うクラス
	使用クラス　：GameScene->EnemySpawn->Enemy
	クラス概要（詳しく）
				：敵のナビゲーションAI(AStartAlgorithmを使用した経路探索アルゴリズム)

				：ポリゴン間隔でナビゲーションAIを設定し
				　現在のポリゴンから、次のポリゴンへ通って、
				　最終的な目的位置のポリゴンに到着するためのナビゲーションを行ってもらう

				：次のルートのポリゴンを検索→ワールドベクトルにして、敵オブジェクトに知らせる
				　次のルートの設定→敵オブジェクトに知らせる

				：スタートポリゴンから、ゴールポリゴンへの通るポリゴン位置を取得する
				　経路としてクラスに保存しておくことで、敵ごとのナビゲーション経路を取得することができる。
				　一度経路探索を終了させたら、その経路を保存しておくことで、最短経路の探索を行うことができる


				：★現段階においては、地面セルごとのコストが等しいため、経路探索の重要性がたなくない。
				　しかし、今後確実に、地面セルごとにコストを付ける。（セルのもととなる地面オブジェクトのポリゴン。そのポリゴンが緑に塗られているか、塗られていないかの判定によって、コストを変更する）
				　その時には重要性が高くなる。

				：★上記において、地面のコスト（ポリゴン事の塗り情報）は動的に変化するので、
				　本来は、●ｓに一回、ナビゲーションの経路を更新する必要がある
*/
class NavigationForEnemy
{
//private メンバ定数
private : 
	//static constexpr に設定しない理由
		//：コンストラクタの初期化にて宣言するため。

	//マスのWIDTH(ポリゴンの列数)
		//詳細：（あくまでポリゴン数である。（ポリゴンを作る頂点数ではないので注意））
	const int WIDTH_;
	//マスのHEIGHT（ポリゴンの行数）
	const int DEPTH_;

//private メンバ変数、ポインタ、配列
private : 
	//シーンマネージャークラス
		//詳細：他クラスオブジェクトとの橋渡し
	GameSceneManager* pGameSceneManager_;


	//敵ごとの経路探索済みの経路群の登録
		//詳細：常に最新の経路を保っておく
		//　　：リストのリスト
		//　　：次のルート（次の移動先のポリゴン番号）を登録するリストのリスト（敵オブジェクトごと）
	std::list<std::list<Cell*>*> nextRoutes_;	



	//AStarアルゴリズム実行クラス
		//詳細：実際のセル目コスト、セルの連結セルなどを保存しておく
		//　　：スタートポリゴンから、ゴールポリゴンへの最短経路を求めることができる
	AStarAlgorithm* pAStarAlgorithm_;


	//地面のマス目　地面の移動コストなどを登録しておく
		//詳細：経路をもとめるために、経路の走査のために必要となる2次元配列
		//　　：2次元配列を変数要素数で確保するために、ポインタのポインタで宣言
		//　　：地面ポリゴンにおける緑ポリゴンか、違うかによって、コストを変動することができる。
	//int** costMass_;

//private メソッド
private : 

	//引数ポリゴンをCell型で表される座標値に変換する
		//詳細：ポリゴンは1次元配列の連番で取られている。
		//　　：その連番を2次元配列の連番（Cell型）に変更する
		//引数：ポリゴン番号
		//戻値：Cell型の座標値
	Cell GetCood(int polyNum);

	//引数の座標値をポリゴン番号にする
		//詳細：GetCoode()の反対
		//引数：Cell型の座標値
		//戻値：ポリゴン番号
	int GetPolygonNumber(Cell cell);


	//AStar実行
		//詳細：引数のマス地から、目的のマス地までのAStar法を求めて、
		//　　：目的の経路を　次の経路群（nextRoute_）に登録する
		//引数：経路へのハンドル（nextRoutes_）
		//引数：スタート位置
		//引数：ゴール位置
		//引数：スタートセル
		//引数：ゴールセル
		//戻値：なし
	void ExecutionAStarAlgorithm(int handle , 
		XMVECTOR& startPos, XMVECTOR& goalPos,
		Cell start , Cell target);


	//引数にて示される経路へのハンドルから該当リスト（経路）を取得する
		//引数：経路へのハンドル（nextRoutes_）
		//戻値：経路リスト
	std::list<Cell*>* GetNextRouteList(int handle);

//public メソッド
public : 

	//コンストラクタ
		//詳細：経路探索を行うために、マス目を作るための（width , depth）をもらう
		//　　：AStarによる、経路探索をやりやすくするために、マス目（2次元配列）を実際に作り、それを経路探索の元として扱う
		//引数:ポリゴンの列数
		//引数:ポリゴンの行数
		//引数:シーンマネージャー
		//戻値：なし
	NavigationForEnemy(int width, int depth , GameSceneManager* pSampleSceneManager);

	//デストラクタ
		//引数：なし
		//戻値：なし
	~NavigationForEnemy();



	//初期化	
		//詳細：経路探索を行うAStartAlgorithmの初期化
		//引数：なし
		//戻値：なし
	void Initialize();

	//ナビゲーションのルートを確保する枠を作成
		//引数：なし
		//戻値：経路リストへのハンドル（敵ごとのルートとなるCell群をリストとして持っておく）
	int AddNewList();


	//次の移動先に更新
		//詳細：リストに登録している、次の登録先群の一番頭、つまり、次の移動先を一つ進めて、次の移動先を更新
		//　　：つまり、次の移動先に登録されている位置についたため、次の移動先を更新する
		//　　：Removeの意味するもの:今回までの経路をリストからRemove
		//　　：Updateの意味するもの:次回からの経路にリストへUpdate
		//戻値：次の目的位置が存在するか(存在するTrue、存在しないFalse)（Falseの場合、ナビゲーションの終了となる）
	bool RemoveAndUpdateNextRoute(int handle);

	//ナビゲーション開始
		//詳細：現在あるナビゲーションを削除して、
		//　　：新たな目的地へのナビゲーションを開始する
		//引数：経路へのハンドル（nextRoutes_）
		//引数：スタート位置
		//引数：ゴール位置
		//引数：スタートセル
		//引数：ゴールセル
		//戻値：なし
	void StartNavigation(int handle , 
		XMVECTOR& startPos , XMVECTOR& goalPos,
		int currentPolyNum , int targetPolyNum);


	//次の目的地を取得
		//詳細：RemoveAndUpdateNextRouteを呼び出し後、次のルートとなる、目的位置ポリゴンから、
		//　　：ワールド座標で、目的の位置を取得する
		//引数：経路へのハンドル（nextRoutes_）
		//戻値：次の目的ポリゴンのワールド座標
	XMVECTOR GetNextDestination(int handle);

	//現在のナビゲーション情報の解放
		//詳細：ルート
		//　　：開始位置
		//　　：次の目的位置
		//引数：経路へのハンドル（nextRoutes_）
		//戻値：なし
	void ReleaseMyRoute(int handle);

	//ナビゲーション自体の解放
		//引数：経路へのハンドル（nextRoutes_）
		//戻値：なし
	void EraseMyNavigator(int handle);


	//次のポリゴン番号を取得
		//詳細：現在移動中の移動先のポリゴンを取得して、ポリゴン番号を取得	
		//引数：経路へのハンドル（nextRoutes_）
		//戻値：次のポリゴン
	int GetNextPolygon(int handle);






};




