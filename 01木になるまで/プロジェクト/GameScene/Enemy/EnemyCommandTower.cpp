#include "EnemyCommandTower.h"					//ヘッダ
#include "../../Engine/Algorithm/Timer.h"		//タイマークラス
#include "../Scene/GameSceneManager.h"			//シーンマネージャー
#include "../CountTimer/CountTimer.h"			//カウントタイマー
#include "Enemy.h"								//敵オブジェクト
#include "EnemySpawn.h"							//敵スポーンクラス
#include "NavigationForEnemy.h"					//敵オブジェクトのナビゲーター(ナビゲーションAI)
#include "EnemyStrategyBulletinBoard.h"			//Enemyの戦略掲示板（情報をテキスト描画）


//コンストラクタ
EnemyCommandTower::EnemyCommandTower(GameObject* parent)
	: GameObject(parent, "EnemyCommandTower"),
	isStopTime_(false),
	pNavigationForEnemy_(nullptr),
	pEnemyBulletinBoard_(nullptr),

	pEnemySpawn_(nullptr),
	pSceneManager_(nullptr),
	pCountTimer_(nullptr)
{
	//可変配列のクリア
	listNextTargetPolyNum_.clear();
}

//初期化
void EnemyCommandTower::Initialize()
{
	{
		//シーンマネージャーの取得
		//シーンチェンジャーを経由して
			//引数指定のシーンのマネージャークラスを取得
		pSceneManager_ = (GameSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);

		//SceneManagerに自身を登録する
		pSceneManager_->AddCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_COMMAND_TOWER, (GameObject*)this);
	}


	{
		//シーンマネージャークラス（クラス間の橋渡しクラス）から
			//地面オブジェクトのWidth（横頂点数）、Depth（奥行き頂点数）の確保
		int width;
		int depth;
		pSceneManager_->GetVertexesWidthCountAndDepthCount(&width, &depth);

		//上記の関数呼び込みで取得できる
		//Width,Depthは、あくまでも頂点数のため、
			//敷き詰めてある、ポリゴン数にそれぞれ変換して、渡す。
		width = width - 1;	//横頂点数 - 1
		depth = (depth - 1) * 2;	//(奥頂点数 - 1) * 2 <一行に2個分存在するため * 2>





		//ナビゲーター（ナビゲーションAI）の確保
			//インスタンスの生成
		//地面に敷き詰めてある、横のポリゴン数、奥のポリゴン数をセット（＋シーンマネージャー）
		pNavigationForEnemy_ = new NavigationForEnemy(width, depth, pSceneManager_);
		pNavigationForEnemy_->Initialize();
	}


	{
		//カウントタイマーのインスタンス生成
		pCountTimer_ = (CountTimer*)Instantiate<CountTimer>(this);

		//タイマーのスタート
		StartCountTimer();
	}


	{
		//敵の戦略掲示板インスタンス生成
		//インスタンス生成
		pEnemyBulletinBoard_ = (EnemyStrategyBulletinBoard*)Instantiate<EnemyStrategyBulletinBoard>(this);
	
	}

}

//更新
void EnemyCommandTower::Update()
{
	//条件：時間が停止していないか
	if (!(IsStopTime()))
	{
		//一定間隔で敵が出現するようにする。
		JudgementSpawnEnemy();
		
		//仕事の割り当て
			//仕事を持たないオブジェクトを探し
			// そのオブジェクトへ
			//攻撃対象の割り当て
		FindEnemyNotHaveAJob();

		//敵オブジェクトへフレームの経過時間（デルタタイム）の計測を行わせる
		UpdateDeltaTime();

	}
}

//描画
void EnemyCommandTower::Draw()
{
}

//解放
void EnemyCommandTower::Release()
{
}

//スポーンから指定敵を取得
Enemy* EnemyCommandTower::GetEnemy(int suffix)
{
	//スポーンからオブジェクトを取得する
	return (Enemy*)pEnemySpawn_->GetSpawnObject(suffix);
}

//スポーンにて扱う敵オブジェクト合計数を取得
int EnemyCommandTower::GetObjectCount()
{
	return pEnemySpawn_->GetObjectCount();
}

//敵オブジェクトの時間計測を更新する
void EnemyCommandTower::UpdateDeltaTime()
{
	//フレームの経過時間を計測する
	float elpasedTime = Timer::GetDelta();


	//ゲームオブジェクトを一つずつ取得
	const int MAX_COUNT = GetObjectCount();

	//条件：スポーン管理のゲームオブジェクト数
	//全オブジェクトのデルタタイムの更新
	for (int i = 0; i < MAX_COUNT; i++)
	{
		//オブジェクト取得
		Enemy* pEnemy = GetEnemy(i);

		//経過時間を更新
		pEnemy->UpdateDeltaTime(elpasedTime);

	}
}
//タイマーの開始(初期)
void EnemyCommandTower::StartCountTimer()
{
	//経過時間計測のために
		//タイマー（カウントアップ）の開始
	pCountTimer_->StartTimerForCountUp(0.0f, TIME_CREATING);

}
//タイマーの再計測開始
void EnemyCommandTower::ReStartCountTimer()
{
	//再計測開始
		//タイマーのハンドル＝０
	pCountTimer_->ReStartTimerForCountUp(0, 0.0f, TIME_CREATING);
}
//タイマーの停止
void EnemyCommandTower::NotPermittedTimer()
{
	pCountTimer_->NotPermittedTimer();
}
//タイマーの再開
void EnemyCommandTower::PermitTimer()
{
	pCountTimer_->PermitTimer();
}
//敵が出現判定
void EnemyCommandTower::JudgementSpawnEnemy()
{
	//一定間隔で敵が出現するようにする。

	//カウントアップ
	//条件：生成間隔を終了時間として指定されたタイマーが終了時間以上と計測結果を出したら
	//　　：&&
	//　　：次のナビゲーション先が存在するならば
	if (pCountTimer_->EndOfTimerForCountUp() && !(listNextTargetPolyNum_.empty()))
	{
		//敵を生成する
		ForciblyCreateEnemy();

		//看板描画
		//敵を生成したことを
			//看板に描画し、知らせる
		AddTalkWhenSpawnEnemy();


		//時間の再計測
		ReStartCountTimer();
	}
}

//敵オブジェクトの初期化処理
void EnemyCommandTower::InitEnemyPointer(Enemy* pEnemy)
{
	//初期化
	//スポーンのポインタのセット
	pEnemy->SetMySpawn(pEnemySpawn_);


	//ナビゲーターへの経路を保存するリストの確保
	int hMyNavigation =  pNavigationForEnemy_->AddNewList();

	//ナビゲーションのリストへアクセスするためのハンドル　と
	//ナビゲーターのポインタのセット
	pEnemy->SetMyNavigator(hMyNavigation ,  pNavigationForEnemy_);

}

//仕事を持たないオブジェクトを探す
void EnemyCommandTower::FindEnemyNotHaveAJob()
{
	//自身の全インスタンスにアクセスして
	//現在、割り当てるべき、番号があるか、

	//ある場合、その番号を誰に割り当てるか、

	//現在移動を行っていない敵に対して割り当てる
	//→敵にアクセスして、現在移動中かを調べる。
	//→移動中でなかったら、移動させるためのナビゲーション開始


	//仕事を持たないオブジェクト群
	//現在参照する全敵オブジェクト
	//仕事を持っているものは除外されるリスト
	std::list<Enemy*> noneWork;
	noneWork.clear();


	//ゲームオブジェクトを一つずつ取得
	const int MAX_COUNT = GetObjectCount();

	//条件：スポーン管理のゲームオブジェクト数
	//全オブジェクトのデルタタイムの更新
	for (int i = 0; i < MAX_COUNT; i++)
	{
		//オブジェクト取得
		Enemy* pEnemy = GetEnemy(i);


		//条件：仕事を持ってい無かったら
		//仕事を欲していたら
			//ナビゲーションを行っていない
			//かつ
			//攻撃を行っていない
		if (pEnemy->WantJob())
		{

			//仕事を持っていないリストへ追加
			noneWork.push_back(pEnemy);
		}
	}


	//仕事を持たないオブジェクト群を渡して
	//そのオブジェクト一人一人に仕事を与える
	JobAssignment(&noneWork);

}

//仕事を持たないオブジェクトに仕事を割り当てる
void EnemyCommandTower::JobAssignment(std::list<Enemy*>* noneWork)
{
	//イテレータ
	//仕事を持っていないオブジェクトの先頭を取得
	auto noneWorkerItr = noneWork->begin();

	//初期値：対象のポリゴン番号群の初期イテレータ
	//条件　：イテレータの最後の要素ではない
	//現在スタックとしてある、
	//木オブジェクトへのポリゴン番号
	//この番号を、仕事の持っていない人に割り当てる
	for (auto itr = listNextTargetPolyNum_.begin(); itr != listNextTargetPolyNum_.end();)
	{
		//条件：仕事を持っていないオブジェクトのイテレータにて、仕事を持っていないオブジェクト群の最後の要素ではないとき
		//仕事を持たないリストにいる、Enemyに割り当てる
		if (noneWorkerItr != noneWork->end())
		{
			//ナビゲーション開始を宣言

			//現在の敵オブジェクトの現在位置から、真下にレイを飛ばして、衝突しているポリゴンを探す
			//→このポリゴンをスタート位置とする
			int collisionPolyNum = pSceneManager_->RayCastForEnemyAndGround((*noneWorkerItr));

			//条件：衝突したポリゴン番号がー１でない
			if (collisionPolyNum != -1)
			{
				////Enemyのメソッドへアクセスするために、ダウンキャスト（親を子へ）
				//Enemy* pEnemy = (Enemy*)(*noneWorkerItr);

				//スタート位置とゴール位置を敵オブジェクトに知らせて、
				//ナビゲーションの開始
				(*noneWorkerItr)->StartNavigation(collisionPolyNum, (*itr));

			}


			//イテレータを回して、
			noneWorkerItr++;
			//ポリゴン番号も解放
			itr = listNextTargetPolyNum_.erase(itr);

		}
		else
		{
			break;
		}

	}

}

//EnemySpawnのセット
void EnemyCommandTower::SetEnemySpawn(EnemySpawn* pEnemySpawn)
{
	pEnemySpawn_ = pEnemySpawn;
}

//ナビゲーション命令
void EnemyCommandTower::StartNavigation(int goalPolyNum)
{
	//リストの番号をスタック
	//次のフレームのUpdateにて、
	//手の空いている敵を見つけて、目的位置まで移動する
	listNextTargetPolyNum_.push_back(goalPolyNum);
}

//司令塔へナビゲーションへアクセスするためのハンドル番号の更新を行ってもらう
void EnemyCommandTower::ReassigningMyNavigationHandle()
{
	//敵オブジェクトをスポーンから取得
		//スポーンから取得するオブジェクト一つ一つ, 取得する順番でハンドル番号を連番で取り、
		//番号更新を敵オブジェクトへ伝える

	//オブジェクトのカウント数取得
	const int MAX_COUNT = GetObjectCount();

	//条件：スポーン管理のゲームオブジェクト数
	//全オブジェクトのデルタタイムの更新
	for (int i = 0; i < MAX_COUNT; i++)
	{
		//オブジェクト取得
		Enemy* pEnemy = GetEnemy(i);
	
		//ハンドル番号の更新
		//繰り返しの変数が０オリジンから、と連番で取られているためハンドル番号へそのまま使用する
		pEnemy->UpdateMyNavigationHandle(i);

	}

}

//時間が停止しているか
bool EnemyCommandTower::IsStopTime()
{
	return isStopTime_;
}

//自身のタイマーの停止
void EnemyCommandTower::StopTime()
{
	//時間停止フラグを立てる
	isStopTime_ = true;
	//タイマー停止
	NotPermittedTimer();
}

//自身のタイマーの再開
void EnemyCommandTower::StartTime()
{
	//時間停止フラグをおろす
	isStopTime_ = false;
	//タイマー再開
	PermitTimer();

}

//強制的に敵を一体生成させる
void EnemyCommandTower::ForciblyCreateEnemy()
{
	//エネミー出現
		//親：スポーン
	Enemy* pEnemy = (Enemy*)pEnemySpawn_->AddNewInstance(pEnemySpawn_);

	//追加時のEnemyへの初期化
	//管理下に置くことで、
	//必要なポインタ類を渡すなどの処理実行。
	InitEnemyPointer(pEnemy);
}

//敵情報看板にトーク追加
//木を倒した
void EnemyCommandTower::AddTalkWhenTreeFall(int enemyNum, int treeNum)
{
	//引数要素から会話の文字列として登録する
	{
		//描画テキストの作成
		//会話情報の作成
		TalkInfo talkInfo(GOOD_EVENING);
		//表示用の文字列を追加
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "敵" + std::to_string(enemyNum) + "　は、");
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "木" + std::to_string(treeNum) + "　を倒しました。");


		//テキスト情報のスタック
		pEnemyBulletinBoard_->AddStackTalkInfo(&talkInfo);
	}
}

//敵情報看板にトーク追加
//敵が倒れた
void EnemyCommandTower::AddTalkWhenReleaseEnemy(int enemyNum)
{
	//引数要素から会話の文字列として登録する

	{
		//描画テキストの作成
		//会話情報の作成
		TalkInfo talkInfo(GOOD_EVENING);
		//表示用の文字列を追加
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "敵" + std::to_string(enemyNum) + "　は、");
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "自然消滅しました。");

		//テキスト情報のスタック
		pEnemyBulletinBoard_->AddStackTalkInfo(&talkInfo);
	}

	
}

//敵情報看板にトーク追加
//ナビゲーションを行っていたが対象の木が存在しなかった(目的地に木が存在しなかった)
void EnemyCommandTower::AddTalkWhenNullTree(int enemyNum)
{
	//引数要素から会話の文字列として登録する
	{
		//描画テキストの作成
		//会話情報の作成
		TalkInfo talkInfo(GOOD_EVENING);
		//表示用の文字列を追加
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "敵" + std::to_string(enemyNum) + "　は、");
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "攻撃対象の敵を見つけられませんでした。");

		//テキスト情報のスタック
		pEnemyBulletinBoard_->AddStackTalkInfo(&talkInfo);
	}

}

//敵情報看板にトーク追加
//攻撃開始を宣言
void EnemyCommandTower::AddTalkWhenStartAttack(int enemyNum, int treeNum)
{
	//引数要素から会話の文字列として登録する
	{
		//描画テキストの作成
		//会話情報の作成
		TalkInfo talkInfo(GOOD_EVENING);
		//表示用の文字列を追加
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "敵" + std::to_string(enemyNum) + "　は、");
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "木" + std::to_string(treeNum) + "　への攻撃を開始します。");

		//テキスト情報のスタック
		pEnemyBulletinBoard_->AddStackTalkInfo(&talkInfo);
	}
}

//敵情報看板にトーク追加
//敵のナビゲーションを知らせる
void EnemyCommandTower::AddTalkWhenStartNavigation(int enemyNum, int treeNum)
{
	//引数要素から会話の文字列として登録する
	{
		//描画テキストの作成
		//会話情報の作成
		TalkInfo talkInfo(GOOD_MORNING);
		//表示用の文字列を追加
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "敵" + std::to_string(enemyNum) + "　は、");
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "木" + std::to_string(treeNum) + "　への移動をお願いします。");

		//テキスト情報のスタック
		pEnemyBulletinBoard_->AddStackTalkInfo(&talkInfo);
	}
}

//敵情報看板にトーク追加
//敵の生成を知らせる
void EnemyCommandTower::AddTalkWhenSpawnEnemy()
{
	//引数要素から会話の文字列として登録する
	{
		//描画テキストの作成
		//会話情報の作成
		TalkInfo talkInfo(GOOD_MORNING);
		//表示用の文字列を追加
		pEnemyBulletinBoard_->PushBackStr(&talkInfo, "敵を、新たに一体生成しました。");

		//テキスト情報のスタック
		pEnemyBulletinBoard_->AddStackTalkInfo(&talkInfo);
	}
}

