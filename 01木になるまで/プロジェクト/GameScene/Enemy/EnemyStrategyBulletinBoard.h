#pragma once
//システム内　標準ヘッダ(usr/)
#include <list>		//リストを使用するためにインクルード（途中で中身を消去する可変長配列）
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス


//クラスのプロトタイプ宣言
class Texts;
class CsvReader;
class CountTimer;
class UIGroup;
class EnemySpawn;
class GameSceneManager;


//テキスト描画における必要画像タイプ
enum UI_TYPE_BULLETIN_BOARD
{
	BULLETIN_BOARD_UI_TEXT_FRAME = 0 ,		//テキストの枠組み
	BULLETIN_BOARD_UI_ENEMY_ICON ,			//敵アイコン
	BULLETIN_BOARD_UI_COMMAND_TOWER_ICON,	//司令塔（指揮官）アイコン

	BULLETIN_BOARD_UI_MAX,					//MAX
};
//音楽タイプ
enum AUDIO_TYPE_BULLETIN_BOARD
{
	//敵A
	//敵B
	//司令塔A

	BULLETIN_BOARD_AUDIO_MAX,

};
//会話のタイプ
	//詳細：会話における表示パターンを示すタイプ
enum TALK_TYPE
{
	HELLO = 0,			//こんにちは（テスト）
	GOOD_MORNING ,		//おはようございます（テスト）
	GOOD_EVENING,		//こんばんは（テスト）

	TALK_MAX,			//MAX
};

/*
	構造体詳細	：会話情報
	構造体概要（詳しく）
				：会話における表示文字列を格納し、会話タイプごとに表示方法を切り替える

*/
struct TalkInfo
{
	//会話タイプ
	TALK_TYPE talkType;
	//会話内容（文字列）(可変長配列)
	std::vector<std::string> talkTexts;

	//コンストラクタ
	TalkInfo();
	TalkInfo(TALK_TYPE setType);

};





/*
	クラス詳細：敵の戦略掲示板
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：敵の攻撃、木の倒木状況、
				　司令塔（指揮官）の攻撃対象の指示、　などを、敵の会話としてゲーム中に表示する。敵のコミュニケーション、戦略を統計して、ゲーム中の文字として出力するクラス

				：出力文字のデフォルト（共通）部分
				　〜〜　「を攻撃しました。」などはCSVに保存して起き、そのCSVより読み取る

*/
/*

	//司令塔の指示からの会話表示方法
	//敵の行動
	//BulletinBoardへ行動における会話描画を行う方法は、
	//司令塔：　�@会話が発生する行動を行った。（司令塔クラスにて）
	//			�ASceneMnagerにアクセスして、行動の種類と、行動詳細を引数に渡して　会話を描画する関数を呼び込む（司令塔クラスにて）
	//			�BSceneManagerから、BulletinBoardの会話描画関数を呼び込む（SceneManagerクラスにて）
	//			�CBulletinBoardの次に描画するテキスト軍に登録
	//			�D描画準備ができたら、そのテキストを描画

	//上記を踏まえると、
	//BulletinBoardがSpawnや、司令塔のポインタを持っておく必要はなく、
	//必要時に呼び込まれる、一種のオブザーバーパターンの実装を行えばよい。

*/
class EnemyStrategyBulletinBoard : public GameObject
{
//private メンバ定数
private : 

	//看板描画　時間間隔：開始時間
	static constexpr float INTERVAL_START = 0.f;
	//看板描画　時間間隔：終了時間
	static constexpr float INTERVAL_END = 2.f;

	//描画位置
		//テキスト描画開始位置
		//フォントサイズ：25
	static constexpr int FIRST_LINE_X = 250;
	static constexpr int FIRST_LINE_Y = 70;
	static constexpr int SECOND_LINE_X = 250;
	static constexpr int SECOND_LINE_Y = 100;



//private メンバ変数、ポインタ、配列
private : 
	//時間停止のフラグ
		//詳細：時間による行動、移動、攻撃、HP現象などなど様々な動的要因を停止する
	bool isStopTime_;

	//各会話タイプごとに音楽ハンドルをロードして、そのハンドル番号を保存しておく配列
	int* hAudioForEachTalks_;

	//シーンマネージャークラス
	GameSceneManager* pSceneManager_;
	
	//UIGroup
		//詳細：テキスト描画を行う際の、テキストの枠組みや、他テキスト描画における必要画像を所有しておくクラス
	UIGroup* pUIGroup_;
	//テキストの共通部分の文章を保存したCSVファイル
	CsvReader* pCommonTexts_;
	//タイマークラス
		//詳細：一つの会話を描画する時間を計測するクラス
	CountTimer* pCountTimer_;
	
	//テキスト群
	Texts* pTexts_;

	//現在描画中の会話情報
	TalkInfo* pCurrentTalkInfo_;

	//各会話タイプごとの描画タイプ
		//詳細：会話描画時の描画UIのタイプを、会話タイプごとに持っておく
		//　　：可変長配列の一次元配列
	std::vector<UI_TYPE_BULLETIN_BOARD>* pUIForEachTalks_;

	//現在描画中のUI番号
	//詳細：UIGroupにおけるUIの番号
	std::list<int> visibleImageNums_;

	//表示文字のスタック
		//詳細：敵の行動を動的に表示するために、次に表示するものをスタックで持たせておく
		//　　：一定時間表示後、スタックの一番上の文字から描画していく
	std::list<TalkInfo> pNextTalkInfos_;


	//表示文字のタイプに応じた処理関数を持たせた関数ポインタの配列
		//詳細：TALK_TYPEごとの関数を用意するので、
		//　　：TALK_TYPEごとの関数ポインタを格納できるようにする。
	void(EnemyStrategyBulletinBoard::* pFunctionForEachTalks_[TALK_MAX])();


//private メソッド
private : 

	//時間が停止しているか
		//引数：なし
		//戻値：停止しているか
	bool IsStopTime();


	//UIGroupにおける　必要画像の作成、ロード
		//引数：なし
		//戻値：なし
	void CreateUIGroup();
	//関数ポインタの登録
		//引数：なし
		//戻値：なし
	void SetFunctionPointaForEachTalks();
	//会話ごとの描画UIタイプのセット
		//引数：なし
		//戻値：なし
	void SetUIForEachTalks();
	//会話ごとのSEタイプのセット（同時に、音楽のロード）
		//引数：なし
		//戻値：なし
	void SetAudioForEachTalks();

	//スタックの先頭データを取得	
		//引数：なし
		//戻値：なし
	TalkInfo* GetStackTopData();
	//スタックの先頭データを削除
		//引数：なし
		//戻値：なし
	void EraseStackTopData();

	//現在描画中の画像番号を削除する
		//引数：なし
		//戻値：なし
	void InitVisibleImageNums();
	//現在描画中と判断されている画像番号を使用し画像を描画させる
		//引数：なし
		//戻値：なし
	void VisiblePermitImage();
	//描画する画像番号として登録
		//引数：描画UIタイプ
		//戻値：なし
	void VisibleImage(UI_TYPE_BULLETIN_BOARD uiType);
	//全画像の非描画
		//引数：なし
		//戻値：なし
	void InvisibleImage();


	//会話情報の更新（現在描画の会話の更新）
		//引数：次の描画対象となる会話情報（更新情報）へ更新
		//戻値：なし
	void UpdateCurrentTalkInfo(TalkInfo* nextTalkInfo);
	//会話情報の削除（現在描画の会話の削除）
		//引数：現在の描画対象となる会話情報の削除
		//戻値：なし
	void DeleteCurrentTalkInfo();

	/*会話タイプにおける　各会話の書式設定**********************************************/
		/*
			//現在の会話タイプにより呼び込む関数を切り替え、
			//会話先で指定されたフォーマットにのっとり、会話を切り替える
			//あるいは、CSVファイルから、会話の共通部分を読み取り、会話を作る関数を求める。→その際、この会話は、CSVのこのセルと、このセルで、文章を作るなどのフォーマットを整える。
				//→関数に、会話の文章を入れると、文章を変える際に、いちいちソースを開くことになるので、、ソースには、どのように表示するかなどを書き、実際の文章はCSVに保存しておく
		*/

	//詳細：こんにちは（テスト）　表示
		//タイプ：HELLO
		//引数：なし
		//戻値：なし
	void DrawTextHello();
	//詳細：おはようございます（テスト）　表示
		//タイプ：GOOD_MORNING
		//引数：なし
		//戻値：なし
	void DrawTextGoodMorning();
	//詳細：こんばんは（テスト）　表示
		//タイプ：GoodEvening
		//引数：なし
		//戻値：なし
	void DrawTextGoodEvening();

	//テキスト
	//一行目の描画
		//引数：描画テキスト
		//戻値：なし
	void DrawTextFirstLine(const std::string& drawText);
	//テキスト
	//二行目の描画
		//引数：描画テキスト
		//戻値：なし
	void DrawTextSecondLine(const std::string& drawText);

	//会話中の音楽を停止
		//引数：なし
		//戻値：なし
	void StopAudioCurrentTalkInfo();

//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	EnemyStrategyBulletinBoard(GameObject* parent);
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~EnemyStrategyBulletinBoard() override;

	//初期化
				//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
				//レベル：オーバーライド
				//引数：なし
				//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public:

	//時間の停止（行動、移動、攻撃などの停止）
		//引数：なし
		//戻値：なし
	void StopTime();
	//時間の再開（行動、移動、攻撃などの再開）
		//引数：なし
		//戻値：なし
	void StartTime();

	//会話のスタック
		//引数：会話情報
		//戻値：なし
	void AddStackTalkInfo(TalkInfo* stackTalkInfo);

	//引数構造体のベクター（可変長配列）に文字列を追加
		//引数：テキストを追加したい会話情報
		//引数：追加テキスト
		//戻値：なし
	void PushBackStr(TalkInfo* stackTalkInfo , const std::string& pushBackStr);

};



