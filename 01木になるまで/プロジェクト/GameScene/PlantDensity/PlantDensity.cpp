#include "PlantDensity.h"					//ヘッダ
#include "../../CommonData/GlobalMacro.h"	//共通マクロ
#include "../../Engine/Text/Texts.h"		//テキスト
#include "../../Engine/Text/TextWriter.h"	//テキスト書込みクラス
#include "../Scene/GameSceneManager.h"		//シーンマネージャー
#include "../Scene/StageData.h"				//ステージデータ
											//クリアまでの繁殖率


//コンストラクタ
PlantDensity::PlantDensity(GameObject * parent)
	: GameObject(parent, "PlantDensity"),
	pSceneManager_(nullptr),
	pTexts_(nullptr),
	percentage_(0)
{
}

//デストラクタ
PlantDensity::~PlantDensity()
{
	//解放
	SAFE_DELETE(pTexts_);
}

//初期化
void PlantDensity::Initialize()
{
	//シーンマネージャーの取得
		//シーンチェンジャーを経由して
			//引数指定のシーンのマネージャークラスを取得
	pSceneManager_ = (GameSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);

	//テキストインスタンス生成
	pTexts_ = new Texts;
	//テキスト初期化
	pTexts_->Initialize();

}

//更新
void PlantDensity::Update()
{
	//シーンマネージャーにアクセスして、
		//現在のGrassLandのポリゴン合計数と、　範囲を拡大した、ポリゴンの合計数を取得し、
		//割合をint値の％で取得する

	int polygonNumOfGrass = pSceneManager_->GetTotalNumberOfPolygonWithGrass();
	int totalPolygonNUm = pSceneManager_->GetTotalPolygonCountOfGrassLand();

	//部分 / 全体にて割合を求める
	float ratio = (float)polygonNumOfGrass / totalPolygonNUm;

	//割合をパーセンテージにする
		//上記の割合にて、小数の値になっている
		//それを3桁の％にする
		//2桁右へ→100倍
	percentage_ = (int)(ratio * 100.f);

}

//描画
void PlantDensity::Draw()
{
	//条件：シーンマネージャーの描画許可が行われていたら
	if (pSceneManager_->IsPermitTimer())
	{
		//フォントサイズの変更
		pTexts_->SetFontSize(15.f);
		

		//現在の繁殖率出力
		//テキストDraw
		pTexts_->DrawStringTex("現在", TEXT_COLOR_RED, 1050, 75);
		
		pTexts_->SetFontSize(20.f);
		//メンバ変数のパーセンテージを描画
		pTexts_->DrawTex<int>(percentage_, TEXT_COLOR_RED, 1100, 70);
		//”％”の描画
		pTexts_->DrawTex<char>('%', TEXT_COLOR_RED, 1150, 70);



		////フォントサイズの変更
		pTexts_->SetFontSize(13.f);

		//クリアまでの残り％出力
		pTexts_->DrawStringTex("クリアまで残り" , TEXT_COLOR_RED, 1000, 110);
		
		pTexts_->SetFontSize(20.f);
		
		//75%まで　０以上の場合　75% - 現在%
		//75%まで　０未満の場合　０
			//max関数にて、大きいほうを出すようになるので、
			//現在が７５％以上の場合は、第二引数はマイナスとなるので、　０が表示されるようになる
		int remaining = max(0, (int)(StageData::CLEAR_BREEDING_RATE - percentage_));
		pTexts_->DrawTex<int>(remaining , TEXT_COLOR_RED, 1100, 100);
		pTexts_->DrawTex<char>('%', TEXT_COLOR_RED, 1150, 100);
	}



}

//解放
void PlantDensity::Release()
{
	//自身が消去時に、
	//最終的に獲得していた％を、
	//ファイルにて書き込み
	CreateFileOfPlantDensity();
}

//繁殖率をファイルに書き込む
void PlantDensity::CreateFileOfPlantDensity()
{
	//自身が消去時に、
	//最終的に獲得していた％を、
	//ファイルにて書き込み

	//そのファイルを次のシーンで読み込ませる


	TextWriter* pTextWriter = new TextWriter(DELIMITER_COMMA);

	//書き込み文字の作成
	pTextWriter->PushBackString(std::to_string(percentage_), true);

	//書き込み終了
	pTextWriter->PushEnd();

	//ファイル書き込み
	pTextWriter->FileWriteExcecute("Assets/Scene/OutputData/ResultScene/Percentage.csv");

}

//繁殖率を取得する
int PlantDensity::GetPlantDensity()
{
	return percentage_;
}
