#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス

//クラスのプロトタイプ宣言
class Texts;
class GameSceneManager;


/*
	クラス詳細	：ゲーム内の植物密度（別名：植物の繁殖率、緑の豊かさ）を管理するクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：全体のポリゴン数と、現段階でGrassオブジェクトによって、範囲を広げられている合計のポリゴン数
				　上記の値を、部分 / 全体　によって、割合（○○○％（０％〜１００％））の値を求めて、
				：その値を該当の場初期描画させる

*/
class PlantDensity : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 
	//描画に用いる割合値
		//int : 3桁（０〜１００）
	int percentage_;

	//シーンマネージャークラス
	GameSceneManager* pSceneManager_;
	//テキスト群
	Texts* pTexts_;


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	PlantDensity(GameObject* parent);

	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~PlantDensity() override;

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//繁殖率をファイルに書き込む
		//引数：なし
		//戻値：なし
	void CreateFileOfPlantDensity();
	//繁殖率を取得する
		//引数：なし
		//戻値：なし
	int GetPlantDensity();

};

