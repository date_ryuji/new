#include "GrassLandObject.h"					//ヘッダ
#include "../../Engine/Model/Model.h"			//モデル
#include "../../Engine/GrassLand/GrassLand.h"
	/*
		//ハイトマップ画像から、
		//地面オブジェクトを作成するクラス
			//ハイトマップから画像を読み込ませる処理を呼んで、
			//そのクラスを、Model.cppのモデルデータベース情報群にセットさせる
			// + UVを動的に切り替えて、テクスチャを部分的に切り替えるモデル
	
	*/
#include "../../Engine/Csv/CsvReader.h"			//テキスト（,区切り）を読み込ませるリーダー
#include "../Scene/GameSceneManager.h"			//シーンマネージャー


//コンストラクタ
GrassLandObject::GrassLandObject(GameObject * parent)
	: GameObject(parent, "GrassLandObject"),
	hModel_(-1),
	pSceneManager_(nullptr),
	pGrassLand_(nullptr)
{
}

//初期化
void GrassLandObject::Initialize()
{
	//シーンオブジェクト取得
	GameObject* pScene = GetCurrentScene(SCENE_ID::SCENE_ID_GAME);
	//上記にてシーンオブジェクトを取得出来ていることで、
	//現在のシーンがGameSceneであることが確定した。（上記を行う理由は、他シーンにおいてもGrassLandObject（自クラス）を生成するため、その際には下記処理が不要になる。エラーの原因となってしまう）
	//条件：取得出来ているか
	if(pScene != nullptr)
	{
		//シーンマネージャーの取得
			//シーンチェンジャーを経由して
				//引数指定のシーンのマネージャークラスを取得
		pSceneManager_ = (GameSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);

		//自身をセットする
		pSceneManager_->AddCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND, this);
	}

	//地面情報
	float maxY = 0.f;
	float minY = 0.f;
	float size = 0.f;

	{
		//テキストファイルを読み込んで、
		//テキストファイルから
		//地面を作る頂点の最高Y座標値、最低Y座標値を取得する
		CsvReader* pCsvReader = new CsvReader;
		pCsvReader->Load("Assets/Scene/OutputData/GameScene/MapEditorResult/MaxY_And_MinY.txt");

		//CSVにおける
		//ｘ , ｙ
		//０，０
		//最高値
		maxY = pCsvReader->GetValueFloat(0, 0);
		//１，０
		//最低値
		minY = pCsvReader->GetValueFloat(1, 0);
		//CSVReaderの解放
		SAFE_DELETE(pCsvReader);
	}

	
	{
		//ポリゴンサイズ
		CsvReader* pCsvReader = new CsvReader;	//CsvReaderクラスには、
									//一度ロードすると、そのデータを常に持ち続けるので、一回解放して、もう一度違うファイルをロードさせる
		pCsvReader->Load("Assets/Scene/OutputData/GameScene/MapEditorResult/PolygonSize.txt");
		//サイズ取得
		size = pCsvReader->GetValueFloat(0, 0);
		//解放
		SAFE_DELETE(pCsvReader);
	}



	//ハイトマップ地面のモデルの初期化
	pGrassLand_ = new GrassLand;
	//Y座標最高、最低値、サイズを渡し、ハイトマップ画像とも合わせ地面を作成
	pGrassLand_->Initialize(maxY, minY, size, SHADER_TYPE::SHADER_HEIGHT_MAP,
		"Assets/Scene/OutputData/GameScene/MapEditorResult/HeightMap.png", "Assets/Scene/Image/GameScene/HeightMap/Grass_Red.png");

	//Model.cppにおける
	//データベースに追加する形に変形
	Model::ModelData modelData;
	modelData.fileName = "Assets/Scene/OutputData/GameScene/MapEditorResult/HeightMap.png";
	modelData.pPolygonGroup = pGrassLand_;
	modelData.thisPolygonGroup = POLYGON_GROUP_TYPE::GRASS_LAND_POLYGON_GROUP;
	modelData.thisShader = SHADER_TYPE::SHADER_HEIGHT_MAP;
	modelData.transform = transform_;

	//Model.cppのデータベース軍に追加
	hModel_ = Model::AddModelData(modelData);
	//警告
	assert(hModel_ != -1);


	//地面オブジェクトの降下
	transform_.position_.vecY = -5.0f;

}

//更新
void GrassLandObject::Update()
{
}

//描画
void GrassLandObject::Draw()
{
	//描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//解放
void GrassLandObject::Release()
{
	//解放（Delete）はしない
		//解放はModelクラスに任せているので、変に解放しない
		//ただ、ポインタを使えなくするだけ
	pGrassLand_ = nullptr;
}

//時間停止を宣言
void GrassLandObject::StopTime()
{
	//時間停止を宣言
	pGrassLand_->StopTime();
}

//時間計測再開を宣言
void GrassLandObject::StartTime()
{
	//時間計測再開を宣言
	pGrassLand_->StartTime();
}

//指定ポリゴンがGrass（草オブジェクト）に、塗られているか、塗られていないか
bool GrassLandObject::IsGreenPolygon(int polyNum)
{
	return pGrassLand_->IsGreenPolygon(polyNum);
}

//該当ポリゴンを緑ポリゴンにするGrass（草オブジェクト）を作る
HRESULT GrassLandObject::CreateGrassPolygon(int polyNum)
{
	//ポリゴンコピークラス（Grass）の作成宣言
	return pGrassLand_->CreateGrassPolygon(polyNum);
}

//ポリゴン番号のポリゴンを作る
bool  GrassLandObject::GetLocalPosOfPolygon(XMVECTOR* localPos, int polyNum)
{
	//3Dポリゴンクラスの
	//GrassLandの該当の関数を呼ぶ
	return pGrassLand_->GetLocalPosOfPolygon(localPos , polyNum);
}

//合計ポリゴン数の取得
int GrassLandObject::GetPolygonCount()
{
	return pGrassLand_->GetPolygonCount();
}

//頂点を敷き詰めたときのWidthとDepthの取得
void GrassLandObject::GetWidthAndDepth(int * getWidth, int * getDepth)
{
	pGrassLand_->GetWidthAndDepth(getWidth, getDepth);
}

//ポリゴンサイズの取得
float GrassLandObject::GetPolygonSize()
{
	return pGrassLand_->GetPolygonSize();
}

//Grassの広げた緑の範囲を狭める
void GrassLandObject::StartShrinkingGrass(int targetPolyNum)
{
	pGrassLand_->StartShrinkingGrass(targetPolyNum);
}

//Grassを拡げている、Grassが繁殖しているポリゴンの合計数を取得する
int GrassLandObject::GetTotalNumberOfPolygonWithGrass()
{
	//GrassLandポリゴンクラスを呼び込む
	return pGrassLand_->GetTotalNumberOfPolygonWithGrass();

}

//特定ポリゴンを緑ポリゴンにする
void GrassLandObject::CreateOneGreenPolygon(int polyNum)
{
	//GrassLandの同様の処理を呼び込む
	pGrassLand_->CreateOneGreenPolygon(polyNum);

}

//指定ポリゴンが緑ポリゴン（全Grassの中で誰も塗っていなかったら）
void GrassLandObject::IfNotPaintedCreateOneGreenPolygon(int polyNum)
{
	//GrassLandの同様の処理を呼び込む
	pGrassLand_->IfNotPaintedCreateOneGreenPolygon(polyNum);
}

//強制的にGrassの拡張、成長を行う（１回）
void GrassLandObject::ForciblyGrowGass()
{
	//GrassLandの同様の処理を呼び込む
	pGrassLand_->ForciblyGrowGass();
}

//指定回数拡張、成長を行わせる
void GrassLandObject::ForciblyGrowGass(int count)
{
	pGrassLand_->ForciblyGrowGass(count);
}

//Grassに、地面の全ポリゴンを持たせる
void GrassLandObject::CreateAllGreenPolygon()
{
	pGrassLand_->CreateAllGreenPolygon();
}
