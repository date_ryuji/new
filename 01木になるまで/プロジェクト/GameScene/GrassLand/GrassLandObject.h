#pragma once
//システム内　標準ヘッダ(usr/)
#include <windows.h>	//Windows機能をまとめたヘッダ
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス


//クラスのプロトタイプ宣言
class GameSceneManager;
class GrassLand;


/*
	クラス詳細：地面オブジェクトクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：ハイトマップから、画像を読み込み、
				　地面の幅、奥行きを決めて地面オブジェクトを作る。
				  読み込んだ画像の幅ピクセルを地面のポリゴン幅に使用する。
				  地面のY座標の高さを決める

				：オブジェクト単位で地面を管理するクラス
				　詳しいポリゴンなどのクラスは、自身クラスにてロードするGrassLand（class）ポリゴンクラスにて管理する。

*/
class GrassLandObject : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 

	//モデル番号
	int hModel_;

	//シーンマネージャークラス
	GameSceneManager* pSceneManager_;

	//ポリゴン群
	GrassLand* pGrassLand_;


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	GrassLandObject(GameObject* parent);

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//時間の停止宣言
		//詳細：GrassLand（地面のポリゴンを管理するクラス）のポインタへ
		//　　：時間の停止を宣言
		//引数：なし
		//戻値：なし
	void StopTime();

	//時間の計測再開宣言
		//詳細：GrassLand（地面のポリゴンを管理するクラス）のポインタへ
		//　　：時間の計測再開を宣言
		//引数：なし
		//戻値：なし
	void StartTime();


	//指定ポリゴンがGrass（草オブジェクト）に、塗られているか、塗られていないか
		//詳細：GrassLand（地面のポリゴンを管理するクラス）のポインタへ
		//　　：指定ポリゴン番号が、黒ポリゴンか、緑ポリゴンか
		//引数：ポリゴン番号
		//戻値：ポリゴンの色（緑ポリゴン：true , 黒ポリゴン：false）
	bool IsGreenPolygon(int polyNum);

	//モデル番号を与える
		//引数：なし
		//戻値：モデル番号
	int GetModelHandle() { return hModel_; };

	//該当ポリゴンを緑ポリゴンにするGrass（草オブジェクト）を作る
		//詳細：引数ポリゴンをもとにそのポリゴンをコピーして、ポリゴンを新たに作り、
		//　　：そのポリゴンを四方にコピーしていって、
		//　　：ポリゴンを動的に広げていく処理を行う
		//　　：→これは、ポリゴンの色を、もとのモデルデータと別の色にすることで、
		//　　：→ポリゴン（色違い）がコピーされることで、
		//　　：→ポリゴンが塗られた表現を行える
		//　　：新規にGrassクラスの作成
		//引数：ポリゴン番号
		//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	HRESULT CreateGrassPolygon(int polyNum);


	//ポリゴン番号のポリゴンを作る
		//詳細：ポリゴンのローカル座標取得
		//　　：ポリゴンを作る　３頂点の中心を求めて、
		//　　：そのローカル座標を渡す
		//引数：ローカル座標（ポインタ）
		//引数：ポリゴン番号
		//戻値：取得できたか
	bool GetLocalPosOfPolygon(XMVECTOR* localPos, int polyNum);

	//合計ポリゴン数の取得
		//引数：なし
		//戻値：地面を作るポリゴンの合計数
	int GetPolygonCount();
	//頂点を敷き詰めたときのWidthとDepthの取得
		//詳細：地面を作るポリゴンのWidth,Depthを求める
		//引数：Width（ポインタ）
		//引数：Depth（ポインタ）
		//戻値：なし
	void GetWidthAndDepth(int* getWidth, int* getDepth);
	//ポリゴンサイズの取得
		//引数：なし
		//戻値：ポリゴンサイズ
	float GetPolygonSize();


	//木オブジェクトが解放されて
		//詳細：GrassLand（地面のポリゴンを管理するクラス）のポインタへ
		//　　：木オブジェクトが立地していたポリゴン番号を受け取り、
		//　　：そのポリゴン番号と同様のポリゴンから草オブジェクト（Grass）を広げたオブジェクトを見つけ出し、
		//　　：そのオブジェクトの広げた緑の範囲を狭める
		//引数：対象ポリゴン番号
		//戻値：なし
	void StartShrinkingGrass(int targetPolyNum);


	//Grassを拡げている、Grassが繁殖しているポリゴンの合計数を取得する
		//詳細：緑に塗られたポリゴン数
		//　　：各々のGrassにて、重複するポリゴン番号は、合計数にカウントしない
		//　　：メンバに所有している、各Grassクラスの所有しているポリゴンのポリゴン番号とから、最終的に塗られている、ポリゴン番号合計数を取得
		//引数：なし
		//戻値：緑に塗られたポリゴン数
	int GetTotalNumberOfPolygonWithGrass();


	//特定ポリゴンを緑ポリゴンにする
		//詳細：ポリゴンを緑ポリゴンにするには、Grass（草オブジェクト）に、塗るポリゴンを渡して、塗ってもらう必要がある。
		//　　：しかし、そのGrassは、一つではなく、複数存在している。
		//　　：今回の処理では、存在するGrassのうちのどれかのGrassに特定ポリゴンを緑にするように処理をくわえる
		//　　：該当Grassー＞一番目のGrassに、特定のポリゴンを、緑ポリゴンにする処理を実行（そのポリゴンはGrassではなく、あくまでも、存在するGrassの指定ポリゴンだけを緑ポリゴンにさせるやり方である）
		//引数：ポリゴン番号
		//戻値：なし
	void CreateOneGreenPolygon(int polyNum);

	//指定ポリゴンが緑ポリゴン（全Grassの中で誰も塗っていなかったら）
		//詳細：一番目のGrass（詳細は、CreateOneGreenPolygon()コメントを参照）に、塗らせる
		//引数：ポリゴン番号
		//戻値：なし
	void IfNotPaintedCreateOneGreenPolygon(int polyNum);


	//強制的にGrassの拡張、成長を行う（１回）
		//詳細：存在している全Grassの拡張
		//　　：（範囲を1段階広げる。
		//　　：（ここにおける一段階とは、Grassはポリゴンを1ポリゴンから上下左右にポリゴンを広げることで範囲を広げていく。その上下左右にポリゴンを1回広げることを1段階広げると置く））
		//引数：なし
		//戻値：なし
	void ForciblyGrowGass();
	//指定回数拡張、成長を行わせる
		//詳細：ForciblyGrowGass()における拡張を任意回数行う（ForciblyGrowGass()の拡張回数は1回であった）
		//　　：（０，０）からGrassを拡張させるとき、Width回分拡張すれば、一番端まで届く拡張になる
		//　　：拡張数が多ければ、処理数、繰り返し数が多くなるので注意
		//引数：拡張回数
		//戻値：なし
	void ForciblyGrowGass(int count);

	//Grassに、地面の全ポリゴンを持たせる
		
		//詳細：一番目のGrass（詳細は、CreateOneGreenPolygon()コメントを参照）を対象のGrassとする
		//　　：塗られていない、ポリゴンを一つのGrassに持たせて、全ポリゴンを塗られた表現とする
		//引数：なし
		//戻値：なし
	void CreateAllGreenPolygon();

};