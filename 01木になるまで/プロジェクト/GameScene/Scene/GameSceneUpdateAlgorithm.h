#pragma once
//システム内　標準ヘッダ(usr/)
#include <string>	//文字列を使用するためにインクルード



//クラスのプロトタイプ宣言
class GameSceneUpdateAlgorithm;
class GameSceneManager;

//ゲームシーンのシーンの流れ（更新行動の順番）を識別するタイプ
	//詳細：シーン内のイベントごとに識別子を持ち、イベント開始時の初期化、イベント中のUpdate処理、など。
	//　　：ゲーム内の動的処理を管理するクラス識別
enum class GAME_SCENE_UPDATE_ALGORITHM
{
	GAME_UPDATE_ALGORITHM_DISP_HELPIMAGES = 0,		//ヘルプ画像表示
	GAME_UPDATE_ALGORITHM_TRANSITION_START ,		//スタート画像の遷移
	GAME_UPDATE_ALGORITHM_PLAY_GAME ,				//ゲームプレイ
	GAME_UPDATE_ALGORITHM_TRANSITION_END,			//エンド画像の遷移
	GAME_UPDATE_ALGORITHM_CHANGE_SCENE,				//シーン切り替え

	GAME_UPDATE_ALGORITHM_MAX ,						//MAX

};




/*
	クラス詳細	：各シーンの流れ（更新行動の順番）を作るクラス　作成クラス（シーン専用）
	使用デザインパターン：FactoryMethod
	使用クラス	：GameScene
	クラス概要（詳しく）
				：GAME_SCENE_UPDATE_ALGORITHMに登録したシーンイベントタイプにて生成先を識別し、
				　専用クラスを作成する

*/
class FactoryForGameSceneUpdateAlgorithm
{
//private メンバ変数
private : 
	//タグの文字列類
		//詳細：タグ読み込みの際に、読み込んだタグを比較するための基準を確保するための文字列
	std::string** pTags_;

//public メソッド
public : 
	//コンストラクタ
		//引数：なし
		//戻値：なし
	FactoryForGameSceneUpdateAlgorithm();
	//デストラクタ
		//引数：なし
		//戻値：なし
	~FactoryForGameSceneUpdateAlgorithm();

	//タグによる行動関数の生成
		//引数：生成行動（イベント）タグ
		//引数：シーンマネージャークラス
		//戻値：アルゴリズムクラスインスタンス
	GameSceneUpdateAlgorithm* CreateGameSceneUpdateAlgorithm(const std::string tag , GameSceneManager* pSceneManager);


};



//ゲームシーンにおける
//GameScenemanagerという、ゲーム全体の橋渡しのクラス、全体管理のクラスにおける
	//毎フレーム行う、Update処理をクラスで選択することで、
	//GameSceneManagerと分離する

	//クラスにすることで、必要な、処理開始のための初期化、更新をクラスの関数を呼び出すだけで済ませることが可能になる


//CSVに登録している文字列のタグ
	//そのタグを必要タイミングで読み込み（GameSceneManagerにてタイミング管理）


/*
	クラス詳細	：シーンの流れ（更新行動の順番）の実行行動を実装した行動ストラテジークラス
	使用クラス	：GameScene
	クラスレベル：抽象クラス
	クラス概要（詳しく）
				：ゲーム全体の流れを作るUpdate処理をクラス分けし、必要タイミングで、必要イベントを起こすことを可能とする
				　そのイベントごとのクラスの抽象クラス

*/
class GameSceneUpdateAlgorithm
{
//protected メンバ変数、ポインタ、配列
protected : 
	//シーンマネージャークラス
	GameSceneManager* pSceneManager_;


	//処理が終了しているかのフラグは、
		//このフラグをもとに、次のタグを読み込むかの判断を行う
		//GameSceneManagerにて行う

//public メソッド
public : 

	//コンストラクタ
		//引数：シーンマネージャー
		//戻値：なし
	GameSceneUpdateAlgorithm(GameSceneManager* pSceneManager);
	//デストラクタ
		//レベル：仮想関数
		//引数：なし
		//戻値：なし
	virtual ~GameSceneUpdateAlgorithm();

	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：純粋仮想関数
			//引数：なし
			//戻値：なし
	virtual void Initialize() = 0;
	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void Update() = 0;
	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void Draw() = 0;
	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void Release() = 0;


};

