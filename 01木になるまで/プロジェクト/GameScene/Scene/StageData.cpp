#include "StageData.h"
#include "../../Engine/Scene/SceneUserInputter.h"
#include "GameSceneInputStrategy.h"
#include "../../CommonData/GlobalMacro.h"



/*StageData*/
//GameSceneにおけるステージ情報を保存しておく情報名前空間
namespace StageData
{
	//デフォルト（ステージ移動時のキー受付を行う）キー入力、マウス入力の情報を登録
		//詳細：ゲームシーン中の通常のキー入力情報（通常：UI展開時以外）
		//制限：途中でキーの追加、キーの削除を行うことはできない
	DefaultKeyInfo** defaultKeys_;
	
}

//初期化
void StageData::Initialize()
{

	//通常キー種類(enum)　個数分動的確保
	defaultKeys_ = new DefaultKeyInfo* [(int)DEFAULT_KEY_TYPE::MAX];
	/*for (int i = 0; i < height; i++)
	{
		defaultKeys_[i] = new DefaultKeyInfo[width];
	}*/


	//通常キー入力の入力情報登録
	{
		//左クリック：木生成UI表示
		defaultKeys_[(int)DEFAULT_KEY_TYPE::MOUSE_LEFT] = new DefaultKeyInfo(
			KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_DOWN,
			INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_MOUSE,
			(int)MOUSE_CODE::MOUSE_BUTTON_LEFT,
			GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_CREATE_TREE);

		//右クリック：敵情報　OR　木情報UI表示
		defaultKeys_[(int)DEFAULT_KEY_TYPE::MOUSE_RIGHT] = new DefaultKeyInfo(
			KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_DOWN,
			INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_MOUSE,
			(int)MOUSE_CODE::MOUSE_BUTTON_RIGHT,
			GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_TREE_INFO_OR_ENEMY_INFO);
		//ESC：タイトルへ戻る　UI表示
		defaultKeys_[(int)DEFAULT_KEY_TYPE::ESC] = new DefaultKeyInfo(
			KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_DOWN,
			INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_KEY,
			DIK_ESCAPE,
			GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_BACK_TITLE_SCENE);
		//Q：ゲームマップ生成　UI表示
		defaultKeys_[(int)DEFAULT_KEY_TYPE::Q] = new DefaultKeyInfo(
			KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_DOWN,
			INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_KEY,
			DIK_Q,
			GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_CREAE_GAME_MAP);
		//移動
		{
			//W：移動W
			defaultKeys_[(int)DEFAULT_KEY_TYPE::W] = new DefaultKeyInfo(
				KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_ALWAYS,
				INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_KEY,
				DIK_W,
				GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER);

			//A：移動A
			defaultKeys_[(int)DEFAULT_KEY_TYPE::A] = new DefaultKeyInfo(
				KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_ALWAYS,
				INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_KEY,
				DIK_A,
				GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER);

			//S：移動S
			defaultKeys_[(int)DEFAULT_KEY_TYPE::S] = new DefaultKeyInfo(
				KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_ALWAYS,
				INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_KEY,
				DIK_S,
				GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER);
			//D：移動D
			defaultKeys_[(int)DEFAULT_KEY_TYPE::D] = new DefaultKeyInfo(
				KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_ALWAYS,
				INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_KEY,
				DIK_D,
				GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER);
		}
	
	}

	/*
	//		//左クリック：木生成UI表示
		CreateStrategyForEachKey(
			GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_CREATE_TREE, 
			GAME_SCENE_INPUT_TYPE::GAME_INPUT_MOUSE_LEFT);

		//右クリック：敵情報　OR　木情報UI表示
		CreateStrategyForEachKey(
			GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_TREE_INFO_OR_ENEMY_INFO, 
			GAME_SCENE_INPUT_TYPE::GAME_INPUT_MOUSE_RIGHT);

		//ESC：タイトルへ戻る　UI表示
		CreateStrategyForEachKey(
			GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_BACK_TITLE_SCENE,
			GAME_SCENE_INPUT_TYPE::GAME_INPUT_ESC);

		//Q：ゲームマップ生成　UI表示
		CreateStrategyForEachKey(
			GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_CREAE_GAME_MAP , 
			GAME_SCENE_INPUT_TYPE::GAME_INPUT_Q);





		//移動
		{
			//W：移動W
			CreateStrategyForEachKey(
				GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER, 
				GAME_SCENE_INPUT_TYPE::GAME_INPUT_W);
			//A：移動A
			CreateStrategyForEachKey(
				GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER, 
				GAME_SCENE_INPUT_TYPE::GAME_INPUT_A);
			//S：移動S
			CreateStrategyForEachKey(
				GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER, 
				GAME_SCENE_INPUT_TYPE::GAME_INPUT_S);
			//D：移動D
			CreateStrategyForEachKey(
				GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER, 
				GAME_SCENE_INPUT_TYPE::GAME_INPUT_D);
		}

		*/

}

//解放
void StageData::Release()
{
	//動的確保要素の解放
	for (int i = 0; i < (int)DEFAULT_KEY_TYPE::MAX; i++)
	{
		//配列内の要素解放
		SAFE_DELETE(defaultKeys_[i]);
	}
	//配列ポインタ自身、配列枠の解放
	SAFE_DELETE_ARRAY(defaultKeys_);
}

//通常キー入力情報の取得
DefaultKeyInfo** StageData::GetDefaultKeyInfo()
{
	//StageData(namespace)のメンバ
	//通常キー入力情報を渡す
	return defaultKeys_;
}

