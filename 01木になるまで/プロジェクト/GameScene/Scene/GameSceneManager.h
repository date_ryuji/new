#pragma once
//システム内　標準ヘッダ(usr/)
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/SceneManagerParent.h"	//シーンマネージャー　抽象クラス


//クラスのプロトタイプ宣言
class Button;
class UIGroup;
class CountTimer;
class CsvReader;
class Texts;
class HelpImages;
class GameSceneUpdateAlgorithm;
//構造体のプロトタイプ宣言
struct TreeInfo;
struct EnemyInfo;
struct RayCastData;
//enum class のプロトタイプ宣言
enum class HELP_IMAGES_BUTTON_ALGORITHM;
enum class GAME_SCENE_INPUT_ALGORITHM;
enum class GAME_SCENE_INPUT_TYPE;
enum class MOVE_DIRECTION;
enum class TREE_PARTS;
enum class GAME_SCENE_UPDATE_ALGORITHM;



/*
	UIGroup

	UI,Butttonは、下記のUIGroupにて、一括で管理する
	シーンマネージャーにおいても、UIなどはUIGroup管理にて管理する

*/

//ゲームシーンにおけるUIGroup（UI群）のタイプ
	//詳細：ゲームシーン内にて管理する常駐のUIGroup
	//　　：外部からUIGroupの登録する際に使用する
enum class GAME_SCENE_UI_GROUP_TYPE
{
	GAME_UI_GROUP_CREATE_TREE = 0,	//木生成　UIグループ
	GAME_UI_GROUP_ENEMY_INFO,		//敵情報　UIグループ
	GAME_UI_GROUP_TREE_INFO,		//木情報　UIグループ
	GAME_UI_GROUP_PLANT_DENSITY,	//繁殖率背景　UIグループ
	GAME_UI_GROUP_FPS_CURSOR,		//FPSの酔い止め画像　UIグループ
	GAME_UI_GROUP_BACK_TITLE_SCENE,	//タイトルへ戻る　UIグループ
	GAME_UI_GROUP_TIMER,			//タイマー背景　UIグループ
	GAME_UI_GROUP_REMAIN_NUMBER,	//残り生成可能個数　UIグループ
	GAME_UI_GROUP_GAME_MAP ,		//マップ生成　UIグループ

	GAME_UI_GROUP_MAX,				//MAX

};


//ゲームシーンにおける連携や仲介が必要なオブジェクトクラス
	//詳細：連携を行う際にアクセス先のオブジェクトクラス
	//　　：外部からオブジェクトの登録する際に使用する
enum class GAME_SCENE_COOP_OBJECTS
{
	GAME_COOP_PLAYER = 0,			//プレイヤー
	GAME_COOP_GRASS_LAND ,			//地面(草原の地面オブジェクト)
	GAME_COOP_BACK_GROUND_TREE,		//背景用の木オブジェクト群管理クラス
	GAME_COOP_TREE_OWNED_BY_PLAYER,	//プレイヤーが所有する木オブジェクト群管理クラス（描画専用）
	GAME_COOP_ENEMY_SPAWN ,			//敵オブジェクトのスポーン、管理、スタッククラス
	GAME_COOP_ENEMY_COMMAND_TOWER,	//敵の司令塔オブジェクト
	GAME_COOP_GAME_INPUTER ,		//デバイス入力を行うユーザー入力クラス
	GAME_COOP_BULLETIN_BOARD,		//敵の会話表示クラス（敵掲示板クラス）
	GAME_COOP_GAME_MAP ,			//マップクラス

	GAME_COOP_MAX,					//MAX

};

//木オブジェクトの登録方向
	//詳細：木オブジェクトを登録する際、
	//　　：登録先の方向を決めるタイプと、同時に、登録地に各方向の登録角度を定める
enum TREE_DIRECTION
{
	DOWN_DIRECTION = 0 ,		//下
	RIGHT_DIRECTION = 90,		//右
	UP_DIRECTION = 180,			//上
	LEFT_DIRECTION = 270,		//左

};



/*
	クラス詳細：ゲームシーンのマネージャークラス（シーン内オブジェクトとの橋渡しオブジェクト）
	クラスレベル：サブクラス（スーパークラス（SceneManagerParent））
	使用クラス　：GameScene
	クラス概要（詳しく）
				：マネージャークラス
				：シーン内の常駐UIGroupを登録し、表示、非表示
				：シーンの各オブジェクトの連携（橋渡し）を行うクラス
				：詳しくは、SceneManagerParentを参照

				：レイキャストやオブジェクト同士で連携させたいときに、それぞれの干渉度合いを下げるために、シーンマネージャーを利用して連携させる
				：連携が必要である場合は、必ず以下クラスを使用して連携させる
				：オブザーバーとサブジェクトであるならば　サブジェクトのようなクラス。　特定の行動を受け取るオブザーバー（シーンマネージャーに連携を求めるクラス）がサブジェクト（シーンマネージャー）の該当クラスを呼び込み連携。


				：デバイス入力時の行動（SceneInputeStrategy）実行メソッド、
				　ボタン押下時の行動　（SceneButtonStrategy）実行メソッド、
				  ゲームの流れ更新時行動（UpdateAlgorithm）実行メソッド　各オブジェクト間の連携のため、ゲームの流れを作る際に下記メソッドが呼び出される

*/
class GameSceneManager : public SceneManagerParent
{
//private メンバ変数、ポインタ、配列
private : 
	
	/*各オブジェクト連携のために所有しておく　保存用変数値***********************************/
		/*
		
			本来なら、シーンマネージャーに保存しておくのはお勧めできない。
			木の情報なら、木に持たせておくべきだし、プレイヤーの情報なら、プレイヤーに持たせておくべき。
			必要なタイミングで常に呼び出して、取得すれば済むこと。

		*/

	//木の残り生成可能本数テキストの描画フラグ
	bool visibleTreeNum_;


	//前回最後にクリックして衝突したポリゴン番号
		//詳細：地面を作るポリゴンにおいて、プレイヤー操作で、地面との当たり判定を行った際、
		//　　：衝突した最後のポリゴンを保存しておく
		//　　：UI表示中に、UI表示中、UI表示後に使用するポリゴン番号のため記憶のため保存しておく
		//    ：最後のポリゴンあり：地面のポリゴン番号
		//	  ：最後のポリゴンなし：−１
	int lastCollisionPolyNum_;

	//現在参照している木の情報
		//詳細：プレイヤー操作における、触れた、触れている木オブジェクトの情報を保存しておく変数
		//　　：UI表示中に、UI表示中、UI表示後に使用する情報のため記憶のため保存しておく
	TreeInfo* currentTreeInfo_;

	
	/*シーンマネージャークラス管理　変数など*********************************************/

	//時間計測クラス
		//詳細：シーン内制限時間計測、シーンの流れ（更新行動の順番）を記述したタグCSVを読み込むための時間計測のため
	CountTimer* pCountTimer_;


	//テキスト描画クラス
		//詳細：時間計測クラスにて計測した時間をテキストとして表示する際のテキストクラス
	Texts* pTexts_;

	//設定画面におけるヘルプ画像　インスタンス
	HelpImages* pSettingHelpImages_;

	/*シーンの流れ（更新行動の順番）*/
	//シーンの流れ（更新行動の順番）の現在参照中の更新行動クラス（Game全体の管理におけるUpdateを行ってくれるクラス）
		//詳細：現在行動中の更新行動　（イベント実行、クエスト発行、会話イベント発生、特定UI表示、プレイヤー自由行動、クエスト終了、）
		//　　：各イベントごとの更新Updateの実行を行う。
		//　　：各イベントごとのUpdateで行う場合、制限時間計測をおこなったり、UIの遷移をおこなったりと別個にUpdateしなくてはいけないことがあり、終了を受け取る必要がある。その受け取り、遷移を担う
	GameSceneUpdateAlgorithm* pUpdateAlgorithm_;
	//シーンの流れ（更新行動の順番）が保存してあるCSVデータクラス
		//詳細：CSVのタグにて、処理の行動実行順が記述されている。このCSVを先頭から読み取り、タグごとに処理を実行することで、ゲームの流れを作り出すことができる。
		//　　：イベント実行、クエスト発行、会話イベント発生、特定UI表示、プレイヤー自由行動、クエスト終了、などのゲームの流れを管理する
	CsvReader* pCsvReader_;
	//次のタグ読み込みのフラグ
		//詳細：シーンの流れ（更新行動の順番）読み込みフラグが立っているとき、次のタグを呼ぶ
	bool isGetTag_;
	//現在読み込んでいるタグの番号
		//詳細：シーンの流れ（更新行動の順番）における現在参照、実行している更新行動番号
	int currentUpdateAlgorithm_;

	



//private メソッド
private : 

	//ゲームの流れ更新処理
		//引数：なし
		//戻値：なし
	void UpdateGameAlgorithm();



	//残り生成可能本数　テキスト描画
		//引数：なし
		//戻値：なし
	void DrawRemainNumberOfCreateTree();
	//残り制限時間　テキスト描画
		//引数：なし
		//戻値：なし
	void DrawTimeLimit();


	//HelpImageの作成
		//詳細：画面に表示するヘルプイメージ（操作説明やゲームヘルプを示した画像）UIGroupにて　特定画像を遷移、非表示をボタンにて管理するクラス
		//引数：ヘルプイメージ設置位置X
		//引数：ヘルプイメージ設置位置Y
		//引数：ヘルプイメージ設置サイズX
		//引数：ヘルプイメージ設置サイズY
		//引数：ヘルプイメージの遷移用のボタン押下時の行動アルゴリズム
		//戻値：作成したヘルプイメージのインスタンス
	HelpImages* CreateHelpImages(int posX, int posY, int sizeX, int sizeY
		, HELP_IMAGES_BUTTON_ALGORITHM endingType);
	//HelpImage内のボタンの作成
		//引数：ヘルプイメージインスタンス
		//引数：ヘルプイメージの遷移用のボタン押下時の行動アルゴリズム
		//戻値：なし
	void CreateButtonInHelpImages(HelpImages* pHelpImages, HELP_IMAGES_BUTTON_ALGORITHM endingType);



	//木を生やす（木の生成実行）
		//引数：木のタイプ（int型にてもらう（BUttonStretegyのEnum値とTreeのEnum値を同様にする））（通常の木、ダミーの木など）
		//引数：生成位置を示すポリゴン番号
		//戻値：なし
	void GrowTree(int type, int polyNum);

	//TreeオブジェクトのTransformを取得する
		//詳細：引数要素を使用して、Transformの作成
		//引数：ローカル座標
		//引数：ローカルサイズ
		//戻値：Transform
	Transform GetTreeTransform(XMVECTOR& localToWorld, float initSize);


	//木を生成できるか
		//詳細：生成可能本数上限を超えている場合生成不可能
		//引数：生成ポリゴン番号
		//戻値：生成可能（可能：true , 不可能：false）
	bool IsCreateTree(int* polyNum);

	//地面とのレイキャスト
		//詳細：レイキャストデータ（情報）
		//戻値：なし
	void RayCastWithGround(RayCastData* pData);

	//指定ポリゴン番号のローカル座標を取得
		//詳細：ポリゴンを作る３頂点の中心座標
		//　　：上記のローカル座標を取得
		//引数：ローカル座標（ポインタ）
		//引数：ポリゴン番号
		//戻値：取得できたか（取得：true , 取得できない：false）
	bool GetLocalPosOfPolygonMakeGround(XMVECTOR* localPos, int polyNum);

	//ローカル座標を引数Transformでワールド座標に変換
		//引数：ローカル→ワールド座標
		//引数：変換用Transform 
		//戻値：なし
	void TransformedInTheWorldMatrix(XMVECTOR* toPos, Transform& trans);


	//木が生やされたことを敵オブジェクトスポーンに報告する
		//詳細：木がはやされたポリゴン番号を渡すことで、敵へのナビゲーション先を知らせる
		//引数：木がはやされたポリゴン番号
		//戻値：なし
	void InformEnemySpawnThatTreeHaveGrown(int polyNumWithTree);

	//残り生成可能木本数　テキストを描画拒否
		//連携先：GameSceneManager
		//連携元：GameSceneInputStrategy
		//引数：なし
		//戻値：なし
	void InvisibleTreeNum();




//public ゲームループメソッド
public:
	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）	
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	GameSceneManager(GameObject* parent);

	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~GameSceneManager() override;

	
	//初期化
			//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
			//レベル：オーバーライド
			//引数：なし
			//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;


	//UIGroupの個数を取得
		//詳細：自身の所有しているUIGroup群（ppUIGroups_）内のUIGroupの個数を取得
		//  　：○○_SCENE_UI_GROUP_TYPE::○○_UI_GROUP_MAX(enum)を返す。（○○　＝　各シーン名）
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	int GetUIGroupCount() override;


//public メソッド
public : 

	//時間計測が行われているかの確認
		//詳細：世界の時間が停止されているか、動いているか
		//引数：なし
		//戻値：時間計測許可（許可：true , 拒否：false）
	bool IsPermitTimer();


	/*外部からHelpImageへの参照**********************************************************/
	//設定画面(UIGroup)におけるヘルプ画像(HelpImage)を作成する
		//詳細：設定画面（UIGroup）表示中に表示するHelpImageの作成。表示を行う
		//引数：なし
		//戻値：なし
	void CreateSettingHelpImages();
	//設定画面(UIGroup)におけるヘルプ画像(HelpImage)を削除する
		//詳細：設定画面（UIGroup）表示中に表示するHelpImageの削除。
		//引数：なし
		//戻値：なし
	void DeleteSettingHelpImages();

	//初期描画のHelpImages描画開始時限定
	//HelpImages描画開始を受け取る
		//引数：なし
		//戻値：なし
	void StartDrawHelpImages();
	//初期描画のHelpImages描画終了時限定
	//HelpImages描画終了を受け取る
		//引数：なし
		//戻値：なし
	void EndingDrawHelpImages();


	/*UpdatAlgorithmより呼ばれる関数*******************************************************/
	//カウントタイマーのセット
		//詳細：外部のSceneManager更新関数を持つクラスより（UpdateAlgorithm）セットされる
		//引数：カウントタイマー
		//戻値：なし
	void SetCountTimer(CountTimer* pCountTimer);

	//カウントタイマーのゲット
		//詳細：時間経過を受け取る
		//引数：なし	
		//戻値：カウントタイマー
	CountTimer* GetCountTimer();

	//UpdateAlgorithm処理開始	を知らせる
		//詳細：タグ読み込みフラグ（isGetTag_）を下す
		//引数：なし
		//戻値：なし
	void StartUpdateAlgorithm();
	//UpdateAlgorithm処理終了	を知らせる
		//詳細：タグ読み込みフラグ（isGetTag_）を立たせる
		//引数：なし
		//戻値：なし
	void EndUpdateAlgorithm();


	//UpdateAlgorithm処理終了　フラグが立っているかを受け取る
		//詳細：タグ読み込みフラグ（isGetTag_）を受け取る
		//　　：フラグが立っている場合：次のタグ読み込む
		//　　：フラグが降りている場合：何もしない
		//引数：なし
		//戻値：タグ読み込みフラグ(isGetTag_)
	bool IsUpdatingAlgorithm();

public : 

	/*連携処理系****************************************************************************/

	/*プレイヤー木*************/
	
	//木オブジェクト（プレイヤー設置）ト、マウスカーソル位置との衝突を検知
		//詳細：マウスカーソル位置からのレイキャストを発生させて、衝突する木オブジェクトを判別する
		//連携先：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：GameSceneInputStrategy
		//引数：なし
		//戻値：衝突した木情報
	TreeInfo RayCastWithAllTreeOwnedByPlayer();

	//引数ポリゴンの真上からレイを飛ばし衝突する木オブジェクトがあるか判定
		//連携先：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：GameSceneInputStrategy
		//引数：真上からレイを飛ばすときの該当ポリゴン番号
		//戻値：衝突したか（衝突：true , 衝突してない：false）
	bool RayCastFromAdoveWithAllTreeOwnedByPlayer(int polyNum);


	//木オブジェクトのHPUP
		//詳細：メンバ変数に保存してある情報から、木オブジェクトを特定し、そのオブジェクトのステータス（HP）を上昇させる
		//　　：引数にて、上昇させる対象のEnum値を判別。
		//　　：途中で対象のオブジェクトが削除されてしまったときに、UIを非表示にする行動を起こす必要がある。
		//連携先：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：GameSceneButtonStrategy
		//引数：HPUPさせる木のパーツ
		//戻値：成長できたか（成長できた：true , 成長できない：false）
	bool GrowTreeParts(TREE_PARTS part);

	//木をはやす（木が生成可能ならば生成呼出し）
		//詳細：すでに生成先を該当変数(lastCollisionPolyNum_)から参照して、該当ポリゴン上に生成
		//連携先：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：GameSceneButtonStrategy
		//引数：木のタイプ（int型にてもらう（BUttonStretegyのEnum値とTreeのEnum値を同様にする））（通常の木、ダミーの木など）
		//戻値：生成できた
	bool GrowTree(int type);

	//木を強制的に倒木させる
		//詳細：参照対象をメンバに保存している木情報（currentTreeInfo_）の木を削除
		//連携先：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：GameSceneButtonStrategy
		//引数：なし
		//戻値：なし
	void ForciblyEraseTheTree();


	//木が存在するポリゴン番号をすべて知る
		//詳細：引数にて渡した、ベクターへ、木が存在するポリゴン番号を代入してもらう
		//　　：ゲームマップを作る際に、木の存在するポリゴン番号を取得する。その際に使用する
		//連携先：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：GameMap(GAME_COOP_GAME_MAP)
		//引数：立地するポリゴン番号群（可変配列のポインタ）
		//引数：木のタイプ群（可変配列のポインタ）
		//戻値：なし
	void GetPolygonsWithTrees(std::vector<int>* polygonsWithTrees, std::vector<int>* treeTypes);

	//木へ攻撃実行（任意ダメージを与える）
		//詳細：引数ポリゴン番号に立っている、植わっている木があるならば、（全ての木群から該当木があるか参照）
		//　　：その木の番号を受け取る
		//　　：その木の番号をもとに、木グループにアクセスして、該当木にダメージを与えるまで至る
		//連携先：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：EnemySpawn(GAME_COOP_ENEMY_SPAWN)
		//引数：ターゲットとなる木がたっているポリゴン番号（敵のナビゲーションの最終目的ポリゴン番号）
		//引数：ターゲットへ与えるダメージ
		//引数：攻撃で木を倒したか（HPを０にしたか）
		//引数：倒した場合、その倒した木の番号
		//戻値：攻撃に成功したか（true : 攻撃成功。攻撃対象が存在する。 false:攻撃失敗。攻撃対象が存在しない）
	bool AttackSuccess(const int TARGET_POLY_NUM, const int DAMAGE, bool* isKill , int* treeNum );


	//*背景木*********************/
	
	//背景となる、木オブジェクトの生成
		//詳細：地面オブジェクトを加工用に、木オブジェクトを配置して、
		//　　：地面の四方を囲むように地面を作る処理
		//　　：地面オブジェクトのWidth , Depthを取得。そのWidth , Depthから囲む木を配置・
		//連携先：GroupOfTree(GAME_COOP_BACK_GROUND_TREE) , GrassLandObject(GAME_COOP_GRASS_LAND)
		//連携元：GameScene
		//引数：なし
		//戻値：なし
	void CreateBackGroundTrees();
	
	
	//*草**********************************/

	//レイキャストで触れた地面のポリゴン番号　にGrassの作成
		//詳細：マウススクリーン座標をクリック時に
		//　　：マウスクリックをクリックしたときに、
		//　　：その方向へ、レイを飛ばして、衝突するポリゴンを求める
		//　　：そのポリゴンを求めたときに
		//　　：そのポリゴンから範囲を広げていく処理を始める
		//引数：なし
		//戻値：なし
	//void RayCastForCreateGrass();

	//草オブジェクトを狭める
		//詳細：木オブジェクトが解放されて
		//　　：木オブジェクトが立地していたポリゴン番号を受け取り、
		//　　：そのポリゴン番号と同様のポリゴンから草オブジェクト（Grass）を広げたオブジェクトを見つけ出し、
		//　　：そのオブジェクトの広げた緑の範囲を狭める
		//　　：GrassLandObjectへの橋渡し
		//連携先：GrassLandObject(GAME_COOP_GRASS_LAND)
		//連携元：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//引数：対象ポリゴン
		//戻値：なし
	void StartShrinkingGrass(int targetPolyNum);

	//地面の合計ポリゴンのうち、Grassが繁殖している数取得
		//詳細：Grassを拡げている、Grassが繁殖しているポリゴンの合計数を取得する
		//　　：各々のGrassにて、重複するポリゴン番号は、合計数にカウントしない
		//　　：メンバに所有している、各Grassクラスの所有しているポリゴンのポリゴン番号とから、最終的に塗られている、ポリゴン番号合計数を取得
		//　　：上記のポリゴン数を取得することで、
		//　　：塗ったポリゴン数 / 全体のポリゴン数　＝ 塗ったポリゴンの範囲、割合を出す
		//連携先：GrassLandObject(GAME_COOP_GRASS_LAND)
		//連携元：PlantDensity
		//引数：なし
		//戻値：なし
	int GetTotalNumberOfPolygonWithGrass();


	//*敵・敵スポーン*******************************/

	//敵オブジェクトと、マウスカーソル位置との衝突を検知
		//連携先：EnemySpawn(GAME_COOP_ENEMY_SPAWN)
		//連携元：GameSceneInputStrategy
		//引数：なし
		//戻値：衝突した敵情報
	EnemyInfo RayCastWithAllEnemyObject();

	//引数敵オブジェクトが敵スポーンに存在するか
		//詳細：現在行動を起こそうとしているオブジェクトが存在するかを調べてもらう
		//　　：存在していたら、行動実行
		//　　：存在していなかったら、行動終了。とともに終了処理
		//連携先：EnemySpawn(GAME_COOP_ENEMY_SPAWN)
		//連携元：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//引数：存在するか調べる敵オブジェクト
		//戻値：存在しているか（存在している：true , 存在していない：false）
	bool IsExistsTarget(GameObject* pTarget);

	//引数敵オブジェクトを強制的に削除する
		//連携先：EnemySpawn(GAME_COOP_ENEMY_SPAWN)
		//連携元：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//引数：削除敵オブジェクト
		//戻値：なし
	void EraseEnemy(GameObject* pTarget);
	//強制的に指定敵オブジェクトを捕縛（動きを停止）させる
		//詳細：捕縛：行動不能、スタン
		//　　：捕縛が起こる条件：バインドの木に触れた。イーター木に触れた。
		//連携先：EnemySpawn(GAME_COOP_ENEMY_SPAWN)
		//連携元：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//引数：捕縛対象のオブジェクト
		//戻値：なし
	void ForciblyBindEnemy(GameObject* pTarget);
	
	//捕縛解除
		//詳細：強制的に捕縛状態となるが、そのバインドが解除される場合がある。→ほかの敵にバインドの木が切り落とされる
		//　　：その場合、捕縛を解除
		//連携先：EnemySpawn(GAME_COOP_ENEMY_SPAWN)
		//連携元：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//引数：捕縛解除対象の敵オブジェクト
		//戻値：なし
	void ForciblyUnBindEnemy(GameObject* pTarget);


	//敵が存在するポリゴン番号をすべて知る
		//詳細：引数にて渡した、ベクターへ、敵が存在するポリゴン番号を代入してもらう
		//　　：ゲームマップを作る際に、敵の存在するポリゴン番号を取得する。その際に使用する
		//連携先：EnemySpawn(GAME_COOP_ENEMY_SPAWN)
		//連携元：GameMap(GAME_COOP_GAME_MAP)
		//引数：敵が存在するポリゴン番号群（可変配列のポインタ）
		//戻値：なし
	void GetPolygonsWithEnemies(std::vector<int>* polygonsWithEnemies);

	//*プレイヤー***************************************/
	//プレイヤーのワールド座標を取得する
		//連携先：Player(GAME_COOP_PLAYER)
		//連携元：NumberPlate
		//引数：なし
		//戻値：プレイヤーのワールド座標
	XMVECTOR GetPlayerWorldPos();
	
	//プレイヤーの移動値を加算する
		//連携先：Player(GAME_COOP_PLAYER)
		//連携元：GameSceneInputStrategy
		//引数：キーコード（DIK_〜〜）
		//戻値：なし
	void MovePlayer(const int KEY_CODE);

	//レイキャスト　プレイヤーと、地面とのレイキャスト実行
		//連携先：Player(GAME_COOP_PLAYER) , GrassLandObject(GAME_COOP_GRASS_LAND)
		//連携元：Player(GAME_COOP_PLAYER)
		//引数：なし
		//戻値：なし
	void RayCastForPlayerAndGround();

	//地面とのレイキャストを行い　結果を引数変数に入れて返す
		//連携先：Player(GAME_COOP_PLAYER)
		//連携元：GameMap(GAME_COOP_GAME_MAP)
		//引数：衝突したか（参照渡し）
		//引数：衝突点との距離（参照渡し）
		//引数：衝突ポリゴン番号
		//戻値：なし
	void RayCastForPlayerAndGround(bool& isHit , float& dist , int& collPolyNum);

	//プレイヤーのY軸回転量を取得する
		//詳細：マップにおけるプレイヤー回転量計算のために取得する
		//連携先：Player(GAME_COOP_PLAYER)
		//連携元：GameMap(GAME_COOP_GAME_MAP)
		//引数：なし
		//戻値：Y軸回転量
	float GetPlayerRotateY();

	
	//*地面***************************************************************************
	//レイキャスト　敵オブジェクトと地面とのレイキャスト実行
		//詳細：引数のオブジェクトと、地面との衝突判定を行う（引数はGameObject型で取得するが、Enemy成分で使用するのは、Transformだけ、Transformだけであれば、GameObject型で十分）
		//　　：衝突判定を行う際に、オブジェクトの現在位置を自身の親のTransformのWorldMatで変形が必要
		//連携先：GrassLandObject(GAME_COOP_GRASS_LAND)
		//連携元：EnemySpawn(GAME_COOP_ENEMY_SPAWN) , EnemyCommandTower
		//引数：敵オブジェクトポインタ
		//戻値：衝突したポリゴン番号
	int RayCastForEnemyAndGround(GameObject* pEnemy);
	
	//地面を作るポリゴン数の合計を取得
		//連携先：GrassLandObject(GAME_COOP_GRASS_LAND)
		//連携元：PlantDensity , GameMap(GAME_COOP_GAME_MAP)
		//引数：なし
		//戻値：地面を作るポリゴン合計数取得
	int GetTotalPolygonCountOfGrassLand();

	//ポリゴン番号のワールド座標を取得する
		//連携先：GrassLandObject(GAME_COOP_GRASS_LAND)
		//連携元：NavigationForEnemy
		//引数：ワールド座標（ポインタ）
		//引数：ポリゴン番号
		//戻値：ワールド座標を取得できた（取得：true , 取得できなかった：false）
	bool GetWorldPosOfPolygonMakeGround(XMVECTOR* localToWorld, int polyNum);

	//地面オブジェクトの横頂点数と奥頂点数を取得
		//連携先：GrassLandObject(GAME_COOP_GRASS_LAND)
		//連携元：GameMap(GAME_COOP_GAME_MAP) , EnemyCommandTower
		//引数：Width 頂点数
		//引数：Depth 頂点数
		//戻値：なし
	void GetVertexesWidthCountAndDepthCount(int* width, int* depth);

	//指定ポリゴンがGrassとして、塗られているか、塗られていないか
		//詳細：指定ポリゴン番号が、黒ポリゴンか、緑ポリゴンか
		//連携先：GrassLandObject(GAME_COOP_GRASS_LAND)
		//連携元：GameMap(GAME_COOP_GAME_MAP) , EnemySpawn(GAME_COOP_ENEMY_SPAWN)
		//引数：指定ポリゴン番号（黒ポリゴンか、緑ポリゴンかを調べるポリゴン）
		//戻値：塗られているか（塗られている：true , 塗られていない：false）
	bool IsGreenPolygon(int polyNum);

	//マウスの座標と地面ポリゴンとの当たり判定
		//詳細：プレイヤーのマウスカーソル位置をワールド座標系に落とし込み、
		//　　：その座標から、直線に線を伸ばして、世界の終端まで線を伸ばし
		//　　：その線をレイとして、
		//　　：レイキャストを実行して、
		//　　：衝突した地面のポリゴンを返す
		//　　：判定相手：GrassLandObjectのGrassLand（地面オブジェクト）
		//連携先：GrassLandObject(GAME_COOP_GRASS_LAND)
		//連携元：GameSceneInputStrategy
		//引数：なし
		//戻値：なし
	int GetMouseCursorCollisionPolygonNumber();

	//*その他****************************************************************************
	//タイトルシーンへ戻る
		//詳細：シーン切替え
		//連携先：SceneChanger
		//連携元：GameSceneButtonStrategy
		//引数：なし
		//戻値：なし
	void BackTitleScene();

	//時間を停止する（世界の停止）	
		//詳細：UI描画時などに、敵オブジェクト、木オブジェクト、残り時間のタイマー、のすべての動的処理を停止する
		//　　：動的処理を行っている、オブジェクトを連携オブジェクトとして取得しているので、各オブジェクトへ時間停止を知らせる
		//連携先：GrassLandObject(GAME_COOP_GRASS_LAND) , EnemySpawn(GAME_COOP_ENEMY_SPAWN) , EnemyStrategyBulletinBoard(GAME_COOP_BULLETIN_BOARD) , GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：GameSceneInputStrategy
		//引数：なし
		//戻値：なし
	void StopTimeInTheWorld();

	//時間計測を再開する（世界の停止）
		//詳細：UI描画時などに、敵オブジェクト、木オブジェクト、残り時間のタイマー、のすべての動的処理を再開する
		//　　：Updateを止める、再開させるではなく、Updateの中の、特定の処理を止めるなどの処理を行う
		//　　：動的処理を行っている、オブジェクトを連携オブジェクトとして取得しているので、各オブジェクトへ時間停止を知らせる
		//連携先：GrassLandObject(GAME_COOP_GRASS_LAND) , EnemySpawn(GAME_COOP_ENEMY_SPAWN) , EnemyStrategyBulletinBoard(GAME_COOP_BULLETIN_BOARD) , GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：GameSceneButtonStrategy
		//引数：なし
		//戻値：なし
	void StartTimeInTheWorld();

	//ゲームマップ生成
		//詳細：GameMapクラスへ、ゲームマップ（現在の地面情報（ポリゴンの塗られ具合）、木の生成状況、敵情報をマップとして合反映させる）
		//　　：ゲームのマップを生成
		//連携先：GameMap(GAME_COOP_GAME_MAP)
		//連携元：GameSceneInputStrategy
		//引数：なし
		//戻値；なし
	void CreateGameMap();
	
	//ゲームマップ終了
		//詳細：GameMapクラスへ、ゲームのマップを閉じるよう伝える
		//連携先：GameMap(GAME_COOP_GAME_MAP)
		//連携元：GameSceneButtonStrategy
		//引数：なし
		//戻値：なし
	void EndGameMap();

	//立地予定の木を描画
		//詳細：地面クリック時に、指定ポリゴンの上に木が生成されるか聞かれる。木生成位置を視認できるように
		//　　：立地予定ポリゴン位置にワイヤーフレームにかたどれられた木オブジェクトを描画させる
		//連携先：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：GameSceneInputStrategy
		//引数：立地予定木が立つポリゴン
		//戻値：なし
	void CreatePlannedLocationOfTree(int polyNum);

	//立地予定木の削除
		//詳細：描画していた、立地予定木の削除
		//連携先：GroupOfTree(GAME_COOP_TREE_OWNED_BY_PLAYER)
		//連携元：GameSceneButtonStrategy
		//引数：なし
		//戻値：なし
	void ErasePlannedLocationOfTree();

	//残り生成可能木本数　テキストを描画許可
		//連携先：GameSceneManager
		//連携元：GameSceneInputStrategy
		//引数：なし
		//戻値：なし
	void VisibleTreeNum();


	/*ユーザー入力クラス（SceneUserInputter）のキー入力　復活、解除**************************************************************************************/
		/*
			今回は、
			キー入力を追加、消去を行うときは、
			必ず、全てのデフォルトキーの追加、
			必ず、全てのデフォルトキーの削除、と追加、消去にて削除するものが同じ。
			そのため、一つの関数でデフォルトキーを扱う関数とする
	
		*/

	//デフォルトのデバイス入力（キー入力、マウス入力）を追加
		//詳細：デフォルトのキー入力（UI非展開時など）をキー入力へ追加
		//　　：通常プレイシーンの入力キーをキー入力クラス（SceneUserInputter）に登録する
		//引数：なし
		//戻値：なし
	void SetStrategyForDefaultEachKey();
	//デフォルトのデバイス入力（キー入力、マウス入力）を消去
		//詳細：デフォルトのキー入力（UI非展開時など）をキー入力から解放
		//　　：通常プレイシーンの入力キーをキー入力クラス（SceneUserInputter）から解放
		//引数：なし
		//戻値：なし
	void EraseStrategyForDefaultEachKey();




};

