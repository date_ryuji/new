#include "GameSceneInputStrategy.h"			//ヘッダ
#include "../../CommonData/GlobalMacro.h"	//共通マクロ
#include "../Enemy/EnemySpawn.h"			//敵オブジェクトのスポーン
#include "../Tree/GroupOfTree.h"			//木オブジェクトの群
#include "GameSceneManager.h"				//シーンマネージャー



/*行動クラス（シーンごとのシーン名SceneInputStrategyを継承したクラス）宣言*******************************************************************************************************/
	//詳細：Factoryクラス（行動クラス生成クラス）にてインスタンスの動的確保を行うため、Factoryの実装よりも前に記述
	//詳細：行動クラスを複数作ることになるため、行動クラスごとにクラスファイルを作成すると、クラスファイル量が多くなる。
	//　　：→上記を避けるために、一つのクラスに行動クラスをまとめる。（メソッドやメンバも少ないため、ひとくくりにしてしまう。）
/*****************************************************************/
/*木作成のためのUI表示などの　アルゴリズム*/
/*CREATE_TREE_ALGORITHM*/
class UIDisplayForCreateTreeStrategy : public GameSceneInputStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//引数：入力情報（入力デバイス、入力コード　など）
		//戻値：なし
	UIDisplayForCreateTreeStrategy(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo);


	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
	
	//UI描画
		//引数：なし
		//戻値：なし
	void UIDisplay();
};
/*****************************************************************/
/*****************************************************************/
/*木情報UI　OR　敵情報UIの表示などの　アルゴリズム*/
/*	SAMPLE_INPUT_ALGORITHM_TREE_INFO_OR_ENEMY_INFO*/
class UIDisplayForTreeInfoOrEnemyInfoStrategy : public GameSceneInputStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//引数：入力情報（入力デバイス、入力コード　など）
		//戻値：なし
	UIDisplayForTreeInfoOrEnemyInfoStrategy(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo);


	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
	
	//UI描画
		//引数：なし
		//戻値：なし
	void UIDisplay();
};
/*****************************************************************/
/*****************************************************************/
/*敵情報UIなどの表示などの　アルゴリズム*/
/*SAMPLE_INPUT_ALGORITHM_ENEMY_INFO*/
class UIDisplayForEnemyInfoStrategy : public GameSceneInputStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//引数：入力情報（入力デバイス、入力コード　など）
		//戻値：なし
	UIDisplayForEnemyInfoStrategy(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
	
	//実行できたか、の真偽値を返す実行
		//引数：なし
		//戻値：実行できたか
	bool ExecutionAlgorithmInterface();

	//UI描画
		//引数：なし
		//戻値：なし
	void UIDisplay();


};
/*****************************************************************/
/*****************************************************************/
/*木情報UIなどの表示などの　アルゴリズム*/
/*SAMPLE_INPUT_ALGORITHM_TREE_INFO*/
class UIDisplayForTreeInfoStrategy : public GameSceneInputStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//引数：入力情報（入力デバイス、入力コード　など）
		//戻値：なし
	UIDisplayForTreeInfoStrategy(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo);


	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;

	//実行できたか、の真偽値を返す実行
		//引数：なし
		//戻値：実行できたか
	bool ExecutionAlgorithmInterface();

	//UI描画
		//引数：なし
		//戻値：なし
	void UIDisplay();
};
/*****************************************************************/
/*****************************************************************/
/*タイトルへ戻るUIなどの表示などの　アルゴリズム*/
/*SAMPLE_INPUT_ALGORITHM_BACK_TITLE_SCENE*/
class UIDisplayForBackTitleSceneStrategy : public GameSceneInputStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//引数：入力情報（入力デバイス、入力コード　など）
		//戻値：なし
	UIDisplayForBackTitleSceneStrategy(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;

	//UI描画
		//引数：なし
		//戻値：なし
	void UIDisplay();
};
/*****************************************************************/
/*****************************************************************/
/*プレイヤー移動　アルゴリズム*/
/*GAME_INPUT_ALGORITHM_MOVE_PLAYER*/
class MovePlayerStrategy : public GameSceneInputStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//引数：入力情報（入力デバイス、入力コード　など）
		//戻値：なし
	MovePlayerStrategy(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo);


	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;

};
/*****************************************************************/
/*****************************************************************/
/*ゲームマップ生成　アルゴリズム*/
/*GAME_INPUT_ALGORITHM_CREATE_GAME_MAP*/
class CreateGameMapStrategy : public GameSceneInputStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//引数：入力情報（入力デバイス、入力コード　など）
		//戻値：なし
	CreateGameMapStrategy(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo);


	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;

	//UI描画
		//引数：なし
		//戻値：なし
	void UIDisplay();
};
/*****************************************************************/




/*行動クラス生成クラス　実装*******************************************************************************************************/
/*Factory*/
//コンストラクタ
FactoryForGameSceneInputStrategy::FactoryForGameSceneInputStrategy()
{
}

//行動クラスのインスタンスを生成する関数
void FactoryForGameSceneInputStrategy::CreateGameSceneInputStrategy(
	GAME_SCENE_INPUT_ALGORITHM type, GameSceneManager* pSceneManager,
	SceneUserInputter* pSceneUserInputter,
	CreateInputStrategyInfo* pCreateInfo)
{

	//適切なシーンで生成されているかの判断
	//条件：現在のシーンマネージャーが引数ポインタと同様でない
	if (!IsExistsSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME, pSceneManager))
	{
		//処理終了
		return;
	}

	//行動クラス（生成クラスを引数typeより識別）を生成
	GameSceneInputStrategy* pInputStrategy =
		CreateInputStrategy(type, pSceneManager, pCreateInfo);

	//条件：行動クラスを生成できたなら
	if (pInputStrategy != nullptr)
	{
		//行動クラスをユーザー入力受付クラスに登録
			//詳細：入力情報（入力デバイス、入力コード）と、その入力時の行動クラスを登録し、
			//　　：入力時の行動実行準備を行う
		RegistrationStrategyInSceneUserInputter(
			pSceneUserInputter, pInputStrategy, pCreateInfo);

	}

	return;
}

//ストラテジークラスを生成の委託
//パターン：FactoryMethod
GameSceneInputStrategy* FactoryForGameSceneInputStrategy::CreateInputStrategy(
	GAME_SCENE_INPUT_ALGORITHM type, GameSceneManager* pSceneManager, CreateInputStrategyInfo* pCreateInfo)
{

	switch (type)
	{
	case GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_CREATE_TREE:
		//行動クラス生成関数を呼び出す
			//必要引数を渡し
			//該当クラスをテンプレートとして渡し、渡したテンプレートの型でインスタンスを生成
		return CreateStrategy<UIDisplayForCreateTreeStrategy>(pSceneManager, pCreateInfo);
	case GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_TREE_INFO_OR_ENEMY_INFO:
		return CreateStrategy<UIDisplayForTreeInfoOrEnemyInfoStrategy>(pSceneManager, pCreateInfo);
	case GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_ENEMY_INFO:
		return CreateStrategy<UIDisplayForEnemyInfoStrategy>(pSceneManager, pCreateInfo);
	case GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_TREE_INFO:
		return CreateStrategy<UIDisplayForTreeInfoStrategy>(pSceneManager, pCreateInfo);
	case GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_BACK_TITLE_SCENE:
		return CreateStrategy<UIDisplayForBackTitleSceneStrategy>(pSceneManager, pCreateInfo);
	case GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER:
		return CreateStrategy<MovePlayerStrategy>(pSceneManager, pCreateInfo);
	case GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_CREAE_GAME_MAP:
		return CreateStrategy<CreateGameMapStrategy>(pSceneManager, pCreateInfo);


	default:
		return nullptr;
		break;
	};

	return nullptr;
}
/*****************************************************************/


/*シーンごとのユーザー入力時の行動クラス　親クラス　実装*******************************************************************************************************/
//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込みにより初期化
GameSceneInputStrategy::GameSceneInputStrategy(
	SceneManagerParent* pSceneManager,
	CreateInputStrategyInfo* pCreateInfo) :

	SceneInputStrategyParent(pSceneManager, pCreateInfo)
{

}
//継承元クラスを各シーンごとの継承先型に変換
GameSceneManager* GameSceneInputStrategy::TransGameSceneManager(SceneManagerParent* pSceneManager)
{
	return (GameSceneManager*)pSceneManager;
}
//世界全体への影響の設定をまとめた関数
void GameSceneInputStrategy::SetImpactInTheWorld()
{
	//全てのキーに対してキー入力をなくす（NULL）をセットする
	SetNullStrategyForAllKey();
	//敵、木などのオブジェクトの動的処理の停止
	SetTimeStopForMoveingObject();
}
//すべてのUI群を非表示
void GameSceneInputStrategy::InvisibleAllUIGroup()
{
	//ゲームシーンマネージャーに登録されている
	//すべてのUIGroupの非表示
	//MAX以外のUIをすべて非表示にするので、登録されているすべてを非表示とする
	InvisibleAllUIGroupExceptArgument(pSceneManager_->GetUIGroupCount());

}
//デバイス入力時　実行する行動
	//レベル：純粋仮想関数
void GameSceneInputStrategy::AlgorithmInterface()
{
}
//デバイス入力時の行動
void GameSceneInputStrategy::SetNullStrategyForAllKey()
{
	/*
	for (int i = 0; i < (int)GAME_SCENE_INPUT_TYPE::GAME_INPUT_MAX; i++)
	{
		//全てのキーに対して、NULLを対応させる
			//GameInputerの行動アルゴリズムをセットする
		//NULL→次のフレームから、
		//　　→行動を無にする
		pGameSceneManager_->SetStrategyForEachKey(GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_NULL, (GAME_SCENE_INPUT_TYPE)i);
	}
	*/


	//すべての入力を削除させる
		//シーンマネージャーを経由して、
		//すべての入力キー、マウスを削除する関数を呼び込む
	
	//GameSceneManager型へ自身インスタンスのManagerをキャスト
	GameSceneManager* pGameSceneManager = TransGameSceneManager(pSceneManager_);
	//すべての入力を削除する関数を呼び込み
		//今回においては、
		//全てのデフォルトキーの消去で実現が可能であるため
		//全てのデフォルトキーの削除を行う関数呼び込み
	pGameSceneManager->EraseStrategyForDefaultEachKey();



}
//世界の動きを止める
void GameSceneInputStrategy::SetTimeStopForMoveingObject()
{
	//停止
		//詳細は関数先にて
	GameSceneManager* pGameSceneManager = TransGameSceneManager(pSceneManager_);
	pGameSceneManager->StopTimeInTheWorld();
}

/*****************************************************************/


/*行動クラス（シーンごとのシーン名SceneInputStrategyを継承したクラス）実装*******************************************************************************************************/

/*****************************************************************/
/*木作成のためのUI表示などの　アルゴリズム*/
/*CREATE_TREE_ALGORITHM*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
UIDisplayForCreateTreeStrategy::UIDisplayForCreateTreeStrategy(
	SceneManagerParent* pSceneManager,
	CreateInputStrategyInfo* pCreateInfo) :

	GameSceneInputStrategy(pSceneManager, pCreateInfo)
{

}

//デバイス入力時　実行する行動
void UIDisplayForCreateTreeStrategy::AlgorithmInterface()
{
	//木の生成条件
	//�@木を生成しようとしている方向にポリゴンがそんざいするか（地面のポリゴンと衝突しているか）
	//�A衝突ポリゴンに対して、真上からその衝突ポリゴンへ向けたレイを飛ばし、
		//そのレイとすでに生成されている木のコライダーと衝突しているものがないか(すでに生成されている木の範囲にはやそうとしていないか)


	//GameSceneManager型へキャスト
		//Managerクラス獲得時点で、
		//GameSceneManager型のインスタンスであることは確認済み
	GameSceneManager* pGameSceneManager = TransGameSceneManager(pSceneManager_);


	//�@
	//地面との当たり判定を行って、衝突したポリゴンを保存
			//衝突していたら、
			//以下の処理へ

			//衝突していなかったら、
			//何もせず終了
	int collisionPolyNum = pGameSceneManager->GetMouseCursorCollisionPolygonNumber();

	//衝突していたら
	if (collisionPolyNum != -1)
	{


		//�A
		//該当ポリゴンの真上からのレイと衝突する木が存在するか
			//上記にてcollisionPolyNumには、衝突したポリゴン番号が入るので、
			//存在しないポリゴン番号が渡されることはない。そのため、関数先にて、ポリゴン番号自体が正しいのかという判定は行わない
		bool isTreeColl = pGameSceneManager->RayCastFromAdoveWithAllTreeOwnedByPlayer(collisionPolyNum);



		//木と衝突しなかったら
		//生成可能
			//生成可能なポリゴンに触れている
		if (!isTreeColl)
		{


			//UI表示と
				//キー入力の消去、行動無へ
			UIDisplayForCreateTreeStrategy::UIDisplay();

			//世界への影響設定
				//キー入力NULL設定
				//動的処理の停止
			SetImpactInTheWorld();

			//残り生成可能木本数　テキスト描画
			pGameSceneManager->VisibleTreeNum();


			{
				//衝突位置へ
					//木を立地予定だとわかるように、
					//ワイヤーフレームのモデルを描画させる
				pGameSceneManager->CreatePlannedLocationOfTree(collisionPolyNum);

			}
		}
		
	}

}

//UI描画
void UIDisplayForCreateTreeStrategy::UIDisplay()
{
	
	//UI群の表示
	//UI群：木生成UI
	VisibleInvertUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_CREATE_TREE);

	//自身以外のUIをすべて非表示
	InvisibleAllUIGroupExceptArgument((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_CREATE_TREE);

}
/*****************************************************************/


/*****************************************************************/
/*木情報UI　OR　敵情報UIの表示などの　アルゴリズム*/
/*	GAME_INPUT_ALGORITHM_TREE_INFO_OR_ENEMY_INFO*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
UIDisplayForTreeInfoOrEnemyInfoStrategy::UIDisplayForTreeInfoOrEnemyInfoStrategy(
	SceneManagerParent* pSceneManager,
	CreateInputStrategyInfo* pCreateInfo) :

	GameSceneInputStrategy(pSceneManager, pCreateInfo)
{
}
//デバイス入力時　実行する行動
void UIDisplayForTreeInfoOrEnemyInfoStrategy::AlgorithmInterface()
{
	//自クラス内に、
	//ボタンのアルゴリズム実行クラスがあるため、
	//そのインスタンスを生成し、呼び込む
	//アルゴリズム作成クラス
	FactoryForGameSceneInputStrategy* pFactory = new FactoryForGameSceneInputStrategy;


	//GameSceneManager型へキャスト
	GameSceneManager* pGameSceneManager = TransGameSceneManager(pSceneManager_);




	//木（プレイヤーの生やした）との当たり判定実行
		//木との当たり判定ができたか
		//できていた場合、木のUIを表示して終了する

	//木UI表示のアルゴリズム実行
		//アルゴリズム内にて、
		//木との当たり判定と同時に、あたっていた場合、UI表示
	{
		//専用のボタンアルゴリズム生成
		//専用型にキャスト
			//ファクトリークラスを使用して、専用クラスの作成
			//第3引数は、必要ないため、nullptrを送る
		UIDisplayForTreeInfoStrategy* pTreeInfo = 
			(UIDisplayForTreeInfoStrategy*)
			pFactory->CreateInputStrategy(
			GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_TREE_INFO, 
			pGameSceneManager , nullptr);
		/*
		UIDisplayForTreeInfoStrategy* pTreeInfo = 
			(UIDisplayForTreeInfoStrategy*)pFactory->
			CreateGameSceneInputStrategy(
				GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_TREE_INFO,
				pGameSceneManager_ , myInputType_);
		*/

		

		//アルゴリズム実行
		if (pTreeInfo->ExecutionAlgorithmInterface())
		{
			//実行出来たら

			//インスタンスの消去
			//クラス内でインスタンスを作成したので、直ぐに解放する
			SAFE_DELETE(pTreeInfo);
			SAFE_DELETE(pFactory);

			//関数終了
			return;
		};


		SAFE_DELETE(pTreeInfo);
	}



	//敵の情報表示が必要ないため、
	//敵との衝突判定の実装をコメントアウト
	/*
	//敵との当たり判定実行
	//敵UI表示のアルゴリズム実行
		//アルゴリズム内にて、
		//敵との当たり判定と同時に、あたっていた場合、UI表示
	{
		//専用のボタンアルゴリズム生成
		//専用型にキャスト
		UIDisplayForEnemyInfoStrategy* pEnemyInfo =
			(UIDisplayForEnemyInfoStrategy*)
			pFactory->CreateInputStrategy(
				GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_ENEMY_INFO,
				pGameSceneManager, nullptr);




		//アルゴリズム実行
		pEnemyInfo->ExecutionAlgorithmInterface();

		//if (pEnemyInfo->ExecutionAlgorithmInterface())
		//{
		//	//実行出来たら

		//	//インスタンスの消去
		//	//クラス内でインスタンスを作成したので、直ぐに解放する
		//	SAFE_DELETE(pEnemyInfo);
		//	SAFE_DELETE(pFactory);

		//	//関数終了
		//	return;
		//};

		SAFE_DELETE(pEnemyInfo);
		SAFE_DELETE(pFactory);
	}
	*/

}

//UI描画
void UIDisplayForTreeInfoOrEnemyInfoStrategy::UIDisplay()
{
}
/*****************************************************************/



/*****************************************************************/
/*敵情報UIなどの表示などの　アルゴリズム*/
/*GAME_INPUT_ALGORITHM_ENEMY_INFO*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
UIDisplayForEnemyInfoStrategy::UIDisplayForEnemyInfoStrategy(
	SceneManagerParent* pSceneManager,
	CreateInputStrategyInfo* pCreateInfo) :

	GameSceneInputStrategy(pSceneManager, pCreateInfo)
{
}

//デバイス入力時　実行する行動
void UIDisplayForEnemyInfoStrategy::AlgorithmInterface()
{
	//実行
	ExecutionAlgorithmInterface();

}

//デバイス入力時　実行する行動
bool UIDisplayForEnemyInfoStrategy::ExecutionAlgorithmInterface()
{
	//GameSceneManager型へキャスト
	GameSceneManager* pGameSceneManager = TransGameSceneManager(pSceneManager_);


	//サンプルシーンマネージャーに
	//クリックがされたら
	//クリック位置から、衝突する敵オブジェクトを探し
	//衝突したオブジェクトの情報をもらう
	EnemyInfo info = pGameSceneManager->RayCastWithAllEnemyObject();

	//敵オブジェクトと衝突することが確認出来たら
		//衝突したオブジェクトの番号がいるならば、 -1でないならば
	if (info.number != -1)
	{
		//UIの表示
		UIDisplayForEnemyInfoStrategy::UIDisplay();

		//世界への影響設定
			//キー入力NULL設定
			//動的処理の停止
		SetImpactInTheWorld();


		return true;
	}

	return false;
}

//UI描画
void UIDisplayForEnemyInfoStrategy::UIDisplay()
{
	//UI群の表示
	//UI群：敵情報UI
	VisibleInvertUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_ENEMY_INFO);

	//自身以外のUIをすべて非表示
	InvisibleAllUIGroupExceptArgument((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_ENEMY_INFO);

}
/*****************************************************************/

/*****************************************************************/
/*木情報UIなどの表示などの　アルゴリズム*/
/*GAME_INPUT_ALGORITHM_TREE_INFO*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
UIDisplayForTreeInfoStrategy::UIDisplayForTreeInfoStrategy(
	SceneManagerParent* pSceneManager,
	CreateInputStrategyInfo* pCreateInfo) :

	GameSceneInputStrategy(pSceneManager, pCreateInfo)
{
}

//デバイス入力時　実行する行動
void UIDisplayForTreeInfoStrategy::AlgorithmInterface()
{
	//実行
	ExecutionAlgorithmInterface();
}
//デバイス入力時　実行する行動
bool UIDisplayForTreeInfoStrategy::ExecutionAlgorithmInterface()
{
	//GameSceneManager型へキャスト
	GameSceneManager* pGameSceneManager = TransGameSceneManager(pSceneManager_);


	//サンプルシーンマネージャーに
		//クリックがされたら
		//クリック位置から、衝突する木オブジェクトを探し
		//衝突したオブジェクトの情報をもらう
	TreeInfo info = pGameSceneManager->RayCastWithAllTreeOwnedByPlayer();

	//木オブジェクトと衝突することが確認出来たら
		//衝突したオブジェクトの番号がいるならば、 -1でないならば
	if (info.number != -1)
	{
		//UIの表示
		UIDisplayForTreeInfoStrategy::UIDisplay();

		//世界への影響設定
			//キー入力NULL設定
			//動的処理の停止
		SetImpactInTheWorld();

		return true;
	}

	return false;


}
//UI描画
void UIDisplayForTreeInfoStrategy::UIDisplay()
{
	//UI群の表示
	//UI群：敵情報UI
	VisibleInvertUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_TREE_INFO);

	//自身以外のUIをすべて非表示
	InvisibleAllUIGroupExceptArgument((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_TREE_INFO);
}
/*****************************************************************/


/*****************************************************************/
/*タイトルへ戻るUIなどの表示などの　アルゴリズム*/
/*GAME_INPUT_ALGORITHM_BACK_TITLE_SCENE*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
UIDisplayForBackTitleSceneStrategy::UIDisplayForBackTitleSceneStrategy(
	SceneManagerParent* pSceneManager,
	CreateInputStrategyInfo* pCreateInfo) :

	GameSceneInputStrategy(pSceneManager, pCreateInfo)
{

}
//デバイス入力時　実行する行動
void UIDisplayForBackTitleSceneStrategy::AlgorithmInterface()
{
	//UI表示
	UIDisplay();

	//世界への影響設定
			//キー入力NULL設定
			//動的処理の停止
	SetImpactInTheWorld();


	//GameSceneManager型へキャスト
	GameSceneManager* pGameSceneManager = TransGameSceneManager(pSceneManager_);
	//ヘルプ画像群の作成
	pGameSceneManager->CreateSettingHelpImages();


}
//UI描画
void UIDisplayForBackTitleSceneStrategy::UIDisplay()
{
	//UI群の表示
	//UI群：敵情報UI
	VisibleInvertUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_BACK_TITLE_SCENE);

	//自身以外のUIをすべて非表示
	InvisibleAllUIGroupExceptArgument((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_BACK_TITLE_SCENE);
}
/*****************************************************************/

/*****************************************************************/
/*プレイヤー移動　アルゴリズム*/
/*GAME_INPUT_ALGORITHM_MOVE_PLAYER*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
MovePlayerStrategy::MovePlayerStrategy(
	SceneManagerParent* pSceneManager,
	CreateInputStrategyInfo* pCreateInfo) :

	GameSceneInputStrategy(pSceneManager, pCreateInfo)
{
}

//デバイス入力時　実行する行動
void MovePlayerStrategy::AlgorithmInterface()
{
	//GameSceneManager型へキャスト
	GameSceneManager* pGameSceneManager = TransGameSceneManager(pSceneManager_);
	//入力されたキーによって、
		//移動値を判別して、プレイヤーの移動量を加算させる
	pGameSceneManager->MovePlayer(pCreateInfo_->CODE);
}
/*****************************************************************/

/*****************************************************************/
/*ゲームマップ生成　アルゴリズム*/
/*GAME_INPUT_ALGORITHM_CREATE_GAME_MAP*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
CreateGameMapStrategy::CreateGameMapStrategy(
	SceneManagerParent* pSceneManager,
	CreateInputStrategyInfo* pCreateInfo) :

	GameSceneInputStrategy(pSceneManager, pCreateInfo)
{
}

//デバイス入力時　実行する行動
void CreateGameMapStrategy::AlgorithmInterface()
{
	//GameSceneManager型へキャスト
	GameSceneManager* pGameSceneManager = TransGameSceneManager(pSceneManager_);
	//ゲームマップ生成呼び込み
	pGameSceneManager->CreateGameMap();

	//世界への影響設定
		//キー入力NULL設定
		//動的処理の停止
	SetImpactInTheWorld();

	//自身のUI表示と、
	//自身以外のUI非表示
	UIDisplay();

}

//UI描画
void CreateGameMapStrategy::UIDisplay()
{
	//UI群の表示
	//UI群：敵情報UI
	VisibleInvertUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_GAME_MAP);

	//自身以外のUIをすべて非表示
	InvisibleAllUIGroupExceptArgument((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_GAME_MAP);
}

/*****************************************************************/

