#include "GameScene.h"								//ヘッダ
#include "../../Engine/DirectX/Direct3D.h"			//Direct3D　
#include "../../CommonData/GlobalMacro.h"			//共通マクロ
#include "../../Engine/DirectX/Camera.h"			//カメラ
#include "../../CommonData/GameDatas.h"				//ゲーム内情報定義
#include "../../Engine/GameObject/SkyBox.h"			//360度オブジェクト
#include "../../Engine/Image/Image.h"				//画像クラス
#include "../../Engine/Collider/SphereCollider.h"	//当たり判定　球コライダー
#include "../../Engine/Collider/BoxCollider.h"		//当たり判定　箱コライダー
#include "../../Engine/Audio/Audio.h"				//サウンドクラス(音源再生)
#include "../../Engine/GameObject/UIGroup.h"		//UIGroup群
#include "../../Engine/Scene/SceneUIManager.h"		//シーンのUIマネージャー
#include "../../Engine/Scene/SceneButtonManager.h"	//ボタン管理クラス
#include "GameSceneManager.h"						//ゲームシーンのマネージャークラス
#include "../../Engine/Scene/SceneUserInputter.h"	//ユーザー入力クラス
#include "GameSceneButtonStrategy.h"				//シーンのボタン押下時の行動アルゴリズムを生成するクラス
#include "StageData.h"								//ゲーム内ステージデータ
#include "../../Engine/Button/Button.h"				//ボタンクラス
#include "../Enemy/EnemySpawn.h"					//敵クラスのスポーン
#include "../Player/Player.h"						//プレイヤー
#include "../GrassLand/GrassLandObject.h"			//動的にUVを切り替えるハイトマップ
#include "../Tree/GroupOfTree.h"					//木オブジェクトのグループ
#include "../PlantDensity/PlantDensity.h"			//密度をテキスト描画するクラス
#include "../GameMap/GameMap.h"						//ゲームマップ
#include "../Tree/PlannedLocationOfTree.h"			//立地予定木



//コンストラクタ
//SceneParentのコンストラクタ呼び込み
	//引数：親オブジェクト
	//引数：シーン名のオブジェクト名（シーン識別情報群(Project/CommonDatas/SceneType.h)よりシーンオブジェクト名を取得する（シーンをID識別しオブジェクト名取得））
GameScene::GameScene(GameObject * parent)
	: SceneParent(parent, SCENE_OBJECT_NAME[(int)SCENE_ID::SCENE_ID_GAME]),
	hAudio_(-1),
	hImage_(-1) , 
	pPlayer_(nullptr)
{
}



/*
	//オブジェクトの描画順番
		//透過処理

	//透過の処理を行うとき、
	//出現の順番を考えなくてはいけない　→　正しくは、描画の順番
	//Instantiateする順番を考えないと、
	//透過がうまくいかない　→このうまくいかないというのは、　
	//仮に　Water　a = 0.5f
	//  Ground a = 1.0f の順番で描画した場合、
	//Waterの透過部分の下に、Groundのオブジェクトが仮にあったとしても、
	//Groundの色を込みした透過にはならず、
	//背景の色を映してしまう。

	//そのため、　Waterの下に、Groundの色を載せたいのであれば、
	//Ground , Waterの順番で描画をしなくてはいけない。
	//Waterの段階で、　今ある描画の色に自身の色を重ねる（αを含んだ計算）をするため、　のちの描画するピクセルが、　その透過のピクセルよりも遠ければ、　Zバッファの関係で映らなくなる。
	//そうすれば、　Groundの色が反映されないのは当然
*/

//初期化
void GameScene::Initialize()
{
	//視野距離
	//カメラの視認できる距離を変更する
	//引数：１０ｃｍ　から
	//引数：５００　ｍ　まで
	Camera::SetCameraFieldOfView(0.1f, 500.f);

	//背景色の変化
	//背景色取得
	GameDatas::SetGameBackGroundColor(BACKGROUND_COLOR_LIGHTGREEN);

	//ステージ情報の初期化
	StageData::Initialize();

	//ゲームプレイシーンクラスのマネージャー
		//詳細：SceneParentを継承しているため、
		//　　：継承元関数から、シーンマネージャークラスを作成させる
		//制限　　：シーンマネージャークラスは、GameObject型継承のオブジェクト作成関数である、Instantiateでの作成は推奨されない。
		//制限理由：制限をかけるのは、シーンマネージャーが作成されたことを伝えなくてはいけないクラスが存在するためである。
	GameSceneManager* pSceneManager = (GameSceneManager*)CreateSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME);


	//キー、マウス入力と入力時の行動ストラテジーを作成する
	CreateInputStrategy(pSceneManager);
	

	//地面オブジェクトの情報を格納する変数
	int width = -1;
	int depth = -1;
	int size = -1;

	GrassLandObject* pGrassLandObject = nullptr;
	{
		//地面オブジェクト
			//詳細：地面オブジェクトをヘイトマップ画像から作成
		pGrassLandObject = (GrassLandObject*)Instantiate<GrassLandObject>(this);

		//地面のWidth , Depthを取得
		pGrassLandObject->GetWidthAndDepth(&width, &depth);
		//地面のポリゴンのサイズを取得
			//詳細：上記の値が、ひとつのポリゴンのサイズになる。
			//　　：(原点の（０，０）から　上記のサイズが、ポリゴン幅、奥行き分（奥行きは、配列上では＊２の奥行きを持っているが、ゲーム上では一行に2行分のポリゴンが並んでいる）並んでいる)
			//　　：正確な並んでいるポリゴン数は少し計算のためにずれがあるが、スカイボックスの位置を調整する際には、正確なポリゴン数ではなく、おおよその数が分かれば十分
		size = (int)pGrassLandObject->GetPolygonSize();
	
	
		{
			//背景用　木オブジェクトのグループ
				//詳細：背景用として、地面オブジェクトを囲う木オブジェクトの作成
			GroupOfTree* pGroup = (GroupOfTree*)Instantiate<GroupOfTree>(this);
			//木オブジェクトのタイプを設定（背景用）
			pGroup->Initialize(GAME_SCENE_COOP_OBJECTS::GAME_COOP_BACK_GROUND_TREE);
			//シーンマネージャーに背景用の地面オブジェクトを囲む木オブジェクトを作成連携指示
				//　　：地面オブジェクトのサイズを考慮した背景の木オブジェクトを配置
			pSceneManager->CreateBackGroundTrees();
		}
	
	}


	



	{
		//プレイヤー
			//詳細：プレイヤーと同時にカメラコントローラーの作成
		pPlayer_ = (Player*)Instantiate<Player>(this);
	}

	{
		//360度背景（SkyBox）
			//詳細：背景としてCubeMapのテクスチャを考慮した360度のテクスチャを描画するオブジェクト
			//　　：球体のモデルの裏面に360度テクスチャを描画させる。その球体の中心にカメラを配置することで、360度のテクスチャが張られた空間二オブジェクト、世界が形成されるようにする。
		SkyBox* pSkyBox = (SkyBox*)Instantiate<SkyBox>(this);


		//SkyBoxにおける拡大値
		static constexpr float SKYBOX_SCALE_Y = 100.f;

		//SkyBoxのサイズを変更
			//地面の横幅、縦幅を考慮したサイズに可変
		pSkyBox->SetSkyBoxScale(XMVectorSet((float)(size * width), SKYBOX_SCALE_Y, (float)(size * depth), 0.f));

		//移動
			//詳細：地面オブジェクトの中心に移動
			//　　：拡大率も考えた位置調整をする必要がある（特にY座標）
			//　　：ｘ: ポリゴンサイズ（ワールドにおける幅（奥も同様）スケール）＊　地面のポリゴン幅の半分　＝　地面のＸの内の半分の位置へ
			//　　：ｙ: ポリゴンサイズ（ワールドにおける幅（奥も同様）スケール）＊　地面のポリゴン奥の半分　＝　地面のＺの内の半分の位置へ
		pSkyBox->SetSkyBoxPosition(XMVectorSet(size * (width / 2.f), -(SKYBOX_SCALE_Y / 10.f), size * (depth / 2.f), 0.f));

	}

	{
		//プレイヤー専用の木グループクラスの作成
			//詳細：プレイヤー操作によって、生成された木を所有する木グループクラス
			//　　：SkyBoxとの描画順の調整のために後から作成する
		pPlayer_->CreateTreeOwnedByPlayer();
		//プレイヤーの座標を設定
			//詳細：本来は、地面オブジェクトを作成したのち、、ポリゴンの中心の座標を取得し、その座標へ、つまり、地面の中心に移動させる
		pPlayer_->transform_.position_.vecX = width / 3.f;
		pPlayer_->transform_.position_.vecZ = depth / 3.f;
	}

	{
		//植物密度テキスト描画クラス
		Instantiate<PlantDensity>(this);
	}
	{
		/*
		背景透過のために

		//描画順の関係で（透過率が設定された画像を描画すると、後に描画すると、前のピクセルに透過のピクセルを重ねて、透過処理が可能
		//しかし、先に描画すると、透過処理が適用されないため、　オブジェクトの描画の前後はとても重要になる）
		//描画順を変えるために、
		//地面のポリゴンを一度親から削除して、
		//また親に登録しなおす
		//→すると、先に、背景用の木オブジェクトが０番目のオブジェクトとなり、一番最初にDrawされる
		//→描画における透過処理のための、描画順を設定することができた
		*/

		//親切り替え	
			//引数：切り替え先の親
		pGrassLandObject->ChangeParent(this);


	}


	{
		//敵スポーン
			//詳細：敵オブジェクトの生成を委託する。消去を委託する。
		EnemySpawn* pEnemySpawn = (EnemySpawn*)Instantiate<EnemySpawn>(this);
	}

	{
		//ステージの壁の当たり判定設定の作成
			//詳細：ステージの地面を囲う当たり判定を作成し、プレイヤーが地面外へ出ないような当たり判定を作る
			//引数：コライダーを描画するか
		CreateWallCollider(VISIBLE_COLLIDER, width, depth);
	}



	{
		//シーンの常駐　UIGroup作成
			/*
				常駐UI作成順番

				//シーンのUIのオブジェクトは、基本的に、
				//他オブジェクトよりも、あとに宣言するようにする
				//→宣言を後にすることで、描画もあと周りになる。
				//→描画を後回しにすると、
					//→仮にpngの背景透過の画像であったとき、背景透過などのα値の設定は、すでに描画されているピクセル値に対して、α値のに対して、色を重ねることで背景透過を実現している
					//→（背景透過＝詳細を言うと、ピクセルの色の混合によるブレンドの技術で実現している）

				//以上をもとに、UI面々の背景透過を行うようなオブジェクトは、他オブジェクトよりも後に宣言する

			*/
		CreateSceneUIGroups(pSceneManager);
	}

	{
		//ゲームマップの作成
			//詳細：ゲーム内マップを、他オブジェクト作成後に作成
		Instantiate<GameMap>(this);
	}

	{
		//メインBGMのロード
		hAudio_ = Audio::Load("Assets/Scene/Sound/GameScene/木漏れ日.wav");
		//警告
		assert(hAudio_ != -1);
		//メインBGMのループ設定
		Audio::InfiniteLoop(hAudio_);
		//メインBGMの再生
		Audio::Play(hAudio_);
	}

}

//更新
void GameScene::Update()
{
}

//描画
void GameScene::Draw()
{
}

//解放
void GameScene::Release()
{
	//自身でシーンのマネージャークラスを作成させたので
		//作成と同時に、シーンチェンジャーに登録も行ったため、
		//登録した、マネージャークラスを解放させる処理
	ReleaseSceneManager();

	//ステージ情報の解放
	StageData::Release();

	//音楽停止
	Audio::Stop(hAudio_);

}


//シーン内仕様の初期デバイス入力の登録
void GameScene::CreateInputStrategy(GameSceneManager* pSceneManager)
{
	//ユーザー入力設定
	{
		//ユーザー入力クラス作成
		SceneUserInputter* pSceneUserInputter =
			(SceneUserInputter*)Instantiate<SceneUserInputter>(this);

		//入力クラスをゲームマネージャーに登録
		{
			//シーンマネージャーへ自身のインスタンスを知らせる
				//引数：連携オブジェクトのタイプ
				//引数：連携オブジェクトのポインタ
			pSceneManager->AddCoopObject(
				(int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GAME_INPUTER, (GameObject*)pSceneUserInputter);
		}





		/*

		初期のキーと行動

		上記は、
		ButtonStrategy実行後に、必ず、キーを戻すとして、
		ButtonStrategyの中に、デフォルトのキーを戻す関数内にて、戻し作業を行っている。
			//上記をもとに、
				その関数と同じ処理を、Scene内で行うのであれば、あらかじめ、別のところに、適用すするボタンを保存しておき、
					//それを読み込んでデフォルトのキー設定としたい

		今回は、ButtonStrategy内にて、持っておけば十分だと思うが、
		本来であれば、ヘッダーなどの共通部分に、デフォルトのキー群として持たせておくべき

		*/
		//SceneManager内にて、
		//キー追加の関数を用意する。
			//その関数へ必要情報を渡してキー入力登録とする
		//そのうえでデフォルトのキー入力を持たせておきたいため、ヘッダなどで持っておこうという話になった
			//そして、そのほか登録に必要な情報、クラスは、その場で用意可能


		//シーンマネージャーへ
		//デフォルト時のキー、マウス入力を追加させる
		pSceneManager->SetStrategyForDefaultEachKey();

	}

}



//ステージの壁の当たり判定設定
void GameScene::CreateWallCollider(bool isDrawCollider, int width, int depth)
{
	//ステージの壁の当たり判定設定
	{
		/*
		//背景用の木オブジェクトを作成したため
		//その背景用の木オブジェクトを囲む、コライダーを配置させる
		//そのコライダーが、プレイヤーとの当たり判定のコライダーとなり、
		//地面を囲む木オブジェクトにコライダーがあることで、
		//地面の周りに木によって、壁が作られている表現を作り、地面の奥には行けない表現を行う

		//そのコライダーを　、　地面オブジェクトの上下左右に配置
		//箱コライダーの作成
		*/


		/*
		
			//-Z方向をUp
			//+Z方向をDown
			//-X方向をRight
			//+X方向をLeft

		*/

		//定数値
		//コライダーWidth
			//おおよそ、Width数の2倍の長さ、サイズ(ここにおける2倍とは、ポリゴンのサイズ)
		static const float WIDTH_SIZE = width * 2.f;		
		//コライダーDepth
			//WIDTHと同様に、Depth数の2倍の長さ、サイズ
		static const float DEPTH_SIZE = depth * 2.f;		
														

		//Upの壁,つなぎ目用の位置調整用の値
			//上下のZ軸調整用の値
		static constexpr float ADJUSTMENT_UP_AND_RIGHT_Z = 2.f;	
		//Downの壁
		static constexpr float ADJUSTMENT_DOWN_AND_LEFT_Z = ADJUSTMENT_UP_AND_RIGHT_Z * 2.f;


		//壁
		{
			//Y軸の拡大値
			static constexpr float COMMON_HEIGHT_SIZE = 10.f;	//共通
			//壁のローカルY座標
			static constexpr float WALL_COMMON_LOCAL_POS_Y = -2.f;	//共通

			//Up
			BoxCollider* pBoxCollUp = new BoxCollider(XMVectorSet((WIDTH_SIZE / 2.f), WALL_COMMON_LOCAL_POS_Y, ADJUSTMENT_UP_AND_RIGHT_Z, 0.0f), XMVectorSet(WIDTH_SIZE, COMMON_HEIGHT_SIZE, 1.0f, 0.0f), isDrawCollider);
			//自身のゲームオブジェクト内のコライダー群に追加
			AddCollider(pBoxCollUp);	

			//Down
				//奥にずらす大きさは、DEPTHのサイズ　＝LEFTやRIGHTにて、コライダーのポジションのZ値に指定している　サイズ
			BoxCollider* pBoxCollDown = new BoxCollider(XMVectorSet((WIDTH_SIZE / 2.f), WALL_COMMON_LOCAL_POS_Y, DEPTH_SIZE - ADJUSTMENT_DOWN_AND_LEFT_Z, 0.0f), XMVectorSet(WIDTH_SIZE, COMMON_HEIGHT_SIZE, 1.0f, 0.0f), isDrawCollider);
			//自身のゲームオブジェクト内のコライダー群に追加
			AddCollider(pBoxCollDown);	

			//Right
			BoxCollider* pBoxCollLeft = new BoxCollider(XMVectorSet(ADJUSTMENT_UP_AND_RIGHT_Z, WALL_COMMON_LOCAL_POS_Y, (DEPTH_SIZE / 2.f), 0.0f), XMVectorSet(1.f, COMMON_HEIGHT_SIZE, DEPTH_SIZE, 0.0f), isDrawCollider);
			//自身のゲームオブジェクト内のコライダー群に追加
			AddCollider(pBoxCollLeft);	

			//Left
			BoxCollider* pBoxCollRight = new BoxCollider(XMVectorSet(WIDTH_SIZE - ADJUSTMENT_DOWN_AND_LEFT_Z, WALL_COMMON_LOCAL_POS_Y, (DEPTH_SIZE / 2.f), 0.0f), XMVectorSet(1.0f, COMMON_HEIGHT_SIZE, DEPTH_SIZE, 0.0f), isDrawCollider);
			//自身のゲームオブジェクト内のコライダー群に追加
			AddCollider(pBoxCollRight);

		}


		//壁のつなぎ目
		{
			//半径
				//サイズが小さすぎると埋まってしまうため、サイズをある程度大きくする
			static constexpr float RADIUS = 4.f;
			//共通ローカルY座標
			static constexpr float COMMON_LOCAL_Y = -5.f;


			//壁コライダーのつなぎ目に配置する球体コライダー
			//UpRight
				//引数：Right
				//引数：共通Y座標
				//引数：Up
			SphereCollider* pSphereCollUpRight = new SphereCollider(XMVectorSet(ADJUSTMENT_UP_AND_RIGHT_Z, COMMON_LOCAL_Y, ADJUSTMENT_UP_AND_RIGHT_Z, 0.f), RADIUS, isDrawCollider);
			AddCollider(pSphereCollUpRight);

			//DownRight
				//引数：Right
				//引数：共通Y座標
				//引数：Down
			SphereCollider* pSphereCollDownRight = new SphereCollider(XMVectorSet(ADJUSTMENT_UP_AND_RIGHT_Z, COMMON_LOCAL_Y, DEPTH_SIZE - ADJUSTMENT_DOWN_AND_LEFT_Z, 0.f), RADIUS, isDrawCollider);
			AddCollider(pSphereCollDownRight);

			//UpLeft
				//引数：Left
				//引数：共通Y座標
				//引数：Up
			SphereCollider* pSphereCollUpLeft = new SphereCollider(XMVectorSet(WIDTH_SIZE - ADJUSTMENT_DOWN_AND_LEFT_Z, COMMON_LOCAL_Y, ADJUSTMENT_UP_AND_RIGHT_Z, 0.f), RADIUS, isDrawCollider);
			AddCollider(pSphereCollUpLeft);

			//DownLeft
				//引数：Left
				//引数：共通Y座標
				//引数：Down
			SphereCollider* pSphereCollDownLeft = new SphereCollider(XMVectorSet(WIDTH_SIZE - ADJUSTMENT_DOWN_AND_LEFT_Z, COMMON_LOCAL_Y, DEPTH_SIZE - ADJUSTMENT_DOWN_AND_LEFT_Z, 0.f), RADIUS, isDrawCollider);
			AddCollider(pSphereCollDownLeft);
		}
	}

}

//ボタンのサイズと位置をセットする
void GameScene::SetButtonPositionAndScale(Button* pButton, 
	const int POS_X, const int POS_Y, const int SCALE_X, const int SCALE_Y)
{
	//ボタンの位置、サイズ変更
	pButton->SetPixelPosition(POS_X, POS_Y);
	pButton->SetPixelScale(SCALE_X, SCALE_Y);

}

//戻るボタン作成
void GameScene::CreateBackButton(const std::string BUTTON_IAMGE_ROOT, 
	UIGroup* pUIGroup, GameSceneManager* pSceneManager, GAME_SCENE_BUTTON_ALGORITHM algorithm, 
	const int POS_X, const int POS_Y)
{

	//ボタンデザイン画像ファイル名
	const std::string FILE_NAME = BUTTON_IAMGE_ROOT + "CircleBack.png";
	
	//ボタン作成
	Button* pButton =
		CreateButton(
			FILE_NAME,
			pUIGroup, pSceneManager,
			algorithm);
	//位置、サイズ調整
	SetButtonPositionAndScale(pButton,
		POS_X, POS_Y, 100, 100);
}

//ボタンクラス作成
Button* GameScene::CreateButton(const std::string& fileName,
	UIGroup* pUIGroup, GameSceneManager* pSceneManager,
	GAME_SCENE_BUTTON_ALGORITHM algorithmType)
{
	//ボタンインスタンス
	Button* pButton = (Button*)Instantiate<Button>((GameObject*)pUIGroup);
	//引数：デザインのロード
	//引数：画像を設定するボタン（ON or OFF）
	//引数：押下タイプ
	pButton->Initialize(fileName, BUTTON_STATUS::BUTTON_STATUS_OFF, BUTTON_PUSH_TYPE::PUSH_TYPE_DOWN);

	//ボタン生成
	//ボタン押下時の行動作成
	AddStackButton(pUIGroup, pButton, pSceneManager, algorithmType);

	return pButton;
}

//シーン内常駐UIGroupの作成
void GameScene::CreateSceneUIGroups(GameSceneManager * pSceneManager)
{
	//ボタンデザイン画像のルートパス
	static const std::string BUTTON_IMAGE_ROOT = "Assets/Scene/Image/GameScene/Button/";



	//ゲームシーンにて扱う、UI群
	//UI,Buttonをひとまとめにして、表示、非表示、実行、非実行の管理
	//UIGroupごとに、SceneUIManager,SceneButtonManagerを所有して、
	//UIGroupごとに、UI,Buttonを所有する

	//（UI画像）UIとなる画像のファイルへのパスをUIGroupの引数として、関数先でロードさせる
	//（Button）Buttonクラスの親を、UIGroupとして作成して、UIGroupの関数を使い、UIGroupにセットさせる
	{
		//木生成UI群
		{
			//UIGroup作成
			//インスタンス生成
			UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

			//UIGroupをSceneMnagerにセットする
			//SAMPLE_UI_GROUP_CREATE_TREE
			//木生成UI群
			pSceneManager->AddUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_CREATE_TREE, uiGroup);


			//アイコンのサイズ
			static constexpr int TREE_SCALE_X = 150;
			static constexpr int TREE_SCALE_Y = 150;
			static constexpr int TREE_INFO_SCALE_X = 250;




			//木UI背景の生成位置
			static constexpr int TREE_POS_X = 500;
			static constexpr int TREE_POS_Y = 300;
			//木UIにおける情報UI　X位置
			static constexpr int TREE_INFO_POS_X = 300;
			//通常
			static constexpr int TREE_DEF_POS_Y = 100;
			//ダミー
			static constexpr int TREE_DUM_POS_Y = 260;
			//バインド
			static constexpr int TREE_BIN_POS_Y = 420;
			//イーター
			static constexpr int TREE_EAT_POS_Y = 580;






			//UI群となる、
			//UI画像、Buttonの生成

			//UI画像
			{
				//木生成　背景
				//ロードと追加
				int hBackGround = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/Signboard/Signboard.png");

				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();
				{
					//位置の移動
					pSceneUIManager->SetPixelPosition(hBackGround , TREE_POS_X - 120 , TREE_POS_Y);
					//奥へ移動
					pSceneUIManager->LowerOneStep(hBackGround);
				}

				//通常木説明
				{
					int hInfo = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/TreeInfo/InfoTreeDefault.png");

					//位置の移動
					pSceneUIManager->SetPixelPosition(hInfo, TREE_INFO_POS_X, TREE_DEF_POS_Y);
					//サイズの可変
					pSceneUIManager->SetPixelScale(hInfo, TREE_INFO_SCALE_X, TREE_SCALE_Y);
				}
				//ダミー木説明
				{
					int hInfo = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/TreeInfo/InfoTreeDummy.png");

					//位置の移動
					pSceneUIManager->SetPixelPosition(hInfo, TREE_INFO_POS_X, TREE_DUM_POS_Y);
					//サイズの可変
					pSceneUIManager->SetPixelScale(hInfo, TREE_INFO_SCALE_X, TREE_SCALE_Y);
				}
				//バインド木説明
				{
					int hInfo = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/TreeInfo/InfoTreeBind.png");

					//位置の移動
					pSceneUIManager->SetPixelPosition(hInfo, TREE_INFO_POS_X, TREE_BIN_POS_Y);
					//サイズの可変
					pSceneUIManager->SetPixelScale(hInfo, TREE_INFO_SCALE_X, TREE_SCALE_Y);
				}
				//イーター木説明
				{
					int hInfo = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/TreeInfo/InfoTreeEater.png");

					//位置の移動
					pSceneUIManager->SetPixelPosition(hInfo, TREE_INFO_POS_X, TREE_EAT_POS_Y);
					//サイズの可変
					pSceneUIManager->SetPixelScale(hInfo, TREE_INFO_SCALE_X, TREE_SCALE_Y);
				}



			}

			//Button
			{
				//（Start）木生成ボタン
				//UIグループ：木生成UI群
				//主な行動：木生成
				{
					//ボタンデザイン画像ファイル名
					const std::string FILE_NAME = BUTTON_IMAGE_ROOT + "PlaneCreateTreeSample.png";

					//ボタン作成
					Button* pButton = 
						CreateButton(
						FILE_NAME ,
						uiGroup , pSceneManager , 
							GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_TREE);

					//位置、サイズ調整
					SetButtonPositionAndScale(pButton , 
						TREE_POS_X, TREE_DEF_POS_Y, TREE_SCALE_X, TREE_SCALE_Y);

				}
				//ダミー木生成ボタン
				//UIグループ：木生成UI群
				//主な行動：ダミー木生成
				{
					//ボタンデザイン画像ファイル名
					const std::string FILE_NAME = BUTTON_IMAGE_ROOT + "PlaneCreateDummyTreeSample.png";
					//ボタン作成
					Button* pButton =
						CreateButton(
							FILE_NAME,
							uiGroup, pSceneManager,
							GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_DUMMY_TREE);
					//位置、サイズ調整
					SetButtonPositionAndScale(pButton,
						TREE_POS_X, TREE_DUM_POS_Y, TREE_SCALE_X, TREE_SCALE_Y);
				}
				//バインド木生成ボタン
				//UIグループ：木生成UI群
				//主な行動：バインド木生成
				{
					//ボタンデザイン画像ファイル名
					const std::string FILE_NAME = BUTTON_IMAGE_ROOT + "PlaneCreateBindTreeSample.png";
					//ボタン作成
					Button* pButton =
						CreateButton(
							FILE_NAME,
							uiGroup, pSceneManager,
							GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_BIND_TREE);
					//位置、サイズ調整
					SetButtonPositionAndScale(pButton,
						TREE_POS_X, TREE_BIN_POS_Y, TREE_SCALE_X, TREE_SCALE_Y);
				}
				//イーター木生成ボタン
				//UIグループ：木生成UI群
				//主な行動：イーター木生成
				{
					//ボタンデザイン画像ファイル名
					const std::string FILE_NAME = BUTTON_IMAGE_ROOT + "PlaneCreateEaterTreeSample.png";
					//ボタン作成
					Button* pButton =
						CreateButton(
							FILE_NAME,
							uiGroup, pSceneManager,
							GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_EATER_TREE);
					//位置、サイズ調整
					SetButtonPositionAndScale(pButton,
						TREE_POS_X, TREE_EAT_POS_Y, TREE_SCALE_X, TREE_SCALE_Y);
				}





				//（Back）木生成UI群を閉じる
				//UIグループ：木生成UI群
				//主な行動：木生成UI群を閉じる
				{
					//戻るボタン作成
					CreateBackButton(
						BUTTON_IMAGE_ROOT , 
						uiGroup , 
						pSceneManager,
						GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_UI_CREATE_TREE ,
						TREE_POS_X + 200 , 100
					);

				}


				//残り生成可能個数　背景
				{
					AddRemainNumber(uiGroup);
				}
			

			}
			//UIGroupの描画拒否
			pSceneManager->InvisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_CREATE_TREE);
		}

		//敵情報UI群
		{
			//UIGroup作成
			//インスタンス生成
			UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

			//UIGroupをSceneMnagerにセットする
			//SAMPLE_UI_GROUP_ENEMY_INFO
			//敵情報UI群
			pSceneManager->AddUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_ENEMY_INFO, uiGroup);



			//UI群となる、
			//UI画像、Buttonの生成

			//UI画像
			{
				//敵情報　背景
				//ロードと追加
				AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/Signboard/Signboard_Blue.png");
			}


			//Button
			{
				//（Back）敵情報UI群を閉じる
				//UIグループ：敵情報UI群
				//主な行動：敵情報UI群を閉じる
				{
					//戻るボタン作成
					CreateBackButton(
						BUTTON_IMAGE_ROOT,
						uiGroup,
						pSceneManager,
						GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_UI_ENEMY_INFO,
						1000, 100
					);
				}
			}



			//UIGroupの描画拒否
			pSceneManager->InvisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_ENEMY_INFO);

		}


		//木情報UI群
		{
			//UIGroup作成
			//インスタンス生成
			UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

			//UIGroupをSceneMnagerにセットする
			//SAMPLE_UI_GROUP_TREE_INFO
			//木情報UI群
			pSceneManager->AddUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_TREE_INFO, uiGroup);

			//UI画像
			{
				//木情報　背景
				//ロードと追加
				int hBackGround = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/Signboard/Signboard_Green.png");
				{
					//UIManagerを取得
					SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

					//奥へ移動
					pSceneUIManager->LowerOneStep(hBackGround);

				}

				//木削除　情報
				//ロードと追加
				{
					int hInfo = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/TreeInfo/InfoEraseTree.png");
					//UIManagerを取得
					SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

					pSceneUIManager->SetPixelPosition(hInfo, 640, 225);
					pSceneUIManager->SetPixelScale(hInfo, 400, 300);

				}
			}

			//Button
			{
				//（Back）木情報UI群を閉じる
				//UIグループ：木情報UI群
				//主な行動：木情報UI群を閉じる
				{
					//戻るボタン作成
					CreateBackButton(
						BUTTON_IMAGE_ROOT,
						uiGroup,
						pSceneManager,
						GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_UI_TREE_INFO,
						1000, 100
					);
				}

				//木削除実行
				//UIグループ：木情報UI群
				//主な行動：削除
				{
					//ボタンデザイン画像ファイル名
					const std::string FILE_NAME = BUTTON_IMAGE_ROOT + "PlaneEraseTreeSample.png";

					//ボタン作成
					Button* pButton =
						CreateButton(
							FILE_NAME,
							uiGroup, pSceneManager,
							GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_ERASE_TREE);

					//位置、サイズ調整
					SetButtonPositionAndScale(pButton,
						640, 500, 200, 200);

				}


			}

			//UIGroupの描画拒否
			pSceneManager->InvisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_TREE_INFO);

		}





		//繁殖率背景UI群
		{
			//UIGroup作成
			//インスタンス生成
			UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

			//UIGroupをSceneMnagerにセットする
			//SAMPLE_UI_GROUP_ENEMY_INFO
			//繁殖率背景UI群
			pSceneManager->AddUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_PLANT_DENSITY, uiGroup);

			//UI画像
			{
				//繁殖率　背景
				//ロードと追加
				int hBackGround = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/PlantDensity/PlantDensity.png");
				{
					//UIManagerを取得
					SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

					//サイズの可変
					pSceneUIManager->SetPixelScale(hBackGround, 200, 64);
					//位置の移動
					pSceneUIManager->SetPixelPosition(hBackGround, 1100, 100);
				}






			}

		
			//UIGroup描画
			pSceneManager->VisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_PLANT_DENSITY);
		}


		//FPSカーソル　UI群
		{
			//UIGroup作成
			//インスタンス生成
			UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

			//UIGroupをSceneMnagerにセットする
			//SAMPLE_UI_GROUP_FPS_CURSOR
			//FPSカーソル　UI群
			pSceneManager->AddUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_FPS_CURSOR, uiGroup);

			//UI画像
			{
				//FPS視点　背景
				//ロードと追加
				int hFPSCursor = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/FPSCursor/FpsCursor.png");
				{
					//UIManagerを取得
					SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

					//サイズの可変
					pSceneUIManager->SetPixelScale(hFPSCursor, 100, 100);
					//位置の移動
					pSceneUIManager->SetPixelPosition(hFPSCursor, Direct3D::scrWidth / 2 , Direct3D::scrHeight / 2 - 50);
					//奥へ移動
					pSceneUIManager->LowerOneStep(hFPSCursor);

				}

				//操作説明画像
				{
					int hController = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/Controller/Controller.png");
					//UIManagerを取得
					SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

					//サイズの可変
					pSceneUIManager->SetPixelScale(hController, 300, 200);
					//位置の移動
					pSceneUIManager->SetPixelPosition(hController, 150 , 100);
					//奥へ移動
					pSceneUIManager->LowerOneStep(hController);

				}




			}

			//UIGroup描画
			pSceneManager->VisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_FPS_CURSOR);

			//pngの背景透過と、指定α値による透過率変更処理
			uiGroup->SetAlphaSceneUIAndButton(0.5f);

		}

		//タイトルへ戻る　UI群
		{
			//UIGroup作成
			//インスタンス生成
			UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

			//UIGroupをSceneMnagerにセットする
			//SAMPLE_UI_GROUP_BACK_TITLE_SCENE
			//タイトルへ戻る　UI群
			pSceneManager->AddUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_BACK_TITLE_SCENE, uiGroup);

			//UI画像
			{
				//背景　背景
				//ロードと追加
				int hBackGround = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/Signboard/BackTitleScene.png");
				{
					//UIManagerを取得
					SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

					//サイズの可変
					//pSceneUIManager->SetPixelScale(hBackGround, 200, 64);
					//位置の移動
					pSceneUIManager->SetPixelPosition(hBackGround, 700, 400);

					//奥へ移動
					pSceneUIManager->LowerOneStep(hBackGround);
				}





			}

			//（）タイトルに戻る実行
			//UIグループ：タイトルに戻る群
			//主な行動：タイトルシーンへ戻る
			{
				//ボタンデザイン画像ファイル名
				const std::string FILE_NAME = BUTTON_IMAGE_ROOT + "BackTitleSceneButton.png";

				//ボタン作成
				Button* pButton =
					CreateButton(
						FILE_NAME,
						uiGroup, pSceneManager,
						GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_BACK_TITLE_SCENE);

				//位置、サイズ調整
				SetButtonPositionAndScale(pButton,
					800, 600, 200, 200);

			}

			//（Back）タイトルに戻るUI群を閉じる
				//UIグループ：タイトルに戻るUI群
				//主な行動：タイトルに戻るUI群を閉じる
			{
				//戻るボタン作成
				CreateBackButton(
					BUTTON_IMAGE_ROOT,
					uiGroup,
					pSceneManager,
					GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_UI_BACK_TITLE_SCENE,
					1000, 100
				);

			}
			//UIGroupの描画拒否
			pSceneManager->InvisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_BACK_TITLE_SCENE);
		}




		//タイマー背景UI群
		{
			//UIGroup作成
			//インスタンス生成
			UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

			//UIGroupをSceneMnagerにセットする
			//SAMPLE_UI_GROUP_ENEMY_INFO
			//タイマー背景UI群
			pSceneManager->AddUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_TIMER, uiGroup);

			//UI画像
			{
				//タイマー　背景
				//ロードと追加
				int hBackGround = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/Frame/Frame_TimerFrame.png");
				{
					//UIManagerを取得
					SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

					//サイズの可変
					pSceneUIManager->SetPixelScale(hBackGround, 300, 100);
					//位置の移動
					pSceneUIManager->SetPixelPosition(hBackGround, 1100, 300);
				}

			}


			//UIGroupの描画
			pSceneManager->VisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_TIMER);
		}


		//残り生成可能個数　背景UI群
		{
			//UIGroup作成
			//インスタンス生成
			UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

			//UIGroupをSceneMnagerにセットする
			//SAMPLE_UI_GROUP_ENEMY_INFO
			//タイマー背景UI群
			pSceneManager->AddUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_REMAIN_NUMBER, uiGroup);



			//残り生成可能個数　背景
			{
				AddRemainNumber(uiGroup);
			}

			//UIGroupの描画
			pSceneManager->VisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_REMAIN_NUMBER);

		}


	}


	//木情報UI群
	{
		//UIGroup作成
		//インスタンス生成
		UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

		//UIGroupをSceneMnagerにセットする
		pSceneManager->AddUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_GAME_MAP, uiGroup);


		//Button
		{
			//ゲームマップUIを閉じる
			//UIグループ：ゲームマップUI群
			//主な行動：ゲームマップUI群を閉じる
			{
				//戻るボタン作成
				CreateBackButton(
					BUTTON_IMAGE_ROOT,
					uiGroup,
					pSceneManager,
					GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_GAME_MAP,
					1000, 100
				);
			}
		}
		//UI
		{
			//ゲームマップUI　背景
				//ロードと追加
			int hBackGround = AddStackUI(uiGroup, "Assets/Scene/Image/GameScene/Frame/Frame_Wood_GameMap.png");
			{
				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

				//サイズの可変
				pSceneUIManager->SetPixelScale(hBackGround, 1050, 650);
				//位置の移動
				pSceneUIManager->SetPixelPosition(hBackGround, 500, 390);

				//位置を2つ下げる
					//描画の奥行き関係の調整
				for (int i = 0; i < 2; i++)
				{
					pSceneUIManager->LowerOneStep(hBackGround);
				}
			}


		
		}

	}

}


//UIGroupの画像追加
int GameScene::AddStackUI(UIGroup * pUIGroup, const std::string& FILE_NAME)
{
	//引数UIGroupに追加する
	return pUIGroup->AddStackUI(FILE_NAME);
}

//残り生成可能木本数　UIの作成
void GameScene::AddRemainNumber(UIGroup* pUIGroup)
{
	//残り生成可能個数　背景
	int hRemainNumber = AddStackUI(pUIGroup, "Assets/Scene/Image/GameScene/Frame/Frame_RemainNumber.png");
	{
		//UIManagerを取得
		SceneUIManager* pSceneUIManager = pUIGroup->GetSceneUIManager();

		//サイズの可変
		pSceneUIManager->SetPixelScale(hRemainNumber, 350, 100);
		//位置の移動
		pSceneUIManager->SetPixelPosition(hRemainNumber, 1100, 600);
	}

}


//ボタンの作成
int GameScene::AddStackButton(UIGroup * pUIGroup, Button * pButton, GameSceneManager* pSceneManager,
	GAME_SCENE_BUTTON_ALGORITHM algorithmType)
{

	int handle = -1;

	//ボタン押下時のアルゴリズムを所有している、
	//アルゴリズム作成クラス
	FactoryForGameSceneButtonStrategy* pFactory = new FactoryForGameSceneButtonStrategy;

	//ボタン押下時の行動をセットする
	{
		//作成
		GameSceneButtonStrategy* pButtonStrategy = pFactory->
			CreateGameSceneButtonStrategy(algorithmType, pSceneManager);


		//UIGroupから経由して、
		//ゲームシーンのボタンクラス群へ
		//インスタンスをセットする
		//ボタンのインスタンスと、行動ストラテジーのアルゴリズム
		handle = pUIGroup->AddStackButton(pButton, pButtonStrategy);

	}

	//削除
	SAFE_DELETE(pFactory);

	//ButtonManagerを取得
	SceneButtonManager* pSceneButtonManager = pUIGroup->GetSceneButtonManager();

	//ボタンマネージャーを
		//ボタンクラスにセットさせる
	pButton->SetSceneButtonManager(pSceneButtonManager);


	//★pSceneButtonManager->AddStackを終えたのちに描画、非描画を行う必要がある
	//ボタン非描画
	//デフォルトにて非描画
	pSceneButtonManager->InvisibleSceneButton(pButton);

	return handle;
}

