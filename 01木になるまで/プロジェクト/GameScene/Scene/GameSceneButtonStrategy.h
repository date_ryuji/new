#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/ButtonStrategyParent.h"	//ボタン入力行動ストラテジークラス　抽象クラス

//クラスのプロトタイプ宣言
class GameObject;
class GameSceneManager;
class GameSceneButtonStrategy;
//enum class プロトタイプ宣言
enum class GAME_SCENE_INPUT_ALGORITHM;
enum class GAME_SCENE_UI_GROUP_TYPE;


//ゲームシーンのボタン　押下時のアルゴリズム
	//詳細：ボタン（UI画像へ　マウスクリックの判定を付けたクラス）
	//　　：押下際の行動アルゴリズムを記述したクラスを識別するタイプ
enum class GAME_SCENE_BUTTON_ALGORITHM
{
	GAME_BUTTON_ALGORITHM_CREATE_TREE = 0,		//通常木を生成アルゴリズム
	GAME_BUTTON_ALGORITHM_CREATE_DUMMY_TREE,	//ダミー木を生成するアルゴリズム
	GAME_BUTTON_ALGORITHM_CREATE_BIND_TREE,		//バインド木を生成するアルゴリズム
	GAME_BUTTON_ALGORITHM_CREATE_EATER_TREE,	//イーター木を生成するアルゴリズム
	GAME_BUTTON_ALGORITHM_CLOSE_UI_CREATE_TREE ,//木生成UI群を閉じる
	GAME_BUTTON_ALGORITHM_CLOSE_UI_ENEMY_INFO ,	//敵情報看板UI群を閉じる
	GAME_BUTTON_ALGORITHM_CLOSE_UI_TREE_INFO,	//木情報看板UI群を閉じる
	GAME_BUTTON_ALGORITHM_GROW_TRUNK,			//木の幹の成長（木の幹HPをUP）
	GAME_BUTTON_ALGORITHM_GROW_ROOT,			//木の根の成長（木の根HPをUP）
	GAME_BUTTON_ALGORITHM_ERASE_TREE,			//木削除
	GAME_BUTTON_ALGORITHM_BACK_TITLE_SCENE,		//タイトルへ戻る
	GAME_BUTTON_ALGORITHM_CLOSE_UI_BACK_TITLE_SCENE ,	//タイトルへ戻るUIを閉じる
	GAME_BUTTON_ALGORITHM_CLOSE_GAME_MAP ,		//ゲームマップを閉じる

	GAME_BUTTON_ALGORITHM_MAX,					//MAX
};

/*
	クラス詳細	：ボタン入力を行った際の行動クラス　作成クラス（シーン専用）
	使用デザインパターン：FactoryMethod
	使用クラス	：GameScene
	クラス概要（詳しく）
				：GAME_SCENE_BUTTON_ALGORITHMに登録した行動アルゴリズムタイプにて識別し、
				　専用クラスを作成する

*/
class FactoryForGameSceneButtonStrategy
{
//public メンバ
public:
	//親のゲームオブジェクト
	GameObject* pParent_;

//publci メソッド
public:
	//コンストラクタ
		//引数：なし
		//戻値：なし
	FactoryForGameSceneButtonStrategy();

	//行動クラスのインスタンスを生成する関数
		//詳細：引数タイプ（GAME_SCENE_BUTTON_ALGORITHM(enum)）にて生成クラスを識別し、行動クラスのインスタンスを作成、インスタンスを返す
		//　　：親である、行動クラス生成の実行を呼び出す
		//　　：以下関数の中で、
		//　　：行動クラスの作成
		//引数：行動アルゴリズム識別タイプ（GAME_SCENE_BUTTON_ALGORITHM(enum)）
		//引数：シーンマネージャー（行動アルゴリズムにて各シーンオブジェクトの連携が必要である場合、シーンマネージャーを利用するため、シーンマネージャーに呼び込むメソッドを用意しておく。そのメソッド呼び込みのために取得）
		//戻値：行動クラスのインスタンス
	GameSceneButtonStrategy* CreateGameSceneButtonStrategy(
		GAME_SCENE_BUTTON_ALGORITHM type , GameSceneManager* pSceneManager);
};


/*
	クラス詳細	：ボタン入力時の実行行動を実装した行動ストラテジークラス
	使用クラス	：GameScene
	クラスレベル：スーパークラス（スーパークラス（ButtonStrategyParent））
	クラス概要（詳しく）
				：行動実行が確定した場合、行動アルゴリズムを記述するメソッドを持つクラス
				　継承先においても、メソッドをオーバーライドすることで、各継承先で個別の処理を記述可能。
				  ボタン入力時の行動を変えることができる


*/
class GameSceneButtonStrategy : public ButtonStrategyParent
{
//protected メンバ変数、ポインタ、配列
protected : 
	//シーンマネージャークラス
	GameSceneManager* pGameSceneManager_;

//protected メソッド
protected : 

	/*継承先にて共通して使う処理関数*/

	//UIGroupの表示、非表示の切替え
		//詳細：引数にて示されたUIGroup（シーンマネージャーにて管理しているUIGroup）の描画、非描画を切り替える。描画の場合、非描画。非描画の場合、描画。
		//引数：UIGroupタイプ
		//戻値：なし
	void VisibleInvertUIGroup(GAME_SCENE_UI_GROUP_TYPE UIGroupType);

	//プレイ中の通常の入力キーのセット
		//詳細：プレイシーンにおいてUIなどが表示されていないときのキー入力セット（移動などのキー入力）
		//　　：UI表示に伴い、全てのキー入力を一新しているので、
		//　　：UI非表示に伴い、プレイシーン時の通常のキー入力をセット（復活）する
		//　　：継承元である（GameSceneButtonStrategy）に、プレイシーンにおける通常のキー入力をセットする、処理を関数としてまとめておく
		//引数：なし
		//戻値：なし
	void SetDefaultEachInputKey();

	//プレイシーンのおける常駐UIの表示
		//詳細：常駐で表示を行いたいUI（プレイ中に常に表示しているUI）
		//　　：特定のUIが表示されているときは表示を非表示にし、
		//　　：特定UIの表示が終了後に再表示を行うUI
		//　　：その際表示を請け負う関数
		//　　：一つ一つUIGroupを指定して、表示を行う
		//引数：なし
		//戻値：なし
	void DispResidentUIGroups();


	//敵、木オブジェクトなどの動的処理を動作再開する
		//詳細：動きが止められていてた、動的オブジェクトの動きを再開	
		//　　：GameInputStrategyにおける、SetTimeStopForMoveingObjectに対する処理
		//引数：なし
		//戻値：なし
	void SetTimeStartForMoveingObject();


	//常駐UI表示、デバイス入力の復活　により世界の動きを再開
		//詳細：特定UIの非表示を受けて、（表示により、世界の動きを停止させていた）
		//　　：・常駐のUIの表示
		//　　：・通常（常駐）のキーの回復
		//　　：世界の動きを停止させたことにより未設定にしたものを、設定しなおす
		//　　：GameInputStrategyにおけるSetImpactInTheWorldに対する処理
		//引数：非表示としたいグループのタイプ（表示処理によって、上記の・2つを行ったため、自身の非表示と、初期化の呼び込み）
		//戻値：なし
	void ResetInputKeyAndUIGroups(GAME_SCENE_UI_GROUP_TYPE UIGroupType);

//public メソッド
public :

	//コンストラクタ
		//引数：なし
		//戻値：なし
	GameSceneButtonStrategy();

	//コンストラクタ
		//引数：シーンマネージャークラス
		//戻値：なし
	GameSceneButtonStrategy(GameSceneManager* pSceneManager);

	//世界に影響を与える（再開）
		//詳細：GameInputStrategyにおけるSetImpactWorldにて行った、世界の動きの停止など。
		//　　：その処理の反対の処理をお粉う
		//　　：世界全体への影響の設定をまとめた関数（デフォルトキーの設定、動的処理の再開を一関数で呼んでしまう）
		//　　：動的処理の再開
		//引数：なし
		//戻値：なし
	void SetImpactInTheWorld();

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void AlgorithmInterface();	//上記のSetImpact〜の処理関数を呼び込むため、親クラスのインスタンスを作る。そのため抽象クラスでなくする。
};
