#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/SceneInputStrategyParent.h"	//入力行動ストラテジークラス　抽象クラス


/*
	InputStrategyについて

	ゲーム中のボタン（キー、マウスなどのデバイス）の押下をされたときに
	行動する、実行するアルゴリズムをクラス分けしたもの


	//GameInputter（SceneUserInputter）は基本的にキー入力の押下だけを取得し、そのキーが押されたときの行動は、すべて、ほかのクラス、外部に委託する。
		//その委託先の行動をクラス化する
	//キーと行動を固定化しない


	//クラス分けをするメリット�@
		//そして、クラス分けで、行動を分離化することで、
			//現在設定されている、Enter入力時の行動アルゴリズムクラスは、　移動という行動だが、
			//Enterキーの行動を切り替えたいときは、その行動のアルゴリズムクラスを切り替えれば、よいだけ。（切り替えるだけで、入力時の行動を切り替えられる）


	//クラス分けをするメリット�A
		//クラス分けをすることで、
		//キー入力を一時的になくしたいとき、
		//いつもはEnterキーを入力管理してほしいが、
			//この時だけは、Enterキー入力をしてほしくない。。。

		//そうゆうときは、セットしているキー入力時の行動アルゴリズムクラスをnullptrにすれば、
		//つまり、何もセットしなければ、　Enterキーを押したとしても、何も行動され、実行されない



*/

	


/*
	Factoryクラスについて

	//FactoryForGameSceneInputStrategy
	//その上記の生成クラス
	//生成だけを請け負わせて、
	//生成先のクラスをインクルードしなくても済む

	

*/


//クラスのプロトタイプ宣言
class GameSceneManager;
class GameSceneInputStrategy;
class SceneUserInputter;
//構造体のプロトタイプ宣言
struct CreateInputStrategyInfo;
//enum class プロトタイプ宣言
enum class GAME_SCENE_INPUT_TYPE;
enum class GAME_SCENE_UI_GROUP_TYPE;



//ゲームシーンのデバイス入力　押下時のアルゴリズム
	//詳細：デバイス（キーボード、マウス）押下際の行動アルゴリズムを記述したクラスを識別するタイプ
enum class GAME_SCENE_INPUT_ALGORITHM
{
	GAME_INPUT_ALGORITHM_CREATE_TREE = 0,			//木を生成のUIなどの表示アルゴリズム
	GAME_INPUT_ALGORITHM_TREE_INFO_OR_ENEMY_INFO,	//木情報UI　OR　敵情報UIの表示アルゴリズム
													//クリックした時点で、どちらを表示するかは指定していない
	GAME_INPUT_ALGORITHM_ENEMY_INFO ,				//敵情報UIなどの表示アルゴリズム
	GAME_INPUT_ALGORITHM_TREE_INFO ,				//木情報UIなどの表示アルゴリズム
	GAME_INPUT_ALGORITHM_BACK_TITLE_SCENE,			//タイトルへ戻るUIなどの表示アルゴリズム
	GAME_INPUT_ALGORITHM_MOVE_PLAYER,				//移動
	GAME_INPUT_ALGORITHM_CREAE_GAME_MAP ,			//ゲームマップ生成
	GAME_INPUT_ALGORITHM_NULL,						//行動なし
													//キー入力による行動をなし	
													//行動がセットされているときは、その行動を破棄する役割
	GAME_INPUT_ALGORITHM_MAX,						//MAX
};



/*
	クラス詳細	：各デバイス入力を行った際の行動クラス　作成クラス（シーン専用）
	使用デザインパターン：FactoryMethod
	使用クラス	：GameScene
	クラスレベル：サブクラス（スーパークラス（FactoryForSceneInputStrategyParent））
	クラス概要（詳しく）
				：GAME_SCENE_INPUT_ALGORITHMに登録した行動アルゴリズムタイプにて識別し、
				　専用クラスを作成する

*/
class FactoryForGameSceneInputStrategy : public FactoryForSceneInputStrategyParent
{
//public メソッド
public:
	//コンストラクタ
		//引数：なし
		//戻値：なし
	FactoryForGameSceneInputStrategy();

	//ストラテジークラスを生成する
		//詳細：生成するストラテジークラスを引数タイプより識別し、
		//　　：ストラテジータイプを生成して返す
		//    ：！キー登録の関数ではない！
		//　　：ストラテジークラスを生成する
		//　　：生成するストラテジークラスを引数タイプより識別し、
		//　　：ストラテジータイプを生成して返す
		//アクセスレベルpublic 理由：シーン表示、非表示の行動アルゴリズムを外部から作りたい時に使用する
	GameSceneInputStrategy* CreateInputStrategy(
		GAME_SCENE_INPUT_ALGORITHM type,
		GameSceneManager* pSceneManager,
		CreateInputStrategyInfo* pCreateInfo);



	//行動クラスのインスタンスを生成する関数
		//詳細：引数タイプ（TITLE_SCENE_INPUT_ALGORITHM(enum)）にて生成クラスを識別し、行動クラスのインスタンスを作成、インスタンスを返す
		//　　：親である、行動クラス生成の実行を呼び出す
		//　　：以下関数の中で、
		//　　：行動クラスの作成
		//　　：行動クラスをキータイプとともに、UserInputterに登録　の２つを行っている。
		//　　：引数へ該当情報を渡すだけで、キー入力の操作を確定できる
		//引数：行動アルゴリズム識別タイプ（TITLE_SCENE_INPUT_ALGORITHM(enum)）
		//引数：シーンマネージャー（行動アルゴリズムにて各シーンオブジェクトの連携が必要である場合、シーンマネージャーを利用するため、シーンマネージャーに呼び込むメソッドを用意しておく。そのメソッド呼び込みのために取得）
		//引数：入力クラス（ユーザー入力受付クラス）
		//引数：入力情報（入力受付方法+入力デバイスタイプ+入力コード）
		//戻値：なし
		/*
		
			//！キー登録関数！
			//行動クラスのインスタンスを作成し、
			//インスタンスを返す
			//親である、行動クラス生成の実行を呼び出す
			//以下関数の中で、
			//行動クラスの作成
			//行動クラスをキータイプとともに、UserInputterに登録　の２つを行っている。
			//引数へ該当情報を渡すだけで、キー入力の操作を確定できる

		*/
	void CreateGameSceneInputStrategy(
		GAME_SCENE_INPUT_ALGORITHM type, GameSceneManager* pSceneManager,
		SceneUserInputter* pSceneUserInputter,
		CreateInputStrategyInfo* pCreateInfo);


};



/*
	クラス詳細	：デバイス入力時の実行行動を実装した行動ストラテジークラス
	使用クラス	：GameScene
	クラスレベル：スーパークラス（スーパークラス（SceneInputStrategyParent））
	クラス概要（詳しく）
				：行動実行が確定した場合、行動アルゴリズムを記述するメソッドを持つクラス
				　継承先においても、メソッドをオーバーライドすることで、各継承先で個別の処理を記述可能。
				  デバイス入力時の行動を変えることができる

*/
class GameSceneInputStrategy : public SceneInputStrategyParent
{
//protected メソッド
protected:
	//継承元クラスを各シーンごとの継承先型に変換
		//詳細：TitleSceneManager型にキャストして返す
		//引数：シーンマネージャークラス
		//戻値：TitleSceneManager型にキャストしたシーンマネージャークラス
	GameSceneManager* TransGameSceneManager(SceneManagerParent* pSceneManager);


	//デバイス入力時の行動
		//詳細：現在使用している入力キーの全てのキーをNULL（キー入力による行動なし）にセットする
		//引数：なし
		//戻値：なし
	void SetNullStrategyForAllKey();

	//世界の動きを止める
		//詳細：シーンマネージャークラスの世界の動きを止める、時間計測を止める関数呼び出し
		//　　：敵、木オブジェクトなどの動的処理を停止する
		//　　：UI表示中は動きを止めたいため、以下の関数先にて、動的処理を停止するフラグを立てる
		//引数：なし
		//戻値：なし
	void SetTimeStopForMoveingObject();


//public メソッド
public:
	//コンストラクタ
		//詳細：スプラッシュシーンマネージャーの取得
		//引数：シーンマネージャークラス
		//引数：入力情報（入力受付方法+入力デバイスタイプ+入力コード）
		//戻値：なし
	GameSceneInputStrategy(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo);


	//世界全体への影響の設定をまとめた関数
		//詳細：（キーのNULL設定（SetNullStrategyForAllKey()）、動的処理の停止（SetTimeStopForMoveingObject()）を一関数で呼んでしまう）
		//引数：なし
		//戻値：なし
	void SetImpactInTheWorld();

	//すべてのUI群を非表示
		//詳細：シーンマネージャークラスにて管理するすべてのUIGroupを非表示にする	
		//引数：なし
		//戻値：なし
	void InvisibleAllUIGroup();



	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void AlgorithmInterface();	//上記の関数SetImpact〜関数を外部から呼び出す必要が出来たため。そのために、親のインスタンスを生成する必要が出てきた。

};

