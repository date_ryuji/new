#include "GameSceneManager.h"							//ヘッダ
#include "../../CommonData/GlobalMacro.h"				//共通マクロ
#include "../../Engine/GameObject/UIGroup.h"			//UIGroup群
#include "../../Engine/Scene/SceneButtonManager.h"		//ボタンマネージャー
#include "../../Engine/Scene/SceneChanger.h"			//シーン切り替えクラス
#include "../../Engine/Button/Button.h"					//ボタン
#include "../../Engine/Text/Texts.h"					//テキスト
#include "../../Engine/Algorithm/RayCaster.h"			//レイキャスト実行クラス
#include "../../Engine/Csv/CsvReader.h"					//CSVReader
#include "../../Engine/Scene/SceneUserInputter.h"		//ユーザー入力クラス
#include "../Player/Player.h"							//プレイヤー
#include "../Player/CameraController.h"					//カメラ移動クラス
#include "../GrassLand/GrassLandObject.h"				//地面クラス
#include "../CountTimer/CountTimer.h"					//カウントタイマークラス
#include "../Tree/GroupOfTree.h"						//木群
														//木オブジェクトを管理し、
														//一つのモデルを複数回描画することで、メモリを余分に消費させない役割を持たせる
#include "../Tree/Tree.h"								//木オブジェクト
#include "../Enemy/EnemySpawn.h"						//敵オブジェクトのスポーン
#include "../Enemy/EnemyStrategyBulletinBoard.h"		//敵掲示板クラス
#include "../Enemy/EnemyCommandTower.h"					//敵司令塔クラス
#include "../PlantDensity/PlantDensity.h"				//繁殖率を持つクラス
#include "../GameMap/GameMap.h"							//ゲームマップ
#include "../HelpImages/HelpImages.h"					//ヘルプ画像群
#include "../HelpImages/HelpImagesButtonStrategy.h"		//ヘルプ画像群の操作ボタン
#include "GameSceneInputStrategy.h"						//キー入力時の行動クラス
														//時間停止時の処理一覧を持っているクラス
#include "GameSceneButtonStrategy.h"					//ボタン押下時の行動クラス
														//時間計測再開時の処理一覧を持っているクラス
#include "GameSceneUpdateAlgorithm.h"					//更新行動アルゴリズム
#include "StageData.h"									//ステージ情報	//キー入力情報



//コンストラクタ
GameSceneManager::GameSceneManager(GameObject * parent):
	SceneManagerParent::SceneManagerParent(parent, "GameSceneManager"),
	lastCollisionPolyNum_(-1),

	pCountTimer_(nullptr),
	pCsvReader_(nullptr),
	currentTreeInfo_(nullptr),
	pTexts_(nullptr),

	pSettingHelpImages_(nullptr),

	pUpdateAlgorithm_(nullptr),

	isGetTag_(false),
	visibleTreeNum_(true) , 

	currentUpdateAlgorithm_(-1)	//-1から始めて、　一回目のUpdateにて、カウントが回るため　０のタグから取得する
{
}

//デストラクタ
GameSceneManager::~GameSceneManager()
{
}

//初期化
void GameSceneManager::Initialize()
{

	/*シーンマネージャー定義時の必須処理*******************************************************************/
		//詳細：マネージャーとしての機能を持たせるための前提処理、初期化
		//　　：（条件：リソースが存在する場合必須）
		//　　：リソースとは、この場合、連携オブジェクト群における連携オブジェクトが存在するか
		//　　：　　　　　　　　　　　　UIGroup軍におけるUIGroupが存在するか。
		//　　：上記のリソースが一つも存在しない場合に呼び込む必要はない

	//連携オブジェクト群のポインタを保存しておく配列の初期化、生成
		//継承先である、自身のクラスで宣言した、連携オブジェクトを指定するEnum値をもとに、連携オブジェクトの数を引数として渡す
		//関数先にて、引数分の連携オブジェクトを入れる枠、要素を確保してくれる
	NewArrayForCoopObjects((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_MAX);
	//UIオブジェクト群のポインタを保存しておく配列の初期化、生成
	NewArrayForUIGroups((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_MAX);

	/************************************************************************************************************/

	{
		//ゲームのシーンの流れを登録した
			//更新処理行動の順番が書かれているCSVファイルの読み込み
		pCsvReader_ = new CsvReader();
		pCsvReader_->Load("Assets/Scene/InputCsv/GameScene/UpdateAlgorithm.csv");
	}

	//テキストインスタンス　作成
	pTexts_ = new Texts;
	//初期化
	pTexts_->Initialize();
	//テキストのフォントサイズ変更
	pTexts_->SetFontSize(50);


	//ゲームのシーンの流れを回す
	//更新処理のタグを読み込ませるフラグを立たせる
		//次のUpdateにて、1つ目のタグが読み込まれる
	EndUpdateAlgorithm();

}

//更新
void GameSceneManager::Update()
{
	//ゲームの流れ更新処理
	UpdateGameAlgorithm();

}
//ゲームの流れ更新処理
void GameSceneManager::UpdateGameAlgorithm()
{
	/*
		ゲームシーンの流れ

	//ゲームの流れを作るために
	//シーンを分けずに、HelpImage表示して、、スタート画像を表示して、、、ゲーム時間図って、、ゲーム終了画像を表示して、、
		//という流れを作るにはどうするのか、
	//�@1つの考えとしては、
		//CSVのタグにまとめておいて、
		//タグを読み込みたいとき、フラグを立てる

	//そのフラグが立っているとき、
		//CSVからタグを読み込む
		//そのタグにて示される、該当する関数を呼び込む。

		//其の該当先の関数（開始）と（更新）をセットし
		//（開始）を行って、
		//そのあとに、次のフラグが立つまで、セットした、（更新）を毎フレーム行わせる

		//その更新では、
		//仮に、読み込んだタグが、
		//スタート画像の表示であるならば、（画像表示、というタグで、画像種類は、CSVの別部分のタグから読み込む）
		//開始にて、　処理の用意、更新関数のセットを行う
		//（クラスとして分けておいて、そのクラスをセットするでも可）
		//そして、、毎フレームそのセットした更新を呼び込んで更新
			//その更新内容は、画像の表示ならば、　画像のα値変動とか

		//ゲームプレイだったら、
		//ゲームのカウント表示とかとかの表示

	//�A２つ目の考え
	//シーンの中に、小さなシーンクラスのようなものを作る
		//そして、そのシーンクラスのようなものを、必要タイミングでセットして、今は、この処理をするから、このクラスだ。
		//今はこの行動、だから、このクラスだと、動的にセットする。

		//上記であれば、分離もできる

		//だが分離しすぎて、
		//描画も分離してしまうと、背景としては、ゲームプレイ画面を表示したままで、こうゆう、分離した行動をしてもらうタイというときに苦労する


	//そのため、今回は�@を用いる

	//上記を踏まえて、
	//分離を行いたいゲームシーンにおける行動
	//１．HelpImageのヒント表示
	//２．スタート画像の表示
	//３．ゲームプレイ
	//４．エンド画像の表示
	//５．シーン切り替え


	*/




	//ゲームの流れの更新
	//条件：タグの更新が必要ならば
	if (IsUpdatingAlgorithm())
	{
		//条件：存在するならば
		if (pUpdateAlgorithm_ != nullptr)
		{
			//解放を呼ぶ
			pUpdateAlgorithm_->Release();

			//現在のタグの更新行動クラスを削除
			SAFE_DELETE(pUpdateAlgorithm_);
		}
		//条件：カウンターを回した際に最後の要素になった場合
		if (currentUpdateAlgorithm_ + 1 ==
			(int)GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_MAX)
		{
			//何もしない
			return;
		}

		//判断を抜けたなら
		//カウントアップ
		currentUpdateAlgorithm_++;


		//タグ読み込み
		//生成関数の作成
		FactoryForGameSceneUpdateAlgorithm* pAlgorithmFactory = new FactoryForGameSceneUpdateAlgorithm();

		//Csvより
		//タグを読み込む
		//引数：CSVファイルのX
		//引数：CSVファイルのY（＋１は、位置調整のため）
		std::string tag = pCsvReader_->GetString(0, currentUpdateAlgorithm_ + 1);


		//更新処理行動クラスの作成	
			//タグの文字列で制作先クラスを特定
		pUpdateAlgorithm_ = pAlgorithmFactory->CreateGameSceneUpdateAlgorithm(tag, this);

		//初期化呼び込み
		pUpdateAlgorithm_->Initialize();


		//生成クラスの解放
		SAFE_DELETE(pAlgorithmFactory);


		//更新処理開始のフラグを立てる
		StartUpdateAlgorithm();
	}


	//条件：更新が存在するならば
	if (pUpdateAlgorithm_ != nullptr)
	{
		//ゲームの流れを作る
		//更新処理を行わせる
		pUpdateAlgorithm_->Update();
	}

}

//描画
void GameSceneManager::Draw()
{
	//条件：描画許可されていたら
	//　　：タイマーが計測中である場合
	if (IsPermitTimer())
	{
		//残り時間の描画
		DrawTimeLimit();
	}
	//条件：描画許可されていたら
	//　　：残り木生成可能本数の描画許可されているか
	if (visibleTreeNum_)
	{
		//残り木生成個数表示
		DrawRemainNumberOfCreateTree();
	}

}
//テキスト描画　残り生成可能木本数　
void GameSceneManager::DrawRemainNumberOfCreateTree()
{

	//テキストのフォントサイズ変更
	pTexts_->SetFontSize(25);


	//プレイヤーの所有するオブジェクト取得
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);

	//注釈描画
	pTexts_->DrawStringTex("残り生成可能個数：" , 
		TEXT_COLOR_WHITE, 960, 600);


	//残り生成可能個数描画
	pTexts_->DrawTex<int>(pGroupOfTree->RemainingNumberOfCreateTree() , 
		TEXT_COLOR_GREEN_YELLOW, 1200, 600);


}
//テキスト描画　残り時間
void GameSceneManager::DrawTimeLimit()
{
	//テキストのフォントサイズ変更
	pTexts_->SetFontSize(50);


	//残り時間の描画

	//残り時間の計測（S）	
		//残り時間の取得（S）
	float currentTime = 0.f;
	if (pCountTimer_ != nullptr)
	{
		currentTime = pCountTimer_->GetRemaingTimer();
	}

	unsigned int min = 0;
	unsigned int sec = 0;
	if (currentTime != 0.0f)
		//0の場合、割り算をすると計算結果がInfinityになる
	{



		//上記のSを
			//分
			//秒　に分ける

		//分
			//現在の残り時間を一分で割る
			//それをintで受け取り
		min = (unsigned int)(currentTime / 60.0f);

		//秒
			//割ったときの余り
			//残り時間　ー　残り分＊1分の秒数　＝　余りの秒
		sec = (unsigned int)(currentTime - (min * 60));

	}
	else
	{
		min = 0;
		sec = 0;
	}

	//上記をひとつの文字列にして
		//それをテキストとして描画する

	//String方のメソッドの力を借りて、
		//文字列を作っていく
	std::string drawingText = "0";

	{
		//"0x"
		//分の登録
			//文字列への変換
		std::string minStr = std::to_string(min);
		//文字列の文末へ文字列の追加
		drawingText.insert(drawingText.length(), minStr);
	}

	{
		//"0x:"
		//:の追加
		std::string coron = ":";
		//文字列の文末へ文字列の追加
		drawingText.insert(drawingText.length(), coron);

	}

	{
		//"0x:xx"
		//秒の追加
		std::string secStr = std::to_string(sec);
		//文字列の文末へ文字列の追加
		drawingText.insert(drawingText.length(), secStr);

	}

	////完成した
	////文字列の描画

	//pTexts_->DrawTex<std::string>("gjaoijge" , TEXT_COLOR_VIOLET, 0 , 0);


	//文字列での描画が不可能であったため、
		//一文字ずつ描画させる
	pTexts_->DrawTex<int>(0, TEXT_COLOR_BLACK, 1000, 275);

	//分
	pTexts_->DrawTex<int>(min, TEXT_COLOR_BLACK, 1030, 275);

	//コロン
	pTexts_->DrawTex<char>(':', TEXT_COLOR_BLACK, 1060, 275);


	//秒の一桁目
	if (sec != 0)
	{
		
		pTexts_->DrawTex<int>(sec / 10, TEXT_COLOR_BLACK, 1090, 275);
		pTexts_->DrawTex<int>(sec % 10, TEXT_COLOR_BLACK, 1120, 275);
	}
	else
	{
		pTexts_->DrawTex<int>(0, TEXT_COLOR_BLACK, 1090, 275);
		pTexts_->DrawTex<int>(0, TEXT_COLOR_BLACK, 1120, 275);
	}
	



}

//解放
void GameSceneManager::Release()
{
	//シーン内連携オブジェクトの解放
		//解放といっても、オブジェクトはGameObject型であるため、Deleteはしない。マネージャークラス内にて管理しているオブジェクトを登録する配列ポインタの解放
	DeleteArrayForCoopObjects();
	//UIオブジェクト群の解放
	DeleteArrayForUIGroups();
	
	SAFE_DELETE(currentTreeInfo_);
	SAFE_DELETE(pTexts_);

}
//UIGroupの個数を取得
int GameSceneManager::GetUIGroupCount()
{
	return (int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_MAX;
}

//HelpImageの作成
HelpImages* GameSceneManager::CreateHelpImages(int posX, int posY, int sizeX, int sizeY, HELP_IMAGES_BUTTON_ALGORITHM endingType)
{
	//ヘルプ画像群の作成
	// 	   描画の遷移をクラスに一任する
	//画像のロード
	HelpImages* pHelpImages = (HelpImages*)Instantiate<HelpImages>(this);
	//描画遷移を行う順番に画像をロードさせる
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help1.png");
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help2.png");
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help3.png");
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help4.png");
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help5.png");
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help6.png");
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help7.png");
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help8.png");
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help9.png");
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help10.png");
	pHelpImages->AddLoadImage("Assets/Scene/Image/GameScene/HelpImages/Help11.png");

	//背景画像をロード
	pHelpImages->AddLoadBackGroundImage("Assets/Scene/Image/GameScene/Frame/Frame_Wood_Purple.png");

	//遷移を操るボタンクラス
	CreateButtonInHelpImages(pHelpImages, endingType);

	//描画開始
	pHelpImages->StartVisibleImage();

	//描画位置調整
	pHelpImages->SetSize(sizeX, sizeY);
	pHelpImages->SetPosition(posX, posY);


	return pHelpImages;
}
//HelpImage内のボタンの作成
void GameSceneManager::CreateButtonInHelpImages(HelpImages* pHelpImages, HELP_IMAGES_BUTTON_ALGORITHM endingType)
{
	//ボタンルートパス
	const std::string BUTTON_IAMGE_ROOT = "Assets/Scene/Image/GameScene/Button/";
	{
		//ボタンデザイン画像ファイル名
		const std::string FILE_NAME = BUTTON_IAMGE_ROOT + "CircleBack.png";


		//ボタンインスタンス
		Button* pButton = (Button*)Instantiate<Button>((GameObject*)pHelpImages);
		//引数：デザインのロード
		//引数：画像を設定するボタン（ON or OFF）
		//引数：押下タイプ
		pButton->Initialize(FILE_NAME , BUTTON_STATUS::BUTTON_STATUS_OFF, BUTTON_PUSH_TYPE::PUSH_TYPE_DOWN);

		//登録
		pHelpImages->AddButton(pButton, HELP_IMAGES_BUTTON_ALGORITHM::HELP_BUTTON_ALGORITHM_BACK);
		//ボタンのサイズ変更
		pButton->SetPixelScale(100, 100);
	}
	{
		const std::string FILE_NAME = BUTTON_IAMGE_ROOT + "CircleNext.png";
		Button* pButton = (Button*)Instantiate<Button>((GameObject*)pHelpImages);
		pButton->Initialize(FILE_NAME, BUTTON_STATUS::BUTTON_STATUS_OFF, BUTTON_PUSH_TYPE::PUSH_TYPE_DOWN);
		pHelpImages->AddButton(pButton, HELP_IMAGES_BUTTON_ALGORITHM::HELP_BUTTON_ALGORITHM_NEXT);
		//ボタンのサイズ変更
		pButton->SetPixelScale(100, 100);
	}
	{
		const std::string FILE_NAME = BUTTON_IAMGE_ROOT + "CircleEnding.png";
		Button* pButton = (Button*)Instantiate<Button>((GameObject*)pHelpImages);
		pButton->Initialize(FILE_NAME, BUTTON_STATUS::BUTTON_STATUS_OFF, BUTTON_PUSH_TYPE::PUSH_TYPE_DOWN);
		pHelpImages->AddButton(pButton, endingType);
		//ボタンのサイズ変更
		pButton->SetPixelScale(100 , 100);
	}

}
//設定画面(UIGroup)におけるヘルプ画像(HelpImage)を作成する
void GameSceneManager::CreateSettingHelpImages()
{
	//ヘルプ画像群の作成
		//endingType : 通常終了
	pSettingHelpImages_ = CreateHelpImages(225, 405, 390, 230 , HELP_IMAGES_BUTTON_ALGORITHM::HELP_BUTTON_ALGORITHM_ENDING);

	//ボタンの位置変更
	UIGroup* pUIGroup = pSettingHelpImages_->GetUIGroup();

	//ボタンマネージャーを取得
	SceneButtonManager* pSceneButtonManager = pUIGroup->GetSceneButtonManager();

	//ボタンの位置変更
	pSceneButtonManager->SetPixelPosition(0, 100, 200);
	pSceneButtonManager->SetPixelPosition(1, 200, 200);
	pSceneButtonManager->SetPixelPosition(2, 300, 200);


	//背景画像の位置セット
	pSettingHelpImages_->SetBackGroundPosition(225, 400);
	pSettingHelpImages_->SetBackGroundSize(450, 300);


}
//設定画面(UIGroup)におけるヘルプ画像(HelpImage)を削除する
void GameSceneManager::DeleteSettingHelpImages()
{
	//ヘルプ画像を探し
		//削除する
		//Findを回避するために、インスタンスをメンバ変数に持たせておく
			

	//条件：HelpImageがあるか
	if (pSettingHelpImages_ != nullptr)
	{
		//終了処理を呼び込む
			//本来であれば、
			//終了ボタン押下時の終了行動を実行したいが、現段階では以下の終了関数のみ呼ばれるため、その処理は実装しない
		pSettingHelpImages_->GoEndingImage();

		//ポインタを使用できないようにするため
			//nullptrで初期化
		pSettingHelpImages_ = nullptr;
	}

}


//タイトルシーンに戻る
void GameSceneManager::BackTitleScene()
{
	SceneChanger* pSceneChanger = (SceneChanger*)FindObject("SceneChanger");
	pSceneChanger->ChangeScene(SCENE_ID::SCENE_ID_TITLE);
}
//引数敵オブジェクトが敵スポーンに存在するか
bool GameSceneManager::IsExistsTarget(GameObject* pTarget)
{
	EnemySpawn* pEnemySpawn = (EnemySpawn*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_SPAWN);

	//敵スポーンの同様の関数を呼び込み
	return pEnemySpawn->IsExistsTarget(pTarget);
}
//引数敵オブジェクトを強制的に削除する
void GameSceneManager::EraseEnemy(GameObject* pTarget)
{
	EnemySpawn* pEnemySpawn = (EnemySpawn*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_SPAWN);

	//敵スポーンの同様の関数を呼び込み
	pEnemySpawn->ForciblyEraseEnemy(pTarget);
}
//強制的に指定敵オブジェクトを捕縛（動きを停止）させる
void GameSceneManager::ForciblyBindEnemy(GameObject* pTarget)
{
	EnemySpawn* pEnemySpawn = (EnemySpawn*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_SPAWN);

	//敵スポーンの同様の関数を呼び込み
	pEnemySpawn->ForciblyBindEnemy(pTarget);
}
//捕縛解除
void GameSceneManager::ForciblyUnBindEnemy(GameObject* pTarget)
{
	EnemySpawn* pEnemySpawn = (EnemySpawn*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_SPAWN);

	//敵スポーンの同様の関数を呼び込み
	pEnemySpawn->ForciblyUnBindEnemy(pTarget);
}
//敵が存在するポリゴン番号をすべて知る
void GameSceneManager::GetPolygonsWithEnemies(std::vector<int>* polygonsWithEnemies)
{
	EnemySpawn* pEnemySpawn = (EnemySpawn*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_SPAWN);

	//同様の関数呼び来み
	pEnemySpawn->GetPolygonsWithEnemies(polygonsWithEnemies);
}

//プレイヤーの移動値を加算する
void GameSceneManager::MovePlayer(const int KEY_CODE)
{
	//プレイヤーの移動値を加算

	//プレイヤーのオブジェクト
	Player* pPlayer = (Player*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_PLAYER);

	MOVE_DIRECTION moveType;

	/*
	DIK_CODEにて

	移動方向を指定するようにする

	*/
	switch (KEY_CODE)
	{
	case DIK_W :
		moveType = MOVE_DIRECTION::MOVE_DIR_W; break;
	case DIK_A :
		moveType = MOVE_DIRECTION::MOVE_DIR_A; break;
	case DIK_S :
		moveType = MOVE_DIRECTION::MOVE_DIR_S; break;
	case DIK_D :
		moveType = MOVE_DIRECTION::MOVE_DIR_D; break;
	default:
		moveType = MOVE_DIRECTION::MOVE_DIR_MAX; break;

	}

	//移動量加算
	pPlayer->MovePlayer(moveType);


}
//プレイヤーのワールド座標を取得する
void GameSceneManager::RayCastForPlayerAndGround()
{
	//プレイヤーのオブジェクト
		//取得関数（自身の所有する連携オブジェクトから、ポインタを取得する（Player＊でキャストするため、　連携オブジェクトの配列に登録するときも、取得するときも、対象となるオブジェクトはPlayer＊型、或いは、それを継承したクラスである前提））
	Player* pPlayer = (Player*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_PLAYER);
		//Player* pPlayer = (Player*)objects[SAMPLE_COOP_PLAYER];

	
	//結果を入れる変数
	bool isHit;
	float dist;
	int collPolyNum;
	//当たり判定実行
	RayCastForPlayerAndGround(isHit, dist, collPolyNum);




	//rayData.hitには、
	//衝突したら、 true(1), false(0)が入ってくるので、
		//衝突したら、
		//rayData.distの値を残して、
		//衝突していなかったら
		//raydata.distの値を残さず、0.0fにする
	//true -> dist * (float)1
	//false -> dist * (float)0
	pPlayer->SetFallValueY(dist * (float)isHit , isHit);


}
//地面とのレイキャストを行い　結果を引数変数に入れて返す
void GameSceneManager::RayCastForPlayerAndGround(bool& isHit, float& dist, int& collPolyNum)
{
	//プレイヤーのオブジェクト
	//取得関数（自身の所有する連携オブジェクトから、ポインタを取得する（Player＊でキャストするため、　連携オブジェクトの配列に登録するときも、取得するときも、対象となるオブジェクトはPlayer＊型、或いは、それを継承したクラスである前提））
	Player* pPlayer = (Player*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_PLAYER);
	//Player* pPlayer = (Player*)objects[SAMPLE_COOP_PLAYER];




	//レイデータの定義
	RayCastData rayData;
	//方向
		//した
	rayData.dir = XMVectorSet(0.0f, -1.0f, 0.0f, 0.0f);


	//スタート位置
		//プレイヤー位置
	//自身のTransformを親のTransformのワールド行列で掛ける必要がある(プレイヤーの親情報を含めたワールドにする（プレイヤーに登録されているTransformは、あくまでも、プレイヤーの親からのTransformである。）)
		//そのため、親のWorldTransform、ワールド行列とかけることで、世界のワールドにおける座標を取得できる
	XMVECTOR pPos = XMVector3TransformCoord(pPlayer->transform_.position_, pPlayer->pParent_->transform_.GetWorldMatrix());
	//XMVECTOR pPos = pPlayer->transform_.position_;

	/*XMVECTOR upVec = -(rayData.dir) * 10.f;
	*/
	rayData.start = pPos; // + (upVec);	//プレイヤー位置よりも一定量高い位置からレイを飛ばす	//そうでないと、プレイヤーが地面に埋まってしまったときに、地面に対して、レイをあてることができない


	//レイキャストにおける
	//レイ方向のマイナス方向も考慮した衝突判定を行うかのフラグを上げる
		//地面とプレイヤーとのレイキャストで、
		//プレイヤーが地面に埋まったときに、プラス方向のレイだと、衝突しない場合が起きる
		//その時のために、マイナス方向も考慮して衝突判定をする。
		//衝突をし、衝突地点からの距離は、＋方向なら＋の距離で、‐方向なら―の方向で受け取ることが可能。
	rayData.permitNegativeDirection = TRUE;





	//レイキャスト実行
	RayCastWithGround(&rayData);


	//衝突していたら
	//衝突点までの距離をプレイヤーに知らせる
	//if (rayData.hit)
	//{
	//	pPlayer->SetFallValueY(rayData.dist);
	//}
	//else
	//{
	//	//衝突していなかったら
	//	//０で初期化
	//	pPlayer->SetFallValueY(0.0f);
	//}


	////結果が出たのち
	//	//スタート位置にレイの発射で、地面と当たってもらうために伸ばした、分、
	//	//最終結果のdistから、引く。
	//		//上に上がるときは、distの値がマイナスになるので、（？）　プレイヤーがY座標を計算するときは、落下と逆方向に移動できる
	//XMVECTOR upDist = XMVector3Length(upVec);

	//rayData.dist -= upDist.vecX;




	//必要情報を参照渡しの変数へ入れて返す
	isHit = rayData.hit;
	dist = rayData.dist;
	collPolyNum = rayData.polygonNum;


}
//プレイヤーのY軸回転量を取得する
float GameSceneManager::GetPlayerRotateY()
{
	//プレイヤーを取得
	Player* pPlayer = (Player*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_PLAYER);

	//Y軸回転量を取得
		//プレイヤー自体は回転をしておらず、
		//プレイヤーに追尾する、カメラが回転を行っている。
			//そのため、プレイヤーを経由して、その回転量を取得
	return pPlayer->GetPlayerRotateY();
}
//レイキャスト　敵オブジェクトと地面とのレイキャスト実行
int GameSceneManager::RayCastForEnemyAndGround(GameObject * pEnemy)
{
	//レイデータの定義
	RayCastData rayData;
	//スタート位置
		//オブジェクトのワールド座標
		//自身の座標を、自身の親のワールド行列とかけて、世界におけるワールド座標にする（この時に、自身のワールド行列とかけてしまうと、自身の移動値を移動させてしまうので、自身の親のワールド）
	XMVECTOR pPos = XMVector3TransformCoord(pEnemy->transform_.position_, pEnemy->pParent_->transform_.GetWorldMatrix());
	rayData.start = pPos + XMVectorSet(0.0f, 10.f, 0.f, 0.f);	//原点位置よりも一定量高い位置からレイを飛ばす
	//方向
		//下
	rayData.dir = XMVectorSet(0.0f, -1.0f, 0.0f, 0.0f);

	//レイキャストにおける
	//レイ方向のマイナス方向も考慮した衝突判定を行うかのフラグを上げる
		//地面とプレイヤーとのレイキャストで、
		//プレイヤーが地面に埋まったときに、プラス方向のレイだと、衝突しない場合が起きる
		//その時のために、マイナス方向も考慮して衝突判定をする。
		//衝突をし、衝突地点からの距離は、＋方向なら＋の距離で、‐方向なら―の方向で受け取ることが可能。
	rayData.permitNegativeDirection = TRUE;

	//レイキャスト実行
	RayCastWithGround(&rayData);


	//条件：衝突していたら
	if (rayData.hit)
	{
		//衝突したポリゴン番号を返す
		return rayData.polygonNum;
	}
	//衝突しなかった
	return -1;

}
//地面とのレイキャスト
void GameSceneManager::RayCastWithGround(RayCastData * pData)
{

	//地面のオブジェクトの取得
	GrassLandObject* pGrassObj = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);

	//地面オブジェクトから
	//当たり判定の対象となる、モデルのモデルハンドルの取得
	int targetHandle = pGrassObj->GetModelHandle();


	//レイキャスト実行
		//衝突していたら、
	//プレイヤーのY座標を、衝突したときの距離分、下げる
	//あるいは、落下分の距離を、保存しておき、
	//プレイヤーのUpdate時に、その距離分下げる処理を実行させる
	RayCaster* pRayCaster = new RayCaster;
	pRayCaster->RayCastToStartFromTheAnyPos(targetHandle, pData);

	SAFE_DELETE(pRayCaster);

}
//敵オブジェクトと、マウスカーソル位置との衝突を検知
EnemyInfo GameSceneManager::RayCastWithAllEnemyObject()
{
	//敵生成スポーン
	EnemySpawn* pEnemySpawn = (EnemySpawn*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_SPAWN);

	//スポーンへすべてのEnemyモデルと当たり判定を行ってもらう
		//その当たり判定の結果をもらう
	EnemyInfo info =  pEnemySpawn->RayCastToAllData();


	//受け取った情報を返す
	return info;

}
//初期描画のHelpImages描画開始時限定
void GameSceneManager::StartDrawHelpImages()
{
	{
		//ヘルプ画像群の作成
		//endingType : ゲームシーンマネージャーにおける時間停止終了実行を呼び込む
		HelpImages* pHelpImage = CreateHelpImages(460, 380, 790, 530, HELP_IMAGES_BUTTON_ALGORITHM::HELP_BUTTON_ALGORITHM_ENDING_PLUS_TELL_GAME);

		//ボタンの位置変更
		UIGroup* pUIGroup = pHelpImage->GetUIGroup();

		//ボタンマネージャーを取得
		SceneButtonManager* pSceneButtonManager = pUIGroup->GetSceneButtonManager();

		//ボタンの位置変更
		pSceneButtonManager->SetPixelPosition(0, 1000, 200);
		pSceneButtonManager->SetPixelPosition(1, 1000, 300);
		pSceneButtonManager->SetPixelPosition(2, 1000, 400);


		//背景画像の位置セット
		pHelpImage->SetBackGroundPosition(450 , 370);
		pHelpImage->SetBackGroundSize(950 , 700);

		
	}

	//初期描画のHelpImages描画開始時限定


	//時間計測停止
		//キー入力行動クラスに、世界の停止を担うクラスがあるため、そのクラスへ
		//行動を任せる
		//その処理関数は、抽象クラスである、親クラスに持たせている（だが、抽象クラスは、インスタンスを生成することはできないため）
		//継承先の適当なクラスを作成し、行動関数を呼び込む
	
		//→上記の通りに処理を書いていたが、　適当なクラスを生成した、生成時に、そのクラスの特殊行動が実行されるように作成しているため、適当なクラスを使用するわけにもいかない。
		//→そのため、親クラスを抽象クラスでなくする方式へ変える
	//GameSceneInputStrategy* pInputStrategy = new GameSceneInputStrategy(this , GAME_SCENE_INPUT_TYPE::GAME_INPUT_MAX);
	GameSceneInputStrategy* pInputStrategy = 
		new GameSceneInputStrategy(this, nullptr);


	//時間停止
	//キー入力を排除
	pInputStrategy->SetImpactInTheWorld();
	//全てのUIの非表示
	pInputStrategy->InvisibleAllUIGroup();



	//ゲームシーンの各機能を停止
		//HelpImagesの描画が完全に終了するまで、実行を停止
		//＝　HelpImagesの描画終了時に、SceneMnagerの実行を再開

	//実行終了を知らせる方法案
	/*
	�@SceneMnagerの仮想関数にて、
	　処理停止終了の処理を置こう関数を作成
		→処理実行時に、その関数をHelpImagesから呼び込む

		→だが、これでは、SceneManagerが存在しないと使えない機能になって、
		→かつ、処理停止終了の機能を、HelpImagesに操作されては都合が悪い。処理停止と、同時進行で、HelpImagesを描画したいこともある

	�AHelpImagesの処理終了後に行う行動を分割、する
		//→処理終了時の行動を　専用のクラスの実行関数として持たせる。
			//→あるいは、行動クラスの抽象クラスを継承させて、各行動クラスの作成。
				//→終了時の行動→この行動を行います。というのを、各クラスごとで持っておき、終了時に、その行動を実行するようにする
			//→ストラテジークラスのようなもの。

		//クラス数は多くなるが、行動呼び込み側と、実際の行動を分離出来て、HelpImagesにも、HelpImages終了後の行動を求めているクラスにコードを書かずに済む

		//HelpImagesの行動終了後の行動も上記によって複数パターン作れる。



	*/



}
//時間計測が行われているかの確認
bool GameSceneManager::IsPermitTimer()
{
	//条件：存在するか
	if (pCountTimer_ == nullptr)
	{
		return false;
	}
	//タイマーが計測しているか
	return pCountTimer_->IsPermitTimer();
}
//カウントタイマーのセット
void GameSceneManager::SetCountTimer(CountTimer* pCountTimer)
{
	pCountTimer_ = pCountTimer;
}
//カウントタイマーのゲット
CountTimer* GameSceneManager::GetCountTimer()
{
	return pCountTimer_;
}
//UpdateAlgorithm処理終了	を知らせる
void GameSceneManager::EndUpdateAlgorithm()
{
	isGetTag_ = true;
}
//UpdateAlgorithm処理開始	を知らせる
void GameSceneManager::StartUpdateAlgorithm()
{
	isGetTag_ = false;
}
//UpdateAlgorithm処理終了　フラグが立っているかを受け取る
bool GameSceneManager::IsUpdatingAlgorithm()
{
	return isGetTag_;
}
//初期描画のHelpImages描画終了時限定
void GameSceneManager::EndingDrawHelpImages()
{
	//初期描画のHelpImages描画終了時限定


	//更新行動フラグを下す
		//更新行動：ヘルプイメージの表示
		//を終了する
	EndUpdateAlgorithm();


}


//木オブジェクト（プレイヤー設置）ト、マウスカーソル位置との衝突を検知
TreeInfo GameSceneManager::RayCastWithAllTreeOwnedByPlayer()
{
	//木オブジェクト群（プレイヤー所有）
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);


	//グループの管理している、
		//全ての木オブジェクトと当たり判定を行い、その結果を取得する
	TreeInfo info = pGroupOfTree->RayCastToAllData();

	SAFE_DELETE(currentTreeInfo_);
	TreeInfo* newData = new TreeInfo;
	*newData = info;
	currentTreeInfo_ = newData;


	//受け取った情報を返す
	return info;
}
//引数ポリゴンの真上からレイを飛ばし衝突する木オブジェクトがあるか判定
bool GameSceneManager::RayCastFromAdoveWithAllTreeOwnedByPlayer(int polyNum)
{

	//該当ポリゴン番号をワールド座標に変換したときの
		//ワールド座標を渡し、
		//その座標へ真上からレイを飛ばす
			//その際に、衝突する木オブジェクトが存在するかの判定を行う


	//ワールド座標を入れる変数
	XMVECTOR worldPos;

	//ポリゴン番号のワールド座標を取得
	GetWorldPosOfPolygonMakeGround(&worldPos, polyNum);


	//木オブジェクト群（プレイヤー所有）
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);


	//衝突判定実行
		//その結果取得
	return pGroupOfTree->RayCastToAllData(worldPos);


}
//木オブジェクトのHPUP
bool GameSceneManager::GrowTreeParts(TREE_PARTS part)
{
	//プレイヤーの所有するオブジェクト取得
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);

	//成長呼び込み
		//この際に、
		//対象となっている、木の情報をメンバとして所有しているため、その情報を用いて
		//アクセス
	return pGroupOfTree->GrowTreeParts(currentTreeInfo_->number, part);

}
//マウスの座標と地面ポリゴンとの当たり判定
int GameSceneManager::GetMouseCursorCollisionPolygonNumber()
{
	//マウスポジションから
//衝突する地面のポリゴンを求める


//地面オブジェクトのオブジェクト取得
	GrassLandObject* pGrassObj = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);


	//地面オブジェクトから
	//当たり判定の対象となる、モデルのモデルハンドルの取得
	int targetHandle = pGrassObj->GetModelHandle();


	//レイデータの定義
		//衝突判定結果を受け取るための構造体変数
		//レイキャスターにて衝突判定を実行。その結果を以下変数に受け取る。
		//そのために空のレイデータを宣言
	RayCastData rayData;
	//レイキャストにおける
	//レイ方向のマイナス方向も考慮した衝突判定を行うかのフラグを下げる
		//マウスカーソルからのレイキャストであるため、現在のカメラ座標をワールドに変換し、その位置からレイキャストを行う。
		//そのため、現カメラ位置が、地面のくぼみにある場合、レイ方向によってはレイ方向のマイナス方向に地面ポリゴンがある場合もある。その時に衝突したことになると困る。
		//そのため、マイナス方向を考慮しないようにフラグを下ろす
	rayData.permitNegativeDirection = FALSE;


	//RayCastToStartFromTheMouseCursorの当たり判定は、レイの準備も、RayCastToStartFromTheMouseCursorにて、用意するので、
	//空の実体だけ用意

	//レイキャスト実行
	RayCaster* pRayCaster = new RayCaster;
	pRayCaster->RayCastToStartFromTheMouseCursor(targetHandle, &rayData);
	SAFE_DELETE(pRayCaster);

	//実行結果にて
	//条件：衝突していたら
	if (rayData.hit)
	{
		//最後に衝突したポリゴンの更新
		lastCollisionPolyNum_ = rayData.polygonNum;

		//衝突したポリゴンの番号を返す
		return rayData.polygonNum;
	}

	lastCollisionPolyNum_ = -1;
	//衝突したポリゴンがなかったので
	//−１　を返す
	return -1;
}
//木をはやす（木が生成可能ならば生成呼出し）
bool GameSceneManager::GrowTree(int type)
{
	//木を生やす
	int collisionPolyNum = -1;



	//条件：生成可能か
	if(IsCreateTree(&collisionPolyNum))
	{
		//木を生やす処理の実行
		GrowTree(type , collisionPolyNum);
	}
	else
	{
		return false;
	}


	return true;


}
//木を生やす（木の生成実行）
void GameSceneManager::GrowTree(int type , int polyNum)
{
	//そのポリゴン番号の
		//ワールド座標を取得する

	XMVECTOR localToWorld;

	//ポリゴン番号から
	//ローカル座標を取得し、ワールド座標を取得する
	//条件：ポリゴン番号を取得できたか
	if (GetWorldPosOfPolygonMakeGround(&localToWorld, polyNum))
	{
		//ワールド座標を
		//新規のTransformにセットして、
			//そのTransformをもとに、
			//新たに木を生成させる

		//ただし、
		//その木は背景用ではなく、
			//動的に変化をさせる木のため、
			//番号を登録しておく

		//変形後の座標を新規確保のTransformにセットして、
		//そのTransformをもとに、
			//木の生成位置を確定する
			//Transoformのスケールも同時に設定する
		Transform trans = GetTreeTransform(localToWorld , 0.5f);

		//プレイヤーの所有している
			//木オブジェクト群のインスタンスにアクセスして
			//その木オブジェクト群にオブジェクトを追加する
		//木オブジェクト群のインスタンス取得
			//SAMPLE_SCENE_OBJECTS：：SAMPLE_SCENE_TREE_OWNED_BY_PLAYER
		GroupOfTree* pGroupOfTreeOwnedByPlayer = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);

		//新たな木オブジェクトの生成
			//LsystemObjectの親を上記で確した、GroupOfTreeとする
			//それをGroupOfTreeにインスタンスの生成を任せる（詳細は関数先にて）
		//LSystemTree* pNewTree = nullptr;
		Tree* pNewTree = nullptr;

		//LsystemObjectの初期化を行う
		//Transform値の調整などなど
		//描画のための設定を行ってもらう
			//ポインタの参照渡し
		pGroupOfTreeOwnedByPlayer->InitTreeObject(type , &pNewTree);


		////描画位置と、
		//	//コライダーの位置に差を与えないために
		//	//Treeオブジェクトの、Positionを描画位置のTransformと同じにする
		//	//この際にセットするポジションは、描画位置のWorldと同様でよい（あくまでも、コライダーの位置を揃えるためのWorldのため、描画や、計算の上で、正確な位置が必要なわけではない）
		//pNewTree->transform_.position_ = trans.position_;

		//木オブジェクトを生成
			//GroupOfTreeのリスト群に追加して、
			//引数にて渡したLsystemObjectのポインタをリストに追加
			//そして、引数のTransform値に描画させる
		pGroupOfTreeOwnedByPlayer->AddTreeObject(trans, polyNum, pNewTree);



		//条件：木のタイプが通常の木であった場合
			//Grassクラスを木を生成したポリゴンと同様の位置に生成する
			//他の木のタイプの場合、生成しないが、
			//その場合の、Grass消去時の処理は考えなくてもよい。
				//消去時に指定した、ポリゴンが存在しなければ消去しない。
		if (type == (int)TREE_TYPES::TREE_TYPE_DEFAULT)
		{

			//GrassLandのポリゴンを管理するクラス
			GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);

			//木オブジェクトと同様に
			//草オブジェクトを生成させる
			pGrassLand->CreateGrassPolygon(polyNum);
		}


		//テスト
			//敵オブジェクトに
			//木が生成されたことを知らせて、
			//その木オブジェクトに移動させる
		InformEnemySpawnThatTreeHaveGrown(polyNum);
	}
}
//木を生成できるか
bool GameSceneManager::IsCreateTree(int* collisionPolyNum)
{
	//木を生成可能個数を超えていないかの判定を行う
	//上限を超えている場合は、生成しない

//プレイヤーの所有するオブジェクト取得
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);

	//条件：生成可能上限を超えてしまっている場合
	if (!pGroupOfTree->IsCreateTreeLimitExceeded())
	{
		//生成せずに終了
		return false;
	}




	//条件：衝突したポリゴンがあったなら
	if (lastCollisionPolyNum_ == -1)
	{
		//地面との衝突したポリゴンの番号を取得
		(*collisionPolyNum) = GetMouseCursorCollisionPolygonNumber();
	}
	else
	{
		(*collisionPolyNum) = lastCollisionPolyNum_;
	}

	//条件：最終結果が木を生成するポリゴン番号が‐1以外ならば、True生成可能
	//ー１ならば、False、生成不可能と返す
	if ((*collisionPolyNum) != -1)
	{
		return true;
	}
	else
	{
		return false;
	}

}
//木が生やされたことを敵オブジェクトスポーンに報告する
void GameSceneManager::InformEnemySpawnThatTreeHaveGrown(int polyNumWithTree)
{
	//司令塔へ
	//次の攻撃対象として、
		//木を生成したポリゴン番号を伝える
		//余裕のある敵がいたら、その敵をそのポリゴン番号へ移動させる


	//知らせる
	//敵司令塔の取得
	EnemyCommandTower* pEnemyCommandTower = (EnemyCommandTower*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_COMMAND_TOWER);

	//引数のポリゴンの番号を
		//木が生えたポリゴンの番号を
		//目的の目標位置として知らせる
	pEnemyCommandTower->StartNavigation(polyNumWithTree);



}
//草オブジェクトを狭める
void GameSceneManager::StartShrinkingGrass(int targetPolyNum)
{
	//GrassLandObject→GrassLandから、所有しているGrass群から該当のpolyNumを持つクラスを探す→Grass自身が、縮小開始
	GrassLandObject* pGrassObject = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);
	//橋渡し
	pGrassObject->StartShrinkingGrass(targetPolyNum);


}

//地面の合計ポリゴンのうち、Grassが繁殖している数取得
int GameSceneManager::GetTotalNumberOfPolygonWithGrass()
{
	//GrassLandのポリゴンを管理するクラス
	GrassLandObject * pGrassLand = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);

	//ポリゴン数合計を取得する
	return pGrassLand->GetTotalNumberOfPolygonWithGrass();
}
//地面を作るポリゴン数の合計を取得
int GameSceneManager::GetTotalPolygonCountOfGrassLand()
{	
	//GrassLandのポリゴンを管理するクラス
	GrassLandObject * pGrassLand = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);

	return pGrassLand->GetPolygonCount();
}
//背景となる、木オブジェクトの生成
void GameSceneManager::CreateBackGroundTrees()
{
	//地面のオブジェクトの四方を木オブジェクトで囲ませる
	/*
	
	そのポリゴンの「４」ポリゴンごとに、
	ポリゴン番号を取得し、

ポリゴンを作る3頂点の
中心の座標を求めて、
中心の座標を、
ローカル座標でもらう。

もらった、ローカル座標を、
Transformを使って、ワールド座標にする。

その座標に、
木オブジェクトを作成

これを、
地面の上辺
地面の下辺
地面の右辺
地面の左辺　で行う


	
	
	*/


	//ポリゴン数の取得
	//GrassLandのポリゴンを管理するクラス
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);
	int polygonCount = pGrassLand->GetPolygonCount();

	//幅の頂点数、奥行きの頂点数の取得
	int width = -1;
	int depth = -1;
	pGrassLand->GetWidthAndDepth(&width, &depth);


	//木オブジェクトを管理するクラス
		//新しい描画先を指定して、描画してもらうクラス
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_BACK_GROUND_TREE);



	//木を設置するための情報群
	struct polygonInfo
	{
		int polygonNumber;	//ポリゴン番号

		TREE_DIRECTION direction;	//回転方向として扱う値

	};


	//ポリゴン番号
	//一行のポリゴン数
		// width - 1
	std::vector<polygonInfo> polygonNumbers;
	polygonNumbers.clear();


	//木オブジェクトを生成する間隔
		//ポリゴンの間隔
		//幅
	static constexpr unsigned int POLYGON_WIDTH_DISTANCE = 4;
		//ポリゴンの間隔
		//奥行
	static constexpr unsigned int POLYGON_HEIGHT_DISTANCE = 8;
	



	//ポリゴン番号の登録

	//地面オブジェクトの上辺
	//地面オブジェクトの下辺
	//ポリゴン１から
	//width - 1になるまで繰り返す
	//４ポリゴンごと
	for (int i = 1; i < (width - 1); i += POLYGON_WIDTH_DISTANCE)
	{
		//上辺
		{
			//登録構造体
			polygonInfo up;
			up.polygonNumber = i;
			up.direction = UP_DIRECTION;
			polygonNumbers.push_back(up);
		}

		//下辺
		{
			//登録構造体
			polygonInfo down;
			down.polygonNumber = polygonCount - width - 1 + i;
			down.direction = DOWN_DIRECTION;
			polygonNumbers.push_back(down);
		}
	}

	//地面オブジェクトの右辺
	//地面オブジェクトの左辺
		//縦のポリゴン数は
		//一行に２つあるため、
		// depthの２倍のポリゴンが存在することになる
	for (int i = 1; i < (depth*2 - 1); i += POLYGON_HEIGHT_DISTANCE)
	{
		//左辺
			//iは1から始まり、
			//仮に　width = 5;
			//1行目　１
			//2行目　５
			//3行目　９
			//4行目　１３

			//連続の法則性
				//１から width - 1ずつ増えていく
		int polyLeftNum = 1 + ((width - 1) * (i - 1));
		{
			//登録構造体
			polygonInfo left;
			left.polygonNumber = polyLeftNum;
			left.direction = LEFT_DIRECTION;
			polygonNumbers.push_back(left);
		}

		//右辺
			//連続の法則性
			//leftの　-1 
		int polyRightNum = polyLeftNum + (width - 2);
		{
			//登録構造体
			polygonInfo right;
			right.polygonNumber = polyRightNum;
			right.direction = RIGHT_DIRECTION;
			polygonNumbers.push_back(right);
		}

	}




	

	//ポリゴン番号を登録した可変長配列から
		//番号を取得し
		//その番号からTransformを取得し、
		//木オブジェクトを生成する
	//条件：配置するポリゴンの数
	for (int i = 0; i < polygonNumbers.size(); i++)
	{

		//ポリゴンのローカル座標を入れる変数
		XMVECTOR localToWorld;

		//ポリゴン番号から
		//ローカル座標を取得し、ワールド座標を取得する
		//条件：ポリゴンを取得できたか
		if (GetWorldPosOfPolygonMakeGround(&localToWorld, polygonNumbers[i].polygonNumber))
		{
			//ポリゴン番号が存在し、
				//ローカル座標を受け取れたら
				//木オブジェクトを生成する


			//変形後の座標を新規確保のTransformにセットして、
			//そのTransformをもとに、
				//木の生成位置を確定する

			//木のTransformを取得する
			Transform trans = GetTreeTransform(localToWorld , 0.9f);
			trans.rotate_.vecY = (float)polygonNumbers[i].direction;	//enumの値を回転値として登録

			//木の追加
			pGroupOfTree->AddTreeObject(trans , polygonNumbers[i].polygonNumber);

		}
	}

}
//木へ攻撃実行（任意ダメージを与える）
bool GameSceneManager::AttackSuccess(const int TARGET_POLY_NUM, const int DAMAGE, bool* isKill, int* treeNum)
{
	//プレイヤーの所有するオブジェクト取得
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);

	//同様関数の呼び込み
	return pGroupOfTree->AttackSuccess(TARGET_POLY_NUM , DAMAGE , isKill , treeNum);
}
//TreeオブジェクトのTransformを取得する
Transform GameSceneManager::GetTreeTransform(XMVECTOR& localToWorld , float initSize)
{
	Transform trans;
	trans.position_ = localToWorld;
	trans.scale_ = XMVectorSet(initSize, initSize, initSize, 0.0f);

	return trans;

}
//指定ポリゴン番号のローカル座標を取得
bool GameSceneManager::GetLocalPosOfPolygonMakeGround(XMVECTOR* localPos , int polyNum)
{
	//GrassLandのポリゴンを管理するクラスに
	GrassLandObject* pGrassLand =  (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);
	//ローカル座標を求める関数を呼び込み
	return pGrassLand->GetLocalPosOfPolygon(localPos, polyNum);

}
//ローカル座標を引数Transformでワールド座標に変換
void GameSceneManager::TransformedInTheWorldMatrix(XMVECTOR * toPos, Transform & trans)
{
	//ワールド行列の計算
	trans.Calclation();

	//変換
	(*toPos) = XMVector3TransformCoord((*toPos), trans.GetWorldMatrix());


}
//ポリゴン番号のワールド座標を取得する
bool GameSceneManager::GetWorldPosOfPolygonMakeGround(XMVECTOR* localToWorld , int polyNum)
{


	//ポリゴン番号から、ポリゴンの中心のローカル座標を取得
	//条件：ポリゴンが取得できた
	if (GetLocalPosOfPolygonMakeGround(localToWorld, polyNum))
	{
		//ポリゴン番号が存在し、
			//ローカル座標を受け取れたら
			//木オブジェクトを生成する

		//GrassLandのポリゴンを管理するクラス
		GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);

		//そのローカル座標を
		//そのポリゴンのTransformである、
		//pGrassLand_のTransformを使い、ワールド座標に変形させる
		TransformedInTheWorldMatrix(localToWorld, pGrassLand->transform_);

		return true;
	}


	return false;
}
//地面オブジェクトの横頂点数と奥頂点数を取得
void GameSceneManager::GetVertexesWidthCountAndDepthCount(int * width, int * depth)
{
	//GrassLandのポリゴンを管理するクラス
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);

	pGrassLand->GetWidthAndDepth(width, depth);
}
//指定ポリゴンがGrassとして、塗られているか、塗られていないか
bool GameSceneManager::IsGreenPolygon(int polyNum)
{
	//GrassLandのポリゴンを管理するクラス
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);

	return pGrassLand->IsGreenPolygon(polyNum);
}
//木を強制的に倒木させる
void GameSceneManager::ForciblyEraseTheTree()
{
	//プレイヤーの所有するオブジェクト取得
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);

	//削除呼び込み
		//メンバに保存しておいた、木の情報から、木の番号、ハンドルを取得し、消去
	pGroupOfTree->ForciblyEraseTheTree(currentTreeInfo_->number);

}
//木が存在するポリゴン番号をすべて知る
void GameSceneManager::GetPolygonsWithTrees(std::vector<int>* polygonsWithTrees, std::vector<int>* treeTypes)
{
	//木グループ取得
		//プレイヤーの木グループ
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);

	//同様の関数呼び来み
	pGroupOfTree->GetPolygonsWithTrees(polygonsWithTrees , treeTypes);

}
//プレイヤーのワールド座標を取得する
XMVECTOR GameSceneManager::GetPlayerWorldPos()
{
	//プレイヤー取得
	Player* pPlayer = (Player*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_PLAYER);

	//プレイヤーのワールド座標取得
	return pPlayer->GetWorldPos();

}
//時間を停止する（世界の停止）	
void GameSceneManager::StopTimeInTheWorld()
{
	//敵オブジェクトの動きを止める
		//敵スポーン→各敵オブジェクト　
		//動きを止める（攻撃の停止、移動の停止、時間によるHP減少の停止）
	{
		//敵スポーンの取得
		EnemySpawn* pEnemySpawn = (EnemySpawn*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_SPAWN);

		//スポーンへ時間停止を宣言
		pEnemySpawn->StopTimeAllObject();

	}


	//木オブジェクトの動きを止める
		//木グループ→各木オブジェクト
		//（成長停止）
	{
		//プレイヤーの所有している
				//木オブジェクト群
		GroupOfTree* pGroupOfTreeOwnedByPlayer = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);

		//時間停止
		pGroupOfTreeOwnedByPlayer->StopTimeAllTree();

	}

	//草オブジェクトの動きを止める
		//草原オブジェクト→草原
		//成長停止、時間計測停止
	{
		GrassLandObject* pGrassLandObject = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);
		//時間停止
		pGrassLandObject->StopTime();
	}

	//敵の掲示板（会話描画クラス）
		//掲示板クラス
		//テキスト描画停止、UIGroupのUI描画停止、時間計測停止
	{
		EnemyStrategyBulletinBoard* pBulletinBoard = (EnemyStrategyBulletinBoard*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_BULLETIN_BOARD);
		//時間停止
		pBulletinBoard->StopTime();
	}

	//条件：カウントタイマーを所有しているか
	if (pCountTimer_ != nullptr)
	{
		//ステージ残り時間の停止
			//自クラスのタイマークラス
			//（タイマー停止）
		pCountTimer_->NotPermittedTimer(0);
	}

	//残り生成可能本数　テキストの非描画
	InvisibleTreeNum();

}
//時間計測を再開する（世界の停止）
void GameSceneManager::StartTimeInTheWorld()
{
	//StopTimeInTheWorldの逆を行う


	//敵オブジェクトの動きを再開
	{
		//敵スポーンの取得
		EnemySpawn* pEnemySpawn = (EnemySpawn*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_ENEMY_SPAWN);

		//スポーンへ時間計測再開を宣言
		pEnemySpawn->StartTimeAllObject();
	}

	//木オブジェクトの動きを再開
	{
		//プレイヤーの所有している
				//木オブジェクト群
		GroupOfTree* pGroupOfTreeOwnedByPlayer = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);

		//時間計測再開
		pGroupOfTreeOwnedByPlayer->StartTimeAllTree();

	}

	//草オブジェクトの動きを再開
		//草原オブジェクト→草原
		//成長再開、時間計測再開
	{
		GrassLandObject* pGrassLandObject = (GrassLandObject*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GRASS_LAND);
		//時間計測再開
		pGrassLandObject->StartTime();
	}

	//敵の掲示板（会話描画クラス）
		//掲示板クラス
		//テキスト描画再開、UIGroupのUI描画再開、時間計測再開
	{
		EnemyStrategyBulletinBoard* pBulletinBoard = (EnemyStrategyBulletinBoard*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_BULLETIN_BOARD);
		//時間再開
		pBulletinBoard->StartTime();
	}

	//条件：カウントタイマー取得しているか
	if (pCountTimer_ != nullptr)
	{
		//ステージ残り時間の再開
			//自クラスのタイマークラス
			//（タイマー再開）
		pCountTimer_->PermitTimer(0);
	}

	//残り生成可能本数　テキストの描画
	VisibleTreeNum();

}
//ゲームマップ生成
void GameSceneManager::CreateGameMap()
{
	//ゲームマップ取得
	GameMap* pGameMap = (GameMap*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GAME_MAP);

	//マップ生成呼び込み
	pGameMap->CreteMap();
}
//ゲームマップ終了
void GameSceneManager::EndGameMap()
{
	//ゲームマップ取得
	GameMap* pGameMap = (GameMap*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GAME_MAP);

	//マップ終了
	pGameMap->EndMap();
}
//立地予定の木を描画
void GameSceneManager::CreatePlannedLocationOfTree(int polyNum)
{
	//GroupOfTreeに任せる
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);

	//ワールド座標の変数
	XMVECTOR setPos;
	//立地予定地のポリゴンから示されるワールド座標をセット
	this->GetWorldPosOfPolygonMakeGround(&setPos, polyNum);

	//GroupOfTreeの同関数呼ぶこむ
	pGroupOfTree->CreatePlannedLocationOfTree(setPos);

}
//立地予定木の削除
void GameSceneManager::ErasePlannedLocationOfTree()
{
	//GroupOfTreeの同関数呼び込み
	//GroupOfTreeに任せる
	GroupOfTree* pGroupOfTree = (GroupOfTree*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_TREE_OWNED_BY_PLAYER);
	pGroupOfTree->ErasePlannedLocationOfTree();


}
//残り生成可能木本数　テキストを描画許可
void GameSceneManager::VisibleTreeNum()
{
	visibleTreeNum_ = true;
}
//残り生成可能木本数　テキストを描画拒否
void GameSceneManager::InvisibleTreeNum()
{
	visibleTreeNum_ = false;
}

//デフォルトのデバイス入力（キー入力、マウス入力）を追加
void GameSceneManager::SetStrategyForDefaultEachKey()
{
	//SceneUserInputterの取得
	SceneUserInputter* pSceneUserInputter = (SceneUserInputter*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GAME_INPUTER);


	//ユーザー入力作成用のファクトリークラスの作成
	// ゲームシーン専用であることを確認
	//GameSceneInputStrategyより
	FactoryForGameSceneInputStrategy* pFactory = new FactoryForGameSceneInputStrategy();

	//デフォルトのキー、マウス入力情報を取得
		//ポインタの配列なので、ポインタのポインタ
	DefaultKeyInfo** pDefaultKeys_ = StageData::GetDefaultKeyInfo();

	//デフォルトキー、マウスの情報を読み込み、
	//一つ一つのキーを追加していく
		//キーの詳細は
		//StageDataを参照
	//条件：通常キーのMAX（enum）
	for (int i = 0; i < (int)DEFAULT_KEY_TYPE::MAX; i++)
	{
		//入力情報の作成
		CreateInputStrategyInfo* pCreateInfo = new CreateInputStrategyInfo(
			pDefaultKeys_[i]->inputPushType ,
			pDefaultKeys_[i]->keyOrMouse ,
			pDefaultKeys_[i]->keyCode );


		//入力追加
			//入力情報と他もろもろの情報を渡す。
			//キー入力を登録し、次のフレームから入力受け付けを行わせる
		pFactory->CreateGameSceneInputStrategy(
			pDefaultKeys_[i]->algorithmType ,
			this ,
			pSceneUserInputter,
			pCreateInfo);

	}

	//生成クラスの消去
	SAFE_DELETE(pFactory);

}
//デフォルトのデバイス入力（キー入力、マウス入力）を消去
void GameSceneManager::EraseStrategyForDefaultEachKey()
{
	//SceneUserInputterの取得
	SceneUserInputter* pSceneUserInputter = (SceneUserInputter*)GetCoopObject((int)GAME_SCENE_COOP_OBJECTS::GAME_COOP_GAME_INPUTER);

	//デフォルトのキー、マウス入力情報を取得
		//ポインタの配列なので、ポインタのポインタ
	DefaultKeyInfo** pDefaultKeys_ = StageData::GetDefaultKeyInfo();

	//２次元配列の情報にアクセスして、
	//一つずつ解放していく
	//条件：通常キーのMAX（enum）
	for (int i = 0; i < (int)DEFAULT_KEY_TYPE::MAX; i++)
	{
		//マウスかキーか調べる
		//条件：キーか
		if (pDefaultKeys_[i]->keyOrMouse == INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_KEY)
		{
			pSceneUserInputter->RequestSetEmptyKeyCode(pDefaultKeys_[i]->keyCode);
		}
		//条件：マウスか
		else if(pDefaultKeys_[i]->keyOrMouse == INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_MOUSE)
		{
			pSceneUserInputter->RequestSetEmptyMouseCode((MOUSE_CODE)pDefaultKeys_[i]->keyCode);
		}

	}
	//本来は、
	//他にキーコード入力を行わないようにしているため、
	//全てのキーコードを解放する関数を呼んでもよいのだが、
		//きちんとデフォルトキーの解放を行っていることが分かるように、一つのキーごとに解放を呼び込む

}
