#pragma once

//enum class のプロトタイプ宣言
enum class KEY_AND_MOUSE_INPUT_PUSH_TYPE;
enum class INPUT_TYPE_KEY_OR_MOUSE;
enum class GAME_SCENE_INPUT_ALGORITHM;

//GameSceneにおいて使用する　通常のキー入力の種類
	//詳細：GameSceneというシーン限定で使用する、デバイスの入力。その入力の種類
	//　　：ゲーム中に、入力の停止、入力の再開を行うことがある。その際に、通常入力（移動、視点操作など）を一度に戻してしまいたい、。何度も同じく戻すものであれば、あらかじめ定義しておきたい。
enum class DEFAULT_KEY_TYPE
{
	MOUSE_LEFT = 0,
	MOUSE_RIGHT,
	ESC,
	Q,
	W,
	A,
	S,
	D,

	MAX,
};

/*
	構造体詳細	：GameSceneにおいて使用する　通常のキー入力　入力における必要情報を持たせた構造体
	構造体概要（詳しく）
				：キー入力を停止、再開を行う際に設定する処理に必要な情報
				　キー入力のキーコード、
				　入力をされた際に、どのような処理を行うのか
*/
struct DefaultKeyInfo
{
	//入力の押下タイプ（常に押している、初めて押したなど）
		//詳細：キーに対する、どの方法で入力したという判別とするか
	KEY_AND_MOUSE_INPUT_PUSH_TYPE inputPushType;
	//入力デバイス
		//詳細：入力を行った際に、キーボードであるか、マウスであるかにより、入力受け取りの処理が違ってくる。そのため、分類分けを行う
	INPUT_TYPE_KEY_OR_MOUSE keyOrMouse;
	//キーコード
		//詳細：入力受付を行ってほしいコード。キーボードであるならば、入力受付を行ってほしいキー。マウスであるならば、マウスのボタン。
	int keyCode;
	//入力時の実行アルゴリズム
		//詳細：入力がされた際に、行う行動。入力をされたら移動を行うなど。行動を識別するタイプ。
	GAME_SCENE_INPUT_ALGORITHM algorithmType;

	//コンストラクタ
	DefaultKeyInfo(
		KEY_AND_MOUSE_INPUT_PUSH_TYPE ipType , 
		INPUT_TYPE_KEY_OR_MOUSE komType , 
		int code , 
		GAME_SCENE_INPUT_ALGORITHM aType) :

		inputPushType(ipType) , 
		keyOrMouse(komType) , 
		keyCode(code)  , 
		algorithmType(aType)
	{

	}

};

/*
	構造体詳細	：GameSceneにおけるステージ情報を保存しておく情報名前空間
	構造体概要（詳しく）
				：ステージの制限時間。
				　ステージのクリア条件などのステージ情報を保存しておく。
				　ステージごとに可変にする必要がある場合、外部からのファイルで、ステージ情報を登録する。

*/
namespace StageData
{
	
	//ステージの制限時間（ｓ）
		//詳細：ステージにおける、ゲーム終了までの制限時間
		//制限：単位　ｓ
	static constexpr float TIME_LIMIT = 120.f;

	//クリアの基準値（％）
		//詳細：クリアに必要な緑の豊かさ（繁殖率）
		//　　：ゲーム終了時、自身の緑の豊かさの値が、下記を上回っていた場合ゲームクリアとなる。下回っていた場合、ゲームオーバーとなる。
		//制限：単位　％
	static constexpr float CLEAR_BREEDING_RATE = 75.f;


	//初期化
		//詳細：ステージ情報の初期化
		//　　：デフォルトキー、マウスの情報登録
		//引数：なし
		//戻値：なし
	void Initialize();
	//解放
		//詳細：ステージ情報の解放
		//　　：デフォルトキー、マウスの情報解放
		//引数：なし
		//戻値：なし
	void Release();


	//デフォルト（ステージ移動時のキー受付を行う）キー入力、マウス入力の情報を取得
		//詳細：入力停止、入力再開を行う際にキー情報を貰う必要がある。そのために、以下の関数で呼び出す
		//引数：なし
		//戻値：なし
	DefaultKeyInfo** GetDefaultKeyInfo();


}
