#include "GameSceneButtonStrategy.h"		//ヘッダ
#include "../../CommonData/GlobalMacro.h"	//共通マクロや定数保存先
#include "../Tree/Tree.h"					//木のオブジェクト
#include "GameSceneManager.h"				//ゲームマネージャー
#include "GameSceneInputStrategy.h"			//キー入力における行動アルゴリズムクラス
											//ゲームのキー入力における行動をセットする際のenum取得のため



/*行動クラス（シーンごとのシーン名SceneButtonStrategyを継承したクラス）宣言*******************************************************************************************************/
	//詳細：Factoryクラス（行動クラス生成クラス）にてインスタンスの動的確保を行うため、Factoryの実装よりも前に記述
	//詳細：行動クラスを複数作ることになるため、行動クラスごとにクラスファイルを作成すると、クラスファイル量が多くなる。
	//　　：→上記を避けるために、一つのクラスに行動クラスをまとめる。（メソッドやメンバも少ないため、ひとくくりにしてしまう。）
/*****************************************************************/
/*通常木作成　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CREATE_TREE*/
class CreateTreeStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	CreateTreeStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
	//デバイス入力時　実行する行動
		//詳細：引数にて木のタイプを指定し、行動アルゴリズムを行う
		//引数：木タイプ
		//戻値：なし
	void AlgorithmInterface(TREE_TYPES type);
};
/*****************************************************************/
/*****************************************************************/
/*ダミー木作成　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CREATE_DUMMY_TREE*/
class CreateDummyTreeStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	CreateDummyTreeStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*バインド木作成　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CREATE_BIND_TREE*/
class CreateBindTreeStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	CreateBindTreeStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*イーター木作成　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CREATE_EATER_TREE*/
class CreateEaterTreeStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	CreateEaterTreeStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/

/*****************************************************************/
/*木作成UI群を閉じる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CLOSE_UI_CREATE_TREE*/
class UICloseForCreateTreeStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	UICloseForCreateTreeStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*敵情報看板UI群を閉じる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CLOSE_UI_ENEMY_INFO*/
class UICloseForEnemyInfoStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	UICloseForEnemyInfoStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*木情報看板UI群を閉じる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CLOSE_UI_TREE_INFO*/
class UICloseForTreeInfoStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	UICloseForTreeInfoStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*木の幹を成長させる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_GROW_TRUNK*/
class GrowTheTrunkStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	GrowTheTrunkStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*木の根を成長させる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_GROW_ROOT*/
class GrowTheRootStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	GrowTheRootStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*木の削除　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_ERASE_TREE*/
class EraseTreeStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	EraseTreeStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*タイトルへ戻る　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_BACK_TITLE_SCENE*/
class BackTitleSceneStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	BackTitleSceneStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*タイトルへ戻るUI群を閉じる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CLOSE_UI_BACK_TITLE_SCENE*/
class UICloseForBackTitleSceneStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	UICloseForBackTitleSceneStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/
/*****************************************************************/
/*ゲームマップを閉じる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CLOSE_GAME_MAP*/
class UICloseForGameMapStrategy : public GameSceneButtonStrategy
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	UICloseForGameMapStrategy(GameSceneManager* pSceneManager);

	//デバイス入力時　実行する行動
		//詳細：デバイスの入力を受け付けた後、呼び込む。入力を受けての行動アルゴリズム
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void AlgorithmInterface() override;
};
/*****************************************************************/



/*行動クラス生成クラス　実装*******************************************************************************************************/
/*Factory*/
//コンストラクタ
FactoryForGameSceneButtonStrategy::FactoryForGameSceneButtonStrategy() :
	pParent_(nullptr)
{
}

//ストラテジークラスを生成の委託
//パターン：FactoryMethod
GameSceneButtonStrategy * FactoryForGameSceneButtonStrategy::CreateGameSceneButtonStrategy(
	GAME_SCENE_BUTTON_ALGORITHM type , GameSceneManager* pSceneManager)
{
	switch (type)
	{
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_TREE:
		//インスタンスを生成して
			//その戻値のポインタを、親のクラスでキャストして返す
		return (GameSceneButtonStrategy*)(new CreateTreeStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_DUMMY_TREE:
		return (GameSceneButtonStrategy*)(new CreateDummyTreeStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_BIND_TREE:
		return (GameSceneButtonStrategy*)(new CreateBindTreeStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_EATER_TREE:
		return (GameSceneButtonStrategy*)(new CreateEaterTreeStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_UI_CREATE_TREE :
		return (GameSceneButtonStrategy*)(new UICloseForCreateTreeStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_UI_ENEMY_INFO:
		return (GameSceneButtonStrategy*)(new UICloseForEnemyInfoStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_UI_TREE_INFO:
		return (GameSceneButtonStrategy*)(new UICloseForTreeInfoStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_GROW_TRUNK:
		return (GameSceneButtonStrategy*)(new GrowTheTrunkStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_GROW_ROOT :
		return (GameSceneButtonStrategy*)(new GrowTheRootStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_ERASE_TREE:
		return (GameSceneButtonStrategy*)(new EraseTreeStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_BACK_TITLE_SCENE :
		return (GameSceneButtonStrategy*)(new BackTitleSceneStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_UI_BACK_TITLE_SCENE:
		return (GameSceneButtonStrategy*)(new UICloseForBackTitleSceneStrategy(pSceneManager));
	case GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_GAME_MAP:
		return (GameSceneButtonStrategy*)(new UICloseForGameMapStrategy(pSceneManager));


	
	default : 
		return nullptr;
		break;
	};



	return nullptr;
}
/*****************************************************************/






/*シーンごとのユーザー入力時の行動クラス　親クラス　実装*******************************************************************************************************/
//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込みにより初期化
GameSceneButtonStrategy::GameSceneButtonStrategy(GameSceneManager * pSceneManager):
	pGameSceneManager_(pSceneManager)
{
}
//コンストラクタ
GameSceneButtonStrategy::GameSceneButtonStrategy() :
	GameSceneButtonStrategy(nullptr)
{
}
//デバイス入力時　実行する行動
void GameSceneButtonStrategy::AlgorithmInterface()
{
}
//UIGroupの表示、非表示の切替え
void GameSceneButtonStrategy::VisibleInvertUIGroup(GAME_SCENE_UI_GROUP_TYPE UIGroupType)
{
	//表示、非表示切り替え
	pGameSceneManager_->VisibleInvertSceneUIGroup((int)UIGroupType);
}
//世界に影響を与える（再開）
void GameSceneButtonStrategy::SetImpactInTheWorld()
{
	//プレイシーンにおける通常のキー入力を回復する
	//UI表示における、キー入力をNULLにしたものを回復する
	SetDefaultEachInputKey();

	//常駐のUIの再表示
		//UI表示のために、一度非表示にしたものを再表示
	DispResidentUIGroups();

	//敵、木などのオブジェクトの動的処理の再開
	SetTimeStartForMoveingObject();


}
//プレイ中の通常の入力キーのセット
void GameSceneButtonStrategy::SetDefaultEachInputKey()
{
	//通常時のキー入力を回復させる
	/*
	//キー入力を回復する
		//サンプルシーンのマネージャーに、キー変更を請け負ってもらう
	//キー：マウスの左クリックにて
	//行動：木生成UIの表示
	pGameSceneManager_->SetStrategyForEachKey(GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_CREATE_TREE, GAME_SCENE_INPUT_TYPE::GAME_INPUT_MOUSE_LEFT);

	//キー：マウスの右クリックにて
	//行動：敵情報　OR 　木情報UIの表示
	pGameSceneManager_->SetStrategyForEachKey(GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_TREE_INFO_OR_ENEMY_INFO, GAME_SCENE_INPUT_TYPE::GAME_INPUT_MOUSE_RIGHT);

	//キー：ESC
	//行動：タイトルへ戻る
	pGameSceneManager_->SetStrategyForEachKey(GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_BACK_TITLE_SCENE, GAME_SCENE_INPUT_TYPE::GAME_INPUT_ESC);


	//Q：ゲームマップ生成　UI表示
	pGameSceneManager_->SetStrategyForEachKey(GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_CREAE_GAME_MAP, GAME_SCENE_INPUT_TYPE::GAME_INPUT_Q);


	//キー：WASD
	//行動：移動（WASD）
	{
		//W：移動W
		pGameSceneManager_->SetStrategyForEachKey(GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER, GAME_SCENE_INPUT_TYPE::GAME_INPUT_W);
		//A：移動A
		pGameSceneManager_->SetStrategyForEachKey(GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER, GAME_SCENE_INPUT_TYPE::GAME_INPUT_A);
		//S：移動S
		pGameSceneManager_->SetStrategyForEachKey(GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER, GAME_SCENE_INPUT_TYPE::GAME_INPUT_S);
		//D：移動D
		pGameSceneManager_->SetStrategyForEachKey(GAME_SCENE_INPUT_ALGORITHM::GAME_INPUT_ALGORITHM_MOVE_PLAYER, GAME_SCENE_INPUT_TYPE::GAME_INPUT_D);
	}
	*/

	//GameSceneManagerの専用の関数を呼び込み
	//入力回復を図る
	pGameSceneManager_->SetStrategyForDefaultEachKey();


}
//プレイシーンのおける常駐UIの表示
void GameSceneButtonStrategy::DispResidentUIGroups()
{
	//繁殖率　背景UI
	pGameSceneManager_->VisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_PLANT_DENSITY);
	//タイマー　背景UI
	pGameSceneManager_->VisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_TIMER);
	//FPS酔い止めUI
	pGameSceneManager_->VisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_FPS_CURSOR);
	//残り生成可能個数　背景UI
	pGameSceneManager_->VisibleSceneUIGroup((int)GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_REMAIN_NUMBER);

}
//敵、木オブジェクトなどの動的処理を動作再開する
void GameSceneButtonStrategy::SetTimeStartForMoveingObject()
{
	//再開
		//詳細は関数先にて
	pGameSceneManager_->StartTimeInTheWorld();
}
//常駐UI表示、デバイス入力の復活　により世界の動きを再開
void GameSceneButtonStrategy::ResetInputKeyAndUIGroups(GAME_SCENE_UI_GROUP_TYPE UIGroupType)
{

	//世界への影響設定
		//キー入力回復
		//動的処理の再開
	SetImpactInTheWorld();



	//UI群を閉じる
	//UI群：木生成UI
	VisibleInvertUIGroup(UIGroupType);

}

/*****************************************************************/






/*行動クラス（シーンごとのシーン名SceneInputStrategyを継承したクラス）実装*******************************************************************************************************/


/*****************************************************************/
/*木作成　アルゴリズム*/
/*CREATE_TREE_ALGORITHM*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
CreateTreeStrategy::CreateTreeStrategy(GameSceneManager * pSceneManager) :
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void CreateTreeStrategy::AlgorithmInterface()
{
	//引数にて
	//自身の木のタイプを指定し、
		//行動実行
	//引数：通常木
	AlgorithmInterface(TREE_TYPES::TREE_TYPE_DEFAULT);

}
//デバイス入力時　実行する行動
void CreateTreeStrategy::AlgorithmInterface(TREE_TYPES type)
{
	//木の生成関数を呼び出す
	if (!pGameSceneManager_->GrowTree((int)type))
	{
		//返り値 false
		//になったとき、　生成できなかったため、
			//何もせずに終了する
		return;

	};


	//専用UI群の非表示
	{

		//自クラス内に、
			//UIの非表示（非表示の場合は、表示を行う）クラスがあるため、
			//そのインスタンスを生成し、呼び込む
		//アルゴリズム作成クラス
		FactoryForGameSceneButtonStrategy* pFactory = new FactoryForGameSceneButtonStrategy;
		//UIの非表示アルゴリズム所有のクラス作成
		GameSceneButtonStrategy* pUICloser = pFactory->
			CreateGameSceneButtonStrategy(
				GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_UI_CREATE_TREE,
				pGameSceneManager_);

		//アルゴリズム実行（UIの非表示）
		pUICloser->AlgorithmInterface();


		//インスタンスの消去
			//クラス内でインスタンスを作成したので、直ぐに解放する
		SAFE_DELETE(pUICloser);
		SAFE_DELETE(pFactory);

	}

}
/*****************************************************************/

/*****************************************************************/
/*ダミー木作成　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CREATE_DUMMY_TREE*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
CreateDummyTreeStrategy::CreateDummyTreeStrategy(GameSceneManager* pSceneManager):
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void CreateDummyTreeStrategy::AlgorithmInterface()
{
	//引数が違う程度ならば、
		//行動クラスのメンバなどに、タイプを持たせておき、行動実行のAlgorithmInterfaceが呼ばれたら、
		//そのメンバタイプで識別する形も良いとは思ったが、
		//行動クラスのメンバに渡す際に、タイプを渡す側が知らなくてはいけない。余計な情報を持たせないために、クラスを複数に、タイプごと分ける方法を取った。




	//自クラス内に、
		//引数タイプごとに行動を行なわせるクラスがあるため、
		//そのインスタンスを生成し、呼び込む
	//アルゴリズム作成クラス
	FactoryForGameSceneButtonStrategy* pFactory = new FactoryForGameSceneButtonStrategy;
	//UIの非表示アルゴリズム所有のクラス作成
	CreateTreeStrategy* pCreateTree = 
		(CreateTreeStrategy*)pFactory->
		CreateGameSceneButtonStrategy(
			GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_TREE,
			pGameSceneManager_);

	//アルゴリズム実行
	//引数：ダミー木
	pCreateTree->AlgorithmInterface(TREE_TYPES::TREE_TYPE_DUMMY);


	//インスタンスの消去
		//クラス内でインスタンスを作成したので、直ぐに解放する
	SAFE_DELETE(pCreateTree);
	SAFE_DELETE(pFactory);
}
/*****************************************************************/

/*****************************************************************/
/*バインド木作成　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CREATE_BIND_TREE*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
CreateBindTreeStrategy::CreateBindTreeStrategy(GameSceneManager* pSceneManager):
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void CreateBindTreeStrategy::AlgorithmInterface()
{
	//自クラス内に、
		//引数タイプごとに行動を行なわせるクラスがあるため、
		//そのインスタンスを生成し、呼び込む
	//アルゴリズム作成クラス
	FactoryForGameSceneButtonStrategy* pFactory = new FactoryForGameSceneButtonStrategy;
	//UIの非表示アルゴリズム所有のクラス作成
	CreateTreeStrategy* pCreateTree =
		(CreateTreeStrategy*)pFactory->
		CreateGameSceneButtonStrategy(
			GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_TREE,
			pGameSceneManager_);

	//アルゴリズム実行
	//引数：バインド木
	pCreateTree->AlgorithmInterface(TREE_TYPES::TREE_TYPE_BIND);


	//インスタンスの消去
		//クラス内でインスタンスを作成したので、直ぐに解放する
	SAFE_DELETE(pCreateTree);
	SAFE_DELETE(pFactory);
}
/*****************************************************************/

/*****************************************************************/
/*イーター木作成　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CREATE_EATER_TREE*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
CreateEaterTreeStrategy::CreateEaterTreeStrategy(GameSceneManager* pSceneManager):
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void CreateEaterTreeStrategy::AlgorithmInterface()
{
	//自クラス内に、
		//引数タイプごとに行動を行なわせるクラスがあるため、
		//そのインスタンスを生成し、呼び込む
	//アルゴリズム作成クラス
	FactoryForGameSceneButtonStrategy* pFactory = new FactoryForGameSceneButtonStrategy;
	//UIの非表示アルゴリズム所有のクラス作成
	CreateTreeStrategy* pCreateTree =
		(CreateTreeStrategy*)pFactory->
		CreateGameSceneButtonStrategy(
			GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CREATE_TREE,
			pGameSceneManager_);

	//アルゴリズム実行
	//引数：イーター木
	pCreateTree->AlgorithmInterface(TREE_TYPES::TREE_TYPE_EATER);


	//インスタンスの消去
		//クラス内でインスタンスを作成したので、直ぐに解放する
	SAFE_DELETE(pCreateTree);
	SAFE_DELETE(pFactory);
}
/*****************************************************************/
/*****************************************************************/
/*木作成UI群を閉じる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CLOSE_UI_CREATE_TREE*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
UICloseForCreateTreeStrategy::UICloseForCreateTreeStrategy(GameSceneManager * pSceneManager) :
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void UICloseForCreateTreeStrategy::AlgorithmInterface()
{
	//InputKeyとUIGroupの初期化
		//UI表示前へ戻す

	//UI群を閉じる
	//UI群：木生成UI
	ResetInputKeyAndUIGroups(GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_CREATE_TREE);
	{
		////キー入力を回復する
		//	//サンプルシーンのマネージャーに、キー変更を請け負ってもらう
		////キー：マウスの左クリックにて
		////行動：木生成UIの表示
		//pGameSceneManager_->SetStrategyForEachKey(SAMPLE_INPUT_ALGORITHM_CREATE_TREE, INPUT_MOUSE_LEFT);

		/*
		////プレイシーンにおける通常のキー入力を回復する
		//	//UI表示における、キー入力をNULLにしたものを回復する
		//SetDefaultEachInputKey();

		////常駐のUIの再表示
		//	//UI表示のために、一度非表示にしたものを再表示
		//DispResidentUIGroups();



		////UI群を閉じる
		////UI群：木生成UI
		//VisibleInvertUIGroup(SAMPLE_UI_GROUP_CREATE_TREE);
		*/




		////UI：木作成UI　背景
		//UIVisibleInvert(SAMPLE_UI_TREE_CREATE_BACKGROUND);
		////ボタンの非表示（更新も同時に停止）
		////Button：木生成ボタン
		//ButtonVisibleInvert(SAMPLE_BUTTON_CREATE_TREE);
		////ボタンの非表示（更新も同時に停止）
		////Button：UI群非表示ボタン
		//ButtonVisibleInvert(SAMPLE_BUTTON_CLOSE_UI_CREATE_TREE);
	}

	//木の立地予定地にワイヤーフレームによるモデルを描画していた
		//そのモデルの削除を行う
	{
		
		pGameSceneManager_->ErasePlannedLocationOfTree();

	}

}
/*****************************************************************/

/*****************************************************************/
/*敵情報看板UI群を閉じる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CLOSE_UI_ENEMY_INFO*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
UICloseForEnemyInfoStrategy::UICloseForEnemyInfoStrategy(GameSceneManager * pSceneManager):
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void UICloseForEnemyInfoStrategy::AlgorithmInterface()
{
	//InputKeyとUIGroupの初期化
		//UI表示前へ戻す
	
	//UI群を閉じる
	//UI群：敵情報UI
	ResetInputKeyAndUIGroups(GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_ENEMY_INFO);

}
/*****************************************************************/

/*****************************************************************/
/*木情報看板UI群を閉じる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CLOSE_UI_TREE_INFO*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
UICloseForTreeInfoStrategy::UICloseForTreeInfoStrategy(GameSceneManager * pSceneManager):
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void UICloseForTreeInfoStrategy::AlgorithmInterface()
{
	//InputKeyとUIGroupの初期化
	//UI表示前へ戻す

	//UI群を閉じる
	//UI群：木情報UI
	ResetInputKeyAndUIGroups(GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_TREE_INFO);

}
/*****************************************************************/

/*****************************************************************/
/*木の幹を成長させる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_GROW_TRUNK*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
GrowTheTrunkStrategy::GrowTheTrunkStrategy(GameSceneManager * pSceneManager):
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void GrowTheTrunkStrategy::AlgorithmInterface()
{
	//HP上昇を呼び込み（幹）
		//シーンマネージャークラスにて、現在参照しているオブジェクトの情報をメンバ変数として保存してある。
		//その情報をもとに、木オブジェクトを参照して、
		//木オブジェクトのステータス、HPなどをUPさせる
	pGameSceneManager_->GrowTreeParts(TREE_PARTS::TREE_TRUNK);


	////UI群を閉じる
	////UI群：木情報UI
	//VisibleInvertUIGroup(SAMPLE_UI_GROUP_TREE_INFO);
}
/*****************************************************************/


/*****************************************************************/
/*木の根を成長させる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_GROW_ROOT*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
GrowTheRootStrategy::GrowTheRootStrategy(GameSceneManager * pSceneManager):
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void GrowTheRootStrategy::AlgorithmInterface()
{
	//HP上昇を呼び込み（根）
	//シーンマネージャークラスにて、現在参照しているオブジェクトの情報をメンバ変数として保存してある。
	//その情報をもとに、木オブジェクトを参照して、
	//木オブジェクトのステータス、HPなどをUPさせる
	pGameSceneManager_->GrowTreeParts(TREE_PARTS::TREE_ROOT);


}
/*****************************************************************/
/*****************************************************************/
/*木の削除　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_ERASE_TREE*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
EraseTreeStrategy::EraseTreeStrategy(GameSceneManager* pSceneManager):
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void EraseTreeStrategy::AlgorithmInterface()
{
	//木削除実行
	pGameSceneManager_->ForciblyEraseTheTree();


	//UI閉じる
		//専用UI群の非表示
	{

		//自クラス内に、
			//UIの非表示（非表示の場合は、表示を行う）クラスがあるため、
			//そのインスタンスを生成し、呼び込む
		//アルゴリズム作成クラス
		FactoryForGameSceneButtonStrategy* pFactory = new FactoryForGameSceneButtonStrategy;
		//UIの非表示アルゴリズム所有のクラス作成
		GameSceneButtonStrategy* pUICloser = pFactory->
			CreateGameSceneButtonStrategy(
				GAME_SCENE_BUTTON_ALGORITHM::GAME_BUTTON_ALGORITHM_CLOSE_UI_TREE_INFO,
				pGameSceneManager_);

		//アルゴリズム実行（UIの非表示）
		pUICloser->AlgorithmInterface();


		//インスタンスの消去
			//クラス内でインスタンスを作成したので、直ぐに解放する
		SAFE_DELETE(pUICloser);
		SAFE_DELETE(pFactory);

	}


}
/*****************************************************************/
/*****************************************************************/
/*タイトルへ戻る　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_BACK_TITLE_SCENE*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
BackTitleSceneStrategy::BackTitleSceneStrategy(GameSceneManager * pSceneManager):
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void BackTitleSceneStrategy::AlgorithmInterface()
{
	//タイトルへ戻る処理を行う
	pGameSceneManager_->BackTitleScene();
}
/*****************************************************************/

/*****************************************************************/
/*タイトルへ戻るUI群を閉じる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CLOSE_UI_BACK_TITLE_SCENE*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
UICloseForBackTitleSceneStrategy::UICloseForBackTitleSceneStrategy(GameSceneManager * pSceneManager) :
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void UICloseForBackTitleSceneStrategy::AlgorithmInterface()
{

	//InputKeyとUIGroupの初期化
	//UI表示前へ戻す

	//UI群を閉じる
	//UI群：タイトルへ戻るUI
	ResetInputKeyAndUIGroups(GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_BACK_TITLE_SCENE);

	//ヘルプ画像群の削除
	pGameSceneManager_->DeleteSettingHelpImages();


}
/*****************************************************************/

/*****************************************************************/
/*ゲームマップを閉じる　アルゴリズム*/
/*GAME_BUTTON_ALGORITHM_CLOSE_GAME_MAP*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
UICloseForGameMapStrategy::UICloseForGameMapStrategy(GameSceneManager* pSceneManager) :
	GameSceneButtonStrategy::GameSceneButtonStrategy(pSceneManager)
{
}

//デバイス入力時　実行する行動
void UICloseForGameMapStrategy::AlgorithmInterface()
{
	//ゲームマップを閉じる
	pGameSceneManager_->EndGameMap();

	//UI群を閉じる
	//UI群：ゲームマップUI
	ResetInputKeyAndUIGroups(GAME_SCENE_UI_GROUP_TYPE::GAME_UI_GROUP_GAME_MAP);

}
/*****************************************************************/