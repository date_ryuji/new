#include "GameSceneUpdateAlgorithm.h"						//ヘッダ
#include "../../CommonData/GlobalMacro.h"					//共通マクロ
#include "../../Engine/Scene/SceneChanger.h"				//シーン切り替え
#include "../../Engine/GameObject/GameObject.h"				//GameObject
#include "../CountTimer/CountTimer.h"						//カウントタイマークラス
#include "../../Engine/GameObject/FadeInAndFadeOut.h"		//UIGroup　フェードインフェードアウト
#include "../../Engine/GameObject/UIGroup.h"				//UIGroup群
#include "../../Engine/Scene/SceneUIManager.h"				//シーンUIマネージャー
#include "StageData.h"										//ステージデータ（制限時間などを登録）
#include "GameSceneInputStrategy.h"							//シーン　デバイス入力時行動クラス
#include "GameSceneButtonStrategy.h"						//シーン　ボタン入力時行動クラス
#include "GameSceneManager.h"								//シーンマネージャー




/*
//ゲームの流れ
//各処理クラス
	//一つのクラスファイルに関数をまとめているのは、
	//ファクトリークラスからしか、以下のクラスにアクセスすることはないので、
	//一つ一つのクラスファイルにするのはもったいないと思った。
		//それでなくても、ファイル数が多くなっているので、
*/



/*行動クラス（シーンごとのシーン名SceneUpdateAlgorithmを継承したクラス）宣言*******************************************************************************************************/
	//詳細：Factoryクラス（行動クラス生成クラス）にてインスタンスの動的確保を行うため、Factoryの実装よりも前に記述
	//詳細：行動クラスを複数作ることになるため、行動クラスごとにクラスファイルを作成すると、クラスファイル量が多くなる。
	//　　：→上記を避けるために、一つのクラスに行動クラスをまとめる。（メソッドやメンバも少ないため、ひとくくりにしてしまう。）
/*************************************************************************************/
/*HelpImagesの描画*/
/*GAME_UPDATE_ALGORITHM_DISP_HELPIMAGES*/
class DispHelpImagesAlgorithm : public GameSceneUpdateAlgorithm
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	DispHelpImagesAlgorithm(GameSceneManager* pSceneManager);
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~DispHelpImagesAlgorithm() override;

	//初期化
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;
	//更新
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;
	//描画
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;
	//解放
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;
};
/*************************************************************************************/
/*************************************************************************************/
/*Startロゴの遷移*/
/*GAME_UPDATE_ALGORITHM_TRANSITION_START*/
class TransitionStartLogoAlgorithm : public GameSceneUpdateAlgorithm
{
//private メンバ変数、ポインタ、配列
private : 
	//遷移クラス
	FadeInAndFadeOut* pFadeInAndFadeOut_;
	//画像群
	UIGroup* pUIGroup_;


//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	TransitionStartLogoAlgorithm(GameSceneManager* pSceneManager);
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~TransitionStartLogoAlgorithm() override;

	//初期化
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;
	//更新
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;
	//描画
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;
	//解放
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;
};
/*************************************************************************************/
/*************************************************************************************/
/*ゲームプレイ開始*/
/*GAME_UPDATE_ALGORITHM_PLAY_GAME*/
class PlayGameAlgorithm : public GameSceneUpdateAlgorithm
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	PlayGameAlgorithm(GameSceneManager* pSceneManager);
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~PlayGameAlgorithm() override;

	//初期化
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;
	//更新
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;
	//描画
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;
	//解放
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;
};
/*************************************************************************************/
/*************************************************************************************/
/*エンドロゴの遷移*/
/*GAME_UPDATE_ALGORITHM_TRANSITION_END*/
class TransitionEndLogoAlgorithm : public GameSceneUpdateAlgorithm
{
//private メンバ変数、ポインタ、配列
private:
	//遷移クラス
	FadeInAndFadeOut* pFadeInAndFadeOut_;
	//画像群
	UIGroup* pUIGroup_;


//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	TransitionEndLogoAlgorithm(GameSceneManager* pSceneManager);
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~TransitionEndLogoAlgorithm() override;

	//初期化
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;
	//更新
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;
	//描画
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;
	//解放
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

	//世界の影響を与える
		//引数：なし
		//戻値：なし
	void SetImpactInTheWorld();
};
/*************************************************************************************/
/*************************************************************************************/
/*シーン切り替え*/
/*GAME_UPDATE_ALGORITHM_CHANGE_SCENE*/
class ChangeSceneAlgorithm : public GameSceneUpdateAlgorithm
{
//public メソッド
public:
	//コンストラクタ
		//詳細：シーンマネージャー、行動の詳細情報の確保
		//引数：シーンマネージャー
		//戻値：なし
	ChangeSceneAlgorithm(GameSceneManager* pSceneManager);
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~ChangeSceneAlgorithm() override;

	//初期化
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;
	//更新
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;
	//描画
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;
	//解放
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;
};
/*************************************************************************************/







/*UpdateAlgorithmクラス生成クラス　実装*******************************************************************************************************/
/*Factory*/
//コンストラクタ
FactoryForGameSceneUpdateAlgorithm::FactoryForGameSceneUpdateAlgorithm()
{
	//タグの確保
	//動的確保
	pTags_ = new std::string*[(int)GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_MAX];

	for (int i = 0; i < (int)GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_MAX; i++)
	{
		pTags_[i] = new std::string;
	}

	//タグ初期化
	(*pTags_[(int)GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_DISP_HELPIMAGES]) = "dhi";
	(*pTags_[(int)GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_TRANSITION_START]) = "tst";
	(*pTags_[(int)GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_PLAY_GAME]) = "pgm";
	(*pTags_[(int)GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_TRANSITION_END]) = "ted";
	(*pTags_[(int)GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_CHANGE_SCENE]) = "sch";

}
//デストラクタ
FactoryForGameSceneUpdateAlgorithm::~FactoryForGameSceneUpdateAlgorithm()
{
	for (int i = 0; i < (int)GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_MAX; i++)
	{
		SAFE_DELETE(pTags_[i]);
	}
	SAFE_DELETE_ARRAY(pTags_);
}

//ストラテジークラスを生成の委託
//パターン：FactoryMethod
GameSceneUpdateAlgorithm* FactoryForGameSceneUpdateAlgorithm::CreateGameSceneUpdateAlgorithm(const std::string tag, 
	GameSceneManager* pSceneManager)
{
	GAME_SCENE_UPDATE_ALGORITHM type = GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_MAX;

	//タグから該当する
	//ハンドル番号の取得
	for (int i = 0; i < (int)GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_MAX; i++)
	{
		//取得した文字列タグと
		//あらかじめ指定しておいたタグを比較して同じタグを持つハンドルを確保
		//その際、　tagのほうには、文字末に'\0'がついているが、
			//比較タグのほうにはついていないので、'\0'をつけた文字列とする
		std::string target = (*pTags_[i]) + '\0';
		
		if (tag == target)
		{
			//ハンドル番号をEnum型にキャストして登録
			type = (GAME_SCENE_UPDATE_ALGORITHM)i;
			break;
		}

	}


	switch (type)
	{
	case GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_DISP_HELPIMAGES :
	{
		return new DispHelpImagesAlgorithm(pSceneManager);
	}break;
	case GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_TRANSITION_START :
	{
		return new TransitionStartLogoAlgorithm(pSceneManager);
	}break;
	case GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_PLAY_GAME :
	{
		return new PlayGameAlgorithm(pSceneManager);
	}break;
	case GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_TRANSITION_END :
	{
		return new TransitionEndLogoAlgorithm(pSceneManager);
	}break;
	case GAME_SCENE_UPDATE_ALGORITHM::GAME_UPDATE_ALGORITHM_CHANGE_SCENE :
	{
		return new ChangeSceneAlgorithm(pSceneManager);
	}break;
	


	default : 
	{
		return nullptr;
	}break;

	}


	return nullptr;
}
/*********************************************************************************************/



/*シーンごとのゲームの流れ　親クラス　実装*******************************************************************************************************/

/*GameSceneUpdateAlgorithm***********************************************************************************/
//コンストラクタ
GameSceneUpdateAlgorithm::GameSceneUpdateAlgorithm(GameSceneManager* pSceneManager)
	: pSceneManager_(pSceneManager)
{
}
//デストラクタ
GameSceneUpdateAlgorithm::~GameSceneUpdateAlgorithm()
{
}



/*UpdateAlgorithmクラス（シーンごとのシーン名SceneUpdateAlgorithmを継承したクラス）実装*******************************************************************************************************/
/*GameSceneUpdateAlgorithm***********************************************************************************/


/*************************************************************************************/
/*HelpImagesの描画*/
/*GAME_UPDATE_ALGORITHM_DISP_HELPIMAGES*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
DispHelpImagesAlgorithm::DispHelpImagesAlgorithm(GameSceneManager* pSceneManager):
	GameSceneUpdateAlgorithm(pSceneManager)

{
}
//デストラクタ
DispHelpImagesAlgorithm::~DispHelpImagesAlgorithm()
{
}
//初期化
void DispHelpImagesAlgorithm::Initialize()
{
	//HelpImageの作成
	//描画
	//GameSceneMnagerの所有させる
	////HelpImages描画開始を宣言
			//この際、キー入力を解除したり、行動を止める必要がある。
			//そのため、GameSceneMnagerのInitialize時点では、まだ、インプッターの準備も整っていない状態であることがある。
			//そのため、呼び出すタイミングは、インプッターなどのインスタンスを作成した後

		//インスタンスが生成されるタイミングが
		//Update中になるため、
		//必ずインプッターの後になる
	pSceneManager_->StartDrawHelpImages();


	/*
	
	本来は、
	Update内にて、
	行動処理のフラグの押下
	を管理したいが、

	HelpImageに関しては、
	終了ボタンが押されたら、
	終了を示すようになっているので、

	GameSceneManagerから、
	更新処理クラスの関数を読んで、
	その関数から、フラグを下すだと、遠回りに思えるので、
	HelpImageに関しては、
	GameSceneManagerから、
	更新行動クラスの削除のタイミングを持つようにする

	
	*/
	/*
	
	あるいは
	終了ボタンにおける呼び出す行動（終了ボタンが押されましたよ）を
	自身のクラス（更新行動クラス）の関数にすればよい？
	シーンマネージャーを経由して、
	終了関数を呼ぶクラスを呼べばよい？



	
	*/
}
//更新
void DispHelpImagesAlgorithm::Update()
{
	////更新処理フラグを下す
	//pSceneManager_->EndUpdateAlgorithm();

}
//描画
void DispHelpImagesAlgorithm::Draw()
{
}
//解放
void DispHelpImagesAlgorithm::Release()
{
	//解放処理
}
/*************************************************************************************/

/*************************************************************************************/
/*Startロゴの遷移*/
/*GAME_UPDATE_ALGORITHM_TRANSITION_START*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
TransitionStartLogoAlgorithm::TransitionStartLogoAlgorithm(GameSceneManager* pSceneManager) :
	GameSceneUpdateAlgorithm(pSceneManager),
	pFadeInAndFadeOut_(nullptr),
	pUIGroup_(nullptr)
{
}
//デストラクタ
TransitionStartLogoAlgorithm::~TransitionStartLogoAlgorithm()
{
}
//初期化
void TransitionStartLogoAlgorithm::Initialize()
{
	//遷移するUIGroupの作成
	{
		//UIGroup作成
			//インスタンス生成
			//親の子供にすることで、描画順を後後にする
			//背景透過を行う画像なので、　後々の画像にする必要がある
		pUIGroup_ = (UIGroup*)Instantiate<UIGroup>(pSceneManager_->pParent_);

		//UI画像
		{
			int handle = pUIGroup_->AddStackUI("Assets/Scene/Image/GameScene/Transition/TransitionStart.png");
			SceneUIManager* pSceneUIManager = pUIGroup_->GetSceneUIManager();
			//位置の移動
			pSceneUIManager->SetPixelPosition(handle, 650, 200);
			//サイズの可変
			pSceneUIManager->SetPixelScale(handle, 500, 250);
		}

	}

	//スタートロゴの画像用意
		//画面遷移を行うクラスの作成
	pFadeInAndFadeOut_ = (FadeInAndFadeOut*)Instantiate<FadeInAndFadeOut>(pSceneManager_);


	//遷移情報の登録
	//画像数の取得
	int IMAGE_COUNT = -1;
	{
		SceneUIManager* pUIManager = pUIGroup_->GetSceneUIManager();
		IMAGE_COUNT = pUIManager->GetUINum();
	}

	//画像それぞれの遷移時間の登録
	float* transitionTime = new float[IMAGE_COUNT]
	{
		1.0f,

	};
	//画像それぞれのα値１．０ｆの画像表示時間の登録
	float* stopingTime = new float[IMAGE_COUNT]
	{
		2.0f,

	};


	//遷移を行うUIGroupの登録
	pFadeInAndFadeOut_->SetTransitionUIGroup(
		pUIGroup_, IMAGE_COUNT, transitionTime, stopingTime);


	//計測開始
	pFadeInAndFadeOut_->StartTransition();


	//動的確保要素の解放
	SAFE_DELETE_ARRAY(stopingTime);
	SAFE_DELETE_ARRAY(transitionTime);



}
//更新
void TransitionStartLogoAlgorithm::Update()
{
	//遷移がすべて終了したら
	//更新フラグを下す
	if (pFadeInAndFadeOut_ != nullptr &&
		pFadeInAndFadeOut_->IsEndingTransition())
	{
		//更新処理フラグを下す
		pSceneManager_->EndUpdateAlgorithm();
	}
	

}
//描画
void TransitionStartLogoAlgorithm::Draw()
{
}
//解放
void TransitionStartLogoAlgorithm::Release()
{
	//UIGroupの解放
	pUIGroup_->KillMe();

	//削除
	pFadeInAndFadeOut_->KillMe();


	//時間計測再開
	//ボタンクラスに
	//行動再開の処理を持たせてあるため、その機能を使用する
	//StartDrawH絵lpImagesと同様
	GameSceneButtonStrategy* pButtonStrategy = new GameSceneButtonStrategy(pSceneManager_);

	//時間計測再開
	//キー入力を受け付ける
	pButtonStrategy->SetImpactInTheWorld();


	SAFE_DELETE(pButtonStrategy);

}
/*************************************************************************************/

/*************************************************************************************/
/*ゲームプレイ開始*/
/*GAME_UPDATE_ALGORITHM_PLAY_GAME*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
PlayGameAlgorithm::PlayGameAlgorithm(GameSceneManager* pSceneManager) :
	GameSceneUpdateAlgorithm(pSceneManager)
{
}
//デストラクタ
PlayGameAlgorithm::~PlayGameAlgorithm()
{
}
//初期化
void PlayGameAlgorithm::Initialize()
{

	//カウントタイマーのインスタンス生成
	CountTimer* pCountTimer_ = (CountTimer*)Instantiate<CountTimer>((GameObject*)pSceneManager_);


	//制限時間
	//static constexpr float COUNT_RIMIT = 60.f * 2.f;



	//タイマー（カウントダウン）の開始
	//
	//６０＊ 2ｓ　＝　2分から
	//０ｓ		　＝　０まで
	pCountTimer_->StartTimerForCountDown(StageData::TIME_LIMIT , 0.0f);
	//pCountTimer_->StartTimerForCountDown(60 * 2, 0.0f);



	//カウントタイマーを渡す
	pSceneManager_->SetCountTimer(pCountTimer_);


}
//更新
void PlayGameAlgorithm::Update()
{
	//カウントタイマー取得
	CountTimer* pCountTimer = pSceneManager_->GetCountTimer();

	//条件：制限時間が　最小　０を下回ったら、シーン切り替え
	if (pCountTimer->EndOfTimerForCountDown())
	{
		//更新処理フラグを下す
		pSceneManager_->EndUpdateAlgorithm();
	}
}
//描画
void PlayGameAlgorithm::Draw()
{
}
//解放
void PlayGameAlgorithm::Release()
{
}
/*************************************************************************************/

/*************************************************************************************/
/*エンドロゴの遷移*/
/*GAME_UPDATE_ALGORITHM_TRANSITION_END*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
TransitionEndLogoAlgorithm::TransitionEndLogoAlgorithm(GameSceneManager* pSceneManager) :
	GameSceneUpdateAlgorithm(pSceneManager),
	pFadeInAndFadeOut_(nullptr),
	pUIGroup_(nullptr)
{
}
//デストラクタ
TransitionEndLogoAlgorithm::~TransitionEndLogoAlgorithm()
{
}
//初期化
void TransitionEndLogoAlgorithm::Initialize()
{
	//ゲーム全体への影響処理を行う
	SetImpactInTheWorld();




	//遷移するUIGroupの作成
	{
		//UIGroup作成
			//インスタンス生成
			//親の子供にすることで、描画順を後後にする
			//背景透過を行う画像なので、　後々の画像にする必要がある
		pUIGroup_ = (UIGroup*)Instantiate<UIGroup>(pSceneManager_->pParent_);

		//UI画像
		{
			int handle = pUIGroup_->AddStackUI("Assets/Scene/Image/GameScene/Transition/TransitionEnd.png");
			SceneUIManager* pSceneUIManager = pUIGroup_->GetSceneUIManager();
			//位置の移動
			pSceneUIManager->SetPixelPosition(handle, 600, 200);
			//サイズの可変
			pSceneUIManager->SetPixelScale(handle, 400, 300);

		}

	}

	//エンドロゴの画像用意
		//画面遷移を行うクラスの作成
	pFadeInAndFadeOut_ = (FadeInAndFadeOut*)Instantiate<FadeInAndFadeOut>(pSceneManager_);


	//遷移情報の登録
	//画像数の取得
	int IMAGE_COUNT = -1;
	{
		SceneUIManager* pUIManager = pUIGroup_->GetSceneUIManager();
		IMAGE_COUNT = pUIManager->GetUINum();
	}

	//画像それぞれの遷移時間の登録
	float* transitionTime = new float[IMAGE_COUNT]
	{
		1.0f,

	};
	//画像それぞれのα値１．０ｆの画像表示時間の登録
	float* stopingTime = new float[IMAGE_COUNT]
	{
		2.0f,

	};


	//遷移を行うUIGroupの登録
	pFadeInAndFadeOut_->SetTransitionUIGroup(
		pUIGroup_, IMAGE_COUNT, transitionTime, stopingTime);


	//計測開始
	pFadeInAndFadeOut_->StartTransition();


	//動的確保要素の解放
	SAFE_DELETE_ARRAY(stopingTime);
	SAFE_DELETE_ARRAY(transitionTime);


}

//更新
void TransitionEndLogoAlgorithm::Update()
{
	//遷移がすべて終了したら
	//更新フラグを下す
	if (pFadeInAndFadeOut_ != nullptr &&
		pFadeInAndFadeOut_->IsEndingTransition())
	{
		//更新処理フラグを下す
		pSceneManager_->EndUpdateAlgorithm();
	}

}
//描画
void TransitionEndLogoAlgorithm::Draw()
{
}
//解放
void TransitionEndLogoAlgorithm::Release()
{
	//UIGroupの解放
	pUIGroup_->KillMe();

	//削除
	pFadeInAndFadeOut_->KillMe();
}


//世界への影響設定
void TransitionEndLogoAlgorithm::SetImpactInTheWorld()
{
	//時間を停止する
	//ゲーム中の入力による行動クラスを生成
	GameSceneInputStrategy* pGameInputStrategy = new GameSceneInputStrategy(pSceneManager_, nullptr);
	//クラス内の関数における
	//全キー入力
	//全オブジェクトの動的処理の停止
	//全UIの停止を呼び込み
	pGameInputStrategy->SetImpactInTheWorld();
	pGameInputStrategy->InvisibleAllUIGroup();
	//解放
	SAFE_DELETE(pGameInputStrategy);
}
/*************************************************************************************/

/*************************************************************************************/
/*シーン切り替え*/
/*GAME_UPDATE_ALGORITHM_CHANGE_SCENE*/

//コンストラクタ
	//詳細：継承元のコンストラクタ呼び込み→継承元の継承元のコンストラクタにてメンバの初期化
ChangeSceneAlgorithm::ChangeSceneAlgorithm(GameSceneManager* pSceneManager) :
	GameSceneUpdateAlgorithm(pSceneManager)
{
}
//デストラクタ
ChangeSceneAlgorithm::~ChangeSceneAlgorithm()
{
}
//初期化
void ChangeSceneAlgorithm::Initialize()
{
}
//更新
void ChangeSceneAlgorithm::Update()
{
	//シーン切り替えクラス取得
	SceneChanger* pSceneChanger = (SceneChanger*)(pSceneManager_->FindObject("SceneChanger"));


	//現在の繁殖率を取得する
	//現在の繁殖率が
		//一定以上ならば、勝利シーンへ
		//一定より小さいならば、敗北シーンへ
	//繁殖率の計算
		//繁殖率は、繁殖率クラスに持たせてあるが、連携オブジェクトの一つにセットしていないので、わざわざセットするよりも。
			//	自身のクラスで計算を行うことで、代用できるので、連携のオブジェクトを増やすよりも、自身内で完結できる方法をとる
			//　繁殖率のクラスは、ほかのクラスとの連携が必要としないため、連携オブジェクトには追加しない


	//�@現在のGrassLandのポリゴン合計数と、　�A範囲を拡大した、ポリゴンの合計数を取得し、
	//�B割合をint値の％で取得する

	//�@
	int polygonNumOfGrass = pSceneManager_->GetTotalNumberOfPolygonWithGrass();
	//�A
	int totalPolygonNUm = pSceneManager_->GetTotalPolygonCountOfGrassLand();

	//�B
	//部分 / 全体にて割合を求める
	float ratio = (float)polygonNumOfGrass / totalPolygonNUm;

	//割合をパーセンテージにする
		//上記の割合にて、小数の値になっている
		//それを3桁の％にする
		//2桁右へ→100倍
	int percentage_ = (int)(ratio * 100.f);



	////勝利時の繁殖上限値
	//static constexpr int MAX = 75;


	if (StageData::CLEAR_BREEDING_RATE <= percentage_)
	{
		pSceneChanger->ChangeScene(SCENE_ID::SCENE_ID_WINNING);
	}
	else
	{
		pSceneChanger->ChangeScene(SCENE_ID::SCENE_ID_GAMEOVER);
	}

}
//描画
void ChangeSceneAlgorithm::Draw()
{
}
//解放
void ChangeSceneAlgorithm::Release()
{
}
/*************************************************************************************/

