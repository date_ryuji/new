#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/SceneParent.h"	//シーンオブジェクト　抽象クラス

//クラスのプロトタイプ宣言
class Player;
class UIGroup;
class Button;
class GameSceneManager;

//enum class のプロトタイプ宣言
enum class GAME_SCENE_BUTTON_ALGORITHM;



/*
	クラス詳細	：ゲームシーン　
	クラスレベル：サブクラス（スーパークラス（SceneParent））
	クラス概要（詳しく）
				：ゲームプレイシーン
				：ウィニングシーン、ゲームオーバーシーンに進むゲームジャッチを行うクラス


*/
class GameScene : public SceneParent
{
//private メンバ変数、ポインタ、配列
private :
	
	//画像ハンドル
	int hImage_;

	//音楽ハンドル
		//詳細：シーン内BGM
		//　　：ループ
	int hAudio_;

	//プレイヤークラス
	Player* pPlayer_;

//private メソッド
private :

	//UI画像作成処理
		//詳細：UIGroupへUI画像のロードを担うメソッド
		//引数：UIGroup
		//引数：画像ファイル名
		//戻値：UIGroupへのハンドル
	int AddStackUI(UIGroup* pUIGroup, const std::string& FILE_NAME);


	//RemainNumberの画像をUIGroupのスタックにする処理
		//詳細：共通の処理である、RemainNumberという画像を、UIGroupに追加する処理。
		//引数：RemainNumberをついかするUIGroup
		//戻値：なし
	void AddRemainNumber(UIGroup* pUIGroup);

	//ボタンクラスを作成する
		//詳細：UIGroupにボタンインスタンスを追加
		//　　：ボタンの初期化を担う
		//引数：ボタンのデザイン画像
		//引数：ボタンを登録するUIGroup
		//引数：ゲームシーンマネージャー
		//引数：ボタン押下時の行動アルゴリズム
		//戻値：作成されたボタンクラスのインスタンス
	Button* CreateButton(
		const std::string& fileName , 
		UIGroup* pUIGroup , 
		GameSceneManager* pSceneManager , 
		GAME_SCENE_BUTTON_ALGORITHM algorithm);

	//ボタンの位置とサイズを変更する
		//詳細：ボタンの位置、サイズ調整を担う	
		//引数：ボタンインスタンス
		//引数：設置位置X
		//引数：設置位置Y
		//引数：設置サイズX
		//引数：設置サイズY
		//戻値：なし
	void SetButtonPositionAndScale(Button* pButton , 
		const int POS_X, const int POS_Y, const int SCALE_X, const int SCALE_Y);

	//戻るボタンの作成
		//詳細：共通のボタンであるBackButtonのインスタンスを作る処理
		//引数：ボタンのファイルパス
		//引数：ボタンのインスタンスを登録する、UIGroup
		//引数：シーンマネージャークラス
		//引数：ボタン押下時の行動アルゴリズム
		//引数：設置位置X
		//引数：設置位置Y
		//戻値：なし
	void CreateBackButton(
		const std::string BUTTON_IAMGE_ROOT , 
		UIGroup* pUIGroup,
		GameSceneManager* pSceneManager,
		GAME_SCENE_BUTTON_ALGORITHM algorithm , 
		const int POS_X , const int POS_Y
		);


	//ButtonUIGroup登録処理
		//詳細：ボタン作成、初期化を担う処理
		//引数：ボタンを登録するUIGroup
		//引数：ボタンインスタンス
		//引数：シーンマネージャークラス
		//引数：ボタン押下時行動アルゴリズム
		//戻値：ボタンへのハンドル
	int AddStackButton(UIGroup* pUIGroup , Button* pButton , GameSceneManager* pSceneManager , 
		GAME_SCENE_BUTTON_ALGORITHM algorithmType);


	//シーンのUIGroupを作る
		//詳細：常駐UIGroup群を確保
		//引数：シーンマネージャークラス
		//戻値：なし
	void CreateSceneUIGroups(GameSceneManager * pSceneManager);


	//ステージの壁の当たり判定設定
		//詳細：ステージのWidth , Depthサイズを貰うことで、それをもとにステージ全体を囲う当たり判定のコライダーを作成
		//引数：デバック用のコライダー表示のフラグ
		//引数：ステージWidth 
		//引数：ステージDepth
		//戻値：なし
	void CreateWallCollider(bool drawCollider , int width , int depth);


	//シーン内仕様の初期デバイス入力の登録
		//詳細：初期のデバイスの入力コードと、デバイス入力時の行動ストラテジークラス作成
		//引数：シーンマネージャークラス
		//戻値：なし
	void CreateInputStrategy(GameSceneManager* pSceneManager);


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	GameScene(GameObject* parent);

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;
};
