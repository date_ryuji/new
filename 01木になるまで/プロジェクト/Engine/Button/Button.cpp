#include "Button.h"							//ヘッダ
#include "../Image/Image.h"					//画像
#include "../DirectX/Input.h"				//デバイス　入力クラス
#include "../GameObject/Transform.h"		//Transformクラス
#include "../../CommonData/GlobalMacro.h"	//共通マクロ
#include "../Scene/SceneButtonManager.h"	//ボタンマネージャークラす
											//ボタンの押下が確認されたとき
											//ボタン押下時の処理を呼び出す

//コンストラクタ
Button::Button(GameObject * parent):
	Button(parent, "Button") 
{
}

//コンストラクタ
Button::Button(GameObject * parent, const std::string OBJ_NAME):

	GameObject(parent , OBJ_NAME),
	pButtonManager_(nullptr) , 
	isContact_(false) , 
	currentStatus_(BUTTON_STATUS::BUTTON_STATUS_OFF),	//ボタンOFFがデフォルト
	myPushType_(BUTTON_PUSH_TYPE::PUSH_TYPE_DOWN) , //初めてボタンを押した時押下判定

	alpha_(1.0f)
{
	//ボタンのデザイン画像へのアクセスハンドルの初期化
	//条件：ボタンタイプMAX(enum)
	for (int i = 0; i < (int)BUTTON_STATUS::BUTTON_STATUS_MAX; i++)
	{
		hImage_[i] = -1;
		//trans[i] = nullptr;
	}
}

//デストラクタ
Button::~Button()
{
}

//初期化
void Button::Initialize()
{
	//ボタンサイズの初期化
	//サイズの可変
	//100pix * 100pix
	SetPixelScale(100, 100);

	//ボタン位置の初期化
	//移動
	SetPixelPosition(1000, 100);
}

//更新
void Button::Update()
{
	//ボタンの接触判定
	UpdateButtonForOnContact();
}

//描画
void Button::Draw()
{
	//ボタン描画
	DrawButton();
}

//解放
void Button::Release()
{
	//ボタンクラスの動的確保した要素を解放
	ReleaseButton();
}

//ボタンの初期化
void Button::Initialize(const std::string& FILE_NAME, BUTTON_STATUS status, BUTTON_PUSH_TYPE myPushType)
{
	//自身のボタン押下の種類をセットする
	myPushType_ = myPushType;


	//ボタンのロード
	Load(FILE_NAME, status);
		//引数のstatusにて、ボタンがON時のボタンとボタンがOFF時のボタンを判別することが可能。
		//両方のボタンのボタンデザイン（テクスチャ）を変えることも可能だが、変えない場合、自関数にてロードしたテクスチャがもう一方にも適用される。
		// 今回のロードがON用のボタンデザインであった場合、OFFにもそのボタンデザインが使用される。
		
		// その逆もしかり、OFFの画像のみロードされている場合は、OFFの画像をONに使用する
}

//ボタン押下　かつ　マウスカーソルとボタンの接触時の処理
void Button::OnContactButton()
{
	//ボタン押下の種類に応じて
	//押下判定を受ける
	bool isPress = false;

	switch (myPushType_)
	{
	case BUTTON_PUSH_TYPE::PUSH_TYPE_ALLWAYS : 
	{
		//常に押している
		//マウスの左クリックを、押し続けている間　ON
		//マウスの左クリックを、離し続けている間　OFF
		isPress = ButtonPress();
	}break;
	case BUTTON_PUSH_TYPE::PUSH_TYPE_DOWN :
	{
		//初めて押された
		//マウスの左クリックを、初めて押した　ON
		//マウスの左クリックを、上記以外　　　OFF
		isPress = ButtonPressOneDown();
	}break;
	case BUTTON_PUSH_TYPE::PUSH_TYPE_UP:
	{
		//クリックが離された
		//マウスの左クリックを、初めて離した　ON
		//マウスの左クリックを、上記以外　　　OFF
		isPress = ButtonPressOneUp();
	}break;

	default : 
	{
		isPress = false;
	}break;

	}

	//押下反応がされていた場合
		//押下時の処理を実行させる
		//この処理はデフォルトでは何も存在しない
			//ボタン押下時の処理を行う場合、ボタン宣言時に新たに付加させる必要がある
	if (isPress)
	{
		//ボタンの押下が確認されたら
		//自身のインスタンスを送り、
		//自身のボタン押下時の行動アルゴリズムを実行してもらう
		ExecutionAlgorithm();
	}


	//★他処理パターン
	
	/*
	////ボタンの押下処理
	//	//マウスの左クリックを、押し続けている間　ON
	//	//マウスの左クリックを、離し続けている間　OFF
	//ButtonPress();

	//
	////ボタンの押下
	//if (ButtonPressOneDown())
	//{
	//	//ボタンの押下が確認されたら
	//		//自身のインスタンスを送り、
	//		//自身のボタン押下時の行動アルゴリズムを実行してもらう
	//	ExecutionAlgorithm();
	//}

	////ボタンが離されたら
	//if (ButtonPressOneUp())
	//{
	//	//ボタンのグラフィック表現をOFF状態にする
	//	ButtonOFF();
	//}




	//if (Input::IsMouseButtonDown(0))
	//{
	//	Instantiate<Bullet>(this);
	//}

	//スイッチ式ON,OFF
	//押した瞬間に一回実行
	//ButtonPressOneDown();

	//スイッチ式ON,OFF
	//離した瞬間に一回実行
	//ButtonPressOneUp();
	
	*/


}



//自身のボタンマネージャー（ボタン押下時の行動を持っているマネージャークラス）のセット
void Button::SetSceneButtonManager(SceneButtonManager * pButtonManager)
{
	pButtonManager_ = pButtonManager;
}

//ボタン押下時の行動を実行
void Button::ExecutionAlgorithm()
{
	//ボタンの押下が確認されたら
		//メンバに所有している自身インスタンスを持たせている、ボタンマネージャーに
		//自身のインスタンスを送り、
		//自身のボタン押下時の行動アルゴリズムを実行してもらう
		//制限：ボタンマネージャーに自身インスタンスが登録されている前提
	if (pButtonManager_ != nullptr)
	{
		//ボタン押下時の処理実行呼び込み
		pButtonManager_->ExecutionAlgorithm((Button*)this);
	}

}







void Button::Load(const std::string & FILE_NAME, BUTTON_STATUS status)
{
	//すでにロード済みなら戻す
	if (hImage_[(int)status] != -1) { return; }

	//画像をロード
	hImage_[(int)status] = Image::Load(FILE_NAME);
	assert(hImage_[(int)status] != -1);
	
	
	//トランスフォームの作成
	//trans[status] = new Transform;
	//Image::SetTransform(hImage_[status], trans[status]);
}

void Button::UpdateButtonForOnContact()
{
	//更新フラグが立っていないときには呼び込まない
	//描画フラグが立っていないときには呼び込まない
	if (IsEnter() && IsVisible())
	{





		//接触判定
		XMVECTOR mouseCursorVec = Input::GetMouseCursorPosition();


		//画像の当たり判定
		//マウスカーソル位置と　画像の当たり判定のための情報を入れる構造体
		ImageCollision imageCollision;
		//マウスの位置を代入
		imageCollision.scrPointPos = mouseCursorVec;


		//ここで一度、現在のボタンがすでにロード済みであるかの確認をとる
		CheckIfLoaded(currentStatus_);

		//Transformのセット
			//GameObjectを継承して所有しているTransform
		Image::SetTransform(hImage_[(int)currentStatus_], transform_);


		//画像とマウスカーソル位置の接触判定
		Image::HitCollision(hImage_[(int)currentStatus_], &imageCollision);

		//接触していた
		if (imageCollision.hit)
		{
			//接触時の処理を呼び込む
			OnContactButton();
		}


	}

}



void Button::DrawButton()
{
	//今から描画をしようとする画像がロードされているかの確認
	CheckIfLoaded(currentStatus_);

	//α値セット
		//png画像を使用している場合、変にアルファ値を変更しないほうが良い
	Image::SetAlpha(hImage_[(int)currentStatus_], alpha_);

	//描画のためのTransformセット
	//Image::SetTransform(hImage_[currentStatus_], trans[currentStatus_]);
		//Transformは、Image.cppにセットして登録しているので、
		//ここではセットしなおさない。
		//→ピクセル管理で動かしたりするときに、
		//→移動後のTransformなどを受け取りなおすのは酷なので、
			//→ここではセットせずに、Transformは、画像生成時にセットした、Transformを移動して扱うようにする。
		//改善→　現在見ているボタンのinstance（Trasnform含め）確保されていないならば、
		//　　→　Transformをコピーされるしかし、　新しく確保されているものは、SetTransformされていないので、適用されない→そのため、コピー時にSetTransformするようにする
	//↓
	//Tranformを一ボタン共通にする
	Image::SetTransform(hImage_[(int)currentStatus_] , transform_);



	//描画実行
	Image::Draw(hImage_[(int)currentStatus_]);
}



void Button::ReleaseButton()
{
	//for (int i = 0; i < BUTTON_STATUS_MAX; i++)
	//{
	//	SAFE_DELETE(trans[i]);
	//}
	//SAFE_DELETE_ARRAY(trans);	//ポインタを配列として持っただけなので、
									//配列自体は実体。だが、中身の要素は動的確保したもの
								//要するに、ポインタを配列として持っただけ→　ポインタが示すのは、配列の先頭アドレス（実体を持つ配列と同様である。）（この辺りを混乱しないようにする）

}


//void Button::ChangeButtonTexture(BUTTON_STATUS changeStatus)
//{
//}

void Button::ChangeButtonStatus(BUTTON_STATUS changeStatus)
{
	
	//現在のステータスを切り替える必要がある

	//そうすると、描画の画像が変化する
	//伴って、描画をするための画像が用意されているかの確認
		//すでにロード済みならば、		：そのまま、その画像を描画のために用いる
		//まだロードされていないならば、：現在あるモデルを使用する。（hImage_にモデル番号をコピー）
	CheckIfLoaded(currentStatus_);


	//描画先の画像が確保できた（画像のハンドルを持つことができた）ら初めて
	//ステータスを切り替える
	currentStatus_ = changeStatus;

}

bool Button::Load(BUTTON_STATUS status)
{
	
	//ロードされているかの確認
	//ロードされているなら
	if (hImage_[(int)status] != -1)
	{
		//関数終了
		return true;
	}
	else
	{
		//すでにロードされているものを探して、
		//その画像のモデルハンドルも自身で共有
		for (int i = 0; i < (int)BUTTON_STATUS::BUTTON_STATUS_MAX; i++)
		{
			if(hImage_[i] != -1 && i != (int)status)
			{
				//情報をコピーする
				CopyLoadedInformation((int)status, i);
				return true;
			}
		}
	}


	//すでにロードされている画像が、
	//一つもなかった
	return false;
}

void Button::CheckIfLoaded(BUTTON_STATUS status)
{
	if (!Load(status))
	{
		MessageBox(nullptr, "ボタンの画像を最低１つロードしてください", "エラー", MB_OK);
		assert(0);
	};

	
}


void Button::CopyLoadedInformation(int copyTo, int original)
{
	//画像ハンドルのコピー
	hImage_[copyTo] = hImage_[original];

	////Transformのコピー
	//trans[copyTo] = new Transform;
	////中身をコピー
	//(*trans[copyTo]) = (*trans[original]);

	//セット
	//Image::SetTransform(hImage_[copyTo], trans[copyTo]);

}

void Button::ButtonON()
{
	ChangeButtonStatus(BUTTON_STATUS::BUTTON_STATUS_ON);
}

void Button::ButtonOFF()
{
	ChangeButtonStatus(BUTTON_STATUS::BUTTON_STATUS_OFF);
}
bool Button::ButtonPress()
{
	//列挙型：BUTTON_STATUS
	//これが　０：OFF_BUTTON　、　１：ON_BUTTON　であるとき限定
		//接触時：（OFFである、かつ、ボタンの範囲内にいる）画像を変えるのであれば、更に、条件が必要

	bool buttonPress = Input::IsMouseButton(0);


	//マウスの左クリック入力を受け取り、それが入力されているときはTrue＝ONBUTTON、False＝OFFBUTTON　になる
	ChangeButtonStatus((BUTTON_STATUS)buttonPress);

	return buttonPress;

}

bool Button::ButtonPressOneDown()
{
	//入力1回されたときに、
	//ON,OFFを切り替える処理を行う
	//if (Input::IsMouseButtonDown(0))
	{
		//現在の状態を反転させて、切り替える
		//if文を使わないために
		//論理演算にて識別させる

		//　currentStatus_は何でもいいが、　IsMouseButtonDownは、１の時は、→ToggleButtonSwitchにて、currentStatusの反転したものを帰す。
																//0の時は、→同様にて、currentStatusそのままの値を返す。
		//つまり、1の時は、currentStatusをそのままの値を返して、
		//　　　　0の時は、currentStatusの反転の値を返して
		/*

		current , isMouse = 結果
		0		,		0 =  1
		1		,		0 =  0
		0		,		1 =  0
		1		,		1 =  1

		上記の論理演算結果を見ると
		・同じときは1
		・違うときは0

		→排他的論理和のNOT（！）

		*/
		/*ChangeButtonStatus(ToggleButtonSwitch(
			(BUTTON_STATUS)
			(!(currentStatus_ ^ Input::IsMouseButtonDown(0)))
		));*/
		//排他的論理和をつかえば、そもそもToggleButtonSwtichの関数いらない


		bool buttonPress = Input::IsMouseButtonDown(0);



		/*

		排他的論理和にして

		同じ値の時に　０、違うときに１にすれば、
		今回の処理は通る

		*/
		ChangeButtonStatus(
			(BUTTON_STATUS)
			((int)currentStatus_ ^ (int)buttonPress)
		);

		return buttonPress;
	}
}

bool Button::ButtonPressOneUp()
{
	//入力1回離されたとき、
	//ON,OFFを切り替える処理を行う
	//if (Input::IsMouseButtonUp(0))
	{
		bool buttonPress = Input::IsMouseButtonUp(0);


		//現在の状態を反転させて、切り替える
		//Down時と同様に、排他的論理和のNOT
		/*ChangeButtonStatus(ToggleButtonSwitch(
			(BUTTON_STATUS)
			(!(currentStatus_ ^ Input::IsMouseButtonUp(0)))
		));*/
		ChangeButtonStatus(
			(BUTTON_STATUS)
			((int)currentStatus_ ^ (int)buttonPress)
		);

		return buttonPress;
	}
}

BUTTON_STATUS Button::ToggleButtonSwitch(BUTTON_STATUS status)
{
	//現在がONなら、 OFFへ
	//現在がOFFなら、ONへ　切り替える

	//単純に列挙型の値を整数として扱って、
	//OFF ＝　０
	//ON　＝　１

	//上記として扱い、
	//0を1へ、1を0へ切り替える処理を行う

	/*
	・引数を整数にする
	・引数の値を‐1する
	・その値を絶対値にする(絶対値→符号を＋にした整数値（-1を１に、　ー１００を１００に）)

	引数1の場合
		1−1＝０　→絶対値＝０

	引数0の場合
		0−1＝‐1　→絶対値＝１


	*/

	int switchValue = (int)status;
	switchValue -= 1;
	switchValue = abs(switchValue);


	return (BUTTON_STATUS)switchValue;

}


void Button::SetTransform(Transform & transform)
{
	//Transformにセット
	transform_ = transform;

	//画像事所有する必要がなくなったので、
		//一度にセットしてしまう
	//for (int i = 0; i < BUTTON_STATUS_MAX; i++)
	//{
	//	if (hImage_[i] != -1)
	//	{
	//		//Transformにセット
	//		transform_ = transform;
	//	}
	//}
}
void Button::SetTransform(Transform * transform)
{
	//参照受け取りの関数を呼び込み
		//行うことは同じのため、同様の処理の関数を呼び込み
	SetTransform((*transform));
	/*for (int i = 0; i < BUTTON_STATUS_MAX; i++)
	{
		if (hImage_[i] != -1)
		{
			Image::SetTransform(hImage_[i], transform);
		}
	}*/
}
void Button::MovePixelPosition(int x, int y)
{
	//Image名前空間にアクセスして、
		//現在のTransformから、引数分Moveしてもらう
	Image::MovePixelPosition(transform_, x, y);



	//↓共通のTransformにしたため、必要なし
	////移動のため、
	////仮に、hImage_に登録してある各画像が、共通のものを使っていた場合、
	////２回以上呼ばれて、２倍以上移動してしまう。
	////それを避けるために、一度移動したものは避けるようにする

	////移動したものを保存しておく一時保存先
	//std::vector<int> moved;
	//moved.clear();

	//for (int i = 0; i < BUTTON_STATUS_MAX; i++)
	//{
	//	if (hImage_[i] != -1 && IsExits(moved, hImage_[i]))
	//	{
	//		//移動
	//		Image::MovePixelPosition(hImage_[i], x, y);
	//		//移動済みに追加
	//		moved.push_back(hImage_[i]);

	//	}
	//}
}

bool Button::IsExits(std::vector<int>& moved, int checkValue)
{
	for (int i = 0; i < moved.size(); i++)
	{
		if (checkValue == moved[i])
		{
			return true;
		}
	}

	return false;
}

void Button::SetPixelPosition(int x, int y)
{
	//Image名前空間にアクセスして、
		//引数とランストームを該当位置にセットしてもらう
	Image::SetPixelPosition(transform_, x, y);


	//for (int i = 0; i < BUTTON_STATUS_MAX; i++)
	//{
	//	if (hImage_[i] != -1)
	//	{
	//		Image::SetPixelPosition(hImage_[i], x, y);
	//	}
	//}
	
}
void Button::ChangeShader(SHADER_TYPE shaderType)
{
	//所有している画像ハンドルすべてに切り替えを行ってもらう
	for (int i = 0; i < (int)BUTTON_STATUS::BUTTON_STATUS_MAX; i++)
	{
		if (hImage_[i] != -1)
		{
			Image::ChangeShader(hImage_[i], shaderType);
		}
	}
}

void Button::SetPixelScale(int x, int y)
{
	//画像ハンドル番号を送り、
		//その画像ハンドルのテクスチャサイズから
		//Transformのサイズの可変を行う
		//ここにおけるサイズは、どの画像も同様である前提

	for (int i = 0; i < (int)BUTTON_STATUS::BUTTON_STATUS_MAX; i++)
	{
		if (hImage_[i] != -1)
		{
			//Image名前空間にアクセスして、
			//引数とランストームを該当位置にセットしてもらう
			Image::SetPixelScale(transform_, hImage_[i] ,  x, y);

			//サイズの可変を行えたので、
				//終了する
			break;
		}
	}

	//for (int i = 0; i < BUTTON_STATUS_MAX; i++)
	//{
	//	if (hImage_[i] != -1)
	//	{
	//		Image::SetPixelScale(hImage_[i], x, y);
	//	}
	//}
}

void Button::SetAlpha(float alpha)
{
	alpha_ = alpha;
}

void Button::LowerOneStep()
{
	//transform_単体で、ボタンのTransformを管理しているので、
		//そのTransformを移動させる
	transform_.position_.vecZ += 0.001f;
}

bool Button::IsButtonOFF()
{
	return !(bool)currentStatus_;
}

bool Button::IsButtonON()
{
	return (bool)currentStatus_;
}
