#pragma once
#include <list>		//リストを使用するためにインクルード（途中で中身を消去する可変長配列）
#include "../PolygonGroup/HeightMapGround.h"	//ポリゴンクラス　HeightMapGround(継承元)（地面ポリゴン形成クラス）





//ポリゴン拡張先　上下左右のポリゴン
	//詳細：現在のポリゴンからポリゴンを拡張する際、現在の上下左右のポリゴンが拡張対象となる。その拡張対象先を示す際に使用する
enum class EXPAND_TYPE
{
	EXPAND_UP = 0 ,		//上
	EXPAND_DOWN ,		//下
	EXPAND_RIGHT,		//右
	EXPAND_LEFT,		//左

	EXPAND_MAX,			//MAX
};

//クラスのプロトタイプ宣言
class Grass;

/*
	クラス詳細	：ポリゴンクラス　HeightMapGroundを使用して、地面ポリゴンを作成するクラス（地面ポリゴン作成に特化したクラス）
	クラスレベル：サブクラス（スーパークラス（HeightMapGround））
	クラス概要（詳しく）
				：（HeightMapGround）ハイトマップが画像から地面のY座標の高さを決め、地面の上げ下げを表現するポリゴンモデルクラスを継承させて、
				　地面のポリゴンを表現させるクラス
				
				：とともに規定のポリゴンから、範囲を上下左右と1ポリゴンずつ広げていって、
				　範囲の広がりを表現する
				 （Grassの生成）

*/
class GrassLand : public HeightMapGround
{
//private 構造体
private :

	/*
		構造体詳細：GrassLandに登録された　Grassクラスのポインタと、それが生成された基準のポリゴン番号を保存
		構造体詳細（詳しく）
				　：Grassごとに持つ構造体クラス
				 　 GrassごとにGrassのポインタとGrassの基準のポリゴンを持たせる
				　：外部との連携のために基準のポリゴン番号を所有しておく
	*/
	struct GrassInfo
	{
		//ポリゴン番号
			//Grassが出現した基準のポリゴン番号
		int polyNum;
		//Grassクラス
		Grass* pGrass;

		//コンストラクタ
		GrassInfo(Grass* pObject , int num);
	};

//private メンバ変数、ポインタ、配列
private : 

	//時間停止のフラグ
	//詳細：時間による行動、移動、攻撃、HP現象などなど様々な動的要因を停止する
	bool isStopTime_;



	//子供クラス（Grass群）
		//詳細：Grass群を確保するためにGrassの情報としてまとめて確保する
		//　　：色の違うポリゴンを広げて、1ポリゴンから、
		//　　：どんどん範囲を広げていっているように疑似的に見せるためのクラス群
	std::list<GrassInfo*> myGrasses_;

	//HeightMapGroundで作成するポリゴン事に
	//1ポリゴンずつ　ポリゴンが、どのGrassに塗られているのか
		/*
			下記リストを使用する理由

			ポリゴンごとに塗ったGrassのクラスポインタを所有しておく一次元配列
			どのGrassが塗っているかは、可変であるため、可変長配列の一次元配列とする
			上記を所有しておくことで、Grassがポリゴンを拡張する際に、拡張先のポリゴンがすでに拡張されていたら、拡張先として対象範囲外とする。
			その範囲外のポリゴンであるか判断するために、自身の所有するすべてのポリゴンと比較する必要が出てくる。2桁程度の比較であれば問題ないが、場合により、３桁の比較回数になる。そして、拡張先が３桁だと、３桁＊３桁の計算を１フレームに行うことになってしまう
			上記を避けるために、ポリゴンごとに拡張したGrassのポインタを持っておくことで、
				いざ、Grassがポリゴンを拡張する際に、拡張先のポリゴンが拡張されているか、
					それは、上記の一次元配列に拡張先のポリゴン番号を添え字として使用すれば、処理数を少なく、判断できる
			
			上記の目的のために以下を宣言する
		
		
		*/
		//　　：リストの配列
		//　　：リストの個数は可変ではないため、リストの配列として確保する（リストのリストとしては扱わない）
	std::list<Grass*>* listGrassWithPaintedPolygons_;


	/*
	//ポリゴンが塗られたポリゴン数のカウント方式

	//自身のポリゴンがGrassクラスに塗られているかを判定する配列
		//一つのクラスに塗られるごとに、
		//＋１
		//一つのクラスが塗りの範囲を狭めるごとに、
		//−１
	//ポリゴン番号が1次元で出てくるので、1次元配列で管理
	//int* growingUpPolygons_;
	*/




//private メソッド
private  :
	//時間が停止しているか
		//引数：なし
		//戻値：時間が停止しているか（停止している：true,停止していない：false）
	bool IsStopTime();


	//GrassのUpdateとDraw実行
		//引数：描画位置
		//引数：経過時間(デルタタイム)
		//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	HRESULT UpdateAndDrawGrass(Transform& transform , float elpasedTime);

	//ポリゴンの塗り状態を確認するリストの初期化
		//詳細：リストの動的確保
		//引数：なし
		//戻値：なし
	void InitGrassWithPaintedPolygons();


	//自身のクラスの描画を行う
		//引数：描画Transform
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT ThisDraw(Transform& transform);


//public メソッド
public :

	//コンストラクタ
		//引数：なし
		//戻値：なし
	GrassLand();

	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~GrassLand() override;


	//ハイトマップ画像からモデルの作成を行う（引数のファイル名より）
		//詳細：ハイトマップ画像をよみこみ、　ハイトマップ画像が地面モデルの凹凸として使用できる
		//レベル：仮想関数
		//引数：ファイル名
		//引数：シェーダータイプ
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT Load(const std::string& FILE_NAME , SHADER_TYPE thisShader);


	//ポリゴンクラスのポリゴンの描画を行う
		//詳細：ポリゴンクラスに登録されたポリゴン情報を引数Transform位置へ描画する
		//　　：ポリゴンは、ポリゴンの頂点情報とポリゴンを作るインデックス情報により作られている
		//　　：各ポリゴン単位で頂点情報をシェーダーの頂点シェーダーに送ることで、ポリゴン事最終的にピクセルで出力される
		//引数：描画時の変形情報（移動、拡大、回転）（ローカル座標（メソッドにて、親も考慮したワールド座標を取得可能））
		//レベル：オーバーライド
		//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	HRESULT Draw(Transform& transform) override;
	
	
	//解放
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

	
	//時間の停止（行動、移動、攻撃などの停止）
		//引数：なし
		//戻値：なし
	void StopTime();
	//時間の再開（行動、移動、攻撃などの再開）
		//引数：なし
		//戻値：なし
	void StartTime();



	//指定ポリゴンがGrassとして、塗られているか、塗られていないか
		//詳細：指定ポリゴン番号が、黒ポリゴンか、緑ポリゴンか
		//　　：管理している、Grassクラスすべてに、Grass自身が指定引数ポリゴン番号を有しているかの確認を行う
		//引数：ポリゴン番号
		//戻値：塗られている（塗られている：true、塗られていない：false）
	bool IsGreenPolygon(int polyNum);

	//引数にてもらったポリゴン番号のポリゴンを作る頂点を取得する
		//引数：ポリゴン番号
		//引数：頂点情報
		//引数：頂点番号
		//戻値：なし
	void GetVertex(int polygonNum, VERTEX * pVertex, int* vertexNum);

	//引数にてもらったポリゴン番号の上下左右、それぞれのポリゴン番号を取得する
		//引数：広げる方向
		//引数：広げた先のポリゴン番号（広げられなかった：-1）
		//引数：広げる基準のポリゴン番号
		//戻値：なし
	void GetPolygonNumToExpanded(EXPAND_TYPE type , int* target , int original);

	//引数にてもらったポリゴン番号からポリゴンをコピーして、新規Grassクラスを生成する
		//引数：ポリゴン番号
		//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	HRESULT CreateGrassPolygon(int polyNum);


	//ポリゴン番号にて示されるローカル座標を取得する
		//詳細：３頂点の中心を求めて、そのローカル座標を渡す
		//引数：中心のローカル座標
		//引数：ポリゴン番号
		//戻値：取得できたか（ローカル座標を取得できる：true,取得できない：false）
	bool GetLocalPosOfPolygon(XMVECTOR* centerLocalPos, int polyNum);

	//合計ポリゴン数の取得
		//引数：なし
		//戻値：合計ポリゴン数の取得
	int GetPolygonCount();

	//頂点を敷き詰めたときのWidthとDepthの取得
		//詳細：Width,Depthを一緒に取得したいと思っているため、
		//　　：別々にGet関数を作らずに、一緒に呼び込む
		//引数：取得Widthを代入するポインタ
		//引数：取得Depthを代入するポインタ
		//戻値：なし
	void GetWidthAndDepth(int* getWidth , int* getDepth);

	//ポリゴンのサイズを取得
		//詳細：ポリゴンを作るサイズ
		//引数：なし
		//戻値：ポリゴンサイズ
	float GetPolygonSize();


	//木オブジェクトが解放されて　拡大させたGrassの範囲を狭める
		//詳細：木オブジェクトが立地していたポリゴン番号を受け取り、
		//　　：そのポリゴン番号と同様のポリゴンから草オブジェクト（Grass）を広げたオブジェクトを見つけ出し、
		//　　：そのオブジェクトの広げた緑の範囲を狭める
		//　　：引数ポリゴンにて示される、指定Grassを見つけて、そのポインタへ、縮小指示
		//引数：目標ポリゴン番号
		//戻値：なし
	void StartShrinkingGrass(int targetPolyNum);


	//Grassを拡げている、Grassが繁殖しているポリゴンの合計数を取得する
		//詳細：地面のポリゴンを作るポリゴンにて、塗られているポリゴンの合計数取得
		//引数：なし
		//戻値：塗られた合計ポリゴン数
	int GetTotalNumberOfPolygonWithGrass();


	/*
	//ポリゴンが塗られた＋１
		//GrassLandクラスに所有している、ポリゴンの塗られ具合の値を格納している配列に、
			//ポリゴンが新たに塗られたことを知らせて、配列の要素をカウントアップさせる
	//void GrowingOnePolygon(int polyNum);


	//ポリゴンの塗りが解除されたー１
		//Grass側から、塗った時に、知らせるようにする
		//そして、塗られた分加算していくことで、
			//そのポリゴン番号が0ならば、誰にも塗られておらず、1以上なら、塗られている、かつ、1以上でその数に応じて、　深さがわかるため、
			//そのマスが2ならば、そのマスの草は標準の2倍の大きさに描画したりと、
				//描画の際に工夫をすることもできるかもしれない。→だが、もうすでに、一つのポリゴン群に結合しているので、一つ一つのポリゴンで管理するのは難しいかも、
				//→あるいは、GrassLandにて草のテクスチャ部分は管理するのはありと言えばありだが、面倒になる
	//void RemoveOnePolygon(int polyNum);
	*/


	//ポリゴンが塗られたことを報告
		//詳細：Grassクラスが、ポリゴンを塗ったことを知らせる
		//　　：ポリゴン番号にて示される、だれがGrassを塗ったかを保存しておく一次元配列に、引数Grassを登録する
		//引数：塗ったGrass
		//引数：Grassの基準ポリゴン
		//戻値：なし
	void GrowingOnePolygon(Grass* pGrass, int polyNum);

	//ポリゴンの塗りが解除されたことを報告
		//詳細：自身が塗ったポリゴンを解放する
		//引数：解放するGrass
		//引数：Grassの基準ポリゴン
		//戻値：なし
	void RemoveOnePolygon(Grass* pGrass, int polyNum);


	//引数ポリゴンを、引数Grassが塗っているかの判定
		//引数：リストの一次元配列に登録した、
		//　　：ポリゴン番号ごとの、塗ったGrassを保存している一次元配列に引数ポリゴン番号でアクセスし、
		//　　：引数Grassが存在していれば、塗った。存在しなければ、塗らなかった。
		//戻値：塗られている（塗られている：true,塗られていない：false）
	bool IsPaintedPolygon(Grass* pGrass , int polyNum);


	//特定ポリゴンを緑ポリゴンにする
		//詳細：一番目のGrassに、特定のポリゴンを、緑ポリゴンにする処理を実行
		//　　：（そのポリゴンはGrassではなく、あくまでも、存在するGrassの指定ポリゴンだけを緑ポリゴンにさせるやり方である）
		//引数：ポリゴン番号
		//戻値：なし
	void CreateOneGreenPolygon(int polyNum);
	//指定ポリゴンが緑ポリゴン（全Grassの中で誰も塗っていなかったら）
		//詳細：一番目のGrassに、塗らせる
		//引数：ポリゴン番号
		//戻値：なし
	void IfNotPaintedCreateOneGreenPolygon(int polyNum);
	//Grassに、地面の全ポリゴンを持たせる
		//詳細：塗られていない、ポリゴンを一つのGrassに持たせて、全ポリゴンを塗られた表現とする
		//　　：対象のGrassは、一番目のGrassとする
		//引数：なし
		//戻値：なし
	void CreateAllGreenPolygon();

	//強制的にGrassの拡張、成長を行う（１回）
		//詳細：登録しているGrassの拡張を強制的に一回進める
		//　　：登録してある、全Grassの拡張を行うため、処理が重くなることがある
		//引数：なし
		//戻値：なし
	void ForciblyGrowGass();

	//指定回数拡張、成長を行わせる
		//詳細：（０，０）からGrassを拡張させるとき、Width回分拡張すれば、一番端まで届く拡張になる
		//　　：拡張数が多ければ、処理数、繰り返し数が多くなるので注意
		//引数：なし
		//戻値：なし
	void ForciblyGrowGass(int count);


};

