#include "Grass.h"									//ヘッダ
#include "GrassLand.h"								//親クラス
#include "../DirectX/Texture.h"						//テクスチャクラス
#include "../DirectX/Camera.h"						//カメラオブジェクト
#include "../../Shader/BaseShader.h"				//シェーダークラス　継承元クラス
#include "../Model/Model.h"							//モデル
#include "../PolygonGroup/Plane.h"					//草に追加する　//平面
#include "../PolygonGroup/BranchesMakeTree.h"		//結合クラス

//コンストラクタ
Grass::Grass(int polygonNum, GrassLand * pParent) :
	HeightMapGround::HeightMapGround(),
	FIRST_POLY_NUM_(polygonNum),
	MAX_EXPAND_NUM_(15),

	elapsedTime_(0.f),

	expandCounter_(0),
	hBranchesMakeTree_(-1),
	isKillMe_(false),

	pParent_(pParent),
	pPlane_(nullptr),
	pBranchesMakeTree_(nullptr)

{
	//可変長配列の初期化
	parentVertexArray_.clear();
	polygonsArray_.clear();
	nextPolyTargetArray_.clear();
}

//デストラクタ
Grass::~Grass()
{
}

//ロード
HRESULT Grass::Load(const std::string& FILE_NAME , SHADER_TYPE thisShader)
{
	//シェーダーセット
	thisShader_ = thisShader;

	//処理の結果を入れる変数
	HRESULT hr = S_OK;


	//床の草を示す
		//Grassに結合するポリゴンのテクスチャ
	//テクスチャファイル名
	std::string texfileName = "Assets/Scene/Image/GameScene/HeightMap/FloorGrass2.png";


	//Grassに結合するPlaneクラス
		//Modelのデータベースに追加させるので、
		//クラス内で解放の処理をする必要がなくなる
	{
		////自身のクラスに追加する
		////平面の追加
		//pPlane_ = new Plane;
		//pPlane_->Load("Assets/HeightMap/irasutoya_Grass.png", SHADER_PLANE);
		//↓
		//Plane平面（ソースの）のロード
		int hPlane = Model::Load(texfileName, POLYGON_GROUP_TYPE::PLANE_POLYGON_GROUP, SHADER_TYPE::SHADER_PLANE);
		assert(hPlane != -1);
		//平面クラスのポインタを取得する
		pPlane_ = (Plane*)Model::GetPolygonGroupPointer(hPlane);
	}


	//Plane平面クラスの結合クラス
		//GrassとPlaneを結合し、一つのポリゴンとする
		//そのポリゴンクラス
	{

		//結合クラス
		pBranchesMakeTree_ = new BranchesMakeTree;
		//pBranchesMakeTree_->Load("Assets/HeightMap/Grass_BlackAndGreen.png", SHADER_HEIGHT_MAP);
		pBranchesMakeTree_->Initialize(texfileName, SHADER_TYPE::SHADER_GRASS);

		//空のTransform
		Transform trans;

		//Modelへ登録するための構造体
		Model::ModelData modelData;
		modelData.fileName = "BranchesMakeTreeForGrass.class";	//ファイル名を他のBranchesMakeTreeと区別するために、BranchesMakeTreeとForだれのための、.class（クラスですよ）（ファイル名は、解放の時に、同様の名前が存在するかで、解放するかを判断する。なので、他とは被らないファイル名をセットする）
		modelData.pPolygonGroup = pBranchesMakeTree_;
		modelData.thisPolygonGroup = POLYGON_GROUP_TYPE::BRANCHES_MAKE_TREE_POLYGON_GROUP;
		modelData.thisShader = SHADER_TYPE::SHADER_GRASS;
		modelData.transform = trans;

		//Model.cppのデータベース軍に追加
		hBranchesMakeTree_ = Model::AddModelData(modelData);
		//警告
		assert(hBranchesMakeTree_ != -1);
	}


	//ポリゴンの番号を登録
	PolygonInfo polyInfo;
	polyInfo.parentPolyNum = -1;	//親なし
	polyInfo.thisPolyNum = FIRST_POLY_NUM_;	//初期ポリゴン番号の登録

	//可変長配列に追加
	polygonsArray_.push_back(polyInfo);


	//次の範囲拡大対象に
	//ポリゴン番号をセット
	nextPolyTargetArray_.push_back(FIRST_POLY_NUM_);

	{
		//ひとつのポリゴンを作成し、
		//頂点バッファー、インデックスバッファーまで作成させる
		hr = Initialize(FIRST_POLY_NUM_);
		//エラーメッセージ
		ERROR_CHECK(hr, "initialize失敗", "エラー");
	}
	{
		/*コンスタントバッファ情報*/
		//コンスタントバッファ作成
		hr = CreateConstantBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファの作成失敗", "エラー");
	}
	{
		/*テクスチャ情報*/
		//テクスチャロード
		hr = LoadTexture(FILE_NAME);
		//エラーメッセージ
		ERROR_CHECK(hr, "テクスチャロード失敗", "エラー");
	}

	//処理の成功
	return S_OK;
}

//初期化
HRESULT Grass::Initialize(int polygonNum)
{
	//頂点情報
	//ポリゴンを作る3頂点
	HeightMapGround::VERTEX parentVertex[V_NUM_TO_MAKE_POLYGON_];
	
	//頂点番号
		//元の頂点番号を入れる
		//地面を作る全頂点の中での、頂点番号
	int parentVertexNum[V_NUM_TO_MAKE_POLYGON_] = { -1 , -1 , -1 };
	
	//関数呼び出しによって、
		//vertexNumへ、
		//本来の頂点の順番と同様の順番で、頂点情報が入ってくる（つまり、インデックス情報へ登録する通りに入ってくる）
			//そのため、もともとの頂点番号の配列をもとに、親の頂点番号を登録している配列にアクセスして、
			//子供（自身）の中における頂点の番号と変換することで、
			//インデックスの番号とする。
			//その番号を登録する
	int myVertexNum[V_NUM_TO_MAKE_POLYGON_] = { -1 , -1 , -1 };
	
	//頂点がすでに登録されていたか
		//falseの場合、新たに頂点を登録しなおす必要あり
	bool isExists[V_NUM_TO_MAKE_POLYGON_] = { false , false , false };

	//頂点情報と元の頂点番号を取得
	GetVertex(polygonNum, parentVertex, parentVertexNum);


	/*頂点情報**************************************/
	{

		//頂点番号割り振り
		//上記によって取得した
		//3頂点をもともとの頂点番号を登録
		/*AddParentVertexNum(parentVertexNum[V_NUM_TO_MAKE_POLYGON_ - 3]);
		AddParentVertexNum(parentVertexNum[V_NUM_TO_MAKE_POLYGON_ - 2]);
		AddParentVertexNum(parentVertexNum[V_NUM_TO_MAKE_POLYGON_ - 1]);
		*/
		//引数：元の頂点番号（地面を作る頂点の中での頂点番号）
		//引数：
		//引数：存在していたかのフラグ
		//被りがない場合
		//新たに確保して、確保したときの要素番号をそのまま頂点番号として用いて登録（頂点番号を新規作成）
		//すでに被りがある場合、
		//その頂点番号を、myVertexNumとして、自身のクラスにおける頂点番号に登録させる（頂点番号を使いまわす）
		CheckifExistsAndRegistrationMyVertexNum(parentVertexNum, myVertexNum , isExists);



		//3頂点を頂点情報に登録
		int newVertexCount = 0;
		for (int i = 0; i < V_NUM_TO_MAKE_POLYGON_; i++)
		{
			//条件：存在しないならば
			if (!isExists[i])
			{
				newVertexCount++;
			}
		}
		

		//新規確保先
		HeightMapGround::VERTEX* newVertex = new HeightMapGround::VERTEX[vertexCount_];

		//元の情報を
		//コピー
			//sizeは、カウントする前の数分だけ
		memcpy(newVertex, pVertices_, sizeof(HeightMapGround::VERTEX) * (unsigned long long)((vertexCount_ - newVertexCount)));
		

		//元データは解放
		SAFE_DELETE_ARRAY(pVertices_);

		//データのセットしなおし
		//新データをセット
		pVertices_ = newVertex;
		//新しい頂点情報の確保
		//第3引数：格納開始位置 （頂点数４　新しく追加１→ 4 - 1　→　格納開始[3]からということになる）
		SaveVertexes(parentVertex, isExists, vertexCount_ - newVertexCount);

		//バッファー作成
		//頂点バッファー作成
		HRESULT hr = CreateVertexBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点バッファ失敗", "エラー");
	}

	//ポリゴン数＋１
	polygonCount_++;

	/*インデックス情報**************************************/
	{


		//親の頂点番号を、自身の所有している頂点数からの頂点番号に変換する
		ChangeMyVertexNum(parentVertexNum, myVertexNum);

		//インデックス数を＋３
		indexCountEachMaterial_ += V_NUM_TO_MAKE_POLYGON_;


		//配列を確保
		int* newIndex = new int[indexCountEachMaterial_];

		//元のインデックス情報を
		//コピー
			//sizeは、インデックスをカウントする前の数分だけ
		memcpy(newIndex, pIndex_, sizeof(int) * (unsigned long long)((indexCountEachMaterial_ - (int)V_NUM_TO_MAKE_POLYGON_)));

		//元データを解放して
		//新しく確保したもののアドレスを、セットする
		SAFE_DELETE_ARRAY(pIndex_);
		pIndex_ = newIndex;
		//新しいインデックス情報の確保
		//登録頂点番号（インデックス）：要素３
		//配列への書き込み開始添え字
		SaveIndexes(myVertexNum, indexCountEachMaterial_ - V_NUM_TO_MAKE_POLYGON_);


		//インデックスバッファの確保
		HRESULT hr = CreateIndexBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "インデックスバッファ失敗", "エラー");

	}



	
	//草のテクスチャの元となるポリゴンの結合
	Transform trans;
	trans.position_ = parentVertex[0].position;
	trans.position_ += XMVectorSet(0.f, 0.5f, 0.f, 0.f);
	trans.scale_.vecY = 0.5f;

	//平面ポリゴンを結合する	
		/*
		//一瞬300MBぐらいのプロセスメモリ量になる
		//だが、一回計算を終えたら、100以下まで下がる
		//頂点情報のコピー結合の時点で、メモリをかなり一時期に消費する(拡張先の頂点バッファなどを一時的に確保しているため、一時的に跳ね上げる。だが、不要なものは、消しているため、問題はない)
		*/
	pBranchesMakeTree_->Join3DPolygon(pPlane_, trans);
	
	//平面ポリゴンにおいて、
	//今回は、平面の一方面のポリゴン情報だけ、結合される。
		//しかし、平面は３Dにおいて、裏面も描画してほしい
		//そのため、180度回転させた平面を描画させるように結合させる
	trans.rotate_.vecY += 180;

	//結合クラスに結合
	pBranchesMakeTree_->Join3DPolygon(pPlane_, trans);

	//処理の成功
	return S_OK;

}

//更新
HRESULT Grass::Update(float deltaTime)
{
	//引数にて渡された、デルタタイムを自身の経過時間変数に加算
	elapsedTime_ += deltaTime;


	//頂点を親に知らせ、
	//その頂点の上下左右（すでに登録されているポリゴンは除く）のポリゴンを新たに登録

	//条件：拡張可能回数上限を超えていない
	//　　：&&
	//　　：経過時間が拡張開始の時間の間隔を超えていた場合
	if (expandCounter_ <= MAX_EXPAND_NUM_ && elapsedTime_ >= RANGE_EXPAND_INTERVAL_)
	{
		//経過時間を初期化
		elapsedTime_ = 0.f;

		//ポリゴンを拡張
		HRESULT hr = ExpandThePolygon();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点の範囲拡大の処理失敗", "エラー");

	}

	//処理の成功
	return S_OK;

}

//ポリゴンの拡張
HRESULT Grass::ExpandThePolygon()
{
	//範囲拡大回数をカウントアップ
	expandCounter_++;

	//計算用
	//処理用のリストのコピー
	//直接　保存用を使用せず、コピーし、そちらを使い、範囲拡大の処理を行う
	auto listThisTime = nextPolyTargetArray_;

	//コピーを終えたので、
	//保存用の次の拡大対象は解放
		//以下の処理で、新しい、次の拡大対象が出てくるため、それを保存するために、解放
	nextPolyTargetArray_.clear();


	//上下左右のポリゴンを作る
	//頂点情報を貰う

	//拡大を行うポリゴン事
	//消去の段階で、消去して、次のイテレータを入れて回す回し方
	for (auto itr = listThisTime.begin(); itr != listThisTime.end(); )
	{

		//条件：上下左右に広げる回数分繰り返す
		for (int i = 0; i < (int)EXPAND_TYPE::EXPAND_MAX; i++)
		{
			//広げる基準とする
			//そのポリゴンの番号は知っているが、
			//上下左右に範囲を広げるだけで、
			//その場所のポリゴン番号も、知らない
			//そのため、現在範囲を広げたいポリゴン番号と、範囲を広げる方向だけ示して、
				//あとは、関数先で情報を入れて返してもらう
			int targetPolyNum = -1;

			//拡大先の対象のポリゴン番号を貰う
			pParent_->GetPolygonNumToExpanded((EXPAND_TYPE)i, &targetPolyNum, (*itr));


			//自身が所有しているメンバで、
				//polygonArray -> 塗った判定のポリゴンと、その親とを保存しておく
				//parentArray  -> 実際に塗って、ポリゴンが作られたポリゴン（塗ったポリゴン情報だけ）
					//正直、別々にしておく必要性はなさそう（どちらか一つで代用が可能）

			//自身のGrassが、指定ポリゴンを塗っているかを取得
			//pParent_->IsPaintedPolygon(this, targetPolyNum);


				//ポリゴンを貰えなかったら、
				//それは、範囲を広げられない限界のところに来たということなので、
					//その場合は、何もしない
			//条件：ポリゴン番号をもらえたら
			//　　：&&
			//　　：現在範囲を広げようとしているポリゴンの親のポリゴン番号ではないならば　
			//　　：&&
			//　　：すでに、自身が広げ済みのポリゴン番号でないならば
			if (targetPolyNum != -1 &&
				//GetParentPolyNum((*itr)) != targetPolyNum &&
				//!IsExistsPolyNum(targetPolyNum))
				! pParent_->IsPaintedPolygon(this, targetPolyNum)
				)
			{
				
				//次の対象も、
				//	//polyArrayの配列に確保しているので、個別の判別は必要なし
				//bool isOK = true;
				//for (auto nextItr = nextPolyTargetArray_.begin(); nextItr != nextPolyTargetArray_.end(); nextItr++)
				//{
				//	//次のポリゴン群に追加されているものと同様の時
				//	if ((*nextItr) == targetPolyNum)
				//	{
				//		isOK = false;
				//		break;

				//	
				//	
				//	}
				//
				//
				//
				//}


				//if (isOK)
				{
					//ポリゴンを取得時
						//ポリゴンの番号を登録
					PolygonInfo polyInfo;
					polyInfo.parentPolyNum = (*itr);
					polyInfo.thisPolyNum = targetPolyNum;

					//可変長配列に追加
					polygonsArray_.push_back(polyInfo);


					//今回追加のポリゴンを
					//次の範囲拡大対象に
						//ポリゴン番号をセット
					nextPolyTargetArray_.push_back(targetPolyNum);



					//1ポリゴンを作成し、
					//それに応じて、
					//頂点情報、インデックス情報、頂点バッファー、インデックスバッファーの更新
					HRESULT hr = Initialize(targetPolyNum);
					//エラーメッセージ
					ERROR_CHECK(hr, "initialize失敗", "エラー");

					/*****************************************************************************************/
					//ポリゴンを塗ったことを
					//親のGrassLandへ伝える
						//伝えることで、
						//全体のポリゴン数に対して、現在塗られているポリゴン数が何割を示しているかを確認する
					//pParent_->GrowingOnePolygon(targetPolyNum);
					/*****************************************************************************************/
					//★
					//自身のポインタと、拡張先、塗ったポリゴン番号を知らせる
					pParent_->GrowingOnePolygon(this, targetPolyNum);


				}

					

			}
			




		}
	

		//範囲を広げたものを
		//リスト(コピー済みの、計算対象のリスト)から解放
		//範囲を広げる対象から外す
		itr = listThisTime.erase(itr);
	}


	//処理の成功
	return S_OK;

}

//描画
HRESULT Grass::Draw(Transform & transform)
{
	//自身のDraw
	HRESULT hr = ThisDraw(transform);
	//エラーメッセージ
	ERROR_CHECK(hr, "自身のDraw失敗", "エラー");


	//平面結合クラスのDraw
	{
		Transform planeTrans = transform;
		//planeTrans.scale_.vecY = 0.5f;
			//上記のTransformの段階で、拡大を行ってしまうと、平面のポリゴンの位置座標がずれてしまう。見た目的にずれが生じるため、
			//上記段階で拡大しない。
		planeTrans.Calclation();

		//平面の描画
			//平面を結合して作成したクラスの描画
		//Transformのセット
		Model::SetTransform(hBranchesMakeTree_, planeTrans);
		//描画
		Model::Draw(hBranchesMakeTree_);
	}


	//処理の成功
	return S_OK;
}

//解放
void Grass::Release()
{
	//解放
	HeightMapGround::Release();

	//自身が確保した、拡大したポリゴン番号を親であるGrassLandに解放を知らせる
		//自身が広げた部分を、親から解放してもらう（広げた領域を狭める）
	//条件：ポリゴン数分
	for (int i = 0; i < polygonsArray_.size(); i++)
	{
		/*****************************************************************************************/
		//ポリゴン番号を渡し
			//その番号の塗られ状態を解除する
			//親に自身が塗ったことを知らせているので、逆に、解放することを知らせる
		//pParent_->RemoveOnePolygon(polygonsArray_[i].thisPolyNum);
		/*****************************************************************************************/

		//★
			//自身のポインタと、拡張先、塗ったポリゴン番号を解放
		pParent_->RemoveOnePolygon(this, polygonsArray_[i].thisPolyNum);


	}
}

//削除されているか
bool Grass::IsDead()
{
	return isKillMe_;
}
//Grassが塗られた初めのポリゴンを取得
int Grass::GetFirstPolyNum()
{
	return FIRST_POLY_NUM_;

}
//Grassの縮小開始
void Grass::StartShrinkingGrass()
{
	isKillMe_ = true;
}

//範囲を拡げ済みのポリゴン番号と合計拡げ済みのポリゴン数を返す
void Grass::GetGrowingUpPolygonNumbers(int ** finishedPolygons, int * sumPolygonNum)
{
	//拡大済みのポリゴン数合計
		//Grassのポリゴンの数
	(*sumPolygonNum) = (int)polygonsArray_.size();



	//引数のポインタのポインタ
		//そのポインタ部分に、自身のGrass内の拡大済みのポリゴン数分動的確保
	(*finishedPolygons) = new int[(*sumPolygonNum)];

	//拡大済みのポリゴンのポリゴン番号を
		//引数のポインタのポインタである、配列に確保
	//条件：合計ポリゴン数
	for (int i = 0; i < (*sumPolygonNum); i++)
	{
		(*finishedPolygons)[i] = polygonsArray_[i].thisPolyNum;
	}

}

//頂点情報から頂点バッファーを作成
HRESULT Grass::CreateVertexBuffer()
{
	//解放
	SAFE_RELEASE(pVertexBuffer_);

	//バッファの詳細情報を格納する構造体
	//頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;
	//VERTEXのスコープを定義する
		//Grass::VERTEX
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;

	//バッファへのアクセスポインタ
	//頂点情報のリソースを入れる構造体
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = pVertices_;


	//頂点バッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファ作成失敗", "エラー");

	//処理の成功
	return S_OK;
}


//インデックス情報からインデックスバッファーを作成
HRESULT Grass::CreateIndexBuffer()
{
	//元バッファーを解放
	SAFE_RELEASE(pIndexBuffer_);



	// インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(int) * (indexCountEachMaterial_);	//サイズを、indexのサイズ分だけ、（インデックス情報の入っているサイズだけ）
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;


	//バッファーへのアクセスポインタ
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex_;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	//インデックスバッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "インデックスバッファ作成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//コンスタントバッファの作成	
HRESULT Grass::CreateConstantBuffer()
{
	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//コンスタントバッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "コンスタントバッファ作成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//自身の描画
HRESULT Grass::ThisDraw(Transform & transform)
{
	//処理の結果入れる変数
	HRESULT hr = S_OK;


	//シェーダーを選択
	Direct3D::SetShaderBundle(thisShader_);

	//シェーダーにおけるコンスタントバッファへわたす情報をまとめる
			//詳細：自クラスの　CONSTANT_BUFFER(sturct)とシェーダーファイルにおけるコンスタントバッファの構造体　の格納数、格納情報を等しくしているため、
			//　　：伴い、描画に必要な現在の情報をCONSTANT_BUFFER(sturct)　に格納して、最終的にシェーダーファイルに渡す

	//コンスタントバッファ　格納情報
	HeightMapGround::CONSTANT_BUFFER cb;
	
	//コンスタントバッファ　へのデータを格納（準備）
	{
		//ワールド　＊　ビュー　＊　プロジェクション行列
		cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() * Camera::GetProjectionMatrix());

		//ワールド行列
			//詳細：法線の回転のために必要なワールド行列
		cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));	//

		//ワールド行列
			//詳細：ハイライト表示のためにカメラのワールド座標を求める必要がある 
		cb.matWorld = XMMatrixTranspose(transform.GetWorldMatrix());

		//カメラの座標を入れる
		cb.camPos = XMFLOAT4(Camera::position_.vecX, Camera::position_.vecY, Camera::position_.vecZ, 0);


		//条件：テクスチャあり
		if (pTexture_ != nullptr)
		{
			//nullptrでない　＝　テクスチャが存在する
				//「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
				//サンプラーは、どこに貼り付けますか？などの情報
				//hlsl : g_sampler(s0)
			ID3D11SamplerState* pSampler = pTexture_->GetSampler();
			//第一引数：サンプラー　番目
			Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

			//シェーダーへの橋渡しのビューを取得
			//テクスチャのリソース部分
				//hlsl : g_texture(t0)
			ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
				//第一引数：テクスチャ　番目
			Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);

			//テクスチャ　有
			cb.isTexture = TRUE;
		}
		//条件：テクスチャなし
		else
		{

			//nullptrである　＝　テクスチャが存在しない
				//　＝　マテリアルそのものの色を使用しないといけない

			//テクスチャ　無
			cb.isTexture = FALSE;

		}

	}

	//コンスタントバッファ1の確保
	{
		//シェーダーのコンスタントバッファ１のマップ
			//コンスタントバッファ1へのアクセス許可
		hr = Direct3D::GetShaderClass(thisShader_)->MapShaderConstantBuffer1View();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファMap失敗", "エラー");


		//自身の使用する
			//シェーダー独自の
			//コンスタンバッファ１や、テクスチャの受け渡しを行う
			//処理は、シェーダークラスに一任する
		Direct3D::GetShaderClass(thisShader_)->ShaderConstantBuffer1View();


		//シェーダーのコンスタントバッファ１のアンマップ
			//コンスタントバッファ1へのアクセス拒否
		hr = Direct3D::GetShaderClass(thisShader_)->UnMapShaderConstantBuffer1View();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファUnMap失敗", "エラー");
	}


	//コンスタントバッファをシェーダーファイルへコピー、渡す
	{
		//バッファへのマップ設定
			//ポインタへのアクセスなどに使用する
		D3D11_MAPPED_SUBRESOURCE pdata;
		//コンスタントバッファへのマップ
			//詳細：アクセス許可
			//　　：シェーダーなどのGPUでしか、触れることのできないものに
			//　　：CPUから触れる準備
			//　　：マップを行い、バッファへのアクセスができるようになるが、その詳細のアクセスを上記pdataにて行う。
		hr = Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファのマップの書き込み失敗", "エラー");


		//メモリにコピー
			//シェーダーファイル側のコンスタントバッファ（struct）へ 
			//データを値を送る
		memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));

	}

	//頂点情報のセット
	{
		//頂点バッファをセット（頂点を持っておくための領域（確保場所））
			//詳細：複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		//1頂点のサイズを指定する
			//詳細：位置のみであれば、XMVECTORの構造体であったが
			//　　：位置やUVも確保するため、頂点情報の構造体のサイズを受け取らせる
		UINT stride = sizeof(VERTEX);
		UINT offset = 0;
		//頂点バッファのセット
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

		//サイズの更新
			//詳細：インデックスバッファに登録している情報は、int情報の配列
			//　　：そのため、intのサイズを渡す
		stride = sizeof(int);
		offset = 0;
		//インデックスバッファのセット
		Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	}

	//コンスタントバッファをセット
	{
		//コンスタントバッファをセット
		//頂点シェーダー用	
		Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);
		//ピクセルシェーダー用
		Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);

		//コンスタントバッファへのアンマップ
			//詳細：アクセス拒否
		Direct3D::pContext->Unmap(pConstantBuffer_, 0);

	}


	//描画
		//詳細　　：インデックス数を渡して、頂点によるポリゴンの描画を宣言 
		//第一引数：描画インデックス数　描画の対象となるポリゴンを作るインデックスの数
	Direct3D::pContext->DrawIndexed(indexCountEachMaterial_, 0, 0);

	//処理の成功
	return S_OK;
}

//引数の親における頂点番号をもとに自身のクラスにおける頂点番号へと変換する
void Grass::ChangeMyVertexNum(const int parentVertexNum[V_NUM_TO_MAKE_POLYGON_] , int* myVertexNum)
{
	//スコープ：�@
	//自身の可変配列に登録している
	//要素一つ一つにアクセスして、
		//頂点の中から引数にて渡された要素が、自身のクラスにおける何番目の頂点情報化を判断する
	//条件：親のポリゴン
	for (int i = 0; i < parentVertexArray_.size(); i++)
	{
		//条件：ポリゴンを作る頂点数分
		for (int k = 0; k < V_NUM_TO_MAKE_POLYGON_; k++)
		{
			//条件：可変配列に登録した値と引数における親の頂点番号とで同様ならば
			if (parentVertexArray_[i] == parentVertexNum[k])
			{
				//添え字が、自身の頂点番号となるので、登録
				myVertexNum[k] = i;
			}
		}

		//スコープ：�A
		//条件：3頂点すべてが　−1という初期値でない
		if (myVertexNum[V_NUM_TO_MAKE_POLYGON_ - 3] != -1 &&
			myVertexNum[V_NUM_TO_MAKE_POLYGON_ - 2] != -1 &&
			myVertexNum[V_NUM_TO_MAKE_POLYGON_ - 1] != -1)
		{
			//処理抜ける
				//スコープ：�@
			break;
		}
		
	}

}

//インデックス情報の登録
void Grass::SaveIndexes(const int myVertexNum[V_NUM_TO_MAKE_POLYGON_], int startSuffix)
{
	//引数にて示された書き込みを開始する添え字番号から
	//3頂点書き込む
	//条件：3頂点
	for (int i = 0; i < V_NUM_TO_MAKE_POLYGON_; i++)
	{
		//インデックス情報を持つ配列に
		//登録
		pIndex_[startSuffix + i] = myVertexNum[i];
	}
}

//頂点情報の登録
void Grass::SaveVertexes(const VERTEX * vertexInfo, const bool * isExists, int startSuffix)
{
	//フラグが立っているものから
	//格納していく
	//添え字のカウンター
	int suffix = 0;

	//条件：3頂点分
	for (int i = 0; i < V_NUM_TO_MAKE_POLYGON_; i++)
	{
		//条件：存在しないなら
		if (!isExists[i])
		{
			//配列に頂点情報の確保
				//startには、要素格納添え字から始まるので、0オリジンからsuffixのカウントを始める
			pVertices_[startSuffix + suffix] = vertexInfo[i];
			//カウンター回す
			suffix++;
		}
		
	}
/*
	pVertices_[V_NUM_TO_MAKE_POLYGON_ - 3] = parentVertex[V_NUM_TO_MAKE_POLYGON_ - 3];
	pVertices_[V_NUM_TO_MAKE_POLYGON_ - 2] = parentVertex[V_NUM_TO_MAKE_POLYGON_ - 2];
	pVertices_[V_NUM_TO_MAKE_POLYGON_ - 1] = parentVertex[V_NUM_TO_MAKE_POLYGON_ - 1];
*/
}

//2つの値が同様の値かを調べる
bool Grass::CheckSameValue(bool * isSame, int v1, int v2)
{
	//すでにTrueが入っている
	if ((*isSame))
	{
		//引数2つが等しいわけではないので、falseを返す
		return false;
	}

	//引数の値が同じ
	if (v1 == v2)
	{
		(*isSame) = true;
		//値が同じだったので、戻値はtrue
		return true;
	}

	(*isSame) = false;
	return false;
}

//引数にてもらったポリゴン番号の親のポリゴン番号を返す
int Grass::GetParentPolyNum(int thisPolyNum)
{
	//条件：ポリゴン群
	for (int i = 0; i < polygonsArray_.size(); i++)
	{
		//条件：親ポリゴン群のポリゴン番号と引数ポリゴン番号が等しい時
		if (polygonsArray_[i].thisPolyNum == thisPolyNum)
		{
			//親ポリゴン群の親のポリゴン番号を返す
			return polygonsArray_[i].parentPolyNum;
		}
	}

	//該当ポリゴンなし
	return -1;
}

//自身が所有しているポリゴン群ですでに広げ済みであるかの確認
bool Grass::IsExistsPolyNum(int thisPolyNum)
{
	//return (*std::find(parentVertexArray_.begin(), parentVertexArray_.end(), thisPolyNum)) == thisPolyNum;

	//条件：親ポリゴン数
	for (int i = 0; i < parentVertexArray_.size(); i++)
	{
		//条件：親ポリゴンと引数ポリゴンが等しい
		if (parentVertexArray_[i] == thisPolyNum)
		{
			//存在する
			return true;
		}
	}

	//存在しない
	return false;

}

//加えようとしている頂点番号がすでに登録されていないかを確認
void Grass::CheckifExistsAndRegistrationMyVertexNum(const int parentVertexNums[V_NUM_TO_MAKE_POLYGON_], 
	int * myVertexNum, bool* isExists)
{
	
	//スコープ：�@
	//配列から
	//同様の値があるかを調べる
	//同様の値があった：自身のクラスにおける頂点番号を入れる
	//同様の値がなかった：繰り返し終了後、新しく要素を確保し、要素番号を頂点番号とする
	for (int i = 0; i < parentVertexArray_.size(); i++)
	{
		//条件：添え字にて示される、頂点配列に格納されている番号と
		//　　：引数頂点番号配列とで、要素が同様か

		//3頂点分調べる
		if (CheckSameValue(&isExists[0], parentVertexArray_[i], parentVertexNums[0]))
		{
			//自身のクラスにおける頂点番号として登録
			myVertexNum[0] = i;
		}
		if (CheckSameValue(&isExists[1], parentVertexArray_[i], parentVertexNums[1]))
		{
			myVertexNum[1] = i;
		}
		if (CheckSameValue(&isExists[2], parentVertexArray_[i], parentVertexNums[2]))
		{
			myVertexNum[2] = i;
		}

		//スコープ：�A
		//3つとも存在しているというフラグがたったら
		if (isExists[0] && isExists[1] && isExists[2])
		{
			//処理終了
				//スコープ：�@
			break;
		}

	}

	//条件：3頂点
	for (int i = 0; i < V_NUM_TO_MAKE_POLYGON_; i++)
	{
		//条件：存在していないなら
		if (!isExists[i])
		{
			//存在していなかったら、
				//新たに頂点番号として確保
				//追加
			AddParentVertexNum(parentVertexNums[i]);

			//最大要素 - 1
			//全体サイズ - 1になるので、
			//追加したときの頂点番号を取れる
			myVertexNum[i] = (int)parentVertexArray_.size() - 1;

		}
	}



}

//元の頂点番号を保存しておく配列へ追加（parentVertexArray_）
void Grass::AddParentVertexNum(int vertexNum)
{
	parentVertexArray_.push_back(vertexNum);
	vertexCount_++;

}

//元の頂点番号を保存しておく配列へ追加（parentVertexArray_）
void Grass::RemoveParentVertexNum(int vertexNum)
{
}

//ポリゴン番号からポリゴンを作る頂点情報を取得する
void Grass::GetVertex(int polygonNum, HeightMapGround::VERTEX * pVertex , int* vertexNum)
{
	//ポリゴンの番号を作る
	//頂点情報を取得する
	pParent_->GetVertex(polygonNum, pVertex, vertexNum);

}

