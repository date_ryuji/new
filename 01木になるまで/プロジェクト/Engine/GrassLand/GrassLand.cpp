#include <math.h>						//計算用アルゴリズム　格納ヘッダ
#include "GrassLand.h"					//ヘッダ
#include "../DirectX/Camera.h"			//カメラオブジェクト
#include "../DirectX/Texture.h"			//テクスチャ
#include "../../Shader/BaseShader.h"	//シェーダークラス　継承元クラス
#include "../Algorithm/Timer.h"			//時間計測
#include "Grass.h"
	//子供クラス
		//動的に、自身のクラスのポリゴンと同様のポリゴンを1つ持たせ、
		//そのポリゴンを、動的に増やしていくクラス
		//そのポリゴンを、自身とは別のテクスチャを持たせることで、
		//→色が塗られた表現を疑似表現する


//コンストラクタ
GrassLand::GrassLand():
	HeightMapGround::HeightMapGround(),
	//growingUpPolygons_(nullptr),
	listGrassWithPaintedPolygons_(nullptr),

	isStopTime_(false)
{
	//可変長配列クリア
	myGrasses_.clear();
}

//デストラクタ
GrassLand::~GrassLand()
{
	HeightMapGround::~HeightMapGround();
	SAFE_DELETE_ARRAY(listGrassWithPaintedPolygons_);
	//SAFE_DELETE_ARRAY(growingUpPolygons_);
}

//ロード
HRESULT GrassLand::Load(const std::string& FILE_NAME , SHADER_TYPE thisShader)
{
	//継承元のロードを呼びこむ
	HeightMapGround::Load(FILE_NAME, thisShader);

	//自身のポリゴンがGrassクラスに塗られているかを判定する配列の初期化
	//InitGrowingUpPolygons();

	//リストの動的確保と初期化
	InitGrassWithPaintedPolygons();

	//テスト
	//Grassクラス（ポリゴンをコピーして、ポリゴン範囲を動的に広げていくクラス）の生成
	//CreateGrassPolygon((width_ - 1) * 10);

	//時間のリセット
	Timer::Reset();

	//処理の成功
	return S_OK;
}

//ポリゴンの塗り状態を確認するリストの初期化
void GrassLand::InitGrassWithPaintedPolygons()
{
	//リストの動的確保
		//ポリゴン数分、要素を確保
	listGrassWithPaintedPolygons_ = new std::list<Grass*>[polygonCount_];
	//初期化
	//条件：ポリゴン数
	for (int i = 0; i < polygonCount_; i++)
	{
		//１リストにアクセスして、初期化
		listGrassWithPaintedPolygons_[i].clear();
	}

}

//描画
HRESULT GrassLand::Draw(Transform & transform)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	//自身のクラスのDrawを行う
	hr = ThisDraw(transform);
	//エラーメッセージ
	ERROR_CHECK(hr, "自身のDraw失敗", "エラー");


	//条件：時間停止がされていなかったら
	if(!IsStopTime())
	{ 
		//デルタ時間（前回のフレームから、今回のフレームまでの経過時間）を取得
		float elpasedTime = Timer::GetDelta();

		//GrassのUpdateとDraw実行
		hr = UpdateAndDrawGrass(transform, elpasedTime);
		//エラーメッセージ
		ERROR_CHECK(hr, "Grass描画失敗", "エラー");

	}
	//条件：時間停止がされていたら
	else
	{
		//時間停止が宣言されているとき
			//経過時間は０．ｆとすることで、時間が経過していない表現をする
			//時間停止時にUpdateAndDrawを呼ぶ理由は、
			//時間停止時も、Drawは行いたい。だが、その場合、Updateの時と同様に、リストを回しながら、一つ一つDrawをする必要が出てくる
				//であれば、同じ内容を別の関数にでも分けて書いてしまうのはコードの無駄のため、UpdateAndDrawで０．ｆ→つまり、更新しないが、Drawはｓうるという処理を行う

		//GrassのUpdateとDraw実行
		hr = UpdateAndDrawGrass(transform, 0.f);
		//エラーメッセージ
		ERROR_CHECK(hr, "Grass描画失敗", "エラー");

	}

	//処理の成功
	return S_OK;
}

//GrassのUpdateとDraw実行
HRESULT GrassLand::UpdateAndDrawGrass(Transform& transform , float elpasedTime)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;


	//子供クラスである
	//Grass群の描画実行
	{
		//子供クラスでは、
		//自身である親クラスよりも、一定量高いY座標で描画を行ってほしい
			//そのため、
			//Y座標を少し上げたTransformを渡し、描画を行う
		Transform trans = transform;
		trans.position_.vecY += 0.01f;
		//計算しなおし
		trans.Calclation();

		//条件：GrassLandが所有している　全Grass分
		for (auto itr = myGrasses_.begin(); itr != myGrasses_.end();)
		{
			//子供クラスのGrassをすべて参照し
				//消去フラグが立っていたら、削除
				//立っていなかったら、描画を行う
			//条件：消去フラグがたっていなかったら
			if (!(*itr)->pGrass->IsDead())
			{
				//死亡フラグが立っていなかったら


				//描画の前にポリゴン情報の更新を行わせる
				//ポリゴンの範囲を広げられるときは、範囲を広げる処理を行う
					//フレームの経過時間を渡す
					//そして、一定時間経過をしていた場合、
					//上記の処理を行わせる
				hr = (*itr)->pGrass->Update(elpasedTime);
				//エラーメッセージ
				ERROR_CHECK(hr, "Grass（子供クラス）の範囲拡大処理の失敗", "エラー");

				//描画
				(*itr)->pGrass->Draw(trans);

				//イテレータ回す
				itr++;
			}
			else
			{
				//死亡フラグが立っていたら
				//リストから削除

				//解放処理
				(*itr)->pGrass->Release();
				//動的確保のDelete
				SAFE_DELETE((*itr)->pGrass);

				//構造体の動的確保したものを解放
				SAFE_DELETE((*itr));

				//リストから削除して、
				//戻値にて、次のイテレータを取得
				itr = myGrasses_.erase(itr);

			}



		}
	}

	//処理の成功
	return S_OK;

}


//自身の描画
HRESULT GrassLand::ThisDraw(Transform & transform)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;


	//シェーダーを選択
	Direct3D::SetShaderBundle(thisShader_);

	//シェーダーにおけるコンスタントバッファへわたす情報をまとめる
			//詳細：自クラスの　CONSTANT_BUFFER(sturct)とシェーダーファイルにおけるコンスタントバッファの構造体　の格納数、格納情報を等しくしているため、
			//　　：伴い、描画に必要な現在の情報をCONSTANT_BUFFER(sturct)　に格納して、最終的にシェーダーファイルに渡す

		//コンスタントバッファ　格納情報
	CONSTANT_BUFFER cb;

	//コンスタントバッファ　へのデータを格納（準備）
	{
		//ワールド　＊　ビュー　＊　プロジェクション行列
		cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() * Camera::GetProjectionMatrix());

		//ワールド行列
			//詳細：法線の回転のために必要なワールド行列
		cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));	
	
		//ワールド行列
			//詳細：ハイライト表示のためにカメラのワールド座標を求める必要がある 																								//頂点からカメラへの向きというのを取得するために、頂点に掛けてやらないといけない（移動、回転、拡大を込みしたワールド位置にするために）
		cb.matWorld = XMMatrixTranspose(transform.GetWorldMatrix());

		//カメラの座標を入れる
		cb.camPos = XMFLOAT4(Camera::position_.vecX, Camera::position_.vecY, Camera::position_.vecZ, 0);


		//条件：テクスチャがある
		if (pTexture_ != nullptr)
		{


			//nullptrでない　＝　テクスチャが存在する
				//「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
				//サンプラーは、どこに貼り付けますか？などの情報
				//hlsl : g_sampler(s0)
			ID3D11SamplerState* pSampler = pTexture_->GetSampler();
			//第一引数：サンプラー　番目
			Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

			//シェーダーへの橋渡しのビューを取得
			//テクスチャのリソース部分
				//hlsl : g_texture(t0)
			ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
				//第一引数：テクスチャ　番目
			Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);

			//テクスチャ　有
			cb.isTexture = TRUE;
		}
		//条件：テクスチャなし
		else
		{

			//nullptrである　＝　テクスチャが存在しない
				//　＝　マテリアルそのものの色を使用しないといけない

			//テクスチャ　無
			cb.isTexture = FALSE;

		}
	}
	//コンスタントバッファ1の確保
	{
		//シェーダーのコンスタントバッファ１のマップ
			//コンスタントバッファ1へのアクセス許可
		hr = Direct3D::GetShaderClass(thisShader_)->MapShaderConstantBuffer1View();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファMap失敗", "エラー");


		//自身の使用する
			//シェーダー独自の
			//コンスタンバッファ１や、テクスチャの受け渡しを行う
			//処理は、シェーダークラスに一任する
		Direct3D::GetShaderClass(thisShader_)->ShaderConstantBuffer1View();


		//シェーダーのコンスタントバッファ１のアンマップ
			//コンスタントバッファ1へのアクセス拒否
		hr = Direct3D::GetShaderClass(thisShader_)->UnMapShaderConstantBuffer1View();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファUnMap失敗", "エラー");
	}

	//コンスタントバッファをシェーダーファイルへコピー、渡す
	{
		//バッファへのマップ設定
			//ポインタへのアクセスなどに使用する
		D3D11_MAPPED_SUBRESOURCE pdata;
		//コンスタントバッファへのマップ
			//詳細：アクセス許可
			//　　：シェーダーなどのGPUでしか、触れることのできないものに
			//　　：CPUから触れる準備
			//　　：マップを行い、バッファへのアクセスができるようになるが、その詳細のアクセスを上記pdataにて行う。
		hr = Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファのマップの書き込み失敗", "エラー");


		//メモリにコピー
			//シェーダーファイル側のコンスタントバッファ（struct）へ 
			//データを値を送る
		memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));

	}




	//頂点情報のセット
	{
		//頂点バッファをセット（頂点を持っておくための領域（確保場所））
			//詳細：複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		//1頂点のサイズを指定する
			//詳細：位置のみであれば、XMVECTORの構造体であったが
			//　　：位置やUVも確保するため、頂点情報の構造体のサイズを受け取らせる
		UINT stride = sizeof(VERTEX);
		UINT offset = 0;
		//頂点バッファのセット
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

		//サイズの更新
			//詳細：インデックスバッファに登録している情報は、int情報の配列
			//　　：そのため、intのサイズを渡す
		stride = sizeof(int);
		offset = 0;
		//インデックスバッファのセット
		Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	}

	//コンスタントバッファをセット
	{
		//コンスタントバッファをセット
		//頂点シェーダー用	
		Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);
		//ピクセルシェーダー用
		Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);

		//コンスタントバッファへのアンマップ
			//詳細：アクセス拒否
		Direct3D::pContext->Unmap(pConstantBuffer_, 0);

	}
	//描画
		//詳細　　：インデックス数を渡して、頂点によるポリゴンの描画を宣言 
		//第一引数：描画インデックス数　描画の対象となるポリゴンを作るインデックスの数
	Direct3D::pContext->DrawIndexed(indexCountEachMaterial_, 0, 0);




	//処理の成功
	return S_OK;



}

//解放
void GrassLand::Release()
{
	HeightMapGround::Release();
	//自身のクラスの動的確保解放
	//子供クラス群の解放
	//条件：GrassLand管理の全Grass
	for (auto itr = myGrasses_.begin(); itr != myGrasses_.end();)
	{
		(*itr)->pGrass->Release();
		SAFE_DELETE((*itr)->pGrass);

		//構造体の動的確保したものを解放
		SAFE_DELETE((*itr));

		//リストから削除して、
		//戻値にて、次のイテレータを取得
		itr = myGrasses_.erase(itr);
	}
	//解放
	myGrasses_.clear();

}

//時間停止中か
bool GrassLand::IsStopTime()
{
	return isStopTime_;
}

//時間停止
void GrassLand::StopTime()
{
	isStopTime_ = true;
}

//時間計測再開
void GrassLand::StartTime()
{
	isStopTime_ = false;
}

//引数ポリゴンが緑ポリゴンか
bool GrassLand::IsGreenPolygon(int polyNum)
{
	//塗り状況を
	//カウント数で管理していた場合
	{
		////ポリゴンの塗り状態を一次元配列で管理していて、
		//	//	//ポリゴン番号 - 1 より、そのポリゴンの状態を取得できる
		//			//取得した情報により、　０であれば、そのポリゴンは塗られていない。
		//			//						1以上であれば、そのポリゴンは塗られていると判断できる


		////指定ポリゴンが
		//	//上記の条件に倣って、

		//	//1以上であるならば、　緑ポリゴン（True）
		//	//1以上でないならば、　黒ポリゴン（False）
		//return growingUpPolygons_[polyNum - 1] >= 1;
	}

	//塗り状況を
	//Grassクラスにて管理する場合
	{
		//指定ポリゴンを塗っているGrass軍のリスト
			//その初期値を取得
		auto beginItr = listGrassWithPaintedPolygons_[polyNum - 1].begin();
		//終了値を取得
			//中身がからである場合、begin()と同様の値が入る
		auto endItr = listGrassWithPaintedPolygons_[polyNum - 1].end();


		//開始と終了が違う
			//＝Grassクラスが一つでも存在する
		return beginItr != endItr;

	}
}

//引数にてもらったポリゴン番号のポリゴンを作る頂点を取得する
void GrassLand::GetVertex(int polygonNum, VERTEX * pVertex, int * vertexNum)
{
	//ポリゴン番号から
	//それを作る頂点情報と、頂点番号を登録

	//ポリゴンが
	//範囲外を示すことがあるので、
		//それを回避
		
	int myVertexNum[V_NUM_TO_MAKE_POLYGON_] =
	{
		pIndex_[(polygonNum * V_NUM_TO_MAKE_POLYGON_) + 0] ,
		pIndex_[(polygonNum * V_NUM_TO_MAKE_POLYGON_) + 1] ,
		pIndex_[(polygonNum * V_NUM_TO_MAKE_POLYGON_) + 2]
	};
	
	//頂点番号を登録
	//条件：3頂点分
	for (int i = 0; i < V_NUM_TO_MAKE_POLYGON_; i++)
	{
		//頂点情報取得
		pVertex[i] = pVertices_[myVertexNum[i]];
		//頂点番号
		vertexNum[i] = myVertexNum[i];
	}

}

//引数にてもらったポリゴン番号の上下左右、それぞれのポリゴン番号を取得する
void GrassLand::GetPolygonNumToExpanded(EXPAND_TYPE type, int * target, int original)
{
	//現段階では
	//横＊縦に並べた時の、横端や、縦端という間隔よりも、

	//頂点情報などを、一次元配列で管理しているので、
	//横並びで、番号を取得することしかできない

	//そのため、実際に横＊縦に並べている中で、
	//次の行に行ってしまっているのに、　範囲を広げることができると判定されて、値を入れて返してしまうことが起きる
	//例：４＊５　で、　(x 5 , y 3)のポリゴンが仮にほしいとされたときに、横＊縦でポリゴンが管理されていれば、普通はアクセスできないのだが、
			//今回は、一次元配列で管理しているので、現段階では、　x5 * y3 = 15は、ポリゴン合計数の範囲内だから、範囲外になっていないねと返してしまう


	//それぞれの方向の値を取得する
	int expandValue[(int)EXPAND_TYPE::EXPAND_MAX] =
	{
		//UP
		//基準 - (width - 1)
		original - (width_ - 1), 
		//DOWN
		//基準 + (width - 1)
		original + (width_ - 1),
		//RIGHT
		//基準 + 1
		original + 1,
		//LEFT
		//基準 - 1
		original - 1

	};

	//引数タイプの値を取得
	int toPolyNum = expandValue[(int)type];

	//値が範囲外でないかを調べる
	if (toPolyNum < 1 ||
		toPolyNum >= polygonCount_)
	{
	//範囲外なら
		//ー１代入
		toPolyNum = -1;
	}


	//引数の参照渡しのポインタに代入
	(*target) = toPolyNum;



}

//引数にてもらったポリゴン番号からポリゴンをコピーして、新規Grassクラスを生成する
HRESULT GrassLand::CreateGrassPolygon(int polyNum)
{
	//子供の生成
	Grass* pGrass = new Grass(polyNum, this);
	//Grass* pGrass = new Grass(0, this);
	//初期化、ロード
	HRESULT hr = pGrass->Load("Assets/Scene/Image/GameScene/HeightMap/Grass256.png", SHADER_TYPE::SHADER_GRASS);
	//エラーメッセージ
	ERROR_CHECK(hr, "Grassクラスのロード失敗", "エラー");

	//リストに登録する動的構造体を確保
	GrassInfo* pGrassInfo = new GrassInfo(pGrass , polyNum);

	//リストへ追加
	myGrasses_.push_back(pGrassInfo);

	//処理の成功
	return S_OK;

}

//ポリゴン番号にて示されるローカル座標を取得する
bool GrassLand::GetLocalPosOfPolygon(XMVECTOR* centerLocalPos, int polyNum)
{
	//条件：範囲外のポリゴンである
	if (polyNum < 1 || polyNum > polygonCount_)
	{
		(*centerLocalPos) = XMVECTOR();	//初期値のベクターを入れて返す
		//取得できなかった
		return false;
	}


	//ポリゴンから
	//インデックス情報にアクセスして
		//インデックス情報の 0,1,2　を３角形の頂点として取得
	XMVECTOR localPos[3] = 
	{
		//インデックスの((polyNum * 3) + 0)番目に格納されている頂点番号　の頂点座標
		//0
		pVertices_[pIndex_[((polyNum - 1) * 3) + 0]].position,
		//1
		pVertices_[pIndex_[((polyNum - 1) * 3) + 1]].position,
		//2
		pVertices_[pIndex_[((polyNum - 1) * 3) + 2]].position,


	
	};

	//３頂点から作られる、三角形の中心を求める
	//三角形の重心の位置ベクトルの求め方
		//(ベクトルA + ベクトルB + ベクトルC)  / 3
	(*centerLocalPos) = (localPos[0] + localPos[1] + localPos[2]) / 3.0f;

	//取得できた
	return true;
}

//合計ポリゴン数の取得
int GrassLand::GetPolygonCount()
{
	return polygonCount_;
}

//頂点を敷き詰めたときのWidthとDepthの取得
void GrassLand::GetWidthAndDepth(int * getWidth, int * getDepth)
{
	(*getWidth) = width_;
	(*getDepth) = depth_;

}

//ポリゴンのサイズを取得
float GrassLand::GetPolygonSize()
{
	return DEFAULT_SCALE_;
}

//木オブジェクトが解放されて　拡大させたGrassの範囲を狭める
void GrassLand::StartShrinkingGrass(int targetPolyNum)
{
	//全要素から、
	//該当のオブジェクトを探す
	//条件：GrassLand管理の全Grass
	for (auto itr = myGrasses_.begin(); itr != myGrasses_.end(); itr++)
	{
		//Grassの所有している、
		//条件：Grass自身が範囲を拡大し始めたときの、一番最初の原点のポリゴンと等しい時
		if ((*itr)->pGrass->GetFirstPolyNum() == targetPolyNum)
		{
			//ポリゴンが同様の時
			//そのクラスへ、
				//縮小を開始させることを宣言
			(*itr)->pGrass->StartShrinkingGrass();

		}

	}
}

//Grassを拡げている、Grassが繁殖しているポリゴンの合計数を取得する
int GrassLand::GetTotalNumberOfPolygonWithGrass()
{
	/*
	* ★
	* 
		毎回　ポリゴン数分回してしまうのは、非効率に思えるので、
		//Grassを塗るときなどに、
			新しくGrassが塗られたならば、現在塗っているポリゴン数をカウントアップ
		//Grassを消去して、
			ほかに存在するGrassがいなくなったら那波、現在塗っているポリゴン数をカウントダウン

		上記で、ポリゴン数をカウントするほうが、効率は良さそう。
	*/


	//塗り状況を
	//カウント数で管理していた場合
	{
		////全ての子供から
		//	//その子供Grassの広げたポリゴンの番号を取得する

		////ポリゴン番号の塗られ状態
		//	//そのポリゴンがGrassによって、塗られている→1以上
		//	//そのポリゴンがGrassによって、塗られていない→０
		////塗られている→1以上のポリゴンの場合、塗られた合計のポリゴン数をカウントアップして、最終的なポリゴン合計数を取得する

		////ポリゴン合計数カウンター
		//int sumPolygonCount = 0;

		//for (int i = 0; i < polygonCount_; i++)
		//{
		//	//0の場合、0を加算。1以上の場合、1を加算
		//		//min関数へ入れると、第一引数と、第二引数とで、小さい方を返す
		//		//つまり、第一引数　0の場合、0が返ってきて、1の場合、1が返ってきて、2以上の場合、第二引数の1が返ってくる
		//	sumPolygonCount += min(growingUpPolygons_[i], 1);
		//}

		////合計数を返す
		//return sumPolygonCount;

	}



	//塗り状況を
	//Grassクラスにて管理する場合
	{
		//ポリゴン合計数カウンター
		int sumPolygonCount = 0;
		//条件：ポリゴン数分
		for (int i = 0; i < polygonCount_; i++)
		{
			
			//指定ポリゴンを塗っているGrass軍のリスト
			//その初期値を取得
			auto beginItr = listGrassWithPaintedPolygons_[i].begin();
			//終了値を取得
				//中身がからである場合、begin()と同様の値が入る
			auto endItr = listGrassWithPaintedPolygons_[i].end();

			//条件：イテレータ初期値とイテレータ最終値が同じ場合
			if (beginItr != endItr)
			{
				//Grassが存在するのでカウントアップ
				sumPolygonCount++;
			}


		}

		//合計数を返す
		return sumPolygonCount;

	}

}

//ポリゴンが塗られたことを報告
void GrassLand::GrowingOnePolygon(Grass* pGrass, int polyNum)
{
	//一次元配列に引数ポリゴン番号でアクセスする
			//〇添え字：０オリジン。　引数ポリゴン番号：１オリジン。
		//アクセスした配列の要素（リスト）に、引数のGrassをプッシュバックする
	listGrassWithPaintedPolygons_[polyNum - 1].push_back(pGrass);

}

//ポリゴンの塗りが解除されたことを報告
void GrassLand::RemoveOnePolygon(Grass* pGrass, int polyNum)
{
	//一次元配列に引数ポリゴン番号でアクセスし、対象リスト取得
		//対象リストの全要素の中から、引数ｐGrassを取得し、リストから削除する

	//対象リストへアクセスするためのポインタ
		//配列から、引数添え字でアクセスして、そのアドレスをポインタに渡す
	std::list<Grass*>* pTargetList = &(listGrassWithPaintedPolygons_[polyNum - 1]);

	//イテレータ：�@
	//リストから
		//引数Grassを探す
	//条件：ターゲットとなるポリゴンを塗っているGrass　分
	for (auto itr = pTargetList->begin(); itr != pTargetList->end(); itr++)
	{
		//イテレータ：�A
		//条件：イテレータから示されるポインタと、引数が等しい時
		if ((*itr) == pGrass)
		{
			//イテレータを使用して、要素をリストから解放
			pTargetList->erase(itr);
			
			//処理を抜ける
				//イテレータ：�@
			break;
		}

	}

}

//引数ポリゴンを、引数Grassが塗っているかの判定
bool GrassLand::IsPaintedPolygon(Grass* pGrass, int polyNum)
{
	//一次元配列に引数ポリゴン番号でアクセスし、対象リスト取得
		//対象リストの全要素の中に、引数Grassが存在するかの判断
		//存在しているならば、True,
		//存在していないならば、Falseを返す

	//対象リストへアクセスするためのポインタ
	std::list<Grass*>* pTargetList = &(listGrassWithPaintedPolygons_[polyNum - 1]);

	//リストから
		//引数Grassを探す
	//条件：ターゲットとなるポリゴンを塗っているGrass　分
	for (auto itr = pTargetList->begin(); itr != pTargetList->end(); itr++)
	{
		//条件：イテレータから示されるポインタと、引数が等しい時
		if ((*itr) == pGrass)
		{
			//引数Grassが、指定ポリゴンを塗っているとして、
				//Trueを返す
			return true;
		}
	}

	//引数Grassが、指定ポリゴンを塗っていないとして、
		//Falseを返す
	return false;

}

//特定ポリゴンを緑ポリゴンにする
void GrassLand::CreateOneGreenPolygon(int polyNum)
{
	//一番目のGrassの要素にアクセスし
		//一番目のGrassに引数ポリゴンのポリゴンを結合させ、緑ポリゴンを作成させる
		//Grassを新規に作成するわけではない
	auto firstGrass = myGrasses_.begin();
	
	//条件：イテレータ初期値とGrassLand管理のGrass群のイテレータ最終値が等しくない
	if (firstGrass != myGrasses_.end())
	{
		//初期化を行わせる
		(*firstGrass)->pGrass->Initialize(polyNum);
	}
}

//指定ポリゴンが緑ポリゴン（全Grassの中で誰も塗っていなかったら）
void GrassLand::IfNotPaintedCreateOneGreenPolygon(int polyNum)
{
	//条件：ポリゴンが範囲内でない
		//ポリゴン番号が
		//存在する番号であれか調べる
		//ポリゴン番号　最低：１
		//ポリゴン番号　最高：polygonCount_ - 1
	if (polyNum <= 0 || polyNum >= polygonCount_)
	{
		return;
	}


	//一番目のGrassの要素にアクセスし
	//引数ポリゴン番号が誰にも塗られていなかったら
		//一番目のGrassにポリゴンを持たせる
	auto firstGrass = myGrasses_.begin();

	//条件：イテレータ初期値とGrassLand管理のGrass群のイテレータ最終値が等しくない
	if (firstGrass != myGrasses_.end())
	{
		//条件：リストが空ならば
			//添え字にて示されるリストが、
					//中身が空ならば、
					//Grassへ、ポリゴンを持たせる
				//空　＝　イテレータの最初と最後が一緒
			//添え字（０オリジン）
		if (listGrassWithPaintedPolygons_[polyNum - 1].begin() 
			== listGrassWithPaintedPolygons_[polyNum - 1].end())
		{
			//Grassに、引数ポリゴンの要素を持たせる
			//ポリゴン番号（１オリジン）
			(*firstGrass)->pGrass->Initialize(polyNum);

		}
	
	}

}

//Grassに、地面の全ポリゴンを持たせる
void GrassLand::CreateAllGreenPolygon()
{
	//一番目のGrassの要素にアクセスし
		//一番目のGrassに現在塗られていない（緑のポリゴンでない）ポリゴンを持たせる
	auto firstGrass = myGrasses_.begin();

	//条件：イテレータ初期値とGrassLand管理のGrass群のイテレータ最終値が等しくない
	if (firstGrass != myGrasses_.end())
	{
		//全ポリゴンの塗り状況を確認し
			//塗られていないポリゴンがあれば、イテレータにて示した、Grassへ、ポリゴンを持たせる（ポリゴンを拡張）
			//配列添え字　：０オリジン
			//ポリゴン番号：１オリジン
				//ポリゴン数は、合計ポリゴン数　ー１が最大になっているため、１減らす
		//条件：ポリゴン数　分
		for (int i = 0; i < polygonCount_ - 1; i++)
		{
			//条件：リストが空ならば
				//添え字にて示されるリストが、
				//中身が空ならば、
				//Grassへ、ポリゴンを持たせる
				//空　＝　イテレータの最初と最後が一緒
			if (listGrassWithPaintedPolygons_[i].begin() == listGrassWithPaintedPolygons_[i].end())
			{
				(*firstGrass)->pGrass->Initialize(i + 1);
			}

		
		}

		//(*firstGrass)->pGrass->Initialize(polyNum);
	}

}

//強制的にGrassの拡張、成長を行う（１回）
void GrassLand::ForciblyGrowGass()
{
	//全要素のGrassの要素にアクセスし、
		//すべてのGrassの拡張回数を１回進ませる
	//条件：GrassLand管理の全Grass
	for (auto itr = myGrasses_.begin(); itr != myGrasses_.end(); itr++)
	{
		//成長関数を呼び込む
		(*itr)->pGrass->ExpandThePolygon();
	}
}

//指定回数拡張、成長を行わせる
void GrassLand::ForciblyGrowGass(int count)
{
	//全要素のGrassの要素にアクセスし、
	//すべてのGrassの拡張回数を１回進ませる
	for (auto itr = myGrasses_.begin(); itr != myGrasses_.end(); itr++)
	{
		for (int i = 0; i < count; i++)
		{
			//成長関数を呼び込む
			(*itr)->pGrass->ExpandThePolygon();
		}
	}

}

//コンストラクタ
GrassLand::GrassInfo::GrassInfo(Grass * pObject, int num):
	pGrass(pObject),
	polyNum(num)
{
}
