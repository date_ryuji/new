#pragma once
#include <list>		//リストを使用するためにインクルード（途中で中身を消去する可変長配列）
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
#include "../PolygonGroup/HeightMapGround.h"	//ポリゴンクラス　HeightMapGround(継承元)（地面ポリゴン形成クラス）

//ポリゴンを作る頂点数
#define V_NUM_TO_MAKE_POLYGON_ 3
	/*
		defineにて定数定義している理由

		//defineを使用している理由は
		//メンバにて、引数で配列をコピーして受け取る際に、要素を指定するが、
		//その時に、配列の要素を指定するために使う定数は、メンバでconstし、コンストラクタにて初期化しているものは使用できない
		//define定義しているものは、メンバで、要素数指定として用いることができるため、defineを使用している。

	*/


//クラスのプロトタイプ宣言
class GrassLand;
class Plane;
class BranchesMakeTree;

/*
	クラス詳細	：ポリゴンクラス　Grassクラス（HeightMapGroundに沿った、Grassクラス作成用クラス）
	クラスレベル：サブクラス（スーパークラス（PolygonGroup））
	クラス概要（詳しく）
				：HeightMapGroundに沿うGrass限定　HeightMapGroundが定義されている時限定　
				：HeightMapGroundのポリゴンクラスを継承させる。
				：Grassの大本のGrassLandからの子供クラス
				：GrassLandより、
				　GrassLandのポリゴン群を作るうちの、
				　1つの三角ポリゴンを貰い、
				　そのポリゴンから、
				　GrassLandのポリゴンの作りと同様の、ポリゴンを
				　上下左右に確保していき、
				　ポリゴンの範囲を、動的に確保していく

*/
class Grass : public HeightMapGround
{
protected:
	//頂点情報から頂点バッファーを作成
		//レベル：オーバーライド
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT CreateVertexBuffer() override;

	//インデックス情報からインデックスバッファーを作成
		//レベル：オーバーライド
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT CreateIndexBuffer() override;
	//コンスタントバッファの作成	
		//レベル：オーバーライド
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT CreateConstantBuffer() override;


	//自身のDraw
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT ThisDraw(Transform& transform);


//private 構造体
private : 

	/*
	構造体詳細	：HeightMapGroundを作るポリゴン番号をもとに　Grassが作られた際のポリゴン情報群
	構造体概要（詳しく）
				：Grassが作られた、HeightMapGroundのポリゴン　のポリゴン番号
				：Grass　の親ポリゴン
				：Grass　の自身のポリゴン番号
	*/
	struct PolygonInfo
	{
		//親のポリゴン番号
		int parentPolyNum;
		//自身の番号
		int thisPolyNum;
	};

//priavate メンバ定数
private : 

	//初期のポリゴンのポリゴン番号
	const int FIRST_POLY_NUM_;
	//範囲を広げられる上限回数
	const int MAX_EXPAND_NUM_;

	//範囲を広げる時間の間隔
	static constexpr float RANGE_EXPAND_INTERVAL_ = 5.0f;


//private　メンバ変数、ポインタ、配列
private:

	//経過時間の保存変数
		//詳細：Updateにて、フレームのデルタタイムが更新されるたびに、加算
		//　　：Updateにて、範囲が拡大実行されるたびに、初期化
	float elapsedTime_;

	//モデルハンドル番号
	//詳細：結合クラス（pBranchesMakeTree_）のモデル番号
	//　　：描画を行う際に用いる
	int hBranchesMakeTree_;

	//Grassの範囲を広げた回数
	int expandCounter_;


	//親クラス
	GrassLand* pParent_;

	//平面クラス
		//詳細：Grassは、HeightMapGroundの1マスのポリゴンの形をなぞったポリゴンとなる
		//　　：Planeクラスにて、Grassとは別のGrassをより草ポリゴンらしくするためのポリゴンクラス　
	Plane* pPlane_;

	//結合クラス
	BranchesMakeTree* pBranchesMakeTree_;


	//ポリゴン番号群
		//詳細：Grassによって拡張されたポリゴン番号（ポリゴン番号：HeightMapGroundを作るのポリゴン）
	std::vector<PolygonInfo> polygonsArray_;
	//元の頂点番号を保存しておく配列
		//詳細：親からもらった、ポリゴンを作る3頂点を順番に登録していく
		//　　：登録された順番から、（0オリジンで）
		//　　：自身のクラスにおけるポリゴンを作る、頂点の番号とする
	std::vector<int> parentVertexArray_;

	//次の範囲拡大の際に
	//範囲を広げる対象となるポリゴン番号
		//巻き戻すときも、
		//ここに登録した、番号をpolygonsArrayから探し、その構造体にて、親の番号も管理しているので、その親の番号を、代わりに、リストに追加して、
		//追加を終えたら、自身は退場とすれば、　あとは、退場に伴って、描画もしないという判定も行えれば、ポリゴンの広げた情報を残したまま、　範囲を狭めた表現が可能
	std::list<int> nextPolyTargetArray_;

//private メソッド
private : 

	//ポリゴン番号からポリゴンを作る頂点情報を取得する
		//引数：ポリゴン番号
		//引数：頂点情報のポインタ
		//引数：頂点番号のポインタ（HeightMapGroundを作る頂点の頂点番号）
		//戻値：なし
	void GetVertex(int polygonNum, HeightMapGround::VERTEX* pVertex, int* pVertexNum);


	//引数の親における頂点番号をもとに自身のクラスにおける頂点番号へと変換する
		//詳細：もともとは、親の頂点番号があり、それを登録しているので、
		//　　：その登録している頂点番号と引数の番号を比較して、自身における頂点番号へと変換。それにより、インデックス情報とする
		//引数：親の頂点番号
		//引数：自身のクラスにおける頂点番号に書き換えたものが入る配列ポインタ
		//戻値：なし
	void ChangeMyVertexNum(const int parentVertexNum[V_NUM_TO_MAKE_POLYGON_] , int* myVertexNum);

	//インデックス情報の登録
		//引数：登録インデックス
		//引数：インデックス情報の配列に書き込みを始めるスタート添え字
		//戻値：なし
	void SaveIndexes(const int myVertexNum[V_NUM_TO_MAKE_POLYGON_] , int startSuffix);

	//頂点情報の登録
		//引数：登録インデックス（全てを登録するとは限らない）
		//引数：登録するかのフラグ
		//引数：頂点情報の配列に書き込みを始めるスタート添え字
		//戻値：なし
	void SaveVertexes(const VERTEX* vertexInfo, const bool* isExists, int startSuffix);


	//加えようとしている頂点番号がすでに登録されていないかを確認
		//詳細：3頂点分調べる
		//　　：とともに、自身のクラスにおける頂点番号に変換（調べると同時にできてしまうので一緒に行ってしまう）
		//　　：すでに登録されている場合は、すでに登録されている頂点番号を入れる
		//　　：すでに登録されていない場合は、新たに頂点に追加して、追加したときの添え字番号を頂点番号とする
		//引数：親の頂点番号
		//引数：自身のクラスにおける頂点番号に書き換えたものが入る配列ポインタ
		//引数：存在していたか、（存在していなかった場合、新たに頂点を登録する必要が出てくるため）
	void CheckifExistsAndRegistrationMyVertexNum(const int parentVertexNum[V_NUM_TO_MAKE_POLYGON_],
		int* myVertexNum , bool* isExists);

	//元の頂点番号を保存しておく配列へ追加（parentVertexArray_）
		//引数：登録番号
		//戻値：なし
	void AddParentVertexNum(int vertexNum);
	//元の頂点番号を保存しておく配列から削除（parentVertexArray_）
		//引数：解除番号
		//戻値：なし
	void RemoveParentVertexNum(int vertexNum);

	//2つの値が同様の値かを調べる
		//引数：同じかの結果を入れるフラグ（入ってきたフラグが、Trueならば、そもそも何もせずに帰す）
		//引数：値１
		//引数：値２
		//戻値：値1と値2の値が等しいという処理が通った（入ってきたフラグがTrueで値が返ったのではなく、）
	bool CheckSameValue(bool* isSame, int v1, int v2);

	//引数にてもらったポリゴン番号の親のポリゴン番号を返す
		//引数：ポリゴン番号
		//戻値：追加ポリゴンの親ポリゴン
	int GetParentPolyNum(int thisPolyNum);

	//自身が所有しているポリゴン群ですでに広げ済みであるかの確認
		//引数：ポリゴン番号
		//戻値：存在するか（存在する：true,存在しない：false）
	bool IsExistsPolyNum(int thisPolyNum);


//public メンバ変数、ポインタ、配列
public:
	//自身を消去するかのフラグ
	bool isKillMe_;


//public メソッド
public:

	//コンストラクタ
		//引数：Grassを作成する基準ポリゴン
		//引数：Grassの親クラス
		//戻値：なし
	Grass(int polygonNum , GrassLand* pParent);

	//デストラクタ
		//レベル：オーバーライド
		//引数：なし　
		//戻値：なし
	~Grass() override;

	//ハイトマップ画像からモデルの作成を行う（引数のファイル名より）
		//詳細：ハイトマップ画像をよみこみ、　ハイトマップ画像が地面モデルの凹凸として使用できる
		//レベル：仮想関数
		//引数：ファイル名
		//引数：シェーダータイプ
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT Load(const std::string& FILE_NAME , SHADER_TYPE thisShader);


	//更新
		//詳細：デルタタイムを取得し、フレームの経過時間を計測
		//　　：一定間隔で、上下左右のポリゴンを増やしていく必要があるため、その更新
		//引数：デルタタイム
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT Update(float time);

	//ポリゴンクラスのポリゴンの描画を行う
			//詳細：ポリゴンクラスに登録されたポリゴン情報を引数Transform位置へ描画する
			//　　：ポリゴンは、ポリゴンの頂点情報とポリゴンを作るインデックス情報により作られている
			//　　：各ポリゴン単位で頂点情報をシェーダーの頂点シェーダーに送ることで、ポリゴン事最終的にピクセルで出力される
			//引数：描画時の変形情報（移動、拡大、回転）（ローカル座標（メソッドにて、親も考慮したワールド座標を取得可能））
			//レベル：仮想関数
			//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT Draw(Transform& transform);

	//解放
		//レベル：仮想関数
		//引数：なし
		//戻値：なし
	virtual void Release();


	//自身の死亡フラグが立っているか
		//引数：なし
		//戻値：死亡フラグが立っている（立っている：true , 立っていない：false）
	bool IsDead();


	//ポリゴンをセットする引数にてもらった、ポリゴン番号から、頂点情報の登録を行う
		//引数：ポリゴン番号
		//引数：頂点情報
		//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	HRESULT Initialize(int polygonNum);

	//範囲を広げるポリゴンのマスを広げる
		//詳細：拡張先は現在のポリゴンの上下左右ポリゴンを拡張
		//引数：なし
		//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	HRESULT ExpandThePolygon();

	//自身の原点のポリゴン番号を取得
		//詳細：コンストラクタにて取得した基準のポリゴンを渡す
		//引数：なし
		//戻値：自身の原点のポリゴン番号
	int GetFirstPolyNum();

	//縮小開始の宣言
		//詳細：拡張した範囲を縮小
		//　　：自身が属していた（あくまでも、グラフィカル、企画上で属していたものであり、プログラム上では親子関係などはない）
		//　　：あくまでも、木オブジェクトが生成されて、その設置されたポリゴンと同様のポリゴンからGrassを生成されるので、流れ的に属しているように見える
		//　　：木オブジェクトが削除されたら、動的に、自身の広げた範囲を縮小する
		//引数：なし
		//戻値：なし
	void StartShrinkingGrass();


	//範囲を拡げ済みのポリゴン番号と合計拡げ済みのポリゴン数を返す
		//引数：拡張済みポリゴン群
		//引数：合計拡張済みポリゴン数
		//戻値：なし
	void GetGrowingUpPolygonNumbers(int** finishedPolygons , int* sumPolygonNum);

};

