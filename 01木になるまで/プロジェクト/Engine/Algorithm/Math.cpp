#include "Math.h"	//ヘッダ

/*Math*/
namespace Math
{
	//引数値を1.0fと-1.0fに切り詰める関数
		//詳細：0.fを基準として1.fと−1.0fに分ける関数
		//　　：0.f以上ならtrue
		//　　：0.fよりも小さいならfalse
		//引数：切り詰める値
		//戻値：切り詰め結果（0.f以上なら：true , 0.fよりも小さいなら：false）
	bool DivideIntoPlus1AndMinus1(float value);

}

//三角形の内包判定
float Math::Det(XMVECTOR a, XMVECTOR b, XMVECTOR c)
{
	//DirectXにも、専用の関数がある
	//XMMatrixDetarminentという関数
		//だが、戻り値がベクトルであるため
		//計算式を書いてfloat型を返す計算式で対応
	

	return	(a.vecX * b.vecY * c.vecZ) +
		(a.vecY * b.vecZ * c.vecX) +
		(a.vecZ * b.vecX * c.vecY) -
		(a.vecX * b.vecZ * c.vecY) -
		(a.vecY * b.vecX * c.vecZ) -
		(a.vecZ * b.vecY * c.vecX);
}

//線分と三角形の交差判定
//レイとの衝突判定計算
	/*
		参考サイト

		//https://qiita.com/edo_m18/items/2bd885b13bd74803a368

		//https://sites.google.com/view/jc21dx11/%E3%83%AC%E3%82%A4%E3%82%AD%E3%83%A3%E3%82%B9%E3%83%88/%E7%B7%9A%E5%88%86%E3%81%A8%E4%B8%89%E8%A7%92%E5%BD%A2%E3%81%AE%E4%BA%A4%E5%B7%AE%E5%88%A4%E5%AE%9A
	*/
bool Math::Intersect(XMVECTOR origin, XMVECTOR ray, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2 , float* t , bool permitNegativeDirection)
{
	/*
		算出方法

		※前提
				・第３引数：v0 , 第４引数：v1 , 第５引数：v2 , はそれぞれ三角形を作るベクトル３つ
				　三角形を作る頂点の３次元情報と考えると考えやすい
				・衝突判定は、　edge1*u + edge2*v - ray*t = origin - v0　先の計算式で求められるとする。
				・上記の計算式は、ax + by + cz = d　先の方程式に当てはめることができる。　そのため難しく考えず、先の計算方式に当てはめれば各計算結果を簡単に求められる。
						// a = edge1
						// x = u
						// b = edge2
						// y = v
						// c = dir
						// z = t
						// d = start - v0
				・上記の計算式を使用するために、
				　各abcdのベクトルをDet() (三角形の内包)に渡すことで、各ｘｙｚの値を出すことが可能となる
				・クラメールの公式におけるｘｙｚは今回のUVTに当てはめることができる。


		�@edge1 , edge2 , ray , origin -v0 の　Det() (三角形の内包)へ渡すベクトルを作る
		�AUVTを求めるための、x成分、y成分、z成分をDet() (三角形の内包)を使い求める
		�B線分と三角形の交差の計算のため必要な　U,V,Tの値を　�Aを使用し求める
		�C衝突判定実行

	*/
	


	//�@
		
	//各edgeはベクトルの引き算で求められる　
		// (ベクトルの引き算をすれば、ベクトルからベクトルまでの差ベクトルを出せる)
	
	//edge1 (a)
		//v0 から　v1へ向かうベクトル	
	XMVECTOR edge1 = v1 - v0;
	//edge2 (b)
		//v0 から v2へ向かうベクトル
	XMVECTOR edge2 = v2 - v0;
	//ray   (c)
		//引数rayより
	//d (d)
		//v0 から　origin  へ向かうベクトル
	XMVECTOR d = origin - v0;

	//�A
	//XYZを求める際に分母となる値
	float denom = Det(edge1, edge2, -ray);

	//条件：結果が０より大きい
		//詳細：０以下の場合、三角形とレイは平行である
		//　　：平衡の場合、レイと三角形が当たらない。
		//　　：平行の場合だと、denomの値はマイナスになるらしい
	if (denom < 0)
	{
		//レイと三角形の面が平行だったら、
		//当たっていない
		return false;
	}

	//Xを求める際の分子
	float numX = Det(d, edge2, -ray);
	//Yを求める際の分子
	float numY = Det(edge1, d, -ray);
	//Zを求める際の分子
	float numZ = Det(edge1, edge2, d);

	//�B
	//Det()　（三角形の内包）の返り値による、
	//クラメールの公式に当てはめた
	//ｘｙｚ（uvt）の値を求める(分子/分母)
	//U
	float u = numX / denom;
	//V
	float v = numY / denom;
	//T
	(*t) = numZ / denom;
		/*
			衝突点までの距離
		
			上記におけるTがレイ発射位置と衝突位置までの距離となる


			//＋の場合、レイ方向にて当たっている
			//‐の場合、レイの反対方向にて当たっている

			★
			/本来は、レイの方向を活かすために、
			//Tが-になっていたら、当たっていないということにするべき
	
			※
			//だが、それでは、地面との判定の時に、坂道を上ることができない
			//そのため、レイの方向を、レイの発射位置よりも後ろの方向でも可能としたならば、OKととらえてもいいかも
			//- の場合、長さのT　＝　distには、-の値が入るので、距離が-になる。

			−方向も考慮した計算結果を出すかは、
			引数真偽値にて判別する

		*/



	//�C
	//衝突判定
	/*
		線分と三角形の交差において、
		//edge1*u + edge2*v - ray*t = origin - v0　の式を求めたが
		
		線分と三角形の交点を仮にPと置いたとき、
		Pは、
		V0からV1への線分中にあるU(ｖ０を０、ｖ１を１という割合で求めたときのU位置は少数になる)（v0からUまでのベクトル）という点、
		V0からV2への線分中にあるV（v0からVまでのベクトル）という点、

		この２つのベクトルの足し算で出したベクトルが
		Pである。

		
		U　　　P	
		↑　／
		↑／
		└→→V


		となると、UVが、三角形の辺であるならば、
		UVTという点は以下でないといけない。

		上の式からuとvとtの値を求め

		0 ≦ u ≦ 1

		0 ≦ v ≦ 1

		0 ≦ u+v ≦ 1

		t  ≧ 0

		となっていれば、線分と三角形はぶつかってる（Pは三角形の内部にある）と言える。
		（写真メモを確認、１を超えたときに衝突しない）

	*/

	/*
		//必要なくなる条件の削除
			//上記の真偽を行うために、本来であれば、
			// u >= 0 && u <= 1 && v >= 0 && v <= 1 && u+v <= 0 && u+v >= 1 && t >= 0
			//上記のように判別式を複数記述するようになる。


		//※なくしても絶対に、誤作動は起きないとわかっているうえで
			//＆＆は、上から順番に条件が流れていくので、（見ていくので、）
		//※上から順番でなくても、＆＆になるので、どこから見ても結果は同じになる。
			//順番は関係ない。（一個でもfalseなら＆＆なら通らない。）


		↓　上記を考慮し、判別式を減らすことができる
	*/
	
	//条件：
	//�@U、Vが０以上であれば、（ U >= 0 (YES), V >= 0 (YES)ときたら）
		//U＋Vが０より小さくなることはない。（(U+V) >= 0 は必ずYESとなるのはわかっている）
	//�AU＋Vが１より大きいという条件があるなら
		//U、Vが１より小さいという条件いらない。（→Uが１よりおおきかったら、　U+Vの条件false, Vが１より大きかったら、　U+Vの条件false
												//であれば、どちらにしても、Uが１より小さいと聞くのは、意味がない）
	if (u >= 0 && 
		v >= 0 && 
		(u + v) <= 1 )
	{
		//レイの+方向、-方向判別
			//0以上の場合、レイが＋方向になる
			//0未満の場合、レイが‐方向になる
				//レイの方向を指定しない場合、以下の処理を省くことも可能
				//レイの方向を指定しないと、レイ方向を指定する意味がなくなる。レイの発射方向と逆の方向でも衝突してしまう
				//だが、場合によっては、レイの衝突方向を指定しない場合もある。
					//レイの方向指定が必要な場合（レイ方向＋方向のみ）　：発射位置と発射方向からのレイとオブジェクトが正確に当たっているか判別する時（←大半は左記の衝突判定が標準である）
					//レイの方向指定が不要な場合（レイ方向＋ー方向有効）：地面とのレイキャスト　→地面ポリゴンとのレイキャストを行う際に、プレイヤーが埋まった場合、プレイヤーが埋まった地点から地面へのレイを飛ばす場合、衝突しなくなるので、その場合、方向が逆の場合も考慮したい。
					//　　　　　　　　　　　　　　　　　　　　　　　　　　であれば、レイが必ず衝突する位置からレイを飛ばせばよいのでは？と考えるが、。であれば、レイの方向を＋方向も―方向も可能であるように指定しておけば済む話
		//条件：マイナス方向を考慮する（引数のマイナス方向のレイ方向も衝突判定計算に含むかのフラグ）
		if (permitNegativeDirection)
		{
			//衝突した
			return true;
		}
		//条件：マイナス方向は考慮せず、
		//　　：かつ、＋方向である場合
		else if ((*t) >= 0)
		{
			//衝突した
			return true;
		}
	}

	//衝突しなかった
	return false;
}

//2つのベクトルのなす角を求める（180度以内）
float Math::GetAngleWithin180Degree(XMVECTOR vec1, XMVECTOR vec2)
{
	//ベクトルの正規化
	vec1 = XMVector3Normalize(vec1);
	vec2 = XMVector3Normalize(vec2);


	//2つのベクトルの内積を求める
	//★”内積を求める（２つのベクトルによる、一番近い内積（小さい角度のほうの内積））”
		//★＝　180度以内の方の角度が出される
	float dot = XMVector3Dot(vec1, vec2).vecX;

	//２つのベクトルのなす角
		//結果　＝　ラジアン
		//度に直す(計算の際に、最終的にラジアンに変化させるが、でバック時にわかりやすくするために、度にする)
		//float angle = acos(dot) * (180.f / 3.14f);	//計算式
	float angle = XMConvertToDegrees((float)acos((double)dot));

	//度を返す
	return angle;
}

//2つのベクトルのなす角を求める（360度以内）
float Math::GetAngleWithin360Degree(XMVECTOR vec1, XMVECTOR vec2 , NORMAL_DIR dirType)
{
	//ベクトルの正規化
	vec1 = XMVector3Normalize(vec1);
	vec2 = XMVector3Normalize(vec2);

	//2つのベクトルの内積を求める
	//★”内積を求める（２つのベクトルによる、一番近い内積（小さい角度のほうの内積））”
		//★＝　180度以内の方の角度が出される
	float dot = XMVector3Dot(vec1, vec2).vecX;

	//２つのベクトルのなす角
		//結果　＝　ラジアン
		//度に直す(計算の際に、最終的にラジアンに変化させるが、でバック時にわかりやすくするために、度にする)
	//float angle = acos(dot) * (180.f / 3.14f);	//計算式
	float angle = XMConvertToDegrees((float)acos((double)dot));


	//外積を求める

	//外積によって、
	//2つのベクトルのZ成分が＋か、ーかで、回転方向を求める
	//ベクトル1、ベクトル２にて、　 ベクトル１からベクトル2に行くのに、左回転であれば（反時計回り）、法線方向　は + となる。
	//								ベクトル１からベクトル2に行くのに、右回転であれば（　時計回り）、法線方向　は - となる。
	// 
	// 360度回転によって、　正の回転方向は　右回転（　時計回り）
	//						負の回転方向は　左回転（反時計回り）
	//ベクトルの外積
	XMVECTOR cross = XMVector3Cross(vec1, vec2);
	
		//外積において取得できるのは、
			//その2つのベクトルの法線を求めることができる
			//つまり、ｘｙ成分が違えば、Ｚ成分が法線となる。
			//つまり、ｘｚ成分が違えば、ｙ成分が法線となる。
			//今回においては、ｘｚ成分を動かしているため、Ｙ方向の値を持って、回転方向を示す符号とする

	//法線方向によって、
	//外積の値を求める
		//その外積の値によって、　360度の角度を求める
	bool code;

	//法線方向を指定
	switch (dirType)
	{
	case NORMAL_DIR::NORMAL_DIR_X :
		code = DivideIntoPlus1AndMinus1(cross.vecX);
		break;
	case NORMAL_DIR::NORMAL_DIR_Y:
		code = DivideIntoPlus1AndMinus1(cross.vecY);
		break;
	case NORMAL_DIR::NORMAL_DIR_Z:
		code = DivideIntoPlus1AndMinus1(cross.vecZ);
		break;
	default : 
		code = false;
		break;
	
	}

	//最終結果の角度を求める
	//外積の法線方向が
		//条件：＋だったら、（Trueだったら、）360 - angleにて、180度以降の角度を求める
	if (code)
	{
		return 360 - angle;
	}
		//条件：- だったら、（Falseだったら、）angleにて、180度前の角度を求める
	else
	{
		return angle;
	}

	//求められなかったら
	return 0.0f;
}

//引数値を1.0fと-1.0fに切り詰める関数
bool Math::DivideIntoPlus1AndMinus1(float value)
{
	//0.fを基準に
	//0.f以上だったら		true		
	//0.fより小さかったら　	false
	//を返す


	if (value < 0.f)
	{
		//0より小さいときは、
		//falseを返す
		return false;
	}
	else
	{
		//0より大きいときは、
		//tureを返す
		return true;
	}
	

	return false;
}
