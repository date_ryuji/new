﻿#include <chrono>		//経過時間取得のためのインクルード
#include "Timer.h"		//ヘッダ

//ライブラリのリンク
#pragma comment(lib, "winmm.lib")

/*Timer*/
namespace Timer 
{
	//デルタタイム
	static auto deltaTime = std::chrono::microseconds(0);
	//経過時間
	static auto	elapsedTime = std::chrono::microseconds(0);	
	//マイクロ(10の6乗)
	static float microDiv = 1000000.f;	

};


//シーンがスタートしてからの経過時間(sec)
float	Timer::GetElapsedSecounds()
{
	//経過時間をマイクロ秒から秒に変換してから返す
	return elapsedTime.count() / microDiv;	
}

//フレームのデルタタイム(sec)
float	Timer::GetDelta()
{
	//デルタタイムをマイクロ秒から秒に変換してから返す
	return deltaTime.count() / microDiv;	
}

//シーンタイマーのリセット
void	Timer::Reset()
{
	//各変数の初期化
	elapsedTime = std::chrono::microseconds(0);
	deltaTime = std::chrono::microseconds(0);
}

//デルタタイムの更新
void	Timer::UpdateFrameDelta()
{
	// 前回のマイクロ秒
	static auto prevTime = std::chrono::system_clock::now();	

	// 現在のマイクロ秒
	auto now = std::chrono::system_clock::now();				

	//現在と前回の差
	deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(now - prevTime);	

	//現在の時間を保持する
	prevTime = now;	

	//デルタタイムを加算
	elapsedTime += deltaTime;	
}

