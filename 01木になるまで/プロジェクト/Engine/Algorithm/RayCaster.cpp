#include "RayCaster.h"				//ヘッダ
#include "../DirectX/Input.h"		//デバイス入力クラス
#include "../DirectX/Camera.h"		//カメラクラス


//コンストラクタ
RayCaster::RayCaster()
{
}
//デストラクタ
RayCaster::~RayCaster()
{
}

//マウス座標値から世界の始点、終端までの2つのワールド座標ベクトルを取得する関数
void RayCaster::GetWorldMouseCursorPos(XMVECTOR * mousePosFront, XMVECTOR * mousePosBack)
{
	/*
		�@カメラの見る視点。　　　　その描画位置の始点をmousePosFrontとする（マウス座標値から世界の始点）。
		　カメラから見れる視点限界。その描画位置の終点をmousePosBackとする（マウス座標値から世界の終点）。
		�Aスクリーン座標から、プロジェクション行列に変換するためのビューポート行列の作成
		�Bスクリーン→プロジェクション　と本来の座標変換と逆であるため、逆行列とする。
		　ビューポート、プロジェクション、ビュー行列をそれぞれ逆行列へ
		�C�Bの行列を各�@のベクトルにかけることで、ワールド座標へ変換する

	*/

	//現在のマウスカーソル位置を取得
	XMVECTOR mouseCursorPos = Input::GetMouseCursorPosition();


	//�@
	(*mousePosFront) = mouseCursorPos;
	mousePosFront->vecZ = 0;
	(*mousePosBack) = mouseCursorPos;
	mousePosBack->vecZ = 1;


	//�A
	//ワールド座標に変換するための
	//逆行列の用意

	//２次元から、３次元へ直すための行列を取得する。
	//ビューポート行列
	//ビューポート行列は、これまで、Direct３Dに作ってもらっていたため、
	//どこにも存在していない状態なので、作成する。
	float w = Direct3D::scrWidth / 2.0f;
	float h = Direct3D::scrHeight / 2.0f;
	XMMATRIX vp = {
		w, 0, 0, 0,
		0, -h, 0, 0,
		0, 0, 1, 0,
		w, h, 0, 1
	};

	//�B
	//逆行列を作成
	//ビューポート行列の逆行列
	XMMATRIX invVp = XMMatrixInverse(nullptr, vp);
	//プロジェクション行列の逆行列
	XMMATRIX invPro = XMMatrixInverse(nullptr, Camera::GetProjectionMatrix());
	//ビュー行列の逆行列
	XMMATRIX invView = XMMatrixInverse(nullptr, Camera::GetViewMatrix());


	//�C
	//３つの逆行列で変換することで、
	//スクリーン(*ビューポートの逆行列)→プロジェクション(*プロジェクションの逆行列)→ビュー(*ビューの逆行列)→ワールド
	//ワールドの座標とする。
	(*mousePosFront) = XMVector3TransformCoord(
		(*mousePosFront),
		invVp * invPro * invView
	);

	(*mousePosBack) = XMVector3TransformCoord(
		(*mousePosBack),
		invVp * invPro * invView
	);
}


//RayCast実行
bool RayCaster::RayCastToStartFromTheMouseCursor(const int targetModel, RayCastData* pData)
{
	/*
		レイキャスト方法�@
		//　　：マウスカーソル位置を３D空間上の座標に変換　その座標から世界の端までのレイを飛ばす
		//　　：その道中で、ターゲットと衝突しているか
		//　　：マウスカーソル位置の３D空間上への変換方法（マウスカーソル位置のスクリーン座標を逆行列を使い->スクリーン->プロジェクション->ビューポート->ビュー->ワールド　に変換）
	*/



	//マウスカーソル位置から、
	//プロジェクション座標で考えた時の、�@世界の端（カメラ描画の一番手前）から、�A世界の端（カメラ描画の一番奥）までのそれぞれの
	//奥行VecZを決めた、ベクトルを取得
		//→この座標を、最終的に、ワールド座標に変換する
		//→世界の端（手前）におけるワールド座標と、世界の端（奥）におけるワールド座標が出るので、その間に、対象がいたら、衝突しているとなる

	//�@と�Aのベクトルのワールド座標の取得
	//�@
	XMVECTOR mousePosFront;
	//�A
	XMVECTOR mousePosBack;

	//取得
	GetWorldMouseCursorPos(&mousePosFront, &mousePosBack);



	//ワールドにおける座標が出せたので、
	//レイの方向を、
	//マウス開始位置から終了位置までに延ばす。
	//�@世界の端（カメラ描画の一番手前）から、�A世界の端（カメラ描画の一番奥）まで伸びるレイができる
	XMVECTOR rayDir = mousePosBack - mousePosFront;
	//正規化
		//方向を取得
	rayDir = XMVector3Normalize(rayDir);



	//情報のセット
	//スタート位置
		//�@世界の端（カメラ描画の一番手前）
	pData->start = mousePosFront;	
	//方向
		//�@と�Aの差ベクトル
	pData->dir = rayDir;	


	//レイキャストの実行
	Model::RayCast(targetModel, pData);


	//衝突したか
	return pData->hit;
}

//RayCast実行
bool RayCaster::RayCastToStartFromTheAnyPos(const int targetModel, RayCastData * pData)
{
	/*
	レイキャスト方法�A
		//　　：指定オブジェクトなどから、指定方向へレイを飛ばす
		//　　：その道中で、ターゲットと衝突しているか
	*/

	//レイの方向を入れてもらうので
	//それをそのまま実行するだけ
	Model::RayCast(targetModel, pData);

	//衝突したか
	return pData->hit;
}

//RayCast実行
bool RayCaster::RayCastToVectorDirection(const int targetModel, XMVECTOR & start, XMVECTOR & end)
{
	/*
		レイキャスト方法�B
		//　　：引数で渡された2つのベクトルの始点から終点方向にオブジェクトが存在するか
		//　　：�AでRayCastDataにデータを格納するところを、関数先でやってもらう
	*/

	RayCastData pData;
	pData.start = start;
	pData.dir = XMVector3Normalize(end - start);
	//レイのマイナス方向を考慮するかのフラグ
		//始点から終点の間に衝突するかの衝突判定であるため、マイナス方向を考慮してしてしまうと、点から点への間につなぐ線と衝突しているかの判定を取ることができない
	pData.permitNegativeDirection = FALSE;

	//レイキャスト実行�A実行
	RayCastToStartFromTheAnyPos(targetModel, &pData);

	//衝突したか
	//レイキャスト実行結果を返す
	return pData.hit;
}

//RayCast実行
bool RayCaster::RayCastToStartFromEnd(const int targetModel, XMVECTOR& start, XMVECTOR& end)
{
	/*
		レイキャスト方法�C
		//　　：引数で渡された2つのベクトルの間にターゲットが存在するか、衝突するか
		//　　：�AでRayCastDataにデータを格納するところを、関数先でやってもらう
	*/

	RayCastData pData;
	pData.start = start;
	pData.dir = XMVector3Normalize(end - start);
	//レイのマイナス方向を考慮するかのフラグ
		//始点から終点の間に衝突するかの衝突判定であるため、マイナス方向を考慮してしてしまうと、点から点への間につなぐ線と衝突しているかの判定を取ることができない
	pData.permitNegativeDirection = FALSE;

	//レイキャスト実行�A実行
	RayCastToStartFromTheAnyPos(targetModel, &pData);

	//仮に衝突をしたとしても、その衝突結果は、2点をつなぐ線の進行方向中に衝突するのであれば、衝突TRUEと返されてしまう
	//上記を踏まえて、衝突判定結果後、その開始点から衝突点までの距離が　第二引数、第三引数の2点間の距離より長い場合衝突していないと判断する
	start.vecW = 0.f;
	end.vecW = 0.f;

	float length = XMVector3Length(end - start).vecX;

	//衝突している
	//&&
	//衝突距離が始点から終点の長さ以下である
	return (pData.hit) && (pData.dist <= length);
}
