#include <Math.h>							//計算アルゴリズム（平方根使用のため）
#include <cmath>							//計算アルゴリズム
#include "AStarAlgorithm.h"					//ヘッダ
#include "../Csv/CsvReader.h"				//CSVReader
#include "../../CommonData/GlobalMacro.h"	//共通マクロ

//コンストラクタ
	//
AStarAlgorithm::AStarAlgorithm(const std::string & FILE_NAME, Cell start , Cell goal):
	width_(0),
	height_(0),
	pStartCell_(nullptr),
	pGoalCell_(nullptr)
{
	//マスごとのコストを記述したCSVデータの読み込み
	pCsvReader_ = new CsvReader;
	pCsvReader_->Load(FILE_NAME);

	//マスの横幅、縦幅の入手
	width_ = (int)pCsvReader_->GetWidth();
	height_ = (int)pCsvReader_->GetHeight();

	//セルの持っているコストの更新
	InitCellCost(pCsvReader_);

	//ノードの座標の確保と、隣接ノードの設定
		//隣接ノードは、マス外と。非対称マス（-1がコストになっているマス）を省いたものとなるようにしている
	CreateGraph();

	//Nodeのコストの初期化
	InitHeuristicAndTotalCost();

	//ノードの初期化が終了したのち
		//スタートセルとゴールセルのCellを登録
	SetStartCellAndGoalCell(start, goal);

	//AStartAlgorithmの実行
	Exe();

}

//コンストラクタ
AStarAlgorithm::AStarAlgorithm(const int WIDTH, const int HEIGHT):
	width_(WIDTH) ,
	height_(HEIGHT),

	pStartCell_(nullptr),
	pGoalCell_(nullptr)

{
	//セルの初期化
	CreateCellCost();

	//コストをすべて １で更新
	SetAllCellCostData(1);

	//ノードの座標の確保と、隣接ノードの設定
	//隣接ノードは、マス外と。非対称マス（-1がコストになっているマス）を省いたものとなるようにしている
	CreateGraph();

	//Nodeのコストの初期化
	InitHeuristicAndTotalCost();

}

//デストラクタ
AStarAlgorithm::~AStarAlgorithm()
{
	//条件：マス高さ分回す
	for (int i = 0; i < height_; i++)
	{

		//複数個動的確保したので、
		//配列ポインタの解放

		//ポインタの中身の解放
		//Width_分の動的確保の解放
		SAFE_DELETE_ARRAY(pGraph_[i]);
		SAFE_DELETE_ARRAY(pCosts_[i]);
	}
	//ポインタの側の解放
	//Height_分の動的確保の解放
	SAFE_DELETE_ARRAY(pGraph_);
	SAFE_DELETE_ARRAY(pCosts_);

	SAFE_DELETE(pCsvReader_);
}

//マス（セル）ごとのコストを格納する２次元配列の作成、初期化
void AStarAlgorithm::CreateCellCost()
{
	//コストを登録する2次元配列の初期化
	pCosts_ = new int*[height_];


	//GetValue(x,y)
	//条件：マス高さ分回す
	for (int y = 0; y < height_; y++)
	{
		//行の一次元配列の確保
		pCosts_[y] = new int[width_];
	}

}

//スタート、ゴールセルのノードをメンバに登録する
void AStarAlgorithm::SetStartCellAndGoalCell(Cell startCell, Cell goalCell)
{
	//登録するセル値は
	//Nodeから取得するアドレスからの値でなくてはならない

	//ノードの初期化が終了したのち
	//スタートセルとゴールセルのCellを登録

	//ゴールノードのCell値を保存
	pGoalCell_ = &pGraph_[goalCell.y_][goalCell.x_].Position;
	//スタートノードCell値を保存
	pStartCell_ = &pGraph_[startCell.y_][startCell.x_].Position;

}

//ノードごとのコスト（pCosts_）の初期化
void AStarAlgorithm::SetAllCellCostData(int setValue)
{
	//条件：マス高さ分回す
	for (int y = 0; y < height_; y++)
	{
		//条件：マス幅分回す
		for (int x = 0; x < width_; x++)
		{
			//引数値にてコストを初期化
			pCosts_[y][x] = setValue;
		}
	}
}

//ノードごとのコスト（pCosts_）の更新
void AStarAlgorithm::UpdateCell(Cell cell, int updatingCost)
{
	//指定セルからコストを更新
	pCosts_[cell.y_][cell.x_] = updatingCost;
}



//アルゴリズム実行
	//A＊は、すべてのセルを網羅するわけではない。
	//→大体の目安（フューリスティックコスト）をつけて、　ゴールに向かう線形探索を行い、経路探索による計算量を少なくする
void AStarAlgorithm::Exe()
{
	//アルゴリズム実行関数に送る
		//リストの定義
		//引数なしが呼ばれた場合、
		//意味がなくなるが、
		//無駄なリストを引数として送って、実行の関数自体は一つの関数を共通して処理をさせるようにする
	std::list<Cell*> nextRoute;

	//AStartAlgorithm実行呼び出し
	Exe(&nextRoute);

}

//AStar法のアルゴリズム実行（経路探索）（結果受け取り）
void AStarAlgorithm::Exe(std::list<Cell*>* nextRoute)
{


	//各コストの初期化
	InitHeuristicAndTotalCost();

	//クローズノード群
		//探索終了したノード
	list<Node*> closeList;
	closeList.clear();	//リストの初期化

	//オープンノード群
		//探索対象ノード 
	list<Node*> openList;
	openList.clear();	//リストの初期化
	
	//スタートセルの初期化
		//ヒューリスティクスコストの初期化
		//合計コストの初期化
	pGraph_[pStartCell_->y_][pStartCell_->x_].HeuristicCost = 0;
	pGraph_[pStartCell_->y_][pStartCell_->x_].TotalCost = 0;


	//スタートノードを探索対象ノード群（openList）へ追加
		//セル要素をアドレスで受け取り、　コピーではない
	openList.push_back(&pGraph_[pStartCell_->y_][pStartCell_->x_]);


	//ゴールノード
	Node* goalNode = &pGraph_[pGoalCell_->y_][pGoalCell_->x_];
	//スタートノード
	Node* startNode = &pGraph_[pStartCell_->y_][pStartCell_->x_];

	//★ポインタのポインタのポインタ
		// ポインタのポインタをheight分持った
			//ポインタのポインタの中に、ポインタをwidth分持てる枠を持った。
			//上記により、NodeのCellにアクセスするアドレスを持てるようになった。		
	//最終更新ノード位置保存用
		//トータルコストを更新した際にノードの情報を更新したノードの位置にする（更新したら確定ではないので注意）
	Cell*** lastUpdateCells = new Cell**[height_];
	//条件：マス高さ分回す
	for (int i = 0; i < height_; i++)
	{
		//幅分　動的確保
		lastUpdateCells[i] = new Cell*[width_];
	}

	int roopCounter = 0;


/*スタートからゴールまでの最短経路をCell値で確保********************************************************************************************************************************/
	//条件：オープンリストがからでないならば
	//オープンリストの中身がなくなるまで、進める
		//カラではない場合、以下の処理を繰り返す
	while (openList.empty() == false)
	{

		//�@オープンのリストから
			//経路探索をするノードを一つ取得
			//この時に、ノードは最もトータルコストが低いノードを取得
		//ここで、調べるべき方向を限定する
			//→これで、トータルコストが多いであろう方向を視野に入れずに。探索を行う。
			//結果、計算量の削減につながる


		//ノードで見ているトータルコストは
			//→ノード間のコスト　＋　親ノードからの継承合計コスト	
		//そのため最小コストのものから見ていくときに、
			//ヒューリスティックコスト（そのセルからゴール距離までの直線距離）を加味した最小コストにすべき。（合計コストは小さいが、ヒューリスティックコストを加味すると最小ではなくなることも、、逆に、ヒューリスティクスコストが小さいから最小コストになることも。）
		int minCost = (int)COST_MAX_;
		auto it = openList.begin();


		//条件：オープンノードのイテレータの最後の要素でない
		//オープンノードから最小コストのノードを探す
		//探索しなくてはいけないノードリストから
			//直線距離を含めた、最小のコストを持つノードを求め、それを次の探索者とする。
		for (auto itr = openList.begin();
			itr != openList.end(); itr++)
		{
			//直線距離と、ノードの合計コスト（各ノード間のコストを算出して、スタートからそのノードまでにかかったコスト）
				//処理回数は多い、
				//だが、正確に、ルートを算出している。
				//だが、明らかに遠回りの方向にも探索を掛けているので、
					//非効率になっている（だが、ダイクストラ法は（私の求め方が非効率であった可能性もあるが、）２７００回ほどの計算をしていたので、それに比べれば、圧倒的に早い）
			//そのマスの合計コスト（ノード間のコスト＋親のノードの合計コスト）　＋　ヒューリスティクスコスト
			float currentCost = ((*itr)->TotalCost * 1.0f) + (*itr)->HeuristicCost;


			//直線距離が一番小さい人を次の探索者とする
			//計算時間は確実に早い、
				//だが、ゴールまでのコストのルートが最小ではない。
				//各ノード間のコストも最小になっていないし、、
			//float currentCost = (*itr)->HeuristicCost;

			//条件：0じゃない（スタートじゃない）
			//　　：&&
			//　　：そのマスのコストが最小コストよりも小さい
			if ((*itr)->TotalCost != 0 &&
				currentCost < minCost)
			{
				//最小のリスト用をのイテレータを更新
				it = itr;

				//最小コストの更新
				minCost = (int)currentCost;

			}

		}


		Node* searchNode = (*it);
		//取得したノードを探索リストから削除
		openList.erase(it);



		//条件：探索対象がゴールのノードであるなら終了
		if (IsEqualCell(searchNode->Position, goalNode->Position))
		{
			// クローズリストに最後のノードを追加する
			closeList.push_back(searchNode);
			break;	//繰り返しから終了
		}


		//�A最小のコストの四方（上下左右のノードを参照）のノードから移動可能な最小コストを持つノードを見つける
			//Nodeの構造体のメンバである、
			//隣接ノードを登録しているVector
				//これには、現在見ているノードの隣接のノードのアドレスが入っている
				//ノードの初期化の段階ですでに、登録済みである。
				//そのノードの一つ一つを参照する
			//searchNodeに入っているNodeのポインタを一つずつ取得し、
				//adjacent_nodeに代入して回す
				//foreachのような回し方

			//searchNodeに入っているのは、Grapthのアドレス
		//条件：探索対象ノードの隣接ノードがある場合
		for (Node* adjacent_node : searchNode->NeighborEdges)
		{
			//条件：コストが計算されていなければ
			//　　：＝初期値ならば
			if (adjacent_node->HeuristicCost == COST_MAX_)
			{
				//ヒューリスティクスコスト計算
					//現在見ているノードと、ゴールまでの直線距離の算出
					//計算が終わったヒューリスティクスコストをトータルコスト計算に使用する
				adjacent_node->HeuristicCost =
					CalculateHeuristic(adjacent_node, goalNode);
				
				//ノード間コスト
					//セルからそのセルのコストをもらう
					//float edgeCost = pCsvReader_->GetValueInt(adjacent_node->Position.x_, adjacent_node->Position.y_);
				float edgeCost = (float)GetCellCost(adjacent_node->Position);

				//取得ノードのトータルコスト
				float totalCost = searchNode->TotalCost;

				//修正前
				/*
					トータルコスト算出　＝
					ノード間コスト + ヒューリスティックコスト + （親のノードの）トータルコスト

					修正理由
						＝ヒューリスティックコストは、次に距離を求めるノードを決めるときに、
							ノード間コスト + ヒューリスティックコスト + （親のノードの）トータルコストが一番小さいノードを次の探索者とする。

				*/
				//修正後
				/*
					トータルコスト算出　＝
					ノード間コスト　＋　（親のノードの）トータルコスト

				*/
				float cost = edgeCost + totalCost;



				//ノードの追加
				//算出したコストから、
				//オープンリストに追加できるかを確認する
					//進むべきセル値かを確認する
					//探索済みのノードに自身と同じノードがあるなら、そいつとの、コストの比較
				//条件：追加されたか
				if (AddAdjacentNode(openList, closeList,
					adjacent_node, cost))
				{
					//オープンリストに追加されたので
					//トータルコストの更新
					adjacent_node->TotalCost = cost;

					//トータルコストを更新した、
					//経路を更新したセルを保存（最終更新せる保存用配列に）
						//★保存先はポインタなので、Nodeにて示される、Cellのアドレスを渡す
						//★アクセス先は、常にNodeのCell　つまり、Cellごとにメモリを消費しない
					lastUpdateCells[adjacent_node->Position.y_]
						[adjacent_node->Position.x_] =
						
						&searchNode->Position;
				}

			}

			//ループカウンター（経路探索の計算回数カウント）
			roopCounter++;
		}

		//このノードの探索終了
			//クローズノード群へ追加
		closeList.push_back(searchNode);

	}

/*引数リストへ、経路を保存********************************************************************************************************************************/
	//最終更新セル保存用配列から
	//ゴールセルを逆にたどっていけば、ゴールからスタートまでのルート（最小コストのルートが出る）

	// 経路復元
		//探索終了を受けて、探索終了結果を引数リストへ登録する 
		//★NodeのCellのアドレスを受け取るので、
		//★Cellのポインタ
	std::list<Cell*> route_list;

	// ゴールセルから復元する
		//ゴールセルから、スタートセルへ、登録セルをたどって、道を引く
	route_list.push_back(pGoalCell_);
	//スコープ：�@
	//条件：空でない
	while (route_list.empty() == false)
	{
		//リストの頭から要素を抜く
			//抜いた要素は、実際に、リストから消える
		Cell* route = route_list.front();

		//スコープ：�A
		//条件：スタート位置なら
		if (IsEqualCell((*route), startNode->Position) == true)
		{

			//スコープ：�B
			//条件：ルートリストある場合
			// 復元した経路を表示
			//保存された経路をすべて、引数nextRouteに登録
				//route_listに存在する要素を一つ一つcellに代入し、要素がなくなるまで繰り返す
			for (Cell* cell : route_list)
			{
				//引数の次の移動先セルのリストへ更新
					//route_listには、先頭から、ゴールに近い順で登録されている、そのため、
					
					//nextRouteには、ゴールはリスト最後になるようにしたいので、
					//リスト頭→リスト尾は、スタート→ゴールの順番で並べる
					//つまり　push_backにて登録

				nextRoute->push_back(cell);


				//printf("x = %d y = %d\n", cell->x_, cell->y_);
				//printf("コスト%4.0f\n", pGraph_[cell->y_][cell->x_].TotalCost);
			}
			//処理を抜ける
				//スコープ：�@
			break;
		}
		//条件：スタート位置でないなら
		else
		{
			//スコープ：�B
			//route_listへ一つずつ、登録する
			//条件：セル値がマスの範囲内なら
			if (IsCellWithinTheRange(route->x_, route->y_) == true)
			{
				// 追加
				route_list.push_front(lastUpdateCells[route->y_][route->x_]);
			}
			else
			{
				//printf("経路は見つからなかった\n");
				//処理を抜ける
					//スコープ：�@
				break;
			}
		}
	}

/**********************************************************************************************************************/
	//解放
	//条件：マス高さ分回す
	for (int i = 0; i < height_; i++)
	{
		//lastUpdateCells[i] = new Cell * [width_];
		//の解放（中身のポインタの解放）
		SAFE_DELETE_ARRAY(lastUpdateCells[i]);
	}
	//Cell*** lastUpdateCells = new Cell**[height_];
	//の解放（側の動的確保の解放）
	SAFE_DELETE_ARRAY(lastUpdateCells);

}

//AStar法のアルゴリズム実行（経路探索）（結果受け取り、スタート位置、ゴール位置変更）
void AStarAlgorithm::Exe(std::list<Cell*>* nextRoute, Cell start, Cell goal)
{
	//スタートとゴールの更新
	SetStartCellAndGoalCell(start, goal);

	//実行
	Exe(nextRoute);
}

//グラフ（pGraph_）の作成
void AStarAlgorithm::CreateGraph()
{
	//要素の動的確保
		//マス行確保
	pGraph_ = new Node*[height_];	
	//条件：マス高さ分回す
	for (int i = 0; i < height_; i++)
	{
		//列確保
		pGraph_[i] = new Node[width_];	
	}


	// ノードの作成
	//条件：マス高さ分回す
		for (int y = 0; y < height_; y++)
		{
			//条件：マス幅分回す
			for (int x = 0; x < width_; x++)
			{
				//マス目位置格納
				//０オリジン
				pGraph_[y][x].Position.x_ = x;
				pGraph_[y][x].Position.y_ = y;

				//隣接ノードの設定構造体宣言
				//現在ののｘｙからの隣接
					//上下左右のセル番地の設定
				Cell adjacent_cells[] =
				{
					Cell(x, y - 1),
					Cell(x - 1, y),
					Cell(x + 1, y),
					Cell(x, y + 1),
				};

				//隣接ノードの追加
				//隣接ノードとして、範囲外でない場合隣接として追加
				//条件：隣接ノード分回す　（cellという変数で、構造体の要素をひとつ、一つ回す）
				for (const Cell& cell : adjacent_cells)
				{
					//条件：範囲内であるか
					//　　：&&
					//　　：そこが壁などの移動可能セルか（通路ならtrue　、壁ならfalse）
					if (IsCellWithinTheRange(cell.x_, cell.y_) == true 
						&&
						IsCellCostNotError(cell.x_ , cell.y_) == true )
					{
						
						//現在のノードに隣接するノードとして、隣接ノード群（可変長配列）に登録（アドレスを受け取り）
						pGraph_[y][x].NeighborEdges.push_back(&pGraph_[cell.y_][cell.x_]);
					}
				}
			}
		}

}

//そのセルが
//通路か、壁か
	//隣接ノード（セル）として追加してはいけないコスト（ノード間のコスト）を示しているとき
	//falseを返す
bool AStarAlgorithm::IsCellCostNotError(int x, int y)
{
	//int moveCost = pCsvReader_->GetValueInt(x, y);
	//Csvから読み込むのではなく、
		//クラスに登録した、
		//intの2次元配列から取得する
	//取得できるように
		//xy値を、Cell構造体に代入して、そのCellを送る

	int moveCost = GetCellCost(Cell(x , y));

	//条件：そこが壁
	//　　：（移動コスト
	//　　：　ノード間のコスト　が−１ならエラー）
	if (moveCost == -1)
	{
		//壁である
		return false;
	}

	//通路である
	return true;
}

//引数要素が、グラフ(pGraph_)の範囲内であるかの判定
bool AStarAlgorithm::IsCellWithinTheRange(int x, int y)
{
	//引数のｘ、ｙにより示されるセル値が
	//グラフ（CSVよみとり）の範囲名であるかの判定
	//条件：xが０以上
	//　　：&&
	//　　：xがWidthより小さい
	if (x >= 0 && x < width_)
	{
		//条件：yが０以上
		//　　：&&
		//　　：yがHeightより小さい
		if (y >= 0 && y < height_)
		{
			//範囲内
			return true;
		}
	}
	
	//範囲外
	return false;

}

//Nodeのコストの初期化
void AStarAlgorithm::InitHeuristicAndTotalCost()
{
	//条件：マス高さ分回す
	for (int y = 0; y < height_; y++)
	{
		//条件：マス幅分回す
		for (int x = 0; x < width_; x++)
		{
			pGraph_[y][x].HeuristicCost = COST_MAX_;
			pGraph_[y][x].TotalCost = COST_MAX_;
		}
	}
}

//セルの持つコストの初期化
//CSVよりコストを取得
void AStarAlgorithm::InitCellCost(CsvReader * pCsvReader)
{
	//コストを登録する2次元配列の初期化
	pCosts_ = new int*[height_];


	//GetValue(x,y)
	//条件：マス高さ分回す
	for (int y = 0; y < height_; y++)
	{
		//行の一次元配列の確保
		pCosts_[y] = new int[width_];
		//条件：マス幅分回す
		for (int x = 0; x < width_; x++)
		{
			pCosts_[y][x] = pCsvReader->GetValueInt(x, y);
		}
	}


}

//２つのセルが等しいかの判断
bool AStarAlgorithm::IsEqualCell(Cell searchCell, Cell goalCell)
{
	//引数のCell値が互いに等しいかの判断
	//条件：探索対象セルｘ　と　ゴールセルｘが同じ
	//　　：&&
	//　　：探索対象セルｙ　と　ゴールセルｙが同じ
	if (searchCell.x_ == goalCell.x_ &&
		searchCell.y_ == goalCell.y_)
	{
		//同じセル
		return true;
	}

	//違うセル
	return false;
}

//ヒューリスティクスコストの計算
float AStarAlgorithm::CalculateHeuristic(Node* adjacent_node, Node* goal_node)
{
	//２点間の直線距離
	//を図る（座標地xyが、存在しているので、そこから、直線距離を出すことが可能）
	//https://yttm-work.jp/img/algorithm/algorithm_0047.png?001

	//２点間の直線距離をヒューリスティックコストして返す

	Cell pos1 = adjacent_node->Position;
	Cell pos2 = goal_node->Position;

	//２点間の距離
	double distance = std::sqrt(
		(pos2.x_ - pos1.x_) * (pos2.x_ - pos1.x_) +
		(pos2.y_ - pos1.y_) * (pos2.y_ - pos1.y_));

	//２点間の距離を返す
	return (float)distance;
}

//オープンリストに追加
bool AStarAlgorithm::AddAdjacentNode(std::list<Node*>& open_list, std::list<Node*>& close_list,
	Node * adjacentNode, float cost)
{
	//条件：引数　追加対象セルが０（スタート）でなくて
	//　　：&&
	// 	　：新規追加のコストのほうが小さい
	if (adjacentNode->TotalCost != 0 && adjacentNode->TotalCost < cost)
	{
		//追加しない
		return false;
	}

	//条件：クローズリストに同じノードがあってリストのほうのコストが高いなら消去 true
	//　　：リストのほうのコストが高いなら消去 ture
	//　　：探索のほうのコストが低いならリストがわ消去 true
	//　　：探索して、そもそも存在しなかったら、ture
	//　　：見つかっても、更新できなかった　false

	//クローズリストに消去対象ノードが存在するか
	//条件：消去対象ではない
	if (!EraseNode(close_list, adjacentNode, cost))
	{
		//追加しない
		return false;
	}
	//オープンリストに消去対象ノードが存在するか
	//条件：消去対象ではない
	if (!EraseNode(open_list, adjacentNode, cost))
	{
		//追加しない
		return false;
	}

	//オープンリスト、クローズリストと調べて
	//オープンリストに追加してはいけない結果が出ていなかったら
		//すべてがtrueなら
	{
		//オープンリスト
		//次の探索対象先として追加
		open_list.push_back(adjacentNode);
		//追加した
		return true;
	}

}

//条件を満たすノードの削除
bool AStarAlgorithm::EraseNode(std::list<Node*>& search_list, Node * adjacentNode, float cost)
{
	//処理の結果
	//条件：クローズリストに同じノードがあってリストのほうのコストが高いなら消去 true
	//　　：リストのほうのコストが高いなら消去 ture
	//　　：探索のほうのコストが低いならリストがわ消去 true
	//　　：探索して、そもそも存在しなかったら、ture
	//　　：見つかっても、更新できなかった　false


	//判別方法
	//　　：�@引数のノードと同様のノードを引数リストから探索
	//　　：�A探索成功後、探索して見つかったノード（引数ノードと同様のノード）のコストと、引数ノードのコスト比較
	//　　：�B探索して見つかったノードの方が小さいならば、そのノードをリストから削除


	//adjacentNodeは、Grapthのアドレス
	//search_llistの一つ一つも、Grapthのアドレスなので、
		//元をたどれば、＝＝になるはず

	//�@
	//条件：引数リストのイテレータによる最後の要素ではない
	for (auto itr = search_list.begin();
		itr != search_list.end(); itr++)
	{
		
		//条件：イテレータの要素と引数要素が同じ
		if ((*itr)->Position.x_ == adjacentNode->Position.x_ &&
			(*itr)->Position.y_ == adjacentNode->Position.y_)
		{

			//�A
			//条件：イテレータによるリスト内のNodeのほうがコストが大きかったら
			if((*itr)->TotalCost > cost)
			{
				//�B
				//引数リストからイテレータにより要素の削除
				search_list.erase(itr);

				//消去したことを伝えて
				//trueを返す
				return true;
			}
			else
			{
				//リストのほうがコストが小さかったので
					//falseを返す

				//falseにすることで、
				//オープンリストに追加しないようにする
				return false;
			
			}

		}
	
	}


	//見つからなかった場合も、
	//trueを返すようにする
	return true;

	//falseになるのは、
	//同じノードがあって、かつ、
	//リストのノードのほうが、コストが少なかった時のみ

}

//ノードごとのコスト（pCosts_）を取得
int AStarAlgorithm::GetCellCost(Cell cell)
{
	return pCosts_[cell.y_][cell.x_];
}
