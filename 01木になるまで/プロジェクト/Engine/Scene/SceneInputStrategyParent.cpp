#include "SceneInputStrategyParent.h"			//ヘッダ
#include "SceneUserInputter.h"					//ユーザー入力クラス
#include "SceneManagerParent.h"					//シーンマネージャークラス　継承元クラス
#include "SceneChanger.h"						//シーンマネージャー
#include "../../CommonData/SceneType.h"			//シーンタイプ
#include "../../CommonData/GlobalMacro.h"		//共通マクロ


/************************************************************************************************************************/

//コンストラクタ
FactoryForSceneInputStrategyParent::FactoryForSceneInputStrategyParent()
{
}


//作成された行動クラスをSceneUserInputter（入力受け付けクラス）に登録する処理
void FactoryForSceneInputStrategyParent::RegistrationStrategyInSceneUserInputter(
	SceneUserInputter* pSceneUserInputter,
	SceneInputStrategyParent* pStrategy, 
	CreateInputStrategyInfo* pCreateInfo
)
{


	//SceneUserInputterのコード登録関数を呼び出して、
	//処理を実行する

	//登録関数へ渡す前に、
	//登録情報を作成する必要がある
		//登録情報の作成
	InputInfo* pInputInfo = new InputInfo(pStrategy , pCreateInfo->INPUT_TYPE);

	//その際に、
	//入力をするデバイスが何であるかで、
	//呼び出す関数が切り替わるため、
	//判別を行う
	//条件：デバイスがキーボードである場合
	if (INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_KEY == pCreateInfo->KEY_OR_MOUSE)
	{
		//キーコードの登録関数を呼び出す
		pSceneUserInputter->RequestAddKeyCode(pCreateInfo->CODE, pInputInfo);

	}
	//条件：デバイスがマウスである場合
	else if (INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_MOUSE == pCreateInfo->KEY_OR_MOUSE)
	{
		//マウスコードの登録関数を呼び出す
		pSceneUserInputter->RequestAddMouseCode((MOUSE_CODE)pCreateInfo->CODE, pInputInfo);
	}

}

//引数にて渡されたシーンマネージャーが存在するか
bool FactoryForSceneInputStrategyParent::IsExistsSceneManager(SCENE_MANAGER_ID sceneManagerID , 
	SceneManagerParent* pSceneManagerParent)
{

	//引数にて示されているオブジェクトが
	//きちんと存在し、
	//現在のシーンマネージャーであるか確認する

	//シーンチェンジャー取得
	SceneChanger* pSceneChanger_ = (SceneChanger*)pSceneManagerParent->FindObject("SceneChanger");

	//シーンチェンジャーから
	//現在のシーンマネージャーが正しいか
	return pSceneManagerParent == pSceneChanger_->GetCurrentSceneManager(sceneManagerID);

}



/************************************************************************************************************************/


/************************************************************************************************************************/
//コンストラクタ
SceneInputStrategyParent::SceneInputStrategyParent() : 
	SceneInputStrategyParent(
		nullptr , 
		new CreateInputStrategyInfo(KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_MAX ,
		INPUT_TYPE_KEY_OR_MOUSE::INPUT_TYPE_NONE , 
		-1))
{
}

//コンストラクタ
SceneInputStrategyParent::SceneInputStrategyParent(
	SceneManagerParent* pSceneManager , 
	CreateInputStrategyInfo* pCreateInfo) :
	pSceneManager_(pSceneManager),

	pCreateInfo_(pCreateInfo)
{
}

//デストラクタ
SceneInputStrategyParent::~SceneInputStrategyParent()
{
	//生成情報の削除
		//キー番号などなどの情報を持った動的確保情報を削除
	//継承元にて、解放を行うが
		//各継承先で上書きを行わずに、継承元のデストラクタを呼び込むようにする
	//継承先が解放されるタイミングで、
		//継承元のデストラクタを呼び込むので、あえて記述する必要はない
	SAFE_DELETE(pCreateInfo_);
}

//指定UI群の表示、非表示の切替え
void SceneInputStrategyParent::VisibleInvertUIGroup(const int UIGroupType)
{
	pSceneManager_->VisibleInvertSceneUIGroup((int)UIGroupType);
}

//引数のタイプを除いたUI群の表示
void SceneInputStrategyParent::VisibleAllUIGroupExceptArgument(const int UIGroupType)
{
	//SceneManagerのGetUIGroupsCountにて取得可能。
	//引数値を除いたUI群のすべてを表示フラグを行う
	//条件：UIGrou数
	for (int i = 0; i < pSceneManager_->GetUIGroupCount(); i++)
	{
		//条件：引数のUIGroupのタイプでなかったら
		if ((int)UIGroupType != i)
		{
			//表示を行う
			//pGameSceneManager_->VisibleSceneUIGroup((GAME_SCENE_UI_GROUP_TYPE)i);
			pSceneManager_->VisibleSceneUIGroup(i);
		}
	}


}

//引数のタイプを除いたUI群の非表示
void SceneInputStrategyParent::InvisibleAllUIGroupExceptArgument(const int UIGroupType)
{
	//引数値を除いたUI群のすべてを非表示フラグを行う
	//条件：UIGrou数
	for (int i = 0; i < pSceneManager_->GetUIGroupCount(); i++)
	{
		//条件：引数のUIGroupのタイプでなかったら
		if ((int)UIGroupType != i)
		{
			//非表示を行う
			pSceneManager_->InvisibleSceneUIGroup(i);
		}
	}
}


