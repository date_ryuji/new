#include "SceneUserInputter.h"				//ヘッダ
#include "../../CommonData/GlobalMacro.h"	//共通マクロ
#include "../DirectX/Input.h"				//入力クラス
#include "SceneInputStrategyParent.h"		//入力時の行動実行クラス


//コンストラクタ
SceneUserInputter::SceneUserInputter(GameObject* parent) :
	GameObject::GameObject(parent , "SceneUserInputter") , 
	pKeyCodes_(nullptr) , 
	pMouseCodes_(nullptr) , 
	pKeyMap_(nullptr),
	pMouseMap_(nullptr),

	pEraseKeyCodes_(nullptr),
	pEraseMouseCodes_(nullptr) ,
	pNextKeyMap_(nullptr) , 
	pNextMouseMap_(nullptr) 
{
}


//初期化
void SceneUserInputter::Initialize()
{
	//初期化
		//入力コードを登録する領域の初期化
		//キーボード、マウス　ともども初期化
	{
		pKeyCodes_ = new std::list<int>;
		pEraseKeyCodes_ = new std::list<int>;
		pMouseCodes_ = new std::list<MOUSE_CODE>;
		pEraseMouseCodes_ = new std::list<MOUSE_CODE>;
		pKeyMap_ = new std::map<int, InputInfo*>;
		pNextKeyMap_ = new std::map<int, InputInfo*>;
		pMouseMap_ = new std::map<MOUSE_CODE, InputInfo*>;
		pNextMouseMap_ = new std::map<MOUSE_CODE, InputInfo*>;

		pKeyCodes_->clear();
		pEraseKeyCodes_->clear();
		pMouseCodes_->clear();
		pEraseMouseCodes_->clear();
		pKeyMap_->clear();
		pNextKeyMap_->clear();
		pMouseMap_->clear();
		pNextMouseMap_->clear();

	}
}

//更新
void SceneUserInputter::Update()
{
	//消去依頼を受けたコードを削除
	FrameEraseCode();
	//更新依頼を受けたコードを更新
	FrameUpdateMap();


	//キー入力受け付け
	KeyInputJudgement();
	//マウス入力受け付け
	MouseInputJudgement();
}

//描画
void SceneUserInputter::Draw()
{
}

//解放
void SceneUserInputter::Release()
{
	//コード内のデータを削除
	RequestSetEmptyAllCode();


	SAFE_DELETE(pNextMouseMap_);
	SAFE_DELETE(pMouseMap_);
	SAFE_DELETE(pNextKeyMap_);
	SAFE_DELETE(pKeyMap_);
	SAFE_DELETE(pEraseMouseCodes_);
	SAFE_DELETE(pMouseCodes_);
	SAFE_DELETE(pEraseKeyCodes_);
	SAFE_DELETE(pKeyCodes_);
}

//入力コードの更新（前フレームに更新依頼されたコードを更新）
void SceneUserInputter::FrameUpdateMap()
{
	//前回のフレームに更新したマップ内の要素を
	//常時参照のマップにコピー
	//条件：更新依頼をされたコード　分
	for (auto itr = pNextKeyMap_->begin(); itr != pNextKeyMap_->end();itr++)
	{
		//itr->first　にてKey取得
		int keyCode = itr->first;
		//itr->second　にてValue取得
		InputInfo* pInputInfo = itr->second;

		//常駐のマップへ登録
		AddResidentKeyCode(keyCode, pInputInfo);

	
		//マップのイテレータは
		// リストなどのコレクションクラスと同様のアクセスが可能になるものではない。
		//イテレータは
			//順番（順番といっても、マップ内で昇順されているようだが）にマップ内の要素にアクセスするためのもの
		/*
		//そして、イテレータを回すには、
		//リストのような＋＋で回すことはできない
			//リストなどはイテレータでランダムアクセスが可能だが、
			//マップはそうはいかない
		//専用のメソッドを使用する
		*/


	}

	//マウスも同様である
	//前回のフレームに更新したマップ内の要素を
	//常時参照のマップにコピー
	//条件：更新依頼をされたコード　分
	for (auto itr = pNextMouseMap_->begin(); itr != pNextMouseMap_->end(); itr++)
	{
		//itr->first　にてKey取得
		MOUSE_CODE mouseCode = itr->first;
		//itr->second　にてValue取得
		InputInfo* pInputInfo = itr->second;

		//常駐のマップへ登録
		AddResidentMouseCode(mouseCode, pInputInfo);

	}

	//コピーを終えた前フレームの
	//要素をクリア
	pNextKeyMap_->clear();
	pNextMouseMap_->clear();

}

//入力コードの削除（前フレームに削除依頼をされたコードを削除）
void SceneUserInputter::FrameEraseCode()
{
	//前回のフレームにて
	//消去依頼を受けたコードを削除する

	//キーコード
	//条件：消去依頼されたコード　分
	for (auto itr = pEraseKeyCodes_->begin(); itr != pEraseKeyCodes_->end(); itr++)
	{
		//イテレータにて示されるキーコードを削除
		EraseResidentKeyCode((*itr));
	}
	//マウスコード
	//条件：消去依頼されたコード　分
	for (auto itr = pEraseMouseCodes_->begin(); itr != pEraseMouseCodes_->end(); itr++)
	{
		//イテレータにて示されるマウスコードを削除
		EraseResidentMouseCode((*itr));
	}

	//コピーを終えた前のフレームの要素をクリア
	pEraseKeyCodes_->clear();
	pEraseMouseCodes_->clear();

}

//常駐のキーコード(pKeyCode)へ追加
void SceneUserInputter::AddResidentKeyCode(int keyCode, InputInfo* pInputInfo)
{
	////キーコードの登録
	//AddKeyCode(pKeyMap_, pKeyCodes_, keyCode, pInputInfo);

	//登録前のサイズを取得
	int beforeSize = (int)pKeyMap_->size();

	//マップへ登録
	pKeyMap_->insert(
		std::map<int, InputInfo*>::
		value_type(keyCode, pInputInfo));

	//マップへ登録し、
		//マップに登録ができたときには、サイズが増えているはず、
		//登録できていない（キーが同じものがあった）場合は、サイズが変更されないので、
		//登録できていないときは、キーリストへコードを登録させない
	//条件：登録前サイズ　と　マップサイズが同じでない
	if (beforeSize != pKeyMap_->size())
	{
		//キーリストへ登録
		pKeyCodes_->push_back(keyCode);
	}

}

//常駐マウスコード(pMouseCode)へ追加
void SceneUserInputter::AddResidentMouseCode(MOUSE_CODE mouseCode, InputInfo* pInputInfo)
{
	////マウスコードの登録
	//AddMouseCode(pMouseMap_, pMouseCodes_, mouseCode, pInputInfo);


	//登録前のサイズを取得
	int beforeSize = (int)pMouseMap_->size();

	//マップへ登録
	pMouseMap_->insert(
		std::map<MOUSE_CODE, InputInfo*>::
		value_type(mouseCode, pInputInfo));

	//マップへ登録
	//マップへ登録が完了し、
		//前回のサイズと今回のサイズが違う場合
	//条件：登録前サイズ　と　マップサイズが同じでない
	if (beforeSize != pMouseMap_->size())
	{
		//キーリストへ登録
		pMouseCodes_->push_back(mouseCode);
	}
}

//常駐のキーコード(pKeyCode)から削除
void SceneUserInputter::EraseResidentKeyCode(int keyCode)
{
	//keyCodeにて示される値をもとに
	//要素を削除
	pKeyCodes_->remove(keyCode);
	//あえて、eraseではなく、removeという、リスト内登録の要素を指定して削除命令
	//eraseの場合、イテレータにて指定する必要がある


	//常駐マップ
	{
		//マップ内から
		//引数キーを探す
		//見つかった　：要素のイテレータが取得される
		//見つからない：end()のイテレータが取得される
		auto foundKey = pKeyMap_->find(keyCode);

		//条件：キーが存在するか
		if (foundKey != pKeyMap_->end())
		{
			//存在するならば
			//マップ内から削除
			//マップに登録されている行動クラスを削除
			SAFE_DELETE((*pKeyMap_)[keyCode]->pInputStrategy);
			//infoの削除
			SAFE_DELETE((*pKeyMap_)[keyCode]);

			//マップから要素を削除
			//キーを渡し、同様のキーを持っている要素の、キーとバリューを削除
			pKeyMap_->erase(keyCode);
		}
	}
	//次フレーム追加マップ
		//同じフレーム内にて、
		//追加と、次フレーム追加が指定されないとも限らない
		//その場合、追加、消去の順番に限らず、　消去を優先する
			//そのため、自身フレーム内にて、
			//次フレーム（詳細に言うと、以下の自身の消去関数終了後に行う、フレーム内キー追加にて追加するキーに、消去が立っていたならば、消去を優先）
	{
		//マップ内から
		//引数キーを探す
		//見つかった　：要素のイテレータが取得される
		//見つからない：end()のイテレータが取得される
		auto foundKey = pNextKeyMap_->find(keyCode);

		//条件：キーが存在するか
		if (foundKey != pNextKeyMap_->end())
		{
			//存在するならば
			//マップ内から削除
			//マップに登録されている行動クラスを削除
			SAFE_DELETE((*pNextKeyMap_)[keyCode]->pInputStrategy);
			//infoの削除
			SAFE_DELETE((*pNextKeyMap_)[keyCode]);

			//マップから要素を削除
			//キーを渡し、同様のキーを持っている要素の、キーとバリューを削除
			pNextKeyMap_->erase(keyCode);
		}
	}



}

//常駐のマウスコード(pMouseCode)から削除
void SceneUserInputter::EraseResidentMouseCode(MOUSE_CODE mouseCode)
{
	//キーリストから
	//要素を削除
	pMouseCodes_->remove(mouseCode);
	//あえて、eraseではなく、removeという、リスト内登録の要素を指定して削除命令


	//常駐マップ
	{
		//マップ内から
		//引数キーを探す
		//見つかった　：要素のイテレータが取得される
		//見つからない：end()のイテレータが取得される
		auto foundKey = pMouseMap_->find(mouseCode);


		//条件：キーが存在するか
		if (foundKey != pMouseMap_->end())
		{
			//存在するならば
			//マップ内から削除
			//マップに登録されている行動クラスを削除
			SAFE_DELETE((*pMouseMap_)[mouseCode]->pInputStrategy);
			//infoの削除
			SAFE_DELETE((*pMouseMap_)[mouseCode]);

			//マップから要素を削除
			//キーを渡し、同様のキーを持っている要素の、キーとバリューを削除
			pMouseMap_->erase(mouseCode);
		}
	}
	//次フレーム追加マップ
	{
		//マップ内から
		//引数キーを探す
		//見つかった　：要素のイテレータが取得される
		//見つからない：end()のイテレータが取得される
		auto foundKey = pNextMouseMap_->find(mouseCode);

		//条件：キーが存在するか
		if (foundKey != pNextMouseMap_->end())
		{
			//存在するならば
			//マップ内から削除
			//マップに登録されている行動クラスを削除
			SAFE_DELETE((*pNextMouseMap_)[mouseCode]->pInputStrategy);
			//infoの削除
			SAFE_DELETE((*pNextMouseMap_)[mouseCode]);

			//マップから要素を削除
			//キーを渡し、同様のキーを持っている要素の、キーとバリューを削除
			pNextMouseMap_->erase(mouseCode);
		}
	}



}


//常駐キーコード(pKeyCode)の入力判断
void SceneUserInputter::KeyInputJudgement()
{
	//キーリストを回し
	//キー入力がされているかの判別を行う
		//キー入力がされていると判断された時、キー入力時の行動を実行する
	//条件：キーコードに登録されたコード　分
	for (auto itr = pKeyCodes_->begin(); itr != pKeyCodes_->end(); itr++)
	{
		//入力判定用のフラグ
		bool isInput = false;

		switch ((*pKeyMap_)[(*itr)]->inputType)
		{
		case KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_ALWAYS : 
			//キーが押し続けられているか
			isInput = Input::IsKey((*itr));
			break;
		
		case KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_DOWN:
			//キーが押されたか
			isInput = Input::IsKeyDown((*itr));
			break;
		
		case KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_UP:
			//キーが離されたか
			isInput = Input::IsKeyUp((*itr));
			break;
		default : 
			isInput = false;
			break;

		}


		//条件：キー入力がされていた場合
		if (isInput)
		{
			//入力時の行動実行
			ExecuteKeyInputtedAlgorithm(itr);
		}
	}
}

//常駐キーコードの入力を受けて　行動アルゴリズム実行
void SceneUserInputter::ExecuteKeyInputtedAlgorithm(std::list<int>::iterator& itr)
{
	//入力時の行動クラスを実行する
	//キーをもとに行動がマップに登録されているので、	その登録された行動の実行呼び出し
	(*pKeyMap_)[(*itr)]->pInputStrategy->AlgorithmInterface();
}

//常駐マウスコード(pMouseCode)の入力判断
void SceneUserInputter::MouseInputJudgement()
{
	//キーリストを回し
	//マウス入力がされているかの判別を行う
		//マウス入力がされていると判断された時、マウス入力時の行動を実行する
	//条件：マウスに登録されたコード　分
	for (auto itr = pMouseCodes_->begin(); itr != pMouseCodes_->end(); itr++)
	{
		//入力判定用のフラグ
		bool isInput = false;

		switch ((*pMouseMap_)[(*itr)]->inputType)
		{
		case KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_ALWAYS:
			//マウスが押し続けられているか
			isInput = Input::IsMouseButton((int)(*itr));
			break;

		case KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_DOWN:
			//マウスが押されたか
			isInput = Input::IsMouseButtonDown((int)(*itr));
			break;

		case KEY_AND_MOUSE_INPUT_PUSH_TYPE::INPUT_UP:
			//マウスが離されたか
			isInput = Input::IsMouseButtonUp((int)(*itr));
			break;
		default:
			isInput = false;
			break;

		}


		//条件：マウスー入力がされていた場合
		if (isInput)
		{
			//入力時の行動実行
			ExecuteMouseInputtedAlgorithm(itr);
		}
	}
}

//常駐マウスコードの入力を受けて　行動アルゴリズム実行
void SceneUserInputter::ExecuteMouseInputtedAlgorithm(std::list<MOUSE_CODE>::iterator& itr)
{
	//入力時の行動クラスを実行する
	//キーをもとに行動がマップに登録されているので、	その登録された行動の実行呼び出し
	(*pMouseMap_)[(*itr)]->pInputStrategy->AlgorithmInterface();
}

//キーコード登録(pKeyCode)　依頼
void SceneUserInputter::RequestAddKeyCode(int keyCode, InputInfo* pInputInfo)
{
	//キーコードを次のフレームに更新を催促する
	//マップに登録する

	//マップへ登録
	pNextKeyMap_->insert(
		std::map<int, InputInfo*>::
		value_type(keyCode, pInputInfo));
}

//マウスコード登録(pMouseCode)　依頼
void SceneUserInputter::RequestAddMouseCode(MOUSE_CODE mouseCode, InputInfo* pInputInfo)
{
	//マウスコードを次のフレームに更新を催促する
	//マップに登録する

	//マップへ登録
	pNextMouseMap_->insert(
		std::map<MOUSE_CODE, InputInfo*>::
		value_type(mouseCode, pInputInfo));
}

//常駐コード(pKeyCode , pMouseCode)のすべてのコードを空にする
void SceneUserInputter::RequestSetEmptyAllCode()
{
	//コードをすべて空にする
	SetEmptyAllKeyCode();
	SetEmptyAllMouseCode();
}

//常駐キーコード（pKeyCode）をすべて空へ
void SceneUserInputter::SetEmptyAllKeyCode()
{
	//キーコード分
	//マップに領域が確保されているため
	//キーコードから、
	//マップの要素にアクセスして、
		//要素内のストラテジーを解放する
	//条件：キーコードに登録されたコード　分
	for (auto itr = pKeyCodes_->begin(); itr != pKeyCodes_->end(); itr++)
	{
		//リストに登録された
		//キーコードをマップのキーとしてアクセスする
		//！注意！　マップへのアクセスする添え字は、元々登録したキーからのアクセスであるので、forでアクセスする際にキーコードのリストのイテレータが主となる

		//マップに登録されている行動クラスを削除
		SAFE_DELETE((*pKeyMap_)[(*itr)]->pInputStrategy);
		//InputInfoの解放
		SAFE_DELETE((*pKeyMap_)[(*itr)]);
	}

	//リストを空に
	pKeyCodes_->clear();

	//マップを空にする
	pKeyMap_->clear();
}

//常駐マウスコード（pMouseCode）をすべて空へ
void SceneUserInputter::SetEmptyAllMouseCode()
{
	
	//マウスコードを登録した
	//リストから一つ一つのコードw取得し
	//マップのキーとして使用する
	//条件：マウスに登録されたコード　分
	for (auto itr = pMouseCodes_->begin(); itr != pMouseCodes_->end(); itr++)
	{
		//マップに登録されている行動クラスを削除
		SAFE_DELETE((*pMouseMap_)[(*itr)]->pInputStrategy);
		//InputInfoの解放
		SAFE_DELETE((*pMouseMap_)[(*itr)]);
	}

	//リストを空に
	pMouseCodes_->clear();

	//マップを空にする
	pMouseMap_->clear();
}

//指定キーコード（pKeyCode）を空にする　依頼
void SceneUserInputter::RequestSetEmptyKeyCode(int keyCode)
{

	//次のフレームの削除する
		//上記のために、専用にリストへ登録
		//次のフレームに、そのリストからキーを取得して、
		//削除を実行
	//リストへ登録
	pEraseKeyCodes_->push_back(keyCode);

}

//指定キーコード（pMouseCode）を空にする　依頼
void SceneUserInputter::RequestSetEmptyMouseCode(MOUSE_CODE mouseCode)
{
	//次のフレームの消去リストへ要素登録
	//リストへ登録
	pEraseMouseCodes_->push_back(mouseCode);

}



