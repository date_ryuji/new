#pragma once

//クラスのプロトタイプ宣言
class SceneInputStrategyParent;
class SceneUserInputter;
	/*
	
		SceneUserInputter（入力受け付けクラス）（ユーザーの入力を受け付けるクラス）
	
	*/
class SceneManagerParent;
	/*
		シーンマネージャーについて

		//AlgorithmInterfaceにて、実行する処理などで使用する
		//各行動を実行するのだが、実行をするにも、ストラテジーのAlgorithmInterface内でできることなどたかが知れている。
		//そのため、変に分離したところで行動を実行するよりも、行動することが決まったら、SceneManagerなどに行動をお願いする
		//行動実行を分離できているのかと疑問が生じるが、行動を起こす以上、その行動を起こす先を調べないといけない。その行動先を知っている人に聞かなければいけない。
		//〜〜と辿るよりは、あらかじめ知っている人にその先の処理はお願いしてしまう。　
		//行動実行までを分離して、行動されたらその先の行動はゲーム内
	*/

//enum class のプロトタイプ宣言
enum class INPUT_TYPE_KEY_OR_MOUSE;
enum class KEY_AND_MOUSE_INPUT_PUSH_TYPE;
enum class SCENE_MANAGER_ID;
	/*
		enum class のプロトタイプ宣言

		enum型のプロトタイプ宣言は推奨されていないというエラーが出てくる
		だが、enum clss であれば、エラーが起こらない。
		そのため、クラス間で共通して使うようなクラスはenum class を使用している	
	*/



/*
		構造体詳細	：キー、マウス入力における入力状態を格納する構造体
		構造体概要（詳しく）
					：　入力時の行動クラス作成の際に
					　　必要な情報類をまとめた構造体
*/
struct CreateInputStrategyInfo
{
	//キー入力のプッシュタイプ（押下中、押下時など）
	KEY_AND_MOUSE_INPUT_PUSH_TYPE INPUT_TYPE;
	//キーかマウスか
	INPUT_TYPE_KEY_OR_MOUSE KEY_OR_MOUSE;
	//コード番号
	int CODE;

	//コンストラクタ
	CreateInputStrategyInfo(
		KEY_AND_MOUSE_INPUT_PUSH_TYPE inputType,
		INPUT_TYPE_KEY_OR_MOUSE keyOrMouse,
		int code) :
		INPUT_TYPE(inputType),
		KEY_OR_MOUSE(keyOrMouse),
		CODE(code)
	{
	
	}
};

/*
	各シーンへの継承

	シーンごとにSceneUserInputterを使用するならば、

	各シーンごとに
	SceneFactory・行動クラスの生成クラスを継承して　シーンごとの○○SceneInputStrategyFactory
	SceneInputStrategyParent・行動アルゴリズム実行クラスを継承して　シーンごとの○○SceneInputStrategy　
	
	を作成する（○○　＝　シーン名）

	各シーンごとのクラスを派生させる
*/

/*
	クラス詳細	：各デバイス入力を行った際の行動クラス　作成クラス
	使用デザインパターン：FactoryMethod
	クラスレベル：スーパークラス
	クラス概要（詳しく）
				：該当行動クラスを作成するだけのクラス。
				：FactoryMethodを使用する利点は、Shader/ShaderFactory.hにて記述している

				：各シーンごとに、生成クラスを継承させて、
				　そのシーン内での、ストラテジークラスを生成する生成クラスとして派生させる

				：デバイス入力を登録するには、
				　各シーンのInitializeにて、　生成クラス（Factory）を生成。　入力コード、生成クラスを使用して作った行動クラスを　キー入力クラス（UserInputter）に登録。
				　あとは、キー入力クラス（UserInputter）に任せる

				 ↓
				：生成クラス（Factory）にて、必要情報だけ渡して、生成できる関数を持たせておく形へ変更
				　シーンごとのファクトリークラス（Factory）にて、　シーンごとの行動クラス（SceneInputStrategyParentに継承できるクラスを作る）
				　そして、そのクラス内にて、さらに親元のSceneUserInputtterに登録するクラスまで呼んでしまう（どちらにしても、引数が多くなる


				:継承させる形にすることで
				　生成クラス
				　生成クラスは、各行動のタイプであるenum値（各シーンごとの行動生成クラスと同じファイルに入っている。）その値を生成実行関数では必要となる
		　　　　　であれば、継承元で、いちいちenum値をintに変換して保存しておく理由もない。

*/
class FactoryForSceneInputStrategyParent
{
//protected メソッド
protected : 

	//作成された行動クラスをSceneUserInputter（入力受け付けクラス）に登録する処理
		//引数：SceneUserInputter（入力受け付けクラス）（ユーザーの入力を受け付けるクラス）
		//引数：入力時の行動ストラテジー
		//引数：入力情報
		//戻値：なし
	void RegistrationStrategyInSceneUserInputter(
		SceneUserInputter* pSceneUserInputter , 
		SceneInputStrategyParent* pStrategy , 
		CreateInputStrategyInfo* pCreateInfo);

	


	//引数にて渡されたシーンマネージャーが存在するか
		//詳細：現在のシーンマネージャーであるか
		//引数：マネージャーID
		//引数：シーンマネージャーポインタ
		//戻値：存在した（存在した：true , 存在しない：false）
	bool IsExistsSceneManager(SCENE_MANAGER_ID sceneManagerID , SceneManagerParent* pSceneManagerParent);



	//行動クラスの生成関数
		//詳細：各行動クラス型のまま、インスタンスを生成。インスタンスを受け取る
		//レベル：テンプレート　tempalate<class T>（制限：SceneInputStrategyParent　継承）
		//引数：シーンマネージャーポインタ
		//引数：入力情報
		//戻値：行動クラスのインスタンス（T）
	template <class T>
	T* CreateStrategy(
		SceneManagerParent* pSceneManager_,
		CreateInputStrategyInfo* pCreateInfo
	)
	{
		//行動クラスの動的生成
		T* pInputStrategy = new T(pSceneManager_, pCreateInfo);

		return pInputStrategy;
	};


//public メソッド
public : 
	//コンストラクタ
		//引数：なし
		//戻値：なし
	FactoryForSceneInputStrategyParent();

	

};


/*
	クラス詳細	：デバイス入力の行動ストラテジーの親クラス
	クラスレベル：抽象クラス
	クラス概要（詳しく）
				：行動ストラテジークラス（後述：行動クラス）は、該当キーと一緒に登録することで、
				　キーが押されたときに該当行動クラスの、行動実行メソッドを呼ぶようにする。
				：上記により、デバイス入力による、行動実行が設定可能

*/
class SceneInputStrategyParent
{
//protected メンバ変数、ポインタ、配列
protected : 
	//シーンマネージャークラス
	SceneManagerParent* pSceneManager_;
	//入力情報
		//詳細：キー入力タイプ、デバイスタイプ、キーコード
	CreateInputStrategyInfo* pCreateInfo_;

//protected メソッド
protected : 

	/*描画関連*******************************************************************/
	//指定UI群の表示、非表示の切替え
		//引数：UIGroupのハンドル番号
		//戻値：なし
	void VisibleInvertUIGroup(const int UIGroupType);
	//引数のタイプを除いたUI群の表示
		//引数：表示対象外のUIGroupのハンドル番号
		//戻値：なし
	void VisibleAllUIGroupExceptArgument(const int UIGroupType);
	//引数のタイプを除いたUI群の非表示
		//引数：非表示対象外のUIGroupのハンドル番号
		//戻値：なし
	void InvisibleAllUIGroupExceptArgument(const int UIGroupType);


	/*
		キー入力の解除、設定はManagerにて行う

			//キー入力を扱うクラス
			//SceneUserInputterを持たせるかが迷う
			//持たせてしまえば、キー入力の管理を自身で行うことができるのだが、
			//キー入力の可能不可能を、SceneManagerからManagerの持っているSceneUserInputtterにてセットするようにすれば良いのではと思っていたが、
			//GameSceneManagerを経由しなくてもよいのではとも感じている。

			//今は、とりあえず、SceneManagerを経由するように。
			//となると、SceneManagerParentに、UserInputterを持たせておく必要がある。
			//各継承先で、
			//キー入力に対する処理関数を個別で持たせることにしておく。

	*/
	



public : 
	//コンストラクタ
		//引数：シーンマネージャーポインタ
		//引数：入力情報
	SceneInputStrategyParent(SceneManagerParent* pSceneManager_ , CreateInputStrategyInfo* pCreateInfo);

	//引数なしコンストラクタ
		//引数：なし
		//戻値：なし
	SceneInputStrategyParent();
	//デストラクタ
		//レベル：仮想関数		
		//引数；なし
		//戻値：なし
	virtual ~SceneInputStrategyParent();


	//行動実行クラス
		//詳細：登録キーが押された際に、該当クラスである自身の行動実行クラスが呼ばれる。
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void AlgorithmInterface() = 0;

};

