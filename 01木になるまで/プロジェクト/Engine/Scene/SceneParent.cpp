#include "SceneParent.h"			//ヘッダ
#include "SceneChanger.h"			//シーンチェンジャー　//シーンマネージャーのタイプを識別する、タイプのEnum値を参照するため
#include "SceneManagerFactory.h"	//シーンマネージャー作成クラス
#include "SceneManagerParent.h"		//生成されるシーンマネージャーの継承元クラス
#include "../../CommonData/GlobalMacro.h"	//共通マクロ

//コンストラクタ
SceneParent::SceneParent(GameObject * parent)
	: SceneParent(parent, "SceneParent")
{
}

//コンストラクタ
SceneParent::SceneParent(GameObject * parent, const std::string& OBJ_NAME):
	GameObject(parent , OBJ_NAME),
	pMySceneManager_(nullptr),
	mySceneManagerID_(SCENE_MANAGER_ID::SCENE_MANAGER_ID_NONE)
{
}

//デストラクタ
SceneParent::~SceneParent()
{
	ReleaseSceneManager();
}

//引数タイプ指定のシーンマネージャーを作成する
SceneManagerParent* SceneParent::CreateSceneManager(SCENE_MANAGER_ID sceneManagerID)
{
	//自身のメンバにID保存
	mySceneManagerID_ = sceneManagerID;


	//作成クラスの作成
	SceneManagerFactory* pFactory = new SceneManagerFactory;
	//作成（引数タイプ）
	pMySceneManager_ =  pFactory->CreateSceneManager(sceneManagerID , this);

	//条件：作成ができたのであれば
	if (pMySceneManager_ != nullptr)
	{
		//シーンチェンジャーを入れるポインタ用意
		SceneChanger* pSceneChanger = nullptr;

		//SceneChangerを取得
		//条件：自身の親がSceneChangerならば
		if (pParent_->objectName_ == "SceneChanger")
		{
			//親をセット
			pSceneChanger = (SceneChanger*)pParent_;
		}
		//条件：自身の親がSceneChangerでないならば
		else
		{
			//全オブジェクトから探す
			pSceneChanger = (SceneChanger*)FindObject("SceneChanger");
		}


		//条件：シーンチェインジャーを取得できたなら
		if (pSceneChanger != nullptr)
		{
			//引数IDと、インスタンスをセットする
			pSceneChanger->SetSceneManager(sceneManagerID, pMySceneManager_);
		}
	}

	//作成クラスの削除
	SAFE_DELETE(pFactory);


	//作成されたインスタンスを返す
	return pMySceneManager_;
}

//シーンマネージャー解放
void SceneParent::ReleaseSceneManager()
{
	//シーンチェンジャーを入れるポインタ用意
	SceneChanger* pSceneChanger = nullptr;

	//SceneChangerを取得
	//条件：親がSceneChangerならば
	if (pParent_->objectName_ == "SceneChanger")
	{
		//親をセット
		pSceneChanger = (SceneChanger*)pParent_;
	}
	else
	{
		//全オブジェクトから探す
		pSceneChanger = (SceneChanger*)FindObject("SceneChanger");
	}
	

	//条件：シーンチェインジャーを取得できたなら
	if (pSceneChanger != nullptr)
	{
		//自身のセットしたID,インスタンスをリセット
		pSceneChanger->ResetSceneManager();
	}
}

//シーンマネージャーの取得
SceneManagerParent * SceneParent::GetSceneManager()
{
	return pMySceneManager_;
}

//シーンマネージャーのタイプの取得
SCENE_MANAGER_ID SceneParent::GetSceneManagerID()
{
	return mySceneManagerID_;
}
