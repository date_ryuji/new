#include "SceneManagerParent.h"					//ヘッダ
#include "../../CommonData/GlobalMacro.h"		//共通マクロ
#include "../GameObject/UIGroup.h"				//UIGroup群


//コンストラクタ
SceneManagerParent::SceneManagerParent(GameObject * parent)
	: SceneManagerParent(parent, "SceneManagerParent")
{
}

//コンストラクタ
SceneManagerParent::SceneManagerParent(GameObject * parent, const std::string& OBJ_NAME):
	GameObject(parent , OBJ_NAME),
	ppCoopObjects_(nullptr),
	ppUIGroups_(nullptr)
{
}

//デストラクタ
SceneManagerParent::~SceneManagerParent()
{
}

//初期化
void SceneManagerParent::Initialize()
{
}

//更新
void SceneManagerParent::Update()
{
}

//描画
void SceneManagerParent::Draw()
{
}

//解放
void SceneManagerParent::Release()
{
}


//連携オブジェクト群からオブジェクトを取得する関数
GameObject * SceneManagerParent::GetCoopObject(int type)
{
	//条件：連携オブジェクトを入れる配列が存在する
	if (ppCoopObjects_ != nullptr)
	{
		//連携オブジェクトを返す
		return ppCoopObjects_[type];
	}

	//条件：アクセス先が存在しなかった時注意をする場合
	if (ALERT_WHEN_ACCESSING)
	{
		//アクセス先のオブジェクトが登録されていないことへの注意喚起
		MessageBox(nullptr, "（SceneManager）連携先オブジェクトが存在しない", "エラー", MB_OK);
	}

	//空を返す
	return nullptr;
}

//連携を行うオブジェクト群（ppCoopObjects_）の新規作成（動的確保）
void SceneManagerParent::NewArrayForCoopObjects(int max)
{
	//解放
	DeleteArrayForCoopObjects();

	//引数要素分
		//配列の動的確保
	ppCoopObjects_ = new GameObject*[max];
}

//連携を行うオブジェクト群（ppCoopObjects_）の解放（動的確保の解放）
void SceneManagerParent::DeleteArrayForCoopObjects()
{
	//解放は
		//ポインタのポインタとして、ポインタの配列部分を動的確保したため、動的確保した、部分だけを解放
	SAFE_DELETE_ARRAY(ppCoopObjects_);
}

//UIGroup群（ppUIGroups_）の新規作成（動的確保）
void SceneManagerParent::NewArrayForUIGroups(int max)
{
	//解放
	DeleteArrayForUIGroups();

	//引数要素分
		//配列の動的確保
		//ポインタを複数個入れられる枠の動的確保
	ppUIGroups_ = new UIGroup*[max];

	//要素の初期化
	for (int i = 0; i < max; i++)
	{
		ppUIGroups_[i] = nullptr;
	}
}

//UIGroup群（ppUIGroups_）の解放（動的確保の解放）
void SceneManagerParent::DeleteArrayForUIGroups()
{
	SAFE_DELETE_ARRAY(ppUIGroups_);
}

//オブジェクトの登録（連携オブジェクト群への追加）
void SceneManagerParent::AddCoopObject(int type, GameObject * pCoopObject)
{

	//解放
		//すでに存在しているかもしれないため、空にする
		//ここで、仮に、オブジェクトのポインタがあっても、KillMe（delete処理）はしない
		//ここで、Deleteをしてしまうと、他で参照していた時に整合性が合わなくなるので、勝手にKillMeはしない（そのオブジェクトは役目を終えていないかもしれない）
	ppCoopObjects_[type] = nullptr;

	//引数オブジェクトをセット
	ppCoopObjects_[type] = pCoopObject;

}

//オブジェクトの解除（連携オブジェクト群からの削除）
void SceneManagerParent::RemoveCoopObject(int type, GameObject * pCoopObject)
{
	//条件：第二引数のオブジェクトと配列に登録されているオブジェクトが同じなら解放
		//配列をnullptrにする
	if (ppCoopObjects_[type] == pCoopObject)
	{
		//解放するポインタと同様のポインタ時のみ解放
		ppCoopObjects_[type] = nullptr;
	}
}

//UIGroupの登録（UIGroup群への追加）
void SceneManagerParent::AddUIGroup(int type, UIGroup * pUIGroup)
{
	//解放
	ppUIGroups_[type] = nullptr;

	//引数オブジェクトをセット
	ppUIGroups_[type] = pUIGroup;
}

//UIGroupの削除(UIGroup群からの削除)
void SceneManagerParent::RemoveUIGroup(int type, UIGroup * pUIGroup)
{
	//条件：第二引数のオブジェクトと配列に登録されているオブジェクトが同じなら解放
		//配列をnullptrにする
	if (ppUIGroups_[type] == pUIGroup)
	{
		//解放するポインタと同様のポインタ時のみ解放
		ppUIGroups_[type] = nullptr;
	}
}

//UIGroup群から指定のUIGroup確保
UIGroup * SceneManagerParent::GetUIGroup(int type)
{
	return ppUIGroups_[type];
}

//UIGroupの個数を取得
int SceneManagerParent::GetUIGroupCount()
{
	//各UIGroupのタイプを持っている、
	//enumのMAX値を返す
	return 0;
}

//UIGroupの表示、非表示を切り替える
void SceneManagerParent::VisibleInvertSceneUIGroup(int type)
{
	//条件：参照しているUIGroupが空でない場合
	if (ppUIGroups_[type] != nullptr)
	{
		//UIGroup（UI画像、ボタン）の描画状態（描画する、しないのフラグ）を反転させる
		ppUIGroups_[type]->VisibleInvertSceneUIAndButton();
	}
}

//UIGroupの表示
void SceneManagerParent::VisibleSceneUIGroup(int type)
{

	//条件：参照しているUIGroupが空でない場合
	if (ppUIGroups_[type] != nullptr)
	{
		//UIGroup（UI画像、ボタン）の描画状態を描画するにする
		ppUIGroups_[type]->VisibleSceneUIAndButton();
	}
}

//UIGroupの非表示
void SceneManagerParent::InvisibleSceneUIGroup(int type)
{
	//条件：参照しているUIGroupが空でない場合
	if (ppUIGroups_[type] != nullptr)
	{
		//UIGroup（UI画像、ボタン）の描画状態を描画し内にする
		ppUIGroups_[type]->InvisibleSceneUIAndButton();
	}
}

//UIGroupのα値セット
void SceneManagerParent::SetAlphaSceneUIGroup(int type, float alpha)
{
	//条件：参照しているUIGroupが空でない場合
	if (ppUIGroups_[type] != nullptr)
	{
		//UIGroup（UI画像、ボタン）のα値をセット
		ppUIGroups_[type]->SetAlphaSceneUIAndButton(alpha);
	}
}
