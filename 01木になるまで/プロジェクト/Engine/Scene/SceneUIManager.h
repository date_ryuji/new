#pragma once
//システム内　標準ヘッダ(usr/)
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
#include <d3d11.h>	//ID3D11Texture2D(テクスチャのバッファー型)使用のため
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス


/*
	クラス詳細	：複数UI画像（後述：UI）をまとめて管理するマネージャークラス
	クラス概要（詳しく）
				：UIの表示、非表示
				：UI位置の調整（ローカル座標の調整）
				：UIの奥行き変更（Z方向へ移動することで、画像の前後関係）
				
				：UIGroupの子供インスタンスとして自身を生成
				　SceneUIManagerのインスタンスを直接受け取り、各UIへのアクセスを行う


	
*/
/*
	実装メモ

	以前
		SceneUIManagerにて管理するUI画像の違いは、画像のテクスチャ、画像の位置など、
		そのため、テクスチャを複数所有しておき、
		テクスチャ表示のためのSpriteなどをひとつだけImage（namespace）に確保することで、
		必要な画像テクスチャの描画をするときに、テクスチャを動的に描画するという処理が最適だと思っていた。


	上記の問題
		問題は、Imageなどで行っていた、複数UI画像のロードの重複チェック、フライウェイト機能をまた下記クラス（SceneUIManager）に記述する必要が出てくる
		其れでは意味がない

	解決法（現在の実装方法）
		テクスチャ、画像をImage（namespace）にロードさせて、
		そのロードしたときの画像ハンドルを取得する。
		上記により、ロード画像の重複、フライウェイト問題は解決

		SceneUIManagerも、ハンドル、ハンドルごとのTransformを持たせるだけでよくなる。

	今回の失敗
		効率や、軽量化のために実装した案が、今までの実装方法を否定するもので、
		かつ、さらに重量化してしまう間違った方法であった。
	　　最善の方法を常にかんがえ、かつ、すでに記述しているコードを見直して利用することも大事


*/
class SceneUIManager : public GameObject
{
//private 構造体
private : 


	/*
	構造体詳細	：管理UIの描画情報構造体
	構造体概要（詳しく）
				：SceneUIManagerにて管理しているUI画像のサイズを管理するためのもの
				　SceneUIManagerにて管理しているからといって、表示する画像。このテクスチャは共通のものを使用している。
				  すなわち、同じテクスチャをほかでも使っている可能性もあるということ。(Imageにて管理しているテクスチャを利用している)
				：UIごとのTransformなども管理
				：Image内へアクセスするためのハンドルを持たせておく（あくまでもUIのロードはImageに任せて、その画像を利用する）
	*/
	struct UIData
	{
		//UI画像ハンドル
			//詳細：上記の画像ハンドルは、Image(namespace)に保存してある、画像へのハンドル番号である。
			//　　：描画の際など画像へのアクセスに使用する
		int hImage;

		//UI画像描画フラグ
		bool visible;

		//UI描画Transform
		Transform transform;

		//シェーダー（推奨）
			//推奨理由：シーンマネージャーで管理する画像は、テクスチャはImageにて使用しているテクスチャ群を利用しているので、シェーダーが切り替えられてしまったら、いまのままだと自身専用のシェーダーを使う方法がない。
			//　　　　：SceneUIManagerにて管理している画像一つ一つに専用シェーダー持たせておくべき

		//コンストラクタ
		UIData();
	};



//private メンバ変数、ポインタ、配列
private : 

	//α値
		//詳細：SceneUIManager内管理のすべてのUIへ適用するα値
		//　　：共通している理由は、ゲーム中に登場するすべてのUIを1つのSceneUIManagerにて管理するわけではない。
		//　　：つまり、SceneUIManagerは、グループ化できるUIをまとめておくものである。そのため、グループ化するUIは全て同じα値でよいと考える。
		//　　：今後：共通で使用するが、共通でない場合は、以下のUI情報群に個別でα値を管理する
		//　　：　　　現段階では、α値を個別に管理する必要がないため、共通のα値を使用する
	float alpha_;

	//UI画像群
		//詳細：SceneUIManagerにて管理するUI群 
		//　　：外部へは、Vector配列に登録した順のハンドルにてアクセスする
		//制限：途中解放は不可能
	std::vector<UIData*> pArraySceneUIs_;


public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	SceneUIManager(GameObject* parent);


	//デストラクタ
		//引数：なし
		//戻値：なし
	~SceneUIManager() override;

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public :

	/*移動関連********************************************************************************************/
			/*
				UI画像を登録した際に、
				UI画像の登録ハンドルを戻値として受け取る

				ハンドルにてUI画像にアクセスする
				（Image(namespace)へのハンドル番号ではない）
			*/

	//指定された画像データのTransfromをセットする
		//詳細：トランスフォームの位置、サイズ、拡大率を受け取る
		//引数：UI画像へのハンドル
		//引数：セットするトランスフォーム
		//戻値：なし
	void SetTransform(int handle, Transform& transform);
	//指定された画像データのTransfromをセットする
		//詳細：トランスフォームの位置、サイズ、拡大率を受け取る
		//引数：UI画像へのハンドル
		//引数：セットするトランスフォーム
		//戻値：なし
	void SetTransform(int handle, Transform* transform);
	//ピクセル単位の移動（引数ピクセル分移動）
		//引数：UI画像へのハンドル
		//引数：移動ピクセル位置　X
		//引数：移動ピクセル位置　Y
		//戻値：なし
	void MovePixelPosition(int handle, int x, int y);

	//引数ピクセル位置に移動
		//詳細：引数位置をワールド座標に変換し　その座標をTransformにする
		//引数：UI画像へのハンドル
		//引数：セットピクセル位置　X
		//引数：セットピクセル位置　Y
	void SetPixelPosition(int handle, int x, int y);

	//UI画像ピクセルサイズの変更
		//詳細：ピクセル感覚に縮小拡大
		//　　：→画像を、引数ピクセルの大きさに変更(引数ピクセルをもとに、Transform値の倍率で表す　1.0f = 1倍)
		//引数:UI画像へのハンドル
		//引数:サイズピクセル　X
		//引数:サイズピクセル　Y
		//戻値：なし
	void SetPixelScale(int handle, int scaleX, int scaleY);

	//画像の回転
		//引数：UI画像へのハンドル
		//引数：セット回転値
		//戻値：なし
	void SetRotate(int handle, XMVECTOR setRotate);

	//奥に一つ下げる
		//詳細：UI画像の前後関係を付けるために、UI画像を一定値下げる必要がある。
		//　　：1つといっても、TransformのZを 0.001ほど奥に移動させる
		//引数：ボタンへのハンドル
		//戻値：なし
	void LowerOneStep(int handle);


	//画像数合計を取得
		//詳細：SceneUIManagerにて管理しているUIの個数（pArraySceneUI_.size()）
		//引数：なし
		//戻値：UI画像数（pArraySceneUI_.size()）
	int GetUINum();



public : 
	/*追加********************************************************************************************/

	//UI画像の追加
		//詳細：SceneUIManagerにて管理しているのUI画像群へ追加
		//引数：画像ファイル名
		//戻値：可変長配列に登録されたときの登録番号（ハンドル）（pArraySceneUIs_へのハンドル）
	int LoadAndAddStack(const std::string& FILE_NAME);


	//UI画像の追加
		//詳細：テクスチャバッファからテクスチャを画像としてImageに追加、そしてSceneUIManagerの管理下へ
		//引数：テクスチャ（描画が可能なテクスチャ限定）のバッファー
		//戻値：可変長配列に登録されたときの登録番号（ハンドル）（pArraySceneUIs_へのハンドル）
	int LoadAndAddStack(ID3D11Texture2D* pTexture);


	/*描画関連********************************************************************************************/


	//指定ハンドルのUI画像描画
		//引数：UI画像へのハンドル
		//戻値：なし
	void VisibleSceneUI(int handle);
	//指定ハンドルのUI画像非描画
		//引数：UI画像へのハンドル
		//戻値：なし
	void InvisibleSceneUI(int handle);
	
	//指定ハンドルのUI画像の描画、非描画反転
		//詳細：現在描画中なら、非描画へ。非描画中なら、描画へ。
		//引数：UI画像へのハンドル
		//戻値：なし
	void VisibleInvertSceneUI(int handle);


	//全UI画像の描画、非描画反転
		//詳細：現在描画中なら、非描画へ。非描画中なら、描画へ。
		//引数：なし
		//戻値：なし
	void VisibleInvertAllSceneUI();
	//全UI画像描画
		//引数：なし
		//戻値：なし
	void VisibleAllSceneUI();
	//全UI画像非描画
		//引数：なし
		//戻値：なし
	void InvisibleAllSceneUI();

	//α値セット
		//詳細：SceneUIManager全体のUI画像のα値指定
		//制限：現在、UI画像個別へのα値設定は行えない
		//引数：指定α値(0.f ~ 1.f)
		//戻値：なし
	void SetAlphaSceneUI(float alpha);




};
