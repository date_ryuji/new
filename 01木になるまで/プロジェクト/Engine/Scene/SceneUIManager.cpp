#include "SceneUIManager.h"					//ヘッダ
#include "../Image/Image.h"					//画像クラス
#include "../DirectX/Texture.h"				//テクスチャクラス
#include "../../CommonData/GlobalMacro.h"	//共通マクロ


//コンストラクタ
SceneUIManager::SceneUIManager(GameObject * parent)
	: GameObject(parent, "SceneUIManager"),
	alpha_(1.0f)
{
}

//デストラクタ
SceneUIManager::~SceneUIManager()
{
}

//初期化
void SceneUIManager::Initialize()
{
}

//更新
void SceneUIManager::Update()
{
}

//描画
void SceneUIManager::Draw()
{
	/*
	//可変長配列に登録されたテクスチャ群を回して、
		//描画フラグが立っているものだけ、描画
		//描画時、自身が所有しているhUISource_によって、Spriteにアクセスし、
			//そのSpriteの優先して描画するテクスチャを変更して、描画を実行
	*/

	//ファイルごとに画像をImageにロードする形に変更したため
		//可変長配列に登録されている、画像ハンドルをもとに
		//描画先の画像へアクセスし、描画を行わせる
	//条件：管理UI画像群分
	for (int i = 0; i < pArraySceneUIs_.size(); i++)
	{
		//条件：描画中か
		if (pArraySceneUIs_[i]->visible)
		{
			//α値のセット
			Image::SetAlpha(pArraySceneUIs_[i]->hImage , alpha_);
			
			//登録Transformのセット
			Image::SetTransform(pArraySceneUIs_[i]->hImage, pArraySceneUIs_[i]->transform);
			
			//描画
			Image::Draw(pArraySceneUIs_[i]->hImage);
		}
	}	
}

//解放
void SceneUIManager::Release()
{
	//可変長配列の動的確保データの解放
	//条件：管理UI画像群分
	for (int i = 0; i < pArraySceneUIs_.size(); i++)
	{
		//可変長配列の要素解放
		SAFE_DELETE(pArraySceneUIs_[i]);
	}
	//可変長配列の解放
	pArraySceneUIs_.clear();
}

//UI画像の追加
int SceneUIManager::LoadAndAddStack(const std::string& FILE_NAME)
{
	//データの構造体の確保
	UIData* pData = new UIData;

	//Imageを使用して
	//ロード
	//ロードした際のハンドル番号を取得する
		//自身の画像にアクセスする際に使用する
		//重複ロードチェックは、ImageのLoad先にて管理するため、以下で行う必要はない
	pData->hImage = Image::Load(FILE_NAME);
	//ロードできたかの警告チェック
	assert(pData->hImage != -1);

	//自身のメンバの可変配列に追加
	pArraySceneUIs_.push_back(pData);

	//ハンドル番号＝現在追加したときの要素数　−１
	return (int)pArraySceneUIs_.size() - 1;
}

//UI画像の追加
int SceneUIManager::LoadAndAddStack(ID3D11Texture2D* pTexture)
{
	//テクスチャのバッファーを渡し
	//テクスチャのバッファの画像をImageの管理下に置く


	//データの構造体の確保
	UIData* pData = new UIData;

	//Imageを使用して
	//ロード
	//ロードした際のハンドル番号を取得する
		//自身の画像にアクセスする際に使用する
		//重複ロードチェックは、ImageのLoad先にて管理するため、以下で行う必要はない
	//引数：ファイル名(自身で作成したテクスチャバッファであるため、本来は、ファイル名など存在しないが、登録するうえでは必要になるので、以下のバッファーを識別する名前という意味で持たせておく（他と被らないように注意）)
	//引数：バッファー
	pData->hImage = Image::Load("NewTexture" ,  pTexture);
	//ロードできたかの警告チェック
	assert(pData->hImage != -1);

	//自身のメンバの可変配列に追加
	pArraySceneUIs_.push_back(pData);

	//ハンドル番号＝現在追加したときの要素数　−１
	return (int)pArraySceneUIs_.size() - 1;

}


//指定ハンドルのUI画像描画
void SceneUIManager::VisibleSceneUI(int handle)
{
	pArraySceneUIs_[handle]->visible = true;
}

//指定ハンドルのUI画像非描画
void SceneUIManager::InvisibleSceneUI(int handle)
{
	pArraySceneUIs_[handle]->visible = false;
}

//指定ハンドルのUI画像の描画、非描画反転
void SceneUIManager::VisibleInvertSceneUI(int handle)
{
	pArraySceneUIs_[handle]->visible = !(pArraySceneUIs_[handle]->visible);
}

//全UI画像の描画、非描画反転
void SceneUIManager::VisibleInvertAllSceneUI()
{
	//条件：管理UI画像群分
	for (int i = 0; i < pArraySceneUIs_.size(); i++)
	{
		VisibleInvertSceneUI(i);
	}
}

//全UI画像描画
void SceneUIManager::VisibleAllSceneUI()
{
	//条件：管理UI画像群分
	for (int i = 0; i < pArraySceneUIs_.size(); i++)
	{
		VisibleSceneUI(i);
	}
}


//全UI画像非描画
void SceneUIManager::InvisibleAllSceneUI()
{
	//条件：管理UI画像群分
	for (int i = 0; i < pArraySceneUIs_.size(); i++)
	{
		InvisibleSceneUI(i);
	}
}

//α値セット
void SceneUIManager::SetAlphaSceneUI(float alpha)
{
	alpha_ = alpha;
}

//指定された画像データのTransfromをセットする
void SceneUIManager::SetTransform(int handle , Transform & transform)
{
	pArraySceneUIs_[handle]->transform = transform;
}

//指定された画像データのTransfromをセットする
void SceneUIManager::SetTransform(int handle, Transform * transform)
{
	SetTransform(handle, (*transform));
}

//ピクセル単位の移動（引数ピクセル分移動）
void SceneUIManager::MovePixelPosition(int handle, int x, int y)
{
	//Image名前空間の変換関数を用いて
		//ピクセル値をもとに、Transformを変換する


	//関数先にて、
		//引数にて渡した参照受け取りのTransformを変換して値を入れて返してくれる
	Image::MovePixelPosition(pArraySceneUIs_[handle]->transform, x, y);

}

//引数ピクセル位置に移動
void SceneUIManager::SetPixelPosition(int handle, int x, int y)
{
	Image::SetPixelPosition(pArraySceneUIs_[handle]->transform, x, y);
}

//UI画像ピクセルサイズの変更
void SceneUIManager::SetPixelScale(int handle, int x, int y)
{
	//Imageクラスより
	//テクスチャの標準のサイズを確保
	XMVECTOR defaultTexSize =
		Image::GetDefaultTextureSize(pArraySceneUIs_[handle]->hImage);

	//関数先にて、第一引数へ、サイズを可変した後のTransform値を入れてもらう
	Image::SetPixelScale(pArraySceneUIs_[handle]->transform, 
		defaultTexSize, x, y);
}

//画像の回転
void SceneUIManager::SetRotate(int handle, XMVECTOR setRotate)
{
	pArraySceneUIs_[handle]->transform.rotate_ = setRotate;
}

//奥に一つ下げる
void SceneUIManager::LowerOneStep(int handle)
{
	//画像のグラフィック的な見た目の変化がわかりずらい程度に奥行きを与える
	pArraySceneUIs_[handle]->transform.position_.vecZ += 0.001f;
}

//画像数合計を取得
int SceneUIManager::GetUINum()
{
	return (int)pArraySceneUIs_.size();
}

//構造体　コンストラクタ
SceneUIManager::UIData::UIData() :
	hImage(-1),
	visible(false)
{
}
