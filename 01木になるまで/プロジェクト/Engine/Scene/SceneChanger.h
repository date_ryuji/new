#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス
#include "../../CommonData/SceneType.h"	//シーンごとの識別IDを保存したヘッダ




//クラスのプロトタイプ宣言
class SceneManagerParent;


/*
	ゲームプログラムの構造

	WinMain(メッセージループ)　
	→　RootJob(GameObject継承)　
		→ SceneChanger(GameObject継承)　
		→　各Scene(GameObject継承)　
		→　各シーン内オブジェクト(GameObject継承)

*/


/*
	クラス詳細	：全てのシーンのインスタンスを生成するクラス
				：シーン切り替えを行うクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	クラス概要（詳しく）
				：初めのシーンの作成
				：シーン切り替えが実行されたときに、前回シーンのモデルデータ、画像データ、音楽データ、全オブジェクトの解放（前回シーンの解放）。
				　　　　　　　　　　　　　　　　　　次回シーンのインスタンスの生成。（シーンのGameObjectであるため、インスタンスを生成してしまえば、あとの処理は任せることができる）

				：シーン切り替えはどのクラスからも呼び込むことが可能である。

*/
class SceneChanger : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 
	/*シーンオブジェクト******************************************************************/
	//現在のシーンID
	SCENE_ID currentSceneID_;	
	//次のシーンID
	SCENE_ID nextSceneID_;		
		/*
			シーン切り替え方法

			・シーンが同じ場合、currentSceneID_ , nextSceneID_に同様のIDが入る。
			�@シーン切り替えのChangeScene（）を呼び込むことで、nextSceneID_が切り替わる。
			�ASceneChangerのUpdateにて、currentSceneID_ , nextSceneID_2つの比較。
			�B違う場合、currentSceneID_のインスタンスの解放（モデルデータなどのリソースファイルも）
			　	　　　　nextSceneID_のシーンオブジェクトのインスタンス生成
			　同じ場合、何もしない

			シーン切り替え終了
		*/

	//現在のシーンのオブジェクトポインタ
	GameObject* pCurrentScene_;	

	/*シーンマネージャーオブジェクト******************************************************************/
	//現在のシーンマネージャーID
	SCENE_MANAGER_ID currentSceneManagerID_;	
	//現在のシーンマネージャーのオブジェクトポインタ
		//詳細：シーンクラスから直接セットしてもらう
		//　　：シーン内の連携、管理を行うマネージャークラス
		//制限：マネージャークラスは必ず存在するわけではない
		//　　：存在する場合は、シーンのInitializeにて、セットする関数を呼び込む
	SceneManagerParent* pCurrentSceneManager_;	

//private メソッド
private : 
	
	//シーンのポインタをリセットする
		//詳細：メンバのシーンオブジェクトポインタをnullptrにする
		//引数：なし
		//戻値：なし
	void ResetScene();

	//シーンオブジェクトのインスタンス生成
		//詳細：引数シーンのインスタンスを生成する
		//引数：切り替え先のシーン識別ID
		//戻値：なし
	void CreateScene(SCENE_ID sceneID);

//public ゲームループメソッド
public : 

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	SceneChanger(GameObject* parent);
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~SceneChanger() override;

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//シーン切り替えを宣言する関数（次のフレームにてシーンは切り替えられるようにする）
		//引数：切り替え先のシーン識別ID
		//戻値：なし
	void ChangeScene(SCENE_ID changeScene);

	//シーンマネージャーをセットする
		//引数：シーンマネージャーを識別するタイプID
		//引数：シーンマネージャーのインスタンスのポインタ
		//戻値：なし
	void SetSceneManager(SCENE_MANAGER_ID sceneManagerID, SceneManagerParent* pSceneManagerParent);

	//シーンマネージャーのIDとポインタをリセットする
		//詳細：基本的に、シーンマネージャーのIDとポインタをセットしたオブジェクトが直接以下の関数を呼び出す
		//引数：なし
		//戻値；なし
	void ResetSceneManager();


	//シーンオブジェクトを取得する
		//詳細：現在の自身（SceneChanger）の第一の子供（子供リスト一番目）は、シーンのオブジェクトのため、そのオブジェクトを取得する
		//　　：乱用防止のため、引数にて渡されたシーン識別IDと現在のシーンが同様であるならば、シーンのオブジェクトを返す	
		//制限：しかし、この処理を行うのであれば、FindObjectで全GameObjectから走査する際の処理数と変わらない
		//　　：なぜならば、まず、SceneChangerから以下の関数を呼び出すとしても、まずは、SceneChangerをFindする必要がある。
		//　　：そして、仮にSceneChangerを見つけたとして、
		//　　：SceneChangerの一つ下の子供を引き続き走査して探すのと、
		//　　：SceneChangerの関数を呼び出して、子供を貰うのとでは、
		//　　：どちらも処理数的には変わらない、（確認はしていないが、前者の方が処理数は早い可能性もある）

		//　　：→しかし、そのうえでも以下の関数を用いるのは、
		//　　：　　・現在、動いているシーンであるかを、タイプIDで識別した上で、Enumでの判別になる
		//　　：　　・SceneChangerから直接、シーンオブジェクトを貰うことが出来ていると、視認できる（分かりやすい）

		//　　：以上のことから、文字列でFindして、探すよりも、きちんとシーンのオブジェクトを指定して（Enum判別）取得している感が強いので、以下の関数でシーンオブジェクトを貰うようにする
		//引数：シーン識別ID
		//戻値：オブジェクトポインタ（現シーンが引数IDと正しい：シーンオブジェクトポインタ、正しくない：nullptr）
	GameObject* GetCurrentScene(SCENE_ID sceneID);






	//シーンマネージャーを取得する
		//詳細：GetCurrentSceneと同様の処理で、SceneManagerを取得する
		//　　：引数にて受け取った、シーンマネージャーを識別するタイプIDとSceneChangerにて管理しているタイプIDが同様であれば、インスタンスのポインタを渡す
		//　　：取得先で、シーンマネージャーの継承先のクラスでダウンキャストを行う予定なので、その際に、ダウンキャストするクラス型と合わない場合、参照先が存在しなくなるため、その危機を避けるため引数を付ける
		//引数：シーンマネージャーを識別するタイプID
		//戻値：シーンマネージャーのインスタンスのポインタ（現マネージャーが引数IDと正しい：シーンマネージャーオブジェクトポインタ、正しくない：nullptr）
	SceneManagerParent* GetCurrentSceneManager(SCENE_MANAGER_ID sceneManagerID);

};



/***********************************************************************************************************************************/
/*
	クラス詳細	：シーンオブジェクトを作成するクラス　ファクトリークラス

	制限：シーン数が多くなった場合、ファイルを変える必要アリ
	デザインパターン：FactoryMethod
*/
class SceneFactory
{
//public メソッド
public:
	//コンストラクタ
		//引数：なし
		//戻値：なし
	SceneFactory();

	//シーンマネージャーのインスタンスを作成し、インスタンスを返す
		//引数：シーンのタイプID
		//引数：インスタンスを生成した際の親オブジェクト
		//戻値：生成インスタンス
	GameObject* CreateScene(SCENE_ID type, GameObject* pParent);

};


