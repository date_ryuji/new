#include "SceneManagerFactory.h"			//ヘッダ
#include "../GameObject/GameObject.h"		//ゲームオブジェクト
#include "SceneChanger.h"					//シーンチェンジャー
#include "SceneManagerParent.h"				//シーンマネージャー　継承元クラス

//継承先のオブジェクト
#include "../../GameScene/Scene/GameSceneManager.h"
#include "../../SplashScene/Scene/SplashSceneManager.h"
#include "../../TitleScene/Scene/TitleSceneManager.h"
#include "../../ResultScene/Scene/ResultSceneManager.h"
#include "../../WinningScene/Scene/WinningSceneManager.h"
#include "../../GameOverScene/Scene/GameOverSceneManager.h"


//コンストラクタ
SceneManagerFactory::SceneManagerFactory()
{
}

//シーンマネージャーのインスタンスを作成しインスタンスを返す
SceneManagerParent * SceneManagerFactory::CreateSceneManager(SCENE_MANAGER_ID type, GameObject * pParent)
{
	//引数タイプで指定して
	//生成して返す

	//引数のタイプから生成するオブジェクトの型を選択し
	//親を引数のpParentとする
	switch (type)
	{
	case SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAME : 
		return (SceneManagerParent*)Instantiate<GameSceneManager>(pParent);
	case SCENE_MANAGER_ID::SCENE_MANAGER_ID_SPLASH :
		return (SceneManagerParent*)Instantiate<SplashSceneManager>(pParent);
	case SCENE_MANAGER_ID::SCENE_MANAGER_ID_TITLE:
		return (SceneManagerParent*)Instantiate<TitleSceneManager>(pParent);
	case SCENE_MANAGER_ID::SCENE_MANAGER_ID_WINNING:
		return (SceneManagerParent*)Instantiate<WinningSceneManager>(pParent);
	case SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAMEOVER:
		return (SceneManagerParent*)Instantiate<GameOverSceneManager>(pParent);

	case SCENE_MANAGER_ID::SCENE_MANAGER_ID_RESULT:
		return (SceneManagerParent*)Instantiate<ResultSceneManager>(pParent);


	default : 
		return nullptr;
	}


	return nullptr;
}
