#pragma once
#include "../../CommonData/SceneType.h"	//シーンを識別するIDを保存したヘッダ

//クラスのプロトタイプ宣言
class GameObject;
class SceneManagerParent;



/*
	クラス詳細	：シーンマネージャーオブジェクトを作成するクラス　ファクトリークラス
	クラス詳細（詳しく）
				：マネージャークラスのインスタンスを生成する
	制限：シーンごとにマネージャークラスが存在するとは限らない。
	デザインパターン：FactoryMethod
*/
class SceneManagerFactory
{
//public メソッド
public:
	//コンストラクタ
		//引数：なし
		//戻値：なし
	SceneManagerFactory();


	//シーンマネージャーのインスタンスを作成しインスタンスを返す
		//引数：シーンマネージャーID
		//引数：シーンマネージャーの親オブジェクト
	SceneManagerParent* CreateSceneManager(SCENE_MANAGER_ID type, GameObject* pParent);
};


