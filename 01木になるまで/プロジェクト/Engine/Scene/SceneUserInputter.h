#pragma once
//システム内　標準ヘッダ(usr/)
#include <list>		//リストを使用するためにインクルード（途中で中身を消去する可変長配列）						
#include <map>		//検索マップ　キーとバリューにて値の格納検索を行う
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス
#include "../DirectX/Input.h"			//デバイス入力受付　クラス

//クラスのプロトタイプ宣言
class SceneInputStrategyParent;
class FactoryForSceneInputStrategyParent;

//マウスコードタイプ
	//詳細：マウス入力時の入力タイプ
enum class MOUSE_CODE
{
	MOUSE_BUTTON_LEFT = 0 ,		//マウスクリック入力　左
	MOUSE_BUTTON_RIGHT ,		//マウスクリック入力　右
	MOUSE_BUTTON_CENTER ,		//マウスクリック入力　中

};

//入力受け付けタイプ
	//詳細：入力を受け付ける条件
	//　　：デバイスの入力行動の内、どの行動を入力と判断するかのタイプ
enum class KEY_AND_MOUSE_INPUT_PUSH_TYPE
{
	INPUT_ALWAYS = 0,	//押し続ける
	INPUT_DOWN ,		//初めて押す　DOWN
	INPUT_UP ,			//初めて離す　UP
	INPUT_MAX ,			//MAX
};

//デバイス識別タイプ
	//詳細：キーボードデバイス、マウスデバイスの入力か
enum class INPUT_TYPE_KEY_OR_MOUSE
{
	INPUT_TYPE_KEY = 0 ,	//キーボードデバイス
	INPUT_TYPE_MOUSE ,		//マウスデバイス
	INPUT_TYPE_NONE ,		//どのデバイスにも属さない
};


/*
	構造体詳細	：デバイス入力情報　入力時の行動をまとめた構造体
	構造体概要（詳しく）
				：入力情報と
				　入力時の行動をまとめた構造体


*/
struct InputInfo
{
	//行動
	SceneInputStrategyParent* pInputStrategy;

	//入力タイプ
	KEY_AND_MOUSE_INPUT_PUSH_TYPE inputType;

	//コンストラクタ
	InputInfo(
		SceneInputStrategyParent* pStrategy , 
		KEY_AND_MOUSE_INPUT_PUSH_TYPE type) :
		pInputStrategy(pStrategy),
		inputType(type)
	{

	};

};




/*
	クラス詳細	：シーン内のキー入力受付と　入力時の行動実行呼び出しクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	クラス概要（詳しく）
				：デバイスによる入力を受け付けるクラス
				：シーンにおけるデバイス入力はSceneUserInputter以外での受け付けは行わない（例外在り）
					：例外：ボタンの押下判定
					：　　：マウスによる視点移動
					：　　：マウスカーソルの1ピクセル単位での移動
				：入力コード（キーコード、マウスコード）と入力確定時の実行行動を登録し、
				：入力を受け付けた時、登録行動を実行




*/
/*
	SceneUserInputter実装により不要になるクラスファイル

	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	//
	//SceneUserInputterにより
	//以下のクラスが必要なくなる
	//	○○SceneGameInputter
	//
	//伴って、
	//すべての機能を上記のクラスへ移行する
	//	
	//
	//移行完了
	//★マークを関数や要素に追加していく
	//
	//
	//
	//
	//
	//移行することでのメリットデメリット
	//メリット
	//	各シーンごとに無駄なクラスを作らなくて済む
	//デメリット
	//	マウス入力とキー入力を個別に識別する必要が出たため、
	//	入力コードを保存しておくリストなどが増えた
	//変わらないこと
	//	行動クラスを、切り替える際に、毎回行動クラスを動的確保すること
	//
	//
	//★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★


*/
class SceneUserInputter : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 

	/*入力コード*****************************************************************************************************/
		/*
		　入力受付の判別に使用
		*/
	//ユーザー入力コード（キーボード）
		//詳細：別名：常駐のキーコード
		//　　：各シーンごとに（SceneUserInputterクラスごと）キー入力のコードを登録する
		//　　：登録コード以外の入力受付は行われない
	std::list<int>* pKeyCodes_;
	//フレーム内の消去を指示された入力コード（キーボード）
		//詳細：別名：常駐のマウスコード
		//　　：消去依頼を受けたコードを登録し、フレームの最初に消去を行う
		//　　：消去依頼コードをまとめておき、直ぐに消去を行わない理由
		//　　：今回のフレームで消去を行ってしまうと、不具合が生じる（現在参照しているコードを消去したり　など）
		//　　：そのため、フレームを保存しておいて、次のフレームの開始時に、削除を行うようにする
	std::list<int>* pEraseKeyCodes_;
	
	//ユーザー入力コード（マウス）
		//詳細：各シーンごとに（SceneUserInputterクラスごと）マウス入力のコードを登録する
		//　　：登録コード以外の入力受付は行われない
	std::list<MOUSE_CODE>* pMouseCodes_;
	//フレーム内の消去を指示された入力コード（マウス）
		//詳細：存在理由はpEraseKeyCodes_　コメント参照
	std::list<MOUSE_CODE>* pEraseMouseCodes_;



	/*入力コードにおける行動クラスのマップ*****************************************************************************************************/
		/*
		　入力確定を受けて、入力時の行動実行の際に使用
		*/

	//キーコードと入力時の行動をまとめたマップ
		//詳細：マップとして、
		//　　：キーコードをキー（マップのKey）として、入力時の行動をまとめる
		//　　：キーが押されたら、キーコード（Key）でマップを検索し、（Valueとして）行動クラスを取得、行動クラスの行動を実行させる（入力時の行動実行）
	std::map<int, InputInfo*>* pKeyMap_;
	//フレーム内のキーコード更新を指示されたマップ
		//詳細：次回のフレームにて、
		//　　：下記のマップの情報をpKeyMap_に追加する
		//　　：更新を同フレームにて行わないのは、入力受付実行中に更新が行われてしまうと参照中の更新、新たな入力受付を行ってしまい、直ぐに入力結果を行ってしまうことにもつながるため
	std::map<int, InputInfo*>* pNextKeyMap_;

	//マウスコードと入力時の行動をまとめたマップ
		//詳細：pKeyMap_と同文
	std::map<MOUSE_CODE, InputInfo*>* pMouseMap_;
	//フレーム内のマウスコード更新を指示されたマップ
			//詳細：pNextKeyMap_と同文
	std::map<MOUSE_CODE, InputInfo*>* pNextMouseMap_;

//private メソッド
private : 

	/*入力コードの更新***********************************************************************************************************/
	//入力コードの更新（前フレームに更新依頼されたコードを更新）
		//詳細：pNextKeyMap_ , pNextMouseMap_ の更新（pKeyMap_ , pMouseMap_への更新）
		//　　：前フレーム内にて、追加依頼された入力コードを入力キー群に登録させる
		//　　：　　：追加を前フレーム内で解決させない理由は、
		//　　：　　：キー入力を受け付けて、その入力中に解放されてしまうとエラーの原因となる
		//　　：　　：キー入力先の行動の中で、キー入力を解放する行動が必要になる。
		//　　：　　：しかし、キー入力先の行動自身も開放する処理をすると、現在参照しているにもかかわらず、自身を解放するという自体が生じる。関数が戻ることができない（呼び込みもとへ）。
		//引数：なし
		//戻値：なし
	void FrameUpdateMap();

	//入力コードの削除（前フレームに削除依頼をされたコードを削除）
		//詳細：pEraseKeyCodes_ , pEraseMosueCodes_の更新（pKeyCodes_ , pMouseCodes_ + pKeyMaps_ , pMouseMaps_のへ更新）
		//　　：前フレーム内にて、消去依頼されたコードの削除
		//引数：なし
		//戻値：なし
	void FrameEraseCode();


	/*入力コードへの追加***********************************************************************************************************/
	
	//常駐のキーコード(pKeyCode)へ追加
		//詳細：外部から参照できるRequestAddKeyCodeでは、
		//　　：次のフレームにアクセスできるようになるキーコード(pNextMap)が追加できる
		//　　：以下の関数では、その追加したキーコード（pNextMap）を実際にアクセスできるものへ(pKeyMap , pMouseMap)へ追加する処理が書かれている
		//    ：以下の関数により、入力コードの確定決定
		//引数：キーコード(int(DIK_~~))
		//引数：入力情報（デバイス情報＋入力時の行動）
		//戻値：なし
	void AddResidentKeyCode(int keyCode, InputInfo* pInputInfo);

	//常駐マウスコード(pMouseCode)へ追加
		//詳細：AddResidentKeyCodeのマウスVer
		//引数：マウスコード(MOUSE_CODE(enum))
		//引数：入力情報（デバイス情報＋入力時の行動）
		//戻値：なし
	void AddResidentMouseCode(MOUSE_CODE mouseCode, InputInfo* pInputInfo);

	//常駐のキーコード(pKeyCode)から削除
		//詳細：pKeyCode　からのキーコード削除
		//　　：削除されたコードには今後アクセスされない。
		//引数：キーコード(int(DIK_~~))
		//戻値：なし
	void EraseResidentKeyCode(int KeyCode);
	//常駐のマウスコード(pMouseCode)から削除
		//詳細：pMouseCode　からのマウスコード削除
		//　　：削除されたコードには今後アクセスされない。
		//引数：マウスコード(MOUSE_CODE(enum))
		//戻値：なし
	void EraseResidentMouseCode(MOUSE_CODE mouseCode);


	//常駐キーコード(pKeyCode)の入力判断
		//詳細：常駐のキーコード(pKeyCode)が入力されたかの判別を行う（入力受付）
		//　　：登録された全てのコードを判別	
		//　　：入力が認められたら：コードと一緒に登録した　入力時の行動を実行
		//　　：入力が認められない：何もしない
		//引数：なし
		//戻値：なし
	void KeyInputJudgement();
	//常駐キーコードの入力を受けて　行動アルゴリズム実行
		//詳細：一つ一つのキー入力を受け、入力されたキーコードが存在した場合
		//　　；キーコードと同時に登録した、入力時の行動実行クラス。そのクラスの行動実行メソッド呼び込み
		//引数：入力コードへのイテレータ(pKeyCodeへのイテレータ)
		//戻値：なし
	void ExecuteKeyInputtedAlgorithm(std::list<int>::iterator& itr);

	//常駐マウスコード(pMouseCode)の入力判断
		//詳細：KeyInputJudgementのマウスVer
		//引数：なし
		//戻値：なし
	void MouseInputJudgement();
	//常駐マウスコードの入力を受けて　行動アルゴリズム実行
		//詳細：ExecuteKeyInputtedAlgorithmのマウスVer
		//引数：入力コードへのイテレータ(pMouseCodeへのイテレータ)
		//戻値：なし
	void ExecuteMouseInputtedAlgorithm(std::list<MOUSE_CODE>::iterator& itr);

	//常駐キーコード（pKeyCode）をすべて空へ
		//詳細：全てのキーコード(pKeyCodeの要素)の解放	
		//引数：なし
		//戻値：なし
	void SetEmptyAllKeyCode();
	//常駐マウスコード（pMouseCode）をすべて空へ
		//詳細：全てのマウスコード(pMouseCodeの要素)の解放	
		//引数：なし
		//戻値：なし
	void SetEmptyAllMouseCode();


//public ゲームループメソッド
public : 


	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	SceneUserInputter(GameObject* parent);

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 


	//キーコード登録(pKeyCode)　依頼
		//詳細：
		/*
			登録手順
			・AddKeyCodeの引数として
			・第一引数へ、キーコードを代入
			・第二引数の構造体用意
				・各シーンごとのストラテジー生成クラスを生成
				・キー入力タイプを代入
				・各シーンごとのストラテジーを識別するenumから、生成クラスへそのenum値を送り、ストラテジークラス生成
			・第二引数へ、構造体を代入

		*/
		/*
				//キー入力と
				//キー入力時の行動アルゴリズムのセット
				//自クラスの　”次のフレームで切り替える、キーごとの行動アルゴリズム群”に
				//引数の行動アルゴリズムタイプをセットして
				//次のフレームで、実際にインスタンスを生成して切り替える

		*/
		//引数：キーコード（DIK_~~）
		//引数：入力情報（入力デバイス+入力時の行動）
		//戻値：なし
	void RequestAddKeyCode(int keyCode , InputInfo* pInputInfo);
	//マウスコード登録(pMouseCode)　依頼
		//詳細：RequestAddKeyCodeのマウスVer		
		//引数：マウスコード（MOUSE_CODE(enum)）
		//引数：入力情報（入力デバイス+入力時の行動）
		//戻値：なし
	void RequestAddMouseCode(MOUSE_CODE mouseCode , InputInfo* pInputInfo);

	//指定キーコード（pKeyCode）を空にする　依頼
		//詳細：指定キーコードを削除の依頼
		//　　：同フレーム内にて消去を実行せず、次のフレームにて消去。そのための消去リストへ追加
		//引数：キーコード（DIK_~~）
		//戻値：なし
	void RequestSetEmptyKeyCode(int keyCode);
	//指定キーコード（pMouseCode）を空にする　依頼
		//詳細：RequestSetEmptyKeyCodeのマウスVer
		//引数：マウスコード（MOUSE_CODE(enum)）
		//戻値：なし
	void RequestSetEmptyMouseCode(MOUSE_CODE mouseCode);

	//常駐コード(pKeyCode , pMouseCode)のすべてのコードを空にする
		//詳細：キーコード、マウスコードともに空にする
		//引数：なし
		//戻値：なし
	void RequestSetEmptyAllCode();


};

