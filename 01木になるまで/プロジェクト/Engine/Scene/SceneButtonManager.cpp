#include "SceneButtonManager.h"							//ヘッダ
#include "../Button/Button.h"							//ボタン
#include "../../CommonData/GlobalMacro.h"				//共通マクロ
#include "../../GameScene/CountTimer/CountTimer.h"		//カウントタイマー
#include "ButtonStrategyParent.h"						//ボタン押下時の行動クラス　ストラテジーの親クラス




//コンストラクタ
SceneButtonManager::SceneButtonManager(GameObject * parent)
	: GameObject(parent, "SceneButtonManager") , 
	pCountTimer_(nullptr)
{
	//可変長配列のクリア
	pArraySceneButton_.clear();
}

//デストラクタ
SceneButtonManager::~SceneButtonManager()
{
}

//初期化
void SceneButtonManager::Initialize()
{
	//カウントタイマーのインスタンス生成
	pCountTimer_ =  (CountTimer*)Instantiate<CountTimer>(this);
}

//更新
void SceneButtonManager::Update()
{
}
//描画
void SceneButtonManager::Draw()
{
}

//解放
void SceneButtonManager::Release()
{
	//動的確保した情報を解放
	//条件：管理ボタン数分
	for (int i = 0; i < pArraySceneButton_.size(); i++)
	{
		//ストラテジーの解放
			//ストラテジーは、動的確保したインスタンスを確保していて、解放する人がほかに誰もいない。
			//このインスタンスを知っている人がほかにいないので、ここで削除
		SAFE_DELETE(pArraySceneButton_[i]->pStrategy);

		//管理構造体の解放
		SAFE_DELETE(pArraySceneButton_[i]);
	}
	//クリア
		//要素のボタンインスタンスなどは解放しない
		//ほかのクラスでInstantiateされたクラスなので、ここでDelete解放するとエラーになる
	pArraySceneButton_.clear();
}

//Transformをセット
void SceneButtonManager::SetTransform(int handle, Transform & transform)
{
	pArraySceneButton_[handle]->pButton->SetTransform(transform);
}
//Transformをセット
void SceneButtonManager::SetTransform(int handle, Transform * transform)
{
	SetTransform(handle, (*transform));
}

//ピクセル単位の移動（引数ピクセル分移動）
void SceneButtonManager::MovePixelPosition(int handle, int x, int y)
{
	pArraySceneButton_[handle]->pButton->MovePixelPosition(x, y);
}

//引数ピクセル位置に移動
void SceneButtonManager::SetPixelPosition(int handle, int x, int y)
{
	pArraySceneButton_[handle]->pButton->SetPixelPosition(x, y);
}

//ボタンピクセルサイズの変更
void SceneButtonManager::SetPixelScale(int handle, int x, int y)
{
	pArraySceneButton_[handle]->pButton->SetPixelScale(x, y);
}

//奥に一つ下げる
void SceneButtonManager::LowerOneStep(int handle)
{
	pArraySceneButton_[handle]->pButton->LowerOneStep();
}

//ボタンオブジェクトの追加
int SceneButtonManager::AddStack(Button* pButton, ButtonStrategyParent* pStrategy)
{
	//データの確保
	ButtonData* pData = new ButtonData;
	pData->pButton = pButton;
	pData->pStrategy = pStrategy;


	//ButtonDataのデータ軍を追加
	pArraySceneButton_.push_back(pData);
		////ストラテジーを保存領域に追加（追加する理由は、ヘッダにて）
		//pArrayButtonStrategy_.push_back(pStrategy);
	//カウントアップ
		//タイマーによる計測開始
		//ボタンごとのタイマー計測開始
	StartCountUpTimer();


	//ベクター配列における全体個数　- 1にて、配列追加時の添え字を返す
	return (int)pArraySceneButton_.size() - 1;
	
}

//ボタンごとのタイマー計測開始
void SceneButtonManager::StartCountUpTimer()
{
	//カウントアップ
		//時間計測のために、タイマーの追加
	
	//下記のタイマーは、それぞれ、ボタンが描画されてから、一定時間ボタンの押下を受け付けない時間を必要とする
		//→そのタイマーを、ボタンごとに所有しておいて、ボタン描画が確認されて、指定時間の計測が終了したら、ボタンの押下による、行動を行えるように制限をかける
		//→制限をかけなければ、ボタン描画のそのフレームにて、ボタン押下が確認されて、ボタン押下の行動をされてしまう。それを避けるために、時間による、ボタン押下許可をするための実装
	pCountTimer_->StartTimerForCountUp(0.f, PRESS_PERMISSION_START_);
		//タイマーは、タイマークラスにて、複数個確保されて、アクセスするには、連番のハンドル番号が必要となる。
		//その番号は、可変長配列における添え字の番号と同様になるため、アクセスするときは、添え字を使用する。
}

//タイマーのリスタート
void SceneButtonManager::ReStartCountUpTimer(int handle)
{
	//タイマーの再開
		//再計測
	pCountTimer_->ReStartTimerForCountUp(handle , 0.f, PRESS_PERMISSION_START_);
}
//ボタン描画
void SceneButtonManager::VisibleSceneButton(int handle)
{
	//描画フラグをあげる
	pArraySceneButton_[handle]->pButton->Visible();
	//更新開始
		//描画開始とともに、ボタン押下の判定を受け付け開始
	EnterSceneButton(handle);
}
//ボタン描画拒否
void SceneButtonManager::InvisibleSceneButton(int handle)
{
	//描画フラグを下げる
	pArraySceneButton_[handle]->pButton->Invisible();
	//更新終了
		//ボタンが描画されていないときは
		//ボタンの描画判定を取ってほしくない
	LeaveSceneButton(handle);
}

//ボタンの描画
void SceneButtonManager::VisibleSceneButton(Button * pButton)
{
	//引数のオブジェクトポインタが
		//自クラスの保存領域の配列に存在していたら
		//その添え字を返してもらい
		//なかったら、−１をもらう
	int handle = GetArrayHandle(pButton);

	//条件：ボタンが見つかったら
	if (handle != -1)
	{
		//取得したハンドルを渡して処理実行
		VisibleSceneButton(handle);
	}
}
//ボタンの描画拒否
void SceneButtonManager::InvisibleSceneButton(Button * pButton)
{
	//引数のオブジェクトポインタが
		//自クラスの保存領域の配列に存在していたら
		//その添え字を返してもらい
		//なかったら、−１をもらう
	int handle = GetArrayHandle(pButton);

	//条件：ボタンが見つかったら
	if (handle != -1)
	{
		//取得したハンドルを渡して処理実行
		InvisibleSceneButton(handle);
	}
}

//ボタンの描画、描画拒否反転
void SceneButtonManager::VisibleInvertSceneButton(int handle)
{
	//反転
	//条件：描画状態を取得
	if (pArraySceneButton_[handle]->pButton->IsVisible())
	{
		//非描画、非更新
		InvisibleSceneButton(handle);
	}
	else
	{
		//描画、更新
		VisibleSceneButton(handle);
	}
	
}

//ボタンの描画、描画拒否　反転
void SceneButtonManager::VisibleInvertSceneButton(Button * pButton)
{
	//引数のオブジェクトポインタが
	//自クラスの保存領域の配列に存在していたら
	//その添え字を返してもらい
	//なかったら、−１をもらう
	int handle = GetArrayHandle(pButton);

	//条件：ボタンが見つかったら
	if (handle != -1)
	{
		//取得したハンドルを渡して処理実行
		VisibleInvertSceneButton(handle);
	}
}

//自身の管理しているボタン全ての描画、描画拒否を反転
void SceneButtonManager::VisibleInvertAllSceneButton()
{
	//条件：管理ボタン全て
	//Buttonの表示、非表示を切り替える
	for (int i = 0; i < pArraySceneButton_.size(); i++)
	{
		//取得したハンドルを渡して処理実行
		VisibleInvertSceneButton(i);
	}

}

//自身の管理しているボタン全ての描画
void SceneButtonManager::VisibleAllSceneButton()
{
	//条件：管理ボタン全て
	//Buttonの表示へ切り替える
	for (int i = 0; i < pArraySceneButton_.size(); i++)
	{
		//表示呼び込み
		VisibleSceneButton(i);
	}
}

//自身の管理しているボタン全ての描画拒否
void SceneButtonManager::InvisibleAllSceneButton()
{
	//条件：管理ボタン全て
	//Buttonの非表示へ切り替える
	for (int i = 0; i < pArraySceneButton_.size(); i++)
	{
		//非表示呼び込み
		InvisibleSceneButton(i);
	}
}

//更新許可
void SceneButtonManager::EnterSceneButton(int handle)
{
	//更新フラグを上げる
	pArraySceneButton_[handle]->pButton->Enter();

	//カウントアップ(Re)
	//更新再開とともに、
		//更新を開始を、0.0f として、時間（現実時間）を計測する
		//終了時間を、startButtonTime_として、タイマーをカウントアップする
		
		//終了時間以上になるまで、更新フラグが立っていても、
		//ボタン押下時の行動は実行されないようにする
	ReStartCountUpTimer(handle);
}

//更新拒否
void SceneButtonManager::LeaveSceneButton(int handle)
{
	//更新フラグを下げる
	pArraySceneButton_[handle]->pButton->Leave();
}

//全ボタンの更新許可
void SceneButtonManager::EnterAllSceneButton()
{
	//条件：管理ボタン全て
	//Buttonのフラグを上げる
	for (int i = 0; i < pArraySceneButton_.size(); i++)
	{
		//更新フラグを上げる
		EnterSceneButton(i);
	}
}

//全ボタンの更新拒否
void SceneButtonManager::LeaveAllSceneButton()
{
	//条件：管理ボタン全て
	//Buttonのフラグを下げる
	for (int i = 0; i < pArraySceneButton_.size(); i++)
	{
		//更新フラグを下げる
		LeaveSceneButton(i);
	}
}

//α値セット
void SceneButtonManager::SetAlphaSceneButton(float alpha)
{
	//条件：管理ボタン全て
	//Buttonのα値をセットする
	for (int i = 0; i < pArraySceneButton_.size(); i++)
	{
		pArraySceneButton_[i]->pButton->SetAlpha(alpha);
	}
}

//ボタン押下時の行動のアルゴリズムを実行
void SceneButtonManager::ExecutionAlgorithm(Button * pButton)
{
	//引数のオブジェクトポインタが
		//自クラスの保存領域の配列に存在していたら
		//その添え字を返してもらい
		//なかったら、−１をもらう
	int handle = GetArrayHandle(pButton);

	//条件：ボタンが見つかったら
	if (handle != -1)
	{
		//条件：カウント終了しているなら
		//カウントアップ
		//計測している時間が
			//アルゴリズム実行可能時間を超えていれば
			//実行
			//描画、更新を開始された時点で、
				//タイマーの計測を開始する。
				//0.0f ~ startButtonTime_までの時間を計測する。　タイマーを開始して、startButtonTimeの時間以上になっていたら、ボタン押下時の行動実行可能となる
		if (pCountTimer_->EndOfTimerForCountUp())
		{
			//アルゴリズム実行
			pArraySceneButton_[handle]->pStrategy->AlgorithmInterface();
		}
	}

	
}

//ボタンオブジェクトが保存されている領域の要素番号を渡す(添え字)
int SceneButtonManager::GetArrayHandle(Button * pButton)
{
	//条件：管理ボタン全て
	for (int i = 0; i < pArraySceneButton_.size(); i++)
	{
		//条件：引数インスタンス　と　管理ボタン群のボタンインスタンスが等しい
		if (pButton == pArraySceneButton_[i]->pButton)
		{
			//ハンドルを返す
			return i;
		}
	}
	//見つからなかった
	return -1;
}

