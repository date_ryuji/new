#pragma once

/*
	クラス詳細	：ボタン行動ストラテジークラス　親クラス
	クラスレベル：抽象クラス
	クラス概要（詳しく）
				：ボタン押下時の行動ストラテジークラスを実行部分を格納するクラス
				：共通のクラスを継承することで、　
				  AlgorithmInterfaceを呼び込むことで、アルゴリズムの実行部分を実行することが可能
				：SceneButtonManagerに自身クラス（ボタン行動クラス）とボタンクラスを登録することで、
				　登録ボタンが押されたら、自身の行動実行関数を呼び込む（AlgorithmInterface呼び込み）

*/
class ButtonStrategyParent
{
//public メソッド
public:
	//コンストラクタ
		//引数：なし
		//戻値：なし
	ButtonStrategyParent();

	//ボタン押下時の行動アルゴリズムを行う関数
		//詳細：ボタンが押下された際に、下記関数を呼び込むことで、ボタン押下時の行動実行を呼び込むことができる
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void AlgorithmInterface() = 0;
};
