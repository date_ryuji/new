#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス




//クラスのプロトタイプ宣言
class SceneManagerParent;

/*
	クラス詳細	：シーンオブジェクトの抽象クラス
	クラスレベル：抽象クラス
	クラス概要（詳しく）
				：シーンとなるオブジェクト（~~Scene（Class））が継承するクラス
				：シーンとなるオブジェクトに必要な情報を持つクラス
				：シーンとなるオブジェクトは必ず継承するクラス（SceneChangerより作成されるシーンインスタンスは必ず以下クラスを継承する）
				：シーンマネージャークラス（シーン内のオブジェクト同士の連携を用意にする橋渡しオブジェクト）をメンバに取得しておく。そのために専用のメソッドでシーンマネージャーの作成を行う

*/
class SceneParent : public GameObject
{
//protected メンバ変数、ポインタ、配列
protected : 

	//シーンマネージャーのタイプ
		//詳細：自身が所有しているシーンマネージャーのタイプ（SceneManager（Class）の名前は、シーン名と同様の名前が先頭につく。　例：(Scene)PlayScene , (SceneManager)PlaySceneManager）
		//　　：SceneChangerに、シーンでのシーンマネージャークラスをセットした際に、同時に、自身のメンバに、タイプをセットする
		//　　：SceneChanger内のシーンマネージャーのアクセスに用いる（シーンマネージャーのセット、解放）
		//制限：自身シーンのシーンマネージャーであること限定
		//　　：シーンによっては、シーンマネージャーを持たない場合もある　その場合Null
	SCENE_MANAGER_ID mySceneManagerID_;

	//シーンマネージャーのインスタンス
		//詳細：シーンマネージャー（シーン内のオブジェクト同士の連携を容易にする橋渡しオブジェクト）のインスタンス
		//　　：専用関数（自身メソッド）にて作成する。
		//制限：自身シーンのシーンマネージャーであること限定
	SceneManagerParent* pMySceneManager_;

//protected メソッド
protected : 

	//引数タイプ指定のシーンマネージャーを作成する
		//詳細：この関数処理により
		//　　：・SceneChanger（シーン切り替えクラス）に、作成したシーンマネージャーのインスタンスを渡す
		//　　：・SceneChanger（シーン切り替えクラス）に、引数指定のタイプを渡す
		//　　：・SceneChangerから、引数指定のタイプと同様のタイプで、「GetSceneManager（タイプ）」を呼び出すことで、シーンマネージャーのインスタンスを取得可能
		//制限：自身シーンのシーンマネージャー限定
		//引数：シーンマネージャー識別ID
		//戻値：シーンマネージャーポインタ
	SceneManagerParent* CreateSceneManager(SCENE_MANAGER_ID sceneManagerID);

	//シーンマネージャー解放
		//詳細：シーンマネージャーを登録したシーンチェンジャーからシーンマネージャーを解放
		//引数：なし
		//戻値：なし
	void ReleaseSceneManager();

	//シーンマネージャーの取得
		//詳細：自身に登録しているシーンマネージャーを与える
		//引数：なし
		//戻値：シーンマネージャー（メンバ）
	SceneManagerParent* GetSceneManager();
	
	//シーンマネージャーのタイプの取得
		//詳細：メンバの自信のシーンマネージャータイプを与える
		//引数：なし
		//戻値：シーンマネージャータイプ識別ID
	SCENE_MANAGER_ID GetSceneManagerID();


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	SceneParent(GameObject* parent);

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前引数設定）
		//引数：親オブジェクト（GameObject）
		//引数：オブジェクト名
		//戻値：なし
	SceneParent(GameObject* parent , const std::string& OBJ_NAME);

	//デストラクタ
		//レベル：仮想関数
		//引数：なし
		//戻値：なし
	virtual ~SceneParent();


	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void Initialize() = 0;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void Update() = 0;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void Draw() = 0;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：純粋仮想関数
		//引数：なし
		//戻値：なし
	virtual void Release() = 0;
};