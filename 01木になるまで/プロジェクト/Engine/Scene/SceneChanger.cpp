#include "SceneChanger.h"					//ヘッダ
#include "../Model/Model.h"					//モデル管理クラス
#include "../Image/Image.h"					//画像管理クラス
#include "../Audio/Audio.h"					//音楽管理クラス
#include "../Algorithm/Timer.h"				//シーンの経過時間・デルタタイムを管理するクラス
#include "../../CommonData/GlobalMacro.h"	//共通マクロ


//SceneChangerにて、シーンをインクルードして親子付け
//各シーン
#include "../../TestScene/Scene/TestScene.h"
#include "../../GameScene/Scene/GameScene.h"
#include "../../SplashScene/Scene/SplashScene.h"
#include "../../TitleScene/Scene/TitleScene.h"
#include "../../ResultScene/Scene/ResultScene.h"
#include "../../WinningScene/Scene/WinningScene.h"
#include "../../GameOverScene/Scene/GameOverScene.h"


//シーンマネージャーの親クラス
#include "SceneManagerParent.h"




//コンストラクタ
	//親のInstantiateの関数から
	//親のポインタをもらうコンストラクタをもらって、（継承元）GameObjectクラスの引数２つのコンストラクタを呼ぶ
SceneChanger::SceneChanger(GameObject * parent)
	:GameObject(parent, "SceneChanger"),
	pCurrentScene_(nullptr),

	currentSceneID_(SCENE_ID::SCENE_ID_MAX),
	nextSceneID_(SCENE_ID::SCENE_ID_MAX),
	currentSceneManagerID_(SCENE_MANAGER_ID::SCENE_MANAGER_ID_NONE),
	pCurrentSceneManager_(nullptr)
{
}

//デストラクタ
SceneChanger::~SceneChanger()
{
	//モデルの全データの解放
	Model::Release();
	//画像の全データの解放
	Image::Release();
	
	
	//Audioクラスの解放処理
	//Audio::Release();	//Main.cppにて
}

//初期化
void SceneChanger::Initialize()
{
	//Audioクラスの初期化
	//Audio::Initialize();	//Main.cppにて

	//初期シーンの定義
	{
		//最初のシーンを準備
		currentSceneID_ = SCENE_ID::SCENE_ID_SPLASH;
		//次のシーンを準備
			//次のシーンを現在のシーンで更新　
			//→Updateが呼ばれてもシーンの種類が同じため切り替わることはない
		nextSceneID_ = currentSceneID_;
		//インスタンスを生成（SceneChangerの子供に親子付）
			//pCurrentScene_ = Instantiate<SampleScene>(this);
		//引数タイプによりインスタンス作成
		CreateScene(nextSceneID_);	
	}

}

//更新
void SceneChanger::Update()
{
	//シーン切り替えの条件がそろっていたら
	//シーン切り替えが行われる
	//条件：現在のシーンID　と　次のシーンIDが違うなら
	if (currentSceneID_ != nextSceneID_)
	{
		//シーン情報のリセット
		{
			ResetScene();
			ResetSceneManager();
		}

		//※　自身の子供のReleaseSubと、子供のインスタンスの解放
		{
			//子供のReleaseSub処理
			AllChildrenReleaseSub();
			//子供の解放
				//delete処理
			AllChildrenDelete();
		}

		//ロードしたデータ（リソース）を全削除
		{
			//シーン内で確保した
			//モデルデータの解放
			Model::ReleaseAllModelData();
			//画像データの解放
			Image::ReleaseAllImageData();
			//音楽データの解放
			Audio::ReleaseAllAudioData();
		}


		//現在のシーンを次のシーンに代入
			//次フレームからは、ChangeSceneが呼ばれない限り
			//現在のシーンと次のシーンが同じなので、Updateが呼ばれても、シーンが切り替わることはない。
		currentSceneID_ = nextSceneID_;


		//次のシーンを作成
			//次フレームからプレイされるシーン
		CreateScene(currentSceneID_);

		//シーンタイマーリセット
			//シーンが開始されてからの時間計測
		Timer::Reset();
	}
}

//描画
void SceneChanger::Draw()
{
}

//解放
void SceneChanger::Release()
{	
}

//シーンのリセット
void SceneChanger::ResetScene()
{
	pCurrentScene_ = nullptr;
}

//シーンマネージャーのリセット
void SceneChanger::ResetSceneManager()
{
	//IDへNONEを代入し
		//現在、どのマネージャーもセットされていないことを示す
	currentSceneManagerID_ = SCENE_MANAGER_ID::SCENE_MANAGER_ID_NONE;
	pCurrentSceneManager_ = nullptr;
}

//シーンの切替え催促
void SceneChanger::ChangeScene(SCENE_ID changeSceneID)
{
	//次のシーンIDに引数の値を代入
		//次のフレームのUpdateにてシーン切り替えの実行
	nextSceneID_ = changeSceneID;
}

//シーンマネージャーのセット
void SceneChanger::SetSceneManager(SCENE_MANAGER_ID sceneManagerID, SceneManagerParent * pSceneManagerParent)
{
	//IDとポインタをセット
	currentSceneManagerID_ = sceneManagerID;
	pCurrentSceneManager_ = pSceneManagerParent;
}

//現在のシーンオブジェクト取得
GameObject * SceneChanger::GetCurrentScene(SCENE_ID sceneID)
{
	//条件：引数のID　と　自身が管理している現在のIDが同様
	if (sceneID == currentSceneID_)
	{ 
		//条件：ポインタが空ではない
		if (pCurrentScene_ != nullptr)
		{
			//シーンポインタを渡す
			return pCurrentScene_;
		}
		else
		{
			//シーンIDがセットされていて、ポインタが空
				//＝　シーンのInstantiate時点で　Getが呼ばれている可能性がある
				//＝この場合は、まだ、Instantiateされたポインタが、pCurrentScene_にセットされていない。
				//そのため、Findを使い、全オブジェクトから探し出す必要がある

				//シーンのオブジェクト名を指定して、全オブジェクトから走査する
					//全オブジェクトを経由してしまうため、走査する、しないを引数のBool型で判断する必要がある
			return FindObject(SCENE_OBJECT_NAME[(int)sceneID]);

		}
		
	}

	//空のオブジェクト
	return nullptr;
}

//シーンマネージャー取得
SceneManagerParent * SceneChanger::GetCurrentSceneManager(SCENE_MANAGER_ID sceneManagerID)
{
	//条件：引数のID　と　自身が管理している現在のIDが同様
	//　　：&&
	//　　：シーンマネージャーのポインタが空でない
	if (sceneManagerID == currentSceneManagerID_ && pCurrentSceneManager_ != nullptr)
	{
		//シーンマネージャーオブジェクトを返す
		return pCurrentSceneManager_;
	}

	//空のオブジェクト
	return nullptr;
}


//シーンオブジェクトの作成
void SceneChanger::CreateScene(SCENE_ID sceneID)
{
	//作成クラスを生成し
		//シーンのインスタンスを生成
		//そのインスタンスを戻値として取得する
	SceneFactory* pFactory = new SceneFactory;

	//作成
		//引数に、作成のシーンのタイプセット
		//戻値として作成したインスタンスを取得
	pCurrentScene_ = pFactory->CreateScene(sceneID, this);

	//作成クラスの削除
	SAFE_DELETE(pFactory);
}



/**************************************************************************************************************************************/
/*以下SceneFactoryクラス*/
//コンストラクタ
SceneFactory::SceneFactory()
{
}

//シーンオブジェクト作成
GameObject * SceneFactory::CreateScene(SCENE_ID type, GameObject * pParent)
{
	//引数タイプから生成するオブジェクトを選択肢
		//Instantiate（生成関数）を呼び出しインスタンス生成
		//引数のpParentを生成インスタンスの親とする
	switch (type)
	{
	case SCENE_ID::SCENE_ID_TEST: 
		return Instantiate<TestScene>(pParent);
	case SCENE_ID::SCENE_ID_GAME:
		return Instantiate<GameScene>(pParent);
	case SCENE_ID::SCENE_ID_SPLASH:
		return Instantiate<SplashScene>(pParent);
	case SCENE_ID::SCENE_ID_TITLE:
		return Instantiate<TitleScene>(pParent);
	case SCENE_ID::SCENE_ID_WINNING :
		return Instantiate<WinningScene>(pParent);
	case SCENE_ID::SCENE_ID_GAMEOVER:
		return Instantiate<GameOverScene>(pParent);

	case SCENE_ID::SCENE_ID_RESULT:
		return Instantiate<ResultScene>(pParent);
	default : 
		return nullptr;
	}

	//空オブジェクト
	return nullptr;
}
