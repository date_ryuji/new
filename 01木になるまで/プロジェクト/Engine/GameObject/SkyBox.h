#pragma once
//システム内　標準ヘッダ(usr/)
#include <d3d11.h>		//Direct3D　３D描画を担う　バッファー型やDirect3D用の型　専用のコンスタントバッファを使用するため（ID3D11Buffer型）
#include "GameObject.h"	//ゲーム内オブジェクト　抽象クラス

//クラスのプロトタイプ宣言
class Texture;


//画像背景を360度にわたって描画するオブジェクト
	//ゲームオブジェクトとしてCubeMapを読み込むオブジェクトを作成し、
	//それを必要な時にインスタンスを作成するようにさせる

/*
	クラス詳細	：画像背景を360度にわたって描画するオブジェクト
	クラスレベル：サブクラス（スーパークラス（GameObject））
	クラス概要（詳しく）
				：ゲームオブジェクトとしてCubeMapを読み込むオブジェクトを作成し、
				　それを必要な時にインスタンスを作成するようにさせる
				：CubeMapの専用の画像を読みこみ、専用のシェーダーで出力することで、
				　ゲーム内の背景として使用することが可能となる
				：ゲームオブジェクトとしてSkyBoxを作成する。
				　そして、SkyBoxのTransformは、メンバのtransform_.~を使用する。
				  そのため、SkyBoxオブジェクトの親オブジェクトのTransformに影響を受ける（ローカル座標となる）

*/
class SkyBox : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 
	//SkyBox用のモデルのハンドル
		//詳細：SkyBoxのCubeMapの画像テクスチャを出力する先のモデルのハンドル
		//　　：モデルは、箱OR球である。
	int hModel_;
		/*
			SkyBox実現の初期段階（現在没）
		
			親とは別に、SkyBoxが追尾する（つまり、追尾対象の周囲に一定の景色を表示させる対象の）オブジェクトを所有しようとした
			（実装しなかった理由）→　一番の問題は、追尾対象が削除されたときに、既に存在しないオブジェクトを指してしまう可能性があるから。
							　　　→そのため、常に親がいて、その親についていくようにする（Transformの計算の時に、相するようにしているので、問題ない。）	

		*/
	
	//SkyBox用のCubeMap用のテクスチャ
		//詳細：専用のモデルを用意し(hModel_)、モデルにCubeMapのテクスチャを環境マップとして反射し、映り込みを再現させる。
		//　　：上記により、背景画像CubeMapをモデルに移しこむので、かつ、CubeMapは360度方向のテクスチャを読み込むので、360度読み取りが可能である
		//制限：CubeMapテクスチャ限定
	Texture* pSkyBoxTex_;


private : 
	//球形のSkyBoxの作成
		//引数：なし
		//戻値：なし
	void CreateSphereSkyBox();
	//箱形のSkyBoxの作成	
		//引数：なし
		//戻値：なし
	void CreateBoxSkyBox();

//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	SkyBox(GameObject* parent);
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~SkyBox() override;

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//SkyBoxの座標移動
		//詳細：SkyBoxの親となるオブジェクトからのローカル座標移動
		//引数：セット位置
		//戻値：なし
	void SetSkyBoxPosition(XMVECTOR setPos);

	//SkyBoxのサイズの変更
		//詳細：SkyBoxのローカルスケール
		//引数：セットスケール
		//戻値：なし
	void SetSkyBoxScale(XMVECTOR setScale);




};
