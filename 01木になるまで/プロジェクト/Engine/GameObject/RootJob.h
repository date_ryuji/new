#pragma once
#include "GameObject.h"	//ゲーム内オブジェクト　抽象クラス

/*
	クラス詳細	：ゲームオブジェクト（シーンも含む）の大本の親（すべてのオブジェクトは元をたどると一番上の親にはRootJobがいる。（これがGameObjectのFindなどの機能を使用するとき、すべてのオブジェクトのUpdateを呼ぶなどに便利になる））
	クラスレベル：継承先クラス（GameObject）

	クラス概要（詳しく）
				：
				・（WinMainにて呼ぶ）
				・ゲーム内のゲームオブジェクトの一番上の親オブジェクト。全てのオブジェクトはRootJob以下に生成する
				・RootJobですること
				　★SceneChangerのインスタンスを自分の子供にする
				（これだけ）

				・Updateが呼ばれたら、さらに、その子供のUpdateが呼ばれる

				　なので、Mainでは、このRootJobだけをUpdateなどを呼ぶようにすれば、(正確にはUpdateSub)
					→RootJobの子供の、シーン（シーンマネージャー）のUpdate、
		　			→シーンマネージャーのUpdateが呼ばれれば、その子供のシーンのUpdateが呼ばれる
					→シーンのUpdateが呼ばれれば、子供の各オブジェクト（プレイヤー）が呼ばれる


				※SceneChanger下に各Scene などが、書かれるので、RootJobはSceneChangerに指示するだけ
				※RootJobの子供はSceneChangerのみ
*/
/*
	ゲームプログラムの構造

	WinMain(メッセージループ)　
	→　RootJob(GameObject継承)　
		→ SceneChanger(GameObject継承)　
		→　各Scene(GameObject継承)　
		→　各シーン内オブジェクト(GameObject継承)

*/

/*
	抽象クラスをオーバーライドする際の便利機能
		クラス名クリック→クイックアクションとリファクタリングにて純粋仮想関数の宣言

*/
//GameObjectを継承したので
//純粋仮想関数で定義したUpdateなどをオーバーライドする
	//Updateにてゲームループの各オブジェクトのフレーム処理を実現するオブジェクトとなる
class RootJob :public GameObject
{
//public ゲームループメソッド
public :
	//コンストラクタ
		//引数：なし
		//戻値：なし
	RootJob();
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~RootJob() override;


	/*
		仮想関数　コメント
		オーバーライド　コメント

		Virtualは、あっても、なくても問題はない（Virtualついてても問題は起こらない）
		このクラスをさらに継承するときは、必要
		overrideもあっても、なくても同じ（だが、きちんとオーバーライドしていることを目で分かるように）


		
	*/
	/*ゲームループ　メソッド*/
	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	virtual void Initialize() override;
	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	virtual void Update() override;
	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	virtual void Draw() override;
	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	virtual void Release() override;
};

