#pragma once
//システム内　標準ヘッダ(usr/)
#include <DirectXMath.h>	//XMVECTOR型　3次元ベクトル　など3次元処理のソースをまとめたヘッダ

//スコープの省略
using namespace DirectX;

//クラスのプロトタイプ宣言
class Transform;

/*
	クラス詳細	：ゲーム内オブジェクト（GameObject）に付与するコライダーの範囲を視認できるようにするモデルクラス
	クラス概要（詳しく）
				：コライダー範囲をモデルとして持つことで、
				　当たり判定の範囲を視認できるようになり、かつ、コライダーの範囲として使用しているモデルを当たり判定の対象として使用することで、当たり判定の対象として、ローポリゴンのモデルでの当たり判定を実現できる

*/
class DebugWireFrame
{
//private メンバ変数、ポインタ、配列
private : 

	//モデルのモデル番号
		//詳細：コライダー範囲を示すモデルのモデルハンドル番号
	int hModel_;

	//描画、非描画を示す、フラグ
		//詳細：モデルの描画、非描画を示すフラグ。フラグにより描画を行うかの判別を行う
		//　　：コライダーが作成されるたびに必ずコライダー範囲の自クラスは生成される。そのため、描画を非描画としなければ、必ず描画されてしまう。それを阻止するためのフラグ
	bool enable_;

	//自身のTransformを保存しておくクラス
		//詳細：描画位置をしめす座標などを登録しておくクラス
		//　　：コライダー範囲用のモデルのTransform
	Transform* transform_;
//public メソッド
public :
	//コンストラクタ
		//引数：なし
		//戻値：なし
	DebugWireFrame();
	//デストラクタ
		//引数：なし
		//戻値：なし
	~DebugWireFrame();

	//自身のTransformの親Transformをセット
		//詳細：その親Transformが移動すれば、自身のTransformも移動する親子関係構築に役立つ
		//引数：親のTransform
		//戻値：なし
	void SetParentTransform(Transform* pTrans);


	//ロード（コライダー用のモデルのロード）
		//詳細：コライダータイプによって、対応するモデルをロードする
		//引数：モデルタイプ（コライダータイプ）
		//戻値：なし
	void Load(int type);

	//自身Transformの座標セット（コライダーのローカル座標）
		//詳細：コライダー位置のセット（ローカル座標）
		//引数：セット座標
		//戻値：なし
	void SetPosition(XMVECTOR& position);
	
	//自身のTransformのスケールセット（ローカルスケール）
		//詳細：箱専用　
		//　　：コライダーのスケールセット（ローカルスケール）
		//引数：セットスケール
		//戻値：なし
	void SetScale(XMVECTOR& scale);
	//自身の半径のスケールセット	
		//詳細：球専用
		//　　：球のサイズ変更　球の半径を表すflaotで取得
		//引数：半径
		//戻値：なし
	void SetScale(float radius);


	//描画許可,非許可
		//詳細：コライダーの描画許可
		//引数：描画許可、拒否のフラグ（許可：true , 拒否：false）
		//戻値：なし
	void VisibleWireFrame(bool drawWireFrame);
	//描画
		//詳細：コライダーモデルの描画
		//制限：描画許可をされている場合　描画
		//引数：なし
		//戻値：なし
	void Draw();
	//コライダーのモデル（DebugWireFrame(class)）の描画時のTransform値にモデルをセットする
		//詳細：レイキャストを行う際に、モデルを描画時のTransformに移動させて、から、レイキャストを行う
		//引数：なし
		//戻値：なし
	void SetDebugWireFrameDrawingToTransform();

	//コライダーのモデル（DebugWireFrame(class)）のモデル番号をもらう
		//詳細：当たり判定を行うモデルを判別するために
		//　　：当たり判定を行うためには、モデルのモデルハンドルが必要。
		//引数：なし
		//戻値：モデルハンドル
	int GetModelHandle();



	//引数ベクトルと、Fbxと衝突した面の法線ベクトルを返す
		//詳細：壁ずりベクトルを実行する際に、
		//　　：壁ずりに使用する、面の法線方向を取得するための関数
		//引数：面との衝突判定を行うベクトルのスタート地点
		//引数：面との衝突判定を行うベクトルの方向
		//戻値：衝突した面の法線ベクトル
	XMVECTOR NormalVectorOfCollidingFace(XMVECTOR& start, XMVECTOR& dir);


};

