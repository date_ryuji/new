#include "SkyBox.h"					//ヘッダ
#include "../Model/Model.h"			//モデル
#include "../DirectX/Texture.h"		//テクスチャ
#include "../DirectX/Direct3D.h"	//Direct3D
#include "../PolygonGroup/Fbx.h"	//Fbxクラス(PolygonGroup)
#include "../DirectX/Camera.h"		//カメラクラス


//球形のSkyBoxの作成
void SkyBox::CreateSphereSkyBox()
{
	//コライダーモデル
	//球体
		//一番自然な背景となる
		//テクスチャの張られ方も違和感がない
	hModel_ = Model::Load("Assets/CubeMap/Boll.fbx", POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_SKY_BOX);

	//警告
	assert(hModel_ > -1);

	//SkyBoxのテクスチャを写す、球体のサイズを決める
	//球体
	transform_.scale_ = XMVectorSet(150.f, 100.f, 150.f, 0.f);

}

//箱形のSkyBoxの作成
void SkyBox::CreateBoxSkyBox()
{
	//コライダーモデル
	//箱
	hModel_ = Model::Load("Assets/CubeMap/Cube.fbx", POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_SKY_BOX);
	//警告
	assert(hModel_ > -1);

	//SkyBoxのテクスチャを写す、球体のサイズを決める
	//箱
	transform_.scale_ = XMVectorSet(150.f, 100.f, 150.f, 0.f);
}

//コンストラクタ
SkyBox::SkyBox(GameObject * parent)
	: GameObject(parent, "SkyBox"),
	hModel_(-1),
	pSkyBoxTex_(nullptr)
{
}

//デストラクタ
SkyBox::~SkyBox()
{
	pSkyBoxTex_->Release();
	SAFE_DELETE(pSkyBoxTex_);
}

//初期化
void SkyBox::Initialize()
{
	//SkyBox用のモデルのロード
	//球
	CreateSphereSkyBox();
	////箱
	//CreateBoxSkyBox();


	//SkyBoxに写すテクスチャをロード
		//専用のテクスチャ＝CubeMap＝環境マップ用のテクスチャをロードする
		//テクスチャは、環境マップ用のキューブテクスチャなので、専用のロードを呼ぶ
	pSkyBoxTex_ = new Texture;
	//ロード
	if (FAILED(pSkyBoxTex_->LoadCube("Assets/CubeMap/Map1/CubeMap.dds")))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "SkyBox用のテクスチャ初期化の失敗", "エラー", MB_OK);
		assert(0);	//エラーを返す
	};
	//Release()による解放も忘れずに

	/*
	
		//背景：草原（暗い）
		//if (FAILED(pSkyBoxTex_->LoadCube("Assets/CubeMap/Cube.dds")))
		//背景：草原（明るい）
		//if (FAILED(pSkyBoxTex_->LoadCube("Assets/CubeMap/HeighLight-Cube-Map/Cube_HeighLight.dds")))
		//背景：草原（明るい・床赤）
		//if (FAILED(pSkyBoxTex_->LoadCube("Assets/CubeMap/HeighLight-Cube-Map/Cube_HeighLight_Red.dds")))
	
	*/

}

//更新
void SkyBox::Update()
{
}

//描画
void SkyBox::Draw()
{
	//描画位置の特定
	Model::SetTransform(hModel_, transform_);

	//hlsl : SkyBox.hlsl
	//シェーダーへの情報の橋渡し
	{
		//CubeMapの画像サンプラーを渡す
		//テクスチャの橋渡しを送る
		ID3D11ShaderResourceView* pSRV = pSkyBoxTex_->GetSRV();
		
		//シェーダーに作った変数に渡している
			//hlsl : g_normalTexture(t1)
			//第一引数に、(tn) のnを代入
		Direct3D::pContext->PSSetShaderResources(2, 1, &pSRV);
		//SkyBoxに使用する、CubeMapは、こちらで指定した別のファイルを使用したいので、
		//クラス内で、新たに橋渡しを行う
	}
	
	//描画	
	//描画時　オブジェクトの回転などを考慮せずとも、背景に表示されているのは、
	//画像を反射させたものなので、
		//現在のカメラの視点によって、反射がされるようになっている。　つまり、現在見ている視点は、その視点方向の反射したときの周りの景色が映ることになる。
		//そのため、仮に親が回転していても、反射している画像自体は回転しないので、
		//→支店を回転させて、見る位置を変えたら、　その見ている視点における　反射が映る。　→つまり、360度の視点から背景を見ることができるということになる。
	Model::Draw(hModel_);

}

//解放
void SkyBox::Release()
{
}

//SkyBoxの座標移動
void SkyBox::SetSkyBoxPosition(XMVECTOR setPos)
{
	transform_.position_ = setPos;
}

//SkyBoxのサイズの変更
void SkyBox::SetSkyBoxScale(XMVECTOR setScale)
{
	transform_.scale_ = setScale;
}
