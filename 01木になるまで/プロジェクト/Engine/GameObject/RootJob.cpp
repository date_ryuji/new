#include "RootJob.h"
//RootJob以下へSceneChangerを子供として生成
	//SceneChanger以下へ各Sceneを子供として生成
#include "../Scene/SceneChanger.h"

//コンストラクタ
RootJob::RootJob()
{
}
//デストラクタ
RootJob::~RootJob()
{
}
//初期化
void RootJob::Initialize()
{
	//SceneChangerの生成
		//クラス説明：シーン切り替え、シーン内オブジェクトを保有するクラス
		//			：インスタンス生成時に、初期シーンのインスタンスを生成する
	Instantiate<SceneChanger>(this);
}

//更新
void RootJob::Update()
{
}
//描画
void RootJob::Draw()
{
}
//解放
void RootJob::Release()
{
}
