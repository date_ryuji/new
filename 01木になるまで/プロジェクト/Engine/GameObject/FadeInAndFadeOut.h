#pragma once
#include "GameObject.h"	//ゲーム内オブジェクト　抽象クラス

//クラスのプロトタイプ宣言
class UIGroup;
class CountTimer;

/*
	クラス詳細	：UIGroupでまとめられたUI画像群を任意時間で、フェードインフェードアウト処理を行うクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	クラス概要（詳しく）
				：FadeInFadeOutが存在する限り、遷移終了まで描画を続ける
				：カウントタイマークラスを使用して、遷移を行う

				：上記の遷移情報をもとに
　				　最初の画像を描画、
　				　最初の画像の描画のα値を　次に遷移する時間をもとに、半分でα値を１に、半分でα値を０にする
　				　遷移時間を過ぎたら次の画像へ
　				　タイマーリセット
　				　を最後の画像まで繰り返す

				 ：描画の際の違和感
					画像のα値が1.0に至る際、0.9あたりから1.0までが一瞬にてα値の変動が起こってしまっている（原因不明）

*/
class FadeInAndFadeOut : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 

	//遷移を行うUIGroup
		//詳細：UIGroup画像群、指定画像群を指定遷移秒数より遷移を行う
	UIGroup* pUIGroup_;

	//カウントタイマークラス
		//詳細：遷移を行うための時間経過、計測を行うクラス
	CountTimer* pCountTimer_;


	//遷移停止をしたか
		//詳細：α値を１．０にて止めておく処理が機能したか（あるいは、機能中か）
		//　　：初めて、α値が１．０になった時点で、遷移用のタイマーを止めて、
		//　　：計測を停止、とともに、停止用のタイマー起動。停止時間分停止する
		//　　：停止が終了したら、止めていたタイマーも再起動
	bool isStoped_;

	//現在参照しているUiGroupの画像番号
		//詳細：UIGroupに登録してある画像群配列の添え字番号
		//　　：遷移を行うたびに、番号がカウントアップされていく
		//制限：０オリジン
	int hCurrentImage_;

	//遷移時間用タイマーのインスタンスハンドル（CountTImer）
	//詳細：遷移時間の計測のためのカウントタイマー、そのタイマーのインスタンスへのハンドル
	//　　：CountTImerのタイマー　インスタンスが一つであるならば、インスタンスのハンドルを使用せずとも、アクセス可能であるが、一応ハンドル用意
	int hCountTimer_;

	//停止時間用タイマーのインスタンスハンドル(CountTimer)
		//詳細：停止時間の計測のためのカウントタイマー
	int hStopingTimer_;



	//遷移管理する画像合計数
		//制限：１オリジン
		//　　：UIGroupに登録されている画像と等しい前提
	int IMAGE_COUNT_;

	//遷移管理における遷移情報（次の画像に移る時間）
		//詳細：画像を表示し、次の画像に移るまでの遷移時間（STOPING_TIME_は省いた時間）
		//制限：配列のポインタ、要素数はIMAGE_COUNT_
	float* TRANSITION_TIME_;

	//α値が１．０になった後に停止しておく時間
		//詳細：α値が１．０（完全に描画された状態）でTRANSITION_TIME_の計測を停止して、ロゴを見せる時間を一定時間用意したい
		//　　：α値が1.0に至る時間は、TRANSITION_TIME_に登録した時間の半分である。　
		//　　：STOPING_TIME_　に登録した時間経過後、残りのTRANSITION＿TIMEの計測を再開
	float* STOPING_TIME_;

//private メソッド
private : 
	//画像を遷移
		//詳細：UIGroupにおける現在の画像から、次の画像へ遷移
		//引数：なし
		//戻値：なし
	void TransitionUIGroup();

//public ゲームループメソッド
public:
	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	FadeInAndFadeOut(GameObject* parent);

	//デストラクタ
		//引数：なし
		//戻値：なし
	~FadeInAndFadeOut();


	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//遷移対象のUIGroupをセットする
		//引数：遷移対象UIGroup
		//引数：遷移管理する画像数（UIGroupに登録した遷移を行う画像数）
		//引数：遷移管理における遷移情報（次の画像に移る時間）(配列のポインタ、要素数は第二引数)
		//引数：遷移管理における遷移情報（遷移途中にα値が1.0に至ったとき、停止する時間）
		//戻値：なし
	void SetTransitionUIGroup(UIGroup* pUIGroup , int count , float* pTransitionTime , float* pStopingTime);

	//遷移終了か
		//詳細：現在の画像番号（hCurrentImage_）が画像数（IMAGE_COUNT）と同じ場合、遷移終了（hCurrentImage_は0オリジンであり、IMAGE_COUNTが1オリジンであるため、値が同様になる＝全ての遷移が終了したと判断できる）
		//引数：なし
		//戻値：遷移終了か（遷移終了：true,遷移中：false）
	bool IsEndingTransition();


	//遷移開始
		//詳細：現在登録されているUiGroupと遷移情報で遷移を開始
		//引数：なし
		//戻値：なし
	void StartTransition();

	//遷移描画
		//詳細：停止した遷移を描画再開
		//引数：なし
		//戻値：なし
	void VisibleSceneUI();


	//遷移強制停止	
		//詳細：遷移を強制的に終了させる.UIGroupの登録も解除して、アクセス不可能となる
		//引数：なし
		//戻値：なし
	void EndTransition();

};

