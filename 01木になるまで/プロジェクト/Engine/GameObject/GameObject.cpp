#include "GameObject.h"
#include "../Scene/SceneChanger.h"			//シーン切り替えクラス　シーンオブジェクトのインスタンスと、シーンオブジェクトのマネージャークラスを所有している
#include "../Scene/SceneManagerParent.h"	//シーンのマネージャークラスの継承元クラス
#include "../../CommonData/GlobalMacro.h"	//共通マクロ関数を所有しているヘッダ
											//どこでも使うようなマクロを持っておく
#include "../Model/Model.h"					//３Dモデルを管理するクラス　Fbxのシェーダーを切り替える
											//自身で所有しているモデルハンドル番号群に、シェーダーを切り替えさせる処理をさせるため。
#include "../Collider/Collider.h"			//３D衝突判定用のコライダークラス



//コンストラクタ　引数なし
GameObject::GameObject()
	:GameObject(nullptr , "")
	/*


	//引数なしのコンストラクタが呼ばれたときに、何も要素を持たない引数にして、引数ありのコンストラクタを呼ぶ
	//引数なしのコンストラクタ
	//親もない、名前もない
	//→これは、親もない、名前もないという引数で、引数ありのコンストラクタを呼んでやればいい
	//→引数ありコンストラクタへ、基本的に自クラス内のメンバ変数の初期化を持たせているため、必ず引数ありのコンストラクタを呼ぶ必要がある。


	*/
{
	
}

//コンストラクタ　引数あり
	//各継承先のオブジェクトのコンストラクタにて、
	//以下の引数ありコンストラクタを呼び込む
	//引数にて、
	//引数：親オブジェクト
	//引数：オブジェクト名　を宣言するため、必ず呼び込まなければいけない
GameObject::GameObject(GameObject * parent, const std::string & OBJ_NAME)
	:pParent_(parent),
	objectName_(OBJ_NAME),
	isDead_(false)
{
	//親への自インスタンスの追加は、
		//Instantiat関数にて

	/*可変長配列の初期化*/
		//初期化はclear()にて行う
		//初期化を行う理由は、使用する可変配列が空であることを明示するため。
	//コライダー群の初期化
	collDatas_.clear();
	//子供リストの初期化
	childList_.clear();
	//シェーダー切替え先リストの初期化
	changeShaderFbxList_.clear();
}

//デストラクタ
	//GameObjectをオブジェクトを消すとき、子供を消さないといけない
	//ゲーム内オブジェクトで考えた時
	//自分が死んだとき
		//自分の子供は消すのか？＝消す
		//→自分が親だったら、その親が消えたら、子供を消さないといけない
	//自分が死んだとき
		//自分の親は消すのか＝消さない
		//子供が消えても、親は消えない（プレイシーンの子供の敵が消えても、プレイシーンは消えない）
	//自分のReleaseは、自身のオブジェクトの中で動的確保したものなどをReleaseするものなので、
	//動的要素のDeleteする処理を解放
GameObject::~GameObject()
{
	//すべての子供の解放
		//一番最初は、RootJobのReleaseSubにて、子供のReleaseSubが終わったら、AllChildrenDeleteを呼ぶようにする
			//呼ぶタイミングを考えれば、全オブジェクトの消去ができる
			//AllChildrenDeleteで行うのは、自身の子供だけのDelete。
			//ReleaseSubで、自身の子供のReleaseSubが呼び終わったタイミングであれば、子供を削除のタイミングとしては良いタイミングと思われる。
	AllChildrenDelete();

	//コライダー情報の解放（自身のコライダー）
		//コライダーの削除は、自身以外で作用する要素はない。
		//コライダーが削除されて困るのは、自身なので、単純に解放処理を行ってしまう
	AllColliderDelete();

	
}

//自身のワールド座標を渡す
XMVECTOR GameObject::GetWorldPos()
{
	/*
		算出方法

		自身のワールド座標を求める

		�@親のワールド座標を計算　＝　ワールド行列を作る
		�A親のワールド行列を取得
		�B�Aと自身のローカル座標を掛けて　ワールド行列取得

	*/
	{
		//�@
		//自身の親のTransform を計算させる
			//Transformの計算でワールド行列を取得したときに、親のワールド座標が求められる行列が取得できるようにする
		pParent_->transform_.Calclation();

		//�A
		XMMATRIX parentWorldMat = pParent_->transform_.GetWorldMatrix();

		//�B
		return XMVector3TransformCoord(transform_.position_, parentWorldMat);
	}
}

//自身の座標を引数ワールド座標位置へセット
void GameObject::SetWorldPos(XMVECTOR setWorldPos)
{
	/*
		算出方法

		自身の座標を引数ワールド座標位置へセット
		�@自身のワールド座標を求める
		�Aｘｙｚそれぞれ
		　�@と引数ワールド座標（setWorldPos）との差を自身の座標へ加算
	
	*/
	//�@
	XMVECTOR worldPos = this->GetWorldPos();

	//�A
	//ベクトルの差を求める
		//自身のローカル座標（transform_）をその差分移動させて、引数ワールド座標と同じ位置にする
		//ワールド座標の差をローカル座標に加算させれば、ワールド座標における差もなくなる
	//X
	transform_.position_.vecX += setWorldPos.vecX - worldPos.vecX;
	//Y
	transform_.position_.vecY += setWorldPos.vecY - worldPos.vecY;
	//Z
	transform_.position_.vecZ += setWorldPos.vecZ - worldPos.vecZ;

}

//すべての子供の解放を行う
	//デストラクタ
	//SceneManagerは、シーンを切り替えるときに、自身の子供を全消去を行う
void GameObject::AllChildrenDelete()
{
	//★全ての子供のReleaseSubを呼び終えて、
	//★あとは、子供のインスタンスのDeleteを行うだけ
		//★Releaseとはあえて、別々のタイミングで呼んでいる（Releaseの段階でDeleteする要素にアクセスしないとも限らない）


	//リストの全要素にアクセスするためリストをfor文で回す
		/*
			イテレータのコメント

			リストのポインタ的役割：イテレータの型名：std::list<GameObject*>::iterator
			長くなるので、autoで（自動型）
		
		*/
		
	//子供リスト全ての要素を削除
	//初期式：イテレータ　：子供リストの最初の要素（リスト.begin()）
	//条件式：子供リストの最後の要素でないとき（リスト.end()）
	for (auto itr = childList_.begin(); itr != childList_.end();itr++)
	{
		

			//子供だけを削除
					//1子供→2子供→3子供→4子供
					//　　　　↑　　↑
					//		　↑	4子供の削除（3は自身の削除を行わず）
					//　　　　3子供の削除（2は自身の削除を行わず）

					//そして最後の１はRootJobのクラスになるので、
						//RootJobの解放は、Mainにて行う。

		//子供のオブジェクトインスタンスのdelete
		SAFE_DELETE((*itr));


	}

	//動的確保したポインタを解放したら
	//リストの中身をクリアする
		//リスト自体は、静的確保
	childList_.clear();
}

void GameObject::AllChildrenReleaseSub()
{
	//コライダーリスト全てのReleaseSubを呼び込む
	//初期式：イテレータ　：子供リストの最初の要素（リスト.begin()）
	//条件式：子供リストの最後の要素でないとき（リスト.end()）
	for (auto itr = childList_.begin(); itr != childList_.end();itr++)
	{
		//イテレータのさしている子供のReleaseSubを呼ぶ
			//イテレータのさしているアドレスにある、GameObject
			//ReleaseSubのほうを呼ぶことで、→子供はさらに子供のReleaseSubを呼ぶ→子供はさらに子供の〜〜〜
			//（全要素のReleaseを網羅できる解放処理が完成する）
		(*itr)->ReleaseSub();

		//Releaseのタイミング、順番によって、
			//Releaseで参照しているポインタがすでに存在しない場合がある、（完全にDelete実行されてしまっている）
			//上記を避けるために、全子供のReleaseSubをしてから、
			//完全なdeleteでの消去を行うようにする

			//同じfor文ループを2度行うことになるため、非効率だが、
				//消去されたポインタ、アドレスへの参照を避ける為なので、仕方がない

	}

}

//コライダー情報の解放（自身のコライダー）
void GameObject::AllColliderDelete()
{
	//自身の確保した動的要素の削除
	for (int i = 0; i < collDatas_.size(); i++)
	{
		//解放
		SAFE_DELETE(collDatas_[i]);
	
	}
	//可変配列を空へ
		//静的確保のため、クラス解放とともに解放される
	collDatas_.clear();

}

//Updateを許可（フラグを上げる）
void GameObject::Enter()
{
	myStatus_.entered = true;
}
//Updateを拒否（フラグを下げる）
void GameObject::Leave()
{
	myStatus_.entered = false;
}
//Drawを許可（フラグを上げる）
void GameObject::Visible()
{
	myStatus_.visible = true;
}
//Drawを拒否（フラグを下げる）
void GameObject::Invisible()
{
	myStatus_.visible = false;
}
//子供のUpdateを許可（フラグを上げる）
void GameObject::EnterChildren()
{
	myStatus_.enteredChildren = true;
}
//子供のUpdateを拒否（フラグを下げる）
void GameObject::LeaveChildren()
{
	myStatus_.enteredChildren = false;
}
//子供のDrawを許可（フラグを上げる）
void GameObject::VisibleChildren()
{
	myStatus_.visibleChidren = true;
}
//子供のDrawを拒否（フラグを下げる）
void GameObject::InvisibleChildren()
{
	myStatus_.visibleChidren = false;
}
//更新許可されているか
bool GameObject::IsEnter()
{
	return myStatus_.entered;
}
//描画許可されているか
bool GameObject::IsVisible()
{
	return myStatus_.visible;
}
//子供の更新許可されているか
bool GameObject::IsEnterChildren()
{
	return myStatus_.enteredChildren;
}
//子供の描画許可されているか
bool GameObject::IsVisibleChildren()
{
	return myStatus_.visibleChidren;
}



//自身の親を切り替える
void GameObject::ChangeParent(GameObject * pAfterParent)
{
	//現在の親の子供リストから、自身のオブジェクトを削除する
	pParent_->RemoveOneChild(this);
	//親を更新
	pParent_ = pAfterParent;
	//更新後の親の子供リストに自身を追加
	pParent_->AddOneChild(this);

}
//リストから該当オブジェクトを削除する
void GameObject::RemoveOneChild(GameObject * pChild)
{
	//ChangeParent()にて
	//子供オブジェクトが親オブジェクトを変える際に
	//元親オブジェクトから子供オブジェクトを解放させる必要がある


	//該当オブジェクトを子供リストから探しリストから解放
	//初期式：イテレータ　：子供リストの最初の要素（リスト.begin()）
	//条件式：子供リストの最後の要素でないとき（リスト.end()）
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		//条件式：イテレータの示すオブジェクトと引数がオブジェクトが同様のポインタの時
		if ((*itr) == pChild)
		{
			//リストから削除
				//インスタンスのの解放処理削除はしてはいけない
			childList_.erase(itr);
			return;	//関数終了　抜ける
		}
	}
}
//子供リストへ該当オブジェクトを追加する
void GameObject::AddOneChild(GameObject * pChild)
{
	//ChangeParent()にて
	//子供オブジェクトが親オブジェクトを変える際に
	//新しい親の子供リストに子供オブジェクトをついかする必要がある


	//自身の子供リストに引数オブジェクトを追加
	childList_.push_back(pChild);
}

//コライダー衝突時に呼ばれる関数
	//各継承先にて、オブジェクトごとの衝突時処理を書く
void GameObject::OnCollision(GameObject * pTarget)
{
}


//自身の子供リストから指定の子供を探す
	//自分の子供から引数にもらった名前のオブジェクトがいるか探す
	//子供の子供も網羅する
GameObject * GameObject::FindChildObject(const std::string& OBJ_NAME)
{
	//子供から引数の名前と同じ名前のオブジェクトを探す（自身の子供なので、自身の子供リストから）
	//初期式：イテレータ　：子供リストの最初の要素（リスト.begin()）
	//条件式：子供リストの最後の要素でないとき（リスト.end()）
	for (auto itr = childList_.begin(); itr != childList_.end();itr++)
	{
		/*
		イテレータのコメント

		//イテレータは、各要素を指しているもの
		//イテレータの中身を見たいなら* をつける
		*/

		//条件式：イテレータの示す子供（GameObject*型）の持つ名前と引数の名前が等しいなら
		if ((*itr)->objectName_ == OBJ_NAME)
		{
			//探していたオブジェクトが見つかったため
			//オブジェクトのポインタを返す
			return (*itr);
		}
		

		//子供が該当のオブジェクトでなかった(returnされなかった)
			//その子供の子供も、さらに探すために（その子供の子供も探索）
		
		//再帰（自分で自分の関数を呼ぶ）
			//itrの指しているオブジェクトの子供から探したい（itrの指しているオブジェクトもGameObjectなので、当然FindChildObjectが存在する。）
			//(*itr)->FindChildObject(name);
			//見つかったら、return でその子供のオブジェクトが帰ってくる（FindChildObjectより）
			//だが、現段階では、仮に子供の子供リストの中で見つけたとしても、Findから返り値が帰ってきても返せない
			//だが、見つからなかったら、forを抜けてnullを返す可能性もあるため、ここで一気に返してはいけない
			
			//戻値：探しているオブジェクトがなかったら、nullptrが帰ってくる
			//    ：探しているオブジェクトだったら、その探しているポインタが入ってくる
		GameObject* pObj = (*itr)->FindChildObject(OBJ_NAME);
				

		//条件式：indChildOjbectによって返ってきた値がヌルでない
		if (pObj != nullptr)
		{
			//探していたオブジェクトが見つかったため
					//オブジェクトのポインタを返す
			return pObj;
		}
				
				

		/*
			★子供オブジェクトから任意オブジェクトを探す（例）


			PlaySceneからのFindChildObject（"B"）
			Bが欲しい


			PlayScene →　Player　→　A
								　
					　→　Player１→　B

			PlayScene forにてPlayerを見て、Bではない
			→(子供)PlayerのFindをよぶ
			↓
			（子供）Playerが
			Aを見て、Bではない
			→（子供）AのFindを呼ぶ
			↓
			（子供）Aが
			子供いないので、（for抜けて）
			→nullptrを返す
			↓
			Playerがnullptrを受け取って、
			次の子供がいない（for抜けて）
			→nullptr返す
			↓
			PlaySceneがnullptrを受け取って、FindChildによる結果がnullのため、
			forが回って、PlaySceneの次の子供へ
			→(子供)Player1を呼ぶ
			↓



		*/

		
	}

	//処理を終えて、
	//以下コードまで来た場合
	//子供から任意オブジェクトは見つからなかったのでnullptrを返す
	return nullptr;
}

//RootJobを探す
	//FindObjectを行うために必要なオブジェクト
	//自分の親がいなかったら自身がRootJobオブジェクトなので、
	//自身のRootJobのポインタを返す
GameObject * GameObject::GetRootJob()
{
	//自身がRootJobが調べる
		//RootJobである条件：自身に親がいない
		//				　 ：（オブジェクトは、必ず親がいる、その中で親がいないのであれば、一番上の親）
		//				　 ：RootJobが存在しないということはあり得ない、RootJob以下のオブジェクトで親がいないということはあり得ない。（GameObjectとして、誰かから親子付されたのであれば、）　＝　親がいないオブジェクト　＝　RootJob


	//自分の親を参照して
	//条件式：親がいなかったら
	if (pParent_ == nullptr)
	{
		//自身がRootJobである。
		//→自身のポインタを返す
		return this;
	}
	//自身の親がいたら
	else
	{
		//（再帰）親のGetRootJobを呼ぶ
		return pParent_->GetRootJob();
	}
	/*
		★自身がRootJobであるか調べる
	
		//ゲームオブジェクトである以上、（誰かから親子付されてゲームオブジェクトとして出現した以上）
		//RootJobが一番上にいないということはあり得ない
		//一番上に到達したら→それがRootJob
		//なので、GetRootJobを呼んだところに戻って、必ず最後にはRootJobのポインタが帰ってくる
	
	
		（A）親が
			→nullptrでなかったら
		（A）親がいるので
			→（B）親のGetRootJob()
		（B）親がいないので自身がRootJob
			→（B）関数の戻り値で自身のポインタ返す
		 (A)　


	
	*/

}

//ゲームオブジェクトを探す（全オブジェクトの中から）
	//全オブジェクトの中から、指定（引数）の名前を持つオブジェクトを探す
GameObject * GameObject::FindObject(const std::string& OBJ_NAME)
{
	//全オブジェクトの中から、引数の名前を持つオブジェクトを探したい
		//全オブジェクトの中から探す＝RootJobからFindChildObjectを呼べばいい
		//＝RootJobを探すことができれば、　RootJobのFindChildObject呼ぶだけで、（RootJobの子供に指定オブジェクトがいますか）
		//＝全オブジェクトの中から引数の名前を持つオブジェクトを探せる（ここでnullptrが帰ってくるなら、全オブジェクトの中にオブジェクトが存在しないということになる）
	return GetRootJob()->FindChildObject(OBJ_NAME);
}
//現在のシーンのオブジェクトを取得する
GameObject* GameObject::GetCurrentScene(SCENE_ID sceneID)
{
	//シーンチェンジャークラスを探す
		//クラス説明：シーン切り替えを担う、現在のシーンオブジェクトを所有するクラス
	SceneChanger* pSceneChanger = (SceneChanger*)FindObject("SceneChanger");

	//条件式：シーンチェンジャークラスが存在している
	if (pSceneChanger != nullptr)
	{
		//シーンチェンジャーの関数から
			//現在のシーンオブジェクトのゲット関数呼び込み
			//引数のsceneIDと、現在実行中のシーンのIDが同様の時、シーンのオブジェクトポインタを返す

			//戻値： nullptr 現在のシーンにおけるIDと引数へ渡したsceneIDが違う
			//　　： シーンオブジェクトポインタ 現在のシーンにおけるIDと引数へ渡したsceneIDが同じ
		return pSceneChanger->GetCurrentScene(sceneID);
	}
	//存在しない場合　nullptrを返す
		//シーンチェンジャー自体が作られていない
	return nullptr;
}
//現在のシーンのシーンマネージャーを取得する
SceneManagerParent* GameObject::GetCurrentSceneManager(SCENE_MANAGER_ID sceneManagerID)
{
	//シーンチェンジャークラスを探す
		//クラス説明：シーン切り替えを担う、現在のシーンオブジェクトを所有するクラス
	SceneChanger* pSceneChanger = (SceneChanger*)FindObject("SceneChanger");
	//条件式：シーンチェンジャークラスが存在している
	if (pSceneChanger != nullptr)
	{
		//シーンチェンジャーの関数から
			//現在のシーン　マネージャーオブジェクトのゲット関数呼び込み
			//引数のsceneManagerIDと、現在実行中のシーンのIDが同様の時、シーンのマネージャーオブジェクトポインタを返す

			//戻値： nullptr 現在のシーンにおけるIDと引数へ渡したsceneIDが違う
			//　　： シーンオブジェクトポインタ 現在のシーンにおけるIDと引数へ渡したsceneIDが同じ
		return pSceneChanger->GetCurrentSceneManager(sceneManagerID);
	}
	//存在しない場合　nullptrを返す
		//シーンチェンジャー自体が作られていない
	return nullptr;
}

//自身のオブジェクトへコライダーを追加する
	//自身の所有するコライダー配列（ベクター）に
	//当たり判定を登録
void GameObject::AddCollider(Collider * addCollider)
{
	//自身の所有しているコライダー配列（ベクター）にコライダーを追加
	collDatas_.push_back(addCollider);

	//コライダーに
	//コライダーをセットしているゲームオブジェクトを知らせる
		//コライダー描画の際などに、描画位置をゲームオブジェクトに沿わせる必要がある。その際に、位置を取得するためのアクセスポインタを渡す
	addCollider->SetMyGameObject(this);
}
//コライダーのデバック用に描画している当たり判定モデルのモデルハンドルを取得する
int GameObject::GetColliderModelHandleFromGameObject()
{
	/*
		モデルハンドルを取得する理由

		�@自身が所有しているコライダー、DebugWireFrameを自身オブジェクトのTransformに沿わせる（コライダーの離れ具合を考慮して）
		�Aライダーとの３D衝突判定（レイキャスト）を行うために、コライダーを自身のコライダーのいるべき位置にセットする必要がある。
		�Bコライダーにて使用しているDebugWireFrameとは、コライダー用のモデルを使用している。
		�Cどのオブジェクトにおいても共通のコライダー用のモデルを使用しているため、現在コライダーがどの位置にいるのかは、分からない。そのため、自身のコライダー位置へセットする必要がある。

	
	*/


	//コライダーが一つもない場合
		//取得することは不可能なので
	//条件式：コライダー数が0の場合
	if (collDatas_.size() == 0)
	{
		//-1を返す
		return -1;
	}

	//一番目のコライダーのモデル番号を取得する
		//コライダーを複数使用している場合は考えず、一番目のコライダーの番号を与える
	return collDatas_[0]->GetDebugWireFrameHandle();
}

//コライダーのデバック用に描画している当たり判定モデルを描画時のTransformにセットさせる
void GameObject::SetColliderTransformDrawingFromGameObject()
{
	//GetColliderModelHandleFromGameObject()において
	//ハンドル番号を渡した
	//ハンドル番号を渡すことで、３D衝突判定（レイキャストが）可能となる
	//レイキャストを行う前に、デバック用のモデルを一度適切な位置へ移動させる必要がある。　
		//その移動処理。



	//DebuｇWireFrameのSetTransformを呼び込むだけ
		//最新の描画Transformをセットさせる

	//コライダーが一つもない場合
		//取得することは不可能なので
	//条件式：コライダー数が0の場合
	if (collDatas_.size() == 0)
	{
		return;
	}

	//一番目のコライダーのDebugWireFrameのTransformをセットしてもらう
		//コライダーを複数使用している場合は考えず、一番目のコライダーの番号を与える
	return collDatas_[0]->SetDebugWireFrameDrawingToTransform();
}

//自身の描画と、子供のDrawを呼ぶ処理（子供のDrawは、親であるGameObjectが呼んであげないといけない）
	//大本のWinMainにて、DrawSubを読んであげることで、
		//その下の子供のDrawSubを呼んで→さらに子供のDrawSub呼んでと続くので、すべての子供のDrawを網羅できる
void GameObject::DrawSub()
{
	/*
		�@自身のDrawを呼ぶ
		�A自身の子供のDrawSubを呼ぶ
	*/

	//�@
	//自分のDrawを呼ぶ
		//自身が描画をしてよい状態にあるとき限定
	//条件式：描画許可フラグが立っている
	if (myStatus_.visible)
	{
		//描画
			//継承先にオーバーライドされた描画処理関数呼び出し
		Draw();

		//自身のコライダーリストにある、コライダーを描画
		//デバック用として、ワイヤーフレームとして描画許可がされているものを描画
		DrawCollider();
	}

	//�A
	//自身の子供がが描画をしてよい状態にあるとき限定
	//条件式：子供描画許可フラグが立っている
	if (myStatus_.visibleChidren)
	{
		//自分のDrawが終わった後に
		//子供全員のDrawSubを呼ぶ
		for (auto itr = childList_.begin();
			itr != childList_.end(); itr++)
		{
			//DrawSubを呼び込み
				//DrawSubは自身の関数と同様の処理を行うので、自身DrawSub->子供のDrawSub->更に子供のDrawSub->~~~~
			(*itr)->DrawSub();
		}
	}

}
//自身の更新と、子供の更新を呼ぶ
void GameObject::UpdateSub()
{
	/*
		�@自身のUpdateを呼ぶ
		�A自身の子供のUpdateSubを呼ぶ
	*/

	//自分のUpdateを呼ぶ
	//自身が更新をしてよい状態にあるとき限定
	//条件式：更新許可フラグが立っている
	if (myStatus_.entered)
	{
		///自身のUpdateが終わった後に当たり判定
		//Update内で、衝突した相手との検証を行うので、
		//ここで、衝突相手を特定する。
		//どこでCollisionを呼ぶかは、衝突して、
		//・衝突後、移動後に移動分だけ戻すのか
		//・衝突する前に、移動先が衝突する位置なのか
		//このどちらで判断させるかにもよってくる。
		
		///★今回は→移動してから、移動分だけ戻すという方式をとる衝突判定をとる。（移動後に自身の位置がほかのコライダーと衝突しているか）
		//当たり判定の検証（自身のコライダーと、ほか全体のコライダーとの当たり判定）
		Collision();

		//更新
			//継承先にオーバーライドされた更新処理関数呼び出し
		Update();
	}


	//親が動けば、子も動く
		//親が動いている時点で、子供も動いている
		//なので、子供のUpdateSubが呼ばれる前に、親のTransformの計算がされている必要がある
	//Transformの行列計算
	transform_.Calclation();





	//自身の子供が更新をしてよい状態にあるとき限定
	//条件式：子供更新許可フラグが立っている
	if (myStatus_.enteredChildren)
	{
		//自分のUpdateが終わった後に
		//子供全員のUpdateSubを呼ぶ
		for (auto itr = childList_.begin();
			itr != childList_.end(); itr++)
		{
			//UpdateSubを呼び込み
				//UpdateSubは自身の関数と同様の処理を行うので、自身UpdateSub->子供のUpdateSub->更に子供のUpdateSub->~~~~
			(*itr)->UpdateSub();
		}
	}



	/*消去フラグの立っている子供の消去******************/
		/*
			UpdaeSubにて
			すべてのこどもの
			UpdateSubを呼んだあと(Updateが終わる前に消してはいけない)
		
		*/
	//すべての子供をループして、isDeadのフラグが立っているかの確認
	//初期式：イテレータ　：子供リストの最初の要素（リスト.begin()）
	//条件式：子供リストの最後の要素でないとき（リスト.end()）
	for (auto itr = childList_.begin();itr != childList_.end();)
	{
		//条件式：イテレータ内要素の死亡フラグが立っていたら
		if ((*itr)->isDead_ == true)
		{
			//子供のReleaseSub（子供のすべての解放）
			(*itr)->ReleaseSub();

			//Releaseを読んだら、SAFE_DELETEでポインタの削除
			SAFE_DELETE(*itr);

			//子供リストの中から削除
			itr = childList_.erase(itr);
			/*
				★リストの要素を消すとき
				　イテレータの削除におけるイテレータ更新


				eraseでその場で消してしまうとダメ。

				A,B,C,D
				Aさんは、Bさんの電話番号を知っている（他を知らない）
				Bさんは、Cさんの電話番号を知っている（他を知らない）
			

				回覧板を渡す（その回覧板をイテレータとして）
				イテレータのbeginによって、Aさんに回覧板（イテレータ）回る
				イテレータ＋＋によって、　Bさんに回覧板

				Bさんが途中でいなくなるとする。
					→Bさんがいなくなって、回覧板を持っている状態でいなくなる

				CさんにBさんが持っていた回覧板を渡さなければいけない（BさんだったイテレータがCさんにわたる）

				消えたときCさんがBさんの位置のイテレータの位置に位置することになる
					→なので、for分で回った時に、Cさんがやらずに、次に行ってしまうことになる

				それを防ぐために、
				→イテレータを消したら、イテレータBさんの位置がCさんになる
					→★なので、次に回していけない

				だったら、イテレータを消さなかったら、イテレータを回して（itr++）
				消したらイテレータを回さない(×)
			
			*/
			//イテレータを消されたら、戻り値として
			//消した次の人のイテレータが入ってくる
				//それを次のイテレータとする


		}
		else
		{
			//イテレータが消されなかったらイテレータを回す
			itr++;
		
		}
	}

	
}
//自身の解放と、子供の解放を呼ぶ
void GameObject::ReleaseSub()
{
	/*
		�@自身のReleaseを呼ぶ
		�A自身の子供のReleaseSubを呼ぶ
		�Bインスタンスの削除
	*/

	//�@
	//自分のReleaseを呼ぶ
	Release();
	//�A
	//子供のReleaseSub処理
	AllChildrenReleaseSub();
	//�B
	//子供のインスタンスの削除
		//delete処理
	AllChildrenDelete();

}

//自身のコライダー衝突判定の総当たり（コライダーを持つ全オブジェクトと総当たりさせる）
void GameObject::Collision()
{

	//RootJobから総当たりで
		//自身（Collisionを呼んでいるオブジェクト）と他コライダーとで、衝突しているかをもらう（RootJobから見れば、全オブジェクトと総当たりに判定できる）

	//総当たりのため、全オブジェクトを見るためにRootJobから
		//RootJobから、子供がいるなら
		//→その子供のコライダーとの衝突判定、
		//→子供がいるなら、その子供との衝突判定
	
	/*
		全コライダーと衝突判定を行う

		//自身のコライダーを送って、
		//再帰的に、もらったコライダーと呼ばれた側の持っているコライダーとで、衝突判定
		//衝突していたら、
		//→trueが帰ってくる
		//→だが、これでは、１つのコライダーにあたった、時点で帰ってきてしまう
		//→１つのコライダーとの接触でよいのであれば、問題は無いが、→総当たりに行うとは、そうゆうことではない気がする
		//→★衝突のコライダーに接触しているときに、片方の衝突はなかったことになるので、注意


	
	*/

	/*
		算出方法

		全コライダーと衝突判定
		�@コライダーをオブジェクトに沿わせる
		�ARootJobから、全コライダーと自身コライダーの衝突判定
		�B衝突していたらOnCollision関数呼び出し
	
	
	*/
	
	//全てのコライダーと自身コライダーの衝突判定（コライダーひとつずつ　全コライダーとの衝突判定）
	//初期式：int　：0
	//条件式：コライダー配列の要素数　未満
	for (int i = 0; i < collDatas_.size(); i++)
	{
		
		//�@
		//コライダーに自身のオブジェクトの位置を送り保存してもらう
			//→コライダーの位置を、オブジェクトの位置＋コライダーの離れ具合でとるので、
		collDatas_[i]->SetObjectPos(transform_.position_);


		//�A
		//RootJobから、全オブジェクトのコライダーとと自身のコライダーとの衝突判定
			//RootJobのCollisionChildObjectを呼ぶ
			//×衝突していた場合、衝突した相手のGameObject*がもらえる
		//GameObject* pCollTarget = GetRootJob()->CollisionChildObjectCollider(collDatas_[i]);
		GetRootJob()->CollisionChildObjectCollider(collDatas_[i]);

		/*
		//�B
		//条件式：nullptrでないなら
		if (pCollTarget != nullptr)
		{
			//衝突しているので、
			//衝突の相手を引数に送って、衝突時の処理を行う
			OnCollision(pCollTarget);

		}
		//nullptrなら、衝突していないので何もしない。
		*/
		//★全コライダーとの衝突を行うため、衝突判定がされたコライダーごとに自身のOnCollisionが呼ばれるように変更した。
		//★1つのターゲットとなるコライダーに対して、複数のコライダーの衝突を検知。そして、複数のコライダーごとの衝突処理を行う。

	}





	//返り値で衝突していると判定が来たら
	//自身の衝突されたときに呼ぶ関数を呼びこむ（引数に当たった先のコライダーを渡して）

}
//コライダーのデバック用のワイヤーフレームを描画（描画許可されているもの限定）
void GameObject::DrawCollider()
{
	//自身全コライダーの描画
	//初期式：int　：0
	//条件式：コライダー配列の要素数　未満
	for (int i = 0; i < collDatas_.size(); i++)
	{
		//コライダーのワイヤーフレーム描画
			//描画は、Updateのタイミングで行うと、
			//BeginDrawにて、クリアされてしまうので、
			//DrawSubの中で、自身の、コライダーを描画する処理を行う
		collDatas_[i]->Draw();
	}
}

//引数コライダーが、全オブジェクトの持つコライダーと衝突しているか
	//再帰を行って、自身の子供に引数のコライダーと当たっているオブジェクト(オブジェクトの持つコライダーからすべて調べる)を探す
GameObject* GameObject::CollisionChildObjectCollider(Collider* collider)
{

	//すべての子供をループして、全ての子供のコライダーと引数コライダーの衝突判定を行う
	//初期式：イテレータ　：子供リストの最初の要素（リスト.begin()）
	//条件式：子供リストの最後の要素でないとき（リスト.end()）
	for (auto itr = childList_.begin(); itr != childList_.end();itr++)
	{
	
		//イテレータにて示される　子供の所有するコライダー　1つずつアクセスして、そのコライダーとの衝突判定
		//初期式：int ：0
		//条件式：コライダー配列の要素数　未満
		for (int i = 0; i < (*itr)->collDatas_.size(); i++)
		{
			//ターゲットコライダー：引数コライダー
			//指定コライダー　　　：イテレータにて回っている子供のコライダー


			//条件式：ターゲットコライダーと指定コライダーが同じでない（ポインタなのでアドレスが同じ）
			// &&
			//条件式：ターゲットコライダーと指定コライダーが衝突しているか
					//自身が当たり判定の関数を呼んで、引数にターゲットを入れる
					//→順番が逆でも、結局やっていることは変わらない。
					//→すべての自身以外のコライダーと当たり判定を行うようにする
			if (collider != (*itr)->collDatas_[i] &&
				collider->IsHit((*itr)->collDatas_[i])
				)
			{
				//衝突しているなら、子供のオブジェクトを返す
				//return (*itr);
					//衝突しているオブジェクトは一つとは限らない。
					//ひとつのターゲットコライダーに対して、複数の指定コライダーの当たり判定処理を担う。
					//そのため↓

				//衝突ごとにターゲットコライダーのOnCollisionを呼び込む
				collider->GetMyGameObject()->OnCollision((*itr));
			}

		
		}
		//returnされずに、forを抜けた　＝　”子供のコライダーには”衝突するものはなかった
		//→子供のさらに子供のコライダーから探す
		//↓


		//”子供”の所有するコライダーと目的のコライダーと衝突をしなかった場合
		//（再帰）その子供の子供に同じ衝突判定を呼ぶ込む処理を行う（子供のCollChildObで、子供リストのコライダーと再帰的に判定）
		//GameObject* pCollTarget =
		(*itr)->CollisionChildObjectCollider(collider);

		/*
		//戻り値がnullptrでないなら→衝突者が見つかった
		//→返す
		if (pCollTarget != nullptr)
		{
			//衝突したコライダーを持つオブジェクトを返す
			//return pCollTarget;
		}
		//nullptrなら見つからなかった
		//→そうでないならfor回す
		*/

	}


	//引数のコライダーと、相手のコライダーが同じ場合
	//自分自身となってしまうので、
	//当然接触してしまう。
	//この場合は、接触判定を行わないように。

	//上記の判定では、自分の他のコライダーとも衝突判定が起きる




	return	nullptr;
}
//オブジェクトを自身の子供リストに追加
void GameObject::PushBackChild(GameObject * pChild)
{
	//Instantiate関数における
	//親オブジェクトの子供に生成したオブジェクトを追加する際に呼ぶ関数

	//自身の子供リストに追加
	childList_.push_back(pChild);

	{
		//新たに宣言したオブジェクト
			//の親のTransformを入れてあげる
			//Instantiateは、必ずオブジェクト宣言時に通るので、その時に、
			//自身のTransform内の親のTransformに親のTransformをセット。(参照渡しでアドレスをセット)
		pChild->transform_.pParent_ = &pChild->pParent_->transform_;		//Transformの関数内にて、親のTransformを考慮した行列の計算を行う→親のTransformが変更されたら、自身も変更する。
			//pParentは、GameObjectのこのクラスのparentになるので、
			//新規に作成したオブジェクトの親を指定したいときは、→p->pParent_としないといけない

			//Transformの親を設定することで、
			//親オブジェクトが動けば、親のTransformが変更されるため、
			//子供のTransformも動く

			
	}

}
//オブジェクト内にて管理しているモデルのシェーダーを切り替える
void GameObject::ChangeMyFbxShader(SHADER_TYPE type, bool childrenToo)
{
	//リスト内に登録された、自オブジェクト内にて管理しているモデルの
	//モデルハンドル番号を使用して
	//Model.cppにアクセスし、
		//モデル自体のシェーダーを切り替える
	
	//すべてのモデルハンドルから、モデルのシェーダーを切り替える
	//初期式：イテレータ　：モデルハンドルリストの最初の要素（リスト.begin()）
	//条件式：モデルハンドルリストの最後の要素でないとき（リスト.end()）
	auto itr = changeShaderFbxList_.begin();
	for (itr; itr != changeShaderFbxList_.end(); itr++)
	{
		Model::ChangeShader((*itr), type);
	}

	//子供も同様に、シェーダーを切り替える場合
	//条件式：フラグが立っている
	if (childrenToo)
	{
		//子供も再帰的に切り替えさせるフラグが立っていたら
		//子供の同じ関数を呼ぶ（再帰）
		//すべての子供の、モデルハンドルから、モデルのシェーダーを切り替える
		//初期式：イテレータ　：子供リストの最初の要素（リスト.begin()）
		//条件式：子供ハンドルリストの最後の要素でないとき（リスト.end()）
		for (auto itr = childList_.begin();itr != childList_.end(); itr++)
		{
			//同様のType　かつ　同様のchildrenToo(再帰のため、)
			(*itr)->ChangeMyFbxShader(type, childrenToo);
		}
	}


}
//シェーダーを切り替えるモデル（FBXのモデルハンドル）群に番号を追加
void GameObject::AddChangeShaderFbx(int handle)
{
	//すべてのモデルハンドルから、引数ハンドルと同様の番号があるか
	//初期式：イテレータ　：モデルハンドルリストの最初の要素（リスト.begin()）
	//条件式：モデルハンドルリストの最後の要素でないとき（リスト.end()）
	auto itr = changeShaderFbxList_.begin();
	for (itr; itr != changeShaderFbxList_.end(); itr++)
	{
		//条件式：イテレータ内要素と引数ハンドルが等しい
		if ((*itr) == handle)
		{
			//モデルのハンドル番号が、
				//同じになることはあり得ないので、
				//この場合は、２回呼んでしまったということになる。
				//何もせずに帰る
			return;

		}
	}

	//リストへ追加
	changeShaderFbxList_.push_back(handle);
}

//シェーダーを切り替えるモデル（FBXのモデルハンドル）群から番号を削除
void GameObject::DeleteChangeShaderFbx(int handle)
{
	//すべてのモデルハンドルリストから、引数ハンドルを削除
	//初期式：イテレータ　：モデルハンドルリストの最初の要素（リスト.begin()）
	//条件式：モデルハンドルリストの最後の要素でないとき（リスト.end()）
	auto itr = changeShaderFbxList_.begin();
	for (itr; itr != changeShaderFbxList_.end(); itr++)
	{
		//条件式：イテレータ内要素と引数ハンドルが等しい
		if ((*itr) == handle)
		{
			//イテレータを使用して削除
			changeShaderFbxList_.erase(itr);
			return;

		}
	}

}
