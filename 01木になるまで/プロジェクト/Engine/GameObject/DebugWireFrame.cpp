#include "DebugWireFrame.h"						//ヘッダ
#include "Transform.h"							//Transform
#include "../Model/Model.h"						//モデル
#include "../../CommonData/PolygonGroupType.h"	//ポリゴン群タイプ
#include "../../CommonData/ShaderType.h"		//シェーダー群タイプ
#include "../../CommonData/GlobalMacro.h"		//共通マクロ

//コンストラクタ
DebugWireFrame::DebugWireFrame():
	hModel_(-1),enable_(false),
	transform_(new Transform)
{
}

//デストラクタ
DebugWireFrame::~DebugWireFrame()
{
	SAFE_DELETE(transform_);
}

//自身のTransformの親Transformをセット
void DebugWireFrame::SetParentTransform(Transform * pTrans)
{
	transform_->pParent_ = pTrans;
}

//ロード（コライダー用のモデルのロード）
void DebugWireFrame::Load(int type)
{
	//描画する、しないにかかわらず
	//必ずロードする、コライダーを形作るモデル
	

	switch (type)
	{
	//Sphere
	case 0:
		hModel_ = Model::Load("Assets/DebugCollider/DebugSphereCollider.fbx" , POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP ,   SHADER_TYPE::SHADER_DEBUG_WIRE_FRAME);
		break;
	//Box
	case 1:
		hModel_ = Model::Load("Assets/DebugCollider//DebugBoxCollider.fbx", POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_DEBUG_WIRE_FRAME);
		break;
	//その他
	default:
		break;
	}
	//警告
	assert(hModel_ != -1);

}

//自身Transformの座標セット（コライダーのローカル座標）
void DebugWireFrame::SetPosition(XMVECTOR & position)
{
	transform_->position_ = position;
}

//自身のTransformのスケールセット（ローカルスケール）
void DebugWireFrame::SetScale(XMVECTOR & scale)
{
	transform_->scale_ = scale;
}

//自身の半径のスケールセット	
void DebugWireFrame::SetScale(float radius)
{
	//半径として表すため、
	// 半径　０．５ｆ→　全体サイズ１．０ｆ　となる。

	//そして、球は、ｘｙｚがともに等しいため、Transform.scale は、ｘｙｚ共通の値を入れる

	transform_->scale_ = XMVectorSet(radius, radius, radius, 0.f);
}

//描画許可,拒否
void DebugWireFrame::VisibleWireFrame(bool drawWireFrame)
{
	enable_ = drawWireFrame;
}

//描画
void DebugWireFrame::Draw()
{
	//Transformを計算し、セットする
	SetDebugWireFrameDrawingToTransform();

	//条件：描画許可されている場合
	if (enable_)
	{
		//描画
		Model::Draw(hModel_);
	}
}

//コライダーのモデル（DebugWireFrame(class)）の描画時のTransform値にモデルをセットする
void DebugWireFrame::SetDebugWireFrameDrawingToTransform()
{
	//Transformを計算し、セットする
	transform_->Calclation();
	//Transformをセットする
	Model::SetTransform(hModel_, *transform_);
}

//コライダーのモデル（DebugWireFrame(class)）のモデル番号をもらう
int DebugWireFrame::GetModelHandle()
{
	return hModel_;
}

//引数ベクトルと、Fbxと衝突した面の法線ベクトルを返す
XMVECTOR DebugWireFrame::NormalVectorOfCollidingFace(XMVECTOR & start, XMVECTOR & dir)
{
	return Model::NormalVectorOfCollidingFace(hModel_ , start, dir);
}
