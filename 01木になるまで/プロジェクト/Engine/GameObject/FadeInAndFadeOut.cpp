#include "FadeInAndFadeOut.h"						//ヘッダ
#include "UIGroup.h"								//UIGroup群
#include "../../CommonData/GlobalMacro.h"			//共通マクロ
#include "../../GameScene/CountTimer/CountTimer.h"	//計測クラス


//コンストラクタ
FadeInAndFadeOut::FadeInAndFadeOut(GameObject* parent)
	: GameObject(parent, "FadeInAndFadeOut"),
	pUIGroup_(nullptr),
	pCountTimer_(nullptr),
	hCurrentImage_(-1),
	IMAGE_COUNT_(-1),
	TRANSITION_TIME_(nullptr),
	STOPING_TIME_(nullptr),

	hCountTimer_(-1),
	hStopingTimer_(-1) , 

	isStoped_(false)

{
}

//デストラクタ
FadeInAndFadeOut::~FadeInAndFadeOut()
{
	//遷移情報の解放
	EndTransition();
}

//初期化
void FadeInAndFadeOut::Initialize()
{
	//カウントタイマーインスタンス生成
	pCountTimer_ = (CountTimer*)Instantiate<CountTimer>(this);

	//意味を持たないタイマーの開始
		//タイマーのインスタンスを一つ確保しておき、計測のタイミングになったとき、そのインスタンスをリセットし計測する
	//遷移時間計測用
	hCountTimer_ = pCountTimer_->StartTimerForCountUp(0.f, 0.f);
		//のちにこのタイマーのインスタンスにアクセスするには、
		//ハンドルにて

	//停止時間計測用
	hStopingTimer_ = pCountTimer_->StartTimerForCountUp(0.f, 0.f);

}

//更新
void FadeInAndFadeOut::Update()
{
	//条件：遷移画像がある場合
	if (IMAGE_COUNT_ != -1)
	{
		//画像の遷移（UIGroup）
		TransitionUIGroup();
	}
}

//描画
void FadeInAndFadeOut::Draw()
{
}

//解放
void FadeInAndFadeOut::Release()
{
}

//画像の遷移（UIGroup）
void FadeInAndFadeOut::TransitionUIGroup()
{
	//遷移用タイマーの経過時間
		//前回UIGroupの画像が遷移指定からの経過時間
	float elpasedTime = pCountTimer_->GetElpasedTime(hCountTimer_);
	//遷移時間
		//現在遷移中の画像。その合計遷移時間
	float transitionTime = TRANSITION_TIME_[hCurrentImage_];


	//条件：停止した過去がない 
	//　　：&&
	//　　：初めて　α値１．０を超える経過時間（遷移時間の半分）が経過した
	if (!isStoped_ && (elpasedTime >= (transitionTime / 2.f)))
	{
		//停止をすべきかの判定
			//経過時間が合計遷移時間の半分になったら止める
		pCountTimer_->NotPermittedTimer(hCountTimer_);

		//停止計測タイマーの計測
		pCountTimer_->ReStartTimerForCountUp(
			hStopingTimer_, 0.f, STOPING_TIME_[hCurrentImage_]);

		//停止したというフラグを立てる
		isStoped_ = true;
	}

	//条件：停止機能を終えたか
	//　　：&&
	//　　：タイマーの計測が終了していないなら
	else if (isStoped_ && !pCountTimer_->EndOfTimerForCountUp(hStopingTimer_))
	{
		//関数終了
		return;
	}
	else
	{
		//時間停止のフラグを下す
			//停止中ではないとき、必ず通る
			//処理の内容は、あくまでも、フラグの上下である
		pCountTimer_->PermitTimer(hCountTimer_);
	}


	//現在の遷移情報をもとに遷移
	//条件：遷移時間が終了しているか
	if (pCountTimer_->EndOfTimerForCountUp(hCountTimer_))
	{
		//遷移終了時
			//遷移画像を次の画像へ
			//次の画像がない場合、終了へ

		//現在描画中の画像を非描画に
		//カウントをカウントアップ

		//現在の画像非描画
		pUIGroup_->InvisibleSceneUI(hCurrentImage_);

		//カウントアップ
		hCurrentImage_++;

		//条件：全画像の遷移を終了したか
		if (IsEndingTransition())
		{
			//遷移終了
			EndTransition();
		}
		else
		{
			//遷移終了でない
			//画像描画
				//カウンターリセット
			VisibleSceneUI();

		}

	}
	/*遷移処理*******************************************************************************************/
	else
	{
		//遷移処理を行う

		//経過時間が
		//遷移時間の半分以下なら
			//α値を０から上げる
		//遷移時間の半分以上なら
			//α値を１から下げる


		//計算が減算か増算か求める
		//０．５より小さい　＝　増算
		//０．５より大きい　＝　減算
		//bool isSub = false;
			//減算か、加算かは、isStopedという、これまでに経過時間が遷移時間の半分を越したかのフラグで判断できる
		//if ((elpasedTime >= (transitionTime / 2.f)))
		//条件：時間停止中か
		if(isStoped_)
		{
			////減算
			//isSub = true;

			//経過時間と
			//遷移時間を　それぞれ遷移時間の半分減らす
			elpasedTime -= (transitionTime / 2.f);
			transitionTime -= (transitionTime / 2.f);

			//上記を行うことで、
			//０〜１の割合でα値を出すことが可能
		}


		//条件：経過時間が0の場合
		if (elpasedTime == 0.0f)
		{
			//計算が不可能であるため
			
			//α値を０にして
			//関数終了
			pUIGroup_->SetAlphaSceneUIAndButton(0.f);

			return;
		}

		//透明度
		//現在の経過時間 / 遷移時間　を行って
			//現在の経過時間からの割合で透明度を表す
		float alpha = elpasedTime / (transitionTime / 2.f);
			//経過時間の　50%で１．０ｆ
			//経過時間の　100%で０．０ｆ


		//１〜０の間に切り詰める	//clamp(v, low, high)
			//1: max(alpha , 0.f) = 第一引数が第二引数より大きかったら　第一引数を。それ以外第二引数。
			//2: min(1: , 1.f) = 第一引数が第二引数より小さかったら、第一引数を。それ以外第二引数。
		alpha = min(max(alpha, 0.f), 1.f);




		//０〜１（増算）、１〜０（減算）かを判断。
		// 0.0f（増算）、1.0f（減算）
		//減算の場合
		//if (isSub)
		if (isStoped_)
		{
			//減算
			//α値を１〜０へ変動させるので、
				//�Bの値を反転させる
			alpha = 1.f - alpha;
		}

		
		//targetInfoに格納しているnumに、α値をセットする画像への添え字、タイプが入っている。
			//そのタイプにて、アクセス
		pUIGroup_->SetAlphaSceneUIAndButton(alpha);

	}


}

//遷移対象のUIGroupをセットする
void FadeInAndFadeOut::SetTransitionUIGroup(UIGroup* pUIGroup, 
	int count, float* pTransitionTime , float* pStopingTime)
{
	//UIGroupの取得
	pUIGroup_ = pUIGroup;

	//全要素の非表示
	pUIGroup_->InvisibleSceneUIAndButton();

	//画像数取得
	IMAGE_COUNT_ = count;
	
	//遷移時間の確保
	{
		//遷移情報の動的確保
		//確保済みの情報を解放
		SAFE_DELETE_ARRAY(TRANSITION_TIME_);

		//IMAGE_COUNT分配列の要素を動的確保
		TRANSITION_TIME_ = new float[IMAGE_COUNT_];
		//要素のコピー
		//引数：コピー先
		//引数：コピー元
		//引数：コピーサイズ
		memcpy(TRANSITION_TIME_, pTransitionTime, sizeof(float) * IMAGE_COUNT_);
	}
	
	//停止時間の確保
	{
		//確保済みの情報を解放
		SAFE_DELETE_ARRAY(STOPING_TIME_);

		STOPING_TIME_ = new float[IMAGE_COUNT_];
		memcpy(STOPING_TIME_, pStopingTime, sizeof(float) * IMAGE_COUNT_);

	}
	
	//現在参照する画像ハンドルを０にする
		//一番初めの要素を指す
	hCurrentImage_ = 0;

}

//遷移終了か
bool FadeInAndFadeOut::IsEndingTransition()
{
	//画像数が　ー１　でない
	//あるいは
	//画像番号　と　画像数が同じ
	return (IMAGE_COUNT_ == -1) || (hCurrentImage_ == IMAGE_COUNT_);
}

//遷移開始
void FadeInAndFadeOut::StartTransition()
{
	//スタートできない場合関数を終了
	//条件：画像数が存在しないなら
	if (IMAGE_COUNT_ == -1)
	{
		//関数終了
		return;
	}


	//最初の画像を描画
	VisibleSceneUI();

}

//遷移描画
void FadeInAndFadeOut::VisibleSceneUI()
{


	//タイマーのリセット
	//一番初めの画像の遷移情報にアクセスして、カウントを開始する
	//次に遷移するまでの時間経過
	pCountTimer_->ReStartTimerForCountUp(hCountTimer_, 0.f, TRANSITION_TIME_[hCurrentImage_]);

	//最初の画像を描画
	pUIGroup_->VisibleSceneUI(hCurrentImage_);

	//すべてのα値を０に
		//描画の時点でα値が１．０にされるので、描画指定の後に追加
	pUIGroup_->SetAlphaSceneUIAndButton(0.f);


	//フラグを下す
		//経過時間が遷移時間の半分になっていないかのフラグ
	isStoped_ = false;
}

//遷移強制停止	
void FadeInAndFadeOut::EndTransition()
{
	//各要素の解放
	pUIGroup_ = nullptr;
	IMAGE_COUNT_ = -1;
	hCurrentImage_ = -1;
	SAFE_DELETE_ARRAY(TRANSITION_TIME_);
	SAFE_DELETE_ARRAY(STOPING_TIME_);
}
