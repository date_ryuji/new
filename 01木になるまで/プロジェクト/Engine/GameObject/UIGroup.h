#pragma once
//システム内　標準ヘッダ(usr/)
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
#include <d3d11.h>		//Direct3D　３D描画を担う　バッファー型やDirect3D用の型　ID3D11Texture2D(テクスチャのバッファー型)使用のため
#include "GameObject.h"	//ゲーム内オブジェクト　抽象クラス



		



//クラスのプロトタイプ宣言
class SceneButtonManager;
class SceneUIManager;
class ButtonStrategyParent;		//ボタン押下時の行動クラスの親クラス
class GameSceneButtonStrategy;
class HelpImageButtonStrategy;	//ヘルプ画像時のボタン押下時の行動	//本来は、GameSceneButtonStrategyトHelpImagesButtonStrategyのどちらも応用が聞くように、継承クラスを持つべき。
class Button;



/*
	クラス詳細	：ひとつのまとまった、画像、ボタンを管理するクラス
	クラス概要（詳しく）
				：UI画像や、ボタンをひとつにまとめることで、UIをグループ分けする。
				　該当UI（設定用UI）群でまとめることで、
				  とある処理を行う際に、該当のUI群一つにアクセスすることで、簡易的に該当UIを扱うことができる。
				：UIグループの表示、非表示の管理（表示、非表示を行うことで、グループ内のUIを一括表示、非表示できる）
				：SceneUIManager、SceneButtonManagerと連携することで、
				　UIをSceneUiManagerに持たせて、ButtonをSceneButtonManagerに持たせる。
				  UIGroupにて、上記2つのManagerを持たせて、UIとButtonの2つの管理を行うことができる。


	注意：GameObjectを継承するメリットはないように思える
		　UIの表示、非表示を管理するだけなので、自身(UIGroup)が、GameObject型である必要はないと思う

		：UIGroupにて管理するのは、あくまでもUI,Buttonの表示非表示である。
		　Button押下時の行動は、SceneButtonManagerにて管理。
		：Managerの各UI,Buttonなどを管理するには、
		　Managerに登録したUI順をハンドルとして取得できる。そのため、そのハンドルにてアクセスできる。
		：各UIなどにアクセスするには、SceneUiManager、SceneButtonManagerに直接ポインタを取得してアクセスする

*/
class UIGroup : public GameObject
{
//private メンバ変数、ポインタ、配列
private :

	//シーンUIマネージャー
		//詳細：UIGroupにて管理する、全てのUI画像を扱うマネージャークラス
	SceneUIManager* pSceneUIManager_;

	//シーンボタンマネージャー
		//詳細：UIGroupにて管理する、全てのボタンを扱うマネージャークラス
		//　　：ボタン押下時の行動も所有する
	SceneButtonManager* pSceneButtonManager_;

	/*
	
		注意：　にあるように
				各UIなどにアクセスするには、SceneUiManager、SceneButtonManagerに直接ポインタを取得してアクセスする
				そのため、
				各UIなどへのアクセスハンドルを所有する必要がなくなった
		

			////追加された
			////UIのUIManagerでの管理番号群
			//	//UIManagerにて、それぞれのUIを管理して、そのUIに対して、番号を0オリジンで与える。その番号で、UIの表示、非表示を管理している。
			//	//その番号を管理するVector
			//		//→だが、UIGroupから、UIManagerのUIに編集をくわえるときに、必要になる番号なので、
			//		//→UIGroupから、UIに編集することがないのであれば、必要のないものである。
			//		//→そして、仮にEnumの値を別に持つのであれば、その番号で、Enum値で、UIManagerに直接アクセスが可能になる。
			//std::vector<int> hUIs_;
			//
			////追加された
			////Buttonのポインタ群
			//std::vector<int> hButtons_;

			//↑上記の番号、あるいは、Enumの番号にて、
			//それぞれのUIManager,ButtonManagerのUI,Button群にアクセスすることが可能になるので、
			//直接UIなどのサイズを変更する必要が出てくるならば、直接、UIManager,ButtonManagerのポインタを取得するようにして、編集を行うようにする
	*/


//public ゲームループメソッド
public:
	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	UIGroup(GameObject* parent);

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前引数設定（UIGroupごとに名前を設定したい場合））
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	UIGroup(GameObject* parent, const std::string& OBJ_NAME);

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	/*ゲッターセッター*********************************************************/
		/*
			UI,Buttonのマネージャーのポインタを取得し、
			取得先で、画像の編集をするために、サイズ変更のために、直接アクセスするようにする
		*/


	//UIGroupのシーンUIマネージャーの取得
		//詳細：各UIの詳細設定（UIのサイズ設定、UIの奥行き設定など）のためにマネージャー取得。
		//　　：登録UIへのアクセスには、登録時のUIの登録順をハンドルとし、アクセスする
		//引数：なし
		//戻値：シーンUIマネージャー
	SceneUIManager* GetSceneUIManager();

	//UIGroupのシーンButtonマネージャーの取得
		//詳細：各Buttonの詳細設定（Buttonのサイズ設定、Buttonの奥行き設定など）のためにマネージャー取得。
		//　　：登録Buttonへのアクセスには、登録時のButtonの登録順をハンドルとし、アクセスする
		//引数：なし
		//戻値：シーンButtonマネージャー
	SceneButtonManager* GetSceneButtonManager();

	/*各UI,Buttonの登録*************************************************************************/

	//UIGroup管理のボタンの追加
		//詳細：UIGroup->SceneButtonManagerへボタンオブジェクトの追加
		//引数：ボタンオブジェクト
		//引数：ボタン押下時の行動ストラテジー
		//戻値：SceneButtonManagerの可変長配列に登録されたときの登録番号（ハンドル）（ボタンにアクセスする際のハンドルとなる）
	int AddStackButton(Button* pButton, ButtonStrategyParent* pStrategy);

	//UIGroup管理のUI画像の追加
		//詳細：UIGroup->SceneUIManagerへUIオブジェクトの追加
		//　　：引数ファイル名から読み込んだ画像を登録する
		//引数：UIの画像ファイル名
		//戻値：SceneUIManagerの可変長配列に登録されたときの登録番号（ハンドル）（ボタンにアクセスする際のハンドルとなる）
	int AddStackUI(const std::string& UI_TEXTURE_FILE_NAME);

	//UIGroup管理のUI画像の追加
		//詳細：UIGroup->SceneUIManagerへUIオブジェクトの追加
		//　　：引数テクスチャバッファーから登録
		//引数：UIのテクスチャバッファー
		//戻値：SceneUIManagerの可変長配列に登録されたときの登録番号（ハンドル）（ボタンにアクセスする際のハンドルとなる）
	int AddStackUI(ID3D11Texture2D* pTexture);

	/*各UI,Buttonの描画設定*************************************************************************/
	//UIGroup管理の全UI, 全Buttonの表示、非表示を切り替える
		//詳細：表示の場合、非表示の反転を行う
		//引数：なし
		//戻値：なし
	void VisibleInvertSceneUIAndButton();

	//UIGroup管理の全UIの表示、非表示を切り替える
		//詳細：表示の場合、非表示の反転を行う
		//引数：なし
		//戻値：なし
	void VisibleInvertSceneUI();

	//UIGroup管理の全Buttonの表示、非表示を切り替える
		//詳細：表示の場合、非表示の反転を行う
		//引数：なし
		//戻値：なし
	void VisibleInvertSceneButton();

	//UIGroup管理の全UI、全Buttonの表示
		//詳細：強制的に表示
		//　　：SceneUiManager、SceneButtonManagerに表示を指示
		//引数：なし
		//戻値：なし
	void VisibleSceneUIAndButton();
	//UIGroup管理の全UI表示
		//詳細：強制的に表示
		//　　：SceneUiManagerに表示を指示
		//引数：UI画像へのハンドル
		//戻値：なし
	void VisibleSceneUI(int handle);

	//UIGroup管理の全UI、全Buttonの非表示
		//詳細：強制的に非表示
		//　　：SceneUiManager、SceneButtonManagerに表示を指示
		//引数：なし
		//戻値：なし
	void InvisibleSceneUIAndButton();
	//UIGroup管理の全UI非表示
		//詳細：強制的に非表示
		//　　：SceneUiManagerに非表示を指示
		//引数：UI画像へのハンドル
		//戻値：なし
	void InvisibleSceneUI(int handle);

	//UIGroup管理の全UI、全Buttonのα値をセット
		//詳細：画像のα値をセットして、透過率を設定
		//　　：その場合、png画像を扱う場合、背景透過部分の画像があり、その透過率を変更すると、背景透過部分が黒く表示されるので注意（しかし、それは、元のα値に設定透過率のα値を掛けることで、pngの背景透過を残したまま透過率を上げることができる）
		//引数：α値
		//戻値：なし
	void SetAlphaSceneUIAndButton(float alpha);

	

};