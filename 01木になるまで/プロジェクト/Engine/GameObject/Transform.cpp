#include "Transform.h"				//ヘッダ
#include "../DirectX/Direct3D.h"	//Direct3D
						//ウィンドウの広さを取得するため
						//Direct３Dは、名前空間→なので、publicもprivateも存在しない
						//Direct3D::　と書けばアクセス可能

//名前空間のスコープ省略
using namespace Direct3D;	//以下では、Direct３D::を省略できる

//コンストラクタ
Transform::Transform(): 
	pParent_(nullptr)
{
	//ベクトルの初期化
	//移動値0
	position_ = XMVectorSet(0, 0, 0, 0);
	//回転値0
	rotate_ = XMVectorSet(0, 0, 0, 0);	
	//拡大率１倍（拡大率なので１倍である（注意）（0倍は何も表示されない））
	scale_ = XMVectorSet(1, 1, 1, 0);	

	//行列の初期化
		//意味を持たない行列を確保
	//移動行列
	matTranslate_ = XMMatrixIdentity();	
	//回転行列
	matRotate_ = XMMatrixIdentity();	
	//拡大行列
	matScale_ = XMMatrixIdentity();
}

//デストラクタ
Transform::~Transform()
{
}

//各行列の計算
void Transform::Calclation()
{
	//行列を作成する
	/*
		各ベクトル
		（移動、回転、拡大）を使用して、
		行列へ変換
	*/


	//移動行列
		//詳細：移動ベクトルを使用して、移動行列を作成する
		//　　：移動成分をそれぞれ行列の形へまとめる
	matTranslate_ = XMMatrixTranslation(
		position_.vecX,
		position_.vecY,
		position_.vecZ);

	//回転行列
		//詳細：回転行列はｘ軸回転の行列、y軸回転の行列、z回転の行列と３つ作って
		//　　：3つの回転行列をそれぞれ掛けて、1つの回転行列とする。（3つの軸を考慮した回転行列に）
		//　　：回転ベクトルでの管理は、度。行列に使用するのはラジアンのため。変換を忘れず
	XMMATRIX matX = XMMatrixRotationX(XMConvertToRadians(rotate_.vecX));	//X軸回転の回転行列
	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(rotate_.vecY));	//Y軸回転の回転行列
	XMMATRIX matZ = XMMatrixRotationZ(XMConvertToRadians(rotate_.vecZ));	//Z軸回転の回転行列
	//３つの軸回転の行列を１つに合わせる（掛け算）
		//詳細：掛ける順番を考えないといけない（掛け算の順番で結果が変わってくる）
	matRotate_ = matZ * matX * matY;
		/*
			Unityだと、ｚ＊ｘ＊ｙの順番で掛けられている
			→★エンジンによって、掛ける順を替えないといけない→そもそも、一つの変数にまとめてしまっているのもよくないかも
				→１つ１つ、軸回転の行列をメンバとして持っておく必要があるかも
		*/

	//拡大行列（移動行列）
	matScale_ = XMMatrixScaling(
		scale_.vecX, 
		scale_.vecY, 
		scale_.vecZ);
}

//ワールド行列を渡す
	//移動＊回転＊拡大　の３つの行列を掛けることで、ワールド行列となる
	//この時、ワールド行列は、親のワールド行列を考慮し、自身のワールド行列と掛けることで、世界における自身のワールド座標というものを求めることが可能となる
XMMATRIX Transform::GetWorldMatrix()
{
	//行列の掛ける順番
	//�@拡大
	//�A回転
	//�B移動

	//※回転の前に移動を行ってしまうと、原点における回転となってしまうため、大きなずれが生じる

	//親のワールド行列を考慮したワールド行列を求める
	//親がいない場合は
		//自身の行列のみ返す（一番上の親まで自身と親の行列を返してもらう）
	if (pParent_ == nullptr)
	{
		//自身のワールド行列（拡大＊回転＊移動）
		return matScale_ * matRotate_ * matTranslate_;	
	}

	//自身のワールド行列　＊　親のワールド行列
	return matScale_ * matRotate_ * matTranslate_ * pParent_->GetWorldMatrix();


		/*
		//再帰が非常に便利
		//子供＊親にしないと本来の目的の形を表現できない
		//子供　＊　pParent_->GetWorldMatrix()にすれば！
			//→親のGetWorldMatrixで、親の行列がもらえる。→さらに親のGetWorldMatrixをもらおうとするので、再帰的に一番上の親までの親の行列を掛けたWorld行列をもらえる
			//子供＊親の順番で行列の掛け算をしないといけないので、この再帰の形が便利になる
		*/
}
	

/*
	回転、移動、拡大の順番による結果の違い

	右に　１ｍ
	４５度　回転
	横	　　２倍

	移動してから、　回転すると→原点からの回転なので、違う
	回転してから、拡大すると→斜めの状態から、拡大すると求めるものとは違う

*/
