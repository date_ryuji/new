#include "UIGroup.h"						//ヘッダ
#include "../DirectX/Texture.h"				//テクスチャ
#include "../Scene/SceneButtonManager.h"	//シーンボタンマネージャー（Button）
#include "../Scene/SceneUIManager.h"		//シーンUIマネージャー（画像）


//コンストラクタ
UIGroup::UIGroup(GameObject * parent)
	: UIGroup(parent , "UIGroup")
{
}

//コンストラクタ
UIGroup::UIGroup(GameObject * parent, const std::string& OBJ_NAME):
	GameObject(parent , OBJ_NAME),
	pSceneButtonManager_(nullptr),
	pSceneUIManager_(nullptr)
{
}

//初期化
void UIGroup::Initialize()
{

	//インスタンス生成
		//UIGroupの中で、それぞれ、
		//SceneのButton管理、
		//SceneのUI管理のクラスのインスタンスを生成する

		//※上記において「Sceneの」という表現を使うが、あくまでも今回の場合「UIGroup内での」という表現が適切であるかもしれない
	//SceneのButton管理クラス
	pSceneButtonManager_ = (SceneButtonManager*)Instantiate<SceneButtonManager>(this);
	//SceneのUI管理クラス
	pSceneUIManager_ = (SceneUIManager*)Instantiate<SceneUIManager>(this);

}

//更新
void UIGroup::Update()
{
}

//描画
void UIGroup::Draw()
{
}

//解放
void UIGroup::Release()
{
}

//UIGroupのシーンUIマネージャーの取得
SceneUIManager * UIGroup::GetSceneUIManager()
{
	return pSceneUIManager_;
}

//UIGroupのシーンButtonマネージャーの取得
SceneButtonManager * UIGroup::GetSceneButtonManager()
{
	return pSceneButtonManager_;
}

//UIGroup管理のボタンの追加
int UIGroup::AddStackButton(Button* pButton, ButtonStrategyParent* pStrategy)
{
	//条件：シーンボタンマネージャーが存在する
	if (pSceneButtonManager_ != nullptr)
	{
		//ボタンインスタンスの追加
			//戻値：SceneButtonManager内のボタンへのハンドル
		return pSceneButtonManager_->AddStack(pButton, pStrategy);
	}
	//登録できなかった
	return -1;
}

//UIGroup管理のUI画像の追加
int UIGroup::AddStackUI(const std::string& UI_TEXTURE_FILE_NAME)
{
	//条件：シーンUIマネージャーが存在する
	if (pSceneUIManager_ != nullptr)
	{
		//UI画像インスタンスの追加
			//戻値：SceneUIManager内のUI画像へのハンドル
		return pSceneUIManager_->LoadAndAddStack(UI_TEXTURE_FILE_NAME);
	}
	//登録できなかった
	return -1;
}

//UIGroup管理のUI画像の追加
int UIGroup::AddStackUI(ID3D11Texture2D* pTexture)
{
	//条件：シーンUIマネージャーが存在する
	if (pSceneUIManager_ != nullptr)
	{
		//テクスチャバッファをUI画像管理して登録
		return pSceneUIManager_->LoadAndAddStack(pTexture);
	}
	//登録できなかった
	return -1;
}

//UIGroup管理の全UI, 全Buttonの表示、非表示を切り替える
void UIGroup::VisibleInvertSceneUIAndButton()
{
	//UIの表示、非表示切り替え
	VisibleInvertSceneUI();
	//Buttonの表示、非表示切り替え
	VisibleInvertSceneButton();
}

//UIGroup管理の全UIの表示、非表示を切り替える
void UIGroup::VisibleInvertSceneUI()
{
	//条件：シーンUIマネージャーが存在する
	if (pSceneUIManager_ != nullptr)
	{
		//全UIの表示、非表示切り替え
		pSceneUIManager_->VisibleInvertAllSceneUI();
	}
}

//UIGroup管理の全Buttonの表示、非表示を切り替える
void UIGroup::VisibleInvertSceneButton()
{
	//条件：シーンボタンマネージャーが存在する
	if (pSceneButtonManager_ != nullptr)
	{
		//全Buttonの表示、非表示切り替え
		pSceneButtonManager_->VisibleInvertAllSceneButton();
	}
}

//UIGroup管理の全UI、全Buttonの表示
void UIGroup::VisibleSceneUIAndButton()
{
	//α値を１．０ｆに初期化
		//一度１．０ｆにて初期化し、必ず視認できるα値にする
	SetAlphaSceneUIAndButton(1.f);


	//条件：シーンUIマネージャーが存在する
	if (pSceneUIManager_ != nullptr)
	{
		//全UIの表示、
		pSceneUIManager_->VisibleAllSceneUI();
	}
	//条件：シーンボタンマネージャーが存在する
	if (pSceneButtonManager_ != nullptr)
	{
		//全Buttonの表示、
		pSceneButtonManager_->VisibleAllSceneButton();
	}
}

//UIGroup管理の全UI表示
void UIGroup::VisibleSceneUI(int handle)
{

	//α値を１．０ｆに初期化
		//一度１．０ｆにて初期化し、必ず視認できるα値にする
	SetAlphaSceneUIAndButton(1.f);

	//条件：シーンUIマネージャーが存在する
	if (pSceneUIManager_ != nullptr)
	{
		//指定ハンドル番号を持つ、UIの表示、
		pSceneUIManager_->VisibleSceneUI(handle);
	}

}

//UIGroup管理の全UI、全Buttonの非表示
void UIGroup::InvisibleSceneUIAndButton()
{
	//条件：シーンUIマネージャーが存在する
	if (pSceneUIManager_ != nullptr)
	{
		//全UIの非表示、
		pSceneUIManager_->InvisibleAllSceneUI();
	}
	//条件：シーンボタンマネージャーが存在する
	if (pSceneButtonManager_ != nullptr)
	{
		//全Buttonの非表示、
		pSceneButtonManager_->InvisibleAllSceneButton();
	}
}

//UIGroup管理の全UI非表示
void UIGroup::InvisibleSceneUI(int handle)
{
	//条件：シーンUIマネージャーが存在する
	if (pSceneUIManager_ != nullptr)
	{
		//指定ハンドル番号を持つ、UIの表示、
		pSceneUIManager_->InvisibleSceneUI(handle);
	}
}

//UIGroup管理の全UI、全Buttonのα値をセット
void UIGroup::SetAlphaSceneUIAndButton(float alpha)
{
	//全UI, 全Buttonのα値をセットする
		//その際、pngの背景透過をデフォルトで使用している場合（画像自体に、背景透過のα値がセットされている場合）
		//指定のα値をセットすると、背景透過が切られてしまうので注意。

	//→解決→シェーダーにて、指定α値　＊デフォルトのα値とすることで、元の色を残したまま、（背景透過部部含む）指定α値にセットが可能


	//条件：シーンUIマネージャーが存在する
	if (pSceneUIManager_ != nullptr)
	{
		pSceneUIManager_->SetAlphaSceneUI(alpha);
	}
	//条件：シーンボタンマネージャーが存在する
	if (pSceneButtonManager_ != nullptr)
	{
		pSceneButtonManager_->SetAlphaSceneButton(alpha);
	}
}
