#pragma once
#include <DirectXMath.h>					//XMVECTOR型　3次元ベクトル　など3次元処理のソースをまとめたヘッダ
#include "../../CommonData/GlobalMacro.h"	//共通マクロ

//スコープの省略
using namespace DirectX;

/*
	クラス詳細	：位置ベクトル、回転ベクトル、拡大ベクトルの所有（GameObjectにて使用することで、オブジェクトごとのローカル座標を管理できる）
	クラス概要（詳しく）
				：位置、回転、拡大ベクトルを行列に変換↓
				：位置、回転、拡大行列を使用することで、3つの行列を計算するとワールド行列を作成可能。
		  　　　：自身のワールド行列と親とのTransformからのワールド行列の計算を使用して、　Transform管理のローカル座標をワールド座標に変化することが可能
*/
class Transform
{
//public メンバ変数、ポインタ、配列
public:

	//行列
	//移動行列
		//詳細：位置ベクトルから行列を作成する
	XMMATRIX matTranslate_;	
	//回転行列
		//詳細：回転ベクトルから行列を作成する
	XMMATRIX matRotate_;
	//拡大行列
		//詳細：拡大ベクトルから行列を作成する
	XMMATRIX matScale_;	
		/*
			上記3つの行列を乗算することで、
			ワールド行列となる
			（親のTransformからのワールド行列と掛けることで、Transformの親子関係が作られる（親が移動したら、子供Transformも移動））
		*/

	//ベクトル
	//位置ベクトル
		//詳細：ローカル位置座標
	XMVECTOR position_;	
	//回転ベクトル
		//詳細：ローカル回転値
	XMVECTOR rotate_;	
	//拡大ベクトル
		//詳細：ローカル拡大値
	XMVECTOR scale_;	
		

	//自身の親オブジェクトのTransform
		//詳細：親のTransformと、自身のTransformを計算することで、Transformの親子関係を作成可能
		//　　：親が移動すれば、自身も移動する（回転、拡大も同様に）
		//　　：そのため、自身のTransformというのは、親からのローカルの値ということになる
		//制限：親のTransform。これは解放してはいけない→なぜなら、すでに宣言されているものを入れるだけのポインタであるため
	Transform* pParent_;

//public メソッド
public : 

	//コンストラクタ
		//引数：なし
		//戻値：なし
	Transform();

	//デストラクタ
		//引数：なし
		//戻値：なし
	~Transform();

	//各行列の計算
		//詳細：自身のベクトルから、各行列を作る
		//引数：なし
		//戻値：なし
	void Calclation();	

	//ワールド行列を取得
		//詳細：自身のワールド行列と親のGetWorldMatrix()の結果を取得（親の関数にて、親自身のワールド行列とさらに親のGetWorldMatrix　となるため、親をたどっていく）
		//　　：上記によりTransformの親子関係を作る。
		//　　：親のTransformが反映されるため、親が移動すれば、子供も移動する
	XMMATRIX GetWorldMatrix();	
};