#pragma once

/*Define定数***********************************************************************************/
//Direct2D,DirectWriteを使用したテキスト描画の際のテキスト色
#define TEXT_COLOR_BLACK		D2D1::ColorF::Black				//黒
#define TEXT_COLOR_WHITE		D2D1::ColorF::White				//白
#define TEXT_COLOR_RED			D2D1::ColorF::Red				//赤
#define TEXT_COLOR_BLUE			D2D1::ColorF::Blue				//青
#define TEXT_COLOR_VIOLET		D2D1::ColorF::Violet			//ヴァイオレット
#define TEXT_COLOR_GREEN		D2D1::ColorF::Green				//緑
#define TEXT_COLOR_GREEN_YELLOW		D2D1::ColorF::GreenYellow	//黄緑
