#include <windows.h>		//Windows専用機能
#include "TextWriter.h"		//ヘッダ


//コンストラクタ
TextWriter::TextWriter(DELIMITER_CHAR type):
	text_(""),
	type_(type),
	delimiter_(GetDelimiterChar(type))
{
}

//デストラクタ
TextWriter::~TextWriter()
{
}

//区切り文字を取得
char TextWriter::GetDelimiterChar(DELIMITER_CHAR type)
{
	char delimiters[DELIMITER_MAX] = 
	{
		',',	//COMMA
		'\n',	//NEW_LINE
	};

	return delimiters[type];
}

//指定要素番目に文字列を追加
void TextWriter::AddString(const std::string& STR , int suffix)
{
	//suffix番目の文字を右にずらして、
	//文字列を代入する
	text_.insert(suffix , STR);
}

//書き込み文字の合計文字数（合計バイト数）取得
int TextWriter::GetStringLength()
{
	//lengthによって、
	//sizeと、同様の文字列のサイズを返す（\0を除いた全体長）
		//そのため、文末に、文字を追加したいときは、　GetStringLength() + 1の位置にinsert (insertは、引数にて示された元要素番号に格納された、文字を右にずらして、そこに格納させる)
	return (int)text_.length();
}

//文字列の語尾に区切り文字を結合
std::string TextWriter::GetCombineStrAndDelimiter(const std::string& STR)
{
	return STR + delimiter_;
}

//文字列語尾に改行追加
void TextWriter::PushBackNewLine()
{
	//文末に追加
	text_.push_back('\n');
}

//文字列の文字書き込み終了
void TextWriter::PushEnd()
{
	PushBackNewLine();
}

//文字列語尾に文字追加
void TextWriter::PushBackString(const std::string& STR, bool isDelimiter)
{
	//追加文字
	std::string addText;

	
	//出力文字を確定
	if (isDelimiter)
	{
		addText = GetCombineStrAndDelimiter(STR);
	}
	else
	{
		addText = STR;
	}



	//文字を追加
	//文末に追加
	AddString(addText, GetStringLength());

}

//文字列先頭に文字追加
void TextWriter::PushFrontString(const std::string& STR, bool isDelimiter)
{
	//追加文字
	std::string addText;


	//出力文字を確定
	if (isDelimiter)
	{
		addText = GetCombineStrAndDelimiter(STR);
	}
	else
	{
		addText = STR;
	}



	//文字を追加
	//先頭に追加
	AddString(addText, 0);

}

//文字列語尾に区切り文字の追加
void TextWriter::PushBackDelimiter()
{
	//文末に追加
	text_.push_back(delimiter_);
}

//ファイルへの書き込み実行
void TextWriter::FileWriteExcecute(const std::string& FILE_PATH)
{
	//ファイルを作成
	//からのファイルを作り、　hfileにハンドルを入れる。
	HANDLE hFile;        //ファイルのハンドル

	//ファイル作成
	hFile = CreateFile(
		FILE_PATH.c_str(),                 //ファイル名
		GENERIC_WRITE,           //アクセスモード（書き込み用）
		0,                      //共有（なし）
		NULL,                   //セキュリティ属性（継承しない）
		CREATE_ALWAYS,           //作成方法
									/*
									・作成方法

										〇新しくファイルを作る（同名のファイルがあるとエラー）：CREATE_NEW （新規保存）

										〇新しくファイルを作る（同名のファイルがあると上書き）：CREATE_ALWAYS (上書き保存)

										□ファイルを開く（その名前のファイルがなければエラー）：OPEN_EXISTING （）

										□ファイルを開く（その名前のファイルがなければ作る）   ：OPEN_ALWAYS


									*/

		FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
		NULL);                  //拡張属性（なし）

	//ファイルにデータを書き込む
	//そのどこから保存するかの、位置
	DWORD dwBytes = 0;  //書き込み位置（０＝一番頭から）

	//書き込み実行
	WriteFile(
		hFile,						//ファイルハンドル
		text_.c_str(),				//保存するデータ（文字列）
		(DWORD)std::strlen(text_.c_str()),		//書き込む文字数(サイズ)；文字数を出す関数＝strlen
		&dwBytes,                //書き込んだサイズを入れる変数(頭から保存して、dwBytesに文字数、書き込んだデータのサイズが足されるので、　dwBytesには、次の書き込む位置が入ることになる。
		NULL);                   //オーバーラップド構造体（今回は使わない）


	//ハンドル閉じる
	CloseHandle(hFile);



}
