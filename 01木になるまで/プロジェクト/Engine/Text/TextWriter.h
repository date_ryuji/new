#pragma once
//システム内　標準ヘッダ(usr/)
#include <string>	//文字列を使用するためにインクルード


//文字の区切りタイプ
	//詳細：文字をテキスト出力する際に、
	//　　：文字単位を区切る区切る文字として使用する文字を指定する際に使用するタイプ
enum DELIMITER_CHAR
{
	DELIMITER_COMMA = 0,	//カンマ
	DELIMITER_NEW_LINE,		//改行

	DELIMITER_MAX,			//MAX

};


/*
	クラス詳細	：テキストファイル書き出しクラス
	クラス概要（詳しく）
				：テキストをファイルに書き込むクラス
				：書込みの文字列を追加していき、
				　かつ、
				  区切り文字で区切っていくことで、CSVのような法則性に則った、テキストを格納可能

	制限：出力可能ファイル
		：DELIMITER_CHAR:：DELIMITER_NEW_LINEの場合、　.txt 
		：DELIMITER_CHAR:：DELIMITER_COMMA　 の場合、  .txt , .csv


*/
class TextWriter
{
//private メンバ変数、ポインタ、配列
private : 

	//書き込み文字（保存用）
	std::string text_;

	//区切り文字タイプ
	DELIMITER_CHAR type_;
	
	//区切り文字
		//詳細：区切り文字タイプより、区切り文字の文字をchar型にて保存しておく
	char delimiter_;

//private メソッド
private :

	//区切り文字(char)の取得
		//詳細：引数タイプから、区切り文字の取得
		//引数：区切り文字タイプ
		//戻値：区切り文字（char）
	char GetDelimiterChar(DELIMITER_CHAR type);

	//指定要素番目に文字列を追加
		//引数：追加文字
		//引数：文字列をついかする要素番目
	void AddString(const std::string& STR, int suffix);

	//書き込み文字の合計文字数（合計バイト数）取得
		//引数：なし
		//戻値：合計文字数
	int GetStringLength();

	//文字列の語尾に区切り文字を結合
		//引数：結合の文字
		//戻値：区切り文字結合後の文字
	std::string GetCombineStrAndDelimiter(const std::string& STR);

//public メソッド
public : 

	//コンストラクタ
		//引数：区切り文字タイプ
		//戻値：なし
	TextWriter(DELIMITER_CHAR type);
	//デストラクタ
		//引数：なし
		//戻値：なし
	~TextWriter();


	//文字列語尾に文字追加
		//引数：追加文字列
		//引数：文字追加時　区切り文字を代入するか
		//戻値：なし
	void PushBackString(const std::string& STR , bool isDelimiter);

	//文字列先頭に文字追加
		//引数：追加文字列
		//引数：文字追加時　区切り文字を代入するか
		//戻値：なし
	void PushFrontString(const std::string& STR , bool isDelimiter);

	//文字列語尾に区切り文字の追加
		//引数：なし
		//戻値：なし
	void PushBackDelimiter();

	//文字列語尾に改行追加
		//引数：なし
		//戻値：なし
	void PushBackNewLine();

	//文字列の文字書き込み終了
		//詳細：改行して終了させる
		//　　：書き込み終了として、改行で次の行とする。CSVとして読み込むとき、改行が連続する場合、CSV読み取りをが終了する
		//引数：なし
		//戻値：なし
	void PushEnd();

	//ファイルへの書き込み実行
		//詳細：文字列として書き込んでいた文字列の入力を終了し、ファイルとして出力
		//引数：書き込み先のファイルパス（テキストファイルとして書き込む　書込み先のファイルパス）
		//戻値：なし
	void FileWriteExcecute(const std::string& FILE_PATH);


};

