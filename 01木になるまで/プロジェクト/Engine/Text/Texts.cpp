#include <string>		//string　文字列扱い関係
#include <typeinfo>		//typeinfo　変数型ごとの文字を取得するための機能
#include "Texts.h"		//テキスト


//初期化
void Texts::Initialize()
{
	//描画のための必要ポインタの取得
	pSolidBrush_ = Direct2D::GetBrush();
	pDeviceContext_ = Direct2D::GetDeviceContext();
	
	
	//初期のテキストフォーマット（主に、テキスト、フォントサイズ）を設定
		//フォントサイズを切り替える関数で、引数フォントサイズでテキストフォーマットを作成
	pTextFormat_ = nullptr;
	//初期フォントサイズ
	SetFontSize(30);
}

//ブラシサイズの変更
void Texts::SetFontSize(float fontSize)
{
	fontSize_ = fontSize;

	//ブラシサイズ、フォントサイズを変更する
		//そのため、Direct2Dにアクセスして、テキストフォーマットとして、テキストサイズを変更する
		//自身のメンバで所有しているポインタを送り、新しいテキストのフォーマットを入れてもらう
			//関数先で、引数として渡した、ポインタを解放してから、新しいテキストフォーマットを作成
	//条件：処理の失敗
	if (FAILED(Direct2D::CreateTextFormat(&pTextFormat_, fontSize)))
	{
		//エラーメッセージ
		MessageBox(nullptr, "テキストのフォントサイズ変更失敗", "エラー", MB_OK);
	};

}

//データの型を調べる
template<class E>
DATA_TYPE Texts::GetDataType(E text)
{
	//型ごとのtypeid（型を識別する文字）取得
	const char i = typeid(int).name()[0];
	char f = typeid(float).name()[0];
	char d = typeid(double).name()[0];
	char c = typeid(char).name()[0];
	

	//引数の型を調べる
	char type = typeid(text).name()[0];


	//データタイプ型(enum)
	DATA_TYPE dataType;

	//条件：int型である
	if (i == type)
	{
		//データタイプ：INT
		dataType = DATA_TYPE::INT_TYPE;
	}
	//条件：float型である
	//　　：&&
	//　　：double型である
	else if(f == type
		|| d == type)
	{
		//データタイプ：float
		dataType = DATA_TYPE::FLOAT_TYPE;
	}
	//条件：char型である
	else if (c == type)
	{
		//データタイプ：char
		dataType = DATA_TYPE::CHAR_TYPE;
	}
	else
	{
		//データタイプ：該当なし（MAX）
		dataType = DATA_TYPE::MAX_DATA_TYPE;
	}


	//データタイプ型を返す
	return dataType;
}
//テンプレートの引数として渡されるであろう型を網羅させる
template DATA_TYPE Texts::GetDataType<int>(int); //追加
template DATA_TYPE Texts::GetDataType<double>(double);
template DATA_TYPE Texts::GetDataType<float>(float);
template DATA_TYPE Texts::GetDataType<char>(char);


//描画（文字列以外）
template<class T>
void Texts::DrawTex(const T textValue, D2D1::ColorF::Enum color, int posX, int posY)
{
	//引数値からデータのタイプ型を取得する
	DATA_TYPE dataType = GetDataType<T>(textValue);

	//データを取得
	//文字長定義	
		//256ビットまで描画できるとする
	static constexpr int maxDistance = 256;

	//文字配列の宣言
	WCHAR dispText[maxDistance] = {0};

	//描画できるかのフラグ
	bool isDataType = true;

	//データタイプから
	//出力文字をWCHAR型に変更
		//フォーマット識別子を変更し、
		//文字列に変更
	switch (dataType)
	{
	case DATA_TYPE::INT_TYPE:
		//int型のフォーマットを指定して、Wchar配列の文字に変換
		swprintf(dispText, maxDistance, L"%ld", (int)textValue);	
		break;

	case DATA_TYPE::FLOAT_TYPE:
		//float(double)型のフォーマットを指定して、Wchar配列の文字に変換
		swprintf(dispText, maxDistance, L"%lf", (float)textValue);
		break;
	case DATA_TYPE::CHAR_TYPE:
		//char型のフォーマットを指定して、Wchar配列の文字に変換
		swprintf(dispText, maxDistance, L"%lc", (char)textValue);
		break;
	
	default:
		isDataType = false;	//描画ができないため、フラグを下ろす
		return; break;
	}



	//条件：データが描画できるならば
	//描画
	if (isDataType)
	{
		//ブラシ色を変更
			//引数値から色を取得
		pSolidBrush_->SetColor(D2D1::ColorF(color));

		//描画
		//引数：テキスト文字列（ワイド文字列へのポインタ（wchar_t*））
		//引数：文字列サイズ
		//引数：テキストフォーマット（テキストフォントなど）
		//引数：テキスト描画Rect（描画位置X,　描画位置Y,　描画テキストボックスのX,  描画テキストボックスのY）
		//引数：テキストブラシ（テキストサイズ）
		//引数：オプション
		pDeviceContext_->DrawTextA(dispText, ARRAYSIZE(dispText) - 1, pTextFormat_,

			D2D1::RectF((FLOAT)posX, (FLOAT)posY, (FLOAT)Direct2D::scrWidth, (FLOAT)Direct2D::scrHeight),

			pSolidBrush_, D2D1_DRAW_TEXT_OPTIONS_NONE);

	}

}
//テンプレート関数を
	//ヘッダとソースで分けた時のコンパイル時エラーをなくすために、　明示的に使用例のようなものを宣言させる
	//そして、　引数として渡されるであろう、型をすべて、ここで網羅しなければ、エラーになる
	//そのため、型が何が来るのか、わからない場合は、　ヘッダに宣下してしまうのが一番
template void Texts::DrawTex<int>(int, 
	D2D1::ColorF::Enum color , int posX, int posY); 
template void Texts::DrawTex<double>(double, 
	D2D1::ColorF::Enum color, int posX, int posY);
template void Texts::DrawTex<float>(float, 
	D2D1::ColorF::Enum color, int posX, int posY);
template void Texts::DrawTex<char>(char, 
	D2D1::ColorF::Enum color, int posX, int posY);




//文字列の描画
void Texts::DrawStringTex(const std::string& TEXT , D2D1::ColorF::Enum color, int posX, int posY)
{

	//テキストの描画

	//文字列から、Wstirng型への変換
		//扱い方は、stringも、wstringも変わらない
		//c_str()：string = char*のポインタ、wstring = wchar_t*のポインタ
		//length():文字列の長さ、サイズ
	std::wstring wString = StringToWString(TEXT);

	//ブラシの色を変える
	pSolidBrush_->SetColor(D2D1::ColorF(color));

	//描画
		//引数：テキスト文字列（ワイド文字列へのポインタ（wchar_t*））
		//引数：文字列サイズ
		//引数：テキストフォーマット（テキストフォントなど）
		//引数：テキスト描画Rect（描画位置X,　描画位置Y,　描画テキストボックスのX,  描画テキストボックスのY）
		//引数：テキストブラシ（テキストサイズ）
		//引数：オプション
	pDeviceContext_->DrawTextA(wString.c_str(), (UINT32)wString.length(), pTextFormat_,
		D2D1::RectF((FLOAT)posX, (FLOAT)posY, (FLOAT)Direct2D::scrWidth, (FLOAT)Direct2D::scrHeight), pSolidBrush_, D2D1_DRAW_TEXT_OPTIONS_NONE);

}




//String型からwstring型へ
std::wstring Texts::StringToWString(const std::string& O_STRING)
{
	//文字列をワイド文字列（Unicode）へマップ
		//参考サイト：
		//http://www.t-net.ne.jp/~cyfis/win_api/sdk/MultiByteToWideChar.html

	//SJIS → wstring
	int iBufferSize = MultiByteToWideChar(CP_ACP, 0, O_STRING.c_str()
		, -1, (wchar_t*)NULL, 0);

	//バッファの取得
	wchar_t* cpUCS2 = new wchar_t[iBufferSize];

	//SJIS → wstring
	MultiByteToWideChar(CP_ACP, 0, O_STRING.c_str(), -1, cpUCS2
		, iBufferSize);

	//stringの生成
	std::wstring oRet(cpUCS2, cpUCS2 + iBufferSize - 1);

	//バッファの破棄
	delete[] cpUCS2;

	//ワイド文字返す
	return oRet;
}

