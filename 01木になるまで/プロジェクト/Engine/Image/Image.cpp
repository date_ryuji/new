#include "Image.h"								//ヘッダ
#include "../../CommonData/GlobalMacro.h"		//共通マクロ

/*
	Vector管理

	//要素登録の使い方はListと同じ
	//vectorは、普通の配列のように番号で扱えるので楽。（追加もpush_back）

	シーン終了まで
	画像のリソースは解放しない
	そのため
	Vectorという、途中解放に適さない可変長配列を使用している


*/


/*Image(namespace)*/
namespace Image
{
	//画像情報群（別名：データ群）
		//詳細：画像のデータを入れておく構造体の動的配列（可変長配列）
		//　　：ImageDataの可変長配列（vector型：途中で要素を消したりしない）
	std::vector<ImageData*> datas;		

}


//ロード
int Image::Load(const std::string& FILE_NAME, SHADER_TYPE thisShader)
{

	//条件：ファイルが存在しない場合
		//第一引数：ファイルのパス　を示す変数のポインタ（string型はc_str()にて、　自身のポインタを送ることが可能）
	if (!PathFileExists(FILE_NAME.c_str()))
	{
		//ファイルが存在しないとき
			//−１　：　エラーを返す
		return -1;	
	}

	/*
		�@構造体のインスタンスを作成。
		　データ格納先を確保

		�A引数ファイル名から、すでにロード済みのリソースがある場合、
		　そのリソースを共有する（フライウェイト）

		�BSpriteオブジェクトを作成し、ロードする

		�C構造体の中身が埋まったので動的配列に追加
			//Modelにて確保した動的な配列（可変長配列）に作成して情報を生成した構造体を登録

		�D番号（配列の要素数-1）を返す
			//配列の要素数を出して、−１→つまり、２個入っていれば、１が入る(０オリジン、作成した構造体が追加された要素番号)

	*/

	//�@
	//モデルのデータを入れる構造体のインスタンス生成
	ImageData* pData = new ImageData;	//動的確保

	//構造体メンバのファイル名に引数の内容を登録
	pData->fileName = FILE_NAME;


	//�A
	/*
	引数ファイル名からすでにリソースとして確保済みならば
		Spriteのロードをするところに
		→datasの中に、Spriteが存在するならば、
		→リソースをコピーする
		→newをしないで、Loadしないで、
		→新規に作成した構造体の中のSpriteポインタに、既に存在するSpriteのぽいんたのあドレスを入れてやる

		無駄なリソースを確保せずに済む
	*/
	//スコープ：�@
	//vectorの動的配列を頭から回す(回し方はイテレータ必要なし（なぜなら、要素数もわかるし、vectorは、配列と同じ扱い方なので）)
	//条件：現在のデータ群数
	for (int i = 0; i < datas.size(); i++)
	{
		//スコープ：�A
		//条件：すでに同一のファイルが存在していたら
			//ロードしない
			//同一ファイルの判断は、ファイル名から判断
		if (datas[i]->fileName == FILE_NAME)
		{
			//イテレータによって示されているdatasのメンバであるfileNameと
			//引数のfileNameが同じなら　＝　すでにSpriteファイルはロードされている

			//作成した構造体のメンバpSpriteに、ファイル名が同じのdatasのpSpriteのアドレスを代入
			pData->pSprite = datas[i]->pSprite;

			//抜ける
				//スコープ：�@
			break;
		}
	}


	//�B
	//すでにリソースが確保済みでなかった場合
	//新たにリソースを確保する必要がある場合
		//上記のfor分を抜けてもなお、pSpriteのポインタがnullptrならば
		//上記のfor文によって、同様のSpriteファイルを見つけられなかった
	if (pData->pSprite == nullptr)
	{
		//Spriteオブジェクトの新規作成

		//Spriteオブジェクトのインスタンス生成（ポインタによって動的確保）
		pData->pSprite = new Sprite;

		//Spriteオブジェクトのロード（メンバのファイル名を使用して）
		if (FAILED(pData->pSprite->Initialize(pData->fileName.c_str(), thisShader)))
		{
			//エラーの原因をメッセージボックスとして表示
			MessageBox(nullptr, "Spriteファイルのロードの失敗", "エラー", MB_OK);
			pData->pSprite->Release(); SAFE_DELETE(pData->pSprite);
			SAFE_DELETE(pData);
			
			//−１を返す（ロードしたときにー１が帰ってきたら、Load側で、assartにてエラーを返すようにする。）
			return -1;
		}
	}


	//シェーダーをセット
		//描画を行うときにセットする
	pData->thisShader = thisShader;

	//�C
	//作成した構造体のインスタンスを動的配列に追加
	datas.push_back(pData);

	//�D
	//動的配列のサイズ（要素数）−１
		//今回作成した構造体が動的配列に登録された際の添え字が返される
	return (int)(datas.size()) - 1;
}

//ロード
int Image::Load(const std::string& FILE_NAME, ID3D11Texture2D* pTexture, SHADER_TYPE thisShader)
{

	//モデルのデータを入れる構造体のインスタンス生成
	ImageData* pData = new ImageData;	//動的確保

	//構造体メンバのファイル名に引数の内容を登録
	pData->fileName = FILE_NAME;

	//テクスチャから追加する場合
	//重複管理は行わない

	//Spriteオブジェクトの新規作成

	//Spriteオブジェクトのインスタンス生成（ポインタによって動的確保）
	pData->pSprite = new Sprite;

	//Spriteオブジェクトのロード（メンバのファイル名を使用して）
	if (FAILED(pData->pSprite->Initialize(pTexture, thisShader)))
	{
		//エラーの原因をメッセージボックスとして表示
		MessageBox(nullptr, "Spriteファイルのロードの失敗", "エラー", MB_OK);
		pData->pSprite->Release(); SAFE_DELETE(pData->pSprite);
		SAFE_DELETE(pData);
		//−１を返す（ロードしたときにー１が帰ってきたら、Load側で、assartにてエラーを返すようにする。）
		return -1;
	}


	//シェーダーをセット
		//描画を行うときにセットする
	pData->thisShader = thisShader;


	//作成した構造体のインスタンスを動的配列に追加
	datas.push_back(pData);

	//動的配列のサイズ（要素数）−１
		//今回作成した構造体が動的配列に登録された際の添え字が返される
	return (int)(datas.size()) - 1;
}

//指定されたモデルデータのトランスフォームをセット
void Image::SetTransform(int handle, Transform& transform)
{
	//ハンドルによって、示された、動的配列のモデルデータを選択
	//そのモデルデータの構造体が持っているTransformに引数のTransformを更新
	datas[handle]->transform = transform;

}

//指定された画像データのTransfromをセットする
void Image::SetTransform(int handle, Transform* transform)
{
	datas[handle]->transform = (*transform);
}

//指定された画像データのA値をセットする
void Image::SetAlpha(int handle, float alpha)
{
	datas[handle]->alpha = alpha;
}

//指定されたモデルデータのSpriteを描画
void Image::Draw(int handle)
{
	//ハンドルによって示されたモデルデータ
	//そのSpriteを描画させる

	//α値をセットする
	datas[handle]->pSprite->SetAlpha(datas[handle]->alpha);

	//シェーダーをセットする
	datas[handle]->pSprite->ChangeShader(datas[handle]->thisShader);

	//SpriteをDraw（引数にてTransformをおくる（この際のTransformは、モデルデータの自身の構造体が持っているTransform））
	datas[handle]->pSprite->Draw(datas[handle]->transform);
}

//解放
void Image::Release()
{
	//全ての画像データを解放
	ReleaseAllImageData();
}

//画像データの全消去
void Image::ReleaseAllImageData()
{
	//動的配列内の全データの解放
	//条件：データ群数
	for (int i = 0; i < datas.size(); i++)
	{
		//条件：まだ解放されていなければ
		if (datas[i] != nullptr)
		{
			//データ配列の動的確保したポインタの解放
			//まだ、他で使用されているSpriteデータなどは、解放しないようにする（以下の関数にて）
			ReleaseOneData(i);
		}

	}

	//配列の中身をクリア
	datas.clear();
}

//指定ハンドルのデータ解放
void Image::ReleaseOneData(int handle)
{
	//条件：ハンドルが0未満
	//  　：||
	//　　：ハンドルが音楽ファイル群数　以上
	//　　：||
	//　　：画像データが解放されている
	//考えられるエラーの際に解放を行わずに帰る
	if (handle < 0 || handle >= datas.size() || datas[handle] == nullptr)
	{
		return;
	}

	//自身の所有している、
	//まだ、解放されてないSpriteのポインタをまだ持っているか
	//同じモデルを他でも使っていないか
	bool isExist = false;
	//スコープ：�@
	//まだ解放されていない中に、
	//自身の所有しているデータ部のポインタが、ある場合、それは開放しない。
	//解放されていない中になければ、初めて解放する
	//条件：データ群数
	for (int i = 0; i < datas.size(); i++)
	{
		//スコープ：�A
			//条件：まだ解放されていない
			//　　：&&
			//　　：自身のハンドルと同じでない
			//　　：&&
			//　　：バッファーのポインタが共通である
		if (datas[i] != nullptr 	
			&& i != handle 
			&& datas[i]->pSprite == datas[handle]->pSprite)
		{
			//解放しないとする
				//→まだ、解放されていないデータの中でSpriteポインタを所有している場合消さない
				//→データの中で、配列の一番最後に使われているSpriteファイルが解放を行うようにする。
					//→自分のデータよりも要素が前、後ろのデータの中で同じデータを持っている場合は×。

			//他での同じ画像データを使用している
			isExist = true;

			//処理を抜ける
				//スコープ：�@
			break;
		}
		//最後まで全データを回して、
		//フラグにtrueが入らなかった場合、
		//まだ、解放されていない中で同じSpriteポインタを持つものがいない　＝　Spriteポインタを解放
	}

	//解放されていない、他データ内で
	//Spriteポインタを使ってなければモデル解放
	//条件：バッファーのデータを所有しているほかの画像情報が存在しない場合
	if (isExist == false)
	{
		//解放
		datas[handle]->pSprite->Release();
		SAFE_DELETE(datas[handle]->pSprite);
	}

	//自分のデータを解放（配列の要素を動的確保しているので、解放）
	SAFE_DELETE(datas[handle]);
}

//ピクセル単位の移動（引数ピクセル分移動）
void Image::MovePixelPosition(int handle, int x, int y)
{
	//実行関数を呼び出す
	MovePixelPosition(datas[handle]->transform, x, y);
}

//引数にて渡されたTransformをピクセル単位で変形する
void Image::MovePixelPosition(Transform& trans, int x, int y)
{
	//ワールド座標に変換し
	//移動量を取得
	XMVECTOR moveVec = MoveWorldToPixelConversion(x, y);

	//移動するため、
	//現在のTransfromにワールド座標である割合を足す
	trans.position_.vecX += moveVec.vecX;
	trans.position_.vecY += moveVec.vecY;
}

//引数ピクセル位置に移動
void Image::SetPixelPosition(int handle, int x, int y)
{
	//実行関数の呼び出し
	SetPixelPosition(datas[handle]->transform, x, y);
}

//引数にて渡されたTransformをピクセル位置へ移動
void Image::SetPixelPosition(Transform& transform, int x, int y)
{
	/*
		ピクセル単位での移動

		//ピクセル単位で管理できると良いことは
		//UIだと　ワールド座標で移動を管理するより、　ピクセル管理で動かせた方が楽
		//位置管理をするときも、位置調整をしやすい


		//引数ピクセル位置のため、
		//ｘは、数が大きければ、　→へ
		//ｙは、数が大きければ、　↓へ
		移動する
	*/

	//ワールド座標に変換する
	XMVECTOR setVec = GetWorldToPixelConversion(x, y);

	//現在のTransfromにワールド座標である割合を設置する
	transform.position_.vecX = setVec.vecX;
	transform.position_.vecY = setVec.vecY;
}

//奥に一段階下がる
void Image::LowerOneStep(int handle)
{
	//画像の前後関係を一段階下げる
		//画像を＋Z方向へ0.001ほど移動させる
		//それにより、画像のサイズは多少の移動で、画像同士の前後関係を作る
		//＋Z方向へ移動させることで、画像が移動させていない画像よりも、奥にいくため、前後関係が生まれる
	datas[handle]->transform.position_.vecZ += 0.001f;
}

//画像ピクセルサイズの変更
void Image::SetPixelScale(int handle, int scaleX, int scaleY)
{
	//テクスチャの標準サイズの取得
	XMVECTOR textureSize = datas[handle]->pSprite->GetMyTextureSize();

	//実行関数の呼び出し
	SetPixelScale(
		datas[handle]->transform,
		textureSize,
		scaleX, scaleY);
}

//引数にて渡されたTransformと、指定ハンドルにてピクセルサイズの調整
void Image::SetPixelScale(Transform& transform, int handle, int scaleX, int scaleY)
{
	//テクスチャの標準サイズの取得
	XMVECTOR textureSize = datas[handle]->pSprite->GetMyTextureSize();

	//実行関数の呼び出し
	SetPixelScale(
		transform,
		textureSize,
		scaleX, scaleY);
}

//引数にて渡されたTransformと、元テクスチャサイズにてピクセルサイズの調整
void Image::SetPixelScale(Transform& transform, XMVECTOR& textureSize, int scaleX, int scaleY)
{
	//指定ピクセルに拡大、縮小

	//方法
	/*

		画像のサイズをピクセル感覚で拡大、縮小といっても、
		実際、画像ファイル自体をいじるのではなく、

		画像のTransformを拡大、縮小して、
		そのピクセルサイズに変更するやり方。


		1000 のサイズを　500へ　→　500 / 1000 = 0.5f;

		→TransformのScaleを0.5にセット
		→ピクセルを500にする　＝　Transformの割合で表した値では、　1000ピクセルのサイズが1.0f倍であるならば、そのサイズを0.5倍したということ


		★元画像ピクセル / 指定画像ピクセル　＝　scale値
	*/



	//画像の拡大、縮小値を取得
		//もともとのピクセルサイズがある中で、引数のピクセル値は、もともとの何倍なのか
	//計算
	XMVECTOR texPer = XMVectorSet(scaleX / textureSize.vecX, scaleY / textureSize.vecY, 0.f, 0.f);

	//セットする
	transform.scale_.vecX = texPer.vecX;
	transform.scale_.vecY = texPer.vecY;

}

//画像の元テクスチャサイズを取得
XMVECTOR Image::GetDefaultTextureSize(int handle)
{
	return datas[handle]->pSprite->GetMyTextureSize();
}

//ワールド（プロジェクション）空間における移動量を求める
XMVECTOR Image::MoveWorldToPixelConversion(int x, int y)
{
	//わざわざ、スクリーン座標に直さなくても、
	//スクリーンのサイズから
		//1ピクセルにおけるワールドの（Transform.positonにおける）ｘｙ移動値を計算する
		//上記を　登録してあるTransform.positionに登録する

	//ワールドにおける　１がスクリーンの右端に位置する、０がスクリーンの左端に位置する
	//１にて　スクリーンサイズの半分移動することにする

	int scrWidth = Direct3D::scrWidth;
	int scrHeight = Direct3D::scrHeight;
	float w = scrWidth / 2.0f;
	float h = scrHeight / 2.0f;


	//割合を取得
	float percentageX = ((float)x / w);
	float percentageY = ((float)y / h);
	//ここにおける割合がワールド座標となる
	//１移動することで、　スクリーンサイズの半分を移動することになる。　引数にて渡されたピクセルを、　スクリーンサイズ半分で割ったときの割合がワールド座標における座標となる

	//Vectorのｘｙに求めた値を入れて返す
	return XMVectorSet(percentageX, percentageY, 0.f, 0.f);

}

//引数ピクセルをワールド（プロジェクションにおける）における座標に変換
XMVECTOR Image::GetWorldToPixelConversion(int x, int y)
{
	int scrWidth = Direct3D::scrWidth;
	int scrHeight = Direct3D::scrHeight;

	//引数ピクセル値が
	//スクリーンの範囲外でないか調べる
	if (x > scrWidth || x < 0 || y > scrHeight || y < 0)
	{
		return XMVectorSet(0.f, 0.f, 0.f, 0.f);	//範囲外
	}


	//引数ピクセル位置を
	//プロジェクションにおける座標に変換

		//プロジェクションの座標　x:−１〜１ y:−１〜１ z:０〜１


	//引数ピクセルを２倍にして
	//scrWidth ＝　スクリーンの横幅　と徐分をとる

		//その徐分は　もともとがscrWidth 分の値であった場合、　scrWidth * 2 / scrWidth = 2 となる

		//その値を　-1.0f とすれば、　プロジェクションを　-1 ~ 1の範囲に切り詰めることが可能

	float subX = (x * 2) / (float)Direct3D::scrWidth;
	float subY = (y * 2) / (float)Direct3D::scrHeight;


	//上記で求めた
	//差分の割合から-1.0fをして、切り詰める
	float percentageX = subX - 1.0f;			//左から右へ（つまり、プロジェクションにおける小さいから大きいへ）
	float percentageY = (subY - 1.0f) * -1.0f;	//y座標は、ピクセルが大きくなればなるほど　↓に行く　（つまり、プロジェクションにおいては、値が大きいから小さいへ）
												//そのため、Yは反転



	return XMVectorSet(percentageX, percentageY, 0.f, 0.f);
}

//画像のシェーダーを切り替える
void Image::ChangeShader(int handle, SHADER_TYPE shaderType)
{

	//Spriteのメソッドである、
		//シェーダー切替えの関数を呼び、Spriteのシェーダーを切り替える
		//※一度切り替えたら、それはずっとそのシェーダーのまま。自動で戻される処理などは入らないので注意
	datas[handle]->pSprite->ChangeShader(shaderType);
}

//Spriteクラスの次のDrawで優先するポインタをセットする
void Image::SetPriorityTexture(int handle, Texture* pPriorityTexture)
{
	datas[handle]->pSprite->SetPriorityTexture(pPriorityTexture);
}

//現在のシェーダーをもらう
SHADER_TYPE Image::GetShader(int handle)
{
	return datas[handle]->pSprite->GetMyShader();
}

//画像の当たり判定
void Image::HitCollision(int handle, ImageCollision* imageCollision)
{
	//Transformセット
	datas[handle]->transform.Calclation();
	imageCollision->pTrans = &datas[handle]->transform;

	//画像の衝突判定を呼び込む
	datas[handle]->pSprite->HitCollision(imageCollision);
}

