#pragma once
//システム内　標準ヘッダ(usr/)
#include <DirectXMath.h>	//XMVECTOR型　3次元ベクトル　など3次元処理のソースをまとめたヘッダ

//スコープの省略
using namespace DirectX;

//クラスのプロトタイプ宣言
class GameObject;
class BoxCollider;
class SphereCollider;
class DebugWireFrame;	


//コライダーの種類
	//詳細：コライダーごとの当たり判定を行うコライダーの種類により、計算方法を変更する必要がある。そのため、自身のタイプを識別するために使用sる
enum COLLIDER_TYPE
{
	COLLIDER_TYPE_SPHERE = 0,	//球形
	COLLIDER_TYPE_BOX,			//箱（四角形の六面体）

	COLLIDER_TYPE_MAX,		//MAX

};
//当たり判定の機能の種類
	//詳細：
	//　　：・機能なし（通常のあたり判定のみ）
	//　　：・壁ずり（当たり判定を持つオブジェクトと衝突時、侵入した分だけ押し出される（自身だけ）接触したオブジェクトの面に沿って、押し出される）
	//　　：・押し出し（当たったオブジェクトを押し出す（相手を動かす、自身は動かない））
enum COLLIDER_FUNCTION_TYPE
{
	COLLIDER_FUNCTION_NONE = 0,			//機能なし
	COLLIDER_FUNCTION_ALONG_THE_WALL ,	//壁ずり（壁に沿う）
	COLLIDER_FUNCTION_EXTRUDE,			//押し出す（壁ずりが、機能なしのコライダーとも壁ずりを行うようになるので、機能なしとほとんど機能は変わらない）

	COLLIDER_FUNCTION_MAX,				//MAX

};


/*
	クラス詳細	：GameObjectに持たせる 当たり判定コライダーの親クラス
	クラスレベル：抽象クラス
	クラス概要（詳しく）
				：GameObjectに付与させた　コライダー、オブジェクト同士の当たり判定を担うコライダー。
				：コライダーの種類（各種類にクラスを継承させて、各オブジェクトごとの判定を取らせる）
					・スフィア　球形
					・ボックス　六面体（四角形）
				：当たり判定の種類
					・スフィア×スフィア
					・ボックス×スフィア
					・ボックス×ボックス


*/
class Collider
{
//protected メンバ変数、ポインタ、配列
protected :

	//自身のコライダータイプ
		//詳細：コライダーを識別するタイプ
		//　　：当たり判定を実行する際に、自身のタイプと相手のタイプによって、計算方法を変更する。
	COLLIDER_TYPE myColliderType_;	
	//自身のコライダー機能タイプ
		//詳細：通常の当たり判定と別に、当たり判定のコライダーを対象にコライダー接触時に起こる機能を識別する
	COLLIDER_FUNCTION_TYPE myFunctionType_;	

	//当たり判定　を判定するか、しないかのフラグ
	bool doJudge_;	

	//自身コライダーを視認させるモデルクラス
		//詳細：デバック用の、ワイヤーフレームのサイズを視認できるようにする、ワイヤーフレームモデル
		//　　：コライダーのワールド座標に描画するコライダー―用モデル
	DebugWireFrame* pDebugWireFrame_;


	//自身と衝突したコライダーポインタ
		//詳細：衝突判定後の押し出しの処理時に使用する
		//制限：押し出しなどの処理を行わない場合、使用しない
	Collider* pTarget_;

	//自身のコライダーを所有しているオブジェクト（GameObject）
	GameObject* pGameObject_;


	/*毎フレーム更新*************************************************/
	//オブジェクトからのコライダーの位置（ワールドではない）
		//詳細：オブジェクトの原点からのコライダーの原点の位置、離れ具合（付加オブジェクトからのローカル座標）
		//　　：オブジェクト（GameObject）の座標（Transform.position_）からのローカル座標
	XMVECTOR myPos_;

	//コライダーを付けたオブジェクトの位置
		//詳細：オブジェクト（GameObject）の座標（Transform.position_）
	XMVECTOR objectPos_;

	//コライダーの移動方向
		//詳細：前回との自身の座標との差分によるコライダーの移動方向
	XMVECTOR moveDire_;

	//コライダーのワールド座標
		//詳細：コライダー世界における座標
	XMVECTOR worldPos_;

//protected メソッド
protected :
	//コライダーサイズをワイヤーフレームモデルクラス（DebugWireFrame）に送る
		//詳細：コライダーサイズに沿ったモデルを描画するため、サイズを送り、サイズ指定
		//　　：継承先にて、各コライダータイプごとにサイズ調整に調整が必要。そのため、継承先にて各調整を行ってサイズ指定を行う
		//レベル：仮想関数
		//引数：なし
		//戻値：なし
	virtual void SetWireFrameSize();

	//自身のコライダータイプをセットする
		//詳細：コライダータイプ（COLLIDER_TYPE(enum)）を設定
		//引数：コライダータイプ（COLLIDER_TYPE(enum)）
		//戻値：なし
	void SetMyType(COLLIDER_TYPE colliderType);

	//球コライダーのサイズ調整関数（COLLIDER_TYPE：SPHERE）
		//詳細：float にて示される半径からサイズを変更
		//引数：半径
		//戻値：なし
	void SetWireFrameSize(float radius);

	
	//箱コライダーのサイズ調整関数（COLLIDER_TYPE：BOX）
		//詳細：XMVECTOR にて示される半径からサイズを変更
		//引数：サイズ
		//戻値：なし
	void SetWireFrameSize(XMVECTOR& size);

	//衝突したコライダーをメンバに登録する
		//詳細：今回衝突したコライダーを保存し、衝突したときの押し出し処理などに使用する
		//引数：保存コライダー
		//戻値：なし
	void SetTargetCollider(Collider* pTarget);

	//ターゲットのコライダーと自身のコライダーを判別してターゲットのコライダーを上記の関数（SetTargetCollider）で登録する
		//詳細：2つのコライダーから自身以外のコライダーを判別して、自身以外のコライダーをメンバに登録
		//引数：コライダー１
		//引数：コライダー２
		//戻値：なし
	void JudgeTargetCollider(Collider* pColl1, Collider* pColl2);

	//球コライダーを所有しているモデル（GameObject）のScale値を取得
		//詳細：上記値を考慮して、モデルサイズとコライダーのサイズを考慮した最終サイズを求める
		//　　：球専用：球は楕円のようにゆがまない、半径が常に等しくなるように設定したい
		//　　　　　　：そのため、ｘｙｚともに等しく可変するために、サイズを取得する
		//　　　　　　：この時、vecXのScale値を取得するようにするため、ｘｙｚそれぞれが違うと、想定通りの可変にならない可能性もある。
		//引数：なし
		//戻値：GameObjectのXサイズ
	float GetGameObjectScaleForSphere();
	//箱コライダーを所有しているモデル（GameObject）のサイズを取得
		//詳細：箱専用：ｘｙｚそれぞれを可変するために、それぞれのサイズを取得する
		//　　　　　　：ｘｙｚそれぞれのScale分、箱のサイズのｘｙｚを可変
		//引数：なし
		//戻値：GameObjectのサイズ
	XMVECTOR GetGameObjectScaleForBox();

	
	//引数に手渡されるコライダーからサイズを取得し、サイズを自身を所有しているコライダーのサイズ分可変させる
		//詳細：自身を付加しているGameObjectのサイズと自身のコライダーサイズを 考慮したサイズを求めることで、コライダーのワールドサイズを求める
		//引数：コライダーのポインタ(球専用)
		//戻値：コライダーのワールドサイズ（球コライダーの半径）
	float GetVariableSize(SphereCollider* pSpColl);

	//引数に手渡されるコライダーからサイズを取得し、サイズを自身を所有しているコライダーのサイズ分可変させる
		//詳細：自身を付加しているGameObjectのサイズと自身のコライダーサイズを 考慮したサイズを求めることで、コライダーのワールドサイズを求める
		//引数：コライダーのポインタ(箱専用)
		//戻値：コライダーのワールドサイズ（箱コライダーのサイズ）
	XMVECTOR GetVariableSize(BoxCollider* pBoxColl);

//public メソッド
public : 

	//コンストラクタ
		//詳細：各コライダークラスから、メンバとして持たせる情報以外、objectPosなどをもらい、　親のプロテクテッド変数に渡す
		//引数：コライダーのオブジェクトからの離れ具合（GameObjectからのローカル座標）
		//引数：自身のコライダータイプ（COLLIDER_TYPE(enum)）
		//引数：自身のコライダーの機能のタイプ（COLLIDER_FUNCTION_TYPE(enum)）
		//引数：ワイヤーフレームを描画するか（DebugWireFrameの描画をするか）
		//戻値：なし
	Collider(XMVECTOR position , COLLIDER_TYPE collType , COLLIDER_FUNCTION_TYPE funcType, bool drawWireFrame);


	//デストラクタ
		//レベル：仮想関数
		//引数：なし
		//戻値：なし
	virtual ~Collider();


	//コライダーをセットしているゲームオブジェクトをセット（コライダーを付加させているGameObject）
		//引数：自身をセットしているゲームオブジェクトのポインタ
		//戻値：なし
	void SetMyGameObject(GameObject* pGameObject);

	//コライダーのモデル（DebugWireFrame(class)）のポインタをもらう
		//引数：なし
		//戻値：コライダー表示用のモデルクラス
	DebugWireFrame* GetDebugWireFrame();

	//コライダーのモデル（DebugWireFrame(class)）のモデル番号をもらう
		//詳細：当たり判定を行うモデルを判別するために
		//　　：当たり判定を行うためには、モデルのモデルハンドルが必要。
		//戻値：モデルハンドル
	int GetDebugWireFrameHandle();

	//コライダーのモデル（DebugWireFrame(class)）の描画時のTransform値にモデルをセットする
		//詳細：レイキャストを行う際に、モデルを描画時のTransformに移動させて、から、レイキャストを行う
		//引数：なし
		//戻値：なし
	void SetDebugWireFrameDrawingToTransform();


	//コライダー判定フラグを立てる
		//詳細：コライダーフラグを立てると、コライダー機能を持つ。当たり判定も行う
		//引数：なし
		//戻値：なし
	void DoJudge();

	//コライダー判定フラグを下す
		//詳細：コライダーフラグを下ろすと、コライダー機能を持たなくなる。
		//引数：なし
		//戻値：なし
	void DoNotJudge();

	//衝突判定を行う
		//詳細：自身と引数コライダーとの当たり判定
		//　　：継承先にてそれぞれ、衝突判定式が行える関数を呼び込む
		//　　：自身が球なら、SphereColliderのIsHitが呼ばれるようにし、　IsHitから、球と　相手のコライダーを調べて、そのタイプから、自身と相手の関係性（球と球なのかなど）を調べて、その関係性から計算関数を呼ぶコム
		//レベル：純粋仮想関数
		//引数：相手のコライダー　ポインタ
		//戻値：衝突したか
	virtual bool IsHit(Collider* target) = 0;

	//デバック用のワイヤーフレーム描画（DebugWireFrame(class)）
		//詳細：コライダー描画が指定されていた場合、コライダー範囲を視認するために描画を行う
		//制限：コライダー描画は、基本デバック時にのみ行うようにする
		//引数：なし
		//戻値：なし
	void Draw();
	
	//デバック用のワイヤーフレーム描画の許可、非許可
		//制限：コライダー描画は、基本デバック時にのみ行うようにする
		//　　：コライダーがリリース時に表示されるのは良くない。コライダーのような見た目のオブジェクトがゲームに必要ならば、コライダーとは別のモデルで用意する必要がある
		//引数：なし
		//戻値：なし
	void DrawWireFrame(bool drawWireFrame);


	//コライダーをつけたモデル（GameObject(class)）親の位置をセットする（毎フレーム）
		//詳細：毎フレーム衝突判定を行う前に、
		//    ：コライダーを所有しているオブジェクトの位置をセットしてもらう（コライダーのワールド座標は、オブジェクトの位置＋オブジェクトからのコライダーの位置）
		//引数：コライダーを付加しているオブジェクトのワールド座標
		//戻値：なし
	void SetObjectPos(XMVECTOR objectPosition);

	//コライダーをセットしているゲームオブジェクト（GameObject(class)）を取得
		//詳細：衝突判定終了後、衝突したコライダーを引数に接触判定時に呼ばれる関数を呼び込む。その際に、コライダーを所有しているオブジェクトについて情報を知りたい。そのため所有できるメソッドを呼び込む
		//引数：なし
		//戻値：コライダーをセットしているゲームオブジェクト
	GameObject* GetMyGameObject();


	/*衝突判定********************************************************/
		/*
			アクセスレベル public の理由
			
			衝突判定の相手の以下の関数へアクセスするとき、protectedだと、アクセスが出来なくなるので、注意（相手は、おなじColliderを継承していても、　実際の親子関係はない、ただの兄弟なので、　アクセスは当然できない）
		*/
	
	//コライダーのワールド座標を返す
		//詳細：戻値は、コライダーをつけたオブジェクト位置＋オブジェクトからのコライダーの位置（つまりコライダーのワールド座標が出せる）
		//　　：コライダーのローカル座標＋オブジェクトのワールド座標　＝　コライダーのワールド座標
		//引数：なし
		//戻値：自身のコライダーのワールド座標
	XMVECTOR GetWorldPos();

	//自身のコライダータイプを渡す
		//詳細：コライダーの計算方法確定のために、コライダータイプを取得
		//    ：自身のコライダータイプ、相手のコライダータイプを受けて、当たり判定の計算を指定する
		//引数：なし
		//戻値：自身のコライダータイプ
	COLLIDER_TYPE GetMyType();

	//（衝突判定計算）球(自身)と球(相手)の当たり判定
		//詳細：SphereCollider（自身）より呼ばれる
		//　　：計算に必要な項目を、SphereColliderのIsHitから呼ぶときに、関数の引数として渡す
		//引数：判定を行う　SphereColliderのポインタ（自身）
		//引数：判定を行う　SphereColliderのポインタ（相手）
		//戻値：衝突したか
	bool IsHitSphereToSphere(SphereCollider* pSphere1, SphereCollider* pSphere2);

	//（衝突判定計算）箱(自身)と球(相手)の当たり判定
		//引数：判定を行う　BoxColliderのポインタ（自身）
		//引数：判定を行う　SphereColliderのポインタ（相手）
		//戻値：衝突したか
	bool IsHitBoxToSphere(BoxCollider* pBox1, SphereCollider* pSphere1);

	//（衝突判定計算）箱(自身)と箱(相手)の当たり判定
		//引数：判定を行う　BoxColliderのポインタ（自身）
		//引数：判定を行う　BoxColliderのポインタ（相手）
		//戻値：衝突したか
	bool IsHitBoxToBox(BoxCollider* pBox1, BoxCollider* pBox2);

	/*壁ずり判定********************************************************/
		/*
			コライダー同士が侵食していた場合（衝突＝侵食）

			侵食分を押し出す必要がある。
			この押し出す行為を壁ずりという
		*/

	//自身のコライダーの機能タイプを選択
		//引数：コライダー機能のタイプ（COLLIDER_FUNCTION_TYPE(enum)）
		//戻値：なし
	void SetColliderFunctionType(COLLIDER_FUNCTION_TYPE type);

	//壁ずり判定
		//詳細：自身が壁ずりの時、（COLLIDER_FUNCTION_TYPE(enum)）
		//　　：相手のコライダータイプと、コライダー機能タイプによって、壁ずりの処理を行わせる。
		//　　：”自身”を相手コライダーから押し出す
		//　　：基本的には、　相手のコライダーに沿わせて、壁ずりで移動させる
		//　　：ここにおける相手とは、Updateの処理の前に行わせた、衝突判定で、衝突した相手である。（pTarget_）
		//　　：常に最新の相手が入り、相手がいないときはnullptrが入る。
	void AlongTheWall();

	//自身が壁ずりができるかの確認
		//詳細：自身が壁ずり可能タイプ（COLLIDER_FUNCTION_TYPE(enum)）　＆＆　相手コライダーが存在する（pTarget_）（相手のコライダータイプ、コライダー処理関係なし）
		//引数：なし
		//戻値：壁ずり実行可能
	bool CanColliderAlongTheWall();

	//壁ずりベクトルを求める
		//詳細：壁ずり処理を行い、自身が壁ずりをする、その壁ずり方向ベクトルを取得する
		//制限：壁ずりの方向は求められるが、現段階では、壁ずりの移動量は求められない
		//引数：進行ベクトル
		//引数：法線ベクトル
		//戻値：壁ずりベクトル
	XMVECTOR CalcWallScratchVector(XMVECTOR& progressVec, XMVECTOR& normalVec);
		
};

