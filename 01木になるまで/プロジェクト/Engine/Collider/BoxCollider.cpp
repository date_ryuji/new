#include "BoxCollider.h"		//ヘッダ
#include "SphereCollider.h"		//コライダークラス　相手のコライダーが球である場合を考えてインクルード

//コンストラクタ
BoxCollider::BoxCollider(XMVECTOR position, XMVECTOR size, bool drawWireFrame):
	Collider::Collider(position , COLLIDER_TYPE_BOX , COLLIDER_FUNCTION_NONE ,  drawWireFrame),
	size_(size)
{
	//箱コライダーのサイズ調整関数（COLLIDER_TYPE：BOX）
		//自身のメソッドであるサイズを設定するメソッドを呼び出す
	SetWireFrameSize();
}

//デストラクタ
BoxCollider::~BoxCollider()
{
}

//コライダー同士の衝突判定
bool BoxCollider::IsHit(Collider * target)
{
	//衝突判定結果
	bool hit = false;

	//相手のコライダータイプをもらう
		//そのタイプにより
		//衝突判定の計算式を持つ関数を呼び分ける
	switch (target->GetMyType())
	{
	//相手が「球」コライダーならば
	case COLLIDER_TYPE_SPHERE:
		//衝突判定
			//箱と球
			//自身とターゲット
		hit = IsHitBoxToSphere(this , (SphereCollider*)target);
		//ターゲットは継承の親クラスのCollider型であるため
			//相手が球だとわかったら、
			//球型にキャストする
		break;

	//相手が「箱」コライダーならば
	case COLLIDER_TYPE_BOX :
		//衝突判定
			//箱と箱
			//自身とターゲット
		hit = IsHitBoxToBox(this, (BoxCollider*)target);
		break;

	//その他
	default:
		//衝突していない
		hit = false;
		break;
	}
	
	//衝突判定結果を返す
	return hit;
}

//自身のコライダーサイズを返す
XMVECTOR BoxCollider::GetSize()
{
	return size_;
}

//コライダーサイズをセットする
void BoxCollider::SetWireFrameSize()
{
	//サイズからワイヤーフレームに使用するサイズを変更
		//親のColliderクラスにある関数から、
		//XMVECTOR型から、サイズを変更する関数を呼び出す
	Collider::SetWireFrameSize(size_);
}
