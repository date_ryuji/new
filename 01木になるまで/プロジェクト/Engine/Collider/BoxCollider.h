#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "Collider.h"	//コライダー　抽象クラス



/*
	クラス詳細	：Colliderクラスを継承した、3D衝突判定用コライダー（箱）
	クラスレベル：サブクラス
	クラス概要（詳しく）
				：箱型のコライダー
				：箱型　四角形の六面体であれば、大きさに制限はない。立方体でも、直方体でも可能
*/
class BoxCollider : public Collider
{
//private メンバ変数、ポインタ、配列
private :
	//コライダーサイズ
		//詳細：親オブジェクト（GameObject）のローカルサイズ
	XMVECTOR size_;
	
//public メソッド
public : 

	//コンストラクタ
		//引数：コライダーの離れ具合（オブジェクトからのローカル座標）
		//引数：ｘｙｚ方向へのサイズ（ｘ２→原点からx方向へ１、−x方向へ１の長さ）（ローカルサイズ）
		//引数：デバック用のワイヤーフレームを描画するか（DebugWireFrame）（指定なし：false）
		//戻値：なし
	BoxCollider(XMVECTOR position , XMVECTOR size, bool drawWireFrame = false);
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~BoxCollider() override;

	//衝突判定実行
		//詳細：自身と引数コライダーとの当たり判定
		//　　：引数のコライダーのタイプを調べ、自身（箱）と　相手（箱OR球）の当たり判定実行を呼び込む（Collider（親）クラスに計算式は存在するので、その関数を呼び込む）
		//レベル：オーバーライド
		//引数：相手のコライダー　ポインタ
		//戻値：衝突したか
	bool IsHit(Collider* pTarget) override;
	
	//自身のサイズ返す
		//詳細：自身のローカルサイズ（メンバ変数）を渡す
		//引数：なし
		//戻値：ローカルサイズ
	XMVECTOR GetSize();

	//自身のサイズ（ワールドサイズ）を、ワイヤーフレームモデル（DebugWireFrame）へわたす
		//詳細：DebugWireFrameのサイズを指定するために（描画などのため）、コライダーのワールドサイズを渡す必要がある
		//　　：関数先にて、箱（自身のコライダータイプ）のサイズを指定する関数呼び込み（Collider（親）メソッドより）
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void SetWireFrameSize() override;


};

