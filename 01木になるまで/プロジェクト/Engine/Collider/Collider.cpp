#include "Collider.h"						//ヘッダ
#include "../GameObject/GameObject.h"		//GameObject型（コライダーを付加するオブジェクトクラスの継承元）
#include "../../CommonData/GlobalMacro.h"	//共通マクロ
#include "SphereCollider.h"					//コライダー「球」
#include "BoxCollider.h"					//コライダー「箱」
#include "../GameObject/DebugWireFrame.h"	//ワイヤーフレームモデル（コライダーサイズを視認できるようにした）


//コンストラクタ
Collider::Collider(XMVECTOR position ,COLLIDER_TYPE collType ,COLLIDER_FUNCTION_TYPE funcType ,  bool drawWireFrame):
	myPos_(position),
	objectPos_(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	moveDire_(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	worldPos_(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	myColliderType_(collType),
	myFunctionType_(funcType),
	pDebugWireFrame_(new DebugWireFrame),
	pTarget_(nullptr),
	pGameObject_(nullptr) , 
	doJudge_(true)
{
	//コライダーのモデル（DebugWireFrame(class)）のモデルロード
	pDebugWireFrame_->Load(collType);
	//コライダーのモデル（DebugWireFrame(class)）描画状態の設定
	pDebugWireFrame_->VisibleWireFrame(drawWireFrame);
}

//デストラクタ
Collider::~Collider()
{
}

//コライダーを付加するGameObjectを設定する
void Collider::SetMyGameObject(GameObject * pGameObject)
{
	//コライダー　メンバ内に登録
	pGameObject_ = pGameObject;
	//コライダーのモデル（DebugWireFrame(class)）へ渡す
	pDebugWireFrame_->SetParentTransform(&pGameObject_->transform_);
}
//コライダーのモデル（DebugWireFrame(class)）ポインタを返す
DebugWireFrame * Collider::GetDebugWireFrame()
{
	return pDebugWireFrame_;
}

//コライダーのモデル（DebugWireFrame(class)）モデルハンドルを返す
int Collider::GetDebugWireFrameHandle()
{
	return pDebugWireFrame_->GetModelHandle();
}

//コライダーのモデル（DebugWireFrame(class)）の描画時のTransform値にモデルをセットする
void Collider::SetDebugWireFrameDrawingToTransform()
{
	//描画時の位置へモデルをセットさせる
		//レイキャストを行うときなどに、実際の描画のTransformにセットされている必要がある
		//コライダーは、すべて共通のモデルを使用しているので、セットしなおさなければ、このColliderがセットしたTransformとは限らなくなる
	pDebugWireFrame_->SetDebugWireFrameDrawingToTransform();
}

//コライダー判定フラグを立てる
void Collider::DoJudge()
{
	doJudge_ = true;
}

//コライダー判定フラグを下す
void Collider::DoNotJudge()
{
	doJudge_ = false;
}
//衝突判定を行う
	//レベル：純粋仮想関数
bool Collider::IsHit(Collider * target)
{
	return false;
}

//デバック用のワイヤーフレーム描画（DebugWireFrame(class)）
void Collider::Draw()
{
	pDebugWireFrame_->Draw();
}

//デバック用のワイヤーフレーム描画の許可、非許可
void Collider::DrawWireFrame(bool drawWireFrame)
{
	pDebugWireFrame_->VisibleWireFrame(drawWireFrame);
}

//（衝突判定計算）球(自身)と球(相手)の当たり判定
bool Collider::IsHitSphereToSphere(SphereCollider * pSphere1, SphereCollider * pSphere2)
{
	//今回採用する衝突判定の方法
	/*
		・ベクトル型を使用した衝突判定の計算

		自身のベクトルをA、引数のベクトルをBとする。
		�@２つのベクトルの差分をとって、AからB、あるいはBからAに延びるベクトルを求める。
		�Aベクトルの長さを求める（ベクトルで）

		�B求めたベクトルの、長さと、A・Bの半径の合計より
			・以下の時
			　→衝突しているので、trueを返す
			・大きいとき
			　→衝突していないので、falseを返す
	*/

	//�@
	//anotherCollPos = 自身と衝突判定を行うコライダーの位置
	//GetWorldPos() = 自身のコライダーのワールド位置を取得（外部に渡すための関数だったが、使えるなら使う）
	XMVECTOR length = pSphere1->GetWorldPos() - pSphere2->GetWorldPos();

	//�A
	length = XMVector3Length(length);

	/*球　サイズ********************************************************************************/
	//サイズの可変
		//詳しくは、BoxToSphere関数にて
	float radiusA = GetVariableSize(pSphere1);
	float radiusB = GetVariableSize(pSphere2);
	//２つのコライダーの半径の合計を出す
		//（２つの半径より、２コライダー間の距離が長いなら）　＝　衝突していない
		//（２つの半径より、２コライダー間の距離が短いなら）　＝　衝突している
	float sumRadius = radiusA + radiusB;


	//�B
		//×長さは、ベクトルで出てくるのでX、Y、Zで判断を行い、
		/*
			if (length.vecX <= sumRadius || length.vecY <= sumRadius ||
				length.vecZ <= sumRadius)
		*/
		//〇Lengthの時点で、→ｘｙｚすべてに共通の値が入る、xyzどの値を使っても問題なし
		/*
			if (length.vecX <= sumRadius)

		*/
		//半径の合計より小さければ、衝突していることになる
	if (length.vecX <= sumRadius)
	{
		//１つでも小さいものがあるなら、
		//衝突している
		JudgeTargetCollider((Collider*)pSphere1, (Collider*)pSphere2);
		return true;

	}
	/*
		・半径 1の球コライダー
		・半径 5の球コライダー

		上記の２つが衝突されているかを判定しろ

		・上記２つの直線距離は４である

		２つの半径の合計は６
		２つの直線の距離は４

		〇２つが衝突しているときの最高の距離は６（２つの半径の合計が６なので）
		〇２つの直線距離は４
		〇２つの半径合計より距離が小さいので

		★上記の２つの球は衝突している



	*/

	SetTargetCollider(nullptr);
	//衝突しなかった
	return false;


}

//（衝突判定計算）箱(自身)と球(相手)の当たり判定
bool Collider::IsHitBoxToSphere(BoxCollider * pBox1, SphereCollider * pSphere1)
{
	
	//球体のワールド座標
	XMVECTOR spherePosA = pSphere1->GetWorldPos();
	//箱の　ワールド座標
	XMVECTOR boxPosB = pBox1->GetWorldPos();


	/*球　サイズ********************************************************************************/
	//球の半径を求める
	//球の半径を、コライダーを所有しているオブジェクトのサイズで可変させる
	//↓上記の処理を関数で行う
	float radius = GetVariableSize(pSphere1);
	//球体のサイズは、
	//半径の長さを　それぞれ、ｘｙｚとしておく＝　半径が共通の球　のみ表現できるとする
	XMVECTOR sphereSizeA = XMVectorSet(radius, radius, radius, 0.f);

	/*箱　サイズ********************************************************************************/
	//球と同様
		//箱の場合は、ｘｙｚそれぞれを、それぞれのScale値で可変させる
	XMVECTOR boxSizeB = GetVariableSize(pBox1);



	//衝突判定
		//内外判定を行い、
		//ｘｙｚの座標がそれぞれ、　内外判定を行い、座標が内側にあると判断されたら、衝突しているとフラグを返す
	if (spherePosA.vecX > boxPosB.vecX - boxSizeB.vecX  / 2 - sphereSizeA.vecX &&
		spherePosA.vecX < boxPosB.vecX + boxSizeB.vecX / 2 + sphereSizeA.vecX &&
		spherePosA.vecY > boxPosB.vecY - boxSizeB.vecY / 2 - sphereSizeA.vecX &&
		spherePosA.vecY < boxPosB.vecY + boxSizeB.vecY / 2 + sphereSizeA.vecX &&
		spherePosA.vecZ > boxPosB.vecZ - boxSizeB.vecZ / 2 - sphereSizeA.vecX &&
		spherePosA.vecZ < boxPosB.vecZ + boxSizeB.vecZ / 2 + sphereSizeA.vecX)
	{
		JudgeTargetCollider((Collider*)pBox1, (Collider*)pSphere1);
		return true;
	}
	SetTargetCollider(nullptr);
	return false;
}

//（衝突判定計算）箱(自身)と箱(相手)の当たり判定
bool Collider::IsHitBoxToBox(BoxCollider * pBox1, BoxCollider * pBox2)
{
	//ワールド座標を取得
		//コライダーをつけているオブジェクトのワールド＋　離れ具合
	XMVECTOR boxPosA = pBox1->GetWorldPos();
	XMVECTOR boxPosB = pBox2->GetWorldPos();

	
	/*箱　サイズ********************************************************************************/
	//サイズ
		//ワールド
		//ｘ方向のサイズ（ｍ）(横幅)
		//ｙ方向のサイズ（ｍ）（高さ）
		//ｚ方向のサイズ（ｍ）（奥行）
	//サイズの可変
	XMVECTOR boxSizeA = GetVariableSize(pBox1);
	XMVECTOR boxSizeB = GetVariableSize(pBox2);


	//衝突判定
	if ((boxPosA.vecX + boxSizeA.vecX / 2) > (boxPosB.vecX - boxSizeB.vecX / 2) &&
		(boxPosA.vecX - boxSizeA.vecX / 2) < (boxPosB.vecX + boxSizeB.vecX / 2) &&
		(boxPosA.vecY + boxSizeA.vecY / 2) > (boxPosB.vecY - boxSizeB.vecY / 2) &&
		(boxPosA.vecY - boxSizeA.vecY / 2) < (boxPosB.vecY + boxSizeB.vecY / 2) &&
		(boxPosA.vecZ + boxSizeA.vecZ / 2) > (boxPosB.vecZ - boxSizeB.vecZ / 2) &&
		(boxPosA.vecZ - boxSizeA.vecZ / 2) < (boxPosB.vecZ + boxSizeB.vecZ / 2))
	{
		JudgeTargetCollider((Collider*)pBox1, (Collider*)pBox2);
		return true;
	}

	//ターゲットがいなかったため、
	//nullptrを代入させる
	SetTargetCollider(nullptr);
	return false;


}

//壁ずり判定
void Collider::AlongTheWall()
{
	//壁ずりの処理を行えるかの確認を行う
	if (CanColliderAlongTheWall())
	{
		/*
		http://marupeke296.com/COL_Basic_No5_WallVector.html
		
		�@自身のコライダーと
		　相手のコライダーで差ベクトルを取る
				→自身から相手へのベクトルを求める(進行ベクトル)
		�Aその方向ベクトルと、相手のコライダーの面（三角ポリゴン）とで、接触する面を求める
		�Bその接触した面の法線ベクトルを外積で求める
		�Cその法線ベクトルと、　�@のベクトルとで、壁ずりベクトルを求める
		�D壁ずりベクトル方向へコライダーを所有している　オブジェクトを移動させる
		
		*/

		//�@
		XMVECTOR progressVec = pTarget_->GetWorldPos() - this->GetWorldPos();

		//正規化
		XMVECTOR dir = XMVector3Normalize(progressVec);
		if (moveDire_.vecX != 0.f ||
			moveDire_.vecY != 0.f ||
			moveDire_.vecZ != 0.f )
		{
			//前回の移動方向が更新されているときは
				//そちらの移動方向の方向ベクトルを優先する
			dir = XMVector3Normalize(moveDire_);
		}

		//スタート位置は、
		//自身のコライダー位置を、相手のコライダー方向へー10倍
		XMVECTOR start = this->GetWorldPos() + (dir * -10.f);

		//�A，�B
			//ワイヤーフレームの当たり判定を呼び込み、
				//ここで判定を行うのは、
				//相手のコライダーでなければいけない。
			//その相手のコライダーと当たる、法線ベクトルもらう
		XMVECTOR normalVec = pTarget_->GetDebugWireFrame()->NormalVectorOfCollidingFace(start, dir);



		//�C
		//XMVECTOR alongWallVec = CalcWallScratchVector(progressVec, normalVec);
		XMVECTOR alongWallVec = CalcWallScratchVector(dir, normalVec);
		//XMVECTOR alongWallVec = CalcWallScratchVector(moveDire_, normalVec);
		

		//ゲームオブジェクトを衝突前の座標に戻す
		//法線方向にゲームオブジェクトを移動
			//つまり、壁の法線方向に移動させて、ゲームオブジェクトを壁から押し出して、壁ずりを実行させる
		pGameObject_->transform_.position_ += XMVector3Normalize(normalVec) * 0.02f;
			//コライダーの端の部分になると、コライダーに埋まり始める、埋まると、自動に引っ張られて、移動をしてしまう→これは、おそらく、コライダーの面の切れ目にて、ちょうど、当たらなくなっていたりするのでは？
			//当たらなかった時に、一瞬入り込んでしまう
				//壁の切り目、（壁の切り目の球体に何度も入り込もうとすると、通り抜けてしまうことも多々ある。）



		//pGameObject_->transform_.position_ += XMVector3Normalize(dir) * 0.1f;
			//簡単に壁を通り抜けてしまう
		//pGameObject_->transform_.position_ -= XMVector3Normalize(progressVec) * 0.1f;
			//コライダーの原点に向かって、移動を行うと、壁ずりできなくなる。なぜならば、巻き戻る際の巻き戻しの方向が、進行方向に対して、共通


		//�D
		//壁ずりベクトル方向へ一定量移動
			//壁ずりの方向を反転させる
			//コライダーの原点と、相手のコライダーとの原点で差ベクトルをとって、それを、進行方向としてしまっているので、
			//そのままだと、右に押し出されて、左に押し出されてと常に同じ位置にとどまり続けてしまう。

		//壁ずりの方向は出たが、
			//その方向へ、どの程度移動させるのか、その強さが求められていない。
			//適切な距離を移動させなければ、適切な、壁に沿ったベクトルにならない

		pGameObject_->transform_.position_ += alongWallVec *0.1f;



	}
}

//壁ずりベクトルを求める
XMVECTOR Collider::CalcWallScratchVector(XMVECTOR & progressVec, XMVECTOR & normalVec)
{
	//法線ベクトルの正規化
	XMVECTOR normal = XMVector3Normalize(normalVec);

	//壁ずりベクトルを求める
	XMVECTOR alongWallVec = XMVector3Normalize(
		progressVec - 
		XMVector3Dot(progressVec, normal) *
		normal);


	return alongWallVec;

}

//自身が壁ずりができるかの確認
bool Collider::CanColliderAlongTheWall()
{
	//条件：相手がnullptrでない
	//　　：&&
	//　　：自身の機能タイプが　ALONG_THE_WALL_FUNCTIONである
	if (pTarget_ != nullptr && myFunctionType_ == COLLIDER_FUNCTION_ALONG_THE_WALL)
	{
		//壁ずり可能である
		return true;
	}
	//壁ずり不可能である
	return false;
}

//コライダーのワールド座標を返す
XMVECTOR Collider::GetWorldPos()
{
	//オブジェクト位置＋コライダー位置
	//このコライダーを、
		//自身を導入しているワールドTransformで変形して、
		//座標を計算させる
	Transform trans = pGameObject_->transform_;
	//トランスフォーム計算（ワールド行列作成）
	trans.Calclation();
	//オブジェクトの離れ具合と、
	//コライダーを所有しているオブジェクトのワールド行列（つまり、所有オブジェクトの移動、回転、拡大を考慮したときの行列、これをかけることで、親の位置を原点とした座標を取得可能）
	XMVECTOR worldPos = XMVector3TransformCoord(myPos_ , trans.GetWorldMatrix()) ;

	//ワールド座標を返す
	return worldPos;
}

//自身のコライダータイプを渡す
COLLIDER_TYPE Collider::GetMyType()
{
	//自身のタイプを返す
	return myColliderType_;
}

//コライダーをつけたモデル（GameObject(class)）親の位置をセットする（毎フレーム）
void Collider::SetObjectPos(XMVECTOR objectPosition)
{
	//前回のワールドポジション
		//つまり、ワールド上における座標を保存しておく
	//XMVECTOR beforePos = objectPos_;


	//オブジェクト位置の更新
	objectPos_ = objectPosition;


	//前回とのベクトルによって、差を出し
		//前回とのベクトルの差で、移動方向を出す
	//更新後のワールドポジションとの差分を取る
	//moveDire_ = XMVector3Normalize(this->GetWorldPos() - worldPos_);
	moveDire_ = this->GetWorldPos() - worldPos_;


	worldPos_ = this->GetWorldPos();

	


	//ワイヤーフレームのモデルに座標を送る
		//ここで考慮するのは、
		//コライダークラスが扱っている各情報が、どの座標なのか

		//描画において、
		//親を、仮に、ゲームオブジェクトのTransformとするならば、
			//その親を原点としての、座標となってしまう。
	//離れ具合と、オブジェクトの位置を考慮した座標を送る
		//SetObjectPosの座標に入ってくるのは、
		//親までのTransform,自身のTransformを考慮した位置となるので、
			//親に追尾する形で変わっている座標も考慮されたPositionとなる。
	//XMVECTOR worldPos = GetWorldPos();
	pDebugWireFrame_->SetPosition(myPos_);
}

//コライダーをセットしているゲームオブジェクト（GameObject(class)）を取得
GameObject * Collider::GetMyGameObject()
{
	return pGameObject_;

}

//自身のコライダーの機能タイプを選択
void Collider::SetColliderFunctionType(COLLIDER_FUNCTION_TYPE type)
{
	myFunctionType_ = type;
}

//自身のコライダータイプをセットする
void Collider::SetMyType(COLLIDER_TYPE colliderType)
{
	myColliderType_ = colliderType;
}
//コライダーサイズをワイヤーフレームモデルクラス（DebugWireFrame）に送る
	//レベル：仮想関数
void Collider::SetWireFrameSize()
{
	//箱
	//→　Vectorで、自身のサイズを送る

	//球
	//→　floatで、　自身の半径を送る

}

//球コライダーのサイズ調整関数（COLLIDER_TYPE：SPHERE）
void Collider::SetWireFrameSize(float radius)
{
	pDebugWireFrame_->SetScale(radius);
}

//箱コライダーのサイズ調整関数（COLLIDER_TYPE：BOX）
void Collider::SetWireFrameSize(XMVECTOR & size)
{
	pDebugWireFrame_->SetScale(size);
}

//衝突したコライダーをメンバに登録する
void Collider::SetTargetCollider(Collider * pTarget)
{
	pTarget_ = pTarget;
}

//ターゲットのコライダーと自身のコライダーを判別してターゲットのコライダーを上記の関数（SetTargetCollider）で登録する
void Collider::JudgeTargetCollider(Collider * pColl1, Collider * pColl2)
{
	//条件：コライダー１が自身コライダーである
	//　　：&&
	//　　：コライダー２が自身コライダーではない
	if (pColl1 == this && pColl2 != this)
	{
		//ターゲットコライダーを設定する
		SetTargetCollider(pColl2);
	}
	//条件：コライダー１が自身コライダーではない
	//　　：&&
	//　　：コライダー２が自身コライダーである
	else if(pColl1 != this && pColl2 == this)
	{
		//ターゲットコライダーを設定する
		SetTargetCollider(pColl1);
	}
}

//球コライダーを所有しているモデル（GameObject）のScale値を取得
float Collider::GetGameObjectScaleForSphere()
{
	return pGameObject_->transform_.scale_.vecX;
}

//箱コライダーを所有しているモデル（GameObject）のサイズを取得
XMVECTOR Collider::GetGameObjectScaleForBox()
{
	return pGameObject_->transform_.scale_;
}

//引数に手渡されるコライダーからサイズを取得し、サイズを自身を所有しているコライダーのサイズ分可変させる
float Collider::GetVariableSize(SphereCollider * pSpColl)
{
	//サイズ
		//ワールド
		//ｘ方向のサイズ（ｍ）(横幅)
		//ｙ方向のサイズ（ｍ）（高さ）
		//ｚ方向のサイズ（ｍ）（奥行）
	float radius = pSpColl->GetRadiusSize();
	//ここにおけるサイズは、あくまでも、登録時でのサイズなので、
		//親のモデルが、仮に、サイズの可変をされていたら、それに合わせて拡大、縮小する必要がある。
		//コライダーを、コライダーを持っているモデルのサイズに合わせるやり方をすれば、
		//・デバック用のコライダーモデルの出力にサイズが合うし、
		//・のちに、持っているモデルのサイズを変えたときに、それに合わせて、サイズを可変するという手間を省ける。
	radius *= pSpColl->GetGameObjectScaleForSphere();


	return radius;
}

//引数に手渡されるコライダーからサイズを取得し、サイズを自身を所有しているコライダーのサイズ分可変させる
XMVECTOR Collider::GetVariableSize(BoxCollider * pBoxColl)
{
	XMVECTOR boxSize = pBoxColl->GetSize();
	//箱のサイズも同様に
		//コライダーを持っているモデルのサイズ分可変する
	XMVECTOR scale = pBoxColl->GetGameObjectScaleForBox();
	boxSize = XMVectorSet(
		boxSize.vecX * scale.vecX,
		boxSize.vecY * scale.vecY,
		boxSize.vecZ * scale.vecZ,
		0.f
	);

	return boxSize;


}

