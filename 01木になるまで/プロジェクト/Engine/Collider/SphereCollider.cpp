#include "SphereCollider.h"					//ヘッダ
#include "BoxCollider.h"					//コライダークラス　相手のコライダーが箱である場合を考えてインクルード


//コンストラクタ
SphereCollider::SphereCollider(XMVECTOR position, float size, bool drawWireFrame):
	Collider::Collider(position, COLLIDER_TYPE_SPHERE , COLLIDER_FUNCTION_NONE, drawWireFrame),
	radius_(size)
{
	//球コライダーのサイズ調整関数（COLLIDER_TYPE：SPHERE）
		//自身のメソッドであるサイズを設定するメソッドを呼び出す
	SetWireFrameSize();
}

//デストラクタ
SphereCollider::~SphereCollider()
{
}

//衝突判定
bool SphereCollider::IsHit(Collider* target)
{
	//衝突判定結果を入れるフラグ
	bool hit = false;

	//相手のコライダータイプをもらう
		//そのタイプにより
		//衝突判定の計算式を持つ関数を呼び分ける
	switch (target->GetMyType())
	{
	//相手が「球」ならば
	case COLLIDER_TYPE_SPHERE:
		//衝突判定
			//球と球
			//自身とターゲット
		hit = IsHitSphereToSphere(this, (SphereCollider*)target);
		//ターゲットは継承の親クラスのCollider型なので、
			//相手が球だとわかったら、
			//球型にキャストする
		break;

	//相手が「箱」ならば
	case COLLIDER_TYPE_BOX:
		//衝突判定
			//箱と球
			//ターゲットと箱
		hit = IsHitBoxToSphere((BoxCollider*)target, this);
		break;

	//その他
	default:
		//衝突しなかった
		hit = false;
		break;
	}

	//衝突判定結果を返す
	return hit;
}

//自身のサイズをセットする
void SphereCollider::SetWireFrameSize()
{
	//サイズからワイヤーフレームに使用するサイズを変更
		//親のColliderクラスにある関数から、
		//float型から、サイズを変更する関数を呼び出す
	Collider::SetWireFrameSize(radius_);
}
