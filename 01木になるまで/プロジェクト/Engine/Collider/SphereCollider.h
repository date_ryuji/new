#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "Collider.h"	//コライダー　抽象クラス



/*
	クラス詳細	：Colliderクラスを継承した、3D衝突判定用コライダー（球）
	クラスレベル：サブクラス
	クラス概要（詳しく）
				：球型のコライダー

	制限：球の半径は常に同様。楕円など場所により半径が変わることはない。
		：純粋な球
*/
class SphereCollider : public Collider
{
//private メンバ変数、ポイント、配列
private :
	
	//コライダーのサイズ（球の半径の長さ）
		//詳細：コライダーのローカルサイズ
	float radius_;

//public メソッド
public : 
	

	//コンストラクタ
		//引数：コライダーの離れ具合（ローカル座標）
		//引数：サイズ（半径）（ローカルサイズ）
		//引数：デバック用のワイヤーフレームを描画するか（DebugWireFrame）（指定なし：false）
		//戻値：なし
	SphereCollider(XMVECTOR position , float size , bool drawWireFrame = false);

	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~SphereCollider() override;

	//衝突判定実行
		//詳細：自身と引数コライダーとの当たり判定
		//　　：引数のコライダーのタイプを調べ、自身（球）と　相手（箱OR球）の当たり判定実行を呼び込む（Collider（親）クラスに計算式は存在するので、その関数を呼び込む）
		//レベル：オーバーライド
		//引数：相手のコライダー　ポインタ
		//戻値：衝突したか
	bool IsHit(Collider* pTarget) override;

	//コライダーのサイズ（半径）を返す
		//詳細：自身のローカルサイズ（メンバ変数）を渡す
		//引数：なし
		//戻値：ローカルサイズ（自身のコライダーの半径）
	float GetRadiusSize(){ return radius_;}


	//自身のサイズ（ワールドサイズ）を、ワイヤーフレームモデル（DebugWireFrame）へわたす
		//詳細：DebugWireFrameのサイズを指定するために（描画などのため）、コライダーのワールドサイズを渡す必要がある
		//　　：関数先にて、球（自身のコライダータイプ）のサイズを指定する関数呼び込み（Collider（親）メソッドより）
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void SetWireFrameSize() override;

};

