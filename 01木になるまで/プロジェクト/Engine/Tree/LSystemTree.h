#pragma once
//システム内　標準ヘッダ(usr/)
#include <string>	//文字列を使用するためにインクルード
#include <list>		//リストを使用するためにインクルード（途中で中身を消去する可変長配列）
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス


//クラスのプロトタイプ宣言
class Transform;
class BranchesMakeTree;
class Branch;
class CountTimer;
class FallenLeaves;

/*
	クラス詳細	：LSYSTEMのプロシージャル技術を使用して木の自動生成を行うクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	クラス概要（詳しく）
				：プロシージャル技術　LSYSTEMを使用して、
				　文字列を変更していって再帰的に文字列を結合する方式。
				 その文字列に応じて、モデルの生成、モデルの移動回転を担うことで、木のようなオブジェクトを作ることにつながる。（詳細は実行クラスにて）

				 生成ルール
					http://www.serenelight3d.com/blog55/?p=2126
					https://www.youtube.com/watch?v=E1B4UoSQMFw&feature=youtu.be

				文字列の結合でモデルを生成し、ソースとなる枝モデルを結合していって、一つの木を作る
				その際に、モデルの結合を行うため、
				ポリゴンの結合が行えるポリゴンクラス（BranchMakeTree）とソース（Branch）を使用する


	メリット　：枝モデル（低リソース）のモデルを結合することで木モデルを作ることができる
	　　　　　：複数パターンの木のモデルを作りたい場合、パターンとなる文字列のみを取得して起き、下記クラスへその文字列を渡すだけで、木モデルを作ることができる。
	　　　　　：木ごとにFBXのファイルなどを持たせずに済む。（リソースの削減に繋がる）
		 　　 ：ステージ内に出現させる木モデルに種類を持たせたい、だが、木モデルごとに高グラフィックのリソースを持たせたくない


	デメリット：木のモデルのパターンの文字列を作り出す（探し出す）ことが一苦労
	　　　　　：アルゴリズムに則り生成管理させるため、パターンによっては木にみえないようなものもある
		 　　 
	デメリットを解決するためにできること
			　：パターンを見つけることが一苦労であるため、簡単にパターンを見つけるツールのようなものがあれば、作業効率が上がると考える
			  ： →LSYSTEMのパターン文字列作成のためのツール　を作成
			　：ゲームの世界観、他モデルを木モデルに合わせる。（変な形の木が生えていてもおかしくない世界にする）


	制限：文字列の増加によって、５，６回目あたりから極端に重くなる。
	  　　char*であるため、長さは問題ないとは思うが、常に持っておくには問題がある。


*/
class LSystemTree : public GameObject
{
//private enum
private : 
	//LSYSTEMの変形用タイプ
		//詳細：文字列における文字　一文字を読み取り、その一文字ごとにソースとなる枝モデルに実行処理を行うことで、LSYSTEMという自動生成を実装
		//　　：上記のLSYSTEM実行処理のタイプ
	enum class TRANSFORM_TYPE
	{
		TRANSFORM_F = 0  ,			//'F'
		TRANSFORM_PLUS,				//'+'
		TRANSFORM_MINUS,			//'-'
		TRANSFORM_MULTIPLY ,		//'*'
		TRANSFORM_DIVISION ,		//'/'
		TRANSFORM_START_BRANCH,		//'['
		TRANSFORM_END_BRANCH,		//']'

		TRANSFORM_MAX,				//MAX

	};



//protected 定数
protected:

	/*LSYSTEM計算、実行用********************************************/
	//LSYSTEM実行回数上限
	static const unsigned int LSYSTEM_COUNT_ = 2;

	//サイズの減少値
	static constexpr XMVECTOR SUB_VEC = { 0.5f,0.5f, 0.5f, 0.0f };


//protected メンバ変数、ポインタ、配列
protected:
	//時間停止のフラグ
		//詳細：時間による行動、移動、攻撃、HP現象などなど様々な動的要因を停止する
	bool isStopTime_;
	//消去モーション時のシェーダー切替えフラグ
	bool isEraseMotion_;


	//木オブジェクト（BranchesMakeTree）のモデル番号
		//詳細：Model.cppに登録する際の、モデルへのハンドルを保存しておく
	int hBranchesMakeTree_;

	
	//木オブジェクトのポリゴンモデル(BranchMakeTree)
		//詳細：枝となる（ソース(Branch)）を(BranchMakeTree)に結合して作られる、木オブジェクト
		//　　：LSYSTEMにおける　(TRANSFORM_TYPE::TRANSFORM_F)が実行されるたびに、Branchが結合される
	BranchesMakeTree* pBranchesMakeTree_;
	
	//枝オブジェクト(Branch)
		//詳細：BranchesMakeTreeに結合するモデル
		//　　：（Branch）を結合することで、木モデルが実現する
	Branch* pBranchSource_;

	//時間計測を行うクラス
	CountTimer* pCountTimer_;

	//落ち葉クラス
		//詳細：一定間隔で葉っぱテクスチャを落とし続けるクラス
	FallenLeaves* pFallenLeaves_;


	/*LSYSTEM計算、実行用********************************************/
	//LSYSTEM実行回数
	unsigned int lsystemCounter_;

	//前回の階層の最終配置数
		//詳細：初期値はー１→０オリジンにするために、＋＋されたときにはじめて０にする形に
	int length;
	//次の階層の最終配置数
	int lastLength;

	//現在見ているsentenceの位置（要素位置）
	int pos;
		/*
			更新タイミング

			前回の最終配置数から、順番に置換をしていくが、(最終配置数まで回す）
			その時に、置換の時に回す値が、モデルに登録している番号
			すべての置換が終了したとき、
			次の階層の最終配置数を更新（今回登録した分を登録）

			配置数の更新を行うタイミングを間違わないように

		*/
	//モデル番号（共通）
	int hCommonModel_;
	//枝用モデル番号
	int hBranch;


	//各階層で足されるベクトル値
	XMVECTOR addVec_;
	//枝分かれ時の足されるベクトル値
	XMVECTOR addBranchVec_;

	//枝のソースとなるオブジェクトの拡大率
	XMVECTOR modelScale_;


	//LSYSTEM 保存用文字列
	std::string axiom;
	//置換対象の文字列
		//詳細：置換対象となっている下記文字と置換する場合、
		//　　：LSYSTEM実行時、置換対象文字を置換のルール文字列に切り替える（RULE_）
	std::string REPLACEMENT_TARGET_;
	//置換後に文字列が代入される文字列
	std::string sentence;

		/*
			LSYSTEMのルールについて

			ルール複数がある場合

			自身のルールが複数ある場合
			ルールの数（配列数）
			const int ruleCount;

		
			ルール数分のルールの確保
			文字列置換のルールの文字列
				エラー：静的でないメンバー参照は特定のオブジェクトを基準とする相対参照である必要があります
				メンバ内で作成した定数で、要素数を指定して作成することができない
				２次元配列のポインタとしたいので、ポインタのポインタで取得
			std::string** rules;
		
			//置換パターンのタイプ
			enum RULE_TYPE
			{
				RULE_FIXED = 0,
				RULE_B,
				RULE_C,
				RULE_D,
				RULE_E,


				RULE_END


			};


			//拡張のルール
			//LSystemTree::RULE_TYPE ruleType_;


			//文字列置換のルール
			//std::string rules[RULE_END];
		*/

	//文字列置換のルール
		//詳細：置換対象となる文字（REPLACEMENT_TARGET_）を置換するとき、置換の文字列
	std::string RULE_;




	//各モデル番号
		//消されない前提（なのでVector）
	std::vector<int> hModel_;


	//描画するモデル番号（Drawにて回す）が入る配列の要素（添え字）
		//毎フレーム描画する番号を登録させておく
	std::list<int> drawModel_;

	//各Transform
		//消されない前提
		//描画を行う、枝のTransform値
	std::vector<Transform> modelTransform_;

	//分岐していない枝の現段階の最終地点のTransform(LSYSTEMが回った時に、一番初めに成長させるときにどこから始めるのかしめすTrans)
		//詳細：前回のLSYSTEMによる成長にて、最後に分岐せずにまっすぐ伸びた枝のTransform
		//　　：★クラスはポインタではないので、すでに実態を持っている状態
	Transform lastTreeTrans;

//protected メソッド
protected : 

	//木オブジェクト（BranchesMakeTree）に新しい枝オブジェクト（Branch）の登録
		//詳細：Branchを任意Transformに結合して、BranchesMakeTreeにて木オブジェクトを作る
		//引数：Branchの結合時のTransform
		//戻値：なし
	void Join3DPolygon(Transform& trans);

	//消去モーションの実行を行う
		//詳細：消去フラグ（isEraseMotion_）を立てる
		//　　：消去モーション専用の専用シェーダーへの切り替えも行う
		//引数：なし
		//戻値：なし
	void StartEraseMotion();

	//消去モーションが終了したか
		//詳細：消去モーションが実行されて、消去モーションが終了した場合フラグをtrueで返す
		//引数：なし
		//戻値：消去モーション終了か(終了：true , 終了していない:false)
	bool EndEraseMotion();

	//時間が停止しているか
		//詳細：時間停止が宣言されて、時間停止フラグが立っているか
		//引数：なし
		//戻値：時間停止中（停止中：true , 計測中：false）
	bool IsStopTime();

	//BranchesMakeTreeのモデルハンドルを取得
		//引数：なし
		//戻値：モデルへのハンドル
	int GetBranchMakeATreeHandle();
	
	//樹木生成における文字列の置換�C
		//詳細：文字列実行方法�C　詳細は関数先にて
	void StringReplacement_4();
	//樹木生成�C
		//詳細：lastTransformを使用して親の枝の位置を判断（LSYSTEMの枝生成の下記の関数内でも、lastTransformは更新される）
	void CreateModelTransform4();


	//条件による変形
	//TRANSFORM_TYPE::TRANSFORM_F LSYSTEM実行時 'F'文字処理を受けての処理
		//詳細：ソースを結合
		//引数：セットTransform
		//引数：移動ベクトル
		//引数：ブランチ（分岐した際の）Transform
	void TransformF(Transform& setTrans , XMVECTOR moveVec , Transform& branchTrans);
	//TRANSFORM_TYPE::TRANSFORM_PLUS ,TRANSFORM_MINUS , TRANSFORM_MULTIPLY , TRANSFORM_DIVISION  LSYSTEM実行時を受けての処理
		//詳細：Transformの回転
		//引数：セットTransform
		//引数：回転値
	void TransformSource(Transform& setTrans , XMVECTOR addRotateValue);

	//引数文字から該当LSYSTEM変形用タイプを識別
		//引数：現在の文字
		//戻値：LSYSTEMの変形用タイプ
	TRANSFORM_TYPE ReplacementJudgment(char current);

	//枝の分岐（再帰）
		//詳細：枝生成先の位置変更　枝分岐
		//　　：次に枝が生成されるとき、枝はこの分岐位置から生成される
		//引数：分岐先のTransform
		//戻値：なし
	void BranchTree(Transform trans);

	//落ち葉クラスのロード
		//詳細：木オブジェクトに持たせる、落ち葉クラスの生成
		//引数：落ち葉のテクスチャ名
		//戻値：なし
	void LoadFallenLeaves(const std::string& TEXTURE_FILE_NAME);

//public ゲームループメソッド
public:
	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	LSystemTree(GameObject* parent);
	//コンストラクタ
		//コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前引数設定）
		//引数：親オブジェクト（GameObject）
		//引数：オブジェクト名
		//戻値：なし
	LSystemTree(GameObject* parent , const std::string& OBJ_NAME);

	//ルール（RULE＿）指定、テクスチャ指定(FallenLeavesの使用テクスチャ)の初期化
		//詳細：ルールを外部から指定し、初期化を行う
		//引数：ルール
		//引数：FallenLeavesに使用する　テクスチャ名
		//戻値：なし
	void Initialize(const std::string& RULE , const std::string& TEXTURE_FILE_NAME);

	//初期化
	//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
	//レベル：オーバーライド
	//引数：なし
	//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 
	//時間の停止（行動、移動、攻撃などの停止）
		//詳細：時間停止フラグを立てる
		//引数：なし
		//戻値：なし
	void StopTime();
	//時間の再開（行動、移動、攻撃などの再開）	
		//詳細：時間停止フラグを下す
		//引数：なし
		//戻値：なし
	void StartTime();



	//LSYSTEM実行のルール作成
		//詳細：ルールの初期化
	void CreateRules();


	//一番目の枝　木の幹となる枝の生成
		//詳細：木の幹から、枝が結合されて、木オブジェクトが生成される。完成する。
		//制限：一番最初の枝を生成しなければ、以降のLSYSTEMの実行が行えないので注意
		//引数：生成Transform
		//戻値：なし
	void CreateTheFirstOne(Transform& trans);
	//Transform指定されていない場合の枝生成
		//引数：なし
		//戻値：なし
	void CreateTheFirstOne();

	//BranchesMakeTree（Branchを結合して作成した木オブジェクト）の描画
		//詳細：LSYSTEMにおいて作成した木オブジェクトを描画したい場合は下記関数呼び込み
		//引数：描画ローカル位置
		//戻値：なし
	void DrawBranchesMakeTree(Transform& trans);
	//消去モーションシェーダーセット
		//引数：なし
		//戻値：なし
	void SetEraseMotionShader();
	//輪郭の描画
		//詳細：LSYSTEMの木オブジェクトの輪郭を描画する
		//　　：輪郭として、オブジェクトの輪郭部分に木オブジェクトの一回りの大きさのオブジェクトを描画して輪郭表現。一種の陰のように見える
		//引数：描画ローカル位置
		//戻値：なし
	void DrawOutLineForBranchesMakeTree(Transform& trans);

	////枝の描画実行
	//void DrawBranch();



	//LSYSTEMを実行可能かどうかを調べる
		//詳細：LSYSTEM実行可能回数上限にたっていないか
		//　　：LSYSTEM一回実行ごとにカウンターを1カウント
		//　　：（上限に達している　FALSE、上限に達していない　TRUE）
		//引数：なし
		//戻値：実行可能か（可能：true,不可能：false）
	bool ExecutableLSystem();

	//LSYSTEM実行回数を取得
		//詳細：現段階のLSYSTEM実行回数を取得
		//引数：なし
		//戻値：実行回数
	int GetLSystemCounter();

	//LSYSTEM実行
		//詳細：LSYSTEM実行可能である場合、LSYSTEM実行
		//引数：なし
		//戻値：なし
	void ExecutionLsystem();


};

