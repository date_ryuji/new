#include "LSystemTree.h"							//ヘッダ
#include "../Model/Model.h"							//モデル
#include "../GameObject/Transform.h"				//トランスフォーム
#include "../PolygonGroup/BranchesMakeTree.h"		//木オブジェクト
	/*		
		//自身のクラスにおいて
			//枝オブジェクトを任意の位置にセットし、
			//そのローカル座標を、木オブジェクトに登録することで、
			//３Dポリゴンを３Dポリゴンに結合する処理を請け負う

		//1本の木オブジェクトを管理するクラス
	*/

#include "../PolygonGroup/Branch.h"					//枝オブジェクト
#include "../../GameScene/Tree/FallenLeaves.h"		//落ち葉オブジェクト
#include "../../GameScene/CountTimer/CountTimer.h"	//カウントタイマー
#include "../../Shader/HeightMapEraseMotionShader.h"//消去モーション専用シェーダー
#include "../DirectX/Direct3D.h"					//シェーダーインスタンス所持、取得クラス



//コンストラクタ
LSystemTree::LSystemTree(GameObject * parent)
	: LSystemTree(parent , "LSystemTree")
{


}

//コンストラクタ
LSystemTree::LSystemTree(GameObject* parent, const std::string& OBJ_NAME)
	: GameObject(parent, OBJ_NAME),
	isStopTime_(false),


	pBranchesMakeTree_(nullptr),
	pBranchSource_(nullptr),
	hBranchesMakeTree_(-1),

	isEraseMotion_(false),
	pCountTimer_(nullptr),
	pFallenLeaves_(nullptr),

	lsystemCounter_(0),

	length(-1),
	lastLength(-1),

	hCommonModel_(-1),


	//addVec_(XMVectorSet(0.f, 3.0f,0,0)),
	addVec_(XMVectorSet(0.0f, 6.0f, 0.0f, 0.0f)),
	addBranchVec_(XMVectorSet(0.0f, 3.0f, 0.0f, 0.0f)),
	modelScale_(XMVectorSet(2.0f, 3.0f, 2.0f, 0.0f)),


	pos(0)

{
	//LSYSTEM, 樹木生成のルール作成
	CreateRules();
}

//LSYSTEM, 樹木生成のルール作成
void LSystemTree::CreateRules()
{


	//置換対象文字
	//F

	//ルールの定義
	//ルール:
	//F : 直線に延ばす
	//+ : X軸　45度回転
	//- : X軸　-45度回転
	//* : X軸　90度回転
	// /: X軸　-90度回転
	//[ : 枝分かれ開始
	//] : 枝分かれ終了
	

	//置換対象の文字列セット
	REPLACEMENT_TARGET_ = "F";


	//保存用文字列にルールの１番目の要素を代入
	axiom = REPLACEMENT_TARGET_;

	//置換後の文字列(初期に、ルールの１番目の要素を入れる)
		//上記の置換後の文字列から、次のLSYSTEM実行時、置換文字（REPLACEMENT_TARGET_）を探し、RULE文字列に変換、置換する
	sentence = axiom;

	//モデル番号配列（ベクター）のクリア
	hModel_.clear();

	//描画させるモデル番号リストのクリア
	drawModel_.clear();

	//トランスフォームの配列（ベクター）のクリア
	modelTransform_.clear();

}

//初期化
void LSystemTree::Initialize()
{
}

//初期化
	//LSYSTEMにおける置換RULEの作成と
	//LSYSTEMによって作成される枝のテクスチャの設定
void LSystemTree::Initialize(const std::string& RULE, const std::string& TEXTURE_FILE_NAME)
{
	//文字列置換のルール作成
		//sentence（置換後文字列）における置換対象文字（REPLACEMENT_TARGET_）を見つけて、
		//その置換対象文字を下記RULEという文字列で置き換える
		//その文字列置換のルールを登録
	RULE_ = "";

	//ルールとして登録する前に
		//文字列終わりの\0を抜きにして考える必要がある
		//￥0がついていると、文字列の置換の際に、￥0を省く処理を行っていないため、想定通りに動かなくなる。
		//文字列の置換後にRULE_に\0がついていると、その置換後の文字列に\0が入ってしまう。stringは\0で文字の終了をしめすため、勝手に文字列が終了を示してしまう。上記を避けるために
		//\0を抜く処理
	//スコープ：�@
	//\0前まで、文字列をコピー
	//条件：RULEの文字数分
	for (int i = 0; i < RULE.size(); i++)
	{
		//一文字ずつ取得
		char c = RULE[i];

		//\0でないなら
		if (c != '\0')
		{
			//結合
			RULE_ += c;
		}
		//スコープ：�A
		//文字列なら
		else
		{
			//終了
				//スコープ：�@抜ける
			break;
		}
	}


	//ソース（枝）となるFbxファイルのファイルパス
	static const std::string fbxFileName = "Assets/Scene/Model/GameScene/TreeSource/TreeSourceAndLeaves2.fbx";

	//Treeオブジェクト（木全体のポリゴンオブジェクト）の生成
		//pBranchesMakeTree_->Load("" , SHADER_HEIGHT_MAP);	//ファイルが存在しないものを指すので、　Loadは使えない
	pBranchesMakeTree_ = new BranchesMakeTree;
		
	//引数：テクスチャのパス
	//引数：シェーダータイプ
	pBranchesMakeTree_->Initialize(TEXTURE_FILE_NAME, SHADER_TYPE::SHADER_HEIGHT_MAP);

	//上記Treeオブジェクトを
		//Model.cppにおける　データベースに追加する形に変形
	Model::ModelData modelData;
	modelData.fileName = "BranchesMakeTreeForLSystemTree.class";//ファイル名を他のBranchesMakeTreeと区別するために、BranchesMakeTreeとForだれのための、.class（クラスですよ）（ファイル名は、解放の時に、同様の名前が存在するかで、解放するかを判断する。なので、他とは被らないファイル名をセットする）
	modelData.pPolygonGroup = pBranchesMakeTree_;
	modelData.thisPolygonGroup = POLYGON_GROUP_TYPE::BRANCHES_MAKE_TREE_POLYGON_GROUP;
	modelData.thisShader = SHADER_TYPE::SHADER_HEIGHT_MAP;
	modelData.transform = transform_;

	//Model.cppのデータベース軍に追加
	hBranchesMakeTree_ = Model::AddModelData(modelData);
	assert(hBranchesMakeTree_ != -1);
	Model::SetTransform(hBranchesMakeTree_, transform_);


	//Branch
	//木を作る枝モデルのロード
	hBranch = Model::Load(fbxFileName, POLYGON_GROUP_TYPE::BRANCH_POLYGON_GROUP, SHADER_TYPE::SHADER_HEIGHT_MAP);
	//警告
	assert(hBranch != -1);
	Model::SetTransform(hBranch, transform_);
	
	//3Dポリゴンのモデルポインターを取得
		//モデルをロードして、
		//後にポインタをもらう方式にする
	pBranchSource_ = (Branch*)Model::GetPolygonGroupPointer(hBranch);

}

//一番目の枝　木の幹となる枝の生成
void LSystemTree::CreateTheFirstOne(Transform& trans)
{
	//トランスフォームを計算用変数へコピー
	Transform tranform = trans;
	//前回、直線にのばされた枝のTransformを保存（次回のLSYSTEMにて、枝を生成する位置の参考に使われる。）
	lastTreeTrans = trans;


	//木を一本表示させる
		//★モデル番号をModelcppにて複数個確保管理する
		//CreateTreeSource(trans);
	//★一つのモデルの頂点数を動的に増やし、	
		//モデルの３Dポリゴンを変化させていく
	Join3DPolygon(tranform);


	//生成個数のカウント
		//前回の最終配置数をカウント
	length++;


	//モデルのサイズを縮小
		//生成されるたびにスケール値を小さくしていく
	modelScale_ -= SUB_VEC;

}

//Transform指定されていない場合の枝生成
void LSystemTree::CreateTheFirstOne()
{
	//Transform
		//移動値0のTransfomrを用意し、初期値に枝生成
	Transform trans;
	trans.position_ = XMVectorSet(0.f, 0.f, 0.f, 0.f);

	//枝の生成
	CreateTheFirstOne(trans);

}


//LSYSTEM実行
void LSystemTree::ExecutionLsystem()
{
	//LSYSTEM実行回数カウント
	lsystemCounter_++;

	//LSYSTEM試作段階�C
	{

		StringReplacement_4();

		CreateModelTransform4();
	}

	//モデルのサイズを縮小
		//実行タイミングで
		//枝のモデルサイズを縮小させる
		//LSYSTEMとして枝が生成されるたびに縮小していくことで、枝が分岐、枝が分かれるほど枝が小さく→リアルな木の成長を表現
	modelScale_ -= SUB_VEC;

}

//時間が停止しているか
bool LSystemTree::IsStopTime()
{
	return isStopTime_;
}
//Treeオブジェクトのハンドル取得
int LSystemTree::GetBranchMakeATreeHandle()
{
	return hBranchesMakeTree_;
}

//時間の停止
void LSystemTree::StopTime()
{
	//停止フラグを立てる
	isStopTime_ = true;

	//条件：カウントタイマーが存在するなら
	if (pCountTimer_ != nullptr)
	{
		//タイマーの経過時間の計測を停止する
		pCountTimer_->NotPermittedTimer(0);
	}
	//条件：落ち葉クラスが存在するなら
	if (pFallenLeaves_ != nullptr)
	{
		//落ち葉クラスへ伝える
			//時間停止を伝える
		pFallenLeaves_->StopTimer();
	}
}
//時間計測再開
void LSystemTree::StartTime()
{
	//停止フラグを下ろす
	isStopTime_ = false;

	//条件：カウントタイマーが存在するなら
	if (pCountTimer_ != nullptr)
	{
		//時間計測再開を伝える
		pCountTimer_->PermitTimer(0);
	}
	if (pFallenLeaves_ != nullptr)
	{
		//落ち葉クラスへ伝える
			//時間計測再開を伝える
		pFallenLeaves_->StartTimer();
	}
}

//木オブジェクト（BranchesMakeTree）に新しい枝オブジェクト（Branch）の登録
void LSystemTree::Join3DPolygon(Transform & trans)
{
	//引数Transformの
		//親Transformを、オブジェクトクラスの持っているTransformにする
		//trans.pParent_ = &transform_;
		//→引数のTransformは
		//→pBranchSourceの　”モデルの”原点からのローカル座標における行列を取得して、それらから、モデルの頂点を追加する。
			//→そのため、LSYSTEMのモデルのTransoformを親にすると、ワールドにおける座標の変化ができてしまうので、余計な計算をさせないようにする必要がある。

	//上記の処理によって、
		//オブジェクトクラスが可変したら、木オブジェクトも可変するようにする。
	trans.scale_ = modelScale_;

	//Treeオブジェクトへ
		//枝オブジェクトの結合を行うクラスの呼び込み
		//指定Transformへ枝オブジェクトを結合させる
	pBranchesMakeTree_->Join3DPolygon(pBranchSource_, trans);


	//次回の最終配置数をカウント
	lastLength++;
}

//更新
void LSystemTree::Update()
{
}

/*
	成長

//毎フレーム特定値成長する（一定量ずつTransformのScale値をアップする）
//枝を生成したときに、初期スケール値は幼木として短い長さになる。→そこから成長する
	//★特定キーが押されたときに、LSYSTEMで枝の生成（初期スケール値にて）
	//★成長を行うためには、前回の枝のTransform値を持っておかないといけない。（または、アクセスできるようにしておかないといけない。）
	//★初期のスケール値を１．０として持っておき、毎フレーム一定量スケール値＋して、それぞれ前回のTransformのScaleにセット
	//★→スケール値が一定量足して、最大のスケール値に拡大が終了したときに、次のLSYSTEMの実行が可能になる。

*/


//描画
void LSystemTree::Draw()
{
}

//Treeオブジェクトの描画
void LSystemTree::DrawBranchesMakeTree(Transform& trans)
{
	//消去モーション実行時の場合
		//消去モーションに必要なシェーダー情報の受け渡しを行う
	SetEraseMotionShader();

	//描画
	Model::SetTransform(hBranchesMakeTree_, trans);
	Model::Draw(hBranchesMakeTree_);
}

//消去モーションシェーダーセット
void LSystemTree::SetEraseMotionShader()
{
	//条件：消去モーションのフラグが立っているとき
		//シェーダーを切り替える
	if (isEraseMotion_)
	{
		//シェーダーの切替え
			//消去モーションを必要とするシェーダーに変換
			//輪郭描画のタイミングにて、シェーダーを一回切り替えているため、
			//消去モーション中は毎フレームシェーダーを切り替える処理を呼び込む
		Model::ChangeShader(hBranchesMakeTree_, SHADER_TYPE::SHADER_HEIGHT_MAP_ERASE_MOTION);


		//シェーダーへの追加情報を与える
		//経過時間の取得
		float elpasedTime = pCountTimer_->GetElpasedTime();

		//経過時間をシェーダーに渡す

		//コンスタントバッファ2つ目に代入
		HeightMapEraseMotionShader* pHMEraseMotion = (HeightMapEraseMotionShader*)Direct3D::GetShaderClass(SHADER_TYPE::SHADER_HEIGHT_MAP_ERASE_MOTION);

		//コンスタントバッファにセット
		HeightMapEraseMotionShader::CONSTANT_BUFFER_1 cb1;
		cb1.elpasedTime = elpasedTime;

		//コンスタントバッファ１へ情報をセット
			//描画のタイミングにて
			//セットした情報はシェーダーへ橋渡しされる
		pHMEraseMotion->SetConstantBuffer1Data(&cb1);
	}
}

//Treeオブジェクトの輪郭の描画
void LSystemTree::DrawOutLineForBranchesMakeTree(Transform& trans)
{
	//シェーダーを輪郭に変更
	Model::ChangeShader(hBranchesMakeTree_, SHADER_TYPE::SHADER_HEIGHT_MAP_OUTLINE);

	//描画
		//この際に描画Transformは引数のTransformを使用する
		//引数のTransformにて、描画サイズを指定されているため、関数内にて計算を必要としない
	Model::SetTransform(hBranchesMakeTree_, trans);
	Model::Draw(hBranchesMakeTree_);

	//シェーダーを元に戻す
	Model::ChangeShader(hBranchesMakeTree_, SHADER_TYPE::SHADER_HEIGHT_MAP);
}

//消去モーションの実行を行う
void LSystemTree::StartEraseMotion()
{
	//消去モーション開始のフラグを立てる
	isEraseMotion_ = true;


	//シェーダーの切替え
		//消去モーションを必要とするシェーダーに変換
		//シェーダーの切り替えを実行するが、
		//Treeオブジェクト（hBranchesMakeTree_にてアクセスするモデル）は毎フレーム輪郭のためにシェーダーを変更する。
		//そのため、消去モーションのシェーダーに関しても、毎フレーム設定する必要がある
	Model::ChangeShader(hBranchesMakeTree_ , SHADER_TYPE::SHADER_HEIGHT_MAP_ERASE_MOTION);


	//消去モーション計測用のカウントタイマー計測開始
		//シェーダー切替えによって、
		//シェーダーに渡す情報として、
		//経過時間を必要とする
		//その経過時間の開始を宣言する
	pCountTimer_ = (CountTimer*)Instantiate<CountTimer>(this);
	
	//カウントアップ
	//開始：０．ｆ
	//終了：５．ｆ
	pCountTimer_->StartTimerForCountUp(0.f, 5.f);

}

//消去モーションが終了したか
bool LSystemTree::EndEraseMotion()
{
	//条件：タイマーが存在しており
	if (pCountTimer_ != nullptr)
	{
		//条件：現在の経過時間が、終了時間以上である
		if (pCountTimer_->EndOfTimerForCountUp())
		{
			//終了
			return true;
		}
	}
	//継続中
	return false;
}


//解放
void LSystemTree::Release()
{
	//Treeオブジェクトのモデル、ポリゴンなどは
		//Model.cppというモデル全般を管理するクラスへ持たせており、解放も一任しているため、LSystemTreeにて行うことはない
}

//LSYSTEM実行可能であるか
bool LSystemTree::ExecutableLSystem()
{
	//条件：現在のLSYSTEM実行回数　が　LSYSTEM可能上限回数と同じでない
	return lsystemCounter_ != LSYSTEM_COUNT_;
}

//LSYSTEM実行回数取得
int LSystemTree::GetLSystemCounter()
{
	return lsystemCounter_;
}

//★下記の文章の実装を実現するために、文字列の置換のみ行う
//樹木生成�C
/*
//★枝を伸ばしたら、次に伸ばす枝の開始位置は前回伸ばされた枝の終点から伸びる
	//★枝を伸ばしたら、伸ばした先のTransformが親の枝のTransformとなる。（サイズもこの先使用するかもしれないのでTransformで）
	//枝を発生させる位置（描画の際の枝のTransformは、）は、前回の枝の位置だけ覚えておけば十分だが、
		//描画の際には、枝のTransformは無いといけないので、保存しておく
	//[]で、枝を分岐させるものに関しては、
		//分岐中は、分岐開始の位置を保存しておき、それとは別に、分岐された枝先にて枝の成長を行っていく。
		//]で分岐が終了したら、→親のTransformの位置を保存しておいたTransformで上書きして、成長続ける
//★（前提）一度枝が分岐し、成長し、成長を終えた[]の枠を超えたとき
	//→それ以降で、その枝についかして枝が生えることは絶対にない
*/
//樹木生成における文字列の置換�C
	//あくまでも文字列の置換だけを行う
void LSystemTree::StringReplacement_4()
{
	//計算用文字列
	std::string nextSentence = "";

	
	
	//sentence（現在の文字列（前回までの置換後文字列））の文字１文字１文字
	//をルールにのっとって、置き換える
	//ルール："F"　→　"FF+[+F-F-F]-[-F+F+F]"などに置換
	//ルール：REPLACEMENT_TARGET_ →　RULE_に置換
		/*
			例

			前提
			・sentence = "F"
			・REPLACEMENT_TARGET_ = 'F'
			・RULE_ = "F+F"

			�@sentence = "F+F"
			�A上記の�@文字列を1文字ずつ参照
			�B_1
			  �@を1文字ずつ参照していき、REPLACEMENT_TARGET_と同様の文字を見つけた場合、
			　その�@の文字を　RULE＿の文字列へ置換する
			  置換後の文字はnextSentenceに結合

			  
			�B_2
			  �@を1文字ずつ参照して、置換できない文字であった場合
			  その文字はnextSentenceに結合

			�C�Bをsentenceの文字全てを参照し繰り返す
			�D置換後の文字　nextSentenceを sentenceにコピー
			※sentenceを1回　置換した後の結果
			　nextSentence = "F+F"

			 　説明：sentenceの全文　"F"　を1文字ずつ参照して、
			  　　 ：REPLACEMENT_TARGET_と同様の文字があった場合、それをRULE_にて変換するので、
				 　：変換後の結果は　”F+F”となった
		
		*/
	//�@
	//条件：現在の文字列（前回までの置換後文字列）サイズ
	for (int i = 0; i < sentence.size(); i++)
	{
		//�A
		//文字列の頭(１文字目)を持って来る
			//文字列の1要素（char型の配列でstringは取得されている）を取得
		auto current = sentence[i];


		//置換対象の文字列
			//上記の文字列のchar要素位置(何番目のchar値をとるか)の0番目を取得
			//置換を行う文字であった場合、置換のルールにのっとって、置換後の文字列を結合
		if (current == REPLACEMENT_TARGET_[0])
		{

			//�B_1
			//文字列なのでそのまま代入
			nextSentence += RULE_;

		}
		//変換されていないなら、
		//そのままの値を足す（rules１番目の要素以外の文字だった）
				//現段階参照しているsentence全てを見終えるまで終了しない
		else
		{
			//�B_2
			nextSentence += current;
		}

		//�C
			//繰り返し
			
	}

	//�D
	//置換が完了した文字列を
	//置換完了後の文字列保存変数に代入
	sentence = nextSentence;



}


//�C
//memo 
//[]　枝分岐を使用すれば、
//枝が伸びたときに、その伸びた先の枝の先端が次の枝発生位置となる
	//→　＝　つまり、枝の発生、はやす位置は動的に常に変化し続けているということ。？？
//★枝を伸ばしたなら、次に延びる枝は、左記にて伸ばした枝の続きに書かなくてはならない。
	//→[]があるならば、→その中は、分岐された枝をもとに延ばす。分岐された枝の中で完結させる。


//★枝を上部を盛り上げるためには、
	//→枝を伸ばした枝から生えさせて、行かなければならない。（その時その時で、はやした枝の位置を覚えておく必要がない）

//考える必要がある。

/*
//★枝を伸ばしたら、次に伸ばす枝の開始位置は前回伸ばされた枝の終点から伸びる
	//★枝を伸ばしたら、伸ばした先のTransformが親の枝のTransformとなる。（サイズもこの先使用するかもしれないのでTransformで）
	//枝を発生させる位置（描画の際の枝のTransformは、）は、前回の枝の位置だけ覚えておけば十分だが、
		//描画の際には、枝のTransformは無いといけないので、保存しておく
	//[]で、枝を分岐させるものに関しては、
		//分岐中は、分岐開始の位置を保存しておき、それとは別に、分岐された枝先にて枝の成長を行っていく。
		//]で分岐が終了したら、→親のTransformの位置を保存しておいたTransformで上書きして、成長続ける
//★（前提）一度枝が分岐し、成長し、成長を終えた[]の枠を超えたとき
	//→それ以降で、その枝についかして枝が生えることは絶対にない
*/


//樹木生成�C
	//文字列置換終了後の文字列を使用して
	//枝生成、枝分かれなど樹木生成を行う
void LSystemTree::CreateModelTransform4()
{
	/*
		LSYSTEMを使用した　樹木生成について

		StringReplacement_4()にて、
		文字列の置換を行った。

		しかし、StringReplacement_4においてはあくまでも、
		文字列のみを扱ているだけで、その文字列が直接枝になるわけでもない。

		文字列の法則性を
		3Dモデルにうまく落とし込む必要がある

		StringReplacement_4()にて、
		F(REPLACEMENT_TARGET_)　という文字を　特定の文字列（RULE_）にて置換したことで、

		'F'　や　'+'　や '-' など特定の文字が法則性に則り並んでいる。

		★であれば、Fという文字を枝に見立て、
		　F以外の+ , -　を枝の動的移動　の単位にすると規則性のある樹木生成に応用できるのでは？と考える

		★sentence（前回までの置換後文字列）を1文字ずつ参照し、
		　'F'という文字が来た時には、枝のモデルを幹に生やす。
		  '+'という文字が来た時には、枝を生やすのではなく、次に生やす枝、その生成するときの角度を+45度回転させる。
		  '-'という文字が来た時には、枝を生やすのではなく、次に生やす枝、その生成するときの角度を-45度回転させる。

		  など。

		上記のように
		文字列の置換、パターンに則り、３Dや２Dのリソースをパターンの規則性に合わせて、生成させることをLSYSTEMという。


	*/

	/*
		今回における　LSYSTEMの文字列に使用する文字

		F : 枝生成（現在のTransform位置へ）
		+ : 次に生成する枝の回転角度（Transform.rotate_）を+45度回転させる
		- : 次に生成する枝の回転角度（Transform.rotate_）を-45度回転させる
		* : 次に生成する枝の回転角度（Transform.rotate_）を+90度回転させる
		/ : 次に生成する枝の回転角度（Transform.rotate_）を-90度回転させる
	
		[ : 枝分かれ開始（「現在の枝生成位置」を保存しておき、他の枝とは分離した枝の生成が始まる）
		] : 枝分かれ終了（枝分かれ開始時の　「現在の枝生成位置」まで枝生成位置を戻す。次に生成される枝は先の枝生成位置から生成。）

		※枝分かれとは、[　のタイミングから、　枝生成位置から新しい木を生やすイメージ
		　]　が来るまで、孤立した枝を生やし続ける。
		 [のタイミングの枝が枝が生える幹のような存在になる
		 （上記を担うために、実現するために、関数の再帰を行う）
	*/
	//★★★★★★★★★★
/*
	!!
	再帰

	[が始まったら、まず�@分岐位置を持っておく

	その分岐位置を関数へ送って、その分岐位置をもとに枝を発生、
	再び[が来たら、再び現在の分岐位置を関数へ送って、その分岐位置をもとに枝を発生

	その関数の段階で、]が来たら、終了。関数を返す。（再帰）
	戻った先で、文字を進めて、]が来たら、終了。関数を返す。

	これを再帰的に続ける。。


	（注意）
	まっすぐに進むので、
	関数内の次に発生させる枝の位置、前回の枝の位置は別に持っておかないといけない。
	関数の引数の値は、変更せずに。



	（問題）
	共通の文字列である、sentenceを見るので、
	関数にて飛んだ先においても、sentenceを見ることになる。
	→これを、関数先、関数外でも参照するには、メンバ変数で現在見ている位置を持っておかないと（ただのint型で、LSYSTEM終了したら、０で初期化すればいい。）

*/



	//分岐を起こした場合に分岐前の枝のTransformを保存する変数(分岐が終わった後に分岐した地点から枝を生やせるように)
	//分岐開始位置
	Transform branchTrans = lastTreeTrans;


	//sentence位置を示す要素位置（添え字）を初期化
	pos = 0;


	//セット用のTransform
	Transform setTrans = lastTreeTrans;

	//移動量ベクトル
	XMVECTOR moveVec = addVec_;



	//描画するライン（素材）の回転値（Transform）	
		//初期値は上に伸びてほしいので、＋Y方向に１

	//置換する文字は、決められているので、
	//置換のルールにて決められているものを使用する
	//条件：置換した後の文字列数分
	for (pos = 0; pos < sentence.size(); pos++)
	{
		//1バイト分、char1文字分取得
		auto current = sentence[pos];

		//文字により行動変化
		switch (ReplacementJudgment(current))
		{
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_F :
			TransformF(setTrans, moveVec, branchTrans); break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_PLUS:
			TransformSource(setTrans, XMVectorSet(0, 0, 45 * (-1), 0)); break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_MINUS:
			TransformSource(setTrans, XMVectorSet(0, 0, -45 * (-1), 0)); break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_MULTIPLY:
			TransformSource(setTrans, XMVectorSet(45 * (-1), 0, 0, 0)); break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_DIVISION:
			TransformSource(setTrans, XMVectorSet(-45 * (-1), 0, 0, 0)); break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_START_BRANCH:
			//関数へ飛んで、]が見つかるまで関数先にて、枝生成を行う
				//枝の生成を行う分岐位置を送ってもらう

				//sentenceを見る値を一つ進める（関数先で[の次から文字を見るように）
			pos++;
			BranchTree(branchTrans);
			break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_END_BRANCH:
			//分岐終了
			//関数呼び出しもと（[を受けて、関数を呼び出したものへ）
			return;

		default : 
			return;

		}


	}

	//LSYSTEMの枝生成が完了したので、
	//最後にまっすぐ伸ばした枝の位置を登録
	//LSYSTEmを開始した関数の、最後の分岐位置を登録させる。→つぎのLSYSTEMは、下記にて登録した位置から枝を生成させる
	lastTreeTrans = branchTrans;


}

//条件による変形
	//F:枝の生成
void LSystemTree::TransformF(Transform& setTrans, XMVECTOR moveVec, Transform& branchTrans)
{
	//回転度合いによる移動値を求める
		//回転度合いを行列にし、移動値に変換
	XMMATRIX matXZ;
	//回転させた軸の回転具合を取得（ラジアンに直して、行列にする）
	matXZ = XMMatrixRotationZ(XMConvertToRadians(setTrans.rotate_.vecZ));
	matXZ *= XMMatrixRotationX(XMConvertToRadians(setTrans.rotate_.vecX));
	
	//回転行列と移動ベクトルを掛ける			
	setTrans.position_ += XMVector3TransformCoord(moveVec, matXZ);
	//setTrans.position_ += moveVec;
	setTrans.position_.vecW = 0.f;

	//現在のTransformで描画
		//木を一本表示させる
	//★一つのモデルの頂点数を動的に増やし、	
		//モデルの３Dポリゴンを変化させていく
	Join3DPolygon(setTrans);

	//枝分かれ時のTransformをセット
	branchTrans = setTrans;
	
}

//引数文字から該当LSYSTEM変形用タイプを識別
LSystemTree::TRANSFORM_TYPE LSystemTree::ReplacementJudgment(char current)
{

	//文字から
		//enumTypeのタイプへ変換し　返す
	switch (current)
	{
	case 'F' : 
		return LSystemTree::TRANSFORM_TYPE::TRANSFORM_F;
	case '+':
		return LSystemTree::TRANSFORM_TYPE::TRANSFORM_PLUS;
	case '-':
		return LSystemTree::TRANSFORM_TYPE::TRANSFORM_MINUS;
	case '*':
		return LSystemTree::TRANSFORM_TYPE::TRANSFORM_MULTIPLY;
	case '/':
		return LSystemTree::TRANSFORM_TYPE::TRANSFORM_DIVISION;
	case '[':
		return LSystemTree::TRANSFORM_TYPE::TRANSFORM_START_BRANCH;
	case ']':
		return LSystemTree::TRANSFORM_TYPE::TRANSFORM_END_BRANCH;

	default : 
		return LSystemTree::TRANSFORM_TYPE::TRANSFORM_MAX;

	}

}

//引数ベクトル値分回転させる
void LSystemTree::TransformSource(Transform& setTrans, XMVECTOR addRotateValue)
{
	setTrans.rotate_ += addRotateValue;
}

//[]による枝分かれ後の　処理（[開始時の枝分かれが起き、　]の枝分かれが終了するまでの処理）
	//LSYSTEMの枝生成と（CreateModelTransform4（））同じことをする
	//[の文字を受けて、枝が分岐された後の枝生成
		//引数にてもらったTransform値から枝を生成していく（引数値を開始位置としてLSYSTEMを行う）
		//']'が見つかるまで、見つかったら帰る。
		//途中で[を見つけたら、さらに現在の分岐位置を送って再帰的に行う
//戻値は、なし＝分岐後は、その分岐先にて枝の生成を完結させたいので、その分岐先で最終的な分岐位置も必要なし（そこからさらに枝を生成することもないので。）
void LSystemTree::BranchTree(Transform trans)
{
	//分岐を起こした場合に分岐前の枝のTransformを保存する変数(分岐が終わった後に分岐した地点から枝を生やせるように)
	//分岐開始位置
	Transform branchTrans = trans;

	//セット用のTransform
	Transform setTrans = trans;
	//移動量ベクトル
	XMVECTOR moveVec = addBranchVec_;



	//描画するライン（素材）の回転値（Transform）	
		//初期値は上に伸びてほしいので、＋Y方向に１

	//置換する文字は、決められているので、
	//置換のルールにて決められているものを使用する
	//置換した後の文字列数分
	for (pos; pos < sentence.size(); pos++)
	{
		//1バイト分、char1文字分取得
		auto current = sentence[pos];


		//文字により行動変化
		switch (ReplacementJudgment(current))
		{
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_F:
			TransformF(setTrans, moveVec, branchTrans); break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_PLUS:
			TransformSource(setTrans, XMVectorSet(0, 0, 45 * (-1), 0)); break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_MINUS:
			TransformSource(setTrans, XMVectorSet(0, 0, -45 * (-1), 0)); break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_MULTIPLY:
			TransformSource(setTrans, XMVectorSet(45 * (-1), 0, 0, 0)); break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_DIVISION:
			TransformSource(setTrans, XMVectorSet(-45 * (-1), 0, 0, 0)); break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_START_BRANCH:
			//関数へ飛んで、]が見つかるまで関数先にて、枝生成を行う
				//枝の生成を行う分岐位置を送ってもらう

				//sentenceを見る値を一つ進める（関数先で[の次から文字を見るように）
			pos++;
			BranchTree(branchTrans);
			break;
		case LSystemTree::TRANSFORM_TYPE::TRANSFORM_END_BRANCH:
			//分岐終了
			//関数呼び出しもと（[を受けて、関数を呼び出したものへ戻る）
			return;

		default:
			return;

		}

	}
}

//落ち葉クラスのロード
void LSystemTree::LoadFallenLeaves(const std::string& TEXTURE_FILE_NAME)
{
	//落ち葉クラスへ
	//テクスチャを指定して渡す
	pFallenLeaves_ = (FallenLeaves*)Instantiate<FallenLeaves>(this);
	//テクスチャロード
	pFallenLeaves_->Load(TEXTURE_FILE_NAME);

}




