#pragma once
#include "../DirectX/Direct3D.h"		//DirectX　３D表現　ID３D型のクラスを使用するため、シェーダーの頂点シェーダーの型を取得するため
#include "../DirectX/Texture.h"			//テクスチャバッファを扱うクラス
#include "../GameObject/Transform.h"	//オブジェクトの座標系を持つクラスヘッダ
//スコープの省略
using namespace DirectX;

/*
	構造体詳細	：２D画像衝突判定用　情報格納構造体
	構造体概要（詳しく）
				：マウスのスクリーン座標と画像との衝突判定を行う際の情報構造体
*/
struct ImageCollision
{
	//画像と衝突判定を行う際の衝突判定位置
		//詳細：計算用
		//　　：マウス位置
	XMVECTOR scrPointPos;

	//衝突したかの判定
		//詳細：格納用
	bool hit;

	//ワールドのTransformを取得
		//詳細：計算用
		//　　：2D画像のワールド座標
	Transform* pTrans;

	//コンストラクタ
	ImageCollision() :
		scrPointPos(XMVectorSet(0.f , 0.f, 0.f, 0.f )) , 
		hit(false) , 
		pTrans(nullptr)
	{};


};

/*
	クラス詳細	：ポリゴンクラス　2D画像（Sprite）専用
	クラス概要（詳しく）
				：画像データを読み込み（Imageより）
				　画像データを2Dデータとして表示する。
				：３D空間上に表示するのではなく、２D上に近い座標で表示位置を管理する（プロジェクション座標の座標扱いに近い）
				：２Dのポリゴン（PolygonGroupにおけるPlaneの取り方と一緒（三角形ポリゴンを2つで四角形をつくる　ただし、表示の仕方は、２D上のプロジェクションの座標でスクリーンへ表示する。））
*/
class Sprite
{
//priavte メンバ変数、ポインタ、配列
private : 
	//自身のシェーダー
		//制限：２D専用のシェーダー限定
		//　　：2D専用シェーダーを調べる方法は、CommonData/ShaderType.hにて、namespace::ShaderType メソッドにて確認できる
	SHADER_TYPE thisShader_;
	
	//自身のα値
		// 詳細：（自身のα値を保存しておく（Spriteクラスごと（シェーダーのDraw前にセットしてもらい、画像ハンドルごとのα値wをセット）））
		//　　：シェーダーへのコンスタントバッファ１つ目（Sprite::CONSTANT_BUFFER）のα値　に格納し、渡す
	float alpha_;

//protected 構造体
protected:

	/*
	構造体詳細	：コンスタントバッファ構造体
	構造体概要（詳しく）
			：シェーダーに渡すシェーダー内グローバル情報
			：シェーダーのコンスタントバッファと同じサイズ、同じ順番の定義を行う

	制限：コンスタントバッファ16バイト未満の場合、コンスタントバッファのポインタ作成の際に、サイズを最低16バイトで初期化をする必要がある
		：16の倍数でサイズが取られている必要がある
		：構造体は、構造体内の変数内にて、一番型サイズの大きいもののサイズの倍数でサイズが取られるようになる。
		：例：構造体のサイズ　31バイト、構造体内最大の型サイズ　4バイト　＝　構造体のサイズは4の倍数に拡張される。余分なサイズが取られる。
		：　：最終的に、構造体のサイズは一番近い4の倍数になるので、32バイトとなる。
		：以上のことから、コンスタントバッファ構造体のサイズを考える際に、構造体ないに、行列（32バイト）が存在していれば、自動的にサイズが32の倍数になる。＝16の倍数
*/
	struct CONSTANT_BUFFER
	{
		
		//ワールド行列
		XMMATRIX	matW;	

		//α値
		float alpha;		
	};
	/*
		Spriteにおける　合成行列の必要性

		//合成行列（ワールド＊ビュー＊プロジェクション行列）いらず	（２D描画のため、3D描画のためのカメラのどの位置など必要ないないので）
	
	*/
	

	/*
	構造体詳細：頂点情報
	構造体詳細（詳しく）
			　：シェーダーファイルの頂点シェーダー、引数に渡す情報
	　　　　　：頂点ごとの情報、位置、UV　を取得
			　：2Dであるため、法線や接線は必要ない。２D画像に光表現を付ける必要性はない
	*/
	struct VERTEX
	{
		//位置情報（頂点の位置）
		XMVECTOR position;
		//UV情報（テクスチャの座標）
		XMVECTOR uv;
	};


//protected メンバ変数、ポインタ、配列
protected : 
	//頂点数（FBXファイルより取得）
	int vertex_;
	//インデックス情報
		//詳細：1次元配列（三角ポリゴンを作る頂点の頂点番号）
	int* index;


	//頂点情報を登録したバッファー
	ID3D11Buffer* pVertexBuffer_;
	//頂点の順番、インデックス情報を持っておくバッファー
			//詳細：マテリアルごとのインデックスバッファー、ポインタのポインタ
	ID3D11Buffer* pIndexBuffer_;
	//コンスタントバッファのバッファーポインタ
		//詳細：構造体で取得しているCONSTANT_BUFFERをシェーダーに渡す際のバッファー
		//　　：バッファーへ各構造体の情報を格納して、シェーダーへわたす
	ID3D11Buffer* pConstantBuffer_;

	//テクスチャ
		//詳細：Spriteにて管理する画像のテクスチャクラス
		//　　：ロードした画像のテクスチャをSpriteのポリゴン部分に張り付けることで、画像を２Dとして描画可能となる
	Texture* pTexture_;


	//描画を優先するテクスチャ
		//詳細：外部から、描画をするテクスチャを指定されている場合、
		//　　：自身が持っているテクスチャではなく、外部からのテクスチャを優先してテクスチャとする
		//　　：上記により、一つのSpriteを作っても、それは、立った一つのテクスチャ専用のクラスとしないので、複数のテクスチャのSpriteクラスとできる（Spriteに関しては、皆頂点数が同じで、UVも同じなので、それを、複数のクラスで所有してしまうのはもったいない。なので、共通して使えるときは、Spriteのテクスチャだけを切り替えるようにする）
		//　　：毎フレームpPriorityTexture_は、nullptrにて初期化される
		//　　：初期化によって、次のフレームは指定されていない→nullptrであるならば、自身の所有しているテクスチャを優先する
	Texture* pPriorityTexture_;

//protected メソッド
protected : 
	
	//引数ピクセルをプロジェクション座標値に変更する
		//詳細：引数ピクセル値をスクリーン座標のピクセル値をもとに、プロジェクション座標に変換する
		//引数：ピクセル値X
		//引数：ピクセル値Y
		//戻値：プロジェクション座標
	XMVECTOR GetPixelToWorldConversion(float x, float y);


	//テクスチャを画面に表示するため
	//テクスチャサイズを画面のサイズ比に合わせて拡大、縮小
		//詳細：拡大、縮小を行って、拡大縮小値を登録した行列をコンスタントバッファへ格納
		//引数：テクスチャ
		//引数：コンスタントバッファ
		//引数：トランスフォーム
		//戻値：なし
	void GetConstantBufferInfoEachForTexture(Texture* pTexture , CONSTANT_BUFFER* cb, Transform& trans);

	//テクスチャごとのシェーダーへのサンプラーなどへ情報を与える
		//詳細：シェーダーへテクスチャのサンプラー、リソースビューを伝える
		//引数：情報を与えるテクスチャ
		//戻値：なし
	void SetSamplerAndShaderResourceViewEachForTexture(Texture* pTexture);


//public メソッド
public:
	//コンストラクタ
		//引数：なし
		//戻値：なし
	Sprite();
	//デストラクタ
		//引数：なし
		//戻値：なし
	~Sprite();

	//テクスチャの画像ファイルのロード、初期化を行う（引数のファイル名より）
	//制限：画像ファイル名拡張子（.png , .jpg）
	//    ：�@画像のWidth　2の乗数
	// 	　：シェーダータイプ（2D専用）
	//引数：画像ファイル名
	//引数：シェーダータイプ（2D専用）
	//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT Initialize(const std::string& FILE_NAME , SHADER_TYPE thisShader);
	//テクスチャバッファからロード、初期化を行う
	//制限：�@バッファ画像のWidth　2の乗数
	// 	　：シェーダータイプ（2D専用）
	//引数：テクスチャバッファ
	//引数：シェーダータイプ（2D専用）
	//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT Initialize(ID3D11Texture2D* pTexture, SHADER_TYPE thisShader);

	//頂点バッファ作成
		//詳細：Spriteのをポリゴン作る頂点情報を作成
		//　　：頂点情報を頂点バッファに登録
		//    ：頂点3つにてポリゴンを作る、△ポリゴン2つにて四角形を作る。その四角形が画像を張り付ける板となる
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT InitVertex();

	//テクスチャのロード
		//引数：画像ファイル名
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT TextureLoad(const std::string& FILE_NAME);
	//テクスチャのロード
		//引数：テクスチャのバッファ
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT TextureLoad(ID3D11Texture2D* pTexture);


	//ポリゴンクラスのポリゴンの描画を行う
	//詳細：ポリゴンクラスに登録されたポリゴン情報を引数Transform位置へ描画する
	//　　：ポリゴンは、ポリゴンの頂点情報とポリゴンを作るインデックス情報により作られている
	//　　：各ポリゴン単位で頂点情報をシェーダーの頂点シェーダーに送ることで、ポリゴン事最終的にピクセルで出力される
	//引数：描画時の変形情報（移動、拡大、回転）（ローカル座標（メソッドにて、親も考慮したワールド座標を取得可能））
	//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	HRESULT Draw(Transform transform);

	//解放
		//引数：なし
		//戻値：なし
	void Release();

//public メソッド
public : 


	//シェーダーを切り替える
		//詳細：シェーダー切替えにおける注意は、Image->ChangeShaderにて
		//制限：２D専用シェーダー
		//引数：切り替え先のシェーダータイプ
		//戻値：なし
	void ChangeShader(SHADER_TYPE shaderType);


	//自身メンバのA値をセットする
		//引数：設定α値
		//戻値：なし
	void SetAlpha(float alpha);


	//現在のシェーダーを返す
		//引数：なし
		//戻値：シェーダータイプ
	SHADER_TYPE GetMyShader() { return thisShader_; };

	//Spriteクラスが次のDrawにて、優先するテクスチャを自身のメンバにセットする
		//詳細：次のDrawは、引数のテクスチャを優先してDrawする
		//　　：その次のDrawは優先されない。一回のDrawだけ対象
		//引数：優先するテクスチャ
		//戻値：なし
	void SetPriorityTexture(Texture* pPriorityTexture);



	/*画像の衝突判定***********************************************************************/
		/*
			マウスの座標をピクセル単位で取得

			画像をピクセル単位で管理

			マウスの座標が、画像範囲にあるか
		
		*/
	
	//画像と衝突判定
		//詳細：衝突結果は、引数のデータ群内の処理結果格納変数へ格納される
		//引数：衝突判定のためのデータ群
		//戻値：なし
	void HitCollision(ImageCollision* imageCollision);

	//最高値、最低値　を超えない、下回らない値であることを確認し、
		//詳細：引数値がMAXを超えていた場合：最高値に切り詰める
		//　　：引数値がMINを下回った　場合：最低値に切り詰める
		//　　：引数値が間の値の　　　 場合：そのまま
		//引数：確認する値
		//引数：最高値
		//引数：最低値
		//戻値：結果値（↑参照（2行目〜4行目））
	float WithinRange(int value, int max, int min);

	//外積を求める
		//引数：座標１
		//引数：座標２
		//引数：座標３
		//戻値：外積の結果値
	float Sign(XMVECTOR point1, XMVECTOR point2, XMVECTOR point3);

	//三角形と点の内外判定
		//引数：点の座標
		//引数：三角形の頂点１
		//引数：三角形の頂点２
		//引数：三角形の頂点３
		//戻値：内外判定（内：true,外：false）
	bool PointInTriangle(XMVECTOR point, XMVECTOR v1, XMVECTOR v2, XMVECTOR v3);

	//自身のテクスチャの画像サイズを取得
		//引数：なし
		//戻値：テクスチャサイズ
	XMVECTOR GetMyTextureSize();



};


