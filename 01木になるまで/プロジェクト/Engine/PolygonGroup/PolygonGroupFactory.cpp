#include "PolygonGroupFactory.h"	//ヘッダ

//シェーダーの継承元クラス
#include "PolygonGroup.h"
//シェーダーの継承先クラス
#include "Fbx.h"
#include "HeightMapGround.h"
#include "HeightMapCreater.h"
//#include "BranchesMakeTree.h"
#include "Branch.h"
#include "Plane.h"


//ポリゴンクラス（PolygonGroup＊）のインスタンスを作成し、インスタンスを返す
PolygonGroup* PolygonGroupFactory::CreatePolygonGroupClass(POLYGON_GROUP_TYPE type)
{
	switch (type)
	{
	case POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP :
		return new Fbx;
	case POLYGON_GROUP_TYPE::HEIGHT_MAP_POLYGON_GROUP:
		return new HeightMapGround;
	case POLYGON_GROUP_TYPE::HEIGHT_MAP_CREATER_POLYGON_GROUP :
		return new HeightMapCreater;
	//case BRANCHES_MAKE_TREE_POLYGON_GROUP:
	//	return new BranchesMakeTree;
	case POLYGON_GROUP_TYPE::BRANCH_POLYGON_GROUP :
		return new Branch;
	case POLYGON_GROUP_TYPE::PLANE_POLYGON_GROUP :
		return new Plane;

	default:
		return nullptr;

	}

	return nullptr;
}
