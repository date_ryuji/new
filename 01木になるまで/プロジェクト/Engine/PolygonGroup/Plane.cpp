#include "Plane.h"						//ヘッダ
#include "../DirectX/Texture.h"			//テクスチャクラス
#include "../DirectX/Camera.h"			//カメラクラス
#include "../../Shader/BaseShader.h"	//シェーダークラス（継承元）


//コンストラクタ
Plane::Plane() :
PolygonGroup::PolygonGroup(),

pTexture_(nullptr) , 
pVertices_(nullptr),
pVertexBuffer_(nullptr),
pIndex_(nullptr),
pIndexBuffer_(nullptr),
pConstantBuffer_(nullptr),
vertexCount_(0), indexCountEachMaterial_(0), polygonCount_(0)
{
}

//デストラクタ
Plane::~Plane()
{
}

//ロード
HRESULT Plane::Load(const std::string& TEXTURE_FILE_NAME , SHADER_TYPE thisShader)
{
	//シェーダーの格納
	thisShader_ = thisShader;

	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	{
		//頂点情報の初期化
		hr = InitVertex();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点情報の生成失敗", "エラー");
		//頂点バッファの作成
		hr = CreateVertexBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点バッファーの生成失敗", "エラー");
	}

	{
		//インデックス情報の初期化
		hr = InitIndex();
		//エラーメッセージ
		ERROR_CHECK(hr, "インデックス情報の生成失敗", "エラー");
		//インデックスバッファの初期化
		hr = CreateIndexBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "インデックスバッファーの生成失敗", "エラー");
	}

	{
		//コンスタントバッファの初期化
		hr = CreateConstantBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファーの生成失敗", "エラー");
	}

	{
		//テクスチャのロード
		hr = LoadTexture(TEXTURE_FILE_NAME);
		//エラーメッセージ
		ERROR_CHECK(hr, "テクスチャの生成失敗", "エラー");
	}

	//処理の成功
	return S_OK;
}

//Planeテクスチャのロード
HRESULT Plane::LoadTexture(std::string fileName)
{
	//インスタンスの作成
	pTexture_ = new Texture;
	//ファイルのロード
		//テクスチャのロード
	HRESULT hr = pTexture_->Load(fileName);

	//処理の失敗時
	if (FAILED(hr))
	{
		//解放
		pTexture_->Release();
		SAFE_DELETE(pTexture_);
	}
	//エラーメッセージ
	ERROR_CHECK(hr, "ファイルの読み込みを失敗", "エラー");


	//ロード時
	return S_OK;

}

//ポリゴンクラスのポリゴンの描画を行う
HRESULT Plane::Draw(Transform & transform)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	{
		//シェーダーを選択
		Direct3D::SetShaderBundle(thisShader_);
	}


	//シェーダーにおけるコンスタントバッファへわたす情報をまとめる
		//詳細：自クラスの　CONSTANT_BUFFER(sturct)とシェーダーファイルにおけるコンスタントバッファの構造体　の格納数、格納情報を等しくしているため、
		//　　：伴い、描画に必要な現在の情報をCONSTANT_BUFFER(sturct)　に格納して、最終的にシェーダーファイルに渡す

	//コンスタントバッファ　格納情報
	CONSTANT_BUFFER cb;

	//コンスタントバッファ　へのデータを格納（準備）
	{
		//ワールド　＊　ビュー　＊　プロジェクション行列
		cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() * Camera::GetProjectionMatrix());


		//条件：テクスチャあり
		if (pTexture_ != nullptr)
		{

			//nullptrでない　＝　テクスチャが存在する
			//「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
				//サンプラーは、どこに貼り付けますか？などの情報
				//hlsl : g_sampler(s0)
			ID3D11SamplerState* pSampler = pTexture_->GetSampler();	//テクスチャクラスから、サンプラーを取得
				//第一引数：サンプラー　番目
			Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

			//シェーダーへの橋渡しのビューを取得
				//hlsl : g_texture(t0)
			ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
				//第一引数：テクスチャ　番目
			Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);


			//テクスチャ　有
				//コンスタントバッファのテクスチャ有無にtrueを立てる
			cb.isTexture = TRUE;
		}
		//テクスチャなし
		else
		{
			//nullptrである　＝　テクスチャが存在しない
				//　＝　マテリアルそのものの色を使用しないといけない

			//テクスチャ　無
			cb.isTexture = FALSE;

		}
	}


	//コンスタントバッファ1の確保
	{
		//シェーダーのコンスタントバッファ１のマップ
			//コンスタントバッファ1へのアクセス許可
		hr = Direct3D::GetShaderClass(thisShader_)->MapShaderConstantBuffer1View();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファMap失敗", "エラー");


		//自身の使用する
			//シェーダー独自の
			//コンスタンバッファ１や、テクスチャの受け渡しを行う
			//処理は、シェーダークラスに一任する
		Direct3D::GetShaderClass(thisShader_)->ShaderConstantBuffer1View();


		//シェーダーのコンスタントバッファ１のアンマップ
			//コンスタントバッファ1へのアクセス拒否
		hr = Direct3D::GetShaderClass(thisShader_)->UnMapShaderConstantBuffer1View();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファUnMap失敗", "エラー");
	}

	//コンスタントバッファをシェーダーファイルへコピー、渡す
	{
		//バッファへのマップ設定
			//ポインタへのアクセスなどに使用する
		D3D11_MAPPED_SUBRESOURCE pdata;
		//コンスタントバッファへのマップ
			//詳細：アクセス許可
			//　　：シェーダーなどのGPUでしか、触れることのできないものに
			//　　：CPUから触れる準備
			//　　：マップを行い、バッファへのアクセスができるようになるが、その詳細のアクセスを上記pdataにて行う。
		hr = Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファのマップの書き込み失敗", "エラー");


		//メモリにコピー
			//シェーダーファイル側のコンスタントバッファ（struct）へ 
			//データを値を送る
		memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));
	}

	//頂点情報のセット
	{
		//頂点バッファをセット（頂点を持っておくための領域（確保場所））
			//詳細：複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		//1頂点のサイズを指定する
			//詳細：位置のみであれば、XMVECTORの構造体であったが
			//　　：位置やUVも確保するため、頂点情報の構造体のサイズを受け取らせる
		UINT stride = sizeof(Plane::VERTEX);	
		
		UINT offset = 0;
		//頂点バッファをセット
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

		//サイズの更新
			//詳細：インデックスバッファに登録している情報は、int情報の配列
			//　　：そのため、intのサイズを渡す
		stride = sizeof(int);
		
		offset = 0;
		// インデックスバッファーをセット
		Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	}

	
	//コンスタントバッファをセット
	{
		//コンスタントバッファをセット
		//頂点シェーダー用	
		Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);
		//ピクセルシェーダー用
		Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);
	
		//コンスタントバッファへのアンマップ
			//詳細：アクセス拒否
		Direct3D::pContext->Unmap(pConstantBuffer_, 0);

	}

	

	//描画
		//詳細　　：インデックス数を渡して、頂点によるポリゴンの描画を宣言 
		//第一引数：描画インデックス数　描画の対象となるポリゴンを作るインデックスの数
	Direct3D::pContext->DrawIndexed(indexCountEachMaterial_, 0, 0);



	//処理の成功
	return S_OK;

}

//解放
void Plane::Release()
{
	//ポインタ宣言順と逆に解放
	if (pTexture_ != nullptr)
	{
		pTexture_->Release();
	}
	SAFE_DELETE(pTexture_);
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_RELEASE(pVertexBuffer_);
	SAFE_DELETE_ARRAY(pVertices_);
}

//レイキャスト
void Plane::RayCast(RayCastData* rayData)
{
	//省略
	//衝突判定　計算はほかのポリゴン群と変わらない
}


//頂点バッファ作成
HRESULT Plane::InitVertex()
{
	//頂点数
	static constexpr unsigned int VERTEX_COUNT = 4;

	//頂点数　メンバ変数へ保存
	vertexCount_ = VERTEX_COUNT;


	// 頂点情報
		//詳細：Planeは平面の奥行きのない四角ポリゴンにて再現する
		//　　：そのため頂点4つを正方形（四角形）ができるように並ぶ頂点情報を作成する
	Plane::VERTEX vertices[VERTEX_COUNT] =
	{

		{XMVectorSet(-1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)},	// 四角形の頂点（左上）

		{XMVectorSet(1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f)},		// 四角形の頂点（右上）

		{XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f)},		// 四角形の頂点（右下）

		{XMVectorSet(-1.0f, -1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)},	// 四角形の頂点（左下）		

	};

	//メンバの頂点情報を動的確保
	pVertices_ = new VERTEX[VERTEX_COUNT];


	//メンバの頂点情報にコピーする
	memcpy(pVertices_, vertices, sizeof(VERTEX) * VERTEX_COUNT);

	//処理の成功
	return S_OK;
}

//インデックスバッファ作成
HRESULT Plane::InitIndex()
{
	//インデックス数
	//頂点数 - 1 * ポリゴン数

	polygonCount_ = 2;
	indexCountEachMaterial_ = 6;

	//四角形を作るインデックス情報を格納
		//詳細：表のポリゴンは時計回りのインデックス情報にて実現可能
	pIndex_ = new int[indexCountEachMaterial_] {0, 1, 3, 1, 2, 3};


	//処理の成功
	return S_OK;
}

//頂点情報から頂点バッファーを作成
HRESULT Plane::CreateVertexBuffer()
{
	//verticesはポインタで動的配列になったので、sizeof(vertices)では正しくサイズを取得することができない。
	//	この場合はVERTEX型のサイズ*頂点数を指定すればよい。
	//頂点の数はわかっているので、→vertexの型でいくらのサイズをbyteで使うのか、＊頂点数→最終的に使用する全体サイズ

	//バッファの詳細情報を格納する構造体
		//頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;

	//頂点バッファの使用するバイト長を登録
		//頂点情報のサイズの登録
		//型名＊頂点数（頂点数だけ、VERTEXを取得している）（VERTEXの情報が何個必要なのか）
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	bd_vertex.CPUAccessFlags = 0;

	bd_vertex.MiscFlags = 0;

	bd_vertex.StructureByteStride = 0;

	//バッファへのアクセスポインタ
	//頂点情報のリソースを入れる構造体
	D3D11_SUBRESOURCE_DATA data_vertex;
	//頂点情報を代入
	data_vertex.pSysMem = pVertices_;


	//Direct３DにおけるpDeviceにそれぞれのバッファを作成する
	//頂点バッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");

	//処理の成功
	return S_OK;

}

//インデックス情報からインデックスバッファーを作成
HRESULT Plane::CreateIndexBuffer()
{
	//バッファの詳細情報を格納する構造体
		//インデックス情報の設定
	D3D11_BUFFER_DESC   bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	//int型　＊　インデックス数
	bd.ByteWidth = sizeof(int) * indexCountEachMaterial_;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;


	//バッファーへのアクセスポインタ
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex_;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	//インデックスバッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "インデックスバッファ作成失敗", "エラー");

	//処理の成功
	return S_OK;

}

//コンスタントバッファの作成
HRESULT Plane::CreateConstantBuffer()
{
	//バッファの詳細情報を格納する構造体
		//コンスタントバッファ情報の設定
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(Plane::CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//コンスタントバッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "コンスタントバッファ作成失敗", "エラー");


	//処理の成功
	return S_OK;
}


//頂点数を取得
int Plane::GetVertexCount()
{
	return vertexCount_;
}

//ポリゴン数を取得
int Plane::GetPolygonCount()
{
	return polygonCount_;
}

//インデックス数を取得
int Plane::GetIndexCount()
{
	return indexCountEachMaterial_;
}

//頂点情報群を取得
Plane::VERTEX * Plane::GetVertexes()
{
	return pVertices_;
}

//インデックス情報群を取得
int * Plane::GetIndexes()
{
	return pIndex_;
}

//頂点情報　コンストラクタ
Plane::VERTEX::VERTEX() :
	position(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	uv(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}

//頂点情報　コンストラクタ
	//頂点情報の初期化の時、
	//専用のコンストラクタ（要素のベクトル3つを初期値としてセットして宣言する形）がないと
	//宣言できないので専用のコンストラクタをセットする
Plane::VERTEX::VERTEX(XMVECTOR setPos, XMVECTOR setUv):
	position(setPos),
	uv(setUv)
{
}
