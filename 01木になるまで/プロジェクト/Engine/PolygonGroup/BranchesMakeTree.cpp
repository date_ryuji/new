#include "BranchesMakeTree.h"			//ヘッダ
#include "../DirectX/Texture.h"			//テクスチャ
#include "Branch.h"						//枝クラス
#include "Plane.h"						//平面クラス
#include "../DirectX/Camera.h"			//カメラオブジェクト
#include "../../Shader/BaseShader.h"	//シェーダークラス　継承元


//コンストラクタ
BranchesMakeTree::BranchesMakeTree() :
	PolygonGroup::PolygonGroup(),

	pVertices_(nullptr),
	pVertexBuffer_(nullptr),
	pIndex_(nullptr),
	pIndexBuffer_(nullptr),
	pConstantBuffer_(nullptr),
	pTexture_(nullptr),
	vertexCount_(0), indexCountEachMaterial_(0), polygonCount_(0), materialCount_(0)
{
}

//デストラクタ
BranchesMakeTree::~BranchesMakeTree()
{
}

//ロード
HRESULT BranchesMakeTree::Load(const std::string& FILE_NAME , SHADER_TYPE thisShader)
{
	//BranchesMakeTree　クラス単体では、ポリゴンを持たない
	//外部からのポリゴンの結合を経て、
		//初めてポリゴンを持つことができる
		//そのためロードにおいて行う初期化はない

	//処理の成功
	return S_OK;
}

//初期化
HRESULT BranchesMakeTree::Initialize(const std::string& TEXTURE_FILE_NAME, SHADER_TYPE thisShader)
{
	//シェーダーのセット
	thisShader_ = thisShader;
	
	//処理の結果を入れる変数
	HRESULT hr = S_OK;


	//コンスタントバッファー作成
	hr = CreateConstantBuffer();
	//エラーメッセージ
	ERROR_CHECK(hr, "コンスタントバッファ作成失敗", "エラー");


	//テクスチャのロード
	hr = LoadTexture(TEXTURE_FILE_NAME);
	//エラーメッセージ
	ERROR_CHECK(hr, "テクスチャの生成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//描画
HRESULT BranchesMakeTree::Draw(Transform & transform)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;


	//シェーダーを選択
	Direct3D::SetShaderBundle(thisShader_);

	//シェーダーにおけるコンスタントバッファへわたす情報をまとめる
				//詳細：自クラスの　CONSTANT_BUFFER(sturct)とシェーダーファイルにおけるコンスタントバッファの構造体　の格納数、格納情報を等しくしているため、
				//　　：伴い、描画に必要な現在の情報をCONSTANT_BUFFER(sturct)　に格納して、最終的にシェーダーファイルに渡す

			//コンスタントバッファ　格納情報
	CONSTANT_BUFFER cb;

	//コンスタントバッファ　へのデータを格納（準備）
	{
		//ワールド　＊　ビュー　＊　プロジェクション行列
		cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() * Camera::GetProjectionMatrix());

		//ワールド行列
			//詳細：法線の回転のために必要なワールド行列
		cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));	//

		//ワールド行列
			//詳細：ハイライト表示のためにカメラのワールド座標を求める必要がある 
		cb.matWorld = XMMatrixTranspose(transform.GetWorldMatrix());

		//カメラの座標を入れる
		cb.camPos = XMFLOAT4(Camera::position_.vecX, Camera::position_.vecY, Camera::position_.vecZ, 0);

		//条件：テクスチャがある
		if (pTexture_ != nullptr)
		{


			//nullptrでない　＝　テクスチャが存在する
				//「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
				//サンプラーは、どこに貼り付けますか？などの情報
				//hlsl : g_sampler(s0)
			ID3D11SamplerState* pSampler = pTexture_->GetSampler();
			//第一引数：サンプラー　番目
			Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

			//シェーダーへの橋渡しのビューを取得
			//テクスチャのリソース部分
				//hlsl : g_texture(t0)
			ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
				//第一引数：テクスチャ　番目
			Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);

			//テクスチャ　有
			cb.isTexture = TRUE;
		}
		//条件：テクスチャなし
		else
		{

			//nullptrである　＝　テクスチャが存在しない
				//　＝　マテリアルそのものの色を使用しないといけない

			//テクスチャ　無
			cb.isTexture = FALSE;

		}

	}

	//コンスタントバッファ1の確保
	{
		//シェーダーのコンスタントバッファ１のマップ
			//コンスタントバッファ1へのアクセス許可
		hr = Direct3D::GetShaderClass(thisShader_)->MapShaderConstantBuffer1View();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファMap失敗", "エラー");


		//自身の使用する
			//シェーダー独自の
			//コンスタンバッファ１や、テクスチャの受け渡しを行う
			//処理は、シェーダークラスに一任する
		Direct3D::GetShaderClass(thisShader_)->ShaderConstantBuffer1View();


		//シェーダーのコンスタントバッファ１のアンマップ
			//コンスタントバッファ1へのアクセス拒否
		hr = Direct3D::GetShaderClass(thisShader_)->UnMapShaderConstantBuffer1View();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファUnMap失敗", "エラー");
	}


	//コンスタントバッファをシェーダーファイルへコピー、渡す
	{
		//バッファへのマップ設定
			//ポインタへのアクセスなどに使用する
		D3D11_MAPPED_SUBRESOURCE pdata;
		//コンスタントバッファへのマップ
			//詳細：アクセス許可
			//　　：シェーダーなどのGPUでしか、触れることのできないものに
			//　　：CPUから触れる準備
			//　　：マップを行い、バッファへのアクセスができるようになるが、その詳細のアクセスを上記pdataにて行う。
		hr = Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファのマップの書き込み失敗", "エラー");


		//メモリにコピー
			//シェーダーファイル側のコンスタントバッファ（struct）へ 
			//データを値を送る
		memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));

	}


	//頂点情報のセット
	{
		//頂点バッファをセット（頂点を持っておくための領域（確保場所））
			//詳細：複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		//1頂点のサイズを指定する
			//詳細：位置のみであれば、XMVECTORの構造体であったが
			//　　：位置やUVも確保するため、頂点情報の構造体のサイズを受け取らせる
		UINT stride = sizeof(VERTEX);
		UINT offset = 0;
		//頂点バッファのセット
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

		//サイズの更新
			//詳細：インデックスバッファに登録している情報は、int情報の配列
			//　　：そのため、intのサイズを渡す
		stride = sizeof(int);
		offset = 0;
		//インデックスバッファのセット
		Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	}

	//コンスタントバッファをセット
	{
		//コンスタントバッファをセット
		//頂点シェーダー用	
		Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);
		//ピクセルシェーダー用
		Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);

		//コンスタントバッファへのアンマップ
			//詳細：アクセス拒否
		Direct3D::pContext->Unmap(pConstantBuffer_, 0);

	}


	//描画
		//詳細　　：インデックス数を渡して、頂点によるポリゴンの描画を宣言 
		//第一引数：描画インデックス数　描画の対象となるポリゴンを作るインデックスの数
	Direct3D::pContext->DrawIndexed(indexCountEachMaterial_, 0, 0);



	//処理の成功
	return S_OK;

}

//解放
void BranchesMakeTree::Release()
{
	//ポインタ宣言順と逆に解放
	if (pTexture_ != nullptr)
	{
		pTexture_->Release();
	}
	SAFE_DELETE(pTexture_);
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_RELEASE(pVertexBuffer_);
	SAFE_DELETE_ARRAY(pVertices_);

}

//３Dポリゴンクラス（Branch）を自身のモデルに結合
HRESULT BranchesMakeTree::Join3DPolygon(Branch * pBranch, Transform& trans)
{
	//Transformによって、ワールド行列を取得し、
	//各頂点のローカル座標を変形。

	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	//頂点数を取得
	int toVertexCount = pBranch->GetVertexCount();

	{
		//頂点数分
			//変換先の頂点情報を入れる要素を確保
			//頂点情報は、　構造体の内容は同じだが、
			//コピー先の構造体は、BranchesMakeTreeのほうの構造体だと示す必要がある（なぜならば、BranchのほうのVERTEXはpublicになっているので、混合してしまう。ので、明記）
		BranchesMakeTree::VERTEX* pToVertex = new BranchesMakeTree::VERTEX[toVertexCount];

		//引数３Dポリゴンモデルの頂点情報の取得
		Branch::VERTEX* originalVertex = pBranch->GetVertexes();


		//ワールド行列の計算
		trans.Calclation();


		//引数３Dポリゴンを
			//ワールド行列にて、変換
			//座標をワールド行列（拡大＊回転＊移動）を掛けて、変形
		for (int i = 0; i < toVertexCount; i++)
		{
			//頂点座標の変換
			pToVertex[i].position = XMVector3TransformCoord(originalVertex[i].position, trans.GetWorldMatrix());

			//UVの登録（UV情報はそのまま）
			pToVertex[i].uv = originalVertex[i].uv;

			//法線情報の変換
			pToVertex[i].normal = XMVector3TransformCoord(originalVertex[i].normal, trans.GetWorldMatrix());

		}


		//結合
		//現在ある自身のクラスの頂点情報に(後述：Tree)
		//引数にてもらった、今から結合するクラス(後述：Branch)の頂点情報を結合
		/*
		Branchの頂点数を調べて、
			�@Treeの頂点数＋Branch頂点数の数分、Vertexを確保、
			�ATreeの頂点をコピーして、�BBranchの頂点情報をコピー
		*/

		//�@
		BranchesMakeTree::VERTEX* pNextVertex = new BranchesMakeTree::VERTEX[vertexCount_ + toVertexCount];
		//�A
		memcpy(pNextVertex, pVertices_, sizeof(BranchesMakeTree::VERTEX) * vertexCount_);


		//�B
			//要素数 [vertexCount_ - 1]まで、要素を確保しているので、
			//新しい頂点情報は、[vertexCount]から、toVertexCount分確保
		memcpy(&pNextVertex[vertexCount_], pToVertex, sizeof(BranchesMakeTree::VERTEX) * toVertexCount);

		//古い頂点情報の削除
		SAFE_DELETE_ARRAY(pVertices_);
		SAFE_DELETE_ARRAY(pToVertex);


		//新しい頂点情報の登録
		pVertices_ = pNextVertex;

		//頂点数の更新
		vertexCount_ = vertexCount_ + toVertexCount;

		//頂点バッファー作成
		hr = CreateVertexBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");

	}



	{
		//インデックス情報
		//インデックス数を取得
		int toIndexCount = pBranch->GetIndexCount();
		//新しい頂点情報の格納先
		int* pToIndex = new int[toIndexCount];

		//引数３Dポリゴンのインデックス情報の取得
		int* pOriginalIndex = pBranch->GetIndexes();


		//結合
		//インデックス情報の登録
		for (int i = 0; i < toIndexCount; i++)
		{
			//インデックス情報は
				//引数ポリゴンのBranchの元のインデックス情報に、＋元の頂点数　を足すことで、頂点上番号を取得できる
				//下記の計算をすることで、
				//登録先の頂点番号と番号を合わせることが可能である
			pToIndex[i] = (vertexCount_ - toVertexCount) + pOriginalIndex[i];

		}
		//結合
		//現在ある自身のクラスのインデックス情報に
		//引数にてもらった、今から結合するクラスのインデックス情報を結合
		/*
		Branchのインデックス数を調べて、
			�@Treeのインデックス数＋Branchインデックス数の数分、intを確保、
			�ATreeのインデックスをコピーして、�BBranchのインデックス情報をコピー
		*/

		//�@
		int* pNextIndex = new int[indexCountEachMaterial_ + toIndexCount];

		//�A
		memcpy(pNextIndex, pIndex_, sizeof(int) * indexCountEachMaterial_);

		//�B
		//要素数 [indexCountEachMaterial_ - 1]まで、要素を確保しているので、
		//新しい頂点情報は、[indexCountEachMaterial_]から、toIndexCount分確保
		memcpy(&pNextIndex[indexCountEachMaterial_], pToIndex, sizeof(int) * toIndexCount);

		//古い頂点情報の削除
		SAFE_DELETE_ARRAY(pIndex_);
		SAFE_DELETE_ARRAY(pToIndex);


		//新しい頂点情報の登録
		pIndex_ = pNextIndex;

		//頂点数の更新
		indexCountEachMaterial_ = indexCountEachMaterial_ + toIndexCount;


		//インデックスバッファー作成
		hr = CreateIndexBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "インデックスバッファの作成失敗", "エラー");


	}


	//処理の成功
	return S_OK;
}

//３Dポリゴンクラス（Plane）を自身のモデルに結合
HRESULT BranchesMakeTree::Join3DPolygon(Plane * pPlane, Transform & trans)
{
	//Transformによって、ワールド行列を取得し、
	//各頂点のローカル座標を変形。

	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	//頂点数を取得
	int toVertexCount = pPlane->GetVertexCount();

	{
		//頂点数分
			//変換先の頂点情報を入れる要素を確保
			//頂点情報は、　構造体の内容は同じだが、
			//コピー先の構造体は、BranchesMakeTreeのほうの構造体だと示す必要がある（なぜならば、BranchのほうのVERTEXはpublicになっているので、混合してしまう。ので、明記）
		BranchesMakeTree::VERTEX* pToVertex = new BranchesMakeTree::VERTEX[toVertexCount];

		//引数３Dポリゴンモデルの頂点情報の取得
		Plane::VERTEX* originalVertex = pPlane->GetVertexes();


		//ワールド行列の計算
		trans.Calclation();


		//引数３Dポリゴンを
			//ワールド行列にて、変換
			//座標をワールド行列（拡大＊回転＊移動）を掛けて、変形
		for (int i = 0; i < toVertexCount; i++)
		{
			//頂点座標の変換
			pToVertex[i].position = XMVector3TransformCoord(originalVertex[i].position, trans.GetWorldMatrix());

			//UVの登録（UV情報はそのまま）
			pToVertex[i].uv = originalVertex[i].uv;

			//法線情報の変換
			pToVertex[i].normal = XMVectorSet(0.f, 0.f, 0.f, 0.f);

		}


		//結合
		//現在ある自身のクラスの頂点情報に(後述：Tree)
		//引数にてもらった、今から結合するクラス(後述：Branch)の頂点情報を結合
		/*
		Branchの頂点数を調べて、
			�@Treeの頂点数＋Branch頂点数の数分、Vertexを確保、
			�ATreeの頂点をコピーして、�BBranchの頂点情報をコピー
		*/

		//�@
		BranchesMakeTree::VERTEX* pNextVertex = new BranchesMakeTree::VERTEX[vertexCount_ + toVertexCount];
		//�A
		memcpy(pNextVertex, pVertices_, sizeof(BranchesMakeTree::VERTEX) * vertexCount_);


		//�B
			//要素数 [vertexCount_ - 1]まで、要素を確保しているので、
			//新しい頂点情報は、[vertexCount]から、toVertexCount分確保
		memcpy(&pNextVertex[vertexCount_], pToVertex, sizeof(BranchesMakeTree::VERTEX) * toVertexCount);

		//古い頂点情報の削除
		SAFE_DELETE_ARRAY(pVertices_);
		SAFE_DELETE_ARRAY(pToVertex);


		//新しい頂点情報の登録
		pVertices_ = pNextVertex;

		//頂点数の更新
		vertexCount_ = vertexCount_ + toVertexCount;

		//頂点バッファー作成
		hr = CreateVertexBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");

	}



	{
		//インデックス情報
		//インデックス数を取得
		int toIndexCount = pPlane->GetIndexCount();
		//新しい頂点情報の格納先
		int* pToIndex = new int[toIndexCount];

		//引数３Dポリゴンのインデックス情報の取得
		int* pOriginalIndex = pPlane->GetIndexes();


		//結合
		//インデックス情報の登録
		for (int i = 0; i < toIndexCount; i++)
		{
			//インデックス情報は
				//引数ポリゴンのBranchの元のインデックス情報に、＋元の頂点数　を足すことで、頂点上番号を取得できる
				//下記の計算をすることで、
				//登録先の頂点番号と番号を合わせることが可能である
			pToIndex[i] = (vertexCount_ - toVertexCount) + pOriginalIndex[i];

		}
		//結合
		//現在ある自身のクラスのインデックス情報に
		//引数にてもらった、今から結合するクラスのインデックス情報を結合
		/*
		Branchのインデックス数を調べて、
			�@Treeのインデックス数＋Branchインデックス数の数分、intを確保、
			�ATreeのインデックスをコピーして、�BBranchのインデックス情報をコピー
		*/

		//�@
		int* pNextIndex = new int[indexCountEachMaterial_ + toIndexCount];

		//�A
		memcpy(pNextIndex, pIndex_, sizeof(int) * indexCountEachMaterial_);

		//�B
		//要素数 [indexCountEachMaterial_ - 1]まで、要素を確保しているので、
		//新しい頂点情報は、[indexCountEachMaterial_]から、toIndexCount分確保
		memcpy(&pNextIndex[indexCountEachMaterial_], pToIndex, sizeof(int) * toIndexCount);

		//古い頂点情報の削除
		SAFE_DELETE_ARRAY(pIndex_);
		SAFE_DELETE_ARRAY(pToIndex);


		//新しい頂点情報の登録
		pIndex_ = pNextIndex;

		//頂点数の更新
		indexCountEachMaterial_ = indexCountEachMaterial_ + toIndexCount;


		//インデックスバッファー作成
		hr = CreateIndexBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "インデックスバッファの作成失敗", "エラー");

	}

	//処理の成功
	return S_OK;

}

//頂点情報の初期化
HRESULT BranchesMakeTree::InitVertex()
{
	return S_OK;
}

//頂点バッファの作成
HRESULT BranchesMakeTree::CreateVertexBuffer()
{
	//元の頂点バッファーの解放
	SAFE_RELEASE(pVertexBuffer_);



	//バッファの詳細情報を格納する構造体
	//頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;
	//VERTEXのスコープを定義する
		//BranchesMakeTree::VERTEX
	bd_vertex.ByteWidth = sizeof(BranchesMakeTree::VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;

	//バッファへのアクセスポインタ
	//頂点情報のリソースを入れる構造体
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = pVertices_;

	/*
	////メンバ変数に保存
	//data_vertex_ = new D3D11_SUBRESOURCE_DATA;
	////ここに保存を行って、更新の時は取り出し、更新するというやり方を行う。
	//	//だが、これだとメンバ変数として持っておくことと変わらない。（どっかに動的確保したもののアドレスを持っているだけなので、、、、　これなら、メンバ変数として持っておくほうが幾分見やすい）
	//data_vertex_->pSysMem = vertices;
	*/


	//頂点バッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//インデックス情報の初期化
HRESULT BranchesMakeTree::InitIndex()
{
	return S_OK;
}

//インデックス情報からインデックスバッファーを作成
HRESULT BranchesMakeTree::CreateIndexBuffer()
{
	//元のインデックスバッファーの解放
	SAFE_RELEASE(pIndexBuffer_);


	// インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	//サイズを、indexのサイズ分だけ、（インデックス情報の入っているサイズだけ）
	bd.ByteWidth = sizeof(int) * (indexCountEachMaterial_);	
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;


	//バッファーへのアクセスポインタ
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex_;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	//インデックスバッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "インデックスバッファの作成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//コンスタントバッファの作成	
HRESULT BranchesMakeTree::CreateConstantBuffer()
{
	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//コンスタントバッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "コンスタントバッファの作成失敗", "エラー");

	//処理の成功
	return S_OK;

}

//テクスチャのロード
HRESULT BranchesMakeTree::LoadTexture(const std::string& TEXTURE_FILE_NAME)
{
	pTexture_ = new Texture;
	//ファイルのロード
		//テクスチャのロード
	HRESULT hr = pTexture_->Load(TEXTURE_FILE_NAME);
	
	if (FAILED(hr))
	{
		//テクスチャの解放
		pTexture_->Release();
		SAFE_DELETE(pTexture_);
	}
	//エラーメッセージ
	ERROR_CHECK(hr, "ファイルの読み込みを失敗", "エラー");

	//処理の成功
	return S_OK;
}

//コンストラクタ
BranchesMakeTree::VERTEX::VERTEX() :
	position(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	uv(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	normal(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}
