#include "PolygonGroup.h"			//ヘッダ
#include "../DirectX/Texture.h"		//テクスチャクラス

//コンストラクタ
PolygonGroup::PolygonGroup() :
	thisShader_(SHADER_TYPE::SHADER_MAX)
{
}

//デストラクタ
	//レベル：仮想関数
PolygonGroup::~PolygonGroup()
{
}

//ロード
	//レベル：仮想関数
HRESULT PolygonGroup::Load(const std::string& FILE_NAME, SHADER_TYPE thisShader)
{
	return S_OK;
}

//描画
	//レベル：仮想関数
HRESULT PolygonGroup::Draw(Transform & transform)
{
	return S_OK;
}

//解放
	//レベル：仮想関数
void PolygonGroup::Release()
{
}

//レイキャスト実行
	//レベル：仮想関数
void PolygonGroup::RayCast(RayCastData * rayData)
{
}

//法線取得
	//レベル：仮想関数
XMVECTOR PolygonGroup::NormalVectorOfCollidingFace(XMVECTOR & start, XMVECTOR & dir)
{
	return XMVECTOR();
}

//コンスタントバッファ取得
	//レベル：仮想関数
ID3D11Buffer * PolygonGroup::GetConstantBuffer()
{
	return nullptr;
}





RayCastData::RayCastData() :
	start(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	dir(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	permitNegativeDirection(FALSE) ,

	normal(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	dist(99999.0f),
	hit(FALSE),
	polygonNum(-1)
{
}

