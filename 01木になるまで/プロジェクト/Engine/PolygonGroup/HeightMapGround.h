#pragma once
#include "PolygonGroup.h"		//ポリゴングループ　２D3Dのポリゴンを扱うクラス群

/*
	クラス詳細	：ポリゴンクラス　HeightMap画像読み取り、地面のポリゴン作成専用
	クラスレベル：サブクラス（スーパークラス（PolygonGroup））
	クラス概要（詳しく）
				：ハイトマップ画像を読み込み
				：そのハイトマップ画像の　幅＊高さ　のピクセルを取得し、
				：その幅＊高さ分、頂点を取得
				：頂点一つ一つの高さを、　画像の1ピクセルの色情報から、高さを確定する。（（０，０）のピクセルの色が、(1,1,1)ならば、該当する頂点のY座標の高さを、あらかじめ決めて置いた最高Y座標位置の値に、
				　（500，0）のピクセル色が、(0,0,0)ならば、あらかじめ決めて置いた、最低Y座標の高さにするなど、画像のピクセルの色から、頂点の高さを決める。）

				：一般的には　パーリンノイズの手法に近い
*/
class HeightMapGround : public PolygonGroup
{
//protected 構造体
protected : 
	/*
	構造体詳細	：コンスタントバッファ構造体
	構造体概要（詳しく）
				：シェーダーに渡すシェーダー内グローバル情報
				：シェーダーのコンスタントバッファと同じサイズ、同じ順番の定義を行う
	制限：コンスタントバッファ16バイト未満の場合、コンスタントバッファのポインタ作成の際に、サイズを最低16バイトで初期化をする必要がある
		：16の倍数でサイズが取られている必要がある
		：構造体は、構造体内の変数内にて、一番型サイズの大きいもののサイズの倍数でサイズが取られるようになる。
		：例：構造体のサイズ　31バイト、構造体内最大の型サイズ　4バイト　＝　構造体のサイズは4の倍数に拡張される。余分なサイズが取られる。
		：　：最終的に、構造体のサイズは一番近い4の倍数になるので、32バイトとなる。
		：以上のことから、コンスタントバッファ構造体のサイズを考える際に、構造体ないに、行列（32バイト）が存在していれば、自動的にサイズが32の倍数になる。＝16の倍数
	
	アクセス権限理由：継承を行って、同じ今ストバッファを使用するため
	*/
	struct CONSTANT_BUFFER
	{
		//ワールド行列＊ビュー行列＊プロジェクション行列
			//詳細：スクリーン座標を求める際に使用する
		XMMATRIX	matWVP;
		//回転のためのワールド行列（移動行列なし）
			//詳細：法線の回転のために使用する
		XMMATRIX	matNormal;
		//ワールド行列　移動＊回転＊拡大　行列
			//詳細：ワールド座標として、ワールド内の距離を求める際に使用する
		XMMATRIX	matWorld;
		//カメラポジション（ワールド座標）
			//詳細：シェーダーの反射のため
		XMFLOAT4 camPos;

		//テクスチャが張られているか
		BOOL isTexture;
	};

	/*
		構造体詳細：頂点情報
		構造体詳細（詳しく）
				　：シェーダーファイルの頂点シェーダー、引数に渡す情報
		　　　　　：頂点ごとの情報、位置、UV,法線、接線をもち、最終的に頂点同士をつなげ、一つのポリゴンを作る。そのポリゴンの集合により任意の形を作る
	*/
	struct VERTEX
	{
		//位置情報（頂点の位置）
		XMVECTOR position;
		//UV情報（テクスチャの座標）
		XMVECTOR uv;
		//法線情報（頂点の向き）
		XMVECTOR normal;

		//コンストラクタ
		VERTEX();
	};



//protected メンバ変数、ポインタ、配列
protected:
	//地面の幅
	int width_;
	//地面の奥行き（高さ）
	int depth_;

	//三角形のサイズ(横に並ぶときの間隔になる)
	float DEFAULT_SCALE_;

	//ローカル座標における
	//最高Y座標位置と　最低Y座標位置を決める
	//最高
	float DEFAULT_MAX_Y_;
	//最低
	float DEFAULT_MIN_Y_;


	//頂点数（FBXファイルより取得）
	int vertexCount_;
	//ポリゴン数（FBXファイルより取得）
	int polygonCount_;
	//マテリアルの個数
		//詳細：（MAYAにおける　マテリアルのLambert１、２などを入れるための数）（ノードに存在するマテリアルの個数）
		//　　：テクスチャも一つのマテリアルである
	int materialCount_;

	//インデックス数
	int indexCountEachMaterial_;

	//各頂点ごとの頂点情報を入れる配列
	VERTEX* pVertices_;

	//インデックス情報を登録しておく
	//動的確保した配列
	int* pIndex_;

	//頂点情報を登録したバッファー
	ID3D11Buffer* pVertexBuffer_;
	//頂点の順番、インデックス情報を持っておくバッファー
	ID3D11Buffer* pIndexBuffer_;
	//コンスタントバッファのバッファーポインタ
		//詳細：構造体で取得しているCONSTANT_BUFFERをシェーダーに渡す際のバッファー
		//　　：バッファーへ各構造体の情報を格納して、シェーダーへわたす
	ID3D11Buffer* pConstantBuffer_;

	//テクスチャ
	Texture* pTexture_;



//protected　メソッド
protected : 

	//頂点バッファ作成(Y座標を除く)
		//詳細：指定ポリゴンサイズとポリゴンを敷き詰める数　Width＊Depthを設定して頂点情報を設定する
		//　　：頂点情報を頂点バッファに登録
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT InitVertex();
	//頂点バッファ作成(Y座標限定)
		//詳細：Y座標をヘイトマップという画像から指定する。詳細は関数先にて　
		//引数：テクスチャの情報構造体のポインタ
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT InitVertexY(D3D11_MAPPED_SUBRESOURCE* hMappedres);
	
	//頂点情報から頂点バッファーを作成
		//レベル：仮想関数
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT CreateVertexBuffer();

	//インデックスバッファ作成
		//詳細：頂点情報からインデックス情報に登録
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT InitIndex();
	//インデックス情報からインデックスバッファーを作成
		//レベル：仮想関数
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT CreateIndexBuffer();
	//コンスタントバッファの作成	
		//レベル：仮想関数
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT CreateConstantBuffer();

	//Planeテクスチャのロード
	//引数：ファイル名
	//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT LoadTexture(const std::string& FILE_NAME = "");


public:

	//コンストラクタ
		//引数：なし
		//戻値：なし
	HeightMapGround();

	//デストラクタ
		//レベル：仮想関数
		//引数：なし
		//戻値：なし
	virtual ~HeightMapGround();


	//初期化
		//詳細：引数情報をもとに、初期化を行わせる
		//引数：ローカルYの最高の高さ
		//引数：ローカルYの最低の高さ
		//引数：頂点の幅
		//引数：シェーダータイプ
		//引数：テクスチャファイル名（ヘイトマップ画像ファイル名）
		//戻値：処理の成功、失敗
	virtual HRESULT Initialize(
		const float MAX_Y , const float MIN_Y,
		const float SCALE, SHADER_TYPE thisShader,
		std::string heightMapFile ,
		std::string textureFile = "");


	//ハイトマップ画像からモデルの作成を行う（引数のファイル名より）
		//詳細：ハイトマップ画像をよみこみ、　ハイトマップ画像が地面モデルの凹凸として使用できる
		//レベル：仮想関数
		//引数：ファイル名
		//引数：シェーダータイプ
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT Load(const std::string& FILE_NAME , SHADER_TYPE thisShader);

	//ポリゴンクラスのポリゴンの描画を行う
		//詳細：ポリゴンクラスに登録されたポリゴン情報を引数Transform位置へ描画する
		//　　：ポリゴンは、ポリゴンの頂点情報とポリゴンを作るインデックス情報により作られている
		//　　：各ポリゴン単位で頂点情報をシェーダーの頂点シェーダーに送ることで、ポリゴン事最終的にピクセルで出力される
		//引数：描画時の変形情報（移動、拡大、回転）（ローカル座標（メソッドにて、親も考慮したワールド座標を取得可能））
		//レベル：仮想関数
		//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT Draw(Transform& transform);


	//解放
		//レベル：仮想関数
		//引数：なし
		//戻値：なし
	virtual void Release();



	//レイキャスト
		//詳細：レイとポリゴンクラスのポリゴンとの衝突判定
		//レベル：仮想関数
		//引数：レイキャスト用の構造体ポインタ
		//戻値：なし
	virtual void RayCast(RayCastData* rayData);


	//引数ベクトルと、ポリゴンクラス（PolygonGroup＊）と衝突した面の法線ベクトルを取得
		//詳細：ポリゴンクラス（PolygonGroup＊）のポリゴンと衝突する線。そのポリゴンの法線を返す
		//　　：レイキャストと同様の方法で、衝突する面を調べて、その面を作るベクトルから法線ベクトルを帰す
		//　　：もしかすると、実際に衝突している地点から延びる法線を出さないと、正確にならない？
		//レベル：仮想関数
		//引数：面との衝突判定を行うベクトルのスタート地点
		//引数：面との衝突判定を行うベクトルの方向
		//戻値：衝突した面の法線ベクトル
	virtual XMVECTOR NormalVectorOfCollidingFace(XMVECTOR& start, XMVECTOR& dir);


	//コンスタントバッファを渡す
		//詳細：オブジェクトクラス内から
		//　　：シェーダーに渡すコンスタントバッファを管理するため
		//レベル：仮想関数
		//引数：なし
		//戻値：コンスタントバッファのポインタ
	virtual ID3D11Buffer* GetConstantBuffer();

};

