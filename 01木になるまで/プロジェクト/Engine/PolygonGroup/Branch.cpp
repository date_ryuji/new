#include "Branch.h"	//ヘッダ

//コンストラクタ
Branch::Branch():
	PolygonGroup::PolygonGroup(),

	pVertices_(nullptr),
	pVertexBuffer_(nullptr),
	pIndex_(nullptr),
	pIndexBuffer_(nullptr),
	vertexCount_(0), indexCountEachMaterial_(0), polygonCount_(0)
{
}

//デストラクタ
Branch::~Branch()
{
}

//ロード
HRESULT Branch::Load(const std::string& FILE_NAME, SHADER_TYPE thisShader)
{
	//FBXをロードする

	//シェーダーの保存
	thisShader_ = thisShader;

	//処理の結果を入れる変数
	HRESULT hr = S_OK;


	//FBXファイルから頂点情報を取得して
		//クラス内メンバに格納する

		/*
			前提（読み込むことのできるFBXファイル）

			・FXBファイル（.fbx）
			・ポリゴンがすべて結合されている
			・ポリゴン三角化
			・トランスフォームのフリーズ
			・種類ごとにすべてを削除→ヒストリ

			出力時
			・頂点単位の法線の分割
			・法線、接戦
		*/


		/*FBXファイル管理マネージャーの作成*************************************************************************/
		//FBXファイル管理のマネージャー
			//FBXのトップをCreateで呼ぶ
		//マネージャを生成
	FbxManager* pFbxManager = FbxManager::Create();


	/*FBXファイルローダーの作成********************************************************************************/
	//FBXをロードをする機能を持っているクラスの作成
		//ロードする人
		//インポーター（輸入、ロードして持ってくる）する人を生成
	FbxImporter* fbxImporter = FbxImporter::Create(pFbxManager, "imp");

	//ファイル名でインポーターがインポート（輸入、ロード）している
	fbxImporter->Initialize(FILE_NAME.c_str(), -1, pFbxManager->GetIOSettings());

	//ゲーム内のシーンに読み込む
		//１つのシーンに入れるそれぞれのシーンに、取り込む

	//シーンオブジェクトにFBXファイルの情報を流し込む
	FbxScene* pFbxScene = FbxScene::Create(pFbxManager, "fbxscene");
	//FBXのシーン、をインポートさせて、ゲームのシーン内にてインポート、使えるようにする？
	fbxImporter->Import(pFbxScene);
	//解放
	fbxImporter->Destroy();


	/*FBXのメッシュ情報取得************************************************************************************/
	//メッシュ情報を取得

	//メッシュの一番上のノードを取得
		//FBXのパーツのつながりは、各ノードとしてつながっている
		//メッシュ情報を格納した　Aパーツ（メッシュ情報が格納されている一番上）の下に、Bパーツ（Aパーツの子供）があって、、、
			//だが、今回は、Aパーツ（一番上）のノードのみを取得し、ポリゴンとして表示するようにする。

		//メッシュを結合している場合
		//一番根っこのノードから０番目のノードに一番親のノードが格納されている

		//メッシュを結合していることで
		//FBXのメッシュすべてがそのノードに格納されているので、
		//そのノードからすべての頂点情報などを取得可能


	//一番根っこのノードを取得
	FbxNode* rootNode = pFbxScene->GetRootNode();

	//ルートノードの一番最初のノードを持ってくる
		//メッシュをすべて結合しているならば、
		//すべてのメッシュ情報が入っている	
			//→各頂点、法線
			//→すべての情報を合わせたものをメッシュという
	FbxNode* pNode = rootNode->GetChild(0);

	//取得したノードの中のメッシュ情報取得
		//頂点の情報、法線の情報（ノードからメッシュを取得）
	FbxMesh* mesh = pNode->GetMesh();


	/*メッシュとノードから頂点情報（頂点数など）を取得*********************************************************/
	//メッシュから
	//頂点の個数を取得
		//各情報の個数を取得
		//頂点の情報がわからないとインデックス情報は作れない
	vertexCount_ = mesh->GetControlPointsCount();

	//メッシュから
	//ポリゴンの個数を取得
	polygonCount_ = mesh->GetPolygonCount();




	/*ファイルのディレクトリ取得*********************************************************************************************/
	//FBXファイルを扱うにあたって、引数ファイル名は長くて不便
		//そのため、現在参照中のパス。現在いるカレントディレクトリのパスを変更する
		//そのために、引数ファイル名から、FBXファイル前までのディレクトリをカレントディレクトリにしてしまえば、ファイルへのアクセスが楽になると考える


	//ディレクトリ名を入れる配列
		//引数ファイル名からディレクトリ名のみ格納する配列
		//Assets/Model/Fbx/AA.fbx
		//上記が引数ファイル名として渡された場合、
		//上記の配列には、　「Assets/Model/Fbx/」まで代入される

		//詳細：MAX_DIRには、Windows側が、決めたディレクトリ名の最大文字数はこのくらいだろうという標準のサイズが入っている
	char dir[_MAX_DIR];


	//ファイル名の引数からディレクトリ名だけ取得する
		//詳細：stringの文字列はポインタのアドレスなので、そのまま使うことができない、const char* に変更→ str.c_str()
		//　　：dirにて格納先のポインタが入る（配列の[]なしは、配列の先頭アドレスとなる）
		//　　：dirへファイルのディレクトリ部分が入る
	_splitpath_s(FILE_NAME.c_str(), nullptr, 0, dir, _MAX_DIR, nullptr, 0, nullptr, 0);


	/*カレントディレクトリの変更*******************************************************************************************/
		//FBXファイルロード終了後
		//きちんと元のカレントディレクトリに戻す前提


	//カレントディレクトリ＝現在見ているディレクトリ（標準でパスを見始めるディレクトリ）
	//カレントディレクトリを上記のディレクトリ名にする
		//カレントディレクトリの変更前に
		//現在のカレントディレクトリを覚えておく（もとに戻すときのために）

	//デフォルトのカレントディレクトリ名を入れる配列
		//詳細：MAX_PATHという　Window側の標準で決められた　ファイルパスにおける最大文字数
	char defaultCurrentDir[MAX_PATH];
	//現在のカレントディレクトリ取得（Project/）
	//現在のカレントディレクトリになっているパスを保存	
		//引数：取得文字数
		//引数：格納先アドレス
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);


	//カレントディレクトリの変更
		//★SetCurrentDirectoryをすることで、ファイルをロードするときに　そのカレントディレクトリからの相対パスを指定する必要が出てくる
		//Porject/Assets/ をカレントディレクトリとしたら、　Project/Assets/以下からの相対パスによってファイルパスを指定する必要が出てくる
	SetCurrentDirectory(dir);

	/*
		以下のバッファの作成で、
		失敗してしまうかもしれない
		→失敗したらSetCurrentDirectoryが変わった状態で、returnを返してしまう

		なので、SetCurrentなど、マネージャーの解放などはreturn前に必ず行わなければいけない
		→失敗したら、その時々にSetCurrentDirectoryの処理を書くでもいいし

		TryCatchでも
		※それらのエラー処理の書き方は、他の人のソースを見てみる
	*/


	/*頂点情報から　各バッファの作成****************************************************************************************/

	//頂点バッファの作成を行う（引数のメッシュより）
	hr = InitVertex(mesh);
	//バッファ準備失敗
	if (FAILED(hr))
	{
		//カレントディレクトリ戻す
			//終わったら戻す
		SetCurrentDirectory(defaultCurrentDir);
		//マネージャ解放
			//マネージャーを解放することで、上記の諸々（ノードなど）が解放されるようになっているので、マネージャーだけ解放
		pFbxManager->Destroy();
	};
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");


	//インデックスバッファの作成（引数メッシュより）
	hr = InitIndex(mesh);
	//バッファ準備失敗
	if (FAILED(hr))
	{
		SetCurrentDirectory(defaultCurrentDir);
		pFbxManager->Destroy();
	};
	//エラーメッセージ
	ERROR_CHECK(hr, "インデックスバッファの作成失敗", "エラー");


	/*処理の終了　カレントディレクトリを元に戻す*****************************************************************************/

	//カレントディレクトリを戻す
		//単純に、変更を加える前のパスを保存しておいたため、そのパスをセットして戻す
		//終わったら戻す
	SetCurrentDirectory(defaultCurrentDir);


	/*解放******************************************************************************************************************/

	//マネージャ解放
		//マネージャーを解放することで、上記の諸々（メッシュ、ノード）が解放されるようになっているので、マネージャーだけ解放
	pFbxManager->Destroy();


	//処理の成功
	return S_OK;

}

//解放
void Branch::Release()
{
	//ポインタ宣言順と逆に解放
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_RELEASE(pVertexBuffer_);
	SAFE_DELETE_ARRAY(pVertices_);
}
//頂点数取得
int Branch::GetVertexCount()
{
	return vertexCount_;
}

//ポリゴン数取得
int Branch::GetPolygonCount()
{
	return polygonCount_;
}

//インデックス情報取得
int Branch::GetIndexCount()
{
	return indexCountEachMaterial_;
}

//頂点情報取得
Branch::VERTEX * Branch::GetVertexes()
{
	return pVertices_;
}

//頂点インデックス取得
int * Branch::GetIndexes()
{
	return pIndex_;
}



//頂点バッファの作成
	//引数においてメッシュを受け取り、FBXファイルから頂点情報を取得して、
	//頂点バッファを作成する
HRESULT Branch::InitVertex(fbxsdk::FbxMesh * mesh)
{
	//頂点情報を入れる配列のポインタ
	//頂点数分確保
	pVertices_ = new VERTEX[vertexCount_];


	//FBXより頂点情報の取得
		//ポリゴン単位で頂点情報が格納されているため
		//ポリゴン単位で頂点情報を取得 
	//条件：ポリゴンの枚数分
	for (DWORD poly = 0; poly < (DWORD)(polygonCount_); poly++)
	{
		//3頂点ごとに情報取得
			//3頂点分（ポリゴンは三角形の3頂点で作っているため）
		//条件：3頂点分
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//全体における頂点番号取得（全体から見た頂点番号）
				//詳細：３番目のポリゴンの、何番目の頂点→とすれば、頂点の順番（頂点情報が存在するFBX情報から、全体の上から何番目の頂点だ）の位置が出てくる
				//引数：ポリゴン番目（モデルを作るポリゴン（0オリジン）の何番目）
				//引数：頂点番目（ポリゴンを作る頂点（0オリジン）の何番目）
			int index = mesh->GetPolygonVertex(poly, vertex);

			//頂点の位置（ローカル座標）
				//頂点番号から頂点の座標取得
				//頂点が入っているFBXの情報から、index番目のベクトルを取得
			FbxVector4 pos = mesh->GetControlPointAt(index);

			//頂点情報における座標を登録
				//詳細：ローカル座標をメンバの頂点情報格納先に登録 
				//　　：　　：pVertices_[index].position = XMVectorSet((float)-pos[0], (float)pos[1], (float)pos[2], 0.0f);
				//　　：　　：FBXは左右反転になる前提なので、マイナスにして、左右反転の左右反転になる
				//　　：　　：だが、法線情報は、これにすることで、→法線情報を反転させなくてはいけなくなるので、
				//　　：　　：★左右反転が前提なのであれば、→ここでーにしないのも手

				//詳細：格納順番、ポリゴンの表裏の関係で格納にひと手間かける必要がある↑
				//　　：しかし、接戦の情報を格納することを考えると、頂点情報が逆になる（マイナス方向で格納）することは避けたい
			pVertices_[index].position = XMVectorSet((float)pos[0], (float)pos[1], (float)pos[2], 0.0f);


			//※頂点情報の格納方法にのっとり、UVの格納もポリゴンの裏表を考慮した格納方法を行う

			//頂点のUVを登録
			//メッシュからUV格納先を取得
			FbxLayerElementUV* pUV = mesh->GetLayer(0)->GetUVs();
			//UVの番号を取得（全体から見たUV座標の番号）（頂点番号と同じ）
				//引数：ポリゴン番目
				//引数：頂点番目
			int uvIndex = mesh->GetTextureUVIndex(poly, vertex, FbxLayerElement::eTextureDiffuse);
			//UV値を取得（ベクトル）
			FbxVector2  uv = pUV->GetDirectArray().GetAt(uvIndex);
			//頂点情報におけるUVを登録
			pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0f - uv.mData[1]), 0.0f, 0.0f);


			//頂点の法線を登録
			FbxVector4 Normal;
			//法線ベクトルの取得
				//引数：ポリゴン番目
				//引数：頂点番目
				//引数：法線ベクトル（参照渡し）
			mesh->GetPolygonVertexNormal(poly, vertex, Normal);
			//頂点情報における法線を登録
			pVertices_[index].normal = XMVectorSet((float)Normal[0], (float)Normal[1], (float)Normal[2], 0.0f);
		}
	}


	// 頂点バッファ作成
	HRESULT hr = CreateVertexBuffer();
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファ作成失敗", "エラー");


	//処理の成功
	return S_OK;
}

//インデックスバッファ準備
HRESULT Branch::InitIndex(fbxsdk::FbxMesh * mesh)
{
	//インデックス数の初期化
	indexCountEachMaterial_ = 0;

	//ポリゴン数＊３　＝　インデックス数
	pIndex_ = new int[polygonCount_ * 3];

	//カウンター
	int count = 0;

	//条件：ポリゴンごと
		//マテリアルをマテリアルごとに、分けてインデックス情報を分ける
	for (DWORD poly = 0; poly < (DWORD)(polygonCount_); poly++)
	{
		//条件：3頂点分
			//★　頂点を反転しなかったので、　そのままのインデックスで登録
			//上記で反転していたのは、頂点を反時計回りに取得していたため、反転させていた
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//一次元目：マテリアル
			//二次元目：インデックス情報の添え字
				//引数：ポリゴン番目
				//引数：頂点番目
			pIndex_[count] = mesh->GetPolygonVertex(poly, vertex);
			count++;	//配列のサイズを取得するために、要素数を取得するためのカウント
						//また、動的確保したインデックス情報を登録する配列の添え字としても使用する。

		}

	}


	//合計インデックス情報数の登録
	indexCountEachMaterial_ = count;


	// インデックスバッファを生成する
	HRESULT hr = CreateIndexBuffer();
	//エラーメッセージ
	ERROR_CHECK(hr, "インデックスバッファ作成失敗", "エラー");


	//処理の成功
	return S_OK;

}

//頂点バッファーの作成
HRESULT Branch::CreateVertexBuffer()
{
	//頂点バッファ作成

	/*
		頂点情報の配列のサイズ

		//verticesはポインタで動的配列になったので、sizeof(vertices)では正しくサイズを取得することができない。
			//	この場合はVERTEX型のサイズ*頂点数を指定すればよい。
				//頂点の数はわかっているので、→vertexの型でいくらのサイズをbyteで使うのか、＊頂点数→最終的に使用する全体サイズ

	*/

	//バッファの詳細情報を格納する構造体
		//頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;

	//頂点バッファの使用するバイト長を登録
		//頂点情報のサイズの登録
		//型名＊頂点数（頂点数だけ、VERTEXを取得している）（VERTEXの情報が何個必要なのか）
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;


	//バッファへのアクセスポインタ
		//頂点情報のリソースを入れる構造体
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = pVertices_;


	//頂点バッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//インデックスバッファの作成
HRESULT Branch::CreateIndexBuffer()
{

	//バッファの詳細情報を格納する構造体
	//インデックス情報の設定
	D3D11_BUFFER_DESC   bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(int) * indexCountEachMaterial_;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;


	//バッファーへのアクセスポインタ
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex_;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	//インデックスバッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "インデックスバッファ作成失敗", "エラー");

	//処理の成功
	return S_OK;

}

//頂点情報のコンストラクタ
Branch::VERTEX::VERTEX() :
	position(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	uv(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	normal(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}
