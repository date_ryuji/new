#include "Sprite.h"							//ヘッダ
#include "../../CommonData/GlobalMacro.h"	//共通マクロ

/*
	疑似的に2Dのように見せて描画するため

	Camera::　におけるビュー行列や、プロジェクション行列など
	カメラからの見え方に重きを置く必要がない
*/

/*
	2Dにおける座標

	//詳細：2D上での座標管理は、ワールド座標やローカル座標ではない
		//　　：一番わかりやすい表現としては、プロジェクション座標管理の座標管理である。
		//　　：プロジェクション座標（カメラのしすいだいを、ギュッとH２＊W２＊D１　のサイズに縮小した範囲）



*/


//コンストラクタ
Sprite::Sprite():
	thisShader_(SHADER_TYPE::SHADER_2D),		//デフォルトシェーダーのタイプをセット
	pTexture_(nullptr),
	pPriorityTexture_(nullptr),
	pConstantBuffer_(nullptr),
	index(nullptr),

	pVertexBuffer_(nullptr) , 
	pIndexBuffer_(nullptr) , 

	vertex_(6) , 

	alpha_(1.0f)
{
}


//デストラクタ
Sprite::~Sprite()
{
}


//頂点情報の初期化（頂点バッファー作成まで）
HRESULT Sprite::InitVertex()
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;


	//頂点情報
		//詳細：一頂点の頂点情報を手打ちで入力 
		//詳細：座標は、ローカル座標であるため、画像を表示する四角形を形作る
	VERTEX vertices[] =
	{

		{XMVectorSet(-1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)},	// 四角形の頂点（左上）

		{XMVectorSet(1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f)},		// 四角形の頂点（右上）

		{XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f)},		// 四角形の頂点（右下）

		{XMVectorSet(-1.0f, -1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)},	// 四角形の頂点（左下）		

	};


	//バッファの詳細情報を格納する構造体
		//頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;
	bd_vertex.ByteWidth = sizeof(vertices);
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;

	//バッファへのアクセスポインタ
		//頂点情報のリソースを入れる構造体
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = vertices;

	//頂点バッファの作成
	hr = Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファ作成失敗", "エラー");
	




	//インデックス情報の確保
		//メンバのインデックス情報を宣言
		//→インデックス情報では、各モデルごとに頂点は違うため、オーバーライドの関数内でそれぞれ作成してもらう
	//時計回りに、頂点番号を登録
	index = new int[vertex_] { 0, 2, 3, 0, 1, 2};	

	//処理の成功
	return S_OK;
}


//初期化
HRESULT Sprite::Initialize(const std::string& FILE_NAME , SHADER_TYPE thisShader)
{

	//シェーダーの登録
	thisShader_ = thisShader;


	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	{
		//頂点情報の初期化
		hr = InitVertex();
		//エラーメッセージ
		ERROR_CHECK(hr, "モデルの頂点情報の初期化失敗", "エラー");
	}

	{
		
		//バッファの詳細情報を格納する構造体
		//インデックス情報の設定
		D3D11_BUFFER_DESC   bd;
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(int) * (vertex_);	//サイズを、indexのサイズ分だけ、（インデックス情報の入っているサイズだけ）
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;


		//バッファーへのアクセスポインタ
		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = (void*)index;	//作成したインデックス情報を渡す
											//インデックスのint型のポインタを、メモリーに渡す？
											//送るときに、（void*）出ないと送れないとエラーを返されるため、(void*)でキャスト
		InitData.SysMemPitch = 0;
		InitData.SysMemSlicePitch = 0;


		//インデックスバッファの作成
		hr = Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
		//エラーメッセージ
		ERROR_CHECK(hr, "インデックスバッファ作成失敗", "エラー");

	}

	{
		//バッファの詳細情報を格納する構造体
			//コンスタントバッファ情報の設定
		D3D11_BUFFER_DESC cb;
		cb.ByteWidth = sizeof(CONSTANT_BUFFER);
		cb.Usage = D3D11_USAGE_DYNAMIC;
		cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		cb.MiscFlags = 0;
		cb.StructureByteStride = 0;

		//コンスタントバッファの作成
		hr = Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_);
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファ作成失敗", "エラー");
	}

	{
		//テクスチャのロード
		hr = TextureLoad(FILE_NAME);
		//エラーメッセージ
		ERROR_CHECK(hr, "テクスチャのロード失敗", "エラー");
	}

	//処理の成功
	return S_OK;	
}

//テクスチャのロード
HRESULT Sprite::TextureLoad(const std::string& FILE_NAME) 
{
	pTexture_ = new Texture;
	//ファイルのロード
		//テクスチャのロード
	HRESULT hr = pTexture_->Load(FILE_NAME);
	//エラー時の処理
	if (FAILED(hr))
	{
		pTexture_->Release();
		SAFE_DELETE(pTexture_);
	}
	//エラーメッセージ
	ERROR_CHECK(hr, "ファイルの読み込みを失敗", "エラー");
	
	//処理の成功
	return S_OK;
}

//テクスチャのロード
HRESULT Sprite::TextureLoad(ID3D11Texture2D* pTexture) {
	
	pTexture_ = new Texture;
	//ファイルのロード
		//テクスチャのロード
	HRESULT hr = pTexture_->Load(pTexture);
	//エラー時の処理
	if (FAILED(hr))
	{
		pTexture_->Release();
		SAFE_DELETE(pTexture_);
	}
	//エラーメッセージ
	ERROR_CHECK(hr, "ファイルの読み込みを失敗", "エラー");

	//処理の成功
	return S_OK;
}

//初期化
	//テクスチャのバッファからSpriteの初期化
HRESULT Sprite::Initialize(ID3D11Texture2D* pTexture, SHADER_TYPE thisShader)
{
	//シェーダー保存
	thisShader_ = thisShader;


	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	{
		//頂点情報の初期化
		hr = InitVertex();
		//エラーメッセージ
		ERROR_CHECK(hr, "モデルの頂点情報の初期化失敗", "エラー");
	}
	{

		//バッファの詳細情報を格納する構造体
		//インデックス情報の設定
		D3D11_BUFFER_DESC   bd;
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(int) * (vertex_);	//サイズを、indexのサイズ分だけ、（インデックス情報の入っているサイズだけ）
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;


		//バッファーへのアクセスポインタ
		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = (void*)index;	//作成したインデックス情報を渡す
											//インデックスのint型のポインタを、メモリーに渡す？
											//送るときに、（void*）出ないと送れないとエラーを返されるため、(void*)でキャスト
		InitData.SysMemPitch = 0;
		InitData.SysMemSlicePitch = 0;


		//インデックスバッファの作成
		hr = Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
		//エラーメッセージ
		ERROR_CHECK(hr, "インデックスバッファ作成失敗", "エラー");

	}

	{
		//バッファの詳細情報を格納する構造体
			//コンスタントバッファ情報の設定
		D3D11_BUFFER_DESC cb;
		cb.ByteWidth = sizeof(CONSTANT_BUFFER);
		cb.Usage = D3D11_USAGE_DYNAMIC;
		cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		cb.MiscFlags = 0;
		cb.StructureByteStride = 0;

		//コンスタントバッファの作成
		hr = Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_);
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファ作成失敗", "エラー");
	}

	{
		//テクスチャのロード
			//テクスチャバッファからテクスチャのロード
		hr = TextureLoad(pTexture);
		//エラーメッセージ
		ERROR_CHECK(hr, "テクスチャのロード失敗", "エラー");
	}

	//処理の成功
	return S_OK;
}


//描画
HRESULT Sprite::Draw(Transform transform)
{
	//Transformの計算
	transform.Calclation();

	//シェーダーを選択
		//自身のシェーダーをセット
		//2Dの場合、３D用のシェーダーは使用できないため、シェーダーを切り替える際には注意
	Direct3D::SetShaderBundle(thisShader_);


	//シェーダーにおけるコンスタントバッファへわたす情報をまとめる
		//詳細：自クラスの　CONSTANT_BUFFER(sturct)とシェーダーファイルにおけるコンスタントバッファの構造体　の格納数、格納情報を等しくしているため、
		//　　：伴い、描画に必要な現在の情報をCONSTANT_BUFFER(sturct)　に格納して、最終的にシェーダーファイルに渡す

	//コンスタントバッファ　格納情報
	CONSTANT_BUFFER cb;

	//コンスタントバッファ　へのデータを格納（準備）
	{

		//テクスチャを選択し　そのテクスチャに応じた、コンスタントバッファの情報を取得する
		if (pPriorityTexture_ != nullptr)
		{
			//テクスチャを画面に表示するため
			//テクスチャサイズを画面のサイズ比に合わせて拡大、縮小
			GetConstantBufferInfoEachForTexture(pPriorityTexture_, &cb, transform);
		}
		else
		{
			GetConstantBufferInfoEachForTexture(pTexture_, &cb, transform);
		}

		//α値セット
		cb.alpha = alpha_;

	}


	//コンスタントバッファをシェーダーファイルへコピー、渡す
	{
		//バッファへのマップ設定
			//ポインタへのアクセスなどに使用する
		D3D11_MAPPED_SUBRESOURCE pdata;
		//コンスタントバッファへのマップ
			//詳細：アクセス許可
			//　　：シェーダーなどのGPUでしか、触れることのできないものに
			//　　：CPUから触れる準備
			//　　：マップを行い、バッファへのアクセスができるようになるが、その詳細のアクセスを上記pdataにて行う。
		HRESULT hr = Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファのマップの書き込み失敗", "エラー");

		//memcpy_s (コピー先、コピー先のサイズ、コピー元　、コピー元のサイズ)
			//コピー元：コンスタントバッファのバッファ型をポインタにて渡すのだが、
			//　　　　：特定の型のポインタを渡すことはできないため。(void*)というNULLの型でキャストする。
		memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	

	}

	//テクスチャのサンプラーの取得
		//テクスチャごと
	if (pPriorityTexture_ != nullptr)
	{
		//テクスチャごとのシェーダーへのサンプラーなどへ情報を与える
		SetSamplerAndShaderResourceViewEachForTexture(pPriorityTexture_);
	}
	else
	{
		SetSamplerAndShaderResourceViewEachForTexture(pTexture_);
	}



	//頂点情報のセット
	{
		//頂点バッファをセット（頂点を持っておくための領域（確保場所））
			//詳細：複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		//1頂点のサイズを指定する
			//詳細：位置のみであれば、XMVECTORの構造体であったが
			//　　：位置やUVも確保するため、頂点情報の構造体のサイズを受け取らせる
		UINT stride = sizeof(VERTEX);
		UINT offset = 0;
		//頂点バッファのセット
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

		//サイズの更新
			//詳細：インデックスバッファに登録している情報は、int情報の配列
			//　　：そのため、intのサイズを渡す
		stride = sizeof(int);
		offset = 0;
		//インデックスバッファのセット
		Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

		
	}

	//コンスタントバッファをセット
	{
		//コンスタントバッファをセット
		//頂点シェーダー用	
		Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);
		//ピクセルシェーダー用
		Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);

		//コンスタントバッファへのアンマップ
			//詳細：アクセス拒否
		Direct3D::pContext->Unmap(pConstantBuffer_, 0);

	}


	//描画
		//詳細　　：インデックス数を渡して、頂点によるポリゴンの描画を宣言 
		//第一引数：描画インデックス数　描画の対象となるポリゴンを作るインデックスの数
	Direct3D::pContext->DrawIndexed(vertex_, 0, 0);






	//描画終了後
		//優先テクスチャ（外部から渡されたテクスチャのポインタ）を解放（nullptrで扱わなくする(外部から渡されただけのポインタなので、解放はしない)）
		//次のフレームまで、持ち越さない
	pPriorityTexture_ = nullptr;



	//処理の成功
	return S_OK;

}

//解放
void Sprite::Release()
{
	//ヘッダにて、確保したポインタを、逆順にリリース

	pTexture_->Release();
	SAFE_DELETE(pTexture_);
	SAFE_DELETE(index);	//インデックス情報を使用する、関数内にて、宣言し、使い終わった後に開放してしまうと、エラーになってしまったので、すべての作業が押さった後に、呼び出すようにめんばへんすうにした
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);


	//ID3D11から始まる、型→Releaseが用意されているので、→それで解放
	//頂点バッファなどを、確保するために、使った、型を開放
}

//シェーダーを切り替える
void Sprite::ChangeShader(SHADER_TYPE shaderType)
{
	//２D専用のシェーダーであることを確認
	bool exists = false;


	//引数シェーダーが
	//２D専用のシェーダーであるかの判断を行う
	//条件：引数が２D用シェーダーでない
	if (!ShaderType::Is2DShader(shaderType))
	{
		//エラーメッセージ
		MessageBox(nullptr, "2DSpriteに適さないシェーダーに切り替えようとしました。", "エラー", MB_OK);
	};

	//シェーダー切替え
	thisShader_ = shaderType;
}

//自身メンバのA値をセットする
void Sprite::SetAlpha(float alpha)
{
	alpha_ = alpha;
}

//画像と衝突判定
void Sprite::HitCollision(ImageCollision * imageCollision)
{
	/*
		衝突判定の前提

		・ウィンドウサイズをユーザーが任意に変えていない
		・画像が回転していない
		・画像の奥行き（vecZ）が0である
	
		上記の場合、正しい衝突判定結果が出るとする
	*/

	/*
		画像との衝突判定にて起こる　誤差


		Transformにて　使っている座標変換は
		Direct3D::srcWidth　などのスクリーン座標から座標変換、世界のサイズ、スクリーンのサイズを決めている

		そのため、
		下記にて行う衝突判定においても、
		スクリーンのサイズを
		Direct3D::srcWidth　にて取得すれば、　大きなずれは生じないはず。
		（同じ基準、規定の元　計算を行っている）


		しかし
		誤差が生じてしまう。


		その理由は、おそらく
		ウィンドウにおける　ウィンドウバー、タイトルバー、メニューバーなどの余分なウィンドウ分が含まれて誤差を生じているのだと考える。

	
	*/


	/*
		�@自身の描画位置Transform（プロジェクション座標）をスクリーン座標へ

	
	*/


	//スクリーンサイズ取得
	int scrWidth = Direct3D::scrWidth;
	int scrHeight = Direct3D::scrHeight;


	//画像のTransform値から　画像の原点となるピクセルを求める
	XMVECTOR imagePix = GetPixelToWorldConversion(
		imageCollision->pTrans->position_.vecX,
		imageCollision->pTrans->position_.vecY);

	//画像のテクスチャサイズ取得
	float width = (float)pTexture_->GetWidth();
	float height = (float)pTexture_->GetHeight();


	//四角形を作る頂点数
	static constexpr int VERTEX_NUM = 4;

	//原点の位置から見た
		//四角形を作る頂点の4頂点のピクセル位置を求める
		//詳細：原点ピクセル位置を求めることができている。かつ、画像のピクセルサイズが分かっている。　であれば、画像の上下左右の頂点のピクセル座標も求めることが可能である。
	XMVECTOR scrVertex[VERTEX_NUM] =
	{
		//左上
		XMVectorSet(imagePix.vecX - (width / 2.0f) , imagePix.vecY - (height / 2.0f) , 0.f , 0.f),
		//右上
		XMVectorSet(imagePix.vecX + (width / 2.0f) , imagePix.vecY - (height / 2.0f) , 0.f , 0.f),
		//右下
		XMVectorSet(imagePix.vecX + (width / 2.0f) , imagePix.vecY + (height / 2.0f) , 0.f , 0.f),
		//左下
		XMVectorSet(imagePix.vecX - (width / 2.0f) , imagePix.vecY + (height / 2.0f) , 0.f , 0.f),
	};


	//画像の原点ピクセル位置が（０，０）と考えるとき
		//画像の原点ピクセルの
		//原点（０，０）までの移動量
		//原点に移動し、移動してから、　拡大、回転を行う。
		//そのうえで、　移動値分戻し、　元の座標に戻す。
	float moveValueX = imagePix.vecX;
	float moveValueY = imagePix.vecY;


	/*
		頂点のピクセル位置を

		
		移動行列、回転行列、拡大行列を考慮した位置を求める
		描画における各頂点のピクセル位置（スクリーン座標）を求める

	*/

	//条件：四角形を作る頂点数
	for (int i = 0; i < VERTEX_NUM; i++)
	{
		
		//�@
		//頂点を移動
			//詳細：頂点の原点を（０，０）としたときの各ピクセル位置を求める
			//　　：下記の計算により
			//　　：原点（０，０）から見た各頂点のピクセル座標となる
		//移動の完了
		scrVertex[i] = scrVertex[i] - XMVectorSet(moveValueX, moveValueY, 0.f, 0.f);
		

		//�A
			//詳細：�@（原点（0，0）を原点とした座標に）
			//　　：拡大行列　回転行列を掛けて　拡大、回転を考慮した各頂点のピクセル座標を求める
		scrVertex[i] = XMVector3TransformCoord(
			scrVertex[i],
			imageCollision->pTrans->matScale_ * imageCollision->pTrans->matRotate_);
		
		/*
			頂点の範囲外

			//プロジェクション座標を
			//ビューポート行列を使って、計算するとき、
			//プロジェクション座標の限界値　−１〜１の範囲を超えているときに、ピクセルに直したりすると、
			//スクリーン座標を求めることができない。
			
			//だが、　今回は、ビューポート行列などは使わないため、
			//範囲外のピクセル位置を示されても、衝突判定の計算を行う上では問題ないと考える

		*/
		

		//�B
		//移動した分戻す
			//詳細：�@の移動分戻す
		scrVertex[i] = scrVertex[i] + XMVectorSet(moveValueX, moveValueY, 0.f, 0.f);


	}



	/*
		//各頂点を使い、
		//三角形2つとして、頂点と三角形の内外判定を行う
	*/


	//四角形を　
		//�@SV0 , SV1 , SV3 で一つの三角形
		//�ASV1 , SV2 , SV3 で一つの三角形
	//2つの三角形に分ける
	
	//三角形それぞれをマウスカーソル位置との衝突判定を行わせる


	//�@衝突判定
	bool collision = PointInTriangle(
		imageCollision->scrPointPos,
		scrVertex[0],
		scrVertex[1],
		scrVertex[3]);
	if (collision)
	{
		imageCollision->hit = true;
		return;
	}

	//�A衝突判定
	collision = PointInTriangle(
		imageCollision->scrPointPos,
		scrVertex[1],
		scrVertex[2],
		scrVertex[3]
	);
	if (collision)
	{
		imageCollision->hit = true;
		return;
	}

}

//切り詰める
float Sprite::WithinRange(int value, int max, int min)
{
	//条件：範囲より大きい
	if (value > max)
	{
		//MAX値にて切り詰める
		return (float)max;
	}
	//条件：範囲より小さい
	else if (value < min)
	{
		//MIN値にて切り詰める
		return (float)min;
	}

	//切り詰めずにそのままの値を返す
	return (float)value;
}

//外積を求める
float Sprite::Sign(XMVECTOR point1, XMVECTOR point2, XMVECTOR point3)
{
	return (point1.vecX - point3.vecX) *
		(point2.vecY - point3.vecY) -
		(point2.vecX - point3.vecX) *
		(point1.vecY - point3.vecY);
}

//三角形と点の内外判定
bool Sprite::PointInTriangle(XMVECTOR point, XMVECTOR v1, XMVECTOR v2, XMVECTOR v3)
{
	//参考サイト
		//https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
		//http://www.thothchildren.com/chapter/5b267a436298160664e80763

	//外積
		//https://mathtrain.jp/gaiseki#:~:text=%EF%BC%88%E5%86%85%E7%A9%8D%E3%83%BC1%EF%BC%89%EF%BC%9A%E3%82%B9%E3%82%AB%E3%83%A9%E3%83%BC,%E5%9E%82%E7%9B%B4%E3%81%AA%E3%83%99%E3%82%AF%E3%83%88%E3%83%AB%E3%81%AE%E3%81%93%E3%81%A8%E3%80%82


	//外積を用いて、
	//三角形の内外を求める

	//外積を行ったときに正負が三点で一致すると、三角形は内側にあると判断される
	bool b1, b2, b3;

	b1 = Sign(point, v1, v2) < 0.0f;
	b2 = Sign(point, v2, v3) < 0.0f;
	b3 = Sign(point, v3, v1) < 0.0f;

	return ((b1 == b2) && (b2 == b3));
}

//自身のテクスチャの画像サイズを取得
XMVECTOR Sprite::GetMyTextureSize()
{
	return XMVectorSet((float)pTexture_->GetWidth(), (float)pTexture_->GetHeight(), 0.f, 0.f);
}

//Spriteクラスが次のDrawにて、優先するテクスチャを自身のメンバにセットする
void Sprite::SetPriorityTexture(Texture * pPriorityTexture)
{
	pPriorityTexture_ = pPriorityTexture;
}


//引数ピクセルをプロジェクション座標値に変更する
XMVECTOR Sprite::GetPixelToWorldConversion(float x, float y)
{
	//ピクセルをワールド（プロジェクション）へ

	//ワールド　＝　２Dにおいてはプロジェクション座標として扱われている
	//ここにおいては、プロジェクション座標として考える

	//スクリーンサイズ
	float scrWidth = (float)Direct3D::scrWidth;
	float scrHeight = (float)Direct3D::scrHeight;


	/*
		プロジェクションの座標値

		x y どちらにおいても、
		-1.0f ~ 1.0fという範囲になる
	

	　　
	*/

	//最高値
	//プロジェクションにおける　限界値　−１〜１を　０〜２に切り詰めたら
	//最高値は２になる

	//なので、
	//引数の値は　-1~1になるので、　それを　０〜２の範囲にして、
		//最高値と割合をとる

		//その割合を、　スクリーン座標と計算する
	float maxX = 2.0f;
	float maxY = 2.0f;

	float percentageX = (x + 1.0f) / maxX;
	float percentageY = ((y*-1.0f) + 1.0f) / maxY;

	float worldX = scrWidth * percentageX;
	float worldY = scrHeight * percentageY;





	//上記において、
	//−１〜１におけるピクセル（スクリーン）座標を取得可能
	return XMVectorSet((float)worldX, (float)worldY, 0.f, 0.f);


}


//テクスチャを画面に表示するため
//テクスチャサイズを画面のサイズ比に合わせて拡大、縮小
void Sprite::GetConstantBufferInfoEachForTexture(Texture * pTexture, CONSTANT_BUFFER * cb , Transform& trans)
{
	//拡大の行列として、テクスチャのサイズにする、サイズに拡大行列
	//ピクセル１のサイズが求められたので、テクスチャそのままのサイズを取得→後は、これを１ピクセルのサイズで拡大させればよい（ウィンドウにおける１ピクセルをテクスチャのサイズ分拡大（ウィンドウにおけるテクスチャのサイズを出せる））

	//テクスチャサイズ取得
	float texWidth = (float)pTexture->GetWidth();	
	float texHeight = (float)pTexture->GetHeight();


	//テクスチャのもともとのサイズに拡大、縮小するための拡大行列
		//引数：横幅の拡大率
		//引数：縦幅の拡大率
		//引数：ｚ幅１．０ｆ
	XMMATRIX adjustScale = XMMatrixScaling(
		texWidth,
		texHeight,
		1.0f);	


	//ウィンドウにおける１＊１のサイズに縮小、拡大させる縮小行列
		//デフォルトでウィンドウサイズに表示されるのを
		//→ウィンドウサイズの１ピクセルの大きさに縮小する行列にする
			//1.0f / ウィンドウ幅
			//１ピクセル分の大きさに一度縮小して、そのピクセルをテクスチャサイズ、に拡大させればよい
			//１＊１のサイズを求めることができた→このサイズ分、テクスチャを拡大すれば→ウィンドウサイズ
	XMMATRIX miniScale = XMMatrixScaling(
		1.0f / Direct3D::scrWidth,
		1.0f / Direct3D::scrHeight,
		1);


	//ワールド行列のみ送る
	//位置などが、存在する、ワールド行列を送る
	//Transformで、world行列を取得することができるが、
		//→そのままワールド行列を掛けてしまうと、回転の時に、ウィンドウサイズをもとに回転などがされる(横長のウィンドウサイズなら、斜めに回転させたとき、斜めに伸びる)
		//掛ける順番を考える必要がある（Transformのワールド行列をそのまま使えない）
		//移動、回転、行列の順番を考えて、それぞれadjustScaleに掛けなければならない
		//拡大→回転→移動
	//×順番を考えて, ajustScaleをそれぞれ順番に掛けなくてはいけないが、→まず、最初に、adjustScaleをそれぞれの行列に掛けて、それを最後に順番道理に掛ける？

	/*★******************************************************************/
	//１ピクセルのサイズ（１＊１のサイズ）とtextureサイズ　とワールドそれぞれ
	//順番を考えればテクスチャそのままのサイズで回転などが行えるはず
	//textureそのままのサイズで、拡大したくて、(adjustScale * matScale_)
	//textureそのままのサイズで、回転させたい、（adjustScale * matScale_ * matRotate_）
	//その行列をウィンドウサイズの１＊１のサイズで拡大（ウィンドウにおける１＊１行列でテクスチャで拡大、回転させた行列を拡大）
	//最後に移動

	cb->matW = XMMatrixTranspose(adjustScale * trans.matScale_  *  trans.matRotate_ * miniScale *  trans.matTranslate_);



}


//テクスチャごとのシェーダーへのサンプラーなどへ情報を与える
void Sprite::SetSamplerAndShaderResourceViewEachForTexture(Texture * pTexture)
{

	ID3D11SamplerState* pSampler = pTexture->GetSampler();	//テクスチャクラスから、サンプラーを取得
	Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);
	//シェーダーへの橋渡しのビューを取得
	ID3D11ShaderResourceView* pSRV = pTexture->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
	Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);

}
