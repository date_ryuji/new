#include "HeightMapCreater.h"			//ヘッダ
#include "../DirectX/Texture.h"			//テクスチャ
#include "../Algorithm/ScreenShot.h"	//テクスチャバッファーを画像化
#include "../Text/TextWriter.h"			//テキストライター（ファイル作成）
#include "../DirectX/Camera.h"			//カメラオブジェクト
#include "../Algorithm/Math.h"			//計算用アルゴリズム
#include "../../Shader/BaseShader.h"	//シェーダークラス　継承元クラス


//コンストラクタ
HeightMapCreater::HeightMapCreater() :
	PolygonGroup::PolygonGroup(),
	DEFAULT_SCALE_(0.5f),
	DEFAULT_MAX_Y_(10.0f),
	DEFAULT_MIN_Y_(0.0f),
	polygonCount_(-1),
	vertexCount_(-1),
	depth_(-1),
	width_(-1),
	indexCountEachMaterial_(-1),
	materialCount_(-1),


	pVertices_(nullptr),
	pVertexBuffer_(nullptr),
	pIndex_(nullptr),
	pIndexBuffer_(nullptr),
	pConstantBuffer_(nullptr),
	pTexture_(nullptr)
{
}

//デストラクタ
HeightMapCreater::~HeightMapCreater()
{
}

//初期化
HRESULT HeightMapCreater::Initialize(int width, int depth ,const float SCALE ,
	SHADER_TYPE thisShader , std::string textureFile)
{

	//自身の使用シェーダーの登録
	thisShader_ = thisShader;

	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	//width depthを格納
	width_ = width;
	depth_ = depth;

	//スケール格納
	//DEFAULT_MAX_Y_ = MAX_Y;
	//DEFAULT_MIN_Y_ = MIN_Y;
	DEFAULT_SCALE_ = SCALE;




	//条件：すでに初期化済みであるかを調べる
	//初期化がされていないときに、初期化を呼び出す
	if (pVertices_ == nullptr)
	{	
		//Y座標を除いた
		//頂点情報の確保、確立
		hr = InitVertex();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点情報の初期化失敗", "エラー");
	}

	{
		//頂点バッファー作成
		hr = CreateVertexBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");
	}

	{
		//インデックス情報の作成
		hr = InitIndex();
		//エラーメッセージ
		ERROR_CHECK(hr, "インデックスバッファの作成失敗", "エラー");
	}

	{
		//コンスタントバッファ作成
		hr = CreateConstantBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファの作成失敗", "エラー");
	}

	{
		//テクスチャをロード
		hr = LoadTexture(textureFile);
		//エラーメッセージ
		ERROR_CHECK(hr, "テクスチャの作成失敗", "エラー");
	}



	//処理の成功
	return S_OK;
}

//頂点数、頂点の幅、奥行きの初期化(上書き)
HRESULT HeightMapCreater::OverWriteVertex(int width, int depth, const float SCALE)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;


	width_ = width;
	depth_ = depth;
	DEFAULT_SCALE_ = SCALE;



	//解放
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_RELEASE(pVertexBuffer_);
	SAFE_DELETE_ARRAY(pVertices_);

	{
		//Y座標を除いた
			//頂点情報の確保、確立
		hr = InitVertex();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点情報の初期化失敗", "エラー");
	}

	{
		//頂点バッファー作成
		hr = CreateVertexBuffer();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");
	}

	{
		//インデックス情報の作成
		hr = InitIndex();
		//エラーメッセージ
		ERROR_CHECK(hr, "インデックスバッファの作成失敗", "エラー");
	}



	//処理の結果を入れる変数
	return S_OK;
}

//ポリゴン番号を引数にてもらいの番号に該当する、頂点を一定量上げる
HRESULT HeightMapCreater::UpDownCoodeY(UP_DOWN_TYPE type, const int POLYGON_NUM, const float SPEED)
{
	//上昇：type = 0
	//下降：type = 1
	
	//０の時 -１．０
	//１の時  １．０
	int typeInt = (int)type;
	float code = -1.0f + (2.0f * typeInt);




	//移動量を計算
	float moveValue = SPEED * code;


	//ポリゴン番号から
	//それをインデックス番号として使用するために、三角ポリゴンを作る頂点数で掛ける
	//それにより、示される頂点情報にアクセスし、
	//Y座標を移動させる
	pVertices_[pIndex_[(POLYGON_NUM * 3) + 0]].position.vecY += moveValue;
	pVertices_[pIndex_[(POLYGON_NUM * 3) + 1]].position.vecY += moveValue;
	pVertices_[pIndex_[(POLYGON_NUM * 3) + 2]].position.vecY += moveValue;


	//頂点バッファーを作成しなおす
	SAFE_RELEASE(pVertexBuffer_);
	
	//頂点バッファー作成
	HRESULT hr = CreateVertexBuffer();
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");


	//処理の成功
	return S_OK;
}

//ロード
HRESULT HeightMapCreater::Load(const std::string& FILE_NAME , SHADER_TYPE thisShader)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	{

		/*
		�@
			引数にて示された、
			画像ファイルから、
			ハイトマップの画像をロード。
		�A
			その画像から、
			縦横比をもらう。
		�B
			その比率をもとに、
			頂点数を比率に等しくなるように確保。
			そして、決められたサイズで、頂点のローカル座標を決めていく。
		�C
			頂点のY座標を、
			画像のピクセルごとに、色情報を取得し、
			その色情報をもとに、白に近いほど、あらかじめ決めた、最高Y座標位置に近く、
							　	黒に近いほど、あらかじめ決めた、最低Y座標位置に近く、していく


			これで、基本的な、頂点情報は作成完了。
			あとは、コンスタントバッファとか、いろいろ作成していく。


		*/


		/*�@**************************************************************************/
		//テクスチャのバッファを入れておくポインタ
		ID3D11Texture2D* pTextureBuffer;

		//ハイトマップの画像
		Texture* pHeightMap = new Texture;

		//バッファーの情報を入れておく構造体
		D3D11_MAPPED_SUBRESOURCE hMappedres;

		//バッファーを取得
		//テクスチャを読み込んで、そのバッファーを引数ポインタに登録
		hr = pHeightMap->GetTextureBuffer(FILE_NAME, &pTextureBuffer, &hMappedres);
		if (FAILED(hr))
		{
			pHeightMap->Release(); SAFE_DELETE(pHeightMap);
		}
		//エラーメッセージ
		ERROR_CHECK(hr, "ハイトマップのテクスチャバッファー取得失敗", "エラー");


		/*�A**************************************************************************/
		//画像サイズを取得
		width_ = pHeightMap->GetWidth();
		depth_ = pHeightMap->GetHeight();

		//テクスチャクラスの解放
			//テクスチャのピクセル情報が欲しいわけではないため、バッファーの解放を行う
		pHeightMap->Release();	//何もテクスチャクラスには情報を残していないが、一応解放
		SAFE_DELETE(pHeightMap);

		/*�B**************************************************************************/
		//Y座標を除いた
		//頂点情報の確保、確立
		hr = InitVertex();
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点情報の初期化失敗", "エラー");


		/*�C**************************************************************************/
		//Y座標をヘイトマップから読み取って、Y座標を確保、確立
		hr = InitVertexY(&hMappedres);
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点情報のY座標初期化失敗", "エラー");

		SAFE_RELEASE(pTextureBuffer);

	}

	{
		//頂点情報以外の初期化
		hr = Initialize(width_, depth_, DEFAULT_SCALE_,
			thisShader, "Assets/Scene/Image/GameScene/HeightMap/Grass.jpg");
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点情報以外の初期化失敗", "エラー");
	}


	{
		//テスト
		//ハイトマップ作成呼び込み
		hr = CreateHeightMapImage();
		//エラーメッセージ
		ERROR_CHECK(hr, "ハイトマップの作成失敗", "エラー");
	}


	//処理の成功
	return S_OK;
}


//頂点バッファ作成(Y座標を除く)
HRESULT HeightMapCreater::InitVertex()
{
	//単純に、三角ポリゴンを並べるマップを作ってみる

	//頂点数の確保
		//Width＊Depth分だけ確保
		//そのため、頂点は、画像のピクセル数と同じ数にする。
		//→頂点から作られる、三角ポリゴンを敷き詰めた、マス。→これは、頂点数より1つ少ないマスができるということになる。
			//→頂点を5つ置いたら、頂点と頂点の間のスペースは4つになる。ということ。
	vertexCount_ = (width_) * (depth_);

	//頂点座標(頂点情報などを持つ構造体を頂点ごと取得)
	pVertices_ = new VERTEX[vertexCount_];


	//UV座標
	//var uv = new Vector2[((width + 1) * 2 + 1) * (height + 1) + width + 1];

	//インデックス情報数
		//0行目と1行目で、
			//Width - 1 分だけ、▲ポリゴン * 2ができる
			//▲ポリゴンは3頂点なので
		//Width - 1 * 2 * 3 = 一行のインデックス情報の数
		//上記を＊height - 1分

		//絵にかくと分かりやすいが、　縦横　Width ＊Depth分頂点を書いて、インデックスが何個必要なのか
	indexCountEachMaterial_ = ((width_ - 1) * 2 * 3) * (depth_ - 1);
	//インデックス情報
	pIndex_ = new int[indexCountEachMaterial_];


	//頂点をカウントするカウンター
	int p = 0;

	//頂点を、横にまず、並べて、
	//そして、から次の行に同じ数だけ並べて、を繰り返す

	//頂点確保
	//条件：奥行き
	for (int i = 0; i < depth_; i++)
	{
		//条件：幅
		for (int k = 0; k < width_; k++)
		{
			//Position
			//書込み先のベクトル用意
			//1頂点ずつ、まずは、横並びに等間隔に頂点の取得
			//その行の頂点を確保
			XMVECTOR setPos = XMVectorSet((float)(k * DEFAULT_SCALE_), 0.f, (float)(i * DEFAULT_SCALE_) , 0.f);

			//※バッファーのオーバーランと警告がされてしまう
			//pVertices_[p].position = setPos;
			//回避する方法としては、
			//メモリコピーでコピーする
				//positionに格納する型は、XMVECTORと決まっているので、
				//サイズを指定してコピーさせる
				//上記をおこなえば、警告はなくなる。だが、警告が出されている以上、その警告で言われていることが起こる可能性があるということなので、それを回避する方法を行わなければいけない

			//以下のメモリコピーにおいても、警告が起こる
			//memcpy(&pVertices_[p].position, &setPos, sizeof(XMVECTOR));
			//そのため、
			//pVerticesの構造体型の中に作成したセッターを使用する
			//下記も警告回避ができない
				//setPosの要素を改善しない限り、警告は出てしまうと考える
			pVertices_[p].SetPos(setPos);




			/*
			//Position
			//1頂点ずつ、まずは、横並びに等間隔に頂点の取得
			//その行の頂点を確保
			pVertices_[p].position.vecX = (float)(k * DEFAULT_SCALE_);
			pVertices_[p].position.vecZ = (float)(i * DEFAULT_SCALE_);
			pVertices_[p].position.vecY = 0.f;	//後に改めておこなう
			*/

			
			//UV
			//UV位置指定
			//X	(0~1)を、現在のX座標によって、見る位置を変える
			XMVECTOR setUv = XMVectorSet((float)k / (width_ - 1), (float)i / (depth_ - 1), 0.f, 0.f);

			pVertices_[p].SetUv(setUv);
			/*
			pVertices_[p].uv.vecX = (float)k / (width_ - 1);
			pVertices_[p].uv.vecY = (float)i / (depth_ - 1);
			*/

			
			p++;
		}
	}
	//処理の成功
	return S_OK;

}

//頂点バッファ作成(Y座標限定)
HRESULT HeightMapCreater::InitVertexY(D3D11_MAPPED_SUBRESOURCE * hMappedres)
{


	//１ピクセル事、
	//データを取得する
	int size = 4;	//rgbaのピクセル情報をバッファーに格納しているので、４ずつ
	//テクスチャのピクセルデータにアクセスするためのポインタ
	//０〜２５５の値が取れるように
		//unsigned char 型でデータを取得する。
	unsigned char* dest = static_cast<unsigned char*>(hMappedres->pData);

	//上記のポインタにて示される1つ1つのデータ（unsigned char型）の
	//に入る値が、０　なら　最低。　２５５なら最高という。
		//RGB値を０〜２５５で取得したときの色値が返ってくる
		//その値をもとに、RGBAの値を取得し、1ピクセルの色を取得
		//その色をもとに、黒に近いなら、最低Y座標へ。白に近いなら、最高Y座標へ。

		//https://teratail.com/questions/213854


	//頂点情報の配列の添え字を表すカウンター
	int p = 0;
	//条件：奥行き
	for (int v = 0; v < depth_; v++)
	{
		//条件：幅
		for (int u = 0; u < width_; u++)
		{
		

			//RGBAのR部分だけを取得する
			unsigned char rColor;

			//destが示している1バイトを
				//コピー
			memcpy(&rColor, dest, 1);

			//RGBA分先に進めて、次にRを見たいので、
				//現在見ているポインタ（R）から４つ進める
			/*for (int i = 0; i < size; i++)
			{
				dest++;
			}*/
			dest += 4;

			/*
			�@Rの値を取得する
			�Aその値が、２５５と割って、どの程度の割合か
			�BY座標の最高値と最低値の間の、�Aの割合にて示されるY座標を求める(線形補完)（XMVECTORLerpのようなもの）
				//最高値の割合を仮に１，最低値の割合を仮に０としたとき、　�Aの割合0.5があったとき、 最高値の持っている値と最低値の持っている値　を線で結んだとき、�Aの割合の0.5の位置（値）を求める
			�Cその座標を、その頂点のY座標とする。


			*/

			//1バイト（unsigned char）の最高値
			const int UC_MAX = 255;
			//1バイト（unsigned char）の最低値
			const int UC_MIN = 0;

			//�@
			int r = (int)(rColor);

			//�A
			float per;
			if (r == 0)
			{
				//０との割り算はできないので
				per = 0.0f;
			}
			else
			{
				//割合を取得
				per = r / (float)(UC_MAX);
			}


			//�B
			float between = DEFAULT_MIN_Y_ + per * (DEFAULT_MAX_Y_ - DEFAULT_MIN_Y_);


			//�C登録
			pVertices_[p].position.vecY = between;

			//頂点を進める
			p++;


		}


	}
	//上記で、
	//RGBAの値を取得できるので、
		//その色から、
		// 0000 から　1111の間でのそのピクセル色の割合を取得する

		// rgb の合計が　0なら、黒。　rgb の合計が 3なら、白。　などといった判断の仕方で、
		//どのぐらいの間にいるのかを割合で取得。

	//それによって、出現の高さを取得する。


	//処理の成功
	return S_OK;
}

//頂点情報から頂点バッファーを作成
HRESULT HeightMapCreater::CreateVertexBuffer()
{
	//バッファの詳細情報を格納する構造体
		//頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;

	//バッファへのアクセスポインタ
	//頂点情報のリソースを入れる構造体
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = pVertices_;

	//頂点バッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファ作成失敗", "エラー");

	//処理の成功
	return S_OK;
}


//インデックス情報作成
HRESULT HeightMapCreater::InitIndex()
{
	//インデックス情報の登録

	/*

		//イメージは、
		//仮にWidth=5 , height = 5
		//一行に頂点は　５　、　それが５行　となるように配置


		//0行目から見て行って、
			//0行目と、1行目と三角形の作り方を考える
		//それが終わったら、次は
			//1行目と、2行目の三角形の作り方と遷移する

		//つまり、0行目には、
			// 頂点番号　０〜４
		//		  1行目には、
			// 頂点番号　５〜９
		// 〜

		//★敷き詰めた頂点からインデックスによって、三角ポリゴンを作る。
			//0行目の。０番目の頂点は、
			//1行目の。５と６番目の頂点と1つの三角形
			//0行目の。1番目の頂点は、
			//1行目の。６と７番目の〜
		//上記を　３番目まで続ける。（k < width - 1）

		//1行目の。1番目(つまり６)の頂点は
			//0行目の。０と１番目の頂点と1つの三角形。
		//上記を　４番目まで続ける。　(k < width)


		//上記を　高さ分繰り返す



	*/
	/*
		0行目で、右に斜辺が向いている直角三角形を作り、

		1行目で、左に斜辺が向いている直角三角形を作り、

		その連続を行う

	*/



	int p = 0;

	//ポリゴンの枚数を数える変数
	//int polygonCounter = 0;

	//ポリゴンの枚数を確保
	polygonCount_ = indexCountEachMaterial_ / 3;
	//ポリゴン情報を、ポリゴン数分確保
		//インデックス情報は3つで1枚のポリゴンなので
		//　インデックス情報数 / 3 すれば、ポリゴン数になる
	//pPolygonInfo_ = new PolygonInfo[polygonCount_];

	//高さは、頂点をひとつ減らした数分だけ繰り返す
	//理由は、　一回のforにて、　現在のiの行と、その下の行を見るので、 i　を配列の添え字限界まで回すと、範囲外になる
	for (int i = 0; i < (depth_ - 1); i++)
	{
		//i = 0
		//0行目と、1行目を使った、三角形を作るインデックス情報の作成

		//0行目の三角形のインデックス情報
		for (int k = 0; k < (width_ - 1); k++)
		{
			//時計回りにインデック情報は登録しないといけないので、
			//順番は逆にしている。

			//ここで注意するのが、
				//ｘ（０，０）から、ｘ（５，５）仮　に向かって頂点を埋めるということ
				//つまり、左手前から右奥へ頂点を埋めるということ

			//例
			//[p + 0]　＝　０行目の０番目
			//[p + 1]　＝　１行目の０番目
			//[p + 2]　＝　１行目の１番目
			//計算説明
			//[p + 0]　＝　(行番目 * １行の頂点数) + 現在の列
			//[p + 1]　＝　(行番目 * １行の頂点数) + 現在の列 + (1行の頂点数)
			//[p + 2]　＝　(行番目 * １行の頂点数) + 現在の列 + (1行の頂点数 + 1)
			/*

			+ (行番目 * １行の頂点数) = 行における　頂点番号開始位置を示す
			+ k = 頂点番号＋ｋ　でその行における　現在見ている頂点番号
			+ width = 次の行

			*/
			pIndex_[p + 0] = (i * width_) + k;
			pIndex_[p + 1] = (i * width_) + k + (width_);
			pIndex_[p + 2] = (i * width_) + k + (width_ + 1);

			////ポリゴン情報をセット
			////ポリゴン番号をセット。
			//	//０〜
			//	//インデックス情報が欲しいときは、ポリゴン番号＊３　をした頂点から3つが、
			//	//そのポリゴンを作るインデックス情報となる。
			//pPolygonInfo_->polygonNum = polygonCounter;
			//polygonCounter++;


			XMVECTOR v0 = pVertices_[pIndex_[p + 0]].position;
			XMVECTOR v1 = pVertices_[pIndex_[p + 1]].position;
			XMVECTOR v2 = pVertices_[pIndex_[p + 2]].position;

			//追加したインデックス情報分カウント
			p = p + 3;
		}

		//1行目の三角形のインデックス情報
		for (int k = 1; k < width_; k++)
		{
			//時計回りにインデック情報は登録しないといけないので、
			//順番は逆にしている。


			//例
			//[p + 0]　＝　０行目の１番目
			//[p + 1]　＝　０行目の０番目
			//[p + 2]　＝　１行目の１番目
			//計算説明
			//[p + 0]　＝　(行番目 * １行の頂点数) + 現在の列
			//[p + 1]　＝　(行番目 * １行の頂点数) + 現在の列 - 1
			//[p + 2]　＝　(行番目 * １行の頂点数) + 現在の列 + (1行の頂点数)
			/*

			+ (行番目 * １行の頂点数) = 行における　頂点番号開始位置を示す
			+ k = 頂点番号＋ｋ　でその行における　現在見ている頂点番号
			+ width = 次の行
			- 1 = 頂点１つ左

			*/
			pIndex_[p + 0] = (i * (width_)) + k;
			pIndex_[p + 1] = (i * (width_)) + k - 1;
			pIndex_[p + 2] = (i * (width_)) + k + (width_);

			////ポリゴン情報をセット
			////ポリゴン番号をセット。
			//pPolygonInfo_->polygonNum = polygonCounter;
			//polygonCounter++;

			XMVECTOR v0 = pVertices_[pIndex_[p + 0]].position;
			XMVECTOR v1 = pVertices_[pIndex_[p + 1]].position;
			XMVECTOR v2 = pVertices_[pIndex_[p + 2]].position;


			//追加したインデックス情報分カウント
			p = p + 3;
		}


	}

	//インデックスバッファ作成
	CreateIndexBuffer();

	//処理の成功
	return S_OK;
}

//インデックス情報からインデックスバッファーを作成
HRESULT HeightMapCreater::CreateIndexBuffer()
{

	// インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(int) * (indexCountEachMaterial_);	//サイズを、indexのサイズ分だけ、（インデックス情報の入っているサイズだけ）
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;


	//バッファーの格納情報
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex_;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	//インデックスバッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "インデックスバッファ作成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//コンスタントバッファの作成	
HRESULT HeightMapCreater::CreateConstantBuffer()
{

	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//コンスタントバッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "コンスタントバッファ作成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//テクスチャのロード
HRESULT HeightMapCreater::LoadTexture(const std::string& TEXTURE_FILE_NAME)
{
	//空の時、
	//何もせずに帰る
	if (TEXTURE_FILE_NAME == "")
	{
		return S_OK;
	}

	//ファイルのロード
	//テクスチャのロード
	pTexture_ = new Texture;
	HRESULT hr = pTexture_->Load(TEXTURE_FILE_NAME);
	if (FAILED(hr))
	{
		SAFE_DELETE(pTexture_);
	}
	//エラーメッセージ
	ERROR_CHECK(hr, "テクスチャの読み込みを失敗", "エラー");


	//処理の成功
	return S_OK;
}


//ポリゴンクラスのポリゴンの描画を行う
HRESULT HeightMapCreater::Draw(Transform & transform)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	//シェーダーを選択
	Direct3D::SetShaderBundle(thisShader_);


	//シェーダーにおけるコンスタントバッファへわたす情報をまとめる
		//詳細：自クラスの　CONSTANT_BUFFER(sturct)とシェーダーファイルにおけるコンスタントバッファの構造体　の格納数、格納情報を等しくしているため、
		//　　：伴い、描画に必要な現在の情報をCONSTANT_BUFFER(sturct)　に格納して、最終的にシェーダーファイルに渡す

	//コンスタントバッファ　格納情報
	CONSTANT_BUFFER cb;


	//コンスタントバッファ　へのデータを格納（準備）
	{
		//ワールド　＊　ビュー　＊　プロジェクション行列
		cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() * Camera::GetProjectionMatrix());

		//ワールド行列
			//詳細：法線の回転のために必要なワールド行列
		cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));	//

		//ワールド行列
			//詳細：ハイライト表示のためにカメラのワールド座標を求める必要がある 
		cb.matWorld = XMMatrixTranspose(transform.GetWorldMatrix());

		//カメラの位置を入れる
		cb.camPos = XMFLOAT4(Camera::position_.vecX, Camera::position_.vecY, Camera::position_.vecZ, 0);


		//条件：テクスチャあり
		if (pTexture_ != nullptr)
		{

			//nullptrでない　＝　テクスチャが存在する
			//「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
				//サンプラーは、どこに貼り付けますか？などの情報
				//hlsl : g_sampler(s0)
			ID3D11SamplerState* pSampler = pTexture_->GetSampler();	//テクスチャクラスから、サンプラーを取得
				//第一引数：サンプラー　番目
			Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

			//シェーダーへの橋渡しのビューを取得
				//hlsl : g_texture(t0)
			ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
				//第一引数：テクスチャ　番目
			Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);


			//テクスチャ　有
				//コンスタントバッファのテクスチャ有無にtrueを立てる
			cb.isTexture = TRUE;
		}
		//テクスチャなし
		else
		{
			//nullptrである　＝　テクスチャが存在しない
				//　＝　マテリアルそのものの色を使用しないといけない

			//テクスチャ　無
			cb.isTexture = FALSE;

		}

	}

	//コンスタントバッファ1の確保
	{
		//シェーダーのコンスタントバッファ１のマップ
			//コンスタントバッファ1へのアクセス許可
		hr = Direct3D::GetShaderClass(thisShader_)->MapShaderConstantBuffer1View();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファMap失敗", "エラー");


		//自身の使用する
			//シェーダー独自の
			//コンスタンバッファ１や、テクスチャの受け渡しを行う
			//処理は、シェーダークラスに一任する
		Direct3D::GetShaderClass(thisShader_)->ShaderConstantBuffer1View();


		//シェーダーのコンスタントバッファ１のアンマップ
			//コンスタントバッファ1へのアクセス拒否
		hr = Direct3D::GetShaderClass(thisShader_)->UnMapShaderConstantBuffer1View();
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファUnMap失敗", "エラー");
	}

	//コンスタントバッファをシェーダーファイルへコピー、渡す
	{
		//バッファへのマップ設定
			//ポインタへのアクセスなどに使用する
		D3D11_MAPPED_SUBRESOURCE pdata;
		//コンスタントバッファへのマップ
			//詳細：アクセス許可
			//　　：シェーダーなどのGPUでしか、触れることのできないものに
			//　　：CPUから触れる準備
			//　　：マップを行い、バッファへのアクセスができるようになるが、その詳細のアクセスを上記pdataにて行う。
		hr = Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);
		//エラーメッセージ
		ERROR_CHECK(hr, "コンスタントバッファのマップの書き込み失敗", "エラー");


		//メモリにコピー
			//シェーダーファイル側のコンスタントバッファ（struct）へ 
			//データを値を送る
		memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));
	}

	//頂点情報のセット
	{
		//頂点バッファをセット（頂点を持っておくための領域（確保場所））
			//詳細：複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		//1頂点のサイズを指定する
			//詳細：位置のみであれば、XMVECTORの構造体であったが
			//　　：位置やUVも確保するため、頂点情報の構造体のサイズを受け取らせる
		UINT stride = sizeof(VERTEX);

		UINT offset = 0;
		//頂点バッファをセット
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

		//サイズの更新
			//詳細：インデックスバッファに登録している情報は、int情報の配列
			//　　：そのため、intのサイズを渡す
		stride = sizeof(int);

		offset = 0;
		// インデックスバッファーをセット
		Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	}


	//コンスタントバッファをセット
	{
		//コンスタントバッファをセット
		//頂点シェーダー用	
		Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);
		//ピクセルシェーダー用
		Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);

		//コンスタントバッファへのアンマップ
			//詳細：アクセス拒否
		Direct3D::pContext->Unmap(pConstantBuffer_, 0);

	}


	//描画
		//詳細　　：インデックス数を渡して、頂点によるポリゴンの描画を宣言 
		//第一引数：描画インデックス数　描画の対象となるポリゴンを作るインデックスの数
	Direct3D::pContext->DrawIndexed(indexCountEachMaterial_, 0, 0);


	//処理の成功
	return S_OK;

}

//解放
void HeightMapCreater::Release()
{
	//ポインタ宣言順と逆に解放
	pTexture_->Release();
	SAFE_DELETE(pTexture_);
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pVertices_);
	SAFE_RELEASE(pVertexBuffer_);


}

//レイキャスト
void HeightMapCreater::RayCast(RayCastData * rayData)
{
	//条件：ポリゴンごと
	for (DWORD j = 0; j < (DWORD)polygonCount_; j++)
	{
		//列は、ポリゴン事取得するので、
		//インデックス情報は、三角形を構成する頂点が時計回りに順番に並んでいる。ということは０番め、１番目、２番目が１つの三角形を示す。→この流れがポリゴン数　毎　続く
		// j = 0 
			//０番目＝　j * 3
			//１番目＝　j * 3 +  1
			//２番目＝　j * 3 +  2
		//これでインデックス情報の頂点番号が取得できる
			//頂点番号を、頂点情報の引数にして、位置を取得
		// j = 1 
			//３番目＝　j * 3
			//４番目＝　j * 3 +  1
			//５番目＝　j * 3 +  2


		//3頂点の位置情報を取得
			//インデックス情報は、
			//あらかじめ、時計回りで入っている。
		XMVECTOR v0 = pVertices_[pIndex_[(j * 3) + 0]].position;
		XMVECTOR v1 = pVertices_[pIndex_[(j * 3) + 1]].position;
		XMVECTOR v2 = pVertices_[pIndex_[(j * 3) + 2]].position;

		float dist = 0.0f;

		//その三角形との判定
			//レイの情報は引数にてもらっているので、
			//その情報と、
			//三角形の頂点の情報を与えて、接触判定を行う。
		bool hit = Math::Intersect(
			rayData->start,		//レイ発射位置
			rayData->dir,			//レイの方向
			v0,					//三角形頂点０
			v1,					//三角形頂点１ 
			v2, 					//三角形頂点２
			&dist,					//レイと面の接触位置までの長さ
			rayData->permitNegativeDirection	//マイナス方向も考慮した衝突判定を行うか
		);

		//当たっていたら
		if (hit)
		{
			//当たったフラグを立てる
			rayData->hit = true;
			

			if (rayData->dist > dist)
			{
				//前回に当たった地点よりも短いとき更新
				rayData->dist = dist;

				//面の法線ベクトルを求める
				//�@ v1 - v0 = v0 から　v1 へのベクトル
				//�A v2 - v0 = v0 から　v2 へのベクトル
				//�B上記の2つの外積で、法線ベクトルを求める
				XMVECTOR vector1 = v1 - v0;
				XMVECTOR vector2 = v2 - v0;

				//rayData->normal = XMVector3Cross(vector1, vector2);
				rayData->normal = XMVector3Cross(vector2, vector1);

				//ポリゴン番号
				rayData->polygonNum = j;

			}
			//return;
		}
	}


	

}

//引数ベクトルと、ポリゴンクラス（PolygonGroup＊）と衝突した面の法線ベクトルを取得
XMVECTOR HeightMapCreater::NormalVectorOfCollidingFace(XMVECTOR & start, XMVECTOR & dir)
{
	return XMVECTOR();
}


//コンスタントバッファを渡す
ID3D11Buffer * HeightMapCreater::GetConstantBuffer()
{
	return nullptr;
}


//コンストラクタ
HeightMapCreater::VERTEX::VERTEX() :
	position(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	uv(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	normal(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}

//頂点情報　座標セッター
void HeightMapCreater::VERTEX::SetPos(XMVECTOR& setPos)
{
	position = setPos;
}
//頂点情報　UVセッター
void HeightMapCreater::VERTEX::SetUv(XMVECTOR& setUv)
{
	uv = setUv;
}
//頂点情報　法線セッター
void HeightMapCreater::VERTEX::SetNormal(XMVECTOR& setNormal)
{
	normal = setNormal;
}

//ハイトマップを作成する
HRESULT HeightMapCreater::CreateHeightMapImage()
{
	HRESULT hr = S_OK;

	//ハイトマップ作成の流れ
	/*
	※CPUのなかだけで完全に完結させる
	※現在の頂点情報から、1頂点を1ピクセルとして、テクスチャに書き込む
	※1ピクセルに書き込むピクセルのRGB値は、共通の値である（つまり、　黒なら(0,0,0) , 白なら(1,1,1) , 灰なら(0.5,0.5,0.5)）
		白黒の画像を作成する



	�@頂点により示される四角形の縦横比を取得
	�A�@の比率分のサイズを持った、テクスチャを確保
	�B全頂点の中での最高のY座標と、最低のY座標の値を取得する（それを保存しておく）（或いは、こちらで、最高の値を定数で決めてしまって、その値をもとに、�Cへ）
	�C頂点1つ目から、ピクセルの（0，0）に、ピクセル色を格納していく
		ピクセル色：　その頂点のY座標を取得して、�Bにて取得した最高と最低との線形補完内の、現在見ているYがどの位置にいるか、その割合を取得する。（最低を０，最高を１としたときに、現在見ているY座標の割合）

	�Dテクスチャを格納し終えたら、そのテクスチャを画像化


	
	*/

	//�@
	int width = width_;
	int depth = depth_;

	//�A
	//テクスチャ生成
	//テクスチャのバッファー
	ID3D11Texture2D*	pBuffer;	
	//テクスチャの設定を入れる構造体
	D3D11_TEXTURE2D_DESC texdec;
	texdec.Width = width;	//画像の横幅
	texdec.Height = depth;	//画像の縦幅
	texdec.MipLevels = 1;	
	texdec.ArraySize = 1;
	//ここで、必ず、RGBAの4成分を確保することを設定する
		//4成分にしなければ、書き込むときに、ずれが生じてしまう
		//1バイトずつ、　R,G,B,Aと、4回回して、それぞれの成分を書き込ませるので、4成分にしないとsize、書き込むときのポインタがずれるので注意
	texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texdec.SampleDesc.Count = 1;
	texdec.SampleDesc.Quality = 0;
	texdec.Usage = D3D11_USAGE_DYNAMIC;
	texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	texdec.MiscFlags = 0;
	//設定をもとに作成
	hr = Direct3D::pDevice->CreateTexture2D(&texdec, nullptr, &pBuffer);
	//エラーメッセージ
	ERROR_CHECK(hr, "テクスチャの生成失敗", "エラー");



	//CPUからテクスチャへアクセスすることを宣言
	//アクセスに用いる構造体
	D3D11_MAPPED_SUBRESOURCE hMappedres;

	//第3引数：書き込みを行う
	hr = Direct3D::pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &hMappedres);
	//エラーメッセージ
	ERROR_CHECK(hr, "バッファーのマップ（CPUからの書き込み許可など）", "エラー");


	//�B
	//最高Y座標（ローカル）
	float maxLocalY = -100.0f;
	//最低Y座標（ローカル）
	float minLocalY = 100.0f;
	//座標の中から最高、最低を探す
	for (int i = 0; i < vertexCount_; i++)
	{
		float y = pVertices_[i].position.vecY;
		if (maxLocalY < y)
		{
			maxLocalY = y;
		}
		if (minLocalY > y)
		{
			minLocalY = y;
		}
	}

	//�C
	//引数にて渡した、
	//テクスチャへのアクセスをおこなえる構造体、により、
		//最高値、最低値から求められる、そのピクセルの色を決めて、書き込ませる
	WritePixelsToTexture(&hMappedres, maxLocalY, minLocalY);


	//CPUからのアクセス終了
	Direct3D::pContext->Unmap(pBuffer, 0);

	//�D
	//テクスチャから、
	//画像を作成する
	ScreenShot* pScreenShot = new ScreenShot;
	//引数：バッファー
	//引数：フォルダまでのルートディレクトリ名
	//引数：ファイル保存名
	hr = pScreenShot->CreateImageFile(pBuffer, "Assets/Scene/OutputData/GameScene/MapEditorResult", "HeightMap");
	if (FAILED(hr))
	{
		//解放
		SAFE_DELETE(pScreenShot);
		SAFE_RELEASE(pBuffer);
	}
	//エラーメッセージ
	ERROR_CHECK(hr, "テクスチャバッファの画像化失敗", "エラー");


	

	//最高値と最低値をファイルとして出力
	WriteText(maxLocalY , minLocalY);





	//解放
	SAFE_DELETE(pScreenShot);
	SAFE_RELEASE(pBuffer);




	//処理の成功
	return S_OK;
}

//テクスチャバッファーへ　ピクセル色情報を書き込む
void HeightMapCreater::WritePixelsToTexture(D3D11_MAPPED_SUBRESOURCE * hMappedres, float maxY, float minY)
{


	//１ピクセル事、
	//1ピクセル、計4バイト
	int size = 4;	//rgbaのピクセル情報をバッファーに格納しているので、４ずつ
	/*
	テクスチャ宣言時
	texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;　と設定したことにより、
	テクスチャを、
	R,G,B,Aの　計4バイトずつ管理されていることになる
	
	*/

					
	//テクスチャのピクセルデータにアクセスするためのポインタ
	//０〜２５５の値が取れるように　1バイトずつ
		//unsigned char 型でデータを取得する。（unsigned = 符号なし、の char 1バイトずつ）
	unsigned char* dest = static_cast<unsigned char*>(hMappedres->pData);


	//頂点情報の配列の添え字を表すカウンター
	int p = 0;

	//頂点すべてを回して、
	//1ピクセルの　RGBAの色情報（０〜２５５のunsigned char型）二アクセスして、
		//色を格納させる
	//頂点情報の格納順：（ｘｙの縦横で見た時に）（０，０）から、横に1ずつ増えていく、Width分配置したら、次の行から（０，１）として横に1ずつ増えていく（それを一次元配列で連番で格納している）
	//テクスチャ情報の格納順：（ｘｙの縦横で見た時に）（０，０）から、横に1ずつ増えていく、Width分配置したら、次の行から（０，１）として横に1ずつ増えていく（それを２次元配列で、（１ピクセルRGBAそれぞれ）というイメージになっている）
	for (int i = 0; i < vertexCount_; i++)
	{
		{
			//最高と最低から
			//現在見ている座標の割合を取得
				/*最高と、最低と、割合から、　座標を求める計算から、

				float between = MIN_Y_ + per * (MAX_Y_ - MIN_Y_);
				MIN_Y_ + per * (MAX_Y_ - MIN_Y_) = between;
				per * () = between - MIN_Y_;	//両辺に / ()
				per = (between - MIN_Y_) / (MAX_Y_ - MIN_Y_);

				*/

			float per = (pVertices_[i].position.vecY - minY) / (maxY - minY);

			//上記の割合が　RGBに登録する色情報となる
				//つまり、上記の割合をもとに、色情報の０〜２５５の値に変換して
				//それを、実際のテクスチャのRGBに登録すれば、
				//最低を（０，０，０）の黒、最高を（１，１，１）の白とした、ハイトマップを表現できる

			//unsigned char　における最高値
			const int MAX_RGB = 255;

			//unsigned char にしたときの
			//値の確保
			int rgb = (int)(MAX_RGB * per);


			//この値を
			//RGB　共通の値として使う（白黒なので、RGB共通の色にする必要がある）
			unsigned char commonColor = rgb;

			//ポインタにて示されている
			//RGBの格納領域を示す、ポインタの要素を書き換えて、ピクセル職の確定
			//Aを除いた、色情報の登録
			for (int c = 0; c < (size - 1); c++)
			{
				//ポインタにて示されるRGBの色情報を書き換える
				(*dest) = commonColor;
				//ポインタを回す
				dest++;

			}

		}

		//Aの登録
		//α値　２５５　＝　１．０
		unsigned char alpha = 255;

		(*dest) = alpha;
		dest++;

	}



}

//テクスチャバッファーへ引数にて渡された値から
void HeightMapCreater::WriteText(float maxY, float minY)
{
	TextWriter* pTextWriter = new TextWriter(DELIMITER_COMMA);
	//maxYの追記
	pTextWriter->PushBackString(std::to_string(maxY) , true);

	//minYの追記
	pTextWriter->PushBackString(std::to_string(minY), true);

	//改行
	pTextWriter->PushBackNewLine();
	//終了（最後に改行して終了）
	pTextWriter->PushEnd();


	//書き込み実行
	pTextWriter->FileWriteExcecute("Assets/Scene/OutputData/GameScene/MapEditorResult/MaxY_And_MinY.txt");

	//解放
	SAFE_DELETE(pTextWriter);
}

