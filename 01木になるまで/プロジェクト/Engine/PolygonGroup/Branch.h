#pragma once
#include "PolygonGroup.h"	//ポリゴングループ　２D3Dのポリゴンを扱うクラス群
#include <fbxsdk.h>			//FBXSDK FBXを扱うための補助機能、ソース群


//ライブラリのリンク
#pragma comment(lib, "LibFbxSDK-MT.lib")	//FBXSDK
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")


/*
	クラス詳細	：ポリゴンクラス　BranchesMakeTreeのソース（Branch）専用
	クラスレベル：サブクラス（スーパークラス（PolygonGroup））
	クラス概要（詳しく）
				：３Dポリゴンとして描画を行わないクラス
				：３Dポリゴンとして、BranchesMakeTreeに結合のための素材クラス。
				　自身では描画せず、BranchesMakeTreeを作る、一つのソースの役割を示す。

				 BranchMakeTreeとは、Branchソースを結合していくことで、ひとつのモデルを作るモデルである。
				 BranchMakeTreeは大本のボディ、Branchはボディのパーツ

*/
class Branch : public PolygonGroup
{
//public 構造体
public:

	/*
		構造体詳細：頂点情報
		構造体詳細（詳しく）
				　：シェーダーファイルの頂点シェーダー、引数に渡す情報
		　　　　　：頂点ごとの情報、位置、UV,法線、接線をもち、最終的に頂点同士をつなげ、一つのポリゴンを作る。そのポリゴンの集合により任意の形を作る
		アクセスレベルpublic理由
				　：Engine/BranchesMakeTreeにおけるソースポリゴンとして使用するために、外部から頂点情報にアクセスできるようにする
	*/
	struct VERTEX
	{
		//位置情報（頂点の位置）
		XMVECTOR position;
		//UV情報（テクスチャの座標）
		XMVECTOR uv;
		//法線情報（頂点の向き）
		XMVECTOR normal;

		//コンストラクタ
		VERTEX();

	};



//protected メンバ変数、ポインタ、配列
protected:

	//頂点数（FBXファイルより取得）
	int vertexCount_;
	//ポリゴン数（FBXファイルより取得）
	int polygonCount_;
	//インデックス数
	int indexCountEachMaterial_;

	//各頂点ごとの頂点情報を入れる1次元配列ポインタ
		//詳細：ポリゴンを作る各頂点情報を保存
	VERTEX* pVertices_;

	//インデックス情報を登録しておく
		//動的確保した配列
	int* pIndex_;

	//頂点情報を登録したバッファー
	ID3D11Buffer* pVertexBuffer_;
	//頂点の順番、インデックス情報を持っておくバッファー
			//詳細：マテリアルごとのインデックスバッファー、ポインタのポインタ
	ID3D11Buffer* pIndexBuffer_;


	//描画を行わないため、
	//コンスタントバッファ必要なし


//protected メソッド
protected:

	//頂点バッファ作成
		//詳細：頂点情報を受け取り、頂点情報からシェーダーの頂点シェーダーに渡すための頂点バッファへの格納まで済ませる
		//　　：頂点バッファへ格納後、頂点情報の構造体は必要なくなる。（アニメーションや、頂点情報をいじらないのであれば、今後使うことはない）
		//引数：FBXモデルのノードのメッシュ
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT InitVertex(fbxsdk::FbxMesh* mesh);
	//インデックスバッファ作成
		//詳細：FBXファイルに登録されている、インデックス情報を格納する
		//　　：インデックス情報は、描画の段階で、マテリアルごとのインデックス情報を必要となる。最後まで残す。
		//引数：FBXモデルのノードのメッシュ
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）	
	HRESULT InitIndex(fbxsdk::FbxMesh* mesh);

	
	//頂点情報から頂点バッファーを作成
		//レベル：仮想関数
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT CreateVertexBuffer();
	//インデックス情報からインデックスバッファーを作成
		//レベル：仮想関数
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT CreateIndexBuffer();


public:


	//コンストラクタ
		//引数：なし	
		//戻値：なし
	Branch();

	//デストラクタ
		//引数：なし	
		//戻値：なし
	~Branch() override;

	//（Branch）FBXファイルのロードを行う（引数のファイル名より）
		//レベル：オーバーライド
		//制限：FBXファイル名拡張子（.fbx）
		//    ：�@FBXのポリゴン情報の結合
		//　　：�AFBXを三角化、
		//　　：�Bトランスフォームのフリーズ
		//　　：�Cヒストリの全削除
		//　　：�D接線と法線を含める
		//引数：FBXファイル名
		//引数：シェーダータイプ
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT Load(const std::string& FILE_NAME , SHADER_TYPE thisShader) override;

	//描画なし


	//解放
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;


	/*Engine/BranchMakeTree　のソース使用に呼ぶメソッド********************************************************************/
	//頂点数を取得
		//引数：なし
		//戻値：頂点数
	int GetVertexCount();
	//ポリゴン数を取得
		//引数：なし
		//戻値：ポリゴン数
	int GetPolygonCount();
	//インデックス数を取得
		//引数：なし
		//戻値：インデックス数
	int GetIndexCount();

	//頂点情報群を取得
		//引数：なし
		//戻値：頂点情報群
	Branch::VERTEX* GetVertexes();
	//インデックス情報群を取得
		//引数：なし
		//戻値：インデックス情報群
	int* GetIndexes();


};

