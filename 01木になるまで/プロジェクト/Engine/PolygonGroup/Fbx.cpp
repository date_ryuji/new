#include "Fbx.h"					//ヘッダ
#include "../DirectX/Camera.h"		//カメラオブジェクト
#include "../DirectX/Texture.h"		//テクスチャクラス
#include "../../CommonData/GlobalMacro.h"	//共通マクロ
#include "../Algorithm/Math.h"				//計算アルゴリズムクラス
#include "../../Shader/BaseShader.h"		//シェーダークラス　継承元


//コンストラクタ
Fbx::Fbx() :
	PolygonGroup::PolygonGroup() , 
	vertexCount_(0) ,
	polygonCount_(0) , 
	materialCount_(0) , 

	indexCountEachMaterial_(nullptr) , 
	pConstantBuffer_(nullptr) , 
	pVertexBuffer_(nullptr) , 
	pVertices_(nullptr) , 
	ppIndexBuffer_(nullptr) , 
	ppIndex_(nullptr)  ,
	pMaterialList_(nullptr)
{	
}

//デストラクタ
Fbx::~Fbx()
{
}

//ロード
HRESULT Fbx::Load(const std::string& FILE_NAME, SHADER_TYPE thisShader)
{
	//シェーダーの保存
	thisShader_ = thisShader;


	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	//FBXファイルから頂点情報を取得して
	//クラス内メンバに格納する

	/*
		前提（読み込むことのできるFBXファイル）

		・FXBファイル（.fbx）
		・ポリゴンがすべて結合されている
		・ポリゴン三角化
		・トランスフォームのフリーズ
		・種類ごとにすべてを削除→ヒストリ

		出力時
		・頂点単位の法線の分割
		・法線、接戦
	*/

	
	/*FBXファイル管理マネージャーの作成*************************************************************************/
	//FBXファイル管理のマネージャー
		//FBXのトップをCreateで呼ぶ
	//マネージャを生成
	FbxManager* pFbxManager = FbxManager::Create();


	/*FBXファイルローダーの作成********************************************************************************/
	//FBXをロードをする機能を持っているクラスの作成
		//ロードする人
		//インポーター（輸入、ロードして持ってくる）する人を生成
	FbxImporter *fbxImporter = FbxImporter::Create(pFbxManager, "imp");

	//ファイル名でインポーターがインポート（輸入、ロード）している
	fbxImporter->Initialize(FILE_NAME.c_str(), -1, pFbxManager->GetIOSettings());

	//ゲーム内のシーンに読み込む
		//１つのシーンに入れるそれぞれのシーンに、取り込む

	//シーンオブジェクトにFBXファイルの情報を流し込む
	FbxScene*	pFbxScene = FbxScene::Create(pFbxManager, "fbxscene");
	//FBXのシーン、をインポートさせて、ゲームのシーン内にてインポート、使えるようにする？
	fbxImporter->Import(pFbxScene);	
	//解放
	fbxImporter->Destroy();


	/*FBXのメッシュ情報取得************************************************************************************/
	//メッシュ情報を取得

	//メッシュの一番上のノードを取得
		//FBXのパーツのつながりは、各ノードとしてつながっている
		//メッシュ情報を格納した　Aパーツ（メッシュ情報が格納されている一番上）の下に、Bパーツ（Aパーツの子供）があって、、、
			//だが、今回は、Aパーツ（一番上）のノードのみを取得し、ポリゴンとして表示するようにする。
		
		//メッシュを結合している場合
		//一番根っこのノードから０番目のノードに一番親のノードが格納されている

		//メッシュを結合していることで
		//FBXのメッシュすべてがそのノードに格納されているので、
		//そのノードからすべての頂点情報などを取得可能


	//一番根っこのノードを取得
	FbxNode* rootNode = pFbxScene->GetRootNode();
	
	//ルートノードの一番最初のノードを持ってくる
		//メッシュをすべて結合しているならば、
		//すべてのメッシュ情報が入っている	
			//→各頂点、法線
			//→すべての情報を合わせたものをメッシュという
	FbxNode* pNode = rootNode->GetChild(0);
	
	//取得したノードの中のメッシュ情報取得
		//頂点の情報、法線の情報（ノードからメッシュを取得）
	FbxMesh* mesh = pNode->GetMesh();


	/*メッシュとノードから頂点情報（頂点数など）を取得*********************************************************/
	//メッシュから
	//頂点の個数を取得
		//各情報の個数を取得
		//頂点の情報がわからないとインデックス情報は作れない
	vertexCount_ = mesh->GetControlPointsCount();
	
	//メッシュから
	//ポリゴンの個数を取得
	polygonCount_ = mesh->GetPolygonCount();

	//ルートノードの子供ノード（ルートから一つ下の子供ノード）から
	//マテリアル個数を取得 
		//一つのノードの中に、マテリアル情報とメッシュ情報が入っている
		//マテリアル情報を下記で取得し、メッシュ情報から各頂点個数などを取得した
	materialCount_ = pNode->GetMaterialCount();


	/*ファイルのディレクトリ取得*********************************************************************************************/
	//FBXファイルを扱うにあたって、引数ファイル名は長くて不便
		//そのため、現在参照中のパス。現在いるカレントディレクトリのパスを変更する
		//そのために、引数ファイル名から、FBXファイル前までのディレクトリをカレントディレクトリにしてしまえば、ファイルへのアクセスが楽になると考える


	//ディレクトリ名を入れる配列
		//引数ファイル名からディレクトリ名のみ格納する配列
		//Assets/Model/Fbx/AA.fbx
		//上記が引数ファイル名として渡された場合、
		//上記の配列には、　「Assets/Model/Fbx/」まで代入される

		//詳細：MAX_DIRには、Windows側が、決めたディレクトリ名の最大文字数はこのくらいだろうという標準のサイズが入っている
	char dir[_MAX_DIR];         
								

	//ファイル名の引数からディレクトリ名だけ取得する
		//詳細：stringの文字列はポインタのアドレスなので、そのまま使うことができない、const char* に変更→ str.c_str()
		//　　：dirにて格納先のポインタが入る（配列の[]なしは、配列の先頭アドレスとなる）
		//　　：dirへファイルのディレクトリ部分が入る
	_splitpath_s(FILE_NAME.c_str(), nullptr, 0, dir, _MAX_DIR, nullptr, 0, nullptr, 0);
		

	/*カレントディレクトリの変更*******************************************************************************************/
		//FBXファイルロード終了後
		//きちんと元のカレントディレクトリに戻す前提


	//カレントディレクトリ＝現在見ているディレクトリ（標準でパスを見始めるディレクトリ）
	//カレントディレクトリを上記のディレクトリ名にする
		//カレントディレクトリの変更前に
		//現在のカレントディレクトリを覚えておく（もとに戻すときのために）
	
	//デフォルトのカレントディレクトリ名を入れる配列
		//詳細：MAX_PATHという　Window側の標準で決められた　ファイルパスにおける最大文字数
	char defaultCurrentDir[MAX_PATH];						
	//現在のカレントディレクトリ取得（Project/）
	//現在のカレントディレクトリになっているパスを保存	
		//引数：取得文字数
		//引数：格納先アドレス
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);		


	//カレントディレクトリの変更
		//★SetCurrentDirectoryをすることで、ファイルをロードするときに　そのカレントディレクトリからの相対パスを指定する必要が出てくる
		//Porject/Assets/ をカレントディレクトリとしたら、　Project/Assets/以下からの相対パスによってファイルパスを指定する必要が出てくる
	SetCurrentDirectory(dir);

	/*
		以下のバッファの作成で、
		失敗してしまうかもしれない
		→失敗したらSetCurrentDirectoryが変わった状態で、returnを返してしまう

		なので、SetCurrentなど、マネージャーの解放などはreturn前に必ず行わなければいけない
		→失敗したら、その時々にSetCurrentDirectoryの処理を書くでもいいし
		 
		TryCatchでも
		※それらのエラー処理の書き方は、他の人のソースを見てみる
	*/


	/*頂点情報から　各バッファの作成****************************************************************************************/

	//頂点バッファの作成を行う（引数のメッシュより）
	hr = InitVertex(mesh);
	//バッファ準備失敗
	if (FAILED(hr))
	{
		//カレントディレクトリ戻す
			//終わったら戻す
		SetCurrentDirectory(defaultCurrentDir);
		//マネージャ解放
			//マネージャーを解放することで、上記の諸々（ノードなど）が解放されるようになっているので、マネージャーだけ解放
		pFbxManager->Destroy();
	};
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");
		

	//インデックスバッファの作成（引数メッシュより）
	hr = InitIndex(mesh);
	//バッファ準備失敗
	if (FAILED(hr))		
	{
		SetCurrentDirectory(defaultCurrentDir);
		pFbxManager->Destroy();
	};
	//エラーメッセージ
	ERROR_CHECK(hr, "インデックスバッファの作成失敗", "エラー");


	//コンスタントバッファの作成
	hr = InitConstantBuffer();
	//バッファ準備失敗
	if (FAILED(hr))		
	{
		SetCurrentDirectory(defaultCurrentDir);
		pFbxManager->Destroy();
	};
	//エラーメッセージ
	ERROR_CHECK(hr, "コンスタントバッファの作成失敗", "エラー");


	//マテリアルの取得（ノードより）
		//詳細：マテリアルはノードに入っている→メッシュには入っていないのでノードを送ってその中から取得する
	hr = InitMaterial(pNode);
	//マテリアル取得失敗
	if (FAILED(hr))
	{
		SetCurrentDirectory(defaultCurrentDir);
		pFbxManager->Destroy();
	};
	//エラーメッセージ
	ERROR_CHECK(hr, "マテリアルの取得失敗", "エラー");


	/*処理の終了　カレントディレクトリを元に戻す*****************************************************************************/

	//カレントディレクトリを戻す
		//単純に、変更を加える前のパスを保存しておいたため、そのパスをセットして戻す
		//終わったら戻す
	SetCurrentDirectory(defaultCurrentDir);


	/*解放******************************************************************************************************************/

	//マネージャ解放
		//マネージャーを解放することで、上記の諸々（メッシュ、ノード）が解放されるようになっているので、マネージャーだけ解放
	pFbxManager->Destroy();


	//処理の成功
	return S_OK;
}


//頂点バッファの作成
	//引数においてメッシュを受け取り、FBXファイルから頂点情報を取得して、
	//頂点バッファを作成する
HRESULT Fbx::InitVertex(fbxsdk::FbxMesh * mesh)
{
	//頂点情報を入れる配列のポインタ
		//頂点数分確保
	pVertices_ = new VERTEX[vertexCount_];


	//FBXより頂点情報の取得
		//ポリゴン単位で頂点情報が格納されているため
		//ポリゴン単位で頂点情報を取得 
	//条件：ポリゴンの枚数分
	for (DWORD poly = 0; poly < (DWORD)(polygonCount_); poly++)
	{
		//3頂点ごとに情報取得
			//3頂点分（ポリゴンは三角形の3頂点で作っているため）
		//条件：3頂点分
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//全体における頂点番号取得（全体から見た頂点番号）
				//詳細：３番目のポリゴンの、何番目の頂点→とすれば、頂点の順番（頂点情報が存在するFBX情報から、全体の上から何番目の頂点だ）の位置が出てくる
				//引数：ポリゴン番目（モデルを作るポリゴン（0オリジン）の何番目）
				//引数：頂点番目（ポリゴンを作る頂点（0オリジン）の何番目）
			int index = mesh->GetPolygonVertex(poly, vertex);

			//頂点の位置（ローカル座標）
				//頂点番号から頂点の座標取得
				//頂点が入っているFBXの情報から、index番目のベクトルを取得
			FbxVector4 pos = mesh->GetControlPointAt(index);
			
			//頂点情報における座標を登録
				//詳細：ローカル座標をメンバの頂点情報格納先に登録 
				//　　：　　：pVertices_[index].position = XMVectorSet((float)-pos[0], (float)pos[1], (float)pos[2], 0.0f);
				//　　：　　：FBXは左右反転になる前提なので、マイナスにして、左右反転の左右反転になる
				//　　：　　：だが、法線情報は、これにすることで、→法線情報を反転させなくてはいけなくなるので、
				//　　：　　：★左右反転が前提なのであれば、→ここでーにしないのも手

				//詳細：格納順番、ポリゴンの表裏の関係で格納にひと手間かける必要がある↑
				//　　：しかし、接戦の情報を格納することを考えると、頂点情報が逆になる（マイナス方向で格納）することは避けたい
			pVertices_[index].position = XMVectorSet((float)pos[0], (float)pos[1], (float)pos[2], 0.0f);
	

			//※頂点情報の格納方法にのっとり、UVの格納もポリゴンの裏表を考慮した格納方法を行う
			
			//頂点のUVを登録
			//メッシュからUV格納先を取得
			FbxLayerElementUV * pUV = mesh->GetLayer(0)->GetUVs();
			//UVの番号を取得（全体から見たUV座標の番号）（頂点番号と同じ）
				//引数：ポリゴン番目
				//引数：頂点番目
			int uvIndex = mesh->GetTextureUVIndex(poly, vertex, FbxLayerElement::eTextureDiffuse);
			//UV値を取得（ベクトル）
			FbxVector2  uv = pUV->GetDirectArray().GetAt(uvIndex);
			//頂点情報におけるUVを登録
			pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0f - uv.mData[1]), 0.0f, 0.0f);


			//頂点の法線を登録
			FbxVector4 Normal;
			//法線ベクトルの取得
				//引数：ポリゴン番目
				//引数：頂点番目
				//引数：法線ベクトル（参照渡し）
			mesh->GetPolygonVertexNormal(poly, vertex, Normal);
			//頂点情報における法線を登録
			pVertices_[index].normal = XMVectorSet((float)Normal[0], (float)Normal[1], (float)Normal[2], 0.0f);
		}
	}

	///////////////////////////頂点のＵＶ/////////////////////////////////////
		//座標の格納情報において、格納順とUVの登録順をそろえる
	//UV個数取得
	int m_dwNumUV = mesh->GetTextureUVCount();
	//メッシュからUV格納先を取得
	FbxLayerElementUV* pUV = mesh->GetLayer(0)->GetUVs();
	if (m_dwNumUV > 0 && pUV->GetMappingMode() == FbxLayerElement::eByControlPoint)
	{
		for (int k = 0; k < m_dwNumUV; k++)
		{
			FbxVector2 uv = pUV->GetDirectArray().GetAt(k);
			pVertices_[k].uv = XMVectorSet((float)uv.mData[0], (float)(1.0f - uv.mData[1]), 0.0f, 0.0f);
		}
	}

	///////////////////////////頂点の接線/////////////////////////////////////
		//座標の格納情報において、格納順と法線の登録順をそろえて格納
	//条件：ポリゴン数分
	for (int i = 0; i < polygonCount_; i++)
	{
		//ポリゴン番号から頂点番号へのインデックス取得
		int startIndex = mesh->GetPolygonVertexIndex(i);

		//メッシュから接戦格納先を取得
		FbxGeometryElementTangent* t = mesh->GetElementTangent(0);


		//Tangentがない場合を除いて
		if (t != nullptr)
		{
			//接戦ベクトルを取得
			FbxVector4 tangent = t->GetDirectArray().GetAt(startIndex).mData;

			//条件：ポリゴンを作る3頂点分
			for (int j = 0; j < 3; j++)
			{
				//頂点番号取得
				int index = mesh->GetPolygonVertices()[startIndex + j];
				//頂点情報から接戦情報を登録
				pVertices_[index].tangent = XMVectorSet((float)tangent[0], (float)tangent[1], (float)tangent[2], 0.0f);
			}
		}
	}



	//頂点バッファ作成

	/*
		頂点情報の配列のサイズ

		//verticesはポインタで動的配列になったので、sizeof(vertices)では正しくサイズを取得することができない。
			//	この場合はVERTEX型のサイズ*頂点数を指定すればよい。
				//頂点の数はわかっているので、→vertexの型でいくらのサイズをbyteで使うのか、＊頂点数→最終的に使用する全体サイズ

	*/
	
	//バッファの詳細情報を格納する構造体
		//頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;

	//頂点バッファの使用するバイト長を登録
		//頂点情報のサイズの登録
		//型名＊頂点数（頂点数だけ、VERTEXを取得している）（VERTEXの情報が何個必要なのか）
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;


	//バッファへのアクセスポインタ
		//頂点情報のリソースを入れる構造体
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = pVertices_;


	//頂点バッファの作成
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点バッファの作成失敗", "エラー");

	//処理の成功
	return S_OK;
}



//インデックスバッファ準備
HRESULT Fbx::InitIndex(fbxsdk::FbxMesh * mesh)
{
	/*
		ポインタのポインタにしている理由

		インデックス情報をポインタ確保　→ポインタ
		上記のポインタをマテリアルごとに確保　→ポインタのポインタ

		※ポリゴンを描画するとき、色、テクスチャであるマテリアルを選択して、
		　各々描画する必要がある。
		  そのためマテリアルごとに描画タイミングを変えて描画するために描画に必要なインデックス情報を分ける。

	*/

	//バッファーの動的確保
	//ポインタのポインタ
		//なのでポインタの配列を動的確保
	ppIndexBuffer_ = new ID3D11Buffer*[materialCount_];

	/*
		ポリゴンの作られ方とインデックス情報


		//インデックス情報を登録するint型配列の準備
		//indexには、どの頂点３つが、三角形になっているのかを頭から３つずつ登録していくもの（頂点には、各々番号が振られているため、ポリゴンを作る頂点3つの頂点番号を登録）
			//　＝　ポリゴンの枚数＊　３（３角化したため）（三角形を示す頂点を並べていくため、ポリゴン＊３で示す（頂点数よりも多くなる））


	
	*/


	//インデックス情報数
		//マテリアルごとに要素数（最終的にそのマテリアルごとの、インデックス情報の要素　＝　マテリアルで使用されている頂点数）
	indexCountEachMaterial_ = new int[materialCount_];	//マテリアル数で要素を確保
	//初期化
	for (int i = 0; i < materialCount_; i++)
	{
		indexCountEachMaterial_[i] = 0;
	}
		
	//インデックス情報を格納する領域の動的確保
		//★マテリアルごとのインデックス情報
		//バッファーと同様に
	ppIndex_ = new int*[materialCount_];


	//マテリアルごとに
	//インデックス情報の確保 
		//条件：マテリアルの数分だけループ
		//（マテリアルごとにインデックスバッファを分ける、インデックス情報を分ける）
	for (int i = 0; i < materialCount_; i++)
	{
		//インデックス情報
			//ポリゴンを作る頂点番号の格納先
			//動的確保
			//インデックス情報は、「ポリゴンを作る」「インデックス情報」の確保であるため、ポリゴン数＊３確保しておけば。左記以上に要素が確保されることはないだろうとして余分に確保
		ppIndex_[i] = new int[polygonCount_ * 3];

		//カウンター
		int count = 0;


		//条件：ポリゴンごと
			//マテリアルをマテリアルごとに、分けてインデックス情報を分ける
		for (DWORD poly = 0; poly < (DWORD)(polygonCount_); poly++)
		{

			//マテリアルの情報を見る
			FbxLayerElementMaterial *   mtl = mesh->GetLayer(0)->GetMaterials();
			//マテリアル番号取得
			//今見ているpoly番目は、何番目のマテリアルなのか、
			int mtlId = mtl->GetIndexArray().GetAt(poly);

			//条件：現在参照しているマテリアルの番号がポリゴンごとのマテリアル番号と等しいか
			//今取得したマテリアルの番号が、青だったら、青を登録するように以下の処理に進む
			//変数のiは、マテリアル数分回っている→今欲しいマテリアル番号iと、取得したマテリアルの番号とが、同じなら、登録しなくてはいけないインデックス情報なので、
			if (mtlId == i)
			{
				//条件：3頂点分
				//★　頂点を反転しなかったので、　そのままのインデックスで登録
					//上記で反転していたのは、頂点を反時計回りに取得していたため、反転させていた
				for (int vertex = 0; vertex < 3; vertex++)
				{
					//一次元目：マテリアル
					//二次元目：インデックス情報の添え字
						//引数：ポリゴン番目
						//引数：頂点番目
					ppIndex_[i][count] = mesh->GetPolygonVertex(poly, vertex);
					count++;	//配列のサイズを取得するために、要素数を取得するためのカウント
								//また、動的確保したインデックス情報を登録する配列の添え字としても使用する。
					
				}
			}
		}
		//マテリアルごとのインデックス数格納
		indexCountEachMaterial_[i] = count;



		//バッファの詳細情報を格納する構造体
		//インデックス情報の設定
		D3D11_BUFFER_DESC   bd;
		bd.Usage = D3D11_USAGE_DEFAULT;

		//インデックス情報に登録した数分だけサイズ取得（マテリアルごとに配列を回しているので）
			//サイズを、indexのサイズ分だけ、（インデックス情報の入っている配列のバイトサイズだけ）
			//for分内にて、カウントした変数をかけて、型＊要素数とする
		bd.ByteWidth = sizeof(int) * count;	
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;



		//バッファーへのアクセスポインタ
		D3D11_SUBRESOURCE_DATA InitData;

		//�@マテリアルごとのインデックス情報を渡さないといけないので、今回のインデックス情報のみを渡してあげる
		InitData.pSysMem = ppIndex_[i];	
		InitData.SysMemPitch = 0;
		InitData.SysMemSlicePitch = 0;


		//インデックスバッファの作成
		HRESULT hr = Direct3D::pDevice->CreateBuffer(&bd, &InitData, &ppIndexBuffer_[i]);
		//エラーメッセージ
		ERROR_CHECK(hr, "インデックスバッファ作成失敗", "エラー");


	}


	//処理の成功
	//完全に処理の終えた（バッファを作成し終えたのちに）消去
		//CreateBuffer時点でindex(ポインタ)を登録したInitDataを使用しているので、indexの領域は参照されている→その段階で解放されてしまっていたら、エラーが起こる可能性もある。
		//SAFE_DELETE(ppIndex_);//ポインタの解放
	return S_OK;

}

//コンスタントバッファの作成
HRESULT Fbx::InitConstantBuffer()
{
	//バッファの詳細情報を格納する構造体
	//コンスタントバッファ情報の設定
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//コンスタントバッファを作成する
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_);
	//エラーメッセージ
	ERROR_CHECK(hr, "コンスタントバッファ作成失敗", "エラー");


	//処理の成功
	return S_OK;

};

//マテリアルの確保
HRESULT Fbx::InitMaterial(fbxsdk::FbxNode* pNode)
{
	//マテリアルを入れる配列の宣言
		//マテリアルの数分だけ要素を取得→materialCountより
	pMaterialList_ = new MATERIAL[materialCount_];

	//条件：マテリアル数分だけ繰り返す
	for (int i = 0; i < materialCount_; i++)
	{
		/*
		//マテリアル１つ１つを見て、
		//textureが張られているか、貼られていないかを見ていく
		*/



		//マテリアル格納情報を取得
		// i番目のマテリアル情報を取得
		//１つのマテリアルの情報を持ってくる
		FbxSurfaceMaterial* pMaterial = pNode->GetMaterial(i);	


		/*
			テクスチャの有無関係なく取得する情報
		*/
		/*通常色（ディフューズ）*************************************/
		//マテリアルの色
		
		//フォンのマテリアルポインタの取得
			//詳細：フォンにキャスト
			//　　：この時点ではフォンをMAYA上で使ったどうかは気にしていない
		FbxSurfacePhong* pPhong = (FbxSurfacePhong*)pMaterial;

		//マテリアルのそのものの色を取得
		//マテリアルのポインタの中のDiffuse。
		FbxDouble3  diffuse = pPhong->Diffuse;

		//マテリアルの構造体変数へ
			//上記で取得した色を格納
		pMaterialList_[i].diffuseColor = XMFLOAT4((float)diffuse[0], (float)diffuse[1], (float)diffuse[2], 1.0f);

	
		/*環境光（アンビエント）******************************/
		//環境光のマテリアル情報取得
		FbxDouble3 ambient = pPhong->Ambient;
		//マテリアルの構造体変数へ　
			//上記で取得した色情報を取得
		pMaterialList_[i].ambientColor = XMFLOAT4((float)ambient[0], (float)ambient[1], (float)ambient[2], 1.0f);

		/*鏡面反射情報の初期化********************************/
		pMaterialList_[i].specularColor = XMFLOAT4(0, 0, 0, 0);
		pMaterialList_[i].shininess = 1;


		/*鏡面反射（スペキュラー）******************************/
			//詳細：スペキュラーがかかっているかの判定が必要
			//　　：ラバート（通常のマテリアル）のマテリアルからスペキュラーを取得するとエラーになるので注意
			//　　：フォンの場合
			//　　：FbxSurfaceLambert* pMaterial = (FbxSurfaceLambert*)pNode->GetMaterial(i);
			//　　：Lambertの部分をPhongにしなければいけない
			//　　：フォンシェーディングだったら
			//　　：フォンシェーディングを選んだのか、番号が帰って来る
			//　　：フォンを使ったとなったならスペキュラーの情報を取得したい
		//条件：フォンが使用されているならば
		if (pMaterial->GetClassId().Is(FbxSurfacePhong::ClassId))
		{
			//フォンのマテリアルがあるなら
			//フォンの中のスペキュラーなどの情報を取得
			FbxDouble3 specular = pPhong->Specular;

			//シェーダー内にて計算していたスペキュラー情報を取得
			pMaterialList_[i].specularColor = XMFLOAT4((float)specular[0], (float)specular[1], (float)specular[2], 1.0f);
			pMaterialList_[i].shininess = (float)pPhong->Shininess;
		}

	
		//処理の結果を入れる変数
		HRESULT hr = S_OK;
		
		/*Diffuse（通常色）用のテクスチャのロード**********************************/
			//Deffuse用のテクスチャを取得
			//テクスチャが何枚張られているか
			//張られていたら、テクスチャ名をロードする
				/*ノーマルマップでも、同じことをやって、テクスチャをロードすればよい*///必要部分をコピーすればよい
		{
			//ディフューズテクスチャの取得
				//張られている普通のテクスチャの取得
			hr = LoadDiffuseTexture(i, pMaterial);
			//エラーメッセージ
			ERROR_CHECK(hr, "ディフューズテクスチャのロード失敗", "エラー");
		}
		/*ノーマルマップ用(疑似でこぼこ)のテクスチャのロード*/
		{
			//ノーマルテクスチャの取得
			hr = LoadNormalMapTexture(i, pMaterial);
			//エラーメッセージ
			ERROR_CHECK(hr, "ノーマルマップテクスチャのロード失敗", "エラー");
		}
			
	}

	//処理の成功
	return S_OK;
}

//ノーマルマップのテクスチャが存在するときロード
HRESULT Fbx::LoadNormalMapTexture(int i, FbxSurfaceMaterial* pMaterial)
{

	//テクスチャ情報
		//詳細：プロパティを探す
		//　　：ノーマルマップ用のテクスチャを探すため　::sNormalMap
	FbxProperty  lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sNormalMap);
		//ノーマルマップ＝RGBの色により、偽の凹凸をつけさせる
		//白に近いところは明るく、黒に近いところは暗くさせるように明暗をつけさせることで　描画させることで、疑似の凹凸をつける


	//一つのマテリアルにつけてある
	//テクスチャの数数
		//textureの貼り付けた枚数→普通１枚だが、高解像度で、顔と体を別テクスチャで、枚数を分けることがある（１つのモデルに対して）
	int fileTextureCount = lProperty.GetSrcObjectCount<FbxFileTexture>();


	//テクスチャあり
	//　＝　テクスチャの枚数が０以外であったら、
	//　＝　テクスチャあり　＝　１以上　＝　真
	//　＝　テクスチャなし　＝　０　　　＝　偽
	if (fileTextureCount)
	{
		//テクスチャをロード

		//テクスチャファイルの格納領域を示す
			//テクスチャのファイル名を取得
			//テクスチャのソースから
		FbxFileTexture* textureInfo = lProperty.GetSrcObject<FbxFileTexture>(0);
		//テクスチャファイルパスの取得の取得
		const char* textureFilePath = textureInfo->GetRelativeFileName();
			/*
				//標準では、パスが間違いでエラーになる
				//原因：示されているフォルダが違う（Assetsから読み込みたい）
				//解決：画像が入っているフォルダから読み込みたい（Assets下からとも限らない）
				//ルール：FBXと同じ位置にテクスチャファイルを置くとした
				//解決：LoadでFBXファイルの頭のフォルダと同様の位置がもらえればいい
						//SetCurrentDirectoryをすることで、ファイルをロードするときに、それ以下から、呼びこむようになる（以降のファイルは、そのパス以下から読み込むようになる）
			*/


		//ファイル名をディレクトリからのフルパスにしないために
			//SetCurrentDirectoryにて、
			//カレントディレクトリを変更した
			//ファイル名だけ参照できるようになっている
			//_splitpath_s にてファイル名を取得

		//ファイル名
		char name[_MAX_FNAME];	
		//拡張子
		char ext[_MAX_EXT];	
		
		//ファイルパスからファイル名と拡張子を取得
		_splitpath_s(textureFilePath, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
		//ワイド文字のファイル名変数へ格納
			//printfと同じ扱い方で、printfの結果を第一引数の変数へ代入する
			//ファイル名変数　＝　ファイル名＋拡張し
		wsprintf(name, "%s%s", name, ext);	


		//ファイルからテクスチャ作成
			//テクスチャのクラスに、画像ファイルを読み込むための初期化を書けばいい（使用されているテクスチャをテクスチャクラスにテクスチャを作成）
		//MATERIAL構造体内のTextureクラスを作成
		pMaterialList_[i].pNormalTexture = new Texture;
		//Textureクラスに画像ファイルをロードする（ファイル名を指定して）(ファイル名は上記にて取得した)
		HRESULT hr = pMaterialList_[i].pNormalTexture->Load(name);
		//エラーメッセージ
		ERROR_CHECK(hr, "ファイル読み込み失敗", "エラー");
	}
	//条件：テクスチャ無し
	else
	{
		//テクスチャがない場合は、
		//MATERIALクラスのpTextureを nullにする
		pMaterialList_[i].pNormalTexture = nullptr;	//ポインタヌル

		//テクスチャが存在しないということは、
		//マテリアルそのものの色を取得させればよい
	}

	//処理の成功
	return S_OK;
}


//通常のテクスチャのロード
HRESULT Fbx::LoadDiffuseTexture(int i, FbxSurfaceMaterial * pMaterial)
{


	//テクスチャ情報
		//詳細：プロパティを探す
		//　　：ディフューズのテクスチャを探す
		//　　：ディフューズ　＝　拡散反射光
		//　　：赤色のものが赤く見えるのはなぜ？　　赤い光を全方向に反射している質感があって、その反射の光を見ているから赤く見える（つまりものそのものの色ということ）
	FbxProperty  lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sDiffuse);



	//一つのマテリアルにつけてある
	//テクスチャの数数
		//textureの貼り付けた枚数→普通１枚だが、高解像度で、顔と体を別テクスチャで、枚数を分けることがある（１つのモデルに対して）
	int fileTextureCount = lProperty.GetSrcObjectCount<FbxFileTexture>();


	//テクスチャあり
	//　＝　テクスチャの枚数が０以外であったら、
	//　＝　テクスチャあり　＝　１以上　＝　真
	//　＝　テクスチャなし　＝　０　　　＝　偽
	if (fileTextureCount)
	{
		//テクスチャをロード

		//テクスチャファイルの格納領域を示す
			//テクスチャのファイル名を取得
			//テクスチャのソースから
		FbxFileTexture* textureInfo = lProperty.GetSrcObject<FbxFileTexture>(0);
		//テクスチャファイルパスの取得の取得
		const char* textureFilePath = textureInfo->GetRelativeFileName();
		/*
			//標準では、パスが間違いでエラーになる
			//原因：示されているフォルダが違う（Assetsから読み込みたい）
			//解決：画像が入っているフォルダから読み込みたい（Assets下からとも限らない）
			//ルール：FBXと同じ位置にテクスチャファイルを置くとした
			//解決：LoadでFBXファイルの頭のフォルダと同様の位置がもらえればいい
					//SetCurrentDirectoryをすることで、ファイルをロードするときに、それ以下から、呼びこむようになる（以降のファイルは、そのパス以下から読み込むようになる）
		*/


		//ファイル名をディレクトリからのフルパスにしないために
			//SetCurrentDirectoryにて、
			//カレントディレクトリを変更した
			//ファイル名だけ参照できるようになっている
			//_splitpath_s にてファイル名を取得

		//ファイル名
		char name[_MAX_FNAME];
		//拡張子
		char ext[_MAX_EXT];

		//ファイルパスからファイル名と拡張子を取得
		_splitpath_s(textureFilePath, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
		//ワイド文字のファイル名変数へ格納
			//printfと同じ扱い方で、printfの結果を第一引数の変数へ代入する
			//ファイル名変数　＝　ファイル名＋拡張し
		wsprintf(name, "%s%s", name, ext);


		//ファイルからテクスチャ作成
			//テクスチャのクラスに、画像ファイルを読み込むための初期化を書けばいい（使用されているテクスチャをテクスチャクラスにテクスチャを作成）
		//MATERIAL構造体内のTextureクラスを作成
		pMaterialList_[i].pTexture = new Texture;
		//Textureクラスに画像ファイルをロードする（ファイル名を指定して）(ファイル名は上記にて取得した)
		HRESULT hr = pMaterialList_[i].pTexture->Load(name);
		//エラーメッセージ
		ERROR_CHECK(hr, "ファイル読み込み失敗", "エラー");


	}
	//条件：テクスチャ無し
	else
	{
		//テクスチャがない場合は、
		//MATERIALクラスのpTextureを nullにする
		pMaterialList_[i].pNormalTexture = nullptr;	//ポインタヌル

		//テクスチャが存在しないということは、
		//マテリアルそのものの色を取得させればよい
	}

	//処理の成功
	return S_OK;

}


//ポリゴンクラスのポリゴンの描画を行う
HRESULT Fbx::Draw(Transform & transform)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	//シェーダーを選択
		//自身のシェーダーをセット
	Direct3D::SetShaderBundle(thisShader_);



	//頂点情報のセット
	{
		//頂点バッファをセット（頂点を持っておくための領域（確保場所））
			//詳細：複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		//1頂点のサイズを指定する
			//詳細：位置のみであれば、XMVECTORの構造体であったが
			//　　：位置やUVも確保するため、頂点情報の構造体のサイズを受け取らせる
		UINT stride = sizeof(VERTEX);
		UINT offset = 0;
		//頂点バッファのセット
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

	}



	{

		//マテリアルごとに
		//インデックス情報を取得し、ポリゴンの描画
		for (int i = 0; i < materialCount_; i++)
		{
			//サイズの更新
			//詳細：インデックスバッファに登録している情報は、int情報の配列
			//　　：そのため、intのサイズを渡す
			UINT stride = sizeof(int);
			UINT offset = 0;


			//インデックスバッファのセット
				//それぞれのインデックスバッファを配列から添え字にて指定し、送る
			Direct3D::pContext->IASetIndexBuffer(ppIndexBuffer_[i], DXGI_FORMAT_R32_UINT, 0);


	

			//シェーダーにおけるコンスタントバッファへわたす情報をまとめる
				//詳細：自クラスの　CONSTANT_BUFFER(sturct)とシェーダーファイルにおけるコンスタントバッファの構造体　の格納数、格納情報を等しくしているため、
				//　　：伴い、描画に必要な現在の情報をCONSTANT_BUFFER(sturct)　に格納して、最終的にシェーダーファイルに渡す

			//コンスタントバッファ　格納情報
			CONSTANT_BUFFER cb;

			//コンスタントバッファ　へのデータを格納（準備）
			{
				//ワールド　＊　ビュー　＊　プロジェクション行列
					//詳細：自身Transformのワールド行列
					//　　：カメラからのビュー行列
					//　　：カメラからのプロジェクション行列
					//　　：上記を使い→ローカル座標をプロジェクション座標へ変換する
					//詳細：プロジェクション座標となった座標をシェーダー内にてビューポート行列と書けることでスクリーン座標に変換される
				cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix()* Camera::GetViewMatrix() * Camera::GetProjectionMatrix());

				//ワールド行列
					//詳細：法線の回転のために必要なワールド行列
					//　　：ライトは固定なのに　陰も一緒に回ってしまう→これは、法線が回っていないことで法線が常に同じ方向を向き続けているため
					//　　：法線も同様に回転させる必要がある　
					//　　：しかし法線の回転には、移動行列×、回転行列、拡大行列の逆行列　が必要となる
				cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));	

				//ワールド行列
					//詳細：ハイライト表示のためにカメラのワールド座標を求める必要がある 
					//　　：頂点から　カメラへの向きというのを取得するために、頂点に掛けてやらないといけない（移動、回転、拡大を込みしたワールド位置にするために）
				cb.matWorld = XMMatrixTranspose(transform.GetWorldMatrix());

				//カメラの座標を入れる
					//詳細：XMVECTOR型をFLOAT4型に入れることはできない。
					//　　：変換する関数があったが、忘れてしまったので、1つずつFLOAT４型に変換して代入
				cb.camPos = XMFLOAT4(Camera::position_.vecX, Camera::position_.vecY, Camera::position_.vecZ, 0);


				//テクスチャ情報を渡す処理
					//テクスチャがある場合、シェーダーへテクスチャのサンプラーとリソースを渡す必要がある
					//そして、テクスチャがあることをフラグとして示す必要がある
				{

					//条件：テクスチャがある
					if (pMaterialList_[i].pTexture != nullptr)
					{

						//nullptrでない　＝　テクスチャが存在する
							//「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
							//サンプラーは、どこに貼り付けますか？などの情報
							//hlsl : g_sampler(s0)
						ID3D11SamplerState* pSampler = pMaterialList_[i].pTexture->GetSampler();
						//第一引数：サンプラー　番目
						Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

						//シェーダーへの橋渡しのビューを取得
						//テクスチャのリソース部分
							//hlsl : g_texture(t0)
						ID3D11ShaderResourceView* pSRV = pMaterialList_[i].pTexture->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
							//第一引数：テクスチャ　番目
						Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);

						//テクスチャ　有
						cb.isTexture = TRUE;
					}
					//条件：テクスチャなし
					else
					{

						//nullptrである　＝　テクスチャが存在しない
							//　＝　マテリアルそのものの色を使用しないといけない

						//テクスチャ　無
						cb.isTexture = FALSE;

					}


					//条件：ノーマルテクスチャがある
					if (pMaterialList_[i].pNormalTexture != nullptr)
					{
						//サンプラーは共通のものを使用するため、
						//ここでは何もしない
							//サンプラーとは、テクスチャをどのように読みますかという設定であるため
							//複数必要ない

						//テクスチャの橋渡しを送る
							//hlsl : NormalMap.hlsl
						ID3D11ShaderResourceView* pSRV = pMaterialList_[i].pNormalTexture->GetSRV();
						//シェーダーに作った変数に渡している
							//hlsl : g_normalTexture(t1)
							//第一引数：テクスチャ　番目
						Direct3D::pContext->PSSetShaderResources(1, 1, &pSRV);

					}

				}

				//テクスチャの有無関係なく
					//マテリアルのフォンの情報や、通常色の情報を格納
				{
					//現在のマテリアルの


					//diffuseのマテリアルの色
					cb.diffuseColor = pMaterialList_[i].diffuseColor;	
					//環境光
					cb.ambientColor = pMaterialList_[i].ambientColor;
					//鏡面色
					cb.supecularColor = pMaterialList_[i].specularColor;
					//光沢度
					cb.shininess = pMaterialList_[i].shininess;




				}


				//コンスタントバッファ1の確保
				{
					//シェーダーのコンスタントバッファ１のマップ
						//コンスタントバッファ1へのアクセス許可
					hr = Direct3D::GetShaderClass(thisShader_)->MapShaderConstantBuffer1View();
					//エラーメッセージ
					ERROR_CHECK(hr, "コンスタントバッファMap失敗", "エラー");


					//自身の使用する
						//シェーダー独自の
						//コンスタンバッファ１や、テクスチャの受け渡しを行う
						//処理は、シェーダークラスに一任する
					Direct3D::GetShaderClass(thisShader_)->ShaderConstantBuffer1View();


					//シェーダーのコンスタントバッファ１のアンマップ
						//コンスタントバッファ1へのアクセス拒否
					hr = Direct3D::GetShaderClass(thisShader_)->UnMapShaderConstantBuffer1View();
					//エラーメッセージ
					ERROR_CHECK(hr, "コンスタントバッファUnMap失敗", "エラー");
				}




				


			}

			//コンスタントバッファをシェーダーファイルへコピー、渡す
			{
				//バッファへのマップ設定
					//ポインタへのアクセスなどに使用する
				D3D11_MAPPED_SUBRESOURCE pdata;
				//コンスタントバッファへのマップ
					//詳細：アクセス許可
					//　　：シェーダーなどのGPUでしか、触れることのできないものに
					//　　：CPUから触れる準備
					//　　：マップを行い、バッファへのアクセスができるようになるが、その詳細のアクセスを上記pdataにて行う。
				hr = Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);
				//エラーメッセージ
				ERROR_CHECK(hr, "コンスタントバッファのマップの書き込み失敗", "エラー");


				//メモリにコピー
					//シェーダーファイル側のコンスタントバッファ（struct）へ 
					//データを値を送る
				memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));

			}

			//コンスタントバッファをセット
			{
				//コンスタントバッファをセット
				//頂点シェーダー用	
				Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);
				//ピクセルシェーダー用
				Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);

				//コンスタントバッファへのアンマップ
					//詳細：アクセス拒否
				Direct3D::pContext->Unmap(pConstantBuffer_, 0);

			}



			//描画
				//詳細　　：インデックス数を渡して、頂点によるポリゴンの描画を宣言 
				//第一引数：描画インデックス数　描画の対象となるポリゴンを作るインデックスの数（マテリアルごと）
			Direct3D::pContext->DrawIndexed(indexCountEachMaterial_[i], 0, 0);


		}

	}


	//処理の成功
	return S_OK;

}

//解放処理
void Fbx::Release()
{

	/*
	//コンスタントバッファなどの
		//ID3D11は、DELETEはしてはいけない
		SAFE_RELEASEを使用する（その中で、ポインタのクラスのさらにRelease()関数を呼ぶ（＋ポインタのアドレスは解放=nullptr）（ポインタを使えなくする））
	*/

	//ポインタ宣言順と逆に解放
	SAFE_RELEASE(pConstantBuffer_);	
	SAFE_DELETE_ARRAY(indexCountEachMaterial_);

	//ポインタのポインタ（複数個確保したポインタの）解放
	for (int i = 0; i < materialCount_; i++)
	{
		//MaterialList内のポインタの解放
		if (pMaterialList_[i].pTexture != nullptr) { pMaterialList_[i].pTexture->Release(); SAFE_DELETE(pMaterialList_[i].pTexture);};
		if (pMaterialList_[i].pTexture != nullptr) { pMaterialList_[i].pNormalTexture->Release(); SAFE_DELETE(pMaterialList_[i].pNormalTexture); };

		//まず、ポインタのポインタの→中身のポインタをすべて解放
		SAFE_DELETE_ARRAY(ppIndex_[i]);
		SAFE_RELEASE(ppIndexBuffer_[i]);//ID3D11型なので、Release
	}
	//そして最後に側のポインタを解放(ポインタのポインタ)
	SAFE_DELETE_ARRAY(pMaterialList_);	//配列のポインタを解放
	SAFE_DELETE_ARRAY(ppIndex_);
	SAFE_DELETE_ARRAY(ppIndexBuffer_);	//ポインタのポインタなので、中の要素である、ID3D11の解放はSAFE_RELEASEにて行い、
										//そのポインタは、DELETEにて解放（側は、ただの実体のないポインタである。）
	SAFE_DELETE_ARRAY(pVertices_);
	SAFE_RELEASE(pVertexBuffer_);
	

}


//レイキャスト
void Fbx::RayCast(RayCastData* rayData)
{
	//マテリアル毎
		//各ポリゴンとレイとの当たり判定
		//インデックス情報を、マテリアルごとに分けてある
	//条件：マテリアルごと
	for (DWORD i = 0; i < (DWORD)(materialCount_); i++)
	{
		//そのマテリアルのポリゴン毎
			//indexCountEachMaterial_ = マテリアルごとのインデックス情報の数。
			//　＝　３頂点ごと　＝　１ポリゴン（面）ごと
			//マテリアルごとのインデックス情報を / 3 = 面の枚数だけ回る
			//										 = jをうまく使って、連続するインデックス情報をもらわなければいけない。
		for (DWORD j = 0; j < (DWORD)(indexCountEachMaterial_[i]) / 3; j++)
		{

			//indexは、マテリアルごとに取得している
				//つまり、indexの行はi
			//列は、ポリゴン事取得するので、
			//インデックス情報は、三角形を構成する頂点が時計回りに順番に並んでいる。ということは０番め、１番目、２番目が１つの三角形を示す。→この流れがポリゴン数　毎　続く
			// j = 0 
				//０番目＝　j * 3
				//１番目＝　j * 3 +  1
				//２番目＝　j * 3 +  2
			//これでインデックス情報の頂点番号が取得できる
				//頂点番号を、頂点情報の引数にして、位置を取得
			// j = 1 
				//３番目＝　j * 3
				//４番目＝　j * 3 +  1
				//５番目＝　j * 3 +  2


			//3頂点の位置情報を取得
				//インデックス情報は
				//あらかじめ、時計回りで入っているため、格納順で頂点情報を取得
			XMVECTOR v0 = pVertices_[ppIndex_[i][(j * 3) + 0]].position;
			XMVECTOR v1 = pVertices_[ppIndex_[i][(j * 3) + 1]].position;
			XMVECTOR v2 = pVertices_[ppIndex_[i][(j * 3) + 2]].position;

			//衝突店までの距離
			float dist = 0.0f;

			//三角形を作る頂点とレイとの衝突判定
				//レイの情報は引数にてもらっているので、
				//その情報と、
				//三角形の頂点の情報を与えて、接触判定を行う。
			bool hit = Math::Intersect(
				rayData->start,			//レイ発射位置
				rayData->dir,			//レイの方向
				v0,						//三角形頂点０
				v1,						//三角形頂点１ 
				v2, 					//三角形頂点２
				&dist,					//レイと面の接触位置までの長さ
				rayData->permitNegativeDirection	//マイナス方向も考慮した衝突判定を行うか
			);


			//衝突していたら
			if (hit)
			{
				//当たったフラグを立てる
				rayData->hit = true;

				//条件：衝突時の衝突距離がレイ構造体に格納されている距離より小さい場合
				if (rayData->dist > dist)
				{
					//前回に当たった地点よりも短いとき更新
					rayData->dist = dist;

					//面の法線ベクトルを求める
					//�@ v1 - v0 = v0 から　v1 へのベクトル
					//�A v2 - v0 = v0 から　v2 へのベクトル
					//�B上記の2つの外積で、法線ベクトルを求める
					XMVECTOR vector1 = v1 - v0;
					XMVECTOR vector2 = v2 - v0;

					//面の法線方向を取得
					//法線＋方向
					rayData->normal = XMVector3Cross(vector1, vector2);	
					//rayData->normal = XMVector3Cross(vector2, vector1);	//法線ー方向
				}
			}
		}
	}
}

//引数ベクトルと、ポリゴンクラス（PolygonGroup＊）と衝突した面の法線ベクトルを取得
XMVECTOR Fbx::NormalVectorOfCollidingFace(XMVECTOR & start, XMVECTOR & dir)
{
	//レイ情報
	RayCastData rayData;
	rayData.start = start;
	rayData.dir = dir;

	//当たり判定を実行
		//レイと衝突判定にて、
		//まず、レイと衝突するポリゴンを判定
		//その判定ポリゴンの法線方向を取得
	RayCast(&rayData);

	//結果の法線ベクトルを返す
	return rayData.normal;
}


//コンスタントバッファを渡す
ID3D11Buffer * Fbx::GetConstantBuffer()
{
	return pConstantBuffer_;
}

//マテリアル情報構造体　コンストラクタ
Fbx::MATERIAL::MATERIAL() :
	pTexture(nullptr), pNormalTexture(nullptr),
	diffuseColor(XMFLOAT4(0.f, 0.f, 0.f, 0.f)),
	ambientColor(XMFLOAT4(0.f, 0.f, 0.f, 0.f)),
	specularColor(XMFLOAT4(0.f, 0.f, 0.f, 0.f)),
	shininess(0.f)
{
}

//頂点情報構造体　コンストラクタ
Fbx::VERTEX::VERTEX() :
	position(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	uv(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	normal(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	tangent(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}
