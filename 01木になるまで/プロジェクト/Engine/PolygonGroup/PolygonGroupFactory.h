#pragma once
#include "../../CommonData/PolygonGroupType.h"	//ポリゴングループ　２D3Dのポリゴンを扱うクラス群

//クラスのプロトタイプ宣言
class PolygonGroup;

/*
	クラス詳細	：ポリゴンクラスを作るクラス　ポリゴンクラスを作る工場
	使用デザインパターン：FactoryMethod
	クラス概要（詳しく）
				：
				・ポリゴンクラスを作成するだけのクラス。
				　作成を完全に請け負うことで、ポリゴンクラスを使うクラスにそのインスタンスを返す。
				　これにより、受け取り先に多くの情報を与える必要がなくなり生成、情報の機密性が上がると考える
				・FactoryMethodパターンを使用して、共通の親クラスを継承した
　　　　　　　　　派生クラスのインスタンス生成を請け負って、それを渡すクラス
				　Modelにて使用するポリゴンクラスのインスタンスを生成して返す

				  ModelのLoadにて、ポリゴンタイプが選択され、該当のポリゴンクラスを作成する

*/
class PolygonGroupFactory
{
//public メソッド
public:

	//ポリゴンクラス（PolygonGroup＊）のインスタンスを作成し、インスタンスを返す
		//詳細：引数にてもらった、ポリゴンクラスの識別タイプ（POLYGON_GROUP_TYPE）をもとに、該当するポリゴンクラス（PolygonGroup＊）を作成する。
		//　　：作成後、シェーダークラスの抽象クラスへキャストして返す。
		//　　：これにより、関数呼び出し先はPolygonGroup＊のインクルードのみ行えれば、受け取りが可能になるので、余分にインクルードを行う必要がない。
		//引数：ポリゴンクラス識別タイプ(enum)
		//戻値：ポリゴンクラスインスタンス（生成できた：インスタンスポインタ、　生成できなかった：nullptr）
	PolygonGroup* CreatePolygonGroupClass(POLYGON_GROUP_TYPE type);
};

