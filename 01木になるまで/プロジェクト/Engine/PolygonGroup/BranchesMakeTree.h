#pragma once
#include "PolygonGroup.h"	//ポリゴングループ　２D3Dのポリゴンを扱うクラス群

//クラスのプロトタイプ宣言
class Branch;
class Plane;

/*
	クラス詳細	：ポリゴンクラス　BranchesMakeTreeの結合元クラス（Branch,Planeが結合されるクラス）
	クラスレベル：サブクラス（スーパークラス（PolygonGroup））
	クラス概要（詳しく）
				：頂点情報を持つクラス（Branch , Plane）を結合して、
				　一つの３Dポリゴンクラスとする　1つのポリゴンクラスの情報をもらって、
				　その情報を、自身（BranchesMakeTree）の所有しているポリゴンの情報と結合
				：結合情報（Branchの情報）
					・頂点情報（ローカル座標、UV、法線など）
					・インデックス情報（どの頂点番号で▲ポリゴンをつくるか）

				：結合によって変化する情報（BranchesMakeTreeの更新）
					・頂点バッファの更新
					・インデックスバッファの更新
*/
class BranchesMakeTree : public PolygonGroup
{
//protected 構造体
protected:
	/*
		構造体詳細	：コンスタントバッファ構造体
		構造体概要（詳しく）
				：シェーダーに渡すシェーダー内グローバル情報
				：シェーダーのコンスタントバッファと同じサイズ、同じ順番の定義を行う

		制限：コンスタントバッファ16バイト未満の場合、コンスタントバッファのポインタ作成の際に、サイズを最低16バイトで初期化をする必要がある
			：16の倍数でサイズが取られている必要がある
			：構造体は、構造体内の変数内にて、一番型サイズの大きいもののサイズの倍数でサイズが取られるようになる。
			：例：構造体のサイズ　31バイト、構造体内最大の型サイズ　4バイト　＝　構造体のサイズは4の倍数に拡張される。余分なサイズが取られる。
			：　：最終的に、構造体のサイズは一番近い4の倍数になるので、32バイトとなる。
			：以上のことから、コンスタントバッファ構造体のサイズを考える際に、構造体ないに、行列（32バイト）が存在していれば、自動的にサイズが32の倍数になる。＝16の倍数
	*/
	struct CONSTANT_BUFFER
	{
		//ワールド行列＊ビュー行列＊プロジェクション行列
			//詳細：スクリーン座標を求める際に使用する
		XMMATRIX	matWVP;
		//回転のためのワールド行列（移動行列なし）
			//詳細：法線の回転のために使用する
		XMMATRIX	matNormal;
		//ワールド行列　移動＊回転＊拡大　行列
			//詳細：ワールド座標として、ワールド内の距離を求める際に使用する
		XMMATRIX	matWorld;
		//カメラポジション（ワールド座標）
			//詳細：シェーダーの反射のため
		XMFLOAT4 camPos;

		//テクスチャが張られているか
		BOOL isTexture;
	};

	/*
	構造体詳細：頂点情報
	構造体詳細（詳しく）
			　：シェーダーファイルの頂点シェーダー、引数に渡す情報
	　　　　　：頂点ごとの情報、位置、UV,法線、接線をもち、最終的に頂点同士をつなげ、一つのポリゴンを作る。そのポリゴンの集合により任意の形を作る
	
	*/
	struct VERTEX
	{
		//位置情報（頂点の位置）
		XMVECTOR position;
		//UV情報（テクスチャの座標）
		XMVECTOR uv;
		//法線情報（頂点の向き）
		XMVECTOR normal;

		//コンストラクタ
		VERTEX();
	};


	//priavte メンバ変数、ポインタ、配列
private:

	//頂点数（FBXファイルより取得）
	int vertexCount_;
	//ポリゴン数（FBXファイルより取得）
	int polygonCount_;
	//マテリアルの個数
		//詳細：（MAYAにおける　マテリアルのLambert１、２などを入れるための数）（ノードに存在するマテリアルの個数）
		//　　：テクスチャも一つのマテリアルである
	int materialCount_;

	//インデックス数
	int indexCountEachMaterial_;

	//各頂点ごとの頂点情報を入れる配列
	VERTEX* pVertices_;

	//インデックス情報を登録しておく
	//動的確保した配列
	int* pIndex_;


	//頂点情報を登録したバッファー
	ID3D11Buffer* pVertexBuffer_;
	//頂点の順番、インデックス情報を持っておくバッファー
	ID3D11Buffer* pIndexBuffer_;
	//コンスタントバッファのバッファーポインタ
		//詳細：構造体で取得しているCONSTANT_BUFFERをシェーダーに渡す際のバッファー
		//　　：バッファーへ各構造体の情報を格納して、シェーダーへわたす
	ID3D11Buffer* pConstantBuffer_;

	//テクスチャ
	Texture* pTexture_;




//protected メソッド
protected:


	//頂点バッファ作成(Y座標を除く)
		//詳細：Planeを作るポリゴンをコードで記述し頂点情報を作成
		//　　：頂点情報を頂点バッファに登録
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT InitVertex();
	//インデックスバッファ作成
		//詳細：頂点情報からインデックス情報に登録
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT InitIndex();

	//頂点情報から頂点バッファーを作成
		//レベル：仮想関数
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT CreateVertexBuffer();
	//インデックス情報からインデックスバッファーを作成
		//レベル：仮想関数
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT CreateIndexBuffer();
	//コンスタントバッファの作成	
		//レベル：仮想関数
		//引数：なし
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT CreateConstantBuffer();

	//テクスチャのロード
		//引数：ファイル名
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT LoadTexture(const std::string& FILE_NAME = "");
	
//public メソッド
public:

	//コンストラクタ
		//引数：なし
		//戻値：なし
	BranchesMakeTree();

	//デストラクタ	
		//引数：なし
		//戻値：なし
	virtual ~BranchesMakeTree();


	//ロード
		//必ず呼ばれるロード、しかし意味を持たせないロード
		//引数：ファイル名
		//引数：シェーダータイプ
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT Load(const std::string& FILE_NAME , SHADER_TYPE thisShader);


	//初期化
		//詳細：モデル用のテクスチャと、コンスタントバッファの作成（他バッファの作成はソースBranchが結合された際に行う）
		//引数:テクスチャファイル名(素材モデルに共通して使うテクスチャ)
		//引数：シェーダ-タイプ
		//戻値：処理の成功（成功：S_OK、失敗：E_FAIL）
	HRESULT Initialize(const std::string& TEXTURE_FILE_NAME, SHADER_TYPE thisShader);




	//ポリゴンクラスのポリゴンの描画を行う
		//詳細：ポリゴンクラスに登録されたポリゴン情報を引数Transform位置へ描画する
		//　　：ポリゴンは、ポリゴンの頂点情報とポリゴンを作るインデックス情報により作られている
		//　　：各ポリゴン単位で頂点情報をシェーダーの頂点シェーダーに送ることで、ポリゴン事最終的にピクセルで出力される
		//引数：描画時の変形情報（移動、拡大、回転）（ローカル座標（メソッドにて、親も考慮したワールド座標を取得可能））
		//レベル：仮想関数
		//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	virtual HRESULT Draw(Transform& transform);


	//解放
		//レベル：仮想関数
		//引数：なし
		//戻値：なし
	virtual void Release();


	//３Dポリゴンクラス（Branch）を自身のモデルに結合
		//詳細：Branchポリゴンを自身に結合して　1つのポリゴンクラスにする
		//　　：引数にて渡された、３Dポリゴンクラスから、
		//　　：頂点情報、インデックス情報を取得し、自身の情報に結合する
		//引数：３Dポリゴンクラス（Branch（枝））
		//引数：出現位置のTransform（ローカル座標 各頂点を引数Transformのワールド行列で変形させる）
		//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	HRESULT Join3DPolygon(Branch* pBranch , Transform& trans);


	//３Dポリゴンクラス（Plane）を自身のモデルに結合
		//詳細：Planeポリゴンを自身に結合して　1つのポリゴンクラスにする
		//　　：引数にて渡された、３Dポリゴンクラスから、
		//　　：頂点情報、インデックス情報を取得し、自身の情報に結合する
		//引数：３Dポリゴンクラス（Branch（枝））
		//引数：出現位置のTransform（ローカル座標 各頂点を引数Transformのワールド行列で変形させる）
		//戻値：処理の結果（成功：S_OK、失敗：E_FAIL）
	HRESULT Join3DPolygon(Plane* pPlane, Transform& trans);





};



