#include <wincodec.h>
#include "Texture.h"		//ヘッダ
#include "Direct3D.h"		//Direct３D　

//必ず　Direct3Dより下にインクルード
	//理由は　
	// 　　：Direct3Dにて、<d3d11_1.h>をインクルードしているため、
	//守らないと何が起こるか
	//　　 ：D3DX11 の要素を d3d11_1.hで上書き、オーバーライドしようとしてエラーになる

	//制限：D3DX11は、古い型であるため、通常のインクルードでパスを通すことができない。
	//　　：それに伴って、D3DX11の型の新しくなったのが、 d3d11 や　d3d11_1などに当たる。　そのため、古い型のD3DX11を拡張しようとして、　後からd3d11_1をインクルードすると、エラーとなる。
	//　　：#include <C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\Include\D3DX11.h>	//波線が出てしまう
#include "../../Direct3D11/D3DX11.h"		//旧式のDirectX11
#include "../../CommonData/GlobalMacro.h"	//共通マクロ


//ライブラリのリンク
#pragma comment( lib, "WindowsCodecs.lib" )
#pragma comment( lib, "Direct3D11/D3DX11.lib")	//D3DX11.hのヘッダ―ファイルと同様に、プロジェクトフォルダからの相対パスにてライブラリをリンク


//コンストラクタ
Texture::Texture() : 
	pSampler_(nullptr) , 
	pSRV_(nullptr) , 
	imgWidth_(0) , 
	imgHeight_(0)
{
}

//デストラクタ
Texture::~Texture()
{
}

//ロード
HRESULT Texture::Load(const std::string& FILE_NAME)
{
	//引数の画像ファイル名から
		//画像情報をバッファーにコピーさせる
	//空のバッファー用意
	ID3D11Texture2D* pTextureBuffer = nullptr;
	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	{
		//ファイル名にて示される画像ファイルをバッファーにコピー
			//引数：ファイル名
			//引数：バッファーのポインタのアドレス
		hr = GetTextureBuffer(FILE_NAME, &pTextureBuffer);
		//エラーメッセージ
		ERROR_CHECK(hr, "画像のバッファーの作成失敗", "エラー");

		//サンプラー
			//テクスチャの張られ方の定義
			//テクスチャにおいて、テクスチャの縮小、拡大を許可するかなど
		D3D11_SAMPLER_DESC  SamDesc;
		//ゼロメモリー　初期化
		ZeroMemory(&SamDesc, sizeof(D3D11_SAMPLER_DESC));

		//テクスチャにおける色の補完を行うか
			//テクスチャの拡大、縮小の際に色を伸ばすか（ぼかすか、ぼかさないか）
			//LINEAR　＝　近くの色から　付近のピクセルはこの色だろうと、補完する。
			//　　　　＝　白と黒の間なら、灰色など
			//　　　　＝　色をぼかし、自然にしたい場合に用いる

			//POINT　 ＝　色をぼかさない
		SamDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;


		//テクスチャのアドレスコード
			//詳細：UV値が０〜１より小さいとき（拡大縮小を行うと、0以下の値が発生する）
			//　　：イメージとしては、0以下になると、同じテクスチャが0以下に繋がって、テクスチャの張られていない部分をなくそうとする。（つなぎ目は出てしまうが、）
			//　　：その際に、ゲーム内描画においては、上記のように0以下にテクスチャをつなげる形をとるのか（WRAP）。或いは、テクスチャをつなげずに一つのテクスチャを端まで伸ばしてしまう（CLAMP）。

		//U（UVにおける横）　横のテクスチャは伸ばすのか、伸ばさないのか　
			//CLAMP = 伸ばす
			//WRAP = 伸ばさない
			//MIRRER =　反転してくりかえす（UV0 ~ 1を張って、1から横につながるときは、テクスチャを1~0になるように並べる）
		SamDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		/*
			テクスチャを引き延ばす利点

			大きいモデルに小さいテクスチャを張ると、解像度が荒くなってしまう
			上記を避けるために、
			テクスチャを縮小し、何個も並べるようにすることで、ある程度の解像度の低さをごまかすことができる

		*/

		//V（UVにおける縦）　縦のテクスチャは伸ばすのか、伸ばさないのか　
		SamDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;

		//W
			//不明
		SamDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;

		//サンプラーの作成
		hr = Direct3D::pDevice->CreateSamplerState(&SamDesc, &pSampler_);
		//エラーメッセージ
		ERROR_CHECK(hr, "テクスチャのサンプラー生成失敗", "エラー");
	}

	{
		//シェーダーリソースビュー
			//シェーダーへの橋渡し（〜ビューは橋渡し）
		D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
		srv.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srv.Texture2D.MipLevels = 1;

		//シェーダーリソースビューの作成
		hr = Direct3D::pDevice->CreateShaderResourceView(pTextureBuffer, &srv, &pSRV_);
		//エラーメッセージ
		ERROR_CHECK(hr, "シェーダーリソースビュー生成失敗", "エラー");
	}

	//テクスチャバッファーの解放
	pTextureBuffer->Release();

	//処理の成功
	return S_OK;
}


//DirectX型のバッファーをもとにテクスチャのロード
	//Direct３Dで作られたID3D11型のテクスチャを、ゲームオブジェクトとして描画できるテクスチャに変更する。
HRESULT Texture::Load(ID3D11Texture2D* pTexture)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	{
		//サンプラー
			//テクスチャの張られ方の定義
			//テクスチャにおいて、テクスチャの縮小、拡大を許可するかなど
		D3D11_SAMPLER_DESC  SamDesc;
		//ゼロメモリー　初期化
		ZeroMemory(&SamDesc, sizeof(D3D11_SAMPLER_DESC));

		//テクスチャにおける色の補完を行うか
			//テクスチャの拡大、縮小の際に色を伸ばすか（ぼかすか、ぼかさないか）
			//LINEAR　＝　近くの色から　付近のピクセルはこの色だろうと、補完する。
			//　　　　＝　白と黒の間なら、灰色など
			//　　　　＝　色をぼかし、自然にしたい場合に用いる

			//POINT　 ＝　色をぼかさない
		SamDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;


		//テクスチャのアドレスコード
			//詳細：UV値が０〜１より小さいとき（拡大縮小を行うと、0以下の値が発生する）
			//　　：イメージとしては、0以下になると、同じテクスチャが0以下に繋がって、テクスチャの張られていない部分をなくそうとする。（つなぎ目は出てしまうが、）
			//　　：その際に、ゲーム内描画においては、上記のように0以下にテクスチャをつなげる形をとるのか（WRAP）。或いは、テクスチャをつなげずに一つのテクスチャを端まで伸ばしてしまう（CLAMP）。

		//U（UVにおける横）　横のテクスチャは伸ばすのか、伸ばさないのか　
			//CLAMP = 伸ばす
			//WRAP = 伸ばさない
			//MIRRER =　反転してくりかえす（UV0 ~ 1を張って、1から横につながるときは、テクスチャを1~0になるように並べる）
		SamDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		/*
			テクスチャを引き延ばす利点

			大きいモデルに小さいテクスチャを張ると、解像度が荒くなってしまう
			上記を避けるために、
			テクスチャを縮小し、何個も並べるようにすることで、ある程度の解像度の低さをごまかすことができる

		*/

		//V（UVにおける縦）　縦のテクスチャは伸ばすのか、伸ばさないのか　
		SamDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;

		//W
			//不明
		SamDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;

		//サンプラーの作成
		hr = Direct3D::pDevice->CreateSamplerState(&SamDesc, &pSampler_);
		//エラーメッセージ
		ERROR_CHECK(hr, "テクスチャのサンプラー生成失敗", "エラー");
	}

	{
		//シェーダーリソースビュー
			//シェーダーへの橋渡し（〜ビューは橋渡し）
		D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
		srv.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srv.Texture2D.MipLevels = 1;

		//シェーダーリソースビューの作成
		hr = Direct3D::pDevice->CreateShaderResourceView(pTexture, &srv, &pSRV_);
		//エラーメッセージ
		ERROR_CHECK(hr, "シェーダーリソースビュー生成失敗", "エラー");
	}
	//pTexture->Release();
		//Releaseを呼んでしまうと、　ID3D11で作成したテクスチャをリリースしてしまう、　テクスチャクラスのポインタ自体は、ここで解放したくない。

	//画像サイズを本来であれば、
		//適切なバッファのサイズから取得するべきだが、
		//取得するには専用のクラスなどが必要であるため、今回は省く
	imgWidth_ = Direct3D::scrWidth;
	imgHeight_ = Direct3D::scrHeight;


	//処理の成功
	return S_OK;
}

//CubeMap画像のロード
HRESULT Texture::LoadCube(const std::string& FILE_NAME)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;


	//stringで受け取ったものを、ワイド文字で受け取る
		//ファイル名を全部を2バイトとする、ワイド文字に変換
	wchar_t wtext[FILENAME_MAX];
	//ワイドのキャラ型
		//FILENAME_MAXは、ただの２６０という値→（２６０文字以上にファイル名が多くなることは内だろうという）
	
	size_t ret;
	mbstowcs_s(&ret, wtext, FILE_NAME.c_str(), FILE_NAME.length());

	//CubeMapのロード
	D3DX11_IMAGE_LOAD_INFO loadSMInfo;
	loadSMInfo.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;

	ID3D11Texture2D* SMTexture = 0;
	D3DX11CreateTextureFromFile(Direct3D::pDevice, FILE_NAME.c_str(),
		&loadSMInfo, 0, (ID3D11Resource**)&SMTexture, 0);

	D3D11_TEXTURE2D_DESC SMTextureDesc;
	SMTexture->GetDesc(&SMTextureDesc);

	D3D11_SHADER_RESOURCE_VIEW_DESC SMViewDesc;
	SMViewDesc.Format = SMTextureDesc.Format;

	//CubeMapと指定
	SMViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
	SMViewDesc.TextureCube.MipLevels = SMTextureDesc.MipLevels;
	SMViewDesc.TextureCube.MostDetailedMip = 0;

	//シェーダーリソースビューに渡している
	hr = Direct3D::pDevice->CreateShaderResourceView(SMTexture, &SMViewDesc, &pSRV_);
	//エラーメッセージ
	ERROR_CHECK(hr, "シェーダーリソースビュー生成失敗", "エラー");


	//処理の成功
	return S_OK;
}

//解放
void Texture::Release()
{
	//ID3D11型なので、RELEASEにて解放
	SAFE_RELEASE(pSRV_);
	SAFE_RELEASE(pSampler_);
}

//テクスチャのバッファーを取得(テクスチャ読み取り設定が必要ない場合)
HRESULT Texture::GetTextureBuffer(const std::string& FILE_NAME, ID3D11Texture2D** pBuffer)
{
	//テクスチャの読み方など
		//テクスチャの許可設定もろもろ
	D3D11_MAPPED_SUBRESOURCE hMappedres;
	
	//処理の結果を入れる変数
	HRESULT hr;

	//テクスチャのバッファー作成
	hr = GetTextureBuffer(FILE_NAME, pBuffer, &hMappedres);
	//エラーメッセージ
	ERROR_CHECK(hr, "画像のバッファーの作成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//テクスチャのバッファーを取得（テクスチャ読み取り設定が必要である場合）
HRESULT Texture::GetTextureBuffer(const std::string& FILE_NAME, ID3D11Texture2D** pBuffer, D3D11_MAPPED_SUBRESOURCE* hMappedres)
{
	//処理の結果を入れる変数
	HRESULT hr;

	//引数文字列をワイド文字変換
	wchar_t wtext[FILENAME_MAX];
	size_t ret;
	mbstowcs_s(&ret, wtext, FILE_NAME.c_str(), FILE_NAME.length());
	//マルチバイトをワイド文字に変換する関数
		//multi byte string to wide ~ ？
		//第一引数は必要なもの　fileNameを　wtextに、入れて、どのぐらいの長さになるのか

	//COM使うため(WIC)
	hr = CoInitialize(nullptr);	//COMを使うなら、これが必要
	//エラーメッセージ
	ERROR_CHECK(hr, "COMの初期化失敗", "エラー");

	//Wic用クラス
	//作成クラス
	IWICImagingFactory* pFactory = nullptr;	
	//デコーダー
	IWICBitmapDecoder* pDecoder = nullptr;	
	//フレームデコーダー
	IWICBitmapFrameDecode* pFrame = nullptr;
	//コンバーター
	IWICFormatConverter* pFormatConverter = nullptr;
	//COM(WIC)の作成
	hr = CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, reinterpret_cast<void**>(&pFactory));
	//エラーメッセージ
	ERROR_CHECK(hr, "WICのインスタンス生成の失敗", "エラー");

	//ファイル名からデコーダーを取得
	hr = pFactory->CreateDecoderFromFilename(wtext, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder);
	//エラーメッセージ
	ERROR_CHECK(hr, "画像ファイルの読み込み失敗", "エラー");

	//フレーム取得
	hr = pDecoder->GetFrame(0, &pFrame);
	//エラーメッセージ
	ERROR_CHECK(hr, "フレーム取得の失敗", "エラー");
	//コンバーター取得
	hr = pFactory->CreateFormatConverter(&pFormatConverter);
	//エラーメッセージ
	ERROR_CHECK(hr, "コンバーターの初期化失敗", "エラー");
	//コンバーターの初期化
	hr = pFormatConverter->Initialize(pFrame, GUID_WICPixelFormat32bppRGBA, WICBitmapDitherTypeNone, NULL, 1.0f, WICBitmapPaletteTypeMedianCut);
	//エラーメッセージ
	ERROR_CHECK(hr, "WIC初期化の失敗", "エラー");

	//COM（WIC）の解放
	CoUninitialize();

	//画像サイズの取得
	hr = pFormatConverter->GetSize(&imgWidth_, &imgHeight_);
	//エラーメッセージ
	ERROR_CHECK(hr, "画面サイズ取得失敗", "エラー");


	//テクスチャバッファの生成
	//テクスチャの設定
	D3D11_TEXTURE2D_DESC texdec;
	//画像の横幅
	texdec.Width = imgWidth_;	
	//画像の縦幅
	texdec.Height = imgHeight_;	
	//Mip　奥に行けば小さくなる（奥に行けば行くほど荒くなる）
	texdec.MipLevels = 1;	
	texdec.ArraySize = 1;
	texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texdec.SampleDesc.Count = 1;
	texdec.SampleDesc.Quality = 0;
	texdec.Usage = D3D11_USAGE_DYNAMIC;
	texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	texdec.MiscFlags = 0;

	//テクスチャの作成
	hr = Direct3D::pDevice->CreateTexture2D(&texdec, nullptr, pBuffer);
	//エラーメッセージ
	ERROR_CHECK(hr, "テクスチャの生成失敗", "エラー");


	//コンテキスト（絵を描く人）に渡す準備
	hr = Direct3D::pContext->Map((*pBuffer), 0, D3D11_MAP_WRITE_DISCARD, 0, hMappedres);
	//エラーメッセージ
	ERROR_CHECK(hr, "コンテキストのマップ失敗", "エラー");


	//横一列で何ピクセルか
		//詳細：何バイトか、幅＊RGBAのための＊４
	hr = pFormatConverter->CopyPixels(nullptr, imgWidth_ * 4, imgWidth_ * imgHeight_ * 4, (BYTE*)hMappedres->pData);
	//エラーメッセージ
	ERROR_CHECK(hr, "ピクセル数（列）サイズ取得失敗", "エラー");


	//テクスチャバッファへのアンマップ
		//アクセス拒否
	Direct3D::pContext->Unmap((*pBuffer), 0);


	//処理の成功
	return S_OK;
}




