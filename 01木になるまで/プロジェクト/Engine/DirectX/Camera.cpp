#include "Camera.h"		//ヘッダ
#include "Direct3D.h"	//Direct3D　ウィンドウサイズ取得のため


/*Camera*/
namespace Camera
{
	/*extern変数　実装************************************************************************/
	//カメラの位置（視点）
	XMVECTOR position_;	


	//見る位置（焦点）
	XMVECTOR target_;	
	//ビュー行列
	XMMATRIX viewMatrix_;	
	//プロジェクション行列
	XMMATRIX projMatrix_;	

	//画角角度
	float angle = XM_PIDIV4;


	//カメラの視野範囲行列を作成する関数
		//詳細：プロジェクション行列となる
		//引数：視野範囲となる最低距離（描画開始位置）（単位：ｍ）
		//引数：視野範囲となる最高距離（描画終了位置）（単位：ｍ）
		//戻値：プロジェクション行列
	XMMATRIX GetCameraProjectionMatrix(float viewIn , float viewOut);


}


//初期化
void Camera::Initialize()
{
	//カメラの初期位置を指定
		//原点（0,0,0,0） を見るカメラ。カメラ位置は、原点から　後、斜め上。
	//カメラの位置
	position_ = XMVectorSet(0, 3, -10, 0);	
	//カメラの焦点
	target_ = XMVectorSet(0, 0, 0, 0);	

	//プロジェクション行列　初期化
	//引数：カメラの視点　10	�p　から描画開始
	//引数：カメラの終点　100	m　 まで描画
	SetCameraFieldOfView(0.1f, 100.f);
	
}

//カメラの視野範囲行列を作成する関数
XMMATRIX Camera::GetCameraProjectionMatrix(float viewIn, float viewOut)
{
	//引数：視野範囲
	//引数：プロジェクションの範囲（ウィンドウサイズを基とする）
	//引数：視野範囲となる最低距離（描画開始位置）（単位：ｍ）
	//引数：視野範囲となる最高距離（描画終了位置）（単位：ｍ）
	return XMMatrixPerspectiveFovLH(
		angle, 
		(FLOAT)Direct3D::scrWidth / (FLOAT)Direct3D::scrHeight, 
		viewIn, 
		viewOut);
}


//各更新処理
//ビュー行列の更新
	//ビューはカメラの位置、焦点によって動的に変える必要がある。
	//プロジェクションはカメラのズームによって、ビューの行列情報を更新する必要が出てくる。（行列を必要とするタイミングに計算しなおすだけで十分）
	//プロジェクション行列を毎フレーム更新するとしたら　ウィンドウサイズが動的に切り替わる方式の場合
	//ウィンドウサイズが変わった際の、世界の広さを変える場合は、動的に更新する必要が出てくる。
//更新
void Camera::Update()
{
	//ビュー行列の作成
		//引数：カメラ視点
		//引数：カメラ焦点
		//引数：カメラの上方向（XMVectorSet(0, 1, 0, 0)をカメラの上方向とする）
	viewMatrix_ = XMMatrixLookAtLH(position_, target_, XMVectorSet(0, 1, 0, 0));
		/*
			カメラの上方向

			カメラが真下を向いたときなどに計算が正しく行われないと気がある、
			上記を踏まえて、カメラの真上は、現在の視点や焦点から、現在のカメラの向いている方向から見て真上を常に向き続けることが望ましい。

			しかし、それを踏まえると、
			カメラの見え方や、カメラ移動管理が複雑になってしまう。
		*/

}

//位置を設定
void Camera::SetPosition(const XMVECTOR& position)
{
	position_ = position;
}
//位置を設定
void Camera::SetPosition(float x, float y, float z)
{
	/*
		上記関数で呼ばれたとしても
		オーバーロード関数をよび　同じ記述をしないようにする
	*/
	SetPosition(XMVectorSet(x, y, z, 0));
}

//カメラ視点を取得
XMVECTOR Camera::GetPosition()
{
	return position_;
}

//カメラ焦点を取得
XMVECTOR Camera::GetTarget()
{
	return target_;
}

//焦点（見る位置）を設定
void Camera::SetTarget(XMVECTOR target)
{
	target_ = target;	
}
//焦点（見る位置）を設定
void Camera::SetTarget(float x, float y , float z)
{
	SetTarget(XMVectorSet(x, y, z, 0));
}

//視野距離を設定
void Camera::SetCameraFieldOfView(float viewMin, float viewMax)
{
	//引数：視野範囲となる最低距離（描画開始位置）（単位：ｍ）
	//引数：視野範囲となる最高距離（描画終了位置）（単位：ｍ）
	projMatrix_ = GetCameraProjectionMatrix(viewMin, viewMax);

}

//ビュー行列を取得
XMMATRIX Camera::GetViewMatrix()
{
	return viewMatrix_;

}

//プロジェクション行列を取得
XMMATRIX Camera::GetProjectionMatrix()
{
	return projMatrix_;
}

//画角（視野の広さ）を設定
void Camera::SetAngle(float angleValue)
{
	angle = angleValue;

	//アングルを変更するために
	//プロジェクション行列の画角を変更させる

	//プロジェクション行列(初期値)
	//第一引数：画角角度
	projMatrix_ = XMMatrixPerspectiveFovLH(angle, (FLOAT)Direct3D::scrWidth / (FLOAT)Direct3D::scrHeight, 0.1f, 100.0f);
};
//画角（視野の広さ）を加算
void Camera::AddAngleForZoomOut(float addValue)
{
	angle += addValue;
	//プロジェクション行列(初期値)
	//行列の作り直し
	//第一引数：画角角度
	projMatrix_ = XMMatrixPerspectiveFovLH(angle, (FLOAT)Direct3D::scrWidth / (FLOAT)Direct3D::scrHeight, 0.1f, 100.0f);
};

//画角（視野の広さ）を減算
void Camera::SubAngleForZoomIn(float subValue)
{
	angle -= subValue;

	//プロジェクション行列(初期値)
	//第一引数：画角角度
	projMatrix_ = XMMatrixPerspectiveFovLH(angle, (FLOAT)Direct3D::scrWidth / (FLOAT)Direct3D::scrHeight, 0.1f, 100.0f);
};

