#pragma once

//Direct2D
//DirectWriteを使用した、　テキストの描画などに用いる

//システム内　標準ヘッダ
#include <d2d1.h>		//Direct２D　２D描画用　今回はテキスト描画のために使用する
#include <d2d1_1.h>		//Direct２D Ver1　２D描画用　今回はテキスト描画のために使用する
#include <dwrite.h>		//DirectWrite	Direct2Dと連携し、テキスト描画のために使用する。テキストのライター
#include <wchar.h>		//wchar , swprintf()　ワイド文字型を扱うため


//ライブラリのリンク
#pragma comment( lib, "d2d1.lib" )
#pragma comment( lib, "dwrite.lib" )


/*
	名前空間詳細	：Direct2Dにおける2D表現、描画、絵を描くデバイス用意、画用紙を入れ替えるスワップチェイン用意、Direct3Dとの連携によりDirect3Dの画用紙への書き込みを行う

	名前空間概要（詳しく）
				：
				・2D描画を使い、DirectWriteと連携し、テキスト描画を担う
				・Direct2Dとして独立して画面を作り、描画するのではなく、あらかじめ存在する３D用の画用紙（RenderTarget）を使用して、
				　その画用紙へ、Direct2Dの描画内容を書き込ませる。
				・Direct3Dとの連携

*/
namespace Direct2D
{
	/*extern変数************************************************************************/
	//ウィンドウの横幅
		//詳細：Main.cppより受け取るウィンドウ横幅
		//初期値：後述宣言
		//アクセス：Direct2D初期化にて、Main.cppより受け取る
		//制限：なし
		//理由：テキスト描画位置選定のために使用する
	extern int scrWidth;
	//ウィンドウの縦幅
		//詳細：Main.cppより受け取るウィンドウ縦幅
		//初期値：後述宣言
		//アクセス：Direct2D初期化にて、Main.cppより受け取る
		//制限：なし
		//理由：テキスト描画位置選定のために使用する
	extern int scrHeight;


	/*メソッド************************************************************************/
	//初期化
		//詳細：Direct2Dにおけるゲーム内2D環境を作成する
		//    ：Direct3Dとの連携を行い、Direct2Dにおける描画先をDirect3Dと連携を取る。その準備
		//戻値：処理の結果（エラーなし　S_OK　、エラーあり　E_FAIL）
	HRESULT Initialize();
	
	//Deviceの作成
	//詳細：Direct2D上のデバイスの作成
	//引数：なし
	//戻値：なし
	void CreateDevice();

	//Factoryの作成
		//詳細：Direct2D関係の型作成クラスの作成
		//引数：なし
		//戻値：なし
	void CreateFactory();

	//DeviceContextの作成
		//詳細：Direct2D上のDeviceContextの作成
		//引数：なし
		//戻値：なし
	void CreateDeviceContext();


	//テキストフォーマット（テキストのサイズなど）の作成
		//詳細：フォントサイズのセットし直す際にも使用する
		//　　：引数にて渡された、ポインタにフォントフォーマットを作成して返す
		//　　：ポインタのポインタによって、ポインタのアドレスを取得する→これにより、関数呼び出し元のポインタのアドレスが入るため、ポインタの参照渡しという形になる
		//引数：テキストフォーマット　インスタンス代入先
		//引数：フォントサイズ
		//戻値：なし
	HRESULT CreateTextFormat(IDWriteTextFormat** pTextFormat, float fontSize);



	/*描画************************************************************************/
	//描画開始
		//詳細：Direct3Dと連携した　共通の画用紙へ書き込みを宣言
		//　　：Direct2DのDeviceContextへ書込み宣言　すでに連携済みであるため、Direct2DContextにおける書き込み先がDirect3D上へと至っているとかんがえる　
		//引数：なし
		//戻値：なし
	void BeginDrawDirect2D();

	//描画終了
		//詳細：書き込み終了
		//　　：Direct2DのDeviceContextへ書き込み終了を宣言
		//　　：必ずDirect3DのEndDrawよりも前に行うこと。Direct3Dと画用紙を連携しているため、先にDirect3DにEndDrawを行われると、画面書き込み先がいなくなってしまう。
		//　　：必ず先に書き込みの終了を行う
		//引数：なし
		//戻値：なし
	void EndDrawDirect2D();

	//解放
		//詳細：Direct2Dにおけるデバイス系の解放
		//引数：なし
		//戻値：なし
	void Release();


	//テキストを書くブラシの取得
		//詳細：描画をするために必要な、テキストを書くブラシ
		//引数：なし
		//戻値：描画ブラシのポインタ
	ID2D1SolidColorBrush* GetBrush();

	//DeviceContextの取得
		//引数：なし
		//戻値：DeviceContext
	ID2D1DeviceContext* GetDeviceContext();


}
