#include "Direct2D.h"			//ヘッダ
#include "Direct3D.h"			//Direct3D
	/*
		
		//Direct3Dの所有しているスワップチェインに、２Dのデータを渡せるようにする
		//上記のようにすることで、
		//Direct3Dにて描画されたバッファかつ、Direct2Dにて描画するバッファを共通して扱う、（おそらく。
		//hr = Direct3D::GetSwapChain()->GetBuffer(0, IID_PPV_ARGS(&pDXGISurface));ここで、第一引数にてバッファの番号を指定していて、　その番号が、3Dと同様のため、そのように考えられる）
		//３D描画、かつ、２D描画を行なわせる

	*/

/*Direct2D*/
	//メンバとして外に見せたくないものを宣言
	//ヘッダには、逆に見せてもよいものを宣言
namespace Direct2D
{
	/*extern 変数　初期化*************************************/
	int scrWidth = 0;
	int scrHeight = 0;


	/*Direct2Dを描画するために必要****************************/
	//DirectWriteFactory
		//詳細：作成クラス
	IDWriteFactory* pDWriteFactory = nullptr;
	//Direct2DFactory
		//詳細：作成クラス
	ID2D1Factory1* pD2DFactory1 = nullptr;
	//Direct2Dにて絵を描くクラス
	ID2D1DeviceContext* pD2DDeviceContext = nullptr;
	//Direct2D監督
	ID2D1Device* pD2DDevice = nullptr;
	//Direct2D　ビットマップ
	ID2D1Bitmap1* pBitmap = nullptr;
	//Direct2D　テキストのフォーマット
	IDWriteTextFormat* pTextFormat = nullptr;
	//Direct2D　テキストのブラシフォーマット
	ID2D1SolidColorBrush* pSolidBrush = nullptr;

}



//Direct2Dの初期化
	//Direct3Dの初期化が終わり、
	//スワップチェインが作成されている前提
HRESULT Direct2D::Initialize()
{
	//スクリーンサイズの取得
	scrWidth = Direct3D::scrWidth;
	scrHeight = Direct3D::scrHeight;


	//処理の結果を受け取る変数
	HRESULT hr = S_OK;
	//書込みビットマップの支柱（prop）の作成
	auto propsBit = 
		D2D1::BitmapProperties1(
			D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
			D2D1::PixelFormat(
				DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE));

	//Direct3D::GetIDXGISurface()より、Direct3DとDirect2Dとの連携用のポインタを受け取り、
	//書込みビットマップへの連携をつなげる
		//下記により、Direct2Dにて書き込むBitMapをDirect3Dと連携する。
	pD2DDeviceContext->CreateBitmapFromDxgiSurface(Direct3D::GetIDXGISurface(),
		propsBit,
		&pBitmap);
	//書込みビットマップを指定
	pD2DDeviceContext->SetTarget(pBitmap);

	//DPI
		//ウィンドウの1インチにおけるドット数（dpi）
	FLOAT dpiX;
	FLOAT dpiY;
		//VisualStudio2017
			//pD2DFactory1->GetDesktopDpi(&dpiX, &dpiY);
		//VisualStudio2019において、上記にてDPIを取得することはできないため、以下の関数で取得する
			//https://docs.microsoft.com/en-us/answers/questions/170411/creating-a-simple-direct2d-application-39id2d1fact.html
	//DPI取得
	dpiX = (FLOAT)GetDpiForWindow(GetDesktopWindow());
	//X成分とY成分が同じであるため、コピー
	dpiY = dpiX;

	//コンテキストへDPIのセット
	pD2DDeviceContext->SetDpi(dpiX, dpiY);

	//レンダーターゲットの支柱、設定作成
	D2D1_RENDER_TARGET_PROPERTIES props =
		D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT,
			D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), dpiX, dpiY);
	
	//DirectWrite作成
	hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown * *>(&pDWriteFactory));
	//エラーメッセージ
	ERROR_CHECK(hr, "DirectWrite作成失敗", "エラー");

	//テキストフォーマット（テキストのフォントサイズなど）の作成
		//引数：テキストフォーマットのポインタのアドレス
		//引数：テキストサイズ
	hr = CreateTextFormat(&pTextFormat, 30);
	//エラーメッセージ
	ERROR_CHECK(hr, "テキストフォーマット作成失敗", "エラー");

	//関数CreateSolidColorBrush()
		//引数：フォント色（D2D1::ColorF(D2D1::ColorF::Black)：黒, D2D1::ColorF(D2D1::ColorF(0.0f, 0.2f, 0.9f, 1.0f))：RGBA指定）
	hr = pD2DDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), &pSolidBrush);
	//エラーメッセージ
	ERROR_CHECK(hr, "テキストブラシ作成失敗", "エラー");

	//処理の結果を返す
	return hr;


}


//Direct2Dの描画開始
	//Direct３DのBeginDrawの後に呼び込む
void Direct2D::BeginDrawDirect2D()
{
	//描画開始宣言
	pD2DDeviceContext->BeginDraw();
}



//Direct2Dの描画終了
	//Direct３DのEndDrawの前に呼び込む
	//→画面を切り替えてしまう前に呼び込む
void Direct2D::EndDrawDirect2D()
{
	//描画終了宣言
	pD2DDeviceContext->EndDraw();
}

//解放
void Direct2D::Release()
{
	SAFE_RELEASE(pSolidBrush);
	SAFE_RELEASE(pTextFormat);
	SAFE_RELEASE(pDWriteFactory);
	SAFE_RELEASE(pBitmap);
	SAFE_RELEASE(pD2DFactory1);
	SAFE_RELEASE(pD2DDeviceContext);
	SAFE_RELEASE(pD2DDevice);
}

//ブラシを取得
ID2D1SolidColorBrush * Direct2D::GetBrush()
{
	return pSolidBrush;
}

//デバイスコンテキストの取得
ID2D1DeviceContext * Direct2D::GetDeviceContext()
{
	return pD2DDeviceContext;
}

//デバイスの作成
	//Direct3Dによって、適切なタイミングにて呼び込まれる
void Direct2D::CreateDevice()
{
	//デバイス作成
		//引数：Direct3Dとの連携用ポインタ
		//引数：Direct2DDeviceポインタのアドレス
	pD2DFactory1->CreateDevice(Direct3D::GetIDXGIDevice(),
		&pD2DDevice);
}

//作成クラスの作成
void Direct2D::CreateFactory()
{
	//Factoryクラスの設定
	D2D1_FACTORY_OPTIONS fo = {};

//デバック時
#ifdef _DEBUG
	fo.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
#endif

	//Factory作成
	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED,
		fo,
		&pD2DFactory1);

}

//デバイスコンテキストの作成
void Direct2D::CreateDeviceContext()
{
	//デバイスコンテキストの作成
	pD2DDevice->CreateDeviceContext(
		D2D1_DEVICE_CONTEXT_OPTIONS_NONE,
		&pD2DDeviceContext);
}

//テキストフォーマット（テキストのサイズなど）の作成
HRESULT Direct2D::CreateTextFormat(IDWriteTextFormat ** pTextFormat, float fontSize)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	//pTextFormat
		//古いテキストフォーマットを解放
		//引数のポインタが存在する場合、解放する
	SAFE_RELEASE((*pTextFormat));


	//関数CreateTextFormat()
		//第1引数：フォント名（L"メイリオ", L"Arial", L"Meiryo UI"等）//C:\Windows\Fonts\ （PCによっては存在しないフォントがあるかもしれない、それを踏まえて、実行環境は同じWindowsバージョンでなくてはいけない）
		//第2引数：フォントコレクション（nullptr）
		//第3引数：フォントの太さ（DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_WEIGHT_BOLD等）
		//第4引数：フォントスタイル（DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STYLE_OBLIQUE, DWRITE_FONT_STYLE_ITALIC）
		//第5引数：フォントの幅（DWRITE_FONT_STRETCH_NORMAL,DWRITE_FONT_STRETCH_EXTRA_EXPANDED等）
		//第6引数：フォントサイズ（20, 30等）
		//第7引数：ロケール名（L""）
		//第8引数：テキストフォーマット（&g_pTextFormat）
	hr = pDWriteFactory->CreateTextFormat(L"Arial", nullptr, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL,
		fontSize, L"", pTextFormat);
	//エラーメッセージ
	ERROR_CHECK(hr, "テキストフォーマット作成失敗", "エラー");

			/*
			実装確認済み　フォント（デザインに惹かれたもので確認できたもの）
			※フォント画像に、日本語が書いてないものは日本語対応なし。日本語フォントに変化なし
			※英語と日本語が組み合わさっているものは適用されない（日本語ファイルが未対応なのかも）
	
			******Direct2D参考サイトにておすすめされたフォント******
			・メイリオ
			・Arial
			・Meiryo UI
		
			******Windows１０に存在するフォント**********************
			・Algerian 標準（英語も適用なし）
			・Bauhaus 93 標準（英語も適用なし）
			・Rockwell　（日本語適用なし）
			・Orator Std（日本語適用なし）
			・Gill Sans（日本語適用なし）

			*/


	//関数SetTextAlignment()
		//第1引数：テキストの配置（DWRITE_TEXT_ALIGNMENT_LEADING：前, DWRITE_TEXT_ALIGNMENT_TRAILING：後, DWRITE_TEXT_ALIGNMENT_CENTER：中央）
	hr = (*pTextFormat)->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
	//エラーメッセージ
	ERROR_CHECK(hr, "テキスト配置作成失敗", "エラー");

	//処理の結果
	return S_OK;
}
