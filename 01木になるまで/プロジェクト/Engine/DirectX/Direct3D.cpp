#include <wincodec.h>		//ビットマップ書き込み許可
#include <d3dcompiler.h>	//Direct3Dコンパイラー
#include "Direct3D.h"		//3D描画のための　Direct3D
#include "Direct2D.h"		//テキスト表示のためのDirect2Dクラス
#include "Camera.h"			//カメラ　名前空間
#include "Texture.h"		//２Dテクスチャクラス
#include "../PolygonGroup/Sprite.h"			//レンダーターゲットにて書き込んだ画用紙を、３Dオブジェクトとして、書き込む書き込み先のバッファ
#include "../../Shader/ShaderFactory.h"		//シェーダー生成クラス
#include "../../Shader/BaseShader.h"		//シェーダークラスの継承元クラス
#include "../../Blend/BlendFactory.h"		//ブレンド生成クラス
#include "../../Blend/BaseBlend.h"			//ブレンドクラスの継承元クラス
#include "../../CommonData/GameDatas.h"		//ゲーム内情報定義


//ライブラリのリンク
#pragma comment( lib, "WindowsCodecs.lib" )

//Direct3D
namespace Direct3D
{
	/*
		Direct3D用の既存クラス型の命名規則・注意

		・ID3D11〇〇　＝　宣言をCreate〇〇にて行って、解放時に、Release()を呼び込む
		・●●View　＝　●●への橋渡しのクラス。仲介クラス
	
	*/
	/*
		Direct3Dのヘッダにて宣言して、extern宣言している変数は、
		外部から参照するものである。

		外部から参照を受けない変数などは、
		Direct３D.cpp側にて、同じく名前空間宣言を行い、宣言する
	
	*/


	/*extern宣言変数の初期化*/
	/*Directの３D描画のための必要インスタンス***********************************************************/
	//デバイス
		//詳細：Direct3Dにおける監督を担う
	ID3D11Device*           pDevice = nullptr;		
	//デバイスコンテキスト
		//詳細：絵を描く人（画用紙へ）
	ID3D11DeviceContext*    pContext = nullptr;		
	//レンダーターゲットビュー
		//詳細：RendarTargetへの橋渡し
	ID3D11RenderTargetView* pRenderTargetView = nullptr;	
	//深度ステンシル（Zバッファ法（隠面消去を行う））
		//詳細：ウィンドウに表示する描画を、カメラから、前面にあるものは前面に塗り、後面にあるものは、塗らずに前面の色を残す処理（そのようにすれば、カメラから全面の色だけが残り、後面の色は棒がされなくなる）
	ID3D11Texture2D*	pDepthStencil = nullptr;			
	//深度ステンシルビュー（深度ステンシルとの仲人）
		//詳細：深度ステンシルへの橋渡し
	ID3D11DepthStencilView* pDepthStencilView = nullptr;		
	//レンダーターゲット　（新しい書き込み先）
		//詳細：絵を書き込む画用紙
	ID3D11Texture2D*	pRenderTexture = nullptr;	
	
	//レンダーターゲットビュー２
		//詳細：複数の画面の書き込み先を用意することで、
		//　　：複数に書き込み、それを使い分けることで、　複数画面があるように見せる。
		//制限：２画面用
	ID3D11RenderTargetView* pRenderTargetView2 = nullptr;	

	//２画面の２画面目の書き込み先オブジェクト
		//制限：２画面用
	Sprite* pScreen = nullptr;

	/*Direct2Dと連携するための必要要素*************************************************************************/
	//連携用デバイス
		//詳細：Direct2Dを３Dと共用するために使用するスワップチェインなど
		//　　：IDXGI = Direct3DとDirect2Dとで連携の橋渡しのようなことをしてくれる
		//制限：Direct２Dとの連携用
	IDXGIDevice* pGIDevice = nullptr;
	//デバイスコンテキスト　と　スワップチェーンの接続を担う
		//制限：Direct２Dとの連携用
	IDXGISurface* pGISurface = nullptr;
	//Direct2Dにおける連携を可能とするバージョンのSwapChain
		//詳細：通常のスワップチェインは、Direct2Dとの連携に対応していない。
		//　　：そのため、連携を対応しているSwapChain1（Ver.1）を使用する
		//制限：Direct２Dとの連携用
	IDXGISwapChain1* pSwapChain1 = nullptr;


	/*スクリーン設定用*******************************************************************************************/
	//スクリーンサイズフルの描画範囲
	D3D11_VIEWPORT vpBig;
	//縮小サイズの描画範囲
		//詳細：複数画面にしたいときに、描画範囲を複数作り、それを切り替える。
	D3D11_VIEWPORT vpSmall;


	//スクリーンサイズ
	//Width
	int scrWidth = 0;
	//Height
	int scrHeight = 0;
	


	/*シェーダーごとのシェーダー情報コンパイル済みクラス***********************************************************************/
	//シェーダークラス（継承先）のインスタンスを入れておく配列
		//詳細：ポリゴン描画時のシェーダー表現を詳細に記述したクラス。
		//　　：シェーダーの表現ごとに個別のクラスを持たせて、各クラスのコンパイル済み結果を代入するクラスをDirect３Dへ持たせておく。
		//　　：描画の際に、任意のシェーダーを使用する際に、Direct３Dより下記にアクセスし、シェーダーへのパイプラインを通す。すると、各シェーダーファイルの頂点シェーダー関数、ピクセルシェーダー関数を呼び込むことが可能となる。
		//制限：ゲーム終了までバッファを持たせることになるため、不要なシェーダークラスの宣言を行わないように心がける
	BaseShader* shaderBundle_[(int)SHADER_TYPE::SHADER_MAX] = { nullptr };	//全要素をnullptrにて初期化
		//Direct３Dにて、シェーダーごとの宣言部分を書き込むのは非効率で、Direct３D部分以外を記述してしまうため、クラスファイルに分離した

	/*ブレンド状態ごとのブレンド情報コンパイル済みクラス***********************************************************************/
	//ブレンドクラス（継承先）のインスタンスを入れておく配列
		//詳細：ブレンド（色の混ぜ具合（詳細は、Project/Blend/BaseBlend.hを参照））ごとに個別のクラスを持たせて各クラスのコンパイル済み結果を代入するクラスを持たせておく。
		//　　：詳細は、上記のshaderBundle_と同様である。
	BaseBlend* blendBundle_[(int)BLEND_TYPE:: BLEND_MAX] = { nullptr };
	//Direct３Dにて、ブレンドごとの宣言部分を書き込むのは非効率で、Direct３D部分以外を記述してしまうため、クラスファイルに分離した


	//背景の色（色情報）
		//詳細：ゲーム内背景色
		//初期値：外部より参照
	float clearColor_[4] = { 
	BACKGROUND_COLOR_BLACK.vecX , 
	BACKGROUND_COLOR_BLACK.vecY ,
	BACKGROUND_COLOR_BLACK.vecZ ,
	BACKGROUND_COLOR_BLACK.vecW };


	//色を識別するenum値
	enum RGBA
	{
		R = 0, G,B,A
	};




	//ゲーム全体の背景色の変更
		//詳細：ゲーム内３D空間の背景色（無限に続く世界の色、何も表示しない場合、指定背景色が彩られた世界が表示される）の指定
		//　　：ベクトル変数にて受け取る
		//引数：RGBA値(ベクトル)
		//戻値：なし
	void SetBackGroundColor(XMVECTOR& rgba);
	/*
	//ゲーム全体の背景色の変更
		//詳細：ゲーム内３D空間の背景色（無限に続く世界の色、何も表示しない場合、指定背景色が彩られた世界が表示される）の指定
		//引数：R値
		//引数：G値
		//引数：B値
		//引数：A値
		//戻値：なし
	void SetBackGroundColor(float r, float g, float b, float a);
	//背景色を取得
		//詳細：ゲーム内３D空間の背景色（無限に続く世界の色、何も表示しない場合、指定背景色が彩られた世界が表示される）の取得
		//引数：なし
		//戻値：ゲーム内３D空間の背景色
	XMVECTOR GetBackGroundColor();
	*/
	//→背景色の変更、受け取りは、Project/CommonDatas/GameDatas.hへ移動
	//→移動理由：背景変更のために、Direct３Dへアクセスさせたくない





}

/*
	Direct3Dにおける初期化は、
	Direct2D使用の場合の初期化を行う

	Dierct2D未使用の場合、不要な処理なども個別にあるため。記述量は減少する。
	しかし、Direct2Dは、今回、テキスト描画のために使用している。

	テキスト描画がゲームにおいて不要になることはないため、
	あえてDirect3Dのみの場合の初期化は記述せず、
	Direct2D仕様の場合の初期化のみ記述する

	↓
*/

/*
	■Direct2D対応のコーディング

	・SwapChainをSwapChain1に変更
		→SwapChainのVerを変更

	・DeviceとDeviceContext、SwapChainを別々に作成

	・Direct2Dを初期化し、
	　Direct2Dにおける描画画用紙、描画書き込み先をDirect3Dと連携するように、連携処理を行う


	■Direct2Dにおけるテキスト描画を参考にしたサイト
		//http://dioltista.blogspot.com/2019/04/c-directx11-direct2d-png.html

	■スマートポインタを使わずに、DirectXの型のインスタンスを取得して、Direct2Dを作成する方法
		//https://docs.microsoft.com/ja-jp/archive/msdn-magazine/2013/may/windows-with-c-introducing-direct2d-1-1

	※スマートポインタを使用しないのは、
	　Direct3D,2Dにおけるインスタンスは、参照カウンタ方式を採用している（詳細は、Project/CommonDatas/GlobalMacro.hにて）
	  そのため、作成をCreate〇〇にて、解放をRelease()にてという手順を、自動化せず、きちんと手動で行うことで、
	  参照カウンタ方式を覚えるためにあえて、自動解放のスマートポインタを使用していない

*/
/*
	HRESULTについて

	バッファーの作成などを行う際に、作成が成功したかなどを
	HRESULT型の戻り値にて判断する。

	処理が失敗した場合、多くの場合処理を終了する必要がある。
	そのままゲームを続けては不具合が生じる場合があるため、
	ゲームを終了するために、
	自身の関数ならば、関数の呼び出しもとへ戻り、
	最終的なMain.cppなどのコードの中で、プロジェクト終了（return 0 ）を返すようにすればよい。
		呼び出し先には責任を負わせず、呼び出し元がプロジェクトを続けるかの責任を受ける


	S_OK		: 処理成功
	E_FAIL		: 処理の失敗（全般）
	E_~~		: 処理の失敗（~~にてエラーの種類）
*/
/*
	MessageBox(nullptr , )
	
*/

	//Direct3D初期化
		//詳細：Device・DeviceContext　、　SwapChainを別々作成
		//　　：Direct2D対応
	HRESULT Direct3D::InitDirect3DAndSupprtsDirect2D(int winW, int winH, HWND hWnd)
	{
		//ウィンドウスクリーンのサイズを設定
		scrWidth = winW;
		scrHeight = winH;

		//処理の結果を受け取る変数
		HRESULT hr = S_OK;

		{

			//DirectX11上でDirect2Dを使用するために必要なフラグ値
				//詳細：この値をCreateDeviceの第4引数に入れなければ、　Direct2Dの描画、　レンダーターゲットビューを取得できない//逆に言えば、　これを第4引数に入れるだけで、　Direct2Dによって、　書き込まれる準備が整う
			UINT createDeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

		//制限：Debug時のみ有効
		#ifdef _DEBUG
			//createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;	//デバック用フラグ
				//D3D型などのDirectXにおける　ポインタにおいて発生するDirectX側のエラー（例えば、処理終了後に、解放し忘れがある。など？）のエラーを返してくれる。
				//それらのエラーは、大抵、実行においては問題のないエラーなどで、　問題なく動くが、裏ではエラーをはいている　などというときに、ビルドの出力欄に、エラーを出力してくれる
		#endif

		
			///////////////////////////デバイス、デバイスコンテキストの作成///////////////////////////////////////////
			//デバイス、デバイスコンテキストの作成
			hr = D3D11CreateDevice(
				nullptr,	// どのビデオアダプタを使用するか？既定ならばnullptrで
							//グラフィックボードどれを使うか→nullptrでデフォルト

				D3D_DRIVER_TYPE_HARDWARE,	// ドライバのタイプを渡す。ふつうはHARDWARE
													//ハードウェアに直接指定するが、→対応していないハードウェアにやろうとすると→、対応してなかったら、ソフトウェアにする
													//→ハードウェアで対応しなかったあ、ソフトウェアにすることができるが、→それだと遅い（なので、）

				nullptr,					// 上記をD3D_DRIVER_TYPE_SOFTWAREに設定しないかぎりnullptr
				createDeviceFlags,			//フラグ指定、（DEBUG, Direct2D仕様のためのサポート）
				nullptr,	// デバイス、コンテキストのレベルを設定。nullptrにしとけばOK
				0,			// 上の引数でレベルを何個指定したか
				D3D11_SDK_VERSION,			// SDKのバージョン。Direct３D11
				&pDevice,					// 無事完成したDeviceアドレスが返ってくる
				nullptr,					// level
				&pContext					// 無事完成したDeviceContextアドレスが返ってくる
			);
			//エラーチェック
				//詳細：エラーチェックとともにエラーの場合、引数文字列をウィンドウであるメッセージボックスとして出力し、エラーを返す
				//　　：この時、マクロにて、returnを行う処理を行っている。
				//　　：Define内の関数が、記述以下に展開されることになるため、returnをすれば、呼び込んだスコープ内にてreturnを返す処理となる（注意）
			ERROR_CHECK(hr, "デバイス、コンテキスト、スワップチェイン作成失敗", "エラー");
		}

		{
			///////////////////////////Direct2Dとの連携用デバイスの作成///////////////////////////////////////////
			//DXGIのデバイスの作成
				//詳細：Direct2Dとの連携用のデバイスの作成
				/*
					スマートポインタを使用しない場合に使用する

					//QueryInterface　＝　DirectXのスマートポインタであるComPtrの代用で、
						//dxgiDeviceにアドレスを取得させる
						//見えないところで、実態を持っているIDXGIDeviceのアドレスをもらう。
						//実体は一つで、そのアドレスを取得して、　取得により、見えないところにある、カウントが１アップされる（このカウントは、DirectX型のアドレスが受け渡されたら、カウントが１UPする。）

						//このカウントがあることによって、　カウントが０でない限りは、実態を持ち続けて、カウントが0になったら、見えないところで確保されている実体を解放される
						//自動でカウントされるが、　自動でカウントダウンはされない（ComPtrを使っていない限り、。）
						//なので、カウントダウンはこちらから指定して行う。　→　それがRelease（）である。

				*/
			hr = pDevice->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&pGIDevice));
			//エラーチェック
			ERROR_CHECK(hr, "IDXGIDeviceの作成失敗", "エラー");


			//Adapter
			//Direct3D,Direct2Dの相互間をつなげるケーブルのようなもの
			IDXGIAdapter* adapter = nullptr;
			//Deviceから、連携のためのアダプターを取得する
				//このスコープで宣言したポインタのアドレス格納部分に、アドレスを入れてもらいたいので、（代入するところは、このスコープのポインタアドレスに直接書き込ませたい）
				//そのため、　ポインタ（adapter）のアドレスを　参照渡しする。　すると、受け取り側は、ポインタのアドレス＝　ポインタのポインタで受け取るようになる
			hr = pGIDevice->GetAdapter(&adapter);
			//エラーチェック
			if (FAILED(hr))
			{
				SAFE_RELEASE(adapter);
			}
			ERROR_CHECK(hr, "IDXGIDeviceからAdapter（アダプター）の取得失敗", "エラー");


			//Factory2の取得
			//factory = 工場
			//Direct2Dのリソース作成に用いるファクトリーオブジェクト（リソースを作る工場）
			IDXGIFactory2* dxgiFactory = nullptr;
			//アダプタに連携されているファクトリーをゲット
			hr = adapter->GetParent(__uuidof(IDXGIFactory2), reinterpret_cast<void**>(&dxgiFactory));
			//解放
				//参照カウンタのカウントダウン
			SAFE_RELEASE(adapter);

			//エラーチェック
			ERROR_CHECK(hr, "Adapterからファクトリーオブジェクトの取得失敗", "エラー");
		


			///////////////////////////スワップチェインの作成///////////////////////////////////////////
			//スワップチェインに与える情報
			//ここにおける情報は、SwapChainにて使用していた構造体ではなく、
				//SwapChain1専用の、同様のバージョンの構造体を使用する
			DXGI_SWAP_CHAIN_DESC1 props = {};
			props.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
			props.SampleDesc.Count = 1;
			props.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			props.BufferCount = 1;

			//スワップチェインの作成
			hr = dxgiFactory->CreateSwapChainForHwnd(
				pDevice,	//Direct3Dのデバイス
				hWnd,		//ウィンドウハンドル
				&props,		//スワップチェインの情報
				nullptr,
				nullptr,
				&pSwapChain1// 無事完成したSwapChainのアドレスが返ってくる
			);
			//解放
			SAFE_RELEASE(dxgiFactory);
			if (FAILED(hr))
			{
				SAFE_RELEASE(adapter);
			}
			ERROR_CHECK(hr, "SwapChain1の作成失敗", "エラー");

		}

		{
			///////////////////////////2DDevice作成の準備///////////////////////////////////////////
			//Direct2DのFactory（リソース作成工場）の作成
			Direct2D::CreateFactory();
			//Direct2DのDevice（全体監督）を作成
			Direct2D::CreateDevice();
			//Direct2DのDeviceContext（絵を描く人）を作成
			Direct2D::CreateDeviceContext();

			//デバイスコンテキストと
			//スワップチェーンの接続
			pSwapChain1->GetBuffer(0, __uuidof(pGISurface),
				reinterpret_cast<void**>(&pGISurface));
		}



		{
			///////////////////////////レンダーターゲットビュー作成///////////////////////////////
			//スワップチェーンからバックバッファを取得（バックバッファ ＝ レンダーターゲット）
			ID3D11Texture2D* pBackBuffer;
			hr = pSwapChain1->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
			//エラーメッセージ
			ERROR_CHECK(hr, "スワップチェインのバッファーの取得失敗", "エラー");

			//レンダーターゲットビューを作成
			hr = pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView);
			//エラーメッセージ
			ERROR_CHECK(hr, "レンダーターゲットビュー作成失敗", "エラー");

			//一時的にバックバッファを取得しただけなので解放
			SAFE_RELEASE(pBackBuffer);
		}

		///////////////////////////ビューポート（描画範囲）設定///////////////////////////////
			/*
				描画範囲

				//遠くのもの小さく見えて、近くのもの大きく見える
				//視野で、見える範囲が、広がっていく
				//→それを広がっているものを、視点から、前に長方形に切り取ったものとしてみさせる
				//→なので、遠くのものは小さく見える
			
			*/
		
			/*
				画面を複数用意する場合

				//描画する範囲のため、
				//複数画面を表示するときは、
				//複数個用意する
				//表示画面ごと範囲を用意する
			
			*/

		
		//レンダリング情報　範囲を設定する必要がある
			////深度バッファの結果を受け取らない場合、設定する必要がある
		/*
		//レンダリング結果を表示する範囲
		{
			//ビューポート（描画範囲の設定）
			D3D11_VIEWPORT vp;
			//幅（ウィンドウ幅）
			vp.Width = (float)winW;	
			//高さ（ウィンドウ高さ）
			vp.Height = (float)winH;
			//手前
				//詳細：ワールドの世界を　１ｍの範囲にギュッとつぶした （ビュー座標→プロジェクション座標）
				//詳細：プロジェクション行列における奥行きが0.0 ~ 1.0の範囲に縮小される。その際の奥行きの最小値
			vp.MinDepth = 0.0f;	
			//奥
				//詳細：プロジェクション行列における奥行きが0.0 ~ 1.0の範囲に縮小される。その際の奥行きの最小値
			vp.MaxDepth = 1.0f;	
								
			//左
			vp.TopLeftX = 0;	
			//上
			vp.TopLeftY = 0;	
		}
		*/
		//Main画面のデフォルト描画設定
		{
			//幅（ウィンドウ幅）
			vpBig.Width = (float)winW;
			//高さ（ウィンドウ高さ）
			vpBig.Height = (float)winH;
			//手前
				//詳細：ワールドの世界を　１ｍの範囲にギュッとつぶした （ビュー座標→プロジェクション座標）
				//詳細：プロジェクション行列における奥行きが0.0 ~ 1.0の範囲に縮小される。その際の奥行きの最小値
			vpBig.MinDepth = 0.0f;
			//奥
				//詳細：プロジェクション行列における奥行きが0.0 ~ 1.0の範囲に縮小される。その際の奥行きの最小値
			vpBig.MaxDepth = 1.0f;

			//左
			vpBig.TopLeftX = 0;
			//上
			vpBig.TopLeftY = 0;
		}
		//2画面目
		//BeginDraw１にて使用するビューポートサイズ
		{
			//例：
			//レンダリング結果を表示する範囲
				//画面受け取りウィンドウを10分の1にして、1分の1サイズで受け取る
				//→ピクセルが荒くなるので、10分の1のピクセル表示となるため、荒くなる（簡単にピクセル表示が可能（シェーダーなどよりも簡単にモザイク処理
				//	→シェーダーにてモザイクをおこなおうとすると、複雑な計算を大量に行う必要があるため、簡易的に表現できる））
			//vpSmall.Width = (float)winW / 10;	//幅
			//vpSmall.Height = (float)winH / 10;//高さ

			//幅
			vpSmall.Width = (float)winW;
			//高さ
			vpSmall.Height = (float)winH;
			//手前
			vpSmall.MinDepth = 0.0f;	
			//奥
			vpSmall.MaxDepth = 1.0f;
			//左
			vpSmall.TopLeftX = 0;	
			//上
			vpSmall.TopLeftY = 0;	
		}
		//Spriteにおけるテクスチャバッファとして描画する
			//→テクスチャのFilterが、POINTにすると、　ボケる。
			//→　　　　　　　　　　　LINEARにすると、ちかちかするような表現になる。


		{
			//深度ステンシルビューの作成(Zバッファ（前のものは前、後ろのものは後ろに描画）)
				//レンダリングの描画範囲を設定していないため、
				//詳細な設定をここで行う
			D3D11_TEXTURE2D_DESC descDepth;
			descDepth.Width = winW;
			descDepth.Height = winH;
			descDepth.MipLevels = 1;
			descDepth.ArraySize = 1;
			descDepth.Format = DXGI_FORMAT_D32_FLOAT;
			descDepth.SampleDesc.Count = 1;
			descDepth.SampleDesc.Quality = 0;
			descDepth.Usage = D3D11_USAGE_DEFAULT;
			descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			descDepth.CPUAccessFlags = 0;
			descDepth.MiscFlags = 0;
			pDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
			pDevice->CreateDepthStencilView(pDepthStencil, NULL, &pDepthStencilView);


			//データを画面に描画するための一通りの設定（パイプライン）
			//データの入力種類を指定
			pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			//描画先を設定
				//第３引数に深度ステンシルを入れて、Zバッファ（隠面消去）を渡す
			pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);
			//描画設定	
				//深度バッファを考慮したレンダリング情報、範囲を渡す
				//ポインタとして渡すため、アドレス渡す（関数がSetであるため、中身がいじられることはない(引数をconstにして確認したため、間違いはない(constの変数のアドレスを渡した場合、参照できるのかはわからないが、))）
			pContext->RSSetViewports(1, &vpBig);
		}

		{
			//シェーダーの準備
			hr = InitShader();
			//エラーメッセージ
			ERROR_CHECK(hr, "シェーダクラスの初期化の失敗", "エラー");
			/*
				エラーメッセージの結果

				エラーが返ってくると、
				メソッドの返り値として、HRESULTへE_FAIL（エラー）を返す

				そして、エラーが返っていき、
				最終的にMainへ帰り、
				Mainにて　プロジェクトをreturn0をし、プロジェクトの終了を示す
			*/
		}

		{
			//ブレンドの初期化
				//色の混ぜ方
			hr = InitBlend();
			//エラーメッセージ
			ERROR_CHECK(hr, "ブレンドクラスの初期化失敗", "エラー");
		}


		{
			//カメラの初期化
			Camera::Initialize();
		}


		{
			//描画先２の作成
			hr = CreateRenderTarget2();
			//エラーメッセージ
			ERROR_CHECK(hr, "描画先２の作成失敗", "エラー");
		}

		//処理の成功
		return S_OK;

	}


//初期化
HRESULT Direct3D::Initialize(int winW, int winH, HWND hWnd)

{
	//初期化
		//Direct3D初期化
		//Direct2D非対応
	//InitDirect3D(winW, winH, hWnd);

	//初期化
		//Direct3D初期化
		//Direct2D対応
	return InitDirect3DAndSupprtsDirect2D(winW, winH, hWnd);

}

//シェーダークラスのポインタを渡す
BaseShader * Direct3D::GetShaderClass(SHADER_TYPE type)
{
	return shaderBundle_[(int)type];
}

//ブレンドクラスのポインタを渡す
BaseBlend * Direct3D::GetBlendClass(BLEND_TYPE type)
{
	return blendBundle_[(int)type];
}

//レンダー先を追加（2画面用）
HRESULT Direct3D::CreateRenderTarget2()
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	{
		//レンダリングするためのテクスチャの初期化(描画先を新たに用意)
			//書き込み先の新しい画用紙を用意する
			//テクスチャcppにあった初期化をそのまま使用する
		//DESC = 説明
		D3D11_TEXTURE2D_DESC texdec;
		//ゼロメモリー　メモリ内０に初期化
		ZeroMemory(&texdec, sizeof(texdec));
		//画像の横幅
		texdec.Width = scrWidth;
		//仮に、格納値をWidthの10分の１にすると、
		//サイズが、10分の１の位置だけ、描画範囲になるので、画面が小さくなるのではなくて、　描画位置が小さくなる。
		//描画する範囲→ビューポートが、描画する範囲になる。　なので、その範囲を複数作ってやれば、　描画先を決めることができる。
	//画像の縦幅
		texdec.Height = scrHeight;
		//Mip　奥に行けば小さくなる
			//→テクスチャ、ポリゴンを、近くにいるときは、高解像度のテクスチャを用意して、それを距離ごとに入れていけば、処理を見た目的によく見せて、軽く（遠くに同じきれいな、テクスチャを使ってはもったいない）
		texdec.MipLevels = 1;
		texdec.ArraySize = 1;
		texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		texdec.SampleDesc.Count = 1;
		texdec.SampleDesc.Quality = 0;
		//必ずDefault
		texdec.Usage = D3D11_USAGE_DEFAULT;
		//リソースの使用方法を示す
		//DEFAULT＝　GPUからの読み取り、書き取りのみ可能
		//DYNAMIC＝　GPU,CPUのどちらからも読み取りをおこなえるようにする


	//このテクスチャを何に使用するのか
		//デフォルト＋　レンダーターゲットとして使用するとして（D3D11_BIND_RENDER_TARGET = プログラム側から絵を書き込めるようにする）
		texdec.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		//CPUへのアクセス詳細
		texdec.CPUAccessFlags = 0;
		//WRITE＝書き込み
		//READ＝読み込み
		//０＝CPUアクセスが不要

		//Usageにて選択したリソースの管理方法によっても、どのように管理するのかを指定することができる
			//Usageにて、DEFAULT＝GPUのみ良い。CPUからの書き込みを制限した。
			//＝そのため、ここでは、CPUの書き込む余地をなくすため、０が良いとされると考える。

		//ココをWRITEにしていると、自宅のPCでは動かない。
		//しかし、動くPCも存在する
			//自宅のPCでは０にすることで動かす（テクスチャを作ることが可能）
			//動かくなる理由は不明
		texdec.MiscFlags = 0;

		//テクスチャの作成
		hr = pDevice->CreateTexture2D(&texdec, NULL, &pRenderTexture);
		//エラーメッセージ
		ERROR_CHECK(hr, "描画先のテクスチャ作成失敗", "エラー");


		/*
			テクスチャが作成できない

			//テクスチャを作成することができない
			//原因を調べる
			//CreateDeviceAndSwapChainにて、　デバックログ出力のフラグを入れて検証
			//→結果、　作成時に例外が投げられ、　エラーとして_com_errorとなっていた、なので、　COMの使用宣言がされていない段階なので、エラーが出ているのでは？と考える。
			//なので、一度、テクスチャ作成前に、COMの宣言を行う

			//→結果、治らず
			//https://social.msdn.microsoft.com/Forums/SECURITY/ja-JP/70031db2-19d9-424d-b18c-462070e8cfce/directx1112288300641239412427124871249612452124733829112398124861?forum=vcgeneralja

			//このエラーが、
			//自宅のPC時にのみ、起こってしまう
			//こうゆうものを避けなければ、
			//提出先の企業でのPCで動かないという事態が起こってしまう可能性がある。

		*/
	}


	{
		//レンダーターゲットビュー作成
			//レンダーターゲットを創ったら、
			//もう一つ専用のレンダーターゲットビューを作る必要がある。（橋渡し＝　書き込みのプリンターのようなもの）

		//レンダーターゲットを作る
		//レンダーターゲットビューの設定を行う
		D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
		ZeroMemory(&renderTargetViewDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
		renderTargetViewDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		renderTargetViewDesc.Texture2D.MipSlice = 0;

		//レンダーターゲットの作成
		hr = Direct3D::pDevice->CreateRenderTargetView(
			pRenderTexture, &renderTargetViewDesc, &pRenderTargetView2);
		//エラーメッセージ
		ERROR_CHECK(hr, "レンダーターゲットビューの作成失敗失敗", "エラー");

		//テクスチャバッファの書き込み先
			//ゲーム内に描画するために必要なクラスへわたす
		pScreen = new Sprite;
		//バッファーを渡し初期化
		hr = pScreen->Initialize(pRenderTexture, SHADER_TYPE::SHADER_2D);
		//エラーメッセージ
		ERROR_CHECK(hr, "Spriteクラスの作成失敗失敗", "エラー");

	}


	//処理の成功
	return S_OK;
}

//ブレンド（色の混合）クラスの初期化
HRESULT Direct3D::InitBlend()
{

	//ブレンドクラス（継承先）のインスタンスの生成
	/*ブレンド情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
	//継承元のBaseBlend型にてキャスト

	//生成クラスの作成
	BlendFactory* pBlendFactory = new BlendFactory;

	//生成クラスの生成関数先にて、以下の処理を行う
		/*blendBundle_[BLEND_ALPHA ~] = new AlphaBlendForAlphaExpression;*/

	//それぞれのブレンドクラスの初期化を呼び出す
	//条件：BLEND_MAX（enum）
	for (int i = 0; i < (int)BLEND_TYPE::BLEND_MAX; i++)
	{
		//ブレンドクラスの生成
			//関数先にて、引数にて渡した番号から、生成するクラスを識別して、クラスを生成、そのインスタンスを返してもらう
		blendBundle_[i] = pBlendFactory->CreateBlendClass((BLEND_TYPE)i);

		//初期化の呼び込み
		HRESULT hr = blendBundle_[i]->Initialize();

		//エラーメッセージ
		if (FAILED(hr))
		{
			//作成エラー時　解放
			SAFE_DELETE(pBlendFactory);
		}
		ERROR_CHECK(hr, "ブレンド（継承先）クラスの初期化失敗", "エラー");

	}

	//解放
	SAFE_DELETE(pBlendFactory);

	//初期ブレンドをセット
	SetBlendBundle(BLEND_TYPE::BLEND_ALPHA_FOR_ALPHA);

	//処理の成功
	return S_OK;
}

//ブレンドを適用
void Direct3D::SetBlendBundle(BLEND_TYPE blendType)
{
	blendBundle_[(int)blendType]->SetBlend();
}



//シェーダー準備
	//シェーダー３D,２D　どちらにも対応できるように
	//ラスタライザなど、が、２Dにおいても、特に設定はひつようないが、　
	//同じものでも、２D用３D用を作っておく（構造体）
HRESULT Direct3D::InitShader()

{
	//シェーダークラス（継承先）のインスタンスの生成
	/*シェーダー情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
	//継承元のBaseShader型にてキャスト

	//生成クラスの作成
	ShaderFactory* pShaderFactory = new ShaderFactory;


	//生成クラスの生成関数先にて、以下の処理を行う
		/*shaderBundle_[SHADER_CHOCO_FASHION] = new ChocolateFashionShader;*/
	

	//それぞれのシェーダーの初期化を呼び出す	
		//条件：SHADER_MAX（enum）
	for (int i = 0; i < (int)SHADER_TYPE::SHADER_MAX; i++)
	{
		//シェーダークラスの生成
			//関数先にて、引数にて渡した番号から、生成するクラスを識別して、クラスを生成、そのインスタンスを返してもらう
		shaderBundle_[i] = pShaderFactory->CreateShaderClass((SHADER_TYPE)i);
		
		//初期化の呼び込み
		HRESULT hr = shaderBundle_[i]->Initialize();
		
		//エラーチェック
		if (FAILED(hr))
		{
			//エラー時　解放
			SAFE_DELETE(pShaderFactory);
		}
		ERROR_CHECK(hr, "シェーダー（継承先）クラスの初期化失敗", "エラー");

	}

	//解放
	SAFE_DELETE(pShaderFactory);


	//初期シェーダーのセット
	SetShaderBundle(SHADER_TYPE::SHADER_3D);

	//処理の成功
	return S_OK;
}
//シェーダーポインタに、３D,２D　シェーダーを選んでセット
void Direct3D::SetShaderBundle(SHADER_TYPE type)
{
	/*シェーダー情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
	//クラスに持つ、
	//コンテキストに対して、情報を持たせる処理を行う関数　を呼びこみ、
		//それぞれのクラスのシェーダー情報を適用させる
	shaderBundle_[(int)type]->SetShader();

}

//ゲーム内背景色の設定
void Direct3D::SetBackGroundColor(XMVECTOR& rgba)
{
	clearColor_[R] = rgba.vecX;
	clearColor_[G] = rgba.vecY;
	clearColor_[B] = rgba.vecZ;
	clearColor_[A] = rgba.vecW;
}


//描画開始
	//描画のバックバッファの絵を用意
	//バックバッファへの書き込みであるため、
	//現在画面に映っている画面のバッファは前回のフレームにて描画したものであり、今回書き込むバッファはそのバッファではない。
	//現在描画されていないバッファへの書き込みである。
		//書込みだけでは描画とならず。EndDrawにおいて、前回のバッファと今回の書き込んだバッファを入れ替える。スワップする作業が必要。
void  Direct3D::BeginDraw()
{
	//複数のレンダーターゲットへの書き込みを実現するために
	//マルチレンダーターゲット
	/*
	・レンダーターゲット、レンダーターゲットビューの用意
	・テクスチャクラスに、D3D11型のテクスチャ２Dから、サンプラーなどの情報をもらい、初期化するクラスの取得
	・Textureクラスを使って、Spriteクラスを作成、
	・Spriteクラスをスワップチェインで入れ替えている画像へ描画することで、新しく作成した、レンダーターゲットの描画が完了

	*/

	//描画範囲を決める
		//各設定した描画範囲を適用する
	pContext->RSSetViewports(1, &vpBig);

	//描画先（レンダーターゲットビュー）を指定する
		//引数：描画の橋渡しを行うレンダーターゲットビュー
		//引数：深度バッファ（Zバッファ）→テクスチャのサイズを変えるときは、深度バッファの設定も変える必要があるのだが、今回は使いまわす
	pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);            // 描画先を設定
	
	//ゲーム内背景色の更新
	SetBackGroundColor(GameDatas::gameBackGroundColor_);
	//ゲーム内背景色を取得し
		//ゲーム内背景色色で画用紙（画面）をクリア
	pContext->ClearRenderTargetView(pRenderTargetView, clearColor_);

	//カメラのUpdate
		//毎フレーム呼ばれるようにしたい（カメラは、ゲームの中のことなので、Direct3Dに、入れる(CameraのInitializeも、ゲームに関することなので、)）
		//毎フレーム呼ばれているのは、Beginなので、
		//画面を切り替えた後に、カメラを更新すれば、移動の後を見ることができる
		//EndDrawで、入れ替えるので、その前にカメラを更新
	Camera::Update();		


	//深度バッファクリア（初期化）
		//カメラの距離を登録させるZバッファの塗った配列の初期化
		//各描画ピクセルの深度をピクセルごとに値として持たせる→９９９９でカメラからの距離を登録する配列初期化をする、クリアを行わなければいけない
		//	→だが、→９９９９というのは、この距離より前だったら、書き込みとするやつなので、→この値を、カメラの見える最大距離にしておけば、
		//	→それよりも遠いものは、描画しないといった、形で描画をすることが可能（D3D11_CLEAR_DEPTH）
		//引数：深度バッファ
		//引数：深度限界値
	pContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

}

//2画面目の描画開始
	//描画のバックバッファの絵を用意
	//2画面目の描画情報、レンダーターゲットをSpriteクラスのテクスチャバッファへわたして、
	//描画内容を更新
	//Spriteを描画することで、2画面目を平面として描画

	//現在の実装だと、
	//BeginDraw2を呼び出しているタイミングにて、　BeginDrawにて描画した画面をSpriteに登録して（Spriteのテクスチャがレンダーターゲットになっているので）、それを描画して、2つ目の描画を開始している感じ。
	//レンダーターゲットへの描画をBeginDrawにて宣言され、Drawにて、そのレンダーターゲットへの描画が行われる。
	//そして、BeginDraw2にて、そのレンダーターゲットをSpriteとして画面へ描画（この際に、これまでのレンダーターゲットへの描画内容は削除されている。）
	//BeginDraw2にて本当の画面用の描画が行われる。？

	//正しい描画のイメージとしては、
	//2画面目のイメージとしては、2画面目の描画を開始を宣言し
	//2画面目の描画内容をバッファ、テクスチャとして取得（レンダーターゲットへ書き込む）
	//その書込み内容をSpriteに更新。
	//Spriteを最後のBeginDraw2のEndDraw２のタイミングで画面へ描画
void  Direct3D::BeginDraw2()
{
	//2画面目の描画範囲を指定
	pContext->RSSetViewports(1, &vpBig);

	//バックバッファをテクスチャとして取得し
	//描画
	Transform trans;
	//trans.position_.vecX = -0.5f;
	//trans.position_.vecY = 0.5f;
	//trans.scale_.vecX = 0.5f;
	//trans.scale_.vecY = 0.5f;
	trans.Calclation();

	//2画面目描画
	pScreen->Draw(trans);



	//ゲーム内背景色の更新
	SetBackGroundColor(GameDatas::gameBackGroundColor_);
	//ゲーム内背景色を取得し
		//ゲーム内背景色色で画用紙（画面）をクリア
	pContext->ClearRenderTargetView(pRenderTargetView, clearColor_);


	//描画先を指定する
	pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);


	//カメラのUpdate
	Camera::Update();


	//深度バッファクリア
	pContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

}





//描画終了
	//スワップチェインによって、バックバッファを入れ替える
	//（描画されたものと、前回の画面を入れ替える→描画）
	//BeginDrawにおいては、裏のバッファへの描画でしかない。
	//そのため、書き込んだ裏のバッファと表のバッファを切替え
HRESULT Direct3D::EndDraw()
{
	//スワップチェインによる画面入れ替え
		//現在のフロントバッファ（前回のフレーム書き込んだもの）を 
		//バックバッファ（今回のフレームに書き込んだもの）にスワップ（バックバッファを表に表示する）
	HRESULT hr = pSwapChain1->Present(0, 0);
	//エラーチェック
	ERROR_CHECK(hr, "スワップチェイン失敗", "エラー");


	//上記をFPSに沿って行うことで、
	//フレームの描画が完了する

	//処理の成功
	return S_OK;
}

//描画終了（2画面用）
HRESULT Direct3D::EndDraw2()
{
	//Spriteなどの描画終了
	//或いは描画を示す

	//詳しくはBeginDraw2にて
	//BeginDraw2の処理の後始末、後処理を行って2画面を実現する



	//処理の成功
	return S_OK;
}


//Zバッファへの書き込みのON/OFF
	//奥行き設定のON,OFFの設定
void Direct3D::SetDepthBafferWriteEnable(bool isWrite)
{
	//ON
	if (isWrite)
	{
		//Zバッファ（デプスステンシルを指定する）
		pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);
	}
	//OFF
	else
	{
		pContext->OMSetRenderTargets(1, &pRenderTargetView, nullptr);
	}

}

//連携用スワップチェイン１を渡す
IDXGISwapChain1* Direct3D::GetSwapChain1()
{
	return pSwapChain1;
}

//連携用DXGIDeviceを渡す
IDXGIDevice * Direct3D::GetIDXGIDevice()
{
	return pGIDevice;
}

//連携用デバイスコンテキストとスワップチェーンの接続を担うポインタを取得
IDXGISurface * Direct3D::GetIDXGISurface()
{
	return pGISurface;
}


//解放処理
void Direct3D::Release()
{
	//後に確保したものから解放
	pScreen->Release();
	SAFE_DELETE(pScreen);
	SAFE_RELEASE(pRenderTargetView2);

	//ブレンドクラスの解放
	//条件：BLEND_MAX（enum）
	for (int i = 0; i < (int)BLEND_TYPE::BLEND_MAX; i++)
	{
		/*ブレンド情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
		//インスタンスの解放
		SAFE_DELETE(blendBundle_[i]);
	}

	//シェーダークラスの解放
	//条件：SHADER_MAX（enum）
	for (int i = 0; i < (int)SHADER_TYPE::SHADER_MAX; i++)
	{
		/*シェーダー情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
		//インスタンスの解放
		SAFE_DELETE(shaderBundle_[i]);
	}

	//解放処理　Relese()
		//ID3D型は参照カウント方式を使用しているため、解放にはReleaseを使用する。
		//自らDeleteを呼ぶことはできない
	SAFE_RELEASE(pDepthStencilView);
	SAFE_RELEASE(pDepthStencil);
	SAFE_RELEASE(pRenderTargetView);
	SAFE_RELEASE(pGIDevice);
	SAFE_RELEASE(pGISurface);
	SAFE_RELEASE(pSwapChain1);
	SAFE_RELEASE(pContext);
	SAFE_RELEASE(pDevice);
}