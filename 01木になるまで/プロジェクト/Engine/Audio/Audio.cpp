#include <xaudio2.h>							//XAudio2ヘッダ　音楽再生
#include <vector>								//可変長配列
#include <stdlib.h>								//標準ライブラリ
#include "Audio.h"								//ヘッダ
#include "../../CommonData/GlobalMacro.h"		//共通マクロ


//音楽のSourceVoice解放処理
#define SAFE_DESTROY_VOICE(p) if(p != nullptr){p->DestroyVoice(); p = nullptr;}


/*
	謎のエラーと発生原因

	デバッグ時
	pdbが読み込めないというエラーが出た

	仮対処として
	//プロパティにおいて、
	//コード生成→ランタイムライブラリ→　DLL付きにすると、　pdbが読み込めないというエラーは出ない
	//だが、DirectXを使う上で、DLLなしにしないとエラーになるので、pdbのエラーが出ないようにしなければいけない。

	//或いは、
	//Microsoft〜サーバーから、　シンボルを読み込むとpdbをインストールするので、エラーは通らなくなる


	エラーが発生する原因は
	ソースボイスの解放のし忘れであった

	解放時に、
	音源が再生中であったり、完全に解放を終えていない状態で処理を終えようとしてエラーになっていた。
	再生できるように、解放処理をきちんと行うことでエラー回避へとつながった。
*/


/*Audio*/
namespace Audio
{
	/*
	構造体詳細	：音楽ファイルのチャンク格納構造体
	構造体概要（詳しく）
				：音楽ファイル読み込み時の、読み込みファイルの状態を格納する
				：idなどには、読み込みファイルの拡張子を文字列で受け取るなど、ファイルを識別する際に使用する
	*/
	struct Chunk
	{
		//ID
			//制限：（IDはchar４つの4バイトと決まっている）
		char	id[4]; 		
		// サイズ
			//制限：（サイズがマイナスはない）
		unsigned int	size;	
	};

	/*
	構造体詳細	：音楽情報　構造体
	構造体概要（詳しく）
				：演奏者に演奏させる音楽情報と、演奏者を持たせておく構造体
				：演奏者（ソースボイス）が複数必要である場合があるため、複数所有のために、配列のポインタ（ポインタのポインタ）にて、１つの音楽ファイルに対して複数演奏者を確保できるようにしておく。
	*/
	struct AudioData
	{
		//オーディオファイルのバッファ
			//データ部の情報
		XAUDIO2_BUFFER buf = {};

		//ソースボイスポインタのポインタ
		//オーディオファイルの演奏者
			//詳細：ソースボイスを一度に短い時間で再生したい場合（SE）、
			//　　：ソースボイスを1つのファイルに複数持たせておく。
			//　　：音楽が再生された時、1人目が音楽を再生。再生終了前に、また同じ音楽を再生したい場合、1人目は仕事中のため音楽を再生することはできない。
			//　　：上記を踏まえて、1人目が仕事中でも同様の音楽を鳴らすときのために、2人目、3人目を用意しておく。
			//　　：何個作るのかは、Load時の引数にてもらえばよい。
		IXAudio2SourceVoice** ppSourceVoice;

		//フォーマットチャンク（WAVE用）
		WAVEFORMATEX	fmt;

		//ソースボイス（演奏者）の作成個数
		int createNum;

		//音楽ファイル名
		std::string fileName;

		//コンストラクタ
		AudioData() :
			ppSourceVoice(nullptr)
		{
		}

	};


	//ステージ（演奏におけるステージ（舞台））（XAudio2本体を表すポインタ）
	IXAudio2* pXAudio = nullptr;
	//指揮者
		//詳細：楽譜の作成者、これを演奏してという人。楽譜の演奏があり（別クラス）、その演奏者がいて（別クラス）、全体を監督する指揮者。
	IXAudio2MasteringVoice* pMasteringVoice = nullptr;



	//音楽情報群（別名：演奏者群）
		//詳細：上記の楽譜、演奏者、の情報（AudioData(struct)）をベクター配列で取得し、追加していく
	std::vector<AudioData*>	audioDatas;

	//データ群の中から指定のデータを解放
		//引数：解放させたいデータを示す添え字
		//戻値：なし
	void ReleaseOneData(int handle);

	//データの共有部分をコピーさせる処理
		//引数：コピー元の添え字番号（Vector配列におけるコピー元の添え字）
		//引数：ソースボイスの作成数
		//引数：ファイル名
		//戻値：ハンドル番号
	int ShareData(int original, int create, std::string fileName);


}



//オーディオの準備・初期化
	void Audio::Initialize()
	{
		/*
			COMを使用する際の宣言

			//COMの宣言
			//テクスチャクラスにてすでに宣言を行っている場合、2度宣言行う必要はない
			//プロジェクトの中で1回で充分
			//テクスチャクラスで作成した記憶があるので、エンジンに組み込むときは、これはいらない。
		
			//GameEngine上に一つあれば十分
			//CoInitializeEx(0, COINIT_MULTITHREADED);

			Direct3Dや、
			Mainなど、処理の初期段階にて必ず行う段階にて必ず宣言。
			処理の最終段階（プロジェクト終了）にて必ず解放を宣言すると、忘れずに済む。
		
		*/
	
		//XAudio2本体（ステージ）作成,初期化
			//演奏者など、指揮者を置くためのステージ（舞台）の用意
		XAudio2Create(&pXAudio);	

		//指揮者作成,初期化
		pXAudio->CreateMasteringVoice(&pMasteringVoice);


		//ソースボイスは音ごとに作るため、音楽ファイルロード時に作成。
			//ソースボイス＝演奏者なので、音楽ファイルごと、個別に再生させる演奏者分作成が必要

		//演奏者群（Vector）を初期化
		audioDatas.clear();

	}

	//音楽ファイルのロード関数
	int Audio::Load(const std::string& FILE_NAME , int create)
	{
		//条件：ファイルが存在しない場合
			//第一引数：ファイルのパス　を示す変数のポインタ（string型はc_str()にて、　自身のポインタを送ることが可能）
		if (!PathFileExists(FILE_NAME.c_str()))
		{
			//ファイルが存在しないとき
				//−１　：　エラーを返す
			return -1;	
		}

		//使用デザインパターン：フライウェイト
		//ロードするデータが、既存データ内に存在するか
		{
			//引数にてもらったファイル名がすでにロードされているものなのかを調べる
				//ロード済み：ロードしてあるファイルを共通して使う
							//この時オーディオ構造体のbufに、オーディオのバッファ（データ）が入っているので、それをコピーする
							//ソースボイスは改めて作らなければいけない、なぜなら、演奏者なので、仮にオーディオデータが同じだとしても、演奏者を元居るソースボイスにお願いすることになると、
							//元居るソースボイスが自分自身のオーディオファイルを鳴らすときに、現在再生中のため再生できないとなってしまう、そのため、演奏者であるソースボイスは改めて作らなければいけない
				//ロード未　：新たにロード


			//同様のファイル名を探す
			//同様のファイルがあった場合、共有部分をコピーし（コピーというよりポインタを取得）ハンドル番号を返す
			//条件：音楽情報群数分
			for (int i = 0; i < audioDatas.size(); i++)
			{
				//条件：音楽情報群における一音楽情報内のファイル名と引数ファイルが同じなら
				//すでに同一のファイルが存在していたらロードしない
					//同一ファイルの判断は、ファイル名から判断
				if (audioDatas[i]->fileName == FILE_NAME)
				{
					//データ（音楽データ部）の共有部分をコピーさせる処理
						//関数内にて、データをVector配列に登録し、
						//そして、登録時のハンドル番号を返す
						//戻値をLoadの関数の戻値として処理を終了させる
					return ShareData(i, create, FILE_NAME);

				}

			}
		}




		//新規にオーディオデータをロード
			//上記の繰り返し分にて、既存のデータ内に同様のデータがなかった。
		{
			//ファイルハンドル
			HANDLE hFile;
			//ファイルインスタンスの作成
				//ファイルを開く 
				//ファイルのロード、書き込みの場合でも必ずCreateFileを使用する
			hFile = CreateFile(
				FILE_NAME.c_str(),      //ファイル名(文字列型のポインタは、関数で専用の関数があるのでそれを使用してポインタを返す)
				GENERIC_READ,           //アクセスモード（読み込み）
				0,                      //共有（なし）
				NULL,                   //セキュリティ属性（継承しない）
				OPEN_EXISTING,          //読み込み方法
				FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
				NULL);                  //拡張属性（なし）


			//ファイル情報のチャンクを読み込む
				//ファイルの詳細情報、ファイル拡張子を調べる

			//データサイズ
			DWORD dwBytes = 0;

			//チャンクの構造体
			Chunk riffChunk;
			
			//Chunkの構造体に書き込む
			//引数：ファイルハンドル
			//引数：チャンク格納先のアドレス
			//引数：書き込みサイズ　（8バイト分取得（IDとサイズ部分））
			//引数：今回読み込んだサイズ
			ReadFile(hFile, &riffChunk, 8, &dwBytes, NULL);
				/*
					WAVEファイルなどをテキストエディタなどで開くと
					RIFF　WAVE　など文字にて識別子が記述されているため、
					上記のデータを取得する
				*/
			
			//チャンク比較文字の文字数
			static constexpr UINT CHUNK_CHAR_NUM = 4;

			//取得チャンク　比較用文字配列
			char checkT[CHUNK_CHAR_NUM] = { 'R' ,'I' , 'F' , 'F' };

			/*
				文字の識別

				//文字列とcheckTの判別
				//if (riffChunk.id != checkT)
				//{
				//	//このAudioクラスで再生できるものではないのでー１（エラー）を返す
				//	return -1;
				//}

				//上記の判別は
				//＝＝にはならない
					//なぜなら配列の添え字ナシは、→その配列の先頭アドレスだから(配列の添え字ナシを関数の引数に渡したとき、関数受け取り側でarray[]で受け取ればポインタ受け取りのようになるのは、それが理由)
					//先頭アドレスと、別の配列の先頭アドレスでは、当然＝＝にはならない。

					//中身が同じかどうかを判断しないといけない。
			*/
			
			//取得文字列と比較文字を一文字ずつ調べる
			//条件：チャンク比較文字の文字数　分
			for (int i = 0; i < CHUNK_CHAR_NUM; i++)
			{
				//条件：文字が違う場合
				if (riffChunk.id[i] != checkT[i])
				{
					//ロードしようとしたファイルは
						//自Audioクラスで再生できるものではないのでー１（エラー）を返す
					return -1;
				}
			}

			/*
				ReadFile(x , x , x , m , x);

				第4引数について

				//dwBytesには、今回読み込んだデータのサイズが入ってくる。
				//それでもReadFileに何回も読み込ませると、読み込み位置を移動してくれる。
				//それは、hFileのハンドルの読み込み位置をReadFileが移動しているから。
				//dwBytesの値は、あくまで、今回読み込んだバイト数


				//データが
				//24718バイトのデータサイズ

				//構造体のsizeには、
				//24710が入ってくる　これはつまり、　IDとサイズの8バイトを抜いたサイズがデータ部分だということ。
			
			*/
			//次のReadFileは、
			//上記でデータID,サイズを受け取った後に、そのあとのデータから、
				//８バイト後から読み込む。
			

			//ファイルの種類を取得
				//詳細：音楽ファイルの拡張子は.wavであるのか、WAVEデータであるのか　
				//　　：WAVEとなれば、WAVEファイルの再生を行うようにすればよい
				//　　：WAVEでなければ、再生読み込みしないように。（自Audioクラスにて再生不可能）
			//ファイルの種類を受け取る文字配列
			char wave[CHUNK_CHAR_NUM];
			
			//ファイルから
				//ファイルの種類を記述した場所から文字を取得する
			ReadFile(hFile, &wave, CHUNK_CHAR_NUM, &dwBytes, NULL);
			

			//比較文字配列
			char checkW[CHUNK_CHAR_NUM] = { 'W' ,'A' , 'V' , 'E' };

			//取得文字列と比較文字を一文字ずつ調べる
			//条件：チャンク比較文字の文字数　分
			for (int i = 0; i < CHUNK_CHAR_NUM; i++)
			{
				//条件：文字が違う場合
				if (wave[i] != checkW[i])
				{
					//ロードしようとしたファイルは
						//自Audioクラスで再生できるものではないのでー１（エラー）を返す
					return -1;
				}
			}


			//フォーマットチャンク
				//第3引数：8バイト分（8バイトは　フォーマットチャンクのID,サイズが入っている）
			Chunk formatChunk;
			//Chunkの構造体に書き込む
			ReadFile(hFile, &formatChunk, 8, &dwBytes, NULL);


			//WAVE フォーマット
			WAVEFORMATEX	fmt;
			//取得したフォーマットチャンクのデータ部のサイズ
				///第3引数：取得したデータ部サイズ分だけ
			ReadFile(hFile, &fmt, formatChunk.size, &dwBytes, NULL);

				//nChannels １モノナル、0ステレオ
				//〜〜　＝　1秒間に何回書き換えているか
				//＝＝　＝　1個１６Bit


			//データチャンク（音楽データ部）
			Chunk data;
			//Chunkの構造体に書き込む
				//引数：8バイト分でID,サイズを取得
			ReadFile(hFile, &data, 8, &dwBytes, NULL);


			//音楽データ部のサイズ分データを読み込む
				//詳細：取得データは　char*へ格納
				//　　：サイズ分のデータが欲しいので、
				//　　：ポインタ配列の要素数は、データサイズ分取得
				//　　：動的確保でないと変数で配列の要素数を宣言できないので注意
			char* pBuffer = new char[data.size];
			//char* pBuffer = (char*)malloc(data.size);
			//ZeroMemory(pBuffer, data.size);


			//第2引数：pBufferはポインタなので＆つけない
			ReadFile(hFile, pBuffer, data.size, &dwBytes, NULL);


			//ハンドル閉じる
				//データは取得したのでファイルを閉じる
			CloseHandle(hFile);



			//演奏者の情報を
			//構造体で取得する
			AudioData* ad = new AudioData;
			{
				//ソースボイス作成数を登録
				ad->createNum = create;
				//ファイル名を登録
				ad->fileName = FILE_NAME;

				//ソースボイス（演奏者）を複数作成
					//引数でもらったソースボイス作成数分ポインタの複数個確保
				ad->ppSourceVoice = new IXAudio2SourceVoice*[create];

				//フォーマットチャンクの登録
					//ロード済みのデータをさらにロードすることを防ぐために、
					//すでにあるデータを共通して使うようにする、
					//そのためには、ソースボイスを作成するときに、フォーマットチャンクが必要になる、そのために所有
				ad->fmt = fmt;

				//ソースボイス指定数分作成
				//条件：ソースボイス（演奏者）分
				for (int i = 0; i < create; i++)
				{
					//実体を持つのではなく、
					//ポインタを渡して、そのアドレスに要素を入れてくれる。
					//ad.pSourceVoice[i] = new IXAudio2SourceVoice;

					//演奏者の作成,初期化
						//データファイルごと、ソースボイスを作るようにする
						//引数にて、フォーマットチャンクを使用する
					pXAudio->CreateSourceVoice(&ad->ppSourceVoice[i], &fmt);

				}
				//オーディオのバッファ部分
					//データ部分
					//Byte型のポインタ。サウンドデータのデータ部を表している。すでにロード済みのデータは、このポインタを共通して扱うようにする
				ad->buf.pAudioData = (BYTE*)pBuffer;	
				ad->buf.Flags = XAUDIO2_END_OF_STREAM;
				ad->buf.AudioBytes = data.size;


				//音楽情報群に追加
				audioDatas.push_back(ad);
			}


			//今回追加分の音楽ファイルへのハンドルを返す
				//ベクター配列の一番後ろに追加したので、
				//現在のベクター配列の要素の数ー１　で、ベクター配列にデータを追加したときの添え字を返すことができる
			return (int)(audioDatas.size()) - 1;

			//以上の番号でModel.cppのように
				//ソースボイスを番号（ハンドル）で管理することが可能になるので、
				//Play（）の時に、ソースボイス番号（ハンドル）を渡してやれば、その番号によって再生するソースボイスを識別すればよい

		}


	}

	//データの共有部分をコピーさせる処理
	int Audio::ShareData(int original , int create , std::string fileName)
	{
		//演奏者のデータをオリジナルからコピー

		//演奏者の情報を
			//構造体で取得する
		AudioData* ad = new AudioData;
		{
			//ソースボイス作成数を登録
			ad->createNum = create;
			//ファイル名を登録
			ad->fileName = fileName;

			//ソースボイス（演奏者）を複数作成
			//ソースボイスはコピー元データを共通して使うわけにはいかない。
					//あくまで共通して使いたいのは、ロードをしなくてはいけないオーディオデータ部分。演奏者を共通して使ってしまうと、コピー元のオーディオデータを再生するときに不具合が起こる（ほかの人のオーディオを鳴らしている状態になる）
				//引数でもらったソースボイス作成数分ポインタの複数個確保
			ad->ppSourceVoice = new IXAudio2SourceVoice*[create];

			//フォーマットチャンクの登録
				//ロード済みのデータをさらにロードすることを防ぐために、
				//すでにあるデータを共通して使うようにする、
				//そのためには、ソースボイスを作成するときに、フォーマットチャンクが必要になる、そのために所有
			ad->fmt = audioDatas[original]->fmt;


			//ソースボイス指定数分作成
			//条件：ソースボイス（演奏者）分
			for (int i = 0; i < create; i++)
			{
				//実体を持つのではなく、
				//ポインタを渡して、そのアドレスに要素を入れてくれる。
				//ad.pSourceVoice[i] = new IXAudio2SourceVoice;


				//演奏者の作成,初期化
				//データファイルごと、ソースボイスを作るようにする
				//引数にて、フォーマットチャンクを使用する
				pXAudio->CreateSourceVoice(&ad->ppSourceVoice[i], &ad->fmt);


			}
			//オーディオのバッファ部分
				//データ部分
			//データ部のコピー(コピー元のオーディオデータからコピー)
			ad->buf.pAudioData = audioDatas[original]->buf.pAudioData;	//Byte型のポインタ。サウンドデータのデータ部を表している。すでにロード済みのデータは、このポインタを共通して扱うようにする
			ad->buf.Flags = audioDatas[original]->buf.Flags;
			ad->buf.AudioBytes = audioDatas[original]->buf.AudioBytes;

			//音楽情報群に追加
			audioDatas.push_back(ad);
		}


		//今回追加分の音楽ファイルへのハンドルを返す
			//ベクター配列の一番後ろに追加したので、
			//現在のベクター配列の要素の数ー１　で、ベクター配列にデータを追加したときの添え字を返すことができる
		return (int)(audioDatas.size()) - 1;

		//以上の番号でModel.cppのように
			//ソースボイスを番号（ハンドル）で管理することが可能になるので、
			//Play（）の時に、ソースボイス番号（ハンドル）を渡してやれば、その番号によって再生するソースボイスを識別すればよい

	}

	//再生
	void Audio::Play(int hundle)
	{
		//楽譜ごと（引数ハンドルにて指定）
		//演奏者（ソースボイス）を指定して、演奏させる

		//�@ハンドルより音楽情報群から演奏者情報を指定
		//�A演奏者（ソースボイス）が複数存在するので、一人一人の演奏者に現在演奏中か調べる
		//�B演奏中でない場合、その演奏者（ソースボイス）へ演奏を依頼
		/*
			//ソースボイス個数分
			//繰り返して
				//連続して同じソースボイスに再生が呼ばれたとき
				//→一人のソースボイスは、音を再生中の時、再生が終わるまで次の再生を行うことができない（どんどんスタックがたまっていくイメージ）
			//なので、複数個ソースボイスを持たせているならば、
				//ソースボイスを頭から見ていって、
				//そいつが現在再生中のソースボイスなら、次のソースボイスに移って、
				//再生中でないソースボイスを見つけて、そのソースボイスに再生させる。→そうすれば、連続して再生することが可能になる。
		*/
		//�@
		//�A
		//条件：演奏者のソースボイス数　分
		for (int i = 0; i < audioDatas[hundle]->createNum; i++)
		{
			//現在参照するソースボイスの状態
			XAUDIO2_VOICE_STATE state;
			audioDatas[hundle]->ppSourceVoice[i]->GetState(&state);

			//条件：演奏中でないならば
				//：１＝演奏中
				//：０＝演奏していない
				//演奏中ならば、次へ。　次のソースボイスの同様の条件判断を行う
			if (state.BuffersQueued == 0)
			{
				//鳴らす
				
				//再生データ専用の変数に渡した、
				//楽譜を演奏者に渡す
				audioDatas[hundle]->ppSourceVoice[i]->SubmitSourceBuffer(&audioDatas[hundle]->buf);
				//再生
				audioDatas[hundle]->ppSourceVoice[i]->Start();

				//処理を抜ける
					//スコープ：�@
				break;

			}
		}

	}
	//停止
	void Audio::Stop(int hundle)
	{
		//ソースボイスすべてに停止宣言
		//条件：演奏者のソースボイス数　分
		for (int i = 0; i < audioDatas[hundle]->createNum; i++)
		{
			{
				//停止
				audioDatas[hundle]->ppSourceVoice[i]->Stop();
				//ソースボイスに関連付けた、バッファを解放
				audioDatas[hundle]->ppSourceVoice[i]->FlushSourceBuffers();

			}
		}


	}
	//無限ループを設定
	void Audio::InfiniteLoop(int handle)
	{
		//参考サイト：
		//http://dvdm.blog134.fc2.com/blog-entry-50.html


		//ループ回数
			//ループカウントをInfinite=無限にする
		audioDatas[handle]->buf.LoopCount = XAUDIO2_LOOP_INFINITE;

	}
	//無限ループを止める
	void Audio::NotInfiniteLoop(int handle)
	{
		//参考サイト：
		//http://dvdm.blog134.fc2.com/blog-entry-50.html

		//ループ回数
			//ループをしない
		audioDatas[handle]->buf.LoopCount = 0;
		audioDatas[handle]->buf.LoopBegin = 0;
		audioDatas[handle]->buf.LoopLength = 0;

	}
	//音量変更
	HRESULT Audio::SetVolume(int handle , float volume)
	{
		//参考サイト：
		//http://dvdm.blog134.fc2.com/blog-entry-46.html

		/*
		SourceVoiceのメンバ

		HRESULT SetVolume(
			float Volume,
			UINT32 OperationSet = XAUDIO2_COMMIT_NOW
		);
		
		*/
		//引数の値を０〜１．０ｆの値に切り詰める
		if (volume < 0.0f)
		{
			volume = 0.0f;
		}
		else if(volume > 1.0f)
		{
			volume = 1.0f;
		}


		//ソースボイス全体に音量を変更する
		//条件：演奏者のソースボイス数　分
		for (int i = 0; i < audioDatas[handle]->createNum; i++)
		{
			//第一引数：ボリュームの割合（0.0f ~ 1.0f）
			//第二引数：〜〜〜〜〜〜〜〜（書かなかった時には、= XAUDIO2_COMMIT_NOWの値が自動で入るため入れなくともよい）
			HRESULT hr = audioDatas[handle]->ppSourceVoice[i]->SetVolume(volume);

			//エラーチェック
			ERROR_CHECK(hr, "ボリューム設定ができなかった", "エラー");

		}

		//処理の成功
		return S_OK;
	}
	//解放
	void Audio::Release()
	{
		//参考サイト
		//https://brain.cc.kogakuin.ac.jp/~kanamaru/lecture/C++2/09/09-02.html

		//オーディオデータの解放
		ReleaseAllAudioData();

		//解放
		//COMの開放も一回でいいので、GameEngineの一番上にでも
		//CoUninitialize();
			//COMの宣言をAudioで宣言したならば、Audioにて解放する

		//マスターボイス
			//専用の解放関数を呼びこむだけでよい。
			//さらにDeleteしようとするとエラー
		SAFE_DESTROY_VOICE(pMasteringVoice);
		
		//本体
		SAFE_RELEASE(pXAudio);
		
	}
	
	//オーディオデータの解放
	void Audio::ReleaseAllAudioData()
	{
		//ソースボイスの解放
			//ロードしたオーディオデータの解放
		//条件：音楽情報群数
		for (int i = 0;i < audioDatas.size();i++)
		{
			//条件：まだ解放されていなければ
			if (audioDatas[i] != nullptr)
			{
				//データ配列の動的確保したポインタの解放
				//まだ、他で使用されているpAudioDataデータなどは、解放しないようにする（以下の関数にて）
				ReleaseOneData(i);
			}

		}
		//演奏者群（Vector）を初期化
		audioDatas.clear();
	}

	//データ群の中から指定のデータを解放
	void Audio::ReleaseOneData(int handle)
	{
		//条件：ハンドルが0未満
		//  　：||
		//　　：ハンドルが音楽ファイル群数　以上
		//　　：||
		//　　：音楽データが解放されている
		//考えられるエラーの際に解放を行わずに帰る
		if (handle < 0 || handle >= audioDatas.size() || audioDatas[handle] == nullptr)
		{
			//処理終了
			return;
		}


		//停止
			//詳細：音を止めて、
			//　　：かつ、ソースボイスからバッファを解放してから、
			//　　：解放処理を行わなければ、　(再生中に解放処理を行うと、)解放中にエラーが起こる
		Stop(handle);

		//音楽情報群の中の
			//解放されていないデータ中に、同様のデータを使用している共通のポインタが存在しないかのフラグ
		bool isExist = false;

		//スコープ：�@
		//まだ解放されていない中に、
		//自身の所有しているデータ部のポインタが、ある場合、それは開放しない。
			//解放されていない中になければ、初めて解放する
		//条件：音楽情報群数
		for (int i = 0; i < audioDatas.size(); i++)
		{
			//スコープ：�A
			//条件：まだ解放されていない
			//　　：&&
			//　　：自身のハンドルと同じでない
			//　　：&&
			//　　：バッファーのポインタが共通である
			if (audioDatas[i] != nullptr 
				&& i != handle 
				&& audioDatas[i]->buf.pAudioData == audioDatas[handle]->buf.pAudioData)
			{
				//情報群内に共通のポインタを持つデータが存在する場合、
				//そのバッファーデータは、解放しないとする
					//→まだ、解放されていないデータの中で共通ポインタを所有している場合消さない
					//→データの中で、配列の一番最後に使われている共通のポインタを　解放　行うようにする。
						//→自分のデータよりも要素が前、後ろのデータの中で同じデータを持っている場合は×。

				//共通データがまだ存在するので解放しない
				isExist = true;
				//処理を抜ける
					//スコープ：�@
				break;
			}
			//最後まで全データを回して、
			//フラグにtrueが入らなかった場合、
			//まだ、解放されていない中で同じFBXポインタを持つものがいない　＝　FBXポインタを解放
		}
		//解放されていない、他データ内で
		//共通ポインタを使ってなければモデル解放
		//条件：バッファーのデータを所有しているほか音楽情報が存在しない場合
		if (isExist == false)
		{
			//バッファーデータの解放
			SAFE_DELETE_ARRAY(audioDatas[handle]->buf.pAudioData);	
				/*
					ここで注意が必要なのは、
					バッファの解放をするときに、
					ソースボイスが音楽を再生中であるとき、　解放中にエラーが起こる可能性が高い

					そのため、解放処理の前に、必ず Stop() そして、ソースバイスからバッファの解放であるFlash~~~を呼び込むこと！！
			
				*/
				//参考サイト：
				//https://dixq.net/forum/viewtopic.php?t=4330

		}


		//ソースボイスの解放
			//まず中身のポインタ一つ一つの解放
			//それぞれ確保分
		//条件：演奏者数
		for (int i = 0; i < audioDatas[handle]->createNum; i++)
		{
			//ポインタの解放
				//解放はマスターボイスと同様の解放関数
			SAFE_DESTROY_VOICE(audioDatas[handle]->ppSourceVoice[i]);

			//audioDatas[i].ppSourceVoice[j]は、ポインタの枠を確保しただけで、実際に動的確保しているわけではない
				//そのため、Deleteなどの解放は望まれない
				//×SAFE_DELETE(audioDatas[i].ppSourceVoice[j]);
		}
		//側のポインタの解放
			//ポインタのポインタは、実際にはポインタ（ソースボイスのポインタ）のアドレス部分のみ持っている、
			//つまり、側の部分は実態を持っていない。（ただのポインタ型の配列である。）
			//deleteで、動的確保の配列解放
		SAFE_DELETE_ARRAY(audioDatas[handle]->ppSourceVoice);
		
		//音楽情報の解放
		SAFE_DELETE(audioDatas[handle]);
	}

