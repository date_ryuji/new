#include "Model.h"									//ヘッダ
#include "../DirectX/Direct3D.h"					//Direct3D
													//シェーダーを切り替えるため
													//切り替え先のシェーダーのタイプを知るため
#include "../../CommonData/GlobalMacro.h"			//共通マクロ
#include "../PolygonGroup/PolygonGroupFactory.h"	//ポリゴンクラス生成クラス


/*
	namespace 

	クラスのように
	namespace AA
	{
	
		void GetA()
		{
			~~~
		}
	}
	宣言のスコープ内で関数の実装まで記述することが可能である。

	スコープの外で、
	void AA::GetA()
	{
	
	}
	スコープ名を付けて実装することも可能である


	どちらを使っても良いが、
	プロジェクト内にて、
	使い方、実装の仕方は統一する。

*/


/*Model(namespace)*/
namespace Model 
{
	//モデル情報群（別名：データ群）
		//詳細：モデル（ポリゴンにて作られるモデルデータ）のデータを入れておく構造体の動的配列（可変長配列）
		//　　：ModelDataの可変長配列（vector型：途中で要素を消したりしない）
	std::vector<ModelData*> datas;	
}

//ロード
int Model::Load(const std::string& FILE_NAME, POLYGON_GROUP_TYPE thisPolygonGroup, SHADER_TYPE thisShader)
{
	//条件：ファイルが存在しない場合
		//第一引数：ファイルのパス　を示す変数のポインタ（string型はc_str()にて、　自身のポインタを送ることが可能）
	if (!PathFileExists(FILE_NAME.c_str()))
	{
		//ファイルが存在しないとき
			//−１　：　エラーを返す
		return -1;	
	}

	/*
		�@構造体のインスタンスを作成。
		　データ格納先を確保

		�A引数ファイル名から、すでにロード済みのリソースがある場合、
		　そのリソースを共有する（フライウェイト）

		�BPolygonGroupオブジェクトを作成し、ロードする

		�C構造体の中身が埋まったので動的配列に追加
			//Modelにて確保した動的な配列（可変長配列）に作成して情報を生成した構造体を登録

		�D番号（配列の要素数-1）を返す
			//配列の要素数を出して、−１→つまり、２個入っていれば、１が入る(０オリジン、作成した構造体が追加された要素番号)

	*/
	
	//�@
	//モデルのデータを入れる構造体のインスタンス生成
	ModelData* pData = new ModelData;	//動的確保

	//構造体メンバのファイル名に引数の内容を登録
	pData->fileName = FILE_NAME;


	/*
		引数ファイル名からすでにリソースとして確保済みならば
			PolygonGroupを作るモデルファイルをロードをするところに
			→datasの中に、同様のPorigonGroupを作るモデルファイルが存在するならば、
			→リソースをコピーする、
			→newをしないで、Loadしないで、
			→新規に作成した構造体の中のSpriteポインタに、既に存在するPolygonGroupのポインタ入れてやる

			無駄なリソースを確保せずに済む
		*/
	//スコープ：�@
	//vectorの動的配列を頭から回す(回し方はイテレータ必要なし（なぜなら、要素数もわかるし、vectorは、配列と同じ扱い方なので）)
	//条件：現在のデータ群数
	for (int i = 0; i < datas.size(); i++)
	{
		//スコープ：�A
		//条件：すでに同一のファイルが存在していたら
			//ロードしない
			//同一ファイルの判断は、ファイル名から判断
		if (datas[i]->fileName == FILE_NAME)
		{
			//イテレータによって示されているdatasのメンバであるfileNameと
			//引数のfileNameが同じなら　＝　すでにPolygonGroupファイルはロードされている

			//作成した構造体のメンバpPolygonGroupに、ファイル名が同じのdatasのpPolygonGroupのアドレスを代入
			pData->pPolygonGroup = datas[i]->pPolygonGroup;

			//抜ける
				//スコープ：�@
			break;
		}
	}

	//�B
	//すでにリソースが確保済みでなかった場合
	//新たにリソースを確保する必要がある場合
		//上記のfor分を抜けてもなお、pPolygonGroupのポインタがnullptrならば
		//上記のfor文によって、同様のPolygonGroupファイルを見つけられなかった
	if (pData->pPolygonGroup == nullptr)
	{
		//PolygonGroupオブジェクトの新規作成

		//ポリゴンクラス生成クラスの作成
		PolygonGroupFactory* pFactory = new PolygonGroupFactory;

		//PolygonGroupオブジェクトのインスタンス生成（ポインタによって動的確保）
			//指定されたポリゴン群のクラスを引数にて指定して、
			//そのクラスを生成する
		pData->pPolygonGroup =
			pFactory->CreatePolygonGroupClass(thisPolygonGroup);

		//生成クラスの解放
		SAFE_DELETE(pFactory);

		//PolygonGroupオブジェクトのロード（メンバのファイル名を使用して）
		if (FAILED(pData->pPolygonGroup->Load(pData->fileName.c_str(), thisShader)))
		{
			//エラーの原因をメッセージボックスとして表示
			MessageBox(nullptr, "PolygonGroupファイルのロードの失敗", "エラー", MB_OK);
			SAFE_DELETE(pData->pPolygonGroup);
			SAFE_DELETE(pData);

			//−１を返す（ロードしたときにー１が帰ってきたら、Load側で、assartにてエラーを返すようにする。）
			return -1;
		}
	}

	//シェーダーをセットさせる
		//PolygonGroupに自身のシェーダーをセットするが、
		//PolygonGroupを共通で使うため、
		//シェーダーをその都度セットする必要がある。
		//そのため、データベースにシェーダータイプを持たせる
	pData->thisShader = thisShader;
		//描画をするときにセットする


	//ポリゴン群のクラスをセットする
	pData->thisPolygonGroup = thisPolygonGroup;

	//�C
	//作成した構造体のインスタンスを動的配列に追加
	datas.push_back(pData);

	//�D
	//動的配列のサイズ（要素数）−１
		//今回作成した構造体が動的配列に登録された際の添え字が返される
	return (int)(datas.size()) - 1;
}

//モデルデータの情報構造体を新規に追加する
int Model::AddModelData(ModelData modelData)
{
	//本来はファイルのロードを行うことで
	//ポリゴングループを作るところからはじめるが　

	//外部から作成されていた場合
	//その作成データを引数として取得し、
	//データ群へ格納、追加する


	//格納用動的確保データ
	ModelData* initData = new ModelData;

	//初期化データと比較し、
		//データが格納されているのかを確認
		//構造体作成時、コンストラクタが呼ばれ、初期化され　初期データが入る
	//データが格納されているのかのチェック
		//データ群内に、同様のデータがあるかどうかは、チェックしない
	if (modelData.pPolygonGroup == nullptr ||
		modelData.thisShader == initData->thisShader ||
		modelData.thisPolygonGroup == initData->thisPolygonGroup)
	{
		//格納できない
			//データが格納されていない
		return -1;
	}

	//動的確保したデータにコピー
	//引数データをコピー
	AllDataCopy(initData, &modelData);

	//格納
	datas.push_back(initData);

	//動的配列のサイズ（要素数）−１
	//今回作成した構造体が動的配列に登録された際の添え字が返される
	return (int)(datas.size()) - 1;
}

//引数データをコピーする
	//第一引数へ、第二引数をコピーする
void Model::AllDataCopy(ModelData* pCopyTo, ModelData* pOriginal)
{
	//文字列以外のコピー
	pCopyTo->pPolygonGroup = pOriginal->pPolygonGroup;
	pCopyTo->thisPolygonGroup = pOriginal->thisPolygonGroup;
	pCopyTo->thisShader = pOriginal->thisShader;
	pCopyTo->transform = pOriginal->transform;
	pCopyTo->transform.Calclation();

	//初期化をしていないString型であれば、
	//＝するだけで、コピーが可能
	pCopyTo->fileName = pOriginal->fileName;

	//文字列なしであれば、
	//memcpyなどで、一気にコピーしたいが、文字列があるので、sizeが可変になってしまうので、
		//コピー中に、Stringは、String部分に格納してくれるのであれば、別だが、
	//初期化していない、文字列に、　格納済み、文字列を確保

}

//指定されたモデルデータのトランスフォームをセット
void Model::SetTransform(int handle, Transform& transform)
{
	//ハンドルによって、示された、動的配列のモデルデータを選択
	//そのモデルデータの構造体が持っているTransformに引数のTransformを更新
	datas[handle]->transform = transform;
	//行列計算
	datas[handle]->transform.Calclation();
}

//指定されたモデルデータのPolygonGroupを描画
void Model::Draw(int handle)
{
	//ハンドルによって示されたモデルデータ
	//そのPolygonGroupを描画させる

	//描画前に、
	//PolygonGroupに、描画を行うシェーダーを選択させる
	datas[handle]->pPolygonGroup->ChangeShader(datas[handle]->thisShader);

	//計算
	datas[handle]->transform.Calclation();

	//PolygonGroupをDraw（引数にてTransformをおくる（この際のTransformは、モデルデータの自身の構造体が持っているTransform））
	datas[handle]->pPolygonGroup->Draw(datas[handle]->transform);

}

//解放
void Model::Release()
{
	//モデルデータの全消去
	ReleaseAllModelData();
}

//モデルデータの全消去
void Model::ReleaseAllModelData()
{
	//動的配列内の全データの解放
	//条件：データ群数
	for (int i = 0; i < datas.size(); i++)
	{
		//条件：まだ解放されていなければ
		if (datas[i] != nullptr)
		{
			//データ配列の動的確保したポインタの解放
			//まだ、他で使用されているPolygonGroupデータなどは、解放しないようにする（以下の関数にて）
			ReleaseOneData(i);

		}

		/*
			ひとつづつ、
			動的確保した記憶のある、PolygonGroupのポインタを解放しようとすると
			→エラーになる


			なぜか。。。
			→PolygonGroupポインタは、すでにロードされているものは、ロードされているオブジェクトのポインタを持つようにしている
			→なので、どこかで１度解放したものまで解放しようとしてしまう

			すでに解放したものは、解放しないようにする。
			→だが、ポインタというのは、あくまでアドレスを持っているだけで、
			→配列でそれぞれ同様の位置を指すポインタを持っていても、

			１つのポインタを解放しても、
			違うポインタ変数で持っているものは、まだ、アドレスを指し続ける。（すでに解放されているのに、、、）

			ポインタはただ、「アドレスを持っているだけ。」

			であれば、
			他で解放されても、
			→そのポインタの示すアドレスは解放されている→これは、、分からない。


			だったら、どうするか。
			×→最初に解放して、後に解放しようとするものは、それは解放されていますか？
			〇→解放する前に、このPolygonGroupファイルは、まだ解放されていない配列のデータで使われていますか？
					→使われているならば、解放しません。
					→使われていないならば、解放します。

			★上記の方法をとることで、
				ポインタが解放されたアドレスを解放しようとすることはなくなる。
		*/
		//上記の処理をReleaseOneData（）にて行っている

	}


	//可変長配列のクリア
	datas.clear();


}

//指定ハンドルのデータ解放
void Model::ReleaseOneData(int handle)
{
	//条件：ハンドルが0未満
	//  　：||
	//　　：ハンドルが音楽ファイル群数　以上
	//　　：||
	//　　：モデルデータが解放されている
	//考えられるエラーの際に解放を行わずに帰る
	if (handle < 0 || handle >= datas.size() || datas[handle] == nullptr)
	{
		return;
	}

	//自身の所有している、
	//まだ、解放されてないPolygonGroupのポインタをまだ持っているか
	//同じモデルを他でも使っていないか
	bool isExist = false;

	//スコープ：�@
		//まだ解放されていない中に、
		//自身の所有しているデータ部のポインタが、ある場合、それは開放しない。
		//解放されていない中になければ、初めて解放する
	//条件：データ群数
	for (int i = 0; i < datas.size(); i++)
	{
		//スコープ：�A
			//条件：まだ解放されていない
			//　　：&&
			//　　：自身のハンドルと同じでない
			//　　：&&
			//　　：バッファーのポインタが共通である
		if (datas[i] != nullptr
			&& i != handle 		
			&& datas[i]->pPolygonGroup == datas[handle]->pPolygonGroup)
		{
			//解放しないとする
				//→まだ、解放されていないデータの中でPolygonGroupポインタを所有している場合消さない
				//→データの中で、配列の一番最後に使われているPolygonGroupファイルが解放を行うようにする。
					//→自分のデータよりも要素が前、後ろのデータの中で同じデータを持っている場合は×。

			isExist = true;
			//処理を抜ける
				//スコープ：�@
			break;
		}
		//最後まで全データを回して、
		//フラグにtrueが入らなかった場合、
		//まだ、解放されていない中で同じPolygonGroupポインタを持つものがいない　＝　PolygonGroupポインタを解放
	}

	//解放されていない、他データ内で
	//PolygonGroupポインタを使ってなければモデル解放
	//条件：バッファーのデータを所有しているほかの画像情報が存在しない場合
	if (isExist == false)
	{
		//解放
		datas[handle]->pPolygonGroup->Release();
		SAFE_DELETE(datas[handle]->pPolygonGroup);
	}

	//自分のデータを解放（配列の要素を動的確保しているので、解放）
	SAFE_DELETE(datas[handle]);
}

//レイキャストによる、レイとポリゴンクラス（PolygonGroup＊）との衝突判定
//レイとの衝突判定
	//特定モデルとのレイとの衝突判定を行う
void Model::RayCast(int handle, RayCastData* rayData)
{
	//方向ベクトルの正規化
	rayData->dir = XMVector3Normalize(rayData->dir);

	//保存用の変数
	XMVECTOR initStartPos = rayData->start;
	XMVECTOR initDirection = rayData->dir;

	/*

	�@レイの通過点を求める（startとdirのベクトルを足したもの）
	�Aワールド行列の逆行列を求める(移動、回転、拡大を含む)
	�BrayDataのstartを�Aで変形
	�C�@を�Aで変形
	�DrayDataのdirに�Bから�Cに向かうベクトルを入れる（移動行列を行うと、単純に逆行列では方向は求められない。なので、ワールド行列を掛けた移動方向＋初期位置と、ワールド行列を掛けた初期位置のベクトルとで、差ベクトルを出せば方向を出せる）

	*/

	//�@
	//初期位置　＋　方向ベクトル
	XMVECTOR passing = rayData->start + rayData->dir;


	//�Aワールド行列の逆行列
	XMMATRIX inverceMat = XMMatrixInverse(nullptr,
		//matScale_ * matRotate_ * matTranslate_);
		datas[handle]->transform.GetWorldMatrix());


	//�B
	rayData->start = XMVector3TransformCoord(rayData->start, inverceMat);

	//�C
	passing = XMVector3TransformCoord(passing, inverceMat);

	//�D
	rayData->dir = XMVector3Normalize(passing - rayData->start);


	//レイキャスト実行
		//引数handleにて示されるPolygonGroup
		//そのポリゴンとの衝突判定	
		//レイキャストを呼びこむ（これで、PolygonGroupの全ポリゴンと衝突判定を行う。）
	datas[handle]->pPolygonGroup->RayCast(rayData);


	//元のレイ情報に戻す
		//レイの衝突判定のために
		//PolygonGroupのTransform値分、回転、移動、拡大を行ったので、元の情報に戻す。
	rayData->start = initStartPos;
	rayData->dir = initDirection;

}

//引数ベクトルと、ポリゴンクラス（PolygonGroup＊）と衝突した面の法線ベクトルを取得
XMVECTOR Model::NormalVectorOfCollidingFace(int handle, XMVECTOR& start, XMVECTOR& dir)
{
	//方向ベクトルの正規化
	dir = XMVector3Normalize(dir);

	//保存用の変数
	XMVECTOR useStartPos = start;
	XMVECTOR useDirection = dir;

	/*

	�@レイの通過点を求める（startとdirのベクトルを足したもの）
	�Aワールド行列の逆行列を求める(移動、回転、拡大を含む)
	�BrayDataのstartを�Aで変形
	�C�@を�Aで変形
	�DrayDataのdirに�Bから�Cに向かうベクトルを入れる（移動行列を行うと、単純に逆行列では方向は求められない。なので、ワールド行列を掛けた移動方向＋初期位置と、ワールド行列を掛けた初期位置のベクトルとで、差ベクトルを出せば方向を出せる）

	*/

	//�@
		//初期位置　＋　方向ベクトル
	XMVECTOR passing = start + dir;


	//�Aワールド行列の逆行列
	XMMATRIX inverceMat = XMMatrixInverse(nullptr,
		datas[handle]->transform.GetWorldMatrix());

	//�B
	useStartPos = XMVector3TransformCoord(start, inverceMat);

	//�C
	passing = XMVector3TransformCoord(passing, inverceMat);

	//�D
	useDirection = XMVector3Normalize(passing - useStartPos);

	//法線ベクトルの取得
	return datas[handle]->pPolygonGroup->NormalVectorOfCollidingFace(useStartPos, useDirection);
}


//PolygonGroupのシェーダーを切り替える
void Model::ChangeShader(int handle, SHADER_TYPE shaderType)
{
	//自身のデータのシェーダータイプも切り替える
		//描画Drawの時に、一度、自身のタイプをセットしなおすため、自身も保存しておく必要がある
	datas[handle]->thisShader = shaderType;
	//PolygonGroupのメソッドである、
		//シェーダー切替えの関数を呼び、PolygonGroupのシェーダーを切り替える
		//※一度切り替えたら、それはずっとそのシェーダーのまま。自動で戻される処理などは入らないので注意
	datas[handle]->pPolygonGroup->ChangeShader(shaderType);
}

//現在のシェーダーを取得
SHADER_TYPE Model::GetShader(int handle)
{
	return datas[handle]->pPolygonGroup->GetMyShader();
}

//ポリゴンクラス（PolygonGroup＊）のポインタを渡す
PolygonGroup* Model::GetPolygonGroupPointer(int handle)
{
	return datas[handle]->pPolygonGroup;
}


//コンストラクタ
Model::ModelData::ModelData() :
	pPolygonGroup(nullptr),
	thisShader(SHADER_TYPE::SHADER_MAX),
	thisPolygonGroup(POLYGON_GROUP_TYPE::POLYGON_GROUP_MAX)
{
}




