#pragma once
#include "BaseShader.h"		//シェーダークラス　スーパークラス


/*
	クラス詳細	：黒澤モード（白黒）シェーダー２D	
	クラスレベル：サブクラス（スーパークラス：BaseShader）
	クラス概要（詳しく）
				：
				・白黒灰　のモノクロ表現　２Dポリゴン用
*/
class KurosawaMode2DShader : public BaseShader
{
//protected メソッド
protected:
	//頂点シェーダーファイルのコンパイル
		//詳細：仮想関数にしているのは、シェーダーごとに、関数が違う可能性があるため
		//　　：シェーダーごとのシェーダーファイルの登録と、頂点シェーダーの指定
		//レベル：オーバーライド
		//引数：ポインタのアドレス（ポインタを持っているアドレス部を参照し、関数呼び出し元のデータを直接書き換える、参照渡しを行う）
		//引数：シェーダーファイル名
		//戻値：処理の結果（処理の成功 S_OK , 処理の失敗　E_FAIL）
	HRESULT VSCompileFromShaderFile(ID3DBlob** pCompileVS, const wchar_t* shaderFileName) override;
	//ピクセルシェーダーファイルのコンパイル
		//詳細：シェーダーごとのシェーダーファイルの登録と、頂点シェーダーの指定
		//レベル：オーバーライド
		//引数：ポインタのアドレス（ポインタを持っているアドレス部を参照し、関数呼び出し元のデータを直接書き換える、参照渡しを行う）
		//引数：シェーダーファイル名
		//戻値：処理の結果（処理の成功 S_OK , 処理の失敗　E_FAIL）
	HRESULT PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t* shaderFileName) override;




//public メソッド
public:

	//コンストラクタ
		//引数：なし
		//戻値：なし
	KurosawaMode2DShader();

	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~KurosawaMode2DShader() override;

	//シェーダーの初期化
		//詳細：頂点バッファの引数（頂点インプットレイアウト）を決めたり、シェーダーファイルのコンパイル部分を書く。
		//詳細：シェーダーごとに、必要な頂点インプットレイアウトを設定
		//　　：頂点ごとに、接線が必要であったり、法線が必要であることがある。そのため、頂点シェーダーに渡す情報の設定
		//レベル：オーバーライド
		//引数：なし
		//戻値：処理の結果（処理の成功 S_OK , 処理の失敗　E_FAIL）
	HRESULT Initialize() override;




};

