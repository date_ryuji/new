#pragma once
#include "BaseShader.h"		//シェーダークラス　スーパークラス


/*
	クラス詳細	：ゲートシェーダー				
	クラスレベル：サブクラス（スーパークラス：BaseShader）
	クラス概要（詳しく）
				：
				・動的にUVを移動させることで、模様としてついているテクスチャを動的に移動する。
				・一種のエフェクトとして使用する
*/
class GateShader : public BaseShader
{
//protected メソッド
protected:
	//頂点シェーダーファイルのコンパイル
		//詳細：仮想関数にしているのは、シェーダーごとに、関数が違う可能性があるため
		//　　：シェーダーごとのシェーダーファイルの登録と、頂点シェーダーの指定
		//レベル：オーバーライド
		//引数：ポインタのアドレス（ポインタを持っているアドレス部を参照し、関数呼び出し元のデータを直接書き換える、参照渡しを行う）
		//引数：シェーダーファイル名
		//戻値：処理の結果（処理の成功 S_OK , 処理の失敗　E_FAIL）
	HRESULT VSCompileFromShaderFile(ID3DBlob** pCompileVS, const wchar_t* shaderFileName) override;
	//ピクセルシェーダーファイルのコンパイル
		//詳細：シェーダーごとのシェーダーファイルの登録と、頂点シェーダーの指定
		//レベル：オーバーライド
		//引数：ポインタのアドレス（ポインタを持っているアドレス部を参照し、関数呼び出し元のデータを直接書き換える、参照渡しを行う）
		//引数：シェーダーファイル名
		//戻値：処理の結果（処理の成功 S_OK , 処理の失敗　E_FAIL）
	HRESULT PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t* shaderFileName) override;

	/*コンスタントバッファ１**************************************************************************************************************/
	//コンスタントバッファ１を確保しておくポインタ
		//詳細：コンスタントバッファ2つ目の情報を持たせたバッファー
		//　　：シェーダーごとの独立したデータバッファー。
		//制限：バッファーに入る情報は、シェーダーにおけるコンスタントバッファ１の情報でなくてはならない。
		//　　：最低16バイト
	ID3D11Buffer* pConstantBuffer1_;

//public 構造体
public:

	/*コンスタントバッファ１**************************************************************************************************************/
	/*
		構造体詳細	：コンスタントバッファ2つ目の情報（シェーダーファイルにおけるコンスタントバッファ１に送る情報）
		構造体概要（詳しく）
			：シェーダー独自に持っている、コンスタントバッファ１、そのバッファへ送る情報をまとめた構造体
			　構造体にしている理由は、情報をまとめた方が情報を送る際に処理が少なくてすむ。まとめてある方が便利。
			  登録されている情報は、シェーダー独自の値である。

	　　制限：サイズ　16バイト以下
	  　　　　16バイト以上になる場合、設定の段階で通常と差異が出てくる。
		  　　	Initialize()->CreateConstantBuffer1()の第二引数サイズを　構造体サイズ以上で　かつ　一番近い16の倍数に設定する必要アリ。

		アクセスレベル：public
		　　　　　理由：オブジェクトクラス（GameObjectを継承したゲーム内オブジェクト）から情報を更新し、動的に値を変化させ、その情報をシェーダーに送りたい。
			 　　　　　上記を満たすために、シェーダークラス（自身）とオブジェクトクラスとの値の橋渡しが必要。その橋渡しの情報をまとめておくことで橋渡しを簡易的にする。
				  　　　当然、外部からアクセスするにはpublicではならないため。
	*/
	struct CONSTANT_BUFFER_1
	{
		//UVの移動量
		float moveUVPower;
		//コンストラクタ
		CONSTANT_BUFFER_1();
	}BufferData1_;





//public メソッド
public:

	//コンストラクタ
		//引数：なし
		//戻値：なし
	GateShader();


	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~GateShader() override;

	//シェーダーの初期化
		//詳細：頂点バッファの引数（頂点インプットレイアウト）を決めたり、シェーダーファイルのコンパイル部分を書く。
		//詳細：シェーダーごとに、必要な頂点インプットレイアウトを設定
		//　　：頂点ごとに、接線が必要であったり、法線が必要であることがある。そのため、頂点シェーダーに渡す情報の設定
		//レベル：オーバーライド
		//引数：なし
		//戻値：処理の結果（処理の成功 S_OK , 処理の失敗　E_FAIL）
	HRESULT Initialize() override;


	/*コンスタントバッファ１**************************************************************************************************************/
	//コンスタントバッファ１へデータをセット
		//詳細：オブジェクトクラス（GameObjectを継承した、シェーダーを使用するFbxなどを描画するクラス、）で管理するデータを受け取ることが可能。
		//　　：オブジェクトクラスより、データの構造体をセットしてもらう
		//引数：セットするデータ　自クラス管理の構造体データをポインタ受け取り（サイズの関係でポインタ受け取りを行っている。アドレスを知りたいわけでも、書き換えを行うわけでもない）
		//戻値：なし
	void SetConstantBuffer1Data(CONSTANT_BUFFER_1* BufferData1);

	//コンスタントバッファ１をシェーダーに渡す
		//詳細：シェーダーのコンスタントバッファー１へ、自クラスで管理しているコンスタントバッファ1へわたす情報（バッファーとして管理）を橋渡し
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void ShaderConstantBuffer1View() override;


	//シェーダーのコンスタントバッファ１のマップ
		//詳細： コンスタントバッファ2つ目のマップの際に、コンスタントバッファ2つ目に渡す情報のサイズを指定する必要がある。そのサイズはシェーダー独自のサイズであるため、継承先のクラスでのみ知ることが可能。
		//　　：そのため、継承先でオーバーライドし、独自の情報のサイズをMapShaderConstantBuffer1に渡す。
		//　　：継承元のMapShaderConstantBuffer1()への橋渡し
		//レベル：オーバーライド
		//引数：なし
		//戻値：処理の結果（処理の成功 S_OK , 処理の失敗　E_FAIL）
		//アクセスレベル：public publicである理由は、ポリゴン群で使用する
	HRESULT MapShaderConstantBuffer1View() override;
	//シェーダーのコンスタントバッファ１のアンマップ
		//詳細：継承元のUnMapShaderConstantBuffer1()への橋渡し
		//レベル：オーバーライド
		//引数：なし
		//戻値：処理の結果（処理の成功 S_OK , 処理の失敗　E_FAIL）
		//アクセスレベル：public publicである理由は、ポリゴン群で使用する
	HRESULT UnMapShaderConstantBuffer1View() override;





};

