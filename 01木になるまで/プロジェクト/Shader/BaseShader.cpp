#include <d3dcompiler.h>					//シェーダーファイルコンパイル用
#include "BaseShader.h"						//ヘッダ
#include "../CommonData/GlobalMacro.h"		//共通マクロ
#include "../Engine/DirectX/Texture.h"		//２Dテクスチャクラス
	/*
	//CubeMapのテクスチャからシェーダーリソースビューを取得する関数を呼ぶために必要。
	//BaseShaderにて、テクスチャの関数を呼び込めるようにするために、インクルードするが、
	//継承先でBaseShaderを呼び込んだ場合、BaseShaderにてTextureをインクルードしていれば、Textureのメソッドを呼び込んだりすることができる。
	//継承先でわざわざTextureをインクルードする必要はない。Textureを使うクラスがインクルードしておけばよい。　関数先でTextureの処理が完結しているのであれば、インクルードの必要はない
	*/


//コンストラクタ
BaseShader::BaseShader()
{
}

//デストラクタ
BaseShader::~BaseShader()
{
	//メンバ構造体の解放
	SAFE_RELEASE(shaderInfo_.pRasterizerState);
	SAFE_RELEASE(shaderInfo_.pVertexLayout);
	SAFE_RELEASE(shaderInfo_.pPixelShader);
	SAFE_RELEASE(shaderInfo_.pVertexShader);
}

//初期化
	//継承先にオーバーライドする
	//レベル：仮想関数
HRESULT BaseShader::Initialize()
{
	//処理の結果
	HRESULT hr = S_OK;

	//シェーダーファイル名
		//ワイド文字（全文字　2バイト）
	const wchar_t shaderFileName[] = L"Assets/Shader/NormalMap.hlsl";
	
	//頂点シェーダーの作成（コンパイル）
	{

		//★★頂点インプットレイアウト（シェーダーへ渡すポリゴンを作る頂点情報）★★
			//詳細：下記にて定義した情報をシェーダーの頂点シェーダーに渡されて、
			//　　：各頂点ごとの情報を定義する
			//　　：頂点３つで一つのポリゴンとし、３つの頂点の頂点シェーダーが呼ばれて、出力位置を決めて、ピクセルシェーダーに渡されることで描画のピクセル色を決めることができる。
			//詳細：頂点シェーダーへ渡す情報は、シェーダーごとに違う。
			//　　：光の陰影をつけるのであれば、法線が必要。反射をつけるのであれば、接線が必要。と、シェーダーごとに定義する必要がある。
			//詳細：２Dにおいても、３Dにおいてもポリゴンを作り、ポリゴンごとに描画することには変わりないため、ポリゴン描画においては必ず頂点インプットレイアウトを必要とする
		D3D11_INPUT_ELEMENT_DESC layout[] = {

			//頂点位置（ローカル座標）
			//引数：格納情報のセマンティクス（格納情報を識別する文字列）
			//引数：０
			//引数：格納情報　の次元情報（1次元：R32 , 2次元：R32G32 , 3次元：R32G32B32）
			//引数：０
			//引数：メモリに書き込み開始位置（１つ目は０）
			//引数：格納情報の種類（ベクトル）
			//引数：０
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },

			//UV位置（テクスチャの読み取り位置）
			//詳細：TEXCOORD　＝＞　テクスチャコーディネートの略
			//第５引数：メモリに書き込み開始位置（２つ目は１つ目に格納したサイズ分の次　なので、１つ目にXMVECTORを格納したならば、２つ目はXMVECTORサイズ位置から始める（メモリは０オリジン））
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },

			//法線（頂点のY方向）（頂点の向いている方向）
			//第５引数：メモリに書き込み開始位置（３つ目はXMVECTORの構造体サイズの２つ分先から書き始める）
			{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線

			//接線（頂点のX方向）
			//第５引数：メモリに書き込み開始位置（４つ目はXMVECTORの構造体サイズの３つ分先から書き始める）
			{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 3 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//接線

		};
		/*
			接線と法線にて従法線を求めることが可能である。
		*/

		//頂点シェーダーの初期化
		//シェーダーファイルごとの頂点シェーダーのレイアウトの確定、コンパイルを済ませる関数呼び込み
			//引数：シェーダーファイル名
			//引数：頂点インプットレイアウト
			//引数：頂点インプットレイアウトのサイズ
			//戻値：処理の結果
		hr = InitVertexShaderForEachShaderFile(shaderFileName, layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)));
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点シェーダーの初期化失敗", "エラー");


	}

	// ピクセルシェーダの作成（コンパイル）
	{
		//ラスタライザ作成
			//詳細：ピクセルの塗り方、ピクセルによって作られるポリゴンのピクセルの塗り方などの設定
			//　　：ポリゴンが描画されるとき、ピクセル単位で塗られている。
			//　　：そのピクセル単位で塗られている表現をラスタという。
		D3D11_RASTERIZER_DESC rdc = {};

		//カリングモード
		//ポリゴンの裏面設定
			//詳細：不要となるポリゴンの設定（ポリゴンはポリゴンを作る3頂点のインデックス情報が時計回りになっているとき、表とする。　反時計回りを裏とする。）
			//　　：FRONT:　表　表示しない
			//　　：BACK :　裏　表示しない
			//　　：NONE :　いらないもの　なし（両面表示）
		rdc.CullMode = D3D11_CULL_BACK;
		
		//ソリッド
		//ポリゴンの塗り
			//詳細：ポリゴンを作る3頂点をピクセルで塗る際、その塗り方
			//　　：SOLID		：中身を塗りつぶす
			//　　：WIER_FRAME　：ワイヤーフレーム		
			//					　→枠だけなので、三角形にて表現しているので、三角形のつながりとなる
			//　　　　　　　　　　→当たり判定表示、取り合えず表示されているのか知りたいときなど
		rdc.FillMode = D3D11_FILL_SOLID;
	
		//ポリゴンを作るインデックス情報の読み取り方
			//詳細：時計回りを表、反時計回りを表。
			//　　：TRUE  : 反時計回りを　表　とします
			//　　：FALSE : 時計回り　を　表　とします
				/*
					FBX作成時に、面を三角化して、
					△ポリゴンだけで面を表現できる、その面を描画する際に、時計回りで頂点を読み込むのかなどを決める
				*/
		rdc.FrontCounterClockwise = FALSE;
		

		//ピクセルシェーダーの初期化
		//シェーダーファイルごとのピクセルシェーダーのラスタライザの確定、コンパイルを済ませる関数
			//引数：シェーダーファイル名
			//引数：ピクセルシェーダーのラスタライズ方法
			//戻値：処理の結果
		hr = InitPixelShaderForEachShaderFile(shaderFileName, &rdc);
		//エラーメッセージ
		ERROR_CHECK(hr, "ピクセルシェーダーの初期化失敗", "エラー");

	}

	//処理の成功
	return S_OK;
}

//頂点シェーダーの初期化
HRESULT BaseShader::InitVertexShaderForEachShaderFile(const wchar_t* SHADER_FILE_NAME, D3D11_INPUT_ELEMENT_DESC* layout, size_t layoutSize)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;
	

	//頂点シェーダーへのコンパイルデータへのポインタ
	ID3DBlob* pCompileVS = nullptr;	//コンパイルデータへのポインタ

	//頂点シェーダーのために、シェーダーファイルをコンパイル
		//詳細：第一引数　pCompleVSは、nullptrなので、　何も入っていないからのポインタを渡している
		//　　：つまり、空のポインタをポインタとして渡している。
		//　　：　→ポインタをそのまま渡して、ポインタで受け取ると、ポインタで現わされるアドレスの中身を見るポインタとなる。そのため、nullptrのポインタを渡すことはできない。
		//　　：　　→しかし、ポインタのアドレスを渡してポインタのポインタで受け取ると、ポインタがnullptrでも構わない。
		//　　：　　→ポインタのアドレス部分を関数先にて変更可能。
		//　　：　　→ポインタのアドレスを渡して、それをポインタのポインタで受け取っているだけ。ポインタも一つの変数の扱いのように、ポインタのアドレスを格納している領域があるはず。その領域のアドレスを関数に渡してあげる。
	hr = VSCompileFromShaderFile(&pCompileVS, SHADER_FILE_NAME);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点シェーダーへのシェーダーファイルコンパイル失敗", "エラー");

	//頂点シェーダーの作成
	hr = CreateVertexShader(pCompileVS);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点シェーダーへの作成失敗", "エラー");

	//頂点インプットレイアウト情報の確定
		//頂点インプットの情報群をもとに、
		//頂点インプットに渡す情報の確定
		//第３引数：頂点インプットレイアウトにおける引数の個数
	hr = CreateInputLayout(pCompileVS, layout, layoutSize);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点インプットレイアウトの情報確定失敗", "エラー");
	/*
		配列のサイズ

		//配列をSizeofして、配列のサイズを求めることができるのは、
		//同スコープ内に限られる、
			//関数先にて、ポインタを使って、Sizeofをして、サイズを受け取ることができない
		//size_t base = sizeof(layout);
			//そのため、サイズを調べて、そのサイズを直接引数に渡す

	*/


	//コンパイラ用のデータを解放
	SAFE_RELEASE(pCompileVS);

	//処理の成功
	return S_OK;
}

//ピクセルシェーダーの初期化
HRESULT BaseShader::InitPixelShaderForEachShaderFile(const wchar_t* SHADER_FILE_NAME, D3D11_RASTERIZER_DESC* rdc)
{
	//処理の結果を入れる変数
	HRESULT hr = S_OK;

	//ピクセルシェーダーへのコンパイルデータへのポインタ
	ID3DBlob* pCompilePS = nullptr;

	//ピクセルシェーダーのために、シェーダーファイルをコンパイル
	hr = PSCompileFromShaderFile(&pCompilePS, SHADER_FILE_NAME);
	//エラーメッセージ
	ERROR_CHECK(hr, "ピクセルシェーダーへのシェーダーファイルコンパイル失敗", "エラー");

	//ピクセルシェーダーの作成
	hr = CreatePixelShader(pCompilePS);
	//エラーメッセージ
	ERROR_CHECK(hr, "ピクセルシェーダーの作成失敗", "エラー");

	//コンパイラ用のデータを解放
	SAFE_RELEASE(pCompilePS);

	//ラスタライザ情報の確定
		//作成した情報を渡し、シェーダーに対するラスタライザの確定
	hr = CreateRasterizerState(rdc);
	//エラーメッセージ
	ERROR_CHECK(hr, "ラスタライザの情報確定失敗", "エラー");

	//処理の成功
	return S_OK;
}

//頂点シェーダーのコンパイル
HRESULT BaseShader::VSCompileFromShaderFile(ID3DBlob** pCompileVS, const wchar_t * SHADER_FILE_NAME)
{
	//頂点シェーダーの関数を指定し、シェーダーファイルをコンパイル
	//第３引数：頂点シェーダーの関数名（デフォルトの関数名を定義する（指定がない場合左記関数をシェーダーファイルから探し、頂点シェーダーとして登録する））
	return CompileFromShaderFile(pCompileVS, SHADER_FILE_NAME, "VS", FUNCTION_TYPE::FUNCTION_VERTEX);
}
//ピクセルシェーダーのコンパイル
HRESULT BaseShader::PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t * SHADER_FILE_NAME)
{
	//ピクセルシェーダーの関数を指定し、シェーダーファイルをコンパイル
	//第３引数：ピクセルシェーダーの関数名（デフォルトの関数名を定義する（指定がない場合左記関数をシェーダーファイルから探し、ピクセルシェーダーとして登録する））
	return CompileFromShaderFile(pCompilePS, SHADER_FILE_NAME, "PS", FUNCTION_TYPE::FUNCTION_PIXEL);
}

//頂点シェーダー、あるいはピクセルシェーダーのコンパイル
	//各シェーダーのシェーダー関数を指定する際に使用する
HRESULT BaseShader::CompileFromShaderFile(ID3DBlob** pCompile, 
	const wchar_t * SHADER_FILE_NAME, const std::string& FUNCTION_NAME, FUNCTION_TYPE type)
{

	//シェーダーファイルからコンパイルを行う
		//詳細：引数にて、ポインタのアドレスをもらっているので、
		//　　：関数呼び出し元のポインタ（アドレスを格納している部分のアドレス（ポインタ自体のアドレス））を直接書き換えることになる。
		//引数：シェーダーファイル名
		//引数：nullptr
		//引数：nullptr
		//引数：各シェーダー関数名
		//引数：各グラフィックボードのバージョン
		//~~
	HRESULT hr = D3DCompileFromFile(SHADER_FILE_NAME, nullptr, nullptr, FUNCTION_NAME.c_str(), GetGraphicVersion(type).c_str(), NULL, 0, pCompile, NULL);
	//エラーメッセージ
	ERROR_CHECK(hr, GetErrorMessage(type).c_str(), "エラー");

	//コンパイルできなかったことを警告
		//条件：空ではないはずだ
	assert(pCompile != nullptr);

	//処理の成功
	return S_OK;
}

//頂点シェーダーの作成
//メンバ構造体へシェーダー情報格納
HRESULT BaseShader::CreateVertexShader(ID3DBlob * pCompileVS)
{
	//pVertexShaderを作成（頂点シェーダー）
	//シェーダー情報を保存しておくメンバ構造体へ　シェーダー情報を格納
	HRESULT hr = Direct3D::pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL,
		&shaderInfo_.pVertexShader);
	//エラーメッセージ
	ERROR_CHECK(hr, "３Dコンパイルしたシェーダを代入失敗", "エラー");

	//処理の成功
	return S_OK;

}

//頂点インプットレイアウト作成
//メンバ構造体へシェーダー情報格納
HRESULT BaseShader::CreateInputLayout(ID3DBlob * pCompileVS, D3D11_INPUT_ELEMENT_DESC* layout , size_t size)
{
	/*
		配列のサイズ

		
		//配列の先頭アドレスを仮に、ポインタとして、受け取ったとして、
			//そのポインタを、sizeofしても、ポインタのサイズしか出てこず、
			//配列のサイズは出てこない
		//その理由は、	
			//単一のポインタとして受け取っているので、
			//その単一のポインタのサイズしか出てこない
		//なので、引数で、単一のポインタではなく、　配列のポインタとして受け取る、つまり、array[]という形で、配列のポインタとして先頭アドレスを受け取る
			//→×
			//サイズを、引数にて送ってもらって、そのサイズをもとに、関数の呼び出しを行うようにする

	*/

	//頂点インプットレイアウト情報を作成
	//シェーダー情報を保存しておくメンバ構造体へ　シェーダー情報を格納
	HRESULT hr = Direct3D::pDevice->CreateInputLayout(layout, (UINT)size, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(),
		&shaderInfo_.pVertexLayout);
	//エラーメッセージ
	ERROR_CHECK(hr, "頂点インプットレイアウトの情報を確定失敗", "エラー");

	//処理の成功
	return S_OK;
}

//ピクセルシェーダー作成
//メンバ構造体へシェーダー情報格納
HRESULT BaseShader::CreatePixelShader(ID3DBlob* pCompilePS)
{
	//ピクセルシェーダー情報を作成
	//シェーダー情報を保存しておくメンバ構造体へ　シェーダー情報を格納
	HRESULT hr = Direct3D::pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL,
		&shaderInfo_.pPixelShader);
	//エラーメッセージ
	ERROR_CHECK(hr, "ピクセルシェーダーの作成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//ラスタライザ作成
//メンバ構造体へシェーダー情報格納
HRESULT BaseShader::CreateRasterizerState(D3D11_RASTERIZER_DESC * rdc)
{
	//ピクセルシェーダー情報を作成
	//シェーダー情報を保存しておくメンバ構造体へ　シェーダー情報を格納
	HRESULT hr = Direct3D::pDevice->CreateRasterizerState(rdc, &shaderInfo_.pRasterizerState);
	//エラーメッセージ
	ERROR_CHECK(hr, "ラスタライザの作成失敗", "エラー");
	
	//処理の成功
	return S_OK;

}

//自身のシェーダー情報をDirect3DのContextへ設定する
	//描画のために自身のシェーダー情報を与え、描画のためのパイプラインを通す
	//これにより、頂点情報は設定した自身の頂点インプットレイアウトにのっとった情報のみ受け取り、
	//指定の頂点シェーダー、ピクセルシェーダーを呼び込むようになる。
void BaseShader::SetShader()
{
	//頂点シェーダー
	Direct3D::pContext->VSSetShader(shaderInfo_.pVertexShader, NULL, 0);	
	//ピクセルシェーダー
	Direct3D::pContext->PSSetShader(shaderInfo_.pPixelShader, NULL, 0);	
	//頂点インプットレイアウト
	Direct3D::pContext->IASetInputLayout(shaderInfo_.pVertexLayout);	
	//ラスタライザー
	Direct3D::pContext->RSSetState(shaderInfo_.pRasterizerState);		
}

//コンスタントバッファ１の情報受け渡し実行
	//シェーダーのコンスタントバッファ１へ自身クラスにて管理している、コンスタントバッファ１へ渡す情報を渡す処理実行
	//レベル：仮想関数
void BaseShader::ShaderConstantBuffer1View()
{
}

//コンスタントバッファ１へのマップ
	//コンスタント”バッファ”へのアクセスを許可する（Map）
	//レベル：仮想関数
HRESULT BaseShader::MapShaderConstantBuffer1View()
{
	return S_OK;
}
//コンスタントバッファ１へのアンマップ
	//コンスタント”バッファ”へのアクセスを拒否する（UnMap）
	//レベル：仮想関数
HRESULT BaseShader::UnMapShaderConstantBuffer1View()
{
	return S_OK;
}

//コンスタントバッファ1の作成
HRESULT BaseShader::CreateConstantBuffer1(ID3D11Buffer ** ppConstantBuffer1, UINT size)
{
	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	

	/*
	コンスタントバッファのバッファー部の確保しておくサイズは、16の倍数でないといけない。

	コンスタントバッファ1の構造体は16の倍数にはなるとは限らない
	そのため、以下のように構造体のサイズをそのままsizeofした値を設定するわけにはいかない。
	
	cb.ByteWidth = sizeof(CONSTANT_BUFFER_1);	

	構造体のサイズが１６の倍数でないと、エラーになる(エラーというより、正しく情報を渡せない)
	強制的に16の倍数にする必要がある
	float1つの型であると、4の倍数で、１６の倍数にはならないので、　ここであえて、１６の値を入れると、エラーにはならない
	
	//�@なので、コンスタントバッファの構造体のサイズを１６の倍数にする。
	//�Aあるいは、ここで、偽のサイズとして、　16の倍数の値を入れることでエラーを回避することが可能である。
	
	※構造体内に行列が存在する場合、sizeof(CONSTANT_BUFFER_1)でサイズを指定する。
		→理由は上記のようにサイズは16の倍数にしなくてはいけないという前提で、
		→行列は32バイト→　構造体は構造体内の変数で一番バイトサイズの大きいものに最終的にサイズが等倍される。　→変数"4"バイトが一番大きい場合、構造体の最終的なサイズが[11]であっても、[12]という"4"の倍数に繰り上げられる。
		→上記を踏まえて、32バイトの行列が構造体内で一番大きいサイズの変数である場合、構造体は32の倍数に繰り上げられるため。32の倍数は16の倍数の２倍　＝　16の倍数でもある。

		以上のことから行列がある場合のサイズ指定は、sizeof(CONSTANT_BUFFER_1)でよい。

	*/
	//�A
	//コンスタントバッファ１の格納サイズ
	cb.ByteWidth = size;	
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	//CPUからのアクセス権限
		//詳細：WRITE　＝　書き込みを許可する
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//コンスタントバッファを作成する
	HRESULT hr = Direct3D::pDevice->CreateBuffer(&cb, nullptr, ppConstantBuffer1);
	//エラーメッセージ
	ERROR_CHECK(hr, "コンスタントバッファ作成失敗", "エラー");

	//処理の成功
	return S_OK;
}
//コンスタントバッファ１の情報ををシェーダーへ与える
void BaseShader::ShaderConstantBuffer1View(ID3D11Buffer ** ppConstantBuffer1, void * cb1, UINT size)
{
	//前提
	//・Mapを各ポリゴンのDraw時に呼び込むようにする
	//・シェーダークラス、シェーダーファイルにコンスタントバッファ1が存在し、かつ格納サイズが同じであること
	
	//コンスタントバッファ（登録（自クラスメンバ内のコンスタントバッファを））
		//引数：シェーダーファイルにおけるコンスタントバッファの番号（コンスタントバッファ１ならば１）
	//頂点シェーダー用
	Direct3D::pContext->VSSetConstantBuffers(1, 1, ppConstantBuffer1);	
	//ピクセルシェーダー用
	Direct3D::pContext->PSSetConstantBuffers(1, 1, ppConstantBuffer1);	

}
//CubeMap情報をシェーダーへ渡す
void BaseShader::SetCubeMapTexture(UINT suffix)
{
	/*
	//テクスチャの橋渡しを送る
	ID3D11ShaderResourceView* pSRV = Direct3D::pCubeTex->GetSRV();
	//シェーダーに作った変数に渡している
		//hlsl : 例　：　g_normalTexture(t1)
		//第一引数に、(tn) のnを代入
	Direct3D::pContext->PSSetShaderResources(suffix, 1, &pSRV);
	*/

	//Direct3Dにて参照できなくする
		//→そのため、外部からテクスチャをセットできるようにしておく
		//→GameObject側から、テクスチャのロード、を行う。
			//→だが、それではテクスチャのバッファを大量に持つことになるので、
			//　大量に持たないように、共通の領域に持っているとよい。環境のテクスチャとしてStageDataの名前空間などに共通のバッファを持たせておくことが理想。そのテクスチャをセットできるようにする。
	//ToonTextureも同様に
	//Project/CommonData/GameDatas.hなどで

}

void BaseShader::SetToonTexture(UINT suffix)
{

	/*
	//テクスチャの橋渡しを送る
	ID3D11ShaderResourceView* pSRV = Direct3D::pToonTex->GetSRV();
	//シェーダーに作った変数に渡している
		//hlsl : 例　：　g_normalTexture(t1)
		//第一引数に、(tn) のnを代入
	Direct3D::pContext->PSSetShaderResources(suffix, 1, &pSRV);
	*/

	//Direct3Dにて参照できなくする
		//→そのため、外部からテクスチャをセットできるようにしておく
		//→GameObject側から、テクスチャのロード、を行う。
			//→だが、それではテクスチャのバッファを大量に持つことになるので、
			//　大量に持たないように、共通の領域に持っているとよい。環境のテクスチャとしてStageDataの名前空間などに共通のバッファを持たせておくことが理想。そのテクスチャをセットできるようにする。
	//ToonTextureも同様に
	//Project/CommonData/GameDatas.hなどで


}

//コンスタントバッファ１のマップ（CPUからの書き込み、読み込み許可）
HRESULT BaseShader::MapShaderConstantBuffer1(ID3D11Buffer ** ppConstantBuffer1, void * cb1, UINT size)
{
	//マップ後のアクセス情報を格納する変数
		//シェーダーの格納先のメモリなどのアドレスを所有する
		//下記を使い、格納先へのアクセスを試みる
	D3D11_MAPPED_SUBRESOURCE pdata;

	//マップ（アクセス許可）
	//引数：コンスタントバッファ1
	//引数：０
	//引数：D3D11_MAP_WRITE_DISCARD　書き込み許可
	//引数：０
	//引数：マップ後のアクセス情報
	HRESULT hr = Direct3D::pContext->Map((*ppConstantBuffer1), 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);
	//エラーメッセージ
	ERROR_CHECK(hr, "コンスタントバッファのマップの書き込み失敗", "エラー");


	//メモリーをコピー
		//詳細：マップによって、シェーダーへのパスが通ったため、
		//　　：そのパスは、シェーダーファイルのコンスタントバッファ1へとつながっている。
		//　　：パスへ自クラスのメンバにて扱っていたコンスタントバッファ1へ渡す情報を渡す
		//　　：構造体の型はばらばらだが、情報を渡したい。→コピー元の先頭アドレスを入れたい、void 型のアドレス→アドレスであって、特にどの型でもないので、ポインタとして渡すことが可能。voidの、アドレスの型にキャストしている
	//引数：コピー先ポインタ
	//引数：コピー先の書き込まれるサイズ
	//引数：コピー元ポインタ（コピーデータ）
	//引数：コピー元の書き込むサイズ
	memcpy_s(pdata.pData, pdata.RowPitch, cb1, size);	

	//処理の成功
	return S_OK;

	//※Mapを行ったら、必ずUnMapを行う
}

//コンスタントバッファ１のアンマップ（CPUからの書き込み、読み込み拒否）
HRESULT BaseShader::UnMapShaderConstantBuffer1(ID3D11Buffer ** ppConstantBuffer1)
{
	//※Mapを行ったら、必ずUnMapを行う
	//アクセスを拒否
		//詳細：マップしたコンスタントバッファ1へのパスを閉じる。アクセス拒否
	Direct3D::pContext->Unmap((*ppConstantBuffer1), 0);
						
	//処理の成功
	return S_OK;
}

//グラフィックボードのバージョンを文字列で取得
const std::string BaseShader::GetGraphicVersion(FUNCTION_TYPE type)
{
	switch (type)
	{
	//頂点シェーダー
	case FUNCTION_TYPE::FUNCTION_VERTEX:
		return "vs_5_0";
	//ピクセルシェーダー
	case FUNCTION_TYPE::FUNCTION_PIXEL:
		return "ps_5_0";
	default : 
		return " ";
	}
	return " ";
}

//エラーメッセージ取得
const std::string BaseShader::GetErrorMessage(FUNCTION_TYPE type)
{
	switch (type)
	{
	//頂点シェーダー
	case FUNCTION_TYPE::FUNCTION_VERTEX:
		return "頂点シェーダー関数をもとに、シェーダーファイルのコンパイル失敗";
	//ピクセルシェーダー
	case FUNCTION_TYPE::FUNCTION_PIXEL:
		return "ピクセルシェーダー関数をもとに、シェーダーファイルのコンパイル失敗";
	default:
		return "シェーダーファイルのコンパイル失敗";
	}
	return " ";
}


