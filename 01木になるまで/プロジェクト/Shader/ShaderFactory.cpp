#include "ShaderFactory.h"	//ヘッダ
//シェーダーの継承元クラス
#include "../Shader/BaseShader.h"
//シェーダーの継承先クラス
#include "../Shader/ChocolateFashionShader.h"
#include "../Shader/EnvironmentShader.h"
#include "../Shader/FogShader.h"
#include "../Shader/NormalShader.h"
#include "../Shader/PointLightPlusWorldLightShader.h"
#include "../Shader/PointLightShader.h"
#include "../Shader/Simple2DShader.h"
#include "../Shader/Simple3DShader.h"
#include "../Shader/TestShader.h"
#include "../Shader/ToonShader.h"
#include "../Shader/WaterShader.h"
#include "../Shader/WetBallShader.h"
#include "../Shader/SkyBoxShader.h"
#include "../Shader/Blurred2DShader.h"
#include "../Shader/KurosawaModeShader.h"
#include "../Shader/KurosawaMode2DShader.h"
#include "../Shader/ToonOutLineShader.h"
#include "../Shader/DebugWireFrameShader.h"
#include "../Shader/HeightMapShader.h"
#include "../Shader/HeightMapEraseMotionShader.h"
#include "../Shader/GrassShader.h"
#include "../Shader/GateShader.h"
#include "../Shader/PlaneShader.h"
#include "../Shader/ScrollShader.h"
#include "../Shader/FbxEraseMotionShader.h"
#include "../Shader/HeightMapOutLineShader.h"



//シェーダークラス生成関数
BaseShader* ShaderFactory::CreateShaderClass(SHADER_TYPE type)
{
	/*
		FactoryMethod

		引数タイプより、生成するクラスを選別し、
		インスタンスを動的確保し、インスタンスを返す
	*/
	switch (type)
	{
	case SHADER_TYPE::SHADER_3D:
		return new Simple3DShader;
	case SHADER_TYPE::SHADER_2D:
		return new Simple2DShader;
	case SHADER_TYPE::SHADER_TEST:
		return new TestShader;
	case SHADER_TYPE::SHADER_NORMAL:
		return new NormalShader;
	case SHADER_TYPE::SHADER_TOON:
		return new ToonShader;
	case SHADER_TYPE::SHADER_WATER:
		return new WaterShader;
	case SHADER_TYPE::SHADER_WETBALL:
		return new WetBallShader;
	case SHADER_TYPE::SHADER_ENVI:
		return new EnvironmentShader;
	case SHADER_TYPE::SHADER_CHOCO_FASHION:
		return new ChocolateFashionShader;
	case SHADER_TYPE::SHADER_FOG:
		return new FogShader;
	case SHADER_TYPE::SHADER_POINT_LIGHT:
		return new PointLightShader;
	case SHADER_TYPE::SHADER_POINT_LIGHT_PLUS_WORLD_LIGHT:
		return new PointLightPlusWorldLightShader;
	case SHADER_TYPE::SHADER_SKY_BOX :
		return new SkyBoxShader;
	case SHADER_TYPE::SHADER_BLURRED_2D:
		return new Blurred2DShader;
	case SHADER_TYPE::SHADER_OUTLINE:
		return new ToonOutLineShader;
	case SHADER_TYPE::SHADER_KUROSAWA_MODE:
		return new KurosawaModeShader;
	case SHADER_TYPE::SHADER_KUROSAWA_MODE_2D:
		return new KurosawaMode2DShader;
	case SHADER_TYPE::SHADER_DEBUG_WIRE_FRAME:
		return new DebugWireFrameShader;
	case SHADER_TYPE::SHADER_HEIGHT_MAP:
		return new HeightMapShader;
	case SHADER_TYPE::SHADER_HEIGHT_MAP_ERASE_MOTION:
		return new HeightMapEraseMotionShader;



	case SHADER_TYPE::SHADER_GRASS :
		return new GrassShader;

	case SHADER_TYPE::SHADER_GATE :
		return new GateShader;

	case SHADER_TYPE::SHADER_PLANE :
		return new PlaneShader;

	case SHADER_TYPE::SHADER_SCROLL :
		return new ScrollShader;

	case SHADER_TYPE::SHADER_FBX_ERASE_MOTION:
		return new FbxEraseMotionShader;

	case SHADER_TYPE::SHADER_HEIGHT_MAP_OUTLINE :
		return new HeightMapOutLineShader;



	default : 
		return nullptr;
	
	}

	return nullptr;
}
