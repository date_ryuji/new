#include "Blurred2DShader.h"		//ヘッダ

//コンストラクタ
	//継承元の親のコンストラクタを呼び出し初期化
Blurred2DShader::Blurred2DShader() :
	BaseShader::BaseShader()	
{
}
//デストラクタ
	//継承元のデストラクタを呼び出し解放
Blurred2DShader::~Blurred2DShader()
{
	//自身のクラスのシェーダー情報を解放
	BaseShader::~BaseShader();
}

//初期化
	//レベル：オーバーライド
HRESULT Blurred2DShader::Initialize()
{
	//処理の結果
	HRESULT hr = S_OK;

	//シェーダーファイル名
		//ワイド文字（全文字　2バイト）
	const wchar_t shaderFileName[] = L"Assets/Shader/Simple2D.hlsl";


	//頂点シェーダーの作成（コンパイル）
	{

		//★★頂点インプットレイアウト（シェーダーへ渡すポリゴンを作る頂点情報）★★
			//詳細：下記にて定義した情報をシェーダーの頂点シェーダーに渡されて、
			//　　：各頂点ごとの情報を定義する
			//　　：頂点３つで一つのポリゴンとし、３つの頂点の頂点シェーダーが呼ばれて、出力位置を決めて、ピクセルシェーダーに渡されることで描画のピクセル色を決めることができる。
			//詳細：頂点シェーダーへ渡す情報は、シェーダーごとに違う。
			//　　：光の陰影をつけるのであれば、法線が必要。反射をつけるのであれば、接線が必要。と、シェーダーごとに定義する必要がある。
			//詳細：２Dにおいても、３Dにおいてもポリゴンを作り、ポリゴンごとに描画することには変わりないため、ポリゴン描画においては必ず頂点インプットレイアウトを必要とする
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			
			//頂点位置（ローカル座標）
				//詳細：細かい引数などはProject/Shader/BaseShader.cppにて
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },
			//UV位置（テクスチャの読み取り位置）
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};


		//頂点シェーダーの初期化
		//シェーダーファイルごとの頂点シェーダーのレイアウトの確定、コンパイルを済ませる関数呼び込み
			//引数：シェーダーファイル名
			//引数：頂点インプットレイアウト
			//引数：頂点インプットレイアウトのサイズ
			//戻値：処理の結果
		hr = InitVertexShaderForEachShaderFile(shaderFileName, layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)));
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点シェーダーの初期化失敗", "エラー");


	}


	// ピクセルシェーダの作成（コンパイル）
	{
		//ラスタライザ作成
			//詳細：ピクセルの塗り方、ピクセルによって作られるポリゴンのピクセルの塗り方などの設定
			//　　：ポリゴンが描画されるとき、ピクセル単位で塗られている。
			//　　：そのピクセル単位で塗られている表現をラスタという。
		D3D11_RASTERIZER_DESC rdc = {};


		//カリングモード
		//ポリゴンの裏面設定
			//詳細：不要となるポリゴンの設定（ポリゴンはポリゴンを作る3頂点のインデックス情報が時計回りになっているとき、表とする。　反時計回りを裏とする。）
			//　　：FRONT:　表　表示しない
			//　　：BACK :　裏　表示しない
			//　　：NONE :　いらないもの　なし（両面表示）
		rdc.CullMode = D3D11_CULL_BACK;

		//ソリッド
		//ポリゴンの塗り
			//詳細：ポリゴンを作る3頂点をピクセルで塗る際、その塗り方
			//　　：SOLID		：中身を塗りつぶす
			//　　：WIER_FRAME　：ワイヤーフレーム		
			//					　→枠だけなので、三角形にて表現しているので、三角形のつながりとなる
			//　　　　　　　　　　→当たり判定表示、取り合えず表示されているのか知りたいときなど
		rdc.FillMode = D3D11_FILL_SOLID;
		

		//ポリゴンを作るインデックス情報の読み取り方
		//詳細：時計回りを表、反時計回りを表。
		//　　：TRUE  : 反時計回りを　表　とします
		//　　：FALSE : 時計回り　を　表　とします
			/*
				FBX作成時に、面を三角化して、
				△ポリゴンだけで面を表現できる、その面を描画する際に、時計回りで頂点を読み込むのかなどを決める
			*/
		rdc.FrontCounterClockwise = FALSE;
	

		//ピクセルシェーダーの初期化
		//シェーダーファイルごとのピクセルシェーダーのラスタライザの確定、コンパイルを済ませる関数
			//引数：シェーダーファイル名
			//引数：ピクセルシェーダーのラスタライズ方法
			//戻値：処理の結果
		hr = InitPixelShaderForEachShaderFile(shaderFileName, &rdc);
		//エラーメッセージ
		ERROR_CHECK(hr, "ピクセルシェーダーの初期化失敗", "エラー");


	}

	//処理の成功
	return S_OK;
}

//頂点シェーダーのコンパイル
	//レベル：オーバーライド
HRESULT Blurred2DShader::VSCompileFromShaderFile(ID3DBlob** pCompileVS, const wchar_t * shaderFileName)
{
	//自身のシェーダーファイルで頂点シェーダーを初期化
	return BaseShader::VSCompileFromShaderFile(pCompileVS, shaderFileName);
}

//ピクセルシェーダーのコンパイル
	//レベル：オーバーライド
HRESULT Blurred2DShader::PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t * shaderFileName)
{
	//自身のシェーダーファイルでピクセルシェーダーを初期化
		//第3引数：シェーダーファイル内におけるピクセルシェーダーとして使用する関数の「関数名」
		//第4引数：シェーダー関数タイプ
	return BaseShader::CompileFromShaderFile(pCompilePS , shaderFileName , "PS_BlurredColor" , FUNCTION_TYPE::FUNCTION_PIXEL);
}
