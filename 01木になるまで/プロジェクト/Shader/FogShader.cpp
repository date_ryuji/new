#include "FogShader.h"					//ヘッダ
#include "../Engine/DirectX/Direct3D.h"	//Direct3Dにおける　コンテキストへシェーダーの情報を渡すためにインクルード


//コンストラクタ
	//継承元の親のコンストラクタを呼び出し初期化
FogShader::FogShader() :
	BaseShader::BaseShader() , 
	pConstantBuffer1_(nullptr)
{
}

//デストラクタ
	//継承元のデストラクタを呼び出し解放
FogShader::~FogShader()
{
	//自身のクラスのシェーダー情報を解放
	BaseShader::~BaseShader();
	//コンスタントバッファ1の解放
	SAFE_RELEASE(pConstantBuffer1_);
}

//初期化
	//レベル：オーバーライド
HRESULT FogShader::Initialize()
{
	//処理の結果
	HRESULT hr = S_OK;

	//シェーダーファイル名
		//ワイド文字（全文字　2バイト）
	const wchar_t shaderFileName[] = L"Assets/Shader/Fog.hlsl";




	//頂点シェーダーの作成（コンパイル）
	{

		//★★頂点インプットレイアウト（シェーダーへ渡すポリゴンを作る頂点情報）★★
			//詳細：下記にて定義した情報をシェーダーの頂点シェーダーに渡されて、
			//　　：各頂点ごとの情報を定義する
			//　　：頂点３つで一つのポリゴンとし、３つの頂点の頂点シェーダーが呼ばれて、出力位置を決めて、ピクセルシェーダーに渡されることで描画のピクセル色を決めることができる。
			//詳細：頂点シェーダーへ渡す情報は、シェーダーごとに違う。
			//　　：光の陰影をつけるのであれば、法線が必要。反射をつけるのであれば、接線が必要。と、シェーダーごとに定義する必要がある。
			//詳細：２Dにおいても、３Dにおいてもポリゴンを作り、ポリゴンごとに描画することには変わりないため、ポリゴン描画においては必ず頂点インプットレイアウトを必要とする
		D3D11_INPUT_ELEMENT_DESC layout[] = {
		//頂点位置（ローカル座標）
			//詳細：細かい引数などはProject/Shader/BaseShader.cppにて
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
		//UV位置（テクスチャの読み取り位置）
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
		//法線（頂点のY方向）（頂点の向いている方向）
		{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線
		//接線（頂点のX方向）
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 3 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//接線
		
		};
		/*
			接線と法線にて従法線を求めることが可能である。
		*/

		//頂点シェーダーの初期化
		//シェーダーファイルごとの頂点シェーダーのレイアウトの確定、コンパイルを済ませる関数呼び込み
			//引数：シェーダーファイル名
			//引数：頂点インプットレイアウト
			//引数：頂点インプットレイアウトのサイズ
			//戻値：処理の結果
		hr = InitVertexShaderForEachShaderFile(shaderFileName, layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)));
		//エラーメッセージ
		ERROR_CHECK(hr, "頂点シェーダーの初期化失敗", "エラー");

	}


	// ピクセルシェーダの作成（コンパイル）
	{
		//ラスタライザ作成
			//詳細：ピクセルの塗り方、ピクセルによって作られるポリゴンのピクセルの塗り方などの設定
			//　　：ポリゴンが描画されるとき、ピクセル単位で塗られている。
			//　　：そのピクセル単位で塗られている表現をラスタという。
		D3D11_RASTERIZER_DESC rdc = {};

		//カリングモード
		//ポリゴンの裏面設定
			//詳細：不要となるポリゴンの設定（ポリゴンはポリゴンを作る3頂点のインデックス情報が時計回りになっているとき、表とする。　反時計回りを裏とする。）
			//　　：FRONT:　表　表示しない
			//　　：BACK :　裏　表示しない
			//　　：NONE :　いらないもの　なし（両面表示）
		rdc.CullMode = D3D11_CULL_BACK;

		//ソリッド
		//ポリゴンの塗り
			//詳細：ポリゴンを作る3頂点をピクセルで塗る際、その塗り方
			//　　：SOLID		：中身を塗りつぶす
			//　　：WIER_FRAME　：ワイヤーフレーム		
			//					　→枠だけなので、三角形にて表現しているので、三角形のつながりとなる
			//　　　　　　　　　　→当たり判定表示、取り合えず表示されているのか知りたいときなど
		rdc.FillMode = D3D11_FILL_SOLID;

		//ポリゴンを作るインデックス情報の読み取り方
			//詳細：時計回りを表、反時計回りを表。
			//　　：TRUE  : 反時計回りを　表　とします
			//　　：FALSE : 時計回り　を　表　とします
				/*
					FBX作成時に、面を三角化して、
					△ポリゴンだけで面を表現できる、その面を描画する際に、時計回りで頂点を読み込むのかなどを決める
				*/
		rdc.FrontCounterClockwise = FALSE;

		//ピクセルシェーダーの初期化
		//シェーダーファイルごとのピクセルシェーダーのラスタライザの確定、コンパイルを済ませる関数
			//引数：シェーダーファイル名
			//引数：ピクセルシェーダーのラスタライズ方法
			//戻値：処理の結果
		hr = InitPixelShaderForEachShaderFile(shaderFileName, &rdc);
		//エラーメッセージ
		ERROR_CHECK(hr, "ピクセルシェーダーの初期化失敗", "エラー");

	}

	//シェーダーのコンスタントバッファ2つ目に渡すための、コンスタントバッファを初期化
		//引数：コンスタントバッファ1ポインタ
		//引数：コンスタントバッファ1における渡す情報（CONSTANT_BUFFER_1(struct)）のサイズ（16の倍数）
		//制限：第2引数は必ず16の倍数になるようにする
		//　　：第2引数が16の倍数でない場合、強制的に一番サイズの近い16の倍数に繰り上げる
	hr = BaseShader::CreateConstantBuffer1(&pConstantBuffer1_, 16);
	//エラーメッセージ
	ERROR_CHECK(hr, "コンスタントバッファ１を送るためのポインタの作成失敗", "エラー");

	//処理の成功
	return S_OK;
}

//頂点シェーダーのコンパイル
	//レベル：オーバーライド
HRESULT FogShader::VSCompileFromShaderFile(ID3DBlob** pCompileVS, const wchar_t * shaderFileName)
{
	//自身のシェーダーファイルで頂点シェーダーを初期化
	return BaseShader::VSCompileFromShaderFile(pCompileVS, shaderFileName);
}
//ピクセルシェーダーのコンパイル
	//レベル：オーバーライド
HRESULT FogShader::PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t * shaderFileName)
{
	//自身のシェーダーファイルでピクセルシェーダーを初期化
	return BaseShader::PSCompileFromShaderFile(pCompilePS, shaderFileName);
}

//コンスタントバッファ1（struct）の情報を格納する
	//アクセス：GameObject側より、自身のクラスへ情報を格納する
	//制限　　：�@自身のクラス（ChocolateFashionShader）へ格納する際、Direct3Dより、自身クラス（ChocolateFashionShader）のインスタンスを取得。
	//　　　　：�A自身のクラスのコンスタントバッファ1（struct）への格納構造体のインスタンス（struct）をGameObject側にて定義。（格納する情報の定義）
	//　　　　：�B�@によって取得した自身クラスのインスタンスの下記関数へ、�Aの情報を渡すことで、動的に格納情報を書き換える
	//　　　　：上記の�@〜�BをGameObject側の各Draw時に行うことで、フレームごとの動的なシェーダー表現を行うことも可能である。（GameObect側からシェーダーの操作が可能となる）
void FogShader::SetConstantBuffer1Data(CONSTANT_BUFFER_1 * BufferData1)
{
	BufferData1_.backGroundColor = BufferData1->backGroundColor;
}


//コンスタントバッファ1（Buffer）のマップ（アクセス許可）
HRESULT FogShader::MapShaderConstantBuffer1View()
{
	//自身クラス管理のコンスタントバッファ1（Buffer）（シェーダーファイル内のコンスタントバッファ1へのパスが通ったバッファ）への
	//アクセス許可
	//情報の橋渡しを可能とする
	return BaseShader::MapShaderConstantBuffer1(
		&pConstantBuffer1_,
		(void*)(&BufferData1_),
		sizeof(BufferData1_));
}

//コンスタントバッファ1（Buffer）のアンマップ（アクセス拒否）
HRESULT FogShader::UnMapShaderConstantBuffer1View()
{
	//自身クラス管理のコンスタントバッファ1（Buffer）（シェーダーファイル内のコンスタントバッファ1へのパスが通ったバッファ）への
	//アクセス拒否
	//情報の橋渡しを不可能とする
	return BaseShader::UnMapShaderConstantBuffer1(
		&pConstantBuffer1_);
}

//自身のクラスのコンスタントバッファ1（Buffer）をシェーダーファイルに渡す（Viewによる橋渡し）
	//詳細：シェーダーファイルに渡すことで、シェーダー内のコンスタントバッファ1に情報が適用される。
void FogShader::ShaderConstantBuffer1View()
{
	//シェーダーのコンスタントバッファに渡すための処理関数を呼び出す
	BaseShader::ShaderConstantBuffer1View(
		&pConstantBuffer1_,
		(void*)(&BufferData1_),
		sizeof(BufferData1_));


	//CubeMapのテクスチャを渡す
		//引数：シェーダーファイルにおける　テクスチャ番号（下記参照）
	SetCubeMapTexture(2);
	/*
	//シェーダーに作った変数に渡している
		//hlsl : 例　：　g_normalTexture(t1)
		//第一引数に、(tn) のnを代入
	Direct3D::pContext->PSSetShaderResources(2, 1, &pSRV);
	*/

}

//コンスタントバッファ1（struct）のコンストラクタ
FogShader::CONSTANT_BUFFER_1::CONSTANT_BUFFER_1() :
	backGroundColor(XMVectorSet(0.f , 0.f, 0.f, 0.f))
{

}

