#include "SplashSceneManager.h"							//ヘッダ
#include "../../CommonData/GlobalMacro.h"				//共通マクロ
#include "../../Engine/GameObject/UIGroup.h"			//シーン内画像、ボタンを合わせたUI群
#include "../../Engine/Scene/SceneUIManager.h"			//シーン内UI画像管理クラス
#include "../../Engine/Scene/SceneChanger.h"			//シーン切り替えクラス
#include "../../Engine/GameObject/FadeInAndFadeOut.h"	//UIGroupにおける画像遷移クラス


//コンストラクタ
SplashSceneManager::SplashSceneManager(GameObject * parent) :
	SceneManagerParent::SceneManagerParent(parent, "SplashSceneManager"),
	pFadeInAndFadeOut_(nullptr)
{
}

//デストラクタ
SplashSceneManager::~SplashSceneManager()
{
}

//初期化
void SplashSceneManager::Initialize()
{
	/*シーンマネージャー定義時の必須処理*******************************************************************/
		//詳細：マネージャーとしての機能を持たせるための前提処理、初期化
		//　　：（条件：リソースが存在する場合必須）
		//　　：リソースとは、この場合、連携オブジェクト群における連携オブジェクトが存在するか
		//　　：　　　　　　　　　　　　UIGroup軍におけるUIGroupが存在するか。
		//　　：上記のリソースが一つも存在しない場合に呼び込む必要はない

	//連携オブジェクト群のポインタを保存しておく配列の初期化、生成
	//継承先である、自身のクラスで宣言した、連携オブジェクトを指定するEnum値をもとに、連携オブジェクトの数を引数として渡す
	//関数先にて、引数分の連携オブジェクトを入れる枠、要素を確保してくれる
	NewArrayForCoopObjects((int)SPLASH_SCENE_COOP_OBJECTS::SPLASH_COOP_MAX);
	//UIオブジェクト群のポインタを保存しておく配列の初期化、生成
	NewArrayForUIGroups((int)SPLASH_SCENE_UI_GROUP_TYPE::SPLASH_UI_GROUP_MAX);

	/************************************************************************************************************/

}
//更新
void SplashSceneManager::Update()
{
	//FadeInFadeOutにて管理しているUiGroupのUI画像の
	//遷移がすべて終了したら
	//シーンを切り替える
	if (pFadeInAndFadeOut_ != nullptr && 
		pFadeInAndFadeOut_->IsEndingTransition())
	{
		SceneChanger* pSceneChanger = (SceneChanger*)FindObject("SceneChanger");
		pSceneChanger->ChangeScene(SCENE_ID::SCENE_ID_TITLE);
	}

}
//描画
void SplashSceneManager::Draw()
{
}
//解放
void SplashSceneManager::Release()
{
	//シーン内連携オブジェクトの解放
		//解放といっても、オブジェクトはGameObject型であるため、Deleteはしない。マネージャークラス内にて管理しているオブジェクトを登録する配列ポインタの解放
	DeleteArrayForCoopObjects();
	//UIオブジェクト群の解放
	DeleteArrayForUIGroups();
}
//シーン管理の常駐UIGroupの個数を返す
int SplashSceneManager::GetUIGroupCount()
{
	return (int)SPLASH_SCENE_UI_GROUP_TYPE::SPLASH_UI_GROUP_MAX;
}
//シーン内のUIGroupの遷移（FadeInFadeOut）の開始宣言
void SplashSceneManager::StartTransition()
{
	//画面遷移を行うクラスの作成
	pFadeInAndFadeOut_ = (FadeInAndFadeOut*)Instantiate<FadeInAndFadeOut>(this);

	//遷移情報の登録
	//画像数の取得
	int IMAGE_COUNT = -1;
	UIGroup* pUIGroup = GetUIGroup((int)SPLASH_SCENE_UI_GROUP_TYPE::SPLASH_UI_GROUP_SPLASH);
	{
		SceneUIManager* pUIManager = pUIGroup->GetSceneUIManager();
		IMAGE_COUNT = pUIManager->GetUINum();
	}

	//画像それぞれの遷移時間の登録
	float* transitionTime = new float[IMAGE_COUNT]
	{
		//所属
		5.0f,
		//使用エンジン
		5.0f

	};
	//画像それぞれのα値１．０ｆの画像表示時間の登録
	float* stopingTime = new float[IMAGE_COUNT]
	{
		//所属
		3.0f,
		//使用エンジン
		1.0f

	};


	//遷移を行うUIGroupの登録
	pFadeInAndFadeOut_->SetTransitionUIGroup(
		pUIGroup, IMAGE_COUNT, transitionTime, stopingTime);


	//遷移開始
	pFadeInAndFadeOut_->StartTransition();


	//動的確保要素の解放
	SAFE_DELETE_ARRAY(stopingTime);
	SAFE_DELETE_ARRAY(transitionTime);

}
