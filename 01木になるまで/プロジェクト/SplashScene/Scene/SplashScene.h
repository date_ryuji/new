#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/SceneParent.h"	//シーンオブジェクト　抽象クラス


//クラスのプロトタイプ宣言
class UIGroup;
class SplashSceneManager;


/*
	クラス詳細	：スプラッシュシーン　
	クラスレベル：サブクラス（スーパークラス（SceneParent））
	クラス概要（詳しく）
				：所属ロゴ、使用エンジンロゴを遷移するシーン

*/
class SplashScene : public SceneParent
{
//private メソッド
private:

	//UI画像作成処理
		//詳細：UIGroupへUI画像のロードを担うメソッド
		//引数：UIGroup
		//引数：画像ファイル名
		//戻値：UIGroupへのハンドル
	int AddStackUI(UIGroup* pUIGroup, const std::string& FILE_NAME);

	//シーンの常駐UIGroupを作る
		//詳細：UIGroup（SceneManagerにて管理する）を作成、SceneManagerに登録する
		//　　：SceneManagerにてUIGroupの管理ができるようにする
		//引数：スプラッシュシーンマネージャー
		//戻値：なし
	void CreateSceneUIGroups(SplashSceneManager * pSceneManager);

//public ゲームループメソッド
public:
	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	SplashScene(GameObject* parent);

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

};