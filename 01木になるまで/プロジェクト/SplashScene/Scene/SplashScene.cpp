#include "SplashScene.h"						//ヘッダ
#include "../../Engine/GameObject/UIGroup.h"	//UIGroup　シーン内UI群
#include "../../Engine/Scene/SceneUIManager.h"	//シーンのUIマネージャー
#include "../../CommonData/SceneType.h"			//シーンマネージャー識別ID
#include "../../CommonData/GameDatas.h"			//ゲーム内情報定義
#include "SplashSceneManager.h"					//シーンマネージャー


//コンストラクタ
//SceneParentのコンストラクタ呼び込み
	//引数：親オブジェクト
	//引数：シーン名のオブジェクト名（シーン識別情報群(Project/CommonDatas/SceneType.h)よりシーンオブジェクト名を取得する（シーンをID識別しオブジェクト名取得））
SplashScene::SplashScene(GameObject * parent)
	: SceneParent::SceneParent(parent, SCENE_OBJECT_NAME[(int)SCENE_ID::SCENE_ID_SPLASH])
{
}

//初期化
void SplashScene::Initialize()
{
	//ゲームの背景色を切り替える
		//背景色の変化
	//背景色取得
	GameDatas::SetGameBackGroundColor(BACKGROUND_COLOR_BLACK);


	//スプラッシュシーンクラスのマネージャー作成
		//詳細：SceneParentを継承しているため、
		//　　：継承元関数から、シーンマネージャークラスを作成させる
		//制限　　：シーンマネージャークラスは、GameObject型継承のオブジェクト作成関数である、Instantiateでの作成は推奨されない。
		//制限理由：制限をかけるのは、シーンマネージャーが作成されたことを伝えなくてはいけないクラスが存在するためである。
	SplashSceneManager* pSceneManager = 
		(SplashSceneManager*)CreateSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_SPLASH);


	//シーン内の常駐UI作成
	CreateSceneUIGroups(pSceneManager);

	//UIGroupの遷移開始
		//スプラッシュシーンとして、
		//UIGroupへ登録したUIを、遷移し、チームロゴなどの遷移表示を行う。
		//その遷移の開始を宣言
	pSceneManager->StartTransition();


}

//更新
void SplashScene::Update()
{
}

//描画
void SplashScene::Draw()
{
}

//解放
void SplashScene::Release()
{
}

//UI画像作成処理
int SplashScene::AddStackUI(UIGroup * pUIGroup, const std::string& FILE_NAME)
{
	//引数UIGroupに追加する
	return pUIGroup->AddStackUI(FILE_NAME);
}
//シーンの常駐UIGroupを作る
void SplashScene::CreateSceneUIGroups(SplashSceneManager * pSceneManager)
{
	//UIGroupの作成


	//スプラッシュ群
	{
		//UIGroup作成
		//インスタンス生成
		UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

		//UIGroupをSceneMnagerにセットする
		//SPLASH_UI_GROUP_AFFILIATION
		//所属ロゴUI群
		pSceneManager->AddUIGroup((int)SPLASH_SCENE_UI_GROUP_TYPE::SPLASH_UI_GROUP_SPLASH, uiGroup);


		//UI画像
		{
			//所属ロゴ　
			//ロードと追加
			int hSchool = AddStackUI(uiGroup, "Assets/Scene/Image/SplashScene/SchoolLogo.jpg");
			{
				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

				//サイズの可変
				pSceneUIManager->SetPixelScale(hSchool, 800, 600);
				//位置の移動
				//pSceneUIManager->SetPixelPosition(handle, 1000, 100);
			}

			//使用エンジン
			//ロードと追加
			int hDirectX = AddStackUI(uiGroup, "Assets/Scene/Image/SplashScene/MadeWithDirectX11.png");
			{
				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();

				//サイズの可変
				pSceneUIManager->SetPixelScale(hDirectX, 800, 600);
				//位置の移動
				//pSceneUIManager->SetPixelPosition(handle, 1000, 100);
			}
			
		}

		//UIGroupの非表示
		pSceneManager->InvisibleSceneUIGroup((int)SPLASH_SCENE_UI_GROUP_TYPE::SPLASH_UI_GROUP_SPLASH);
	}

}
