#include "AlphaBlendForAlphaExpression.h"	//ヘッダ

//コンストラクタ
AlphaBlendForAlphaExpression::AlphaBlendForAlphaExpression():
	BaseBlend::BaseBlend()
{
}

//デストラクタ
AlphaBlendForAlphaExpression::~AlphaBlendForAlphaExpression()
{
	BaseBlend::~BaseBlend();
}

//初期化
HRESULT AlphaBlendForAlphaExpression::Initialize()
{
	//ブレンド（色の加減）宣言
	D3D11_BLEND_DESC BlendDesc;
	ZeroMemory(&BlendDesc, sizeof(BlendDesc));
	BlendDesc.AlphaToCoverageEnable = FALSE;
	BlendDesc.IndependentBlendEnable = FALSE;

	//半透明許可　
	BlendDesc.RenderTarget[0].BlendEnable = TRUE;
	
	//今から書き込む色の対処（混ぜ具合の方法）
	BlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;

	//元ある色の対処（混ぜ具合の方法）
	BlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;

	//何算の計算をするか
	BlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		
	BlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	BlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	BlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	BlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	//ブレンド状態を登録
	HRESULT hr = CreateBlendState(&BlendDesc);
	//エラーメッセージ
	ERROR_CHECK(hr, "ブレンド設定をもとにブレンド状態のポインタ作成失敗", "エラー");

	//処理の成功
	return S_OK;
}
