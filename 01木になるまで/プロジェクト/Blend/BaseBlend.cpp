#include "BaseBlend.h"					//ヘッダ
#include "../CommonData/GlobalMacro.h"	//共通マクロ


//★透過処理における
//　描画の順番

/*
//透過の処理を行うとき、
//出現の順番を考えなくてはいけない　→　正しくは、描画の順番
//Instantiateする順番を考えないと、
//透過がうまくいかない　→このうまくいかないというのは、　
//仮に　Water　a = 0.5f
//  Ground a = 1.0f の順番で描画した場合、
//Waterの透過部分の下に、Groundのオブジェクトが仮にあったとしても、
//Groundの色を込みした透過にはならず、
//背景の色を映してしまう。

//そのため、　Waterの下に、Groundの色を載せたいのであれば、
//Ground , Waterの順番で描画をしなくてはいけない。
//Waterの段階で、　今ある描画の色に自身の色を重ねる（αを含んだ計算）をするため、　のちの描画するピクセルが、　その透過のピクセルよりも遠ければ、　Zバッファの関係で映らなくなる。
//そうすれば、　Groundの色が反映されないのは当然

★
Instantiate<Taurus>(this);
Instantiate<Ground>(this);


Instantiate<Water>(this);	//透過（Ground , Taurusのオブジェクトを透過先に移したいのならば、　必ず、Waterは、最後にする。（描画の関係↑））
★



*/


//コンストラクタ
BaseBlend::BaseBlend()
{
}

//デストラクタ
BaseBlend::~BaseBlend()
{
	//構造体の動的確保情報の解放
	SAFE_RELEASE(blendInfo_.pBlendState);
}

//初期化
HRESULT BaseBlend::Initialize()
{
	//ブレンド　（ピクセルの混ぜ具合）情報を設定
		/*
			
			//透過処理を実現するために、色の混ぜ具合を登録
			//ブレンドステート
			//以下のコードを追加するだけで、　透過する
			//要素を変えることで、エフェクトに適した透過にできる　
		*/
	D3D11_BLEND_DESC BlendDesc;
	//構造体要素にNULLを代入
	ZeroMemory(&BlendDesc, sizeof(BlendDesc));
	//AlphaToCoverageの許可
		//重なり合うポリゴンが複数存在するときに重宝する手法
		//RenderTarget[0]のみ使用する場合はFALSE
	BlendDesc.AlphaToCoverageEnable = FALSE;
	//独立したブレンドを有効にするか
		//RenderTarget[0]のみ使用する場合はFALSE
	BlendDesc.IndependentBlendEnable = FALSE;

	//半透明処理の許可
	BlendDesc.RenderTarget[0].BlendEnable = TRUE;	
		/*
			半透明の定義とは

		//青背景、赤タマ
		//青　rgb(０,０,１)　赤 rgb(１,０,０)

		赤　100 で　半透明が0.7
		青　001　で　半透明　1.0

		前後は　赤前、　青後



		この時、αが７０％の赤がある。
		その後ろに１００％の青。

		1 0 0 0.7

		最終的な色は
		(0.7, 0, 0.3, 1)



		0.7％は　前の赤の値
		0.3％は　後の青の値を使っている


		つまり
		赤（1, 0, 0, 0.7）
		青（0, 0, 1, 1  ）

		なので、　
		前にある
		赤は、0.7 = 70 % の色を使い、　残りの３０％は後ろの値を使います。

		最終的な色
		R = (1 * 0.7 + 0 * (1 - 0.7))


		最終的には1にならないと

		
		*/
	
	//今から書き込む色の対処（混ぜ具合の方法）
	BlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		/*
		//D3D11_BLEND_SRC_ALPHA;	//現在	表示されているピクセルの色を込みした、今から書き込もうとしているピクセルの色とで最終的な色を計算する
									//src （= 今から表示しようとしているものは、）　	今から書き込む　色　をα値分使います
										//Src = ALPHA , DEST = INV_SRC_ALPHA ならば　（赤でα７０％の色を、青ピクセルの上に書き込むとき　＝今ある色青　３０％と　今から書き込む色赤　をα７０％分足した色を最終的な色とします。　＝結果　色は赤紫のような色になる）
		//D3D11_BLEND_ONE;			//100%　＝　そのピクセル自体のαを関係なく、　→　背景のRGBを足していく　→不透明のオブジェクトも、後ろの色の色を自身に透過させる
									//つまり、背景の色が移る	//この場合、透明のシェーダーにしているものは、後ろにどの色があるかは関係なく、世界の背景の色を移すので、
									//透過のオブジェクトの下にオブジェクトの色があっても、それを透過しなくなる
		*/


	//元ある色の対処（混ぜ具合の方法）
	BlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		/*
		//D3D11_BLEND_INV_SRC_ALPHA;	//Dest=すでに使われているものは、　今から表示しようとしているものの逆INV　３０％を使う。　青を３０％
		*/
	/*
	
	★（例）

	Src = ONE
	Dest = INV_SRC_ALPHA

	にすると、
	ハイライトは、透過しにくく、
	透過の部分は透過する。

	エフェクトによく使われる


	★加算合成

	エフェクトを作り、
	炎、　キラキラ
	→半透明のオブジェクトを光らせる。

	→場合によっては、表示のオブジェクトごとに変えて、　
	加算合成

	そのα値を関係なく、　
	加算する。
	なので、透過しているように見せる。ことが可能（エフェクトに使える）

	*/


	//何算するのか
	BlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		/*
		//D3D11_BLEND_OP_SUBTRACT;	//減算する、　背景の色（scr ONE Dest INV）を引くことになる。＝なので、黒くなる。　闇の魔法や、重力魔法などに使える
		//D3D11_BLEND_OP_ADD;		//上記を足す	//単純な透過
		*/
		//上記を変えることで、　表示方法を変えられる

	//α値の設定
	BlendDesc.RenderTarget[0].SrcBlendAlpha			= D3D11_BLEND_ONE;
	BlendDesc.RenderTarget[0].DestBlendAlpha		= D3D11_BLEND_ZERO;
	BlendDesc.RenderTarget[0].BlendOpAlpha			= D3D11_BLEND_OP_ADD;
	BlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	//ブレンド状態を登録
	HRESULT hr = CreateBlendState(&BlendDesc);
	//エラーメッセージ
	ERROR_CHECK(hr, "ブレンド設定をもとにブレンド状態のポインタ作成失敗", "エラー");

	//処理の成功	
	return S_OK;

}

//自身のブレンド（色の加減）をセットする
void BaseBlend::SetBlend()
{
	//ブレンドにかかわらず共通にセットする情報
	float blendFactor[4] = { D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO };
	//自身のブレンド情報をセットする
	Direct3D::pContext->OMSetBlendState(blendInfo_.pBlendState, blendFactor, 0xffffffff);
}

//ブレンド（色の加減）の作成
HRESULT BaseBlend::CreateBlendState(D3D11_BLEND_DESC * blendDesc)
{
	//最終的にpBlendStateを渡して、作成している。
		//そのため、
		//何個か、BlenｄStateを作って起き、
		//Setで、切り替えられるようにしておけば（Shaderのように）様々な透過、BlenｄStateを表現できる。（セットは、SetBlend（）にて）
	return Direct3D::pDevice->CreateBlendState(blendDesc, &blendInfo_.pBlendState);

}
