#pragma once
#include "BaseBlend.h"		//ブレンドクラス　スーパークラス


/*
	クラス詳細	：標準のα値による透過を行う　ブレンド方法
	クラスレベル：サブクラス（スーパークラス：BaseBlend）
	クラス概要（詳しく）
				：
				・計算方法：α値　半透明　ブレンド
				・表現　　：α値　表現のため
				・ピクセルが重なった際、すでに出力されている色ピクセル�@、そのうえでα値が1未満のピクセル�Aを描画する際、
				　�@の色に　�Aのα値分加算させた色を出力（正しい計算方法はBaseBlendにて）

*/
class AlphaBlendForAlphaExpression : public BaseBlend
{
//public メソッド
public : 
	//コンストラクタ
		//引数：なし
		//戻値：なし
	AlphaBlendForAlphaExpression();

	//デストラクタ
		//引数：なし
		//戻値：なし
	~AlphaBlendForAlphaExpression() override;

	//ブレンド（色の加減）の初期化
		//詳細：色の加減の許可、色の加減方法を定義
		//レベル：仮想関数
		//引数：なし
		//戻値：処理の結果（成功：S_OK , 失敗：E_FAIL）
	HRESULT Initialize() override;


};

