#pragma once
#include "BaseBlend.h"		//ブレンドクラス　スーパークラス


/*
	クラス詳細	：色を減算し　出力色を反転させる　ブレンド方法
	クラスレベル：サブクラス（スーパークラス：BaseBlend）
	クラス概要（詳しく）
				：
				・計算方法：色の減算　
				・表現　　：α値無効　重力魔法が掛けられたように、色の反転が行える
				・重なった色の減算
*/
class SubBlendForGravityMagicExpression : public BaseBlend
{
//public メソッド
public:
	//コンストラクタ
		//引数：なし
		//戻値：なし
	SubBlendForGravityMagicExpression();
	//デストラクタ
		//引数：なし
		//戻値：なし
	~SubBlendForGravityMagicExpression() override;

	//ブレンド（色の加減）の初期化
		//詳細：色の加減の許可、色の加減方法を定義
		//レベル：仮想関数
		//引数：なし
		//戻値：処理の結果（成功：S_OK , 失敗：E_FAIL）
	HRESULT Initialize() override;


};

