#include "BlendFactory.h"							//ヘッダ

//ブレンドの継承元クラス
#include "BaseBlend.h"
//ブレンドの継承先クラス
#include "AlphaBlendForAlphaExpression.h"			//α値の透過処理
#include "SubBlendForGravityMagicExpression.h"		//重力魔法表現

//ブレンド（色の加減）クラスのインスタンスを作成し、インスタンスを返す
//パターン：FactoryMethod
BaseBlend* BlendFactory::CreateBlendClass(BLEND_TYPE type)
{
	switch (type)
	{
	case BLEND_TYPE::BLEND_ALPHA_FOR_ALPHA:
		return new AlphaBlendForAlphaExpression;
	case BLEND_TYPE::BLEND_SUBTRACT_FOR_GRAVITY_MAGIC:
		return new SubBlendForGravityMagicExpression;

	default:
		return nullptr;

	}

	return nullptr;
}
