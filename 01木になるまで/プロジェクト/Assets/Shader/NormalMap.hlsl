/*
	シェーダー詳細	：ノーマルマップシェーダー（疑似の凹凸）＋反射
	使用シェーダー(BaseShaderを継承するクラス)　
					：NormalShader
	使用可能ポリゴングループ(PolygonGroupを継承するクラス)
					：Fbx ,
	シェーダー概要（詳しく）
					：TestShader陰影表現
					：ノーマルマップ画像を使用した疑似凹凸
					：環境マップを反射させて、反射表現
*/



/*
	■描画テクスチャ＆サンプラーデータのグローバル変数定義
		詳細：（CPU側から読み込んだ、テクスチャ―のテクスチャ情報、テクスチャの読み込み方法などのサンプラーを受け取り、読み込みUVから、描画ピクセル職を取得する）
*/
/*
	テクスチャなどを取得しておく領域をregister(〇n)などで指定する
		→テクスチャの場合、register(tn)
		→サンプラーの場合、register(sn)
		→コンスタントバッファの場合、register(bn)

		※この場合、nには、テクスチャーなどの格納領域の添え字を指定する。
		　0オリジン。連番。
		  CPU側から書き込む、渡す際に、どの領域に渡すのか、その添え字として使用する。

		※コンスタントバッファ1番目
		　1番目のコンスタントバッファ
		　コンスタントバッファをセットするときに、第一引数へ　１と指定する。
		　そうすることで、　書き込み先を1番目と指定する。
		　あとは、値と同様の値をメモリコピーして渡す

*/
//テクスチャ―（０）
	//詳細：モデルに付加されたテクスチャ
Texture2D	g_texture : register(t0);	
//テクスチャー（１）
	//詳細：ノーマルテクスチャ（疑似の凹凸用のRGB値のテクスチャ）
Texture2D	g_normalTexture : register(t1);
	/*
		ノーマルマップ
	
		法線をRGBの色表現にして表している
		法線の方向　００１（Z方向に伸びる法線）
			　 RGB　００１　で青になる

		というように、法線方向をRGB値に置き換えて、嘘の法線のｘｙｚを表している。
	*/
//テクスチャー（２）
	//詳細：SkyBox（環境マップ用のテクスチャ）
	//　　：Cubeのテクスチャを追加する
	//　　：今回はキューブのテクスチャなので、　TextureCube型のテクスチャを用意
	//　　：テクスチャであるので、格納先はテクスチャは(tn)に格納
TextureCube	g_cubeTexture : register(t2);
//サンプラー（０）
SamplerState	g_sampler : register(s0);



/*
	■コンスタントバッファーのグローバル格納構造体
		詳細：DirectX側（CPU側）から送信される頂点情報以外の、頂点情報を描画する描画位置などを決める際に必要な情報群
		　　：頂点バッファーにて受け取り可能である情報は、あくまでも、頂点のローカル座標、ポリゴンのテクスチャ貼り付け位置（UV）、などであるため、実際、3D空間上のどの位置へ描画するかなどの情報が存在しない。そのため、
			：頂点情報とは別に、グローバルである領域に、スクリーン座標に変換するための行列などが必要になる。
			：コンスタントバッファも複数持たせることが可能。(bn)の連番にて管理。

			：コンスタントバッファのサイズは16の倍数になる必要がある。
			：→構造体のサイズの法則性により、構造体内の変数の中で一番大きいサイズの変数（下記によると行列32バイト）が構造体の最終的なサイズの倍数となる。
			：　　→つまり、最終的に最終的な構造体のサイズが60バイトであり、上記における一番大きいサイズの変数が32バイトの行列であった場合、　60バイトは32バイトの倍数に拡張される。
			：　　　→構造体サイズ最終的には、64バイトに拡張される。

			：話を戻し、コンスタントバッファ変数内のサイズは16の倍数でなくてはいけない。
			：そのため、行列があれば32バイトのサイズに拡張されるため、16の倍数のサイズになる。
			：だが、行列が存在しない場合、16の倍数にはならないため、CPU側の、シェーダーファイルのコンスタントバッファーを定義する際に16の倍数二なるように確保するようにする。
			：　→詳細は、Shader/BaseShader.cppにて確認
			：　　→コンスタントバッファー内に行列が存在する場合、深く考える必要はない。
			：　　→コンスタントバッファ（１）以降などで、特別送るモノがあるときに深く考える。

*/
//コンスタントバッファー（０）：1つ目
cbuffer global : register(b0)
{
	/*
		ワールド座標で位置管理を行っているため、
		・カメラのプロジェクション行列とビュー行列を取得し、
		・ポリゴン事のワールド＊ビュー＊プロジェクションの合成行列を使用して、ローカル座標を合成行列を掛けて、プロジェクション座標として取得する

	*/
	/*
		行列
		4次元のベクトルの行列であるため、４＊４（float4*4）
	*/
	/*
		ベクトル
		4次元ベクトルを代入するためのfloat4つ(float4)
	*/


	//ワールド＊ビュー＊プロジェクションの合成行列
		//詳細：頂点のローカル座標をプロジェクション座標に変換するための行列
	float4x4	matWVP;

	//ワールド行列
		//詳細：法線を回転させるために、回転行列を受け取らなければいけない
		//　　：移動行列なし、回転行列、拡大行列の逆行列　を計算してワールド行列として取得
	float4x4	matNormal;

	//ワールド行列（移動座標＊回転座標＊拡大座標）
	float4x4	matWorld;

	//カメラの位置
		//詳細：ハイライト表示、反射を再現するために、カメラの位置（つまり視点）が欲しい
	float4 camPos;

	//マテリアルの色情報
		//詳細：テクスチャがない場合
		//　　：ポリゴンのマテリアルそのものの色（MAYAにおけるDeffuseマテリアルの色を代入する）
	float4		diffuseColor;

	//フォンシェーディングのためのマテリアル情報
	//環境光色
		//詳細：環境光（黒色の描画でも、一定量明るさを持っている。陰を一定量明るくするための光。）その色
	float4		ambientColor;
	//ハイライト色
		//詳細：光の反射で一部分明るくなる部分。その色
	float4		supecularColor;
	//輝度
		//詳細：ハイライトの明るさの度合い。
	float		shininess;

	//テクスチャが存在するかのフラグ
		//詳細：テクスチャのバッファーからテクスチャを読み込んで、出力色を確定するかのフラグ
	bool		isTexture;
};



/*
	■頂点シェーダー出力＆ピクセルシェーダー入力データ構造体
		詳細：頂点シェーダーにて、各頂点ごとの出力座標や、出力時の参照UVなどを代入しておく構造体
		　　：以下構造体から情報を参照して、出力位置の参考とする。
			：頂点ごとの座標を代入することで、頂点間（頂点Aから、頂点Bまでの間のピクセル）を補完する機能も持ち合わせる。その際に参考にするデータ群
		詳細：あくまでもただの構造体である。
		　　：中身に格納されている情報が特殊であるだけ。
		詳細：格納変数の語尾に : SV_POSITIONなどが付与されている
			：→これらはセマンティクスといって、格納する情報の種類を定義するものである。定義を行わなければ格納が許可されない。
			：SV_POSITION	座標
			：TEXCOORD0		UV座標
			：NORMAL		法線
			：TEXCOORD1 ~	他4次元情報（好きな情報を与える際に使用できる。0は必ずUVでなくてはいけない）
			：				下記構造体に格納した値は、補完されるため、TEXCOORD1の値も補完されるようになるため、値が変動されるので注意
*/
struct VS_OUT
{
	//描画スクリーン座標
		//詳細：頂点シェーダーよりプロジェクション行列と頂点のローカル座標の計算（この際に使用する関数にて自動的にビューポート行列も掛けられる＝結果としてスクリーン座標を取得可能）にて最終的に取得したスクリーン座標
	float4 pos		: SV_POSITION;
	//UV
		//詳細：頂点の参照するテクスチャなどのUV位置
	float2 uv		: TEXCOORD;
	//視線ベクトル
		//詳細：カメラのワールド座標
	float4 eye : TEXCOORD1;
	//ライト方向
	float4 light : TEXCOORD2;

};


//初期化関数
	//詳細：hlslの構造体へコンストラクタをつけることができないため、
	//　　：専用の初期化関数を用意
	//引数：VS_OUT型の初期化する変数　（inoutにより、参照渡しとなる）
	//戻値：なし
void InitVS_OUT(inout VS_OUT outData)
{
	outData.pos = float4(0.f, 0.f, 0.f, 0.f);
	outData.uv = float2(0.f, 0.f);
	outData.eye = float4(0.f, 0.f, 0.f, 0.f);
	outData.light = float4(0.f, 0.f, 0.f, 0.f);
	
}




/*
	頂点シェーダー

		詳細：ポリゴン事の頂点情報を引数として取得し、頂点ごとの出力スクリーン座標を取得したりなど、頂点ごとの描画位置を確定される。
		　　：「頂点」ごとに呼ばれる関数
			：CUP側から、頂点インプットレイアウトにて指定した情報を渡される。
			：光の方向による出力色の計算なども行う

*/
//頂点シェーダー（標準）
	//詳細：標準頂点シェーダー（シェーダーファイルの標準頂点シェーダー）
	//　　：CPU側から、シェーダーファイルにおける頂点シェーダーの指定がない場合、下記の頂点シェーダーが呼ばれる。
	//　　：ローカル座標とプロジェクション行列からスクリーン座標を確定する
	//引数：頂点座標（ローカル座標）（引数においてもセマンティクスにて指定が必要）
	//引数：UV（頂点インプットレイアウトの情報にてXMVECTORというfloat4型にて渡されるため、受け取り側もfloat4にて受け取る。しかし、使用するのはfloat2のxy情報のみである）
	//引数：法線（頂点における垂直方向）
	//引数：接線　タンジェント（法線と接線を使って、従法線を求める、　それを用いてノーマルマップを実装する）
	//戻値：ピクセルシェーダーへわたす情報群（最低必要な情報：スクリーン座標、UV座標）
VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD, float4 normal : NORMAL , float4 tangent : TANGENT)
{

	//ノーマルマップ
	{

		//戻値に返す構造体
		VS_OUT outData;
		//初期化
		InitVS_OUT(outData);

		//行列とベクトルの計算（Direcｔ３DのtransCood関数のようなもの）
		outData.pos = mul(pos, matWVP);
		outData.uv = uv.xy;


		//接線と法線とで、従法線を求める
			//接線と法線に垂直な線である、従法線
				//→外積
				//→cross 
		float3 binormal = cross(normal.xyz, tangent.xyz).xyz;


		normal.w = 0;	//normalを使う前に、normal.wに、変な値が入ってしまっている、→なので、その値を初期化しておく
		tangent.w = 0;	//一応tangentのwも０に


		//法線は、ノーマルマップにて扱う				
			//だが、法線をそのまま使用するわけにはいかない。
		//接線空間に置ける　ｘｙｚに変換する必要がある。

		//まず、ワールド行列で変形
		//物体の回転も、ノーマルにかかわってくる
		//接線も　従法線も
		normal = mul(normal, matNormal);
		tangent = mul(tangent, matNormal);
		binormal = mul(float4(binormal , 0.f), matNormal).xyz;


		//ライト
		float4 light = float4(1, -1, 1, 0);
		light = normalize(light);	//長さを正規化


		//ライトを固定の世界ライト方向ではなく、
			//接線空間（その面における垂直方向をZ, としたときのｘｙｚ方向がある空間、　ワールド空間とは別の空間。）
			//における、そのライトの方向に変形すればよい。
			//ワールド空間（ｘｙｚ）がある中でのｘｙｚではなく、
				//接線空間の中でのｘｙｚの値に変換する

		//その際に、
			//それは、内積を行えば、求められる

		//位置情報を別空間への変換。
			//元ある、世界の傾きにあるｘｙｚを、　別の世界の傾きにあるｘｙｚの方向に傾ける。　別世界のｘｙｚ方向におけるｘ方向の値を求める
		//ライトをtangentの方向基準に傾ける
			//tangent方向になんぼなのかを求める
		outData.light.x = dot(light, tangent);
		outData.light.y = dot(light, float4(binormal , 0.f));
		outData.light.z = dot(light, normal);

		


		//見る位置も固定だが、
			//世界を傾けることで、　あっちに向いている、　こっちに向いていると変換しなければいけない。
		//視線ベクトルも傾ける（かたむけるというよりは、正しくは、ワールドのｘｙｚを、接線空間におけるｘｙｚに変形）
			//ライトベクトルと同様に






		//V その位置から見たカメラの位置（視線。視点へのベクトル）
				//頂点ごと違う
				//カメラの位置をシェーダーに持ってきて、頂点の位置・カメラの位置で内積をとって、角度が欲しい
					//カメラ→FBX→FBXのコンスタントバッファ→シェーダーのコンスタントバッファにコピー
			//カメラから頂点を引けば、その頂点から見た、カメラの位置になる（ベクトルの引き算）
			//頂点はローカル位置である、　だが、頂点は回転しているので、ワールド行列を掛けてやらないといけない(このワールド行列は、移動＊回転＊拡大)
		//float4 eye = normalize(camPos - mul(pos, matWorld));
		float4 eye = camPos - mul(pos, matWorld);
		//霧
		//視点からの距離で
			//頂点位置が、視点から一定以上離れているときは、ピクセルを一定値白くする。
			//ピクセルを白に近づける
		

		/*float4 camWorld = mul(camPos, matWVP);

		float disX = outData.pos.x - camWorld.x;
		float disY = outData.pos.y - camWorld.y;
		float disZ = outData.pos.z - camWorld.z;

		float dis = sqrt((disX * disX) + (disY * disY) + (disZ * disZ));
*/

		//dis = distance(outData.pos, eye);
		
		////頂点から見た、
		//	//カメラの位置
		//	//カメラから、頂点まで伸びるベクトルを取得できる
		//	//そのため、そのベクトルの長さを求める(専用の関数を用いる)
		//float dis = length(eye);

		////距離により、
		//	//霧の色とするRGB値に　
		//	//遠ければ、値を大きくし、
		//	//遠くに行くほど遠く
		//if (dis < 7.0)
		//{
		//	outData.fog = float4(0, 0, 0, 0);
		//}
		//else if (dis < 10.0)
		//{
		//	outData.fog = float4(0.3, 0.3, 0.3, 0);
		//}
		//else if (dis < 15.0)
		//{
		//	outData.fog = float4(0.5, 0.5, 0.5, 0);
		//}
		//else if (dis < 20.0)
		//{
		//outData.fog = float4(0.7, 0.7, 0.7, 0);
		//}



		eye = normalize(eye);
		outData.eye.x = dot(eye, tangent);
		outData.eye.y = dot(eye, float4(binormal , 0.f));
		outData.eye.z = dot(eye, normal);





		//情報を変更した構造体をピクセルシェーダーに返す
		return outData;
	}
	//ランバートシェーダーでは、頂点方向の法線から間を保管していた。
	//ピクセルシェーダーにて、
		//法線のｘｙｚを、頂点の法線ではなく、
		//テクスチャのRGBを使用すればよい。



}

//頂点シェーダー（ワイヤーフレーム）
	//詳細：ワイヤーフレームの頂点シェーダー
	//引数：頂点座標（ローカル座標）（引数においてもセマンティクスにて指定が必要）
	//戻値：ピクセルシェーダーへわたす情報群（最低必要な情報：スクリーン座標、UV座標）
VS_OUT VS_WIRE_FRAME(float4 pos : POSITION)
{
	//戻値に返す構造体
	VS_OUT outData;
	//行列とベクトルの計算（Direcｔ３DのtransCood関数のようなもの）
	outData.pos = mul(pos, matWVP);
	//outData.uv = uv;

	return outData;
}



/*
	ピクセルシェーダー

		詳細：ポリゴンを作るピクセルの描画の際のピクセル色を決めるシェーダー関数
		　　：ピクセル単位で呼ばれる
		  　：上記を考慮して、複雑な計算をさせるとピクセルごとであるため、極端に重くなる原因となる。

*/
//ピクセルシェーダー（標準）
	//詳細：標準ピクセルシェーダー（シェーダーファイルの標準ピクセルシェーダー）
	//　　：CPU側から、シェーダーファイルにおけるピクセルシェーダーの指定がない場合、下記の頂点シェーダーが呼ばれる。
	//　　：スクリーン座標上におけるピクセルの出力色を出力
	//引数：頂点シェーダーから受け取った情報群（最低必要な情報：スクリーン座標、UV座標）
	//戻値：ピクセルに出力する色(float4(r , g , b , a))
float4 PS(VS_OUT inData) : SV_TARGET
{



//ノーマルマップ　＋　反射
{
	//float4 light = float4(1, -1, 1, 0);
	//光下方向、法線上方向　これは、１８０度になる。
	//※１．なので光の計算の時は、ライトの方向を逆にしないと、上方向の法線と正しい計算を行えない
	//�B光の向きを正規化	//これは光の向きであり、強さではない(強さを強くしたいときは、光のfloat値を大きくする)
		//★必要であれば、コンスタントバッファにて、光の向きを変えられるようにすればよい。
	inData.light = normalize(inData.light);

	//ランバートシェーダーでは、頂点方向の法線から間を保管していた。
//ピクセルシェーダーにて、
	//法線のｘｙｚを、頂点の法線ではなく、
	//テクスチャのRGBを使用すればよい。

//ノーマルマップの、いま見ているUV位置を
	//ノーマルマップのRGB値を取得し、
	//それを法線とする。
		//こうすることで、偽の法線を作り、明るさ表現
		//=でこぼこ
	float4 normal = g_normalTexture.Sample(g_sampler, inData.uv) * 2 - 1;
	//RGBをそのままｘｙｚにできない、
	//xyzはー１　〜１までの値。
		//なので、上記に合わせる必要がある。

	//XYZ    0  0  0であるならば、
	//RGBは　0.5 0.5 0.5 にする必要がある。

	//XYZ    1  0  0
	//RGB    1  0.5  0.5

	//XYZ    -1 0 0 
	//RGB    0  0.5 0.5


	/*
		２倍にして　−１

		-１　~　１が２差なので、　同様にRGBも２の差にして、
		−１をすれば、　−１〜１の範囲になる


	*/


	//上記の法線を使用して、
		//光の内積などを下でとる
	//normal = normalize(normal);

	//画像のUVが入ってしまうので初期化
	normal.w = 0;




	//環境光　アンビエント
	// ka * ia 
	// ka シーン全体の環境光
	// ia 物体の環境光
		//環境光はシーンに掛けることもできるし、物体にも欠けることができる
	float4 ambient = ambientColor;


	//id 物体自体の色。 黄色を掛けると黄色になる　＝　物体自体の色。持っている色。
		//idをdiffuseColor
		//＝モデルそのものの色を使う
	float4 id;

	//テクスチャが張られていたら、テクスチャのピクセルの色を付ける
	if (isTexture == true)
	{
		//テクスチャのピクセルを取得する
		//UV座標をもとに、テクスチャの張る部分を選択
			//UVは頂点情報なので、
			//頂点シェーダーにて、受け取らなくてはいけない。

		id = g_texture.Sample(g_sampler, inData.uv);
	}
	//テクスチャが張られていなければ、
	//そのものの色
	else
	{
		id = diffuseColor;
	}





	//光の内積
	//L・Nはどっちから内積で角度を求めても同じ、
		//光との角度から内積で明るさ
		//90度を超えるとき、明るさ0にしてほしい
		//saturate () 0から１の間に切り詰めてくれる(0を下回ったら０に、1を上回ったら１に)
	//法線が、間を保管してくれる時、
		//単純に半分にするので、
		//長さが変わってしまうときがある。なので、今回は、正規化をして、長さを1にする。
	float4 LN = saturate(dot(-inData.light, normalize(normal)));

	//LN * id = 拡散反射光
	float4 diffuse = LN * id;

	//ia カット
	//R ライトの反射ベクトル（リフレクション）
		//専用の関数がある
		//引数:ライトのベクトル (進む方向になるので、−にはしない)
		//引数:法線　（この法線に対して、ライトの反射）
	float4 R = reflect(inData.light, normalize(normal));

	////KS 鏡面反射率（ハイライトの強さ）０ならハイライトなし、
	//	//大きくなれば、キュッとする
	float ks = 2;

	//α　光沢度(ハイライトの広がり具合、大きさ)
	//float shininess = 5;

	////is ハイライトの色　＝　ハイライトを薄いグレーにすれば、ハイライトの強い、弱い
	float4 is = supecularColor;

	//鏡面反射光
	//float4 specular = ks * pow(saturate(dot(R, normalize(inData.eye))), shininess) * is;
		//FBXから取得したスペキュラーの色を使用する
	float4 specular = ks * pow(saturate(dot(R, normalize(inData.eye))), shininess) * is;



	//環境マップ
	//当然、テクスチャを読み込んでから、
	/*

		反射を再現する方法の一つ


	//////////////////////////////////////////
	ノーマルの中に、反射を実現すると、
	水にぬれているような表現ができる。

	//////////////////////////////////////////


	*/
	//視線と面の法線の反射ベクトルを求める
	float3 eyeRef = reflect(normalize(inData.eye), normalize(normal)).xyz;
	eyeRef.y *= -1.0f;
	//テクスチャがさかさまになってしまうので、
	//反射ベクトルを逆にする



	//その反射ベクトルを伸ばして、
	//それぞれの視線に反射した反射ベクトルを出せたので、
	//そのベクトルが当たるピクセルを色を出せば、　四角の箱の世界のテクスチャの中から、ベクトルの当たる位置のピクセルをもらうので、映り込みを再現できる

	//テクスチャにてUVを指定しているが、
		//そのUVの位置に、視線のベクトルを指定すれば、
		//ベクトルが伸びたときの当たったピクセルの色を求められる
	//この時に使用するテクスチャは、
		//画像を6面に張った、キューブのテクスチャである。
	float4 envColor = g_cubeTexture.Sample(g_sampler, eyeRef);





	//return envColor;
		//テストで出力して、
		//背景が反射しているかを確認

	//だが、これを、　影の色を含んだ色にしなくてはいけない。
	//反射の色と、　そのものの色にする

	//反射率を計算
	//（そのもの）diffuse 50% + （反射の色）envColor 50%で　合計１００％にするようにして、反射させる

		//反射率を変えたいとき
			//・そのものの色を強くしたいときは、　diffuseの％を大きくすればよく、
			//・反射の　　色を強くしたいときは、　envColorの％を大きくすればよく、
	float reflectance = 0.7f;
	diffuse = diffuse * (1.0f - reflectance) + envColor * reflectance;
			//上記を、
			//コンスタントバッファとして、
				//反射の値を渡されるとよい。









	//最終的な色
	float4 color = ambient + diffuse + specular;
	//pow = 乗　

	//return color + inData.fog;
	return color;
}



}


//ピクセルシェーダー（ワイヤーフレーム）
	//詳細：ワイヤーフレーム用ピクセルシェーダー
	//　　：指定色描画
	//引数：頂点シェーダーから受け取った情報群（最低必要な情報：スクリーン座標、UV座標）
	//戻値：ピクセルに出力する色(float4(r , g , b , a))
float4 PS_WIRE_FRAME(VS_OUT inData) : SV_Target
{
	//ワイヤーフレームの色出力
	//白
	//return float4(1,1,1,1.0f);

	
	//return float4(0.5f , 0.5f , 0.5f ,1.0f);
	return float4(0.0f , 0.5f , 0.0f , 1.0f);

}

