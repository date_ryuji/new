/*
	シェーダー詳細	：2Dシェーダー
	使用シェーダー(BaseShaderを継承するクラス)　
					：Simple2DShader , Blurred2DShader , KurosawaMode2DShader , 
	使用可能ポリゴングループ(PolygonGroupを継承するクラス)
					：Sprite , 
	シェーダー概要（詳しく）
					：2D用の平面ポリゴンを描画するシェーダー
					　２D用であるため、イメージとしては、プロジェクション座標におけるXYZ値によって、描画座標を決める
					：2D用の他シェーダー表現を行う際に使用する
					；2Dであるため、陰の陰影表現が必要のない

*/

/*
	■描画テクスチャ＆サンプラーデータのグローバル変数定義
		詳細：（CPU側から読み込んだ、テクスチャ―のテクスチャ情報、テクスチャの読み込み方法などのサンプラーを受け取り、読み込みUVから、描画ピクセル職を取得する）
		　　：サンプラーは、テクスチャにおいて共通に使用する。サンプラーはテクスチャの読み方であるため、読み方は一通りでよい。
*/
/*
	テクスチャなどを取得しておく領域をregister(〇n)などで指定する
		→テクスチャの場合、register(tn)
		→サンプラーの場合、register(sn)
		→コンスタントバッファの場合、register(bn)

		※この場合、nには、テクスチャーなどの格納領域の添え字を指定する。
		　0オリジン。連番。
		  CPU側から書き込む、渡す際に、どの領域に渡すのか、その添え字として使用する。
*/
//テクスチャ―（０）
	//詳細：画像テクスチャ
Texture2D	g_texture : register(t0);	
//サンプラー（０）
SamplerState	g_sampler : register(s0);	


/*
	■コンスタントバッファーのグローバル格納構造体
		詳細：DirectX側（CPU側）から送信される頂点情報以外の、頂点情報を描画する描画位置などを決める際に必要な情報群
		　　：頂点バッファーにて受け取り可能である情報は、あくまでも、頂点のローカル座標、ポリゴンのテクスチャ貼り付け位置（UV）、などであるため、実際、3D空間上のどの位置へ描画するかなどの情報が存在しない。そのため、
			：頂点情報とは別に、グローバルである領域に、スクリーン座標に変換するための行列などが必要になる。
*/
//コンスタントバッファー（０）
cbuffer global : register(b0)
{
	/*
		プロジェクション座標管理を行っているため、
		・カメラのプロジェクション行列など必要なし、
		・ワールド＊ビュー＊プロジェクションの合成行列なども必要なし
	
	*/

	//プロジェクション行列
		//詳細：DirectX側（CUP側）にて座標管理を行い、座標ベクトルを行列として取得し、最終出力位置のスクリーン座標を求めるために使用する行列
		//　　：ワールド座標という名目となっているが、あくまでもプロジェクション座標（モデル自体の座標）として座標位置管理を行い、その座標を行列として取得している
		//　　：各ポリゴンの頂点のローカル座標を下記のプロジェクション行列と掛けることで、プロジェクション座標を求めることができる。
		//　　：それ以降のビューポート行列との計算は、ピクセルシェーダーなどに任せることができる。
		//詳細：4次元のベクトルの行列であるため、４＊４
	float4x4	matW;	

	//画像のα値
		//詳細：ポリゴン事のα値の設定、画像を透過させる際の透過率
		//制限：0.0f ~ 1.0f
	float alpha;

};

/*
	■頂点シェーダー出力＆ピクセルシェーダー入力データ構造体
		詳細：頂点シェーダーにて、各頂点ごとの出力座標や、出力時の参照UVなどを代入しておく構造体
		　　：以下構造体から情報を参照して、出力位置の参考とする。
			：頂点ごとの座標を代入することで、頂点間（頂点Aから、頂点Bまでの間のピクセル）を補完する機能も持ち合わせる。その際に参考にするデータ群
		詳細：あくまでもただの構造体である。
		　　：中身に格納されている情報が特殊であるだけ。
		詳細：格納変数の語尾に : SV_POSITIONなどが付与されている
			：→これらはセマンティクスといって、格納する情報の種類を定義するものである。定義を行わなければ格納が許可されない。
			：SV_POSITION	座標
			：TEXCOORD0		UV座標
			：NORMAL		法線
			：TEXCOORD1 ~	他4次元情報（好きな情報を与える際に使用できる。0は必ずUVでなくてはいけない）
*/
struct VS_OUT
{
	//描画スクリーン座標
		//詳細：頂点シェーダーよりプロジェクション行列と頂点のローカル座標の計算（この際に使用する関数にて自動的にビューポート行列も掛けられる＝結果としてスクリーン座標を取得可能）にて最終的に取得したスクリーン座標
	float4 pos		: SV_POSITION;	
	//UV
		//詳細：頂点の参照するテクスチャなどのUV位置
	float2 uv		: TEXCOORD;		

};


/*
	頂点シェーダー

		詳細：ポリゴン事の頂点情報を引数として取得し、頂点ごとの出力スクリーン座標を取得したりなど、頂点ごとの描画位置を確定される。
		　　：「頂点」ごとに呼ばれる関数

*/
//頂点シェーダー（標準）
	//詳細：標準頂点シェーダー（シェーダーファイルの標準頂点シェーダー）
	//　　：CPU側から、シェーダーファイルにおける頂点シェーダーの指定がない場合、下記の頂点シェーダーが呼ばれる。
	//　　：ローカル座標とプロジェクション行列からスクリーン座標を確定する
	//引数：頂点座標（ローカル座標）
	//引数：UV
	//戻値：ピクセルシェーダーへわたす情報群（最低必要な情報：スクリーン座標、UV座標）
VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD)
{
	//ピクセルシェーダーへ渡す情報群定義
	VS_OUT outData;

	//スクリーン座標定義
		//詳細：専用関数にてプロジェクション行列とローカル座標を計算する　→本来は、ここで求められる座標はプロジェクション座標となるはず。
		//　　：しかし、シェーダーファイル側にて自動で、ビューポート行列との計算をし、その結果であるスクリーン座標が求められる。
		//　　：そのタイミングの詳細は不明だが、
		//　　：�@この専用関数における計算にてスクリーン座標になっている
		//　　：�Aピクセルシェーダーに渡された際にスクリーン座標となる
		//　　：上記のどれかである。ピクセルシェーダーはピクセルごとに呼ばれる＝スクリーン座標が確定しているということ（スクリーンの座標を作るのはピクセルなので）
	outData.pos = mul(pos, matW);

	//UV定義
		//＝テクスチャの座標（テクスチャ読み込みの際の読み込み位置）
	outData.uv = uv.xy;

	//ピクセルシェーダーへわたす
	return outData;

}


//α値のセット
	//詳細：コンスタントバッファーにて渡された指定α値をセットする
	//　　：しかし、そのままα値をセットすると、pngの背景透過の透過率が崩れてしまう。（pngのα値　0.0f , コンスタントバッファ指定α値 0.5となったら、0.5が適用されるので、背景透過が崩れる）
	//　　：上記を考慮して、コンスタントバッファの指定α値を出力色のα値に掛ける。すると、現在のα値を考慮したα値計算を行うことができる。
	//　　：現在のα値の〇％のα値で出力
	//引数：出力色
	//戻値：α値セット済み出力色
float4 SetAlpha(float4 color)
{
	//コンスタントバッファに取得したα値をセット
		//しかし、現段階だと、pngの背景透過のピクセルが、背景透過の機能を失ってしまうため、
		//下記はそのまま使用することができない
		//解決案�@：現在のα値 * alpha
		//解決案�A：深度バッファを編集(背景透過は、ブレンドの機能で色合成を行っているため、ピクセルの最終色を出したうえで、透過率を計算している、。そのため、ピクセルシェーダーにおけるα値を考慮しても、ブレンドの機能に影響しない可能性もある。　ブレンドや、深度バッファなどのピクセルシェーダー外の機能の変更で解決するものの可能性もある)
	//color.a = alpha;

	//解決↓（解決案�@）
	color.a *= alpha;
		//現在渡されているα値に、指定α値を掛ける。
		//すると、現在のα値　に　指定α値でさらに薄くするか、という判断となる
			//背景透過の部分は、背景透過部分を標準の色で透過し、　自身の色が塗ってあるピクセル部分は、指定α値分透過される
		//→成功


	return color;
}

/*
	ピクセルシェーダー

		詳細：ポリゴンを作るピクセルの描画の際のピクセル色を決めるシェーダー関数
		　　：ピクセル単位で呼ばれる
		  　：上記を考慮して、複雑な計算をさせるとピクセルごとであるため、極端に重くなる原因となる。

*/
//ピクセルシェーダー（標準）
	//詳細：標準ピクセルシェーダー（シェーダーファイルの標準ピクセルシェーダー）
	//　　：CPU側から、シェーダーファイルにおけるピクセルシェーダーの指定がない場合、下記の頂点シェーダーが呼ばれる。
	//　　：スクリーン座標上におけるピクセルの出力色を出力
	//引数：頂点シェーダーから受け取った情報群（最低必要な情報：スクリーン座標、UV座標）
	//戻値：ピクセルに出力する色(float4(r , g , b , a))
float4 PS(VS_OUT inData) : SV_Target
{
	//通常ピクセル色
	float4 color = (g_texture.Sample(g_sampler, inData.uv));

	//color.a = 0.5f;	//アルファ値を指定すると、　全体のテクスチャ部分のアルファ値が変わるので、　全体に対してアルファ値を替える処理は行えない

	//α値の計算
	color = SetAlpha(color);

		//ピクセル、→テクスチャの色そのまま出ればいい
		//３Dにおける、★★影（明るさ）などの情報をすべて撤去すればいい
return color;	//テクスチャのそのものの色
								//*inData.colorは、明るさなどを変えるときに使用したので、いらない
								//テクスチャそのものの色

}


//ピクセルシェーダー（ブラー）
	//詳細：ブラーシェーダー
	//　　：周囲のマスの色を出力ピクセルの色に重ねることでモザイクを掛けることができる
	//制限：計算量が大きくなるため、極力使わない。あるいは計算方法を改める
	//引数：頂点シェーダーから受け取った情報群（最低必要な情報：スクリーン座標、UV座標）
	//戻値：ピクセルに出力する色(float4(r , g , b , a))
float4 PS_BlurredColor(VS_OUT inData) :SV_Target
{
	//通常ピクセル色
//float4 color = (g_texture.Sample(g_sampler, inData.uv));

//2マス以上用の色とで平均を取る際には、上下左右2マス先を取るだけでなく、1マス右上なども必要になる
	//つまり、十字が広がるのではなく、
	//ダイヤのような形で広がっていくイメージが、平均を取るのに適している
//マスを広げる範囲
		//５なら5マス先まで上下左右のピクセルを広げて、　かつ、ひし形のように、範囲を広げていく
		//その位置のピクセル色を足して、足した数分割ることで、平均の色を取得する
int loop = 5;
int sumColor = 0;
float4 defaultColor = (g_texture.Sample(g_sampler, inData.uv));	//通常出力カラー
float4 blurredColor = (g_texture.Sample(g_sampler, inData.uv));	//初期のカラーを、通常のピクセル色にする
sumColor += 1;
float widthPix = (1.0 / 800.f);
float heightPix = (1.0 / 600.f);
for (int i = 1; i <= loop; i++)
{

		float4 up = g_texture.Sample(g_sampler, inData.uv + float2(0, heightPix * (float)i));
		float4 down = g_texture.Sample(g_sampler, inData.uv - float2(0, heightPix * (float)i));
		float4 right = g_texture.Sample(g_sampler, inData.uv + float2(widthPix * (float)i, 0));
		float4 left = g_texture.Sample(g_sampler, inData.uv - float2(widthPix * (float)i, 0));
		blurredColor =
			blurredColor + up + down + right + left;


		sumColor += 4;



}



//斜め位置色も足すのだが、
		//数が大きくなるほど、
		//塗る数がおおくなるため、再帰などの処理が必要になる
		//g_texture.Sample(g_sampler, inData.uv - float2(0, (1.0 / 600.f) * (float)i)) +
		//g_texture.Sample(g_sampler, inData.uv - float2(0, (1.0 / 600.f) * (float)i)) +

		//＋Y　上に伸ばしたピクセルから見て、
			//�@右下
			//�@＿１：＋X方向に　1 ~ (i - 1)までのピクセル　の色を足して
			//�@＿２：- Y方向に　(i - 1) ~ 1までのピクセル　の色を足して

			//�A左下
			//�A＿１：- X方向に　-1 ~ -(i - 1)までのピクセル　の色を足して
			//�A＿２：- Y方向に　(i - 1) ~ 1までのピクセル　の色を足して

		//- Y　↓に伸ばしたピクセルから見て、
			//�B右上
			//�B＿１：＋X方向に　1 ~ (i - 1)までのピクセル　の色を足して
			//�B＿２：＋Y方向に　-(i - 1) ~ -1までのピクセル　の色を足して

			//�C左上
			//�C＿１：- X方向に　-1 ~ -(i - 1)までのピクセル　の色を足して
			//�C＿２：＋Y方向に　-(i - 1) ~ -1までのピクセル　の色を足して

		//これをそれぞれ色を足すことが出来れば、
		//ひし形上に広がるピクセルの合計を取ることが可能
		//X
for (int x = 1; x <= loop - 1; x++)
{
	//�@＿１
		// X は　forの要素
		// Y は　i - 1
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(widthPix * (float)x, heightPix * (float)(i - 1))
			);

	//�A＿１
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(-(widthPix * (float)x), heightPix * (float)(i - 1))
			);

	//�B＿１
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(widthPix * (float)x, -(heightPix * (float)(i - 1)))
			);

	//�C＿１
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(-(widthPix * (float)x), -(heightPix * (float)(i - 1)))
			);

	sumColor += 4;

}


//Y
for (int y = 1; y <= loop - 1; y++)
{
	//�@＿２
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(widthPix * (float)(i - 1), heightPix * (float)y)
			);

	//�A＿２
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(-(widthPix * (float)(i - 1)), heightPix * (float)y)
			);


	//�B＿２
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(widthPix * (float)(i - 1), -(heightPix * (float)y))
			);

	//�C＿２
	blurredColor +=
		g_texture.Sample(
			g_sampler, inData.uv +
			float2(-(widthPix * (float)(i - 1)), -(heightPix * (float)y))
		);

	sumColor += 4;

}


float4 color = blurredColor / sumColor;

//α値の計算
color = SetAlpha(color);


return color;

/*
//ピクセルをボケさせる
	//一ピクセルを基準に、そのピクセルの上下左右のピクセルの色をもらって、
	//そのピクセル色を足して、計算した合計ピクセル数で割る（平均をとる）
	float4 color = (g_texture.Sample(g_sampler, inData.uv) +
		g_texture.Sample(g_sampler, inData.uv + float2(1.0 / 800 , 0)) +
		g_texture.Sample(g_sampler, inData.uv - float2(1.0 / 800 , 0)) +
		g_texture.Sample(g_sampler, inData.uv + float2(0, 1.0 / 600)) +
			g_texture.Sample(g_sampler, inData.uv - float2(0, 1.0 / 600))) / 5;


*/

}



/*
	ぼかしについて

	//ぼかしたいなら、
//そのピクセルの　周囲のピクセルの色を足して割る（平均）をとることができれば、
//ぼかすことが可能

//右隣りのドットを見るには、
//スクリーンがW８００＊H６００ならば、
	//右は　８００分の１　足した位置


	//上下左右の大きさを大きくして、その合計を足して、足した数分割るとすれば、
		//大きい数合計して、割れば、　大きなボケを作れる

	//Yは足さずに、Xだけを足してやる。
		//右横に延びているようなものもできる。

	//きちんとぼかすなら、
	//重みをとる

	より使いたい色を大きい値で取っておいて、
	合計、　で大きい色のピクセル色が出るように。




	//一部分モザイクも、可能。
	//一部分をSpriteとして取得して、その部分だけモザイク処理も可能



*/

//ピクセルシェーダー（黒澤２D）
	//詳細：黒澤モード２DVer
	//　　：黒澤モード（白黒、モノトーン表現）を可能とする
	//　　：単純に、最終に出力される色が一定以上なら白。一定以下ならば黒を描画するという計算をしている。
	//引数：頂点シェーダーから受け取った情報群（最低必要な情報：スクリーン座標、UV座標）
	//戻値：ピクセルに出力する色(float4(r , g , b , a))
float4 PS_KUROSAWA(VS_OUT inData) : SV_Target
{

	float4 color = g_texture.Sample(g_sampler, inData.uv);	//テクスチャのそのものの色

//白黒に切り詰める
//最終的な色のRGB　は　０〜１の値である。
//つまりRGBの合計が3に近ければ白、　0に近ければ黒にすればよい
float sum = color.r + color.g + color.b;

float4 lastColor;

if (sum > 2.5f)
{
	lastColor = float4(1, 1, 1, 1);
}
else if (sum > 2.0f)
{
	lastColor = float4(0.7f, 0.7f, 0.7f, 1);
}
else if (sum > 1.5f)
{
	lastColor = float4(0.5f, 0.5f, 0.5f, 1);
}
else if (sum > 1.0f)
{
	lastColor = float4(0.3f, 0.3f, 0.3f, 1);
}
else
{
	lastColor = float4(0, 0, 0, 1);
}

//α値の計算
lastColor = SetAlpha(lastColor);


return lastColor;



}







