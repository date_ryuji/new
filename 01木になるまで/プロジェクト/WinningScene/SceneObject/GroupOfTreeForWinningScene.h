#pragma once
//システム内　標準ヘッダ(usr/)
#include <list>		//リストを使用するためにインクルード（途中で中身を消去する可変長配列）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス

//WinningScene専用のGroupOfTreeForWinningScene
	//GameSceneから不要な機能を削ったクラス



//クラスのプロトタイプ宣言
class Tree;
//enum class のプロトタイプ宣言
enum class WINNING_SCENE_COOP_OBJECTS;
enum class TREE_PARTS;


/*
	クラス詳細	：木群クラス（ウィニングシーン限定）
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用シーン　：WinningScene
	クラス概要（詳しく）
				：GameSceneにて使用していた、GroupOfTreeから、不要なメソッドなどを削ったクラス	
				：木を管理するクラス

*/
class GroupOfTreeForWinningScene : public GameObject
{
//private メンバ定数
private : 
	//木の生成可能個数
		//詳細：プレイヤーの所有木グループ限定の個数
	static constexpr int MAXIMUM_NUMBER_ = 5;

	//木のサイズ
	static constexpr XMVECTOR DEFAULT_SCALE_ = { 1.0f, 1.0f, 1.0f, 0.0f };


//private 構造体
private : 
	//描画のTransformと
	//対象の描画対象の木オブジェクトを登録しておく構造体
	struct DrawTreeInfo
	{
		//描画のTransform
		Transform* pSourceTransform;

		//描画対象の木オブジェクトのポインタ
		Tree* pTreeSource;

		//初期設置時のポリゴン番号
		int polyNum;


		//コンストラクタ
		DrawTreeInfo();
	};

//private メンバ変数、ポインタ、配列
private : 

	//現在の生成個数
	int currentNumberOfTrees_;

	//木オブジェクトクラスのカウンター
		//詳細：木のオブジェクトクラスが生成されたらカウントアップする
	unsigned int treeNumCounter_;

	//自身のタイプ
	WINNING_SCENE_COOP_OBJECTS myType_;

	//描画対象の木オブジェクト
		//詳細：クラス内で共通のオブジェクトを使う場合に、GroupOfTreeForWinningSceneのクラスが最初に定義されたときに必ず定義するオブジェクト
		//　　：★描画の対象のLSYSTEMが指定されていない場合、この初めに定義したLSYSTEMが描画の対象となる
		//　　：全て共通の形を持たせた木オブジェクトを描画させるので、
		//　　：クラスは一つだけ用いて、
		//　　：クラスは、GameObjectクラスなので、オブジェクトクラスのTransformをTransform群に登録されているTransformに動的に変化させて
		//　　：Drawを呼びこむ
		//　　：→そうすれば、　クラスは一つで、違う位置に、同様のモデルをDrawできる
	Tree* pTreeSources_;


	//描画対象の木オブジェクト群
		//詳細：登録された木オブジェクト群
	std::list<DrawTreeInfo*> pSourceTransforms_;

	//木インスタンス群
		//詳細：描画対象内に存在する木オブジェクトのポインタ群
		//　　：描画対象は、下記の木オブジェクト群の中から、木オブジェクトを選択して構造体に格納している
	std::list<Tree*> pListTreeSources_;

//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	GroupOfTreeForWinningScene(GameObject* parent);
	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~GroupOfTreeForWinningScene();

	//初期化
		//詳細：自身のタイプを選択して、セットする
		//引数：セットするオブジェクトのタイプ（GroupOfTreeは、複数のインスタンスを生成する。そのためインスタンスのタイプを分けるためのタイプ分け）
		//戻値：なし
	void Initialize(WINNING_SCENE_COOP_OBJECTS myType);

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public:


	//LSYSTEM実行	
		//詳細：LSYSTEM実行可能なTreeのLSYSTEMを一回実行 
		//引数：なし
		//戻値：なし
	void ExecutionLSystem();
	//木オブジェクト作成
		//詳細：木生成
		//引数：なし
		//戻値：なし
	void CreateATree();

	//AddListリストへ木のオブジェクトを追加
		//引数：追加のTransform
		//引数：木の立地ポリゴン番号
		//引数：追加するLsystemのSourceのインスタンスクラス（選択がない場合、nullptrとする）（必須：LsystemのポインタはInstantiateにて制作されて、親は、必ず自身のGroupOfTreeForWinningSceneとする（理由は、解放時に、LsystemObjectの解放の手間をなくすため）（Instantiateで確保されたものであれば、解放は必ずされるので、問題ないが、LsystemObjectの親は、GroupOfTreeForWinningSceneであると、確定されたほうが、解放のタイミングが追いやすい））
		//戻値：リストへの木オブジェクトのハンドル（添え字）
	int AddTreeObject(Transform& trans, int polyNum, Tree* pSource = nullptr);


	//木オブジェクトの場所（ワールド座標）を返す
		//詳細：WinningSceneManager専用
		//引数：木へのハンドル
		//戻値：木のワールド座標
	XMVECTOR GetTreeObjectPosition(int num);
	//オブジェクトをランダムで取得する
		//詳細：登録木オブジェクトの中から、ランダムでアクセスする木オブジェクトを木へのハンドル（リストへの添え字）を決める。
		//　　：ハンドルを取得して、GetTreeObjectPosition(int num)の引数へわたし座標を取得
		//引数：なし
		//戻値：木のワールド座標
	XMVECTOR GetRandomTreeObjectPosition();

	//LsystemObjectの初期設定
		//詳細：木オブジェクトとして、生成するときの初期設定もろもろを委託する
		//　　：ポインタのポインタ、つまり、ポインタの参照渡しをして、関数先で、ポインタのインスタンスを生成して、引数に代入してもらう
		//引数：インスタンスの生成していないポインタ（ポインタのアドレスを受け取るため、ポインタのポインタ）
		//戻値：なし
	void InitTreeObject(Tree** pSource);
	//インスタンスのステータス初期化
		//引数：インスタンスの生成していないポインタ（ポインタのアドレスを受け取るため、ポインタのポインタ）
		//戻値：なし
	void InitStatus(Tree** pSource);

	//木オブジェクトのサイズを大きくする
		//引数：拡大する拡大値
		//戻値：なし
	void IncreaseScale(float increaseValue);


};
