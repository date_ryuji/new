#include "GroupOfTreeForWinningScene.h"			//ヘッダ
#include "../../CommonData/GlobalMacro.h"		//共通マクロ
#include "../../Engine/Algorithm/RayCaster.h"	//レイキャスト実行クラス
#include "../../GameScene/Tree/Tree.h"			//木クラス
#include "../Scene/WinningSceneManager.h"		//シーンマネージャー


//コンストラクタ
GroupOfTreeForWinningScene::GroupOfTreeForWinningScene(GameObject* parent)
	: GameObject(parent, "GroupOfTreeForWinningScene"),
	currentNumberOfTrees_(0),
	pTreeSources_(nullptr),
	treeNumCounter_(0) , 	//1オリジン
	myType_(WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_BACK_GROUND_TREE)
{
	//可変長配列の初期化
	pSourceTransforms_.clear();
	pSourceTransforms_.clear();
}

//デストラクタ
GroupOfTreeForWinningScene::~GroupOfTreeForWinningScene()
{
}

//初期化
void GroupOfTreeForWinningScene::Initialize()
{
}

//初期化
	//詳細：任意タイミングで呼び込む初期化
	//　　：WINNINGSCENEシーンにて生成されたタイプを識別して初期化する
void GroupOfTreeForWinningScene::Initialize(WINNING_SCENE_COOP_OBJECTS myType)
{
	//自身のタイプ取得
	myType_ = myType;


	{
		//シーンマネージャーの取得
			//シーンチェンジャーを経由して
				//引数指定のシーンのマネージャークラスを取得
		WinningSceneManager* pSceneManager_ = (WinningSceneManager*)GetCurrentSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_WINNING);

		//自身をセットする
		pSceneManager_->AddCoopObject((int)myType_, this);
	}

	//LSystemの木オブジェクト1つを取得
	CreateATree();
}

//更新
void GroupOfTreeForWinningScene::Update()
{

		//木のLSYSTEM（枝生成）を実行
		ExecutionLSystem();

		//木のサイズの動的可変
		IncreaseScale(0.001f);
	
}

//描画
void GroupOfTreeForWinningScene::Draw()
{
	//Transformの可変長配列に登録されている分だけ
		//可変長配列に登録されているオブジェクトのポインタでDrawを呼びこむ
		//ここにおけるDrawとは、GameObjectにおけるDrawではなく、Lsystemのポインタが持っている、専用のDraw関数である。
		//描画
		//可変長配列に登録されている、Transformで、描画位置をセットして、
		//任意の座標に描画させる
	//初期値：イテレータbegin()
	//条件　：イテレータの最後の要素ではないとき
	//増減　：イテレータ＋＋
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end(); itr++)
	{
		//イテレータにて示される
		//構造体のオブジェクトポインタを参照
			//引数に、同構造体のTransformを送り、Transformの位置にDrawさせる
		(*itr)->pTreeSource->DrawBranchesMakeTree(*((*itr)->pSourceTransform));
	}

}

//解放
void GroupOfTreeForWinningScene::Release()
{
	//動的確保の全要素の解放
	//初期値：イテレータbegin()
	//条件　：イテレータの最後の要素ではないとき
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end();)
	{
		//動的確保したTransformの解放
		SAFE_DELETE((*itr)->pSourceTransform);


		{
			//対象のオブジェクトポインタは解放をする必要がない
				//なぜならば、オブジェクトのポインタはGameObjectクラスを継承していて、
				//なおかつ、GroupOfTreeForWinningSceneの子供として生成しているので、（仮に、他のオブジェクトが親だとしても、ここで解放してはいけない（なぜならば、その人が解放するときに、すでに存在しないところを解放しようとするから））
				//GroupOfTreeForWinningSceneが解放されたら、解放されるようになる。
				//なので、ポインタの解放はせずに、ここでは、ポインタを使えないように、nullptrに変えるだけ
			(*itr)->pTreeSource = nullptr;
		}


		//構造体自体の解放
		SAFE_DELETE((*itr));

		//イテレータから
		//自身の解放
			//解放して、
			//次の要素のイテレータを返してもらう
			//イテレータをfor文にて増減していない理由は以下にある
		itr = pSourceTransforms_.erase(itr);

	}

	//リストを空に
	pSourceTransforms_.clear();

}

//初期の木オブジェクトのインスタンス作成
void GroupOfTreeForWinningScene::CreateATree()
{
	//インスタンスの生成
	//条件：ポインタが空の場合
	if (pTreeSources_ == nullptr)
	{
		//pTreeSources_ = (Tree*)Instantiate<Tree>(this);

		//引数に渡したポインタに
			//生成したインスタンスを返してもらう、かつ、
			//ポインタの参照渡し　そして、ポインタのポインタで受け取り
		//LsystemObjectの初期化を行う
			//Transform値の調整などなど
			//描画のための設定を行ってもらう
		InitTreeObject(&pTreeSources_);
	}

}

//木群へ木オブジェクト追加
int GroupOfTreeForWinningScene::AddTreeObject(Transform& trans, int polyNum, Tree* pSource)
{
	//引数Transformを
	//可変配列に追加

	//あくまで追加するといっても、
		//引数の値をそのまま代入するのではなく、
		//新しく動的確保した領域に、コピーさせる

	//サイズの取得
	size_t size = (sizeof(trans));

	//リストに登録する要素の動的確保
	Transform* transform = new Transform;


	//Transformの初期値を変更



	//コピー
	//引数： to
	//引数： original
	//引数： size
	memcpy(transform, &trans, size);


	//可変配列に登録する
	//構造体の確保
	DrawTreeInfo* pData = new DrawTreeInfo;
	//Transform	//ポインタのコピー
	pData->pSourceTransform = transform;
	pData->pSourceTransform->Calclation();

	//ポリゴン番号の更新
	pData->polyNum = polyNum;


	//条件：引数のポインタがからの場合
	if (pSource == nullptr)
	{
		//引数のpSourceがnullptrの場合
			//自クラスのInitializeにて確保した
			//Lsystemのクラスインスタンス（メンバポインタ）、オブジェクトを登録する
		pData->pTreeSource = pTreeSources_;

	}
	else
	{
		//引数にて、新しいオブジェクトが指定されたとき
		//そのポインタを登録
		pData->pTreeSource = pSource;


		//オブジェクトのTransofmrを描画位置にセットする
		pData->pTreeSource->transform_.position_ = pData->pSourceTransform->position_;



		//クラス内で確保しているオブジェクト以外の	
					//新たに確保されたオブジェクトだったら
				//木オブジェクトのソースに
				//新たな描画先が追加されたということで、
				//コライダーの追加を行う
				//実際、pTreeSource事態は移動せずに、Transformを移動して、描画を行っている、
				//そのため、描画先々に、新たなコライダーを必要とする
		pData->pTreeSource->AddNewCollider(*(pData->pSourceTransform));


		


	}
	//木のオブジェクト個数をカウントアップ
	currentNumberOfTrees_++;






	//コピー先の動的確保した要素を、可変配列にコピー
	pSourceTransforms_.push_back(pData);


	//ハンドル番号を返す
	return (int)pSourceTransforms_.size() - 1;

}

//指定木オブジェクトの座標取得
XMVECTOR GroupOfTreeForWinningScene::GetTreeObjectPosition(int num)
{
	//リストの初期値をイテレータにて取得
	auto itr = pSourceTransforms_.begin();

	//カウンター
	int i = 0;
	//全要素から引数番号番目に格納されているオブジェクトを探す
	//条件：イテレータが最後の要素ではない間
	while (itr != pSourceTransforms_.end())
	{
		//条件：イテレータ番目のカウンターと引数番号が同じ場合
		if (i == num)
		{
			//イテレータ番目にて表されるイテレータから座標を取得
			return (*itr)->pSourceTransform->position_;
		}

		//イテレータとカウンターを回す
		itr++;
		i++;

	}

}

//木オブジェクトの中から参照する木オブジェクトをランダムで選び、そのオブジェクトの座標取得
XMVECTOR GroupOfTreeForWinningScene::GetRandomTreeObjectPosition()
{
	//現在の個数をもとに、
		//参照する木オブジェクトを選択する
	int random = rand() % currentNumberOfTrees_;

	//乱数を渡し、オブジェクトを取得する
	return GetTreeObjectPosition(random);
}

//木オブジェクトの初期化
	//制限：引数ポインタのポインタは、nullptrのポインタであるため、インスタンスを生成する所から行う
void GroupOfTreeForWinningScene::InitTreeObject(Tree** pSource)
{

	//インスタンスの生成
		//引数にて渡された、ポインタのポインタ（つまり、ポインタのアドレス）によって、
		//関数呼び出し元の宣言したポインタに要素を入れる
		//つまり、ポインタの参照渡しの実装を行っている
	(*pSource) = (Tree*)Instantiate<Tree>(this);



	//初期化呼び込み
	//引数：通常の木
	(*pSource)->Initialize(TREE_TYPES::TREE_TYPE_DEFAULT, true);
	//木オブジェクトの初期スケール値セット
	(*pSource)->transform_.scale_ = XMVectorSet(0.5f, 0.5f, 0.5f, 0.0f);

	//枝を生成
		//制限：この枝生成を呼びこまないと、最初の枝ができないので注意
		//　　：後のLSYSTEMを実行するための初期処理である。
	(*pSource)->CreateTheFirstOne();

	//木のステータスの選定
	InitStatus(pSource);

	//リストに追加
		//制限：木オブジェクト群へ登録
		//　　：描画対象の木オブジェクト群ではなく、木のポインタのみを集めたリスト群。
	pListTreeSources_.push_back((*pSource));

}

//木オブジェクトのステータス初期化
void GroupOfTreeForWinningScene::InitStatus(Tree** pSource)
{
	//番号のカウントアップ
		//初期値：０
		//オリジン：０
	treeNumCounter_++;

	//定数値　HP
	static constexpr int HP = 100;
	//木のステータスの選定
	//引数：幹HP
	//引数：根HP
	//引数：木の番号（木の合計数　ー　１（0オリジンにするためー１））
	(*pSource)->InitTreeStatus(HP, HP, treeNumCounter_ - 1);

}

//全木オブジェクトのスケールを拡大
void GroupOfTreeForWinningScene::IncreaseScale(float increaseValue)
{
	//全描画対象の木オブジェクトを参照し、引数値分スケールを拡大
	//初期値：イテレータ初期値
	//条件　：イテレータが最後の要素ではない
	//増減　：イテレータ＋＋
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end(); itr++)
	{
		//LSYSTEMの実行
			//ある程度の大きさの木になったとき
			//LSYSTEMを実行させる
			//ここにおける木の大きさは、ｘｙｚすべてが、同じ値であることが前提である

		//条件：登録Transformがデフォルトサイズを超えない間
		if ((*itr)->pSourceTransform->scale_.vecX < DEFAULT_SCALE_.vecX)
		{
			//拡大
			//引数の値分　スケール値を足す
			(*itr)->pSourceTransform->scale_ += XMVectorSet(increaseValue, increaseValue, increaseValue, 0.0f);
		}


	}


}

//LSYSTEMを実行できるか
	//実行できる場合、実行
void GroupOfTreeForWinningScene::ExecutionLSystem()
{

	//全描画対象の木オブジェクトを参照し、LSYSTEM実行可能オブジェクトへのLSYSTEM実行
	//初期値：イテレータ初期値
	//条件　：イテレータが最後の要素ではない
	//増減　：イテレータ＋＋
		//�@LSYSTEMが実行可能かどうかを調べる
		//�A実行可能の時に、一定上の大きさを超えたら、
		//�BLSYSTEM実行を呼び込み
		//※InitTreeObjectにて、初期サイズを決定(Treeオブジェクト自体のサイズ)
	for (auto itr = pSourceTransforms_.begin(); itr != pSourceTransforms_.end(); itr++)
	{
		//�@
		if ((*itr)->pTreeSource->ExecutableLSystem())
		{
			//実際にLSYSTEM実行するかのフラグ
			bool isLSystem = false;

			//�A
			switch ((*itr)->pTreeSource->GetLSystemCounter())
			{
			case 0:
			{
				if ((*itr)->pSourceTransform->scale_.vecX > 0.3f)
				{
					isLSystem = true;

				}
			}break;

			case 1:
			{
				if ((*itr)->pSourceTransform->scale_.vecX > 0.6f)
				{
					isLSystem = true;

				}
			}break;
			

			default:
				break;
			}


			//�B
			if (isLSystem)
			{
				//実行
				(*itr)->pTreeSource->ExecutionLsystem();

			}


		}


	}


}

//描画対象木オブジェクトを格納する構造体
//コンストラクタ
GroupOfTreeForWinningScene::DrawTreeInfo::DrawTreeInfo() :
	pSourceTransform(nullptr),
	pTreeSource(nullptr),
	polyNum(-1)
{
}
