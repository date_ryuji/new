#include "WinningDirectingModel.h"					//ヘッダ
#include "../../Engine/Model/Model.h"				//ポリゴン描画クラス
#include "../../GameScene/CountTimer/CountTimer.h"	//時間計測タイマークラス
#include "../../Shader/FbxEraseMotionShader.h"		//消去モーションシェーダー


//コンストラクタ
WinningDirectingModel::WinningDirectingModel(GameObject* parent)
	: GameObject(parent, "WinningDirectingModel"),
	hModel_(-1), isEraseMotion_(false),
	eraseSpeed_(1.0f),

	pCountTimer_(nullptr),
	polyNum_(-1)
{
}

//初期化
void WinningDirectingModel::Initialize()
{
}
//モデルのロード
void WinningDirectingModel::LoadModel(const std::string& FILE_NAME)
{
	//ロード（通常のFBX描画シェーダーにてロード）
	hModel_ = Model::Load(FILE_NAME, POLYGON_GROUP_TYPE::FBX_POLYGON_GROUP, SHADER_TYPE::SHADER_TEST);
	/*
		消去モーションシェーダーは
		シェーダー：HeightMap専用　にて存在する。

		しかし、HeightMapと、FBXでは、ポリゴン表示のためにシェーダーに渡す情報が違う。
		そのため、FBX専用の消去モーションシェーダーを作成する
	
	*/

	//警告
		//条件：ハンドルはー1ではないはずだ
	assert(hModel_ != -1);
}

//更新
void WinningDirectingModel::Update()
{
}

//描画
void WinningDirectingModel::Draw()
{
	//条件：ハンドルが存在するならば
	if (hModel_ != -1)
	{
		//条件：消去モーションを実行している場合
			//シェーダーにて用いるコンスタントバッファ２つ目へ、必要情報を与える
		if (isEraseMotion_)
		{
			//シェーダーへの追加情報を与える
			//経過時間の取得
			float elpasedTime = pCountTimer_->GetElpasedTime();

			//経過時間をシェーダーに渡す
				//コンスタントバッファ2つ目に代入
			FbxEraseMotionShader* pFbxEraseMotion = (FbxEraseMotionShader*)Direct3D::GetShaderClass(SHADER_TYPE::SHADER_FBX_ERASE_MOTION);

			//コンスタントバッファにセット
			FbxEraseMotionShader::CONSTANT_BUFFER_1 cb1;
			cb1.elpasedTime = elpasedTime * eraseSpeed_;

			//セット
			pFbxEraseMotion->SetConstantBuffer1Data(&cb1);
		}


		//描画
		Model::SetTransform(hModel_, transform_);
		Model::Draw(hModel_);
	}

}

//解放
void WinningDirectingModel::Release()
{
}

//自クラス管理のモデルの消去モーション開始
void WinningDirectingModel::StartEraseMotion(float endTime)
{
	//消去モーション実行中のフラグを立てる
	isEraseMotion_ = true;

	/*
	//消去モーション
	//現段階でのシェーダーでは、
		//引数にて渡された終了時間で、きれいに透明度が０になる計算にはなっていないので注意
		//上記の実装を行うのであれば、TestShader.hlslのPS_EraseMotion()の修正が必要
	*/


	/*
		//シェーダーの切替え
		//消去モーションを必要とするシェーダーに変換
			//各々のハンドル番号は、重複することはない。クラスが違う場合、同じハンドル番号になることはない。
			//仮に、同じFbxファイルを使用していたとしても、
				//そのため、シェーダータイプを各々のハンドル番号ごとに持たせているため、同じFbxでも、シェーダーを都度、ハンドル番号ごと管理することが可能である。
	*/
	//シェーダーを切り替える
		//切り替え先：Fbxモデル専用の消去モーションシェーダー
	Model::ChangeShader(hModel_, SHADER_TYPE::SHADER_FBX_ERASE_MOTION);


	//タイマーの確保
	pCountTimer_ = (CountTimer*)Instantiate<CountTimer>(this);

	//タイマーのセット
	pCountTimer_->StartTimerForCountUp(0.f, endTime);





}

//自身がたっているポリゴン番号を受け取る
void WinningDirectingModel::SetStandPolyNum(int polyNum)
{
	polyNum_ = polyNum;
}

//自身がたっているポリゴン番号を渡す
int WinningDirectingModel::GetStandPolyNum()
{
	return polyNum_;
}

//消去モーション用の消去スピード切り替え
void WinningDirectingModel::ChangeEraseSpeed(float eraseSpeed)
{
	eraseSpeed_ = eraseSpeed;
}

