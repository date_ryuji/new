#include "EnemySpawnForWinningScene.h"			//ヘッダ
#include "../Scene/WinningSceneManager.h"		//シーンマネージャー
#include "WinningDirectingModel.h"				//WinningScene専用のオブジェクト　消去モーション実行のためのオブジェクトクラス


//コンストラクタ
EnemySpawnForWinningScene::EnemySpawnForWinningScene(GameObject* parent) :
	Spawn::Spawn(parent, "EnemySpawnForWinningScene"),
	pSceneManager_(nullptr),
	currentObjectNumber_(0),
	currentEraseMotionObjectNumber_(0)
{
}


//デストラクタ
EnemySpawnForWinningScene::~EnemySpawnForWinningScene()
{
	//親クラスメソッドの解放を呼ぶ
	ReleaseAllData();
}

//初期化
void EnemySpawnForWinningScene::Initialize()
{
}

//更新
void EnemySpawnForWinningScene::Update()
{
}

//描画
void EnemySpawnForWinningScene::Draw()
{
}

//解放
void EnemySpawnForWinningScene::Release()
{
}

//インスタンス生成関数
	//レベル：オーバーライド
GameObject* EnemySpawnForWinningScene::AddNewInstance(GameObject* pParent)
{
	return nullptr;
}
//インスタンス追加
	//詳細：他初期設定も含ませる
void EnemySpawnForWinningScene::AddInstance(GameObject* pNewInstance)
{
	//リストへ追加
	AddObject(pNewInstance);
}

//インスタンスを返す
	//制限：前回参照されたオブジェクトの次に格納されたオブジェクトを返す
	//　　：前回参照されたオブジェクトへの添え字を保存しておき、その次の要素を取得
GameObject* EnemySpawnForWinningScene::GetInstance()
{
	//オブジェクトの取得
	auto itr = objects_.begin();

	//返り値として渡すオブジェクト
	GameObject* pGameObject = nullptr;

	//カウンター
	int i = 0;

	//スコープ：�@
	//メンバ変数分（前回参照されたオブジェクトへの添え字）イテレータを回す
	//条件：イテレータがリストの最後の要素でない場合
	while (itr != objects_.end())
	{
		//スコープ：�A
		//現在メンバ変数にて示される番号分、イテレータを回して、
			//該当のオブジェクトを取得
		//条件：カウンターが前回取得したオブジェクトへの添え字（番号）と同様ならば
		if (i == currentObjectNumber_)
		{
			//返り値のポインタへオブジェクトを格納
			pGameObject = (*itr);
			//処理終了
				//スコープ：�@を抜ける
			break;
		}

		//引数添え字番号と同様でなかった場合、
			//カウンターと、イテレータを回す
		i++;
		itr++;

	}

	//(前回参照されたオブジェクトへの添え字)
	//メンバ変数の
		//現在参照しているオブジェクトの番号、ハンドルをカウントアップ
	currentObjectNumber_++;

	//オブジェクトを返す
	return pGameObject;

}

//リスト要素の消去モーション開始
void EnemySpawnForWinningScene::StartEraseMotion(float endTime)
{
	//シェーダー切り替えのメソッドを持っている型へ
	//キャストするためのポインタ
	WinningDirectingModel* pModel = nullptr;


	//カウンター
	int i = 0;

	//初期オブジェクトの取得
	auto itr = objects_.begin();
	//スコープ：�@
	//メンバ変数分（前回参照されたオブジェクトへの添え字）イテレータを回す
	//条件：イテレータがリストの最後の要素でない場合
	while (itr != objects_.end())
	{
		//スコープ：�A
		//現在メンバ変数にて示される番号分、イテレータを回して、
			//該当のオブジェクトを取得
		//条件：カウンターが前回消去モーションしたオブジェクトへの添え字（番号）と同様ならば
		if (i == currentEraseMotionObjectNumber_)
		{
			//キャストして返す
			pModel = (WinningDirectingModel*)(*itr);
			//処理の終了
				//スコープ：�@を抜ける
			break;
		}

		//引数添え字番号と同様でなかった場合、
			//カウンターと、イテレータを回す
		i++;
		itr++;

	}
	//カウントアップ
	currentEraseMotionObjectNumber_++;


	//条件：参照したオブジェクトが見つかったなら
	if (pModel != nullptr)
	{
		//シェーダー切り替え
		//消去モーション実行呼び込み
		pModel->StartEraseMotion(endTime);
	}

}
