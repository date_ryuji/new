#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/GameObject/GameObject.h"	//ゲーム内オブジェクト　抽象クラス



//クラスのプロトタイプ宣言
class CountTimer;



/*
	クラス詳細	：特殊機能を持たないクラス　Fbxモデルなどをロードするだけの時に使用するクラス
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用シーン　：WinningSceneScene
	クラス概要（詳しく）
				：Fbxロードを行って、モデルへのハンドルを所有し、モデルへのTransformを管理するクラス
				　WinningSceneにて登場するモデルを出現し、WinningSceneにおける遷移を担うクラス

*/
class WinningDirectingModel : public GameObject
{
//private メンバ変数、ポインタ、配列
private : 

	//モデルハンドル
	int hModel_;

	//自身が存在しているポリゴン番号
		//詳細：地面ポリゴンの上に自身が存在している前提で作成される。
		//　　：その中で地面を作るポリゴンのうち、自身が立っているポリゴン番号。
	int polyNum_;

	//消去モーション実行
	bool isEraseMotion_;
	
	//消去モーションのスピード
	float eraseSpeed_;

	//カウントタイマークラス
	CountTimer* pCountTimer_;


//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	WinningDirectingModel(GameObject* parent);

	//初期化
	//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
	//レベル：オーバーライド
	//引数：なし
	//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public : 

	//モデルのロード
		//詳細：引数ファイルパスからFBXモデルを読み取り、自身内で管理するモデルを用意
		//引数：ファイル名
		//戻値：なし
	void LoadModel(const std::string& FILE_NAME);

	//消去モーション開始
		//引数：終了時間
		//戻値：なし
	void StartEraseMotion(float endTime);

	//自身が立っているポリゴン番号を更新
		//引数：ポリゴン番号
		//戻値：なし
	void SetStandPolyNum(int polyNum);

	//自身が立っているポリゴン番号を取得
		//引数：なし
		//戻値：自身が立っているポリゴン番号（polyNum_）
	int GetStandPolyNum();

	//消去スピードの変更
		//引数：消去スピード
		//戻値：なし
	void ChangeEraseSpeed(float eraseSpeed);



};