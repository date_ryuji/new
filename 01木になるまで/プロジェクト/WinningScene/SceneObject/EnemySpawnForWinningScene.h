#pragma once
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../GameScene/Enemy/Spawn.h"		//スポーンクラス　抽象クラス


//クラスのプロトタイプ宣言
class WinningSceneManager;


/*
	クラス詳細	：敵スポーン（ウィニングシーン限定）
	クラスレベル：サブクラス（スーパークラス（GameObject））
	使用シーン　：WinningScene
	クラス概要（詳しく）
				：敵の出現、出現した敵をまとめて取得しておくクラス

*/
class EnemySpawnForWinningScene : public Spawn
{
//private メンバ変数、ポインタ、配列
private:

	//現在参照した敵の番号（インスタンス取得用）
		//詳細：現在まで参照していた、敵の番号、連番
		//　　：1体目を参照した場合、カウントを１UP。　次に参照する際には、2体目を参照させるようにする
	int currentObjectNumber_;

	//現在参照した敵の番号（消去モーション実行用）
	int currentEraseMotionObjectNumber_;


	//シーンマネージャークラス
	WinningSceneManager* pSceneManager_;

//public ゲームループメソッド
public:

	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	EnemySpawnForWinningScene(GameObject* parent);


	//デストラクタ
		//引数：なし
		//戻値：なし
	~EnemySpawnForWinningScene() override;

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;

//public メソッド
public:


	//敵オブジェクトのインスタンス生成　自管理のリストへ追加
		//引数：親オブジェクト
		//戻値：生成インスタンス
	GameObject* AddNewInstance(GameObject* pParent) override;

	//すでに作成済みのインスタンスの追加
		//引数：生成済みインスタンス（追加するインスタンス）
		//戻値：なし
	void AddInstance(GameObject* pNewInstance);
	
	//現在の参照できる敵（メンバの変数にて示される番号のオブジェクト）を渡し、メンバ変数の番号をカウントアップ
		//引数：なし
		//戻値：スポーンにて管理しているインスタンス数
	GameObject* GetInstance();

	//現在参照している敵オブジェクトの消去モーション実行（シェーダー切り替え）を行う
		//引数：消去モーション終了時間
		//戻値：なし
	void StartEraseMotion(float endTime);




};

