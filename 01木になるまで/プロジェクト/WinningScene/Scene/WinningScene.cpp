#include "WinningScene.h"						//ヘッダ
#include "../../Engine/GameObject/UIGroup.h"	//UIGroup　シーン内UI群
#include "../../Engine/Scene/SceneUIManager.h"	//シーンのUIマネージャー
#include "../../CommonData/GameDatas.h"			//ゲーム内情報定義
#include "../../Engine/DirectX/Camera.h"		//ゲーム内カメラクラス
#include "WinningSceneManager.h"				//マネージャークラス


//コンストラクタ
//SceneParentのコンストラクタ呼び込み
	//引数：親オブジェクト
	//引数：シーン名のオブジェクト名（シーン識別情報群(Project/CommonDatas/SceneType.h)よりシーンオブジェクト名を取得する（シーンをID識別しオブジェクト名取得））
WinningScene::WinningScene(GameObject* parent)
	: SceneParent::SceneParent(parent, SCENE_OBJECT_NAME[(int)SCENE_ID::SCENE_ID_WINNING])
{
}

//初期化
void WinningScene::Initialize()
{
	//シーンの背景色を変更
		//背景色の変化
	//背景色取得
	GameDatas::SetGameBackGroundColor(BACKGROUND_COLOR_LIGHTGREEN);


	//視野距離
	//カメラの視認できる距離を変更する
		//引数：最低距離　１０ｃｍ　から
		//引数：最高距離　２００　ｍ　まで
	Camera::SetCameraFieldOfView(0.1f, 400.f);


	//タイトルシーンクラスのマネージャー
		//詳細：SceneParentを継承しているため、
		//　　：継承元関数から、シーンマネージャークラスを作成させる
		//制限　　：シーンマネージャークラスは、GameObject型継承のオブジェクト作成関数である、Instantiateでの作成は推奨されない。
		//制限理由：制限をかけるのは、シーンマネージャーが作成されたことを伝えなくてはいけないクラスが存在するためである。
	WinningSceneManager* pSceneManager = 
		(WinningSceneManager*)CreateSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_WINNING);

}

//更新
void WinningScene::Update()
{
}

//描画
void WinningScene::Draw()
{
}

//解放
void WinningScene::Release()
{
}
