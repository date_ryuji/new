#include "WinningSceneManager.h"						//ヘッダ
#include "../../Engine/GameObject/UIGroup.h"			//UIGroup群
#include "../../CommonData/GlobalMacro.h"				//共通マクロ
#include "../../Engine/Scene/SceneChanger.h"			//シーン切り替えクラス
#include "../../Engine/Csv/CsvReader.h"					//CSV読み取りクラス
#include "../../Engine/DirectX/Camera.h"				//カメラクラス
#include "../../Engine/Audio/Audio.h"					//音源管理クラス
#include "../../Engine/GameObject/SkyBox.h"				//360度背景
#include "../SceneObject/WinningDirectingModel.h"		//シーン用モデルの共通クラス
#include "../SceneObject/GroupOfTreeForWinningScene.h"	//シーン用木オブジェクト群クラス
#include "../SceneObject/EnemySpawnForWinningScene.h"	//シーン用敵オブジェクト群生成管理スポーンクラス
#include "../../GameScene/CountTimer/CountTimer.h"		//時間計測タイマークラス
#include "../../GameScene/Enemy/Enemy.h"				//敵オブジェクト
#include "../../GameScene/Enemy/EffectEnemy.h"			//敵オブジェクト　エフェクト
#include "../../GameScene/Enemy/EnemyWeapon.h"			//敵オブジェクト　武器
#include "../../GameScene/GrassLand/GrassLandObject.h"	//地面オブジェクト　動的にUVを切り替えるハイトマップ

#include "../../GameScene/Scene/GameSceneManager.h"		//GameSceneにおけるシーンマネージャー（GameSceneと同様のステージづくりを行うため、参考にする）


//コンストラクタ
WinningSceneManager::WinningSceneManager(GameObject* parent) :
	SceneManagerParent::SceneManagerParent(parent, "WinningSceneManager"),
	endScene_(false),
	instanceObjectInfos_(nullptr),
	audioInfos_(nullptr),
	
	pCountTimer_(nullptr),
	pCsvReader_(nullptr),
	currentTagNumber_(1),	//セルの初期値（ｙ）：１

	pTagList_(nullptr) , 
	imageInfos_(nullptr)

{
	//可変長配列の初期化
	listCurrenttargetInfo_.clear();
}

//デストラクタ
WinningSceneManager::~WinningSceneManager()
{
	//解放
	SAFE_DELETE_ARRAY(audioInfos_);
	SAFE_DELETE_ARRAY(instanceObjectInfos_);
	SAFE_DELETE_ARRAY(pTagList_);
	SAFE_DELETE(pCsvReader_);
}

//初期化
void WinningSceneManager::Initialize()
{
	/*シーンマネージャー定義時の必須処理*******************************************************************/
		//詳細：マネージャーとしての機能を持たせるための前提処理、初期化
		//　　：（条件：リソースが存在する場合必須）
		//　　：リソースとは、この場合、連携オブジェクト群における連携オブジェクトが存在するか
		//　　：　　　　　　　　　　　　UIGroup軍におけるUIGroupが存在するか。
		//　　：上記のリソースが一つも存在しない場合に呼び込む必要はない

	//連携オブジェクト群のポインタを保存しておく配列の初期化、生成
	//継承先である、自身のクラスで宣言した、連携オブジェクトを指定するEnum値をもとに、連携オブジェクトの数を引数として渡す
	//関数先にて、引数分の連携オブジェクトを入れる枠、要素を確保してくれる
	NewArrayForCoopObjects((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_MAX);
	//UIオブジェクト群のポインタを保存しておく配列の初期化、生成
	NewArrayForUIGroups((int)WINNING_SCENE_UI_GROUP_TYPE::WINNING_UI_GROUP_MAX);

	/********************************************************************************************************/


	//時間計測タイマークラス（カウントタイマー）の初期化
	{
		//カウントタイマーのインスタンス生成
		pCountTimer_ = (CountTimer*)Instantiate<CountTimer>(this);
		
		//タグ読み込み計算用タイマーの初期化
		//カウントアップを開始する
		//開始時間：０．ｆ
		//終了時間：０．ｆ
			//タイマーを開始するが、開始と同時に計測終了となる
			//最初のUpdateにて、最初のタグが実行されるようにする
			//無駄なカウントアップを行う理由は、タイマーの開始宣言が、タイマーを作成することにつながるため、最初にタイマーを作成させる関数を呼び込む必要がある。（開始関数が、タイマー作成関数になる）
		pCountTimer_->StartTimerForCountUp(0.f, 0.f);


		//上記のタイマーを除いた、タイマーの初期化
		for (int i = 1; i < (int)TIMER_TYPE::MAX_TIMER; i++)
		{
			//タイマー数分繰り返し、
			//	//タイマーの確保
			pCountTimer_->StartTimerForCountUp(0.f, 0.f);
		}

	}

	//CSV読み取りクラスの初期化
	{
		//CsvReaderのインスタンス生成
		pCsvReader_ = new CsvReader;

		//Csvファイルのロード
			//CSV:流れを作るタグ
		pCsvReader_->Load("Assets/Scene/InputCsv/WinningScene/StoryTag/WinningStoryTag.csv");
	}


	//タグによる処理系関数などの初期化
	{
		

		//タグによる処理関数 と　タグ文字列の設定
			/*
			//関数のアドレスを示すために、関数呼び出し時の先頭部分を登録することで、
			//pFunction[enum値]　で関数を呼び出すことが可能
				//→上記の方法でアクセスしようとすると、関数型で指定する必要がありますと　エラーが出てくる
				//→原因は、スコープの関数を登録するために、関数ポインタへスコープを付けているため、　→スコープがついていると、呼び出すことができない。おそらく、
					//→スコープありの関数を呼び出す際は、　(this->*ppp)(1, 1);　this-> という、自身のインスタンスであることを指定して、自身のインスタンスの関数を呼び出す
					//→https://ez-net.jp/article/02/H3TodZlL/TL6u8RBzpYo2/

			//構造体で、
			//タグの文字列と、関数ポインタを登録させる
			*/
		{
			/*
			//タグとタグ読み込み時の実行実行時関数の初期化方法
			
			////シーン切り替え
			//	//関数のアドレス部、実際のアクセスするためのアドレスを渡す（関数を呼び出す際　関数名（引数）と呼び込むが、　関数名は、関数へのアドレスなので、その関数を配列に登録させることで、添え字などのハンドルで呼び出す関数を指定することができる）
			//(pFunction[WINNING_TAG_CHANGE_SCENE]) = &WinningSceneManager::ChangeScene;	
				//→コンパイルエラーを回避できる（スコープでの指定が必要。（宣言時にスコープでの宣言を行ったため））
			*/

			//タグの初期化
			InitTag();

		}
		
		//インスタンスを生成するオブジェクトの名前、ファイル名などの初期化
		InitInstanceObjectString();
		//生成する音楽の初期化
		InitAudio();
		//生成する画像の初期化
		InitImage();

	}
	


}

//タグの初期化
void WinningSceneManager::InitTag()
{
	//タグ情報を入れる配列の動的確保
	pTagList_ = new TagList[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MAX];


	//各タグの登録
	{
		
		//タグの種類（enum）
		//タグの文字列
		//タグ読み込み時の実行時関数

			/*
				//タグや、関数名などを手打ちでソース内に入力すると非効率で
				//間違いが起こる可能性が高いため、
				//Csvファイルや、外部ファイルからタグとタグによる呼び出し関数が指定できると便利だと思う。しかし、その方法は分からない。
			*/



		//WINNING_TAG_CHANGE_SCENE
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_CHANGE_SCENE].tag = "scs";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_CHANGE_SCENE].pFunction = &WinningSceneManager::ChangeScene;
		/*
		//WINNING_TAG_CHANGE_GREEN_POLYGON
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_CHANGE_GREEN_POLYGON].tag = "pgo";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_CHANGE_GREEN_POLYGON].pFunction = &WinningSceneManager::ChangeGreenPolygon;
		*/
		//WINNING_TAG_ALL_CHANGE_GREEN_POLYGON
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ALL_CHANGE_GREEN_POLYGON].tag = "pga";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ALL_CHANGE_GREEN_POLYGON].pFunction = &WinningSceneManager::AllChangeGreenPolygon;
		//WINNING_TAG_ALL_CHANGE_SPECIFIED_TIME
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ALL_CHANGE_SPECIFIED_TIME].tag = "pgt";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ALL_CHANGE_SPECIFIED_TIME].pFunction = &WinningSceneManager::AllChangeGreenPolygonTheSpecifiedTime;
		//WINNING_TAG_CREATE_BACKGROUND_TREE
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_CREATE_BACKGROUND_TREE].tag = "bgt";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_CREATE_BACKGROUND_TREE].pFunction = &WinningSceneManager::CreateBackGroundTree;
		/*
		//WINNING_TAG_EXECUTION_LSYSTEM
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_EXECUTION_LSYSTEM].tag = "lsy";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_EXECUTION_LSYSTEM].pFunction = &WinningSceneManager::ExecutionLsystem;
		*/
		//WINNING_TAG_STAT_PAINT_ANIMATION
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_START_PAINT_ANIMATION].tag = "pgs";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_START_PAINT_ANIMATION].pFunction = &WinningSceneManager::StartPolygonPaintAnimation;
		//WINNING_TAG_SET_RANDOM_POS_ALL_ENEMY
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SET_RANDOM_POS_ALL_ENEMY].tag = "eor";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SET_RANDOM_POS_ALL_ENEMY].pFunction = &WinningSceneManager::SetRandomPosAllEnemy;



		//WINNING_TAG_SET_CAMERA
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SET_CAMERA].tag = "cms";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SET_CAMERA].pFunction = &WinningSceneManager::SetCameraPosition;
		//WINNING_TAG_MOVE_CAMERA
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MOVE_CAMERA].tag = "cmm";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MOVE_CAMERA].pFunction = &WinningSceneManager::MoveCameraPosition;
		//WINNING_TAG_MOVE_CAMERA_ROTATE_Y
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MOVE_CAMERA_ROTATE_Y].tag = "cry";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MOVE_CAMERA_ROTATE_Y].pFunction = &WinningSceneManager::MoveCameraRotateY;
		//WINNING_TAG_MOVE_CAMERA_ROTATE_X
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MOVE_CAMERA_ROTATE_X].tag = "crx";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MOVE_CAMERA_ROTATE_X].pFunction = &WinningSceneManager::MoveCameraRotateX;
		/*
		//WINNING_TAG_ROTATE_CAMERA
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ROTATE_CAMERA].tag = "crp";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ROTATE_CAMERA].pFunction = &WinningSceneManager::RotateCameraPosition;
		*/
		//WINNING_TAG_SET_CAMERA_OF_THE_OBJ
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SET_CAMERA_OF_THE_OBJ].tag = "cmo";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SET_CAMERA_OF_THE_OBJ].pFunction = &WinningSceneManager::SetCameraPosOfTheObject;
		//WINNING_TAG_SET_CAMERA_OF_THE_FPS
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SET_CAMERA_OF_THE_FPS].tag = "fps";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SET_CAMERA_OF_THE_FPS].pFunction = &WinningSceneManager::SetCameraPosOfThePlayer;
		//WINNING_TAG_MOVE_TO_SCAN_GROUND_POLYGON
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MOVE_TO_SCAN_GROUND_POLYGON].tag = "grm";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MOVE_TO_SCAN_GROUND_POLYGON].pFunction = &WinningSceneManager::MoveToScanGroundPolygon;
		//WINNING_TAG_MOVE_CAMERA_ROTATE_OF_THE_OBJ
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MOVE_CAMERA_ROTATE_OF_THE_OBJ].tag = "crg";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_MOVE_CAMERA_ROTATE_OF_THE_OBJ].pFunction = &WinningSceneManager::MoveCameraRotateYOfTheObject;



		//WINNING_TAG_CREATE_INSTANCE
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_CREATE_INSTANCE].tag = "ins";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_CREATE_INSTANCE].pFunction = &WinningSceneManager::CreateInstance;
		/*
		//WINNING_TAG_ENTER_OBJECT
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ENTER_OBJECT].tag = "obe";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ENTER_OBJECT].pFunction = &WinningSceneManager::EnterObject;
		//WINNING_TAG_LEAVE_OBJECT
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_LEAVE_OBJECT].tag = "obl";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_LEAVE_OBJECT].pFunction = &WinningSceneManager::LeaveObject;
		*/
		/*
		//WINNING_TAG_SET_TRANSFORM_OBJECT
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SET_TRANSFORM_OBJECT].tag = "obs";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SET_TRANSFORM_OBJECT].pFunction = &WinningSceneManager::SetTransformObject;
		//WINNING_TAG_DELETE_OBJECT
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_DELETE_OBJECT].tag = "obd";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_DELETE_OBJECT].pFunction = &WinningSceneManager::DeleteObject;
		*/

		//WINNING_TAG_LOAD_IMAGE
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_LOAD_IMAGE].tag = "iml";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_LOAD_IMAGE].pFunction = &WinningSceneManager::LoadImageForWinnning;
		//WINNING_TAG_VISIBLE_IMAGE
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_VISIBLE_IMAGE].tag = "imv";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_VISIBLE_IMAGE].pFunction = &WinningSceneManager::VisibleImage;
		//WINNING_TAG_INVISIBLE_IMAGE
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_INVISIBLE_IMAGE].tag = "imi";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_INVISIBLE_IMAGE].pFunction = &WinningSceneManager::InvisibleImage;
		//WINNING_TAG_ADDITION_IMAGE_ALPHA
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ADDITION_IMAGE_ALPHA].tag = "ima";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ADDITION_IMAGE_ALPHA].pFunction = &WinningSceneManager::AddOrSubOfImageAlpha;
		//WINNING_TAG_SUBTRACTION_IMAGE_ALPHA
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SUBTRACTION_IMAGE_ALPHA].tag = "ims";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_SUBTRACTION_IMAGE_ALPHA].pFunction = &WinningSceneManager::AddOrSubOfImageAlpha;


		//WINNING_TAG_LOAD_AUDIO
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_LOAD_AUDIO].tag = "adl";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_LOAD_AUDIO].pFunction = &WinningSceneManager::LoadAudio;
		//WINNING_TAG_PLAY_AUDIO
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_PLAY_AUDIO].tag = "adp";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_PLAY_AUDIO].pFunction = &WinningSceneManager::PlayAudio;
		//WINNING_TAG_STOP_AUDIO
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_STOP_AUDIO].tag = "ads";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_STOP_AUDIO].pFunction = &WinningSceneManager::StopAudio;
		//WINNING_TAG_LOOP_AUDIO
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_LOOP_AUDIO].tag = "ada";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_LOOP_AUDIO].pFunction = &WinningSceneManager::LoopAudio;
		//WINNING_TAG_PAINT_POLYGON_ANIMATION
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_PAINT_POLYGON_ANIMATION].tag = "poa";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_PAINT_POLYGON_ANIMATION].pFunction = &WinningSceneManager::StartPolygonPaintAnimation;
		//WINNING_TAG_START_ERASE_MOTION_ENEMY
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_START_ERASE_MOTION_ENEMY].tag = "sem";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_START_ERASE_MOTION_ENEMY].pFunction = &WinningSceneManager::StartEnemyEraseMotion;
		//WINNING_TAG_ADD_ENEMY_EFFECT
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ADD_ENEMY_EFFECT].tag = "aee";
		pTagList_[(int)WINNING_ANIMATION_TAG::WINNING_TAG_ADD_ENEMY_EFFECT].pFunction = &WinningSceneManager::AddEnemyEffect;

	}

}

//インスタンスを生成するオブジェクトの名前、ファイル名などの初期化
void WinningSceneManager::InitInstanceObjectString()
{
	instanceObjectInfos_ = new SceneInstanceObjectInfo[MAX_MODELS]
	{
		SceneInstanceObjectInfo("Enemy" , "Assets/Scene/Model/GameScene/Enemy/Ghost.fbx"),
		SceneInstanceObjectInfo("Ground" , "NULL"),
		SceneInstanceObjectInfo("SkyBox" , "NULL"),
		SceneInstanceObjectInfo("BackGroundTree" , "NULL"),
		SceneInstanceObjectInfo("Player" , "Assets/Scene/Model/WinningScene/PlayerGhost.fbx"),
		SceneInstanceObjectInfo("PlayerOwnedTree" , "NULL"),
		SceneInstanceObjectInfo("Tree" , "NULL"),
		SceneInstanceObjectInfo("EnemySpawn" , "NULL"),

	};


}

//生成する音楽の初期化
void WinningSceneManager::InitAudio()
{
	audioInfos_ = new SceneAudioInfo[MAX_AUDIOS]
	{
		SceneAudioInfo("Main" , "Assets/Scene/Sound/WinningScene/光芒の大地.wav"),
		SceneAudioInfo("GameScene" , "Assets/Scene/Sound/GameScene/木漏れ日.wav"),
		SceneAudioInfo("PlayerDelete" , "Assets/Scene/Sound/WinningScene/夕暮れの風.wav"),

	};
}

//生成する画像の初期化
void WinningSceneManager::InitImage()
{

	imageInfos_ = new SceneImageInfo[MAX_IMAGES]
	{
		SceneImageInfo("White" , "Assets/Scene/Image/WinningScene/WhiteImage.png"),
		SceneImageInfo("Clear" , "Assets/Scene/Image/WinningScene/GameClear.png"),

	};

}
//更新
void WinningSceneManager::Update()
{

	//タグの更新処理
	UpdateTags();

	//タイマーの更新処理
	UpdateTimers();

}

//タイマーの更新処理
void WinningSceneManager::UpdateTimers()
{
	//時間による、現在のタイマーによる行動
	//リストに登録された、タイマーによる行動を各々実行する
	for (auto itr = listCurrenttargetInfo_.begin(); itr != listCurrenttargetInfo_.end();)
	{
		//タイマーの行動関数
		bool(WinningSceneManager:: * funcs[(int)TIMER_TYPE::MAX_TIMER])(CurrentTargetInfo&) =
		{
			nullptr,
			&WinningSceneManager::TimerAlgorithmMoveCameraPostion,
			&WinningSceneManager::TimerAlgorithmMoveCameraRotate,
			//&WinningSceneManager::TimerAlgorithmMoveCameraTarget,
			&WinningSceneManager::TimerAlgorithmTransformImageAlpha,
			&WinningSceneManager::TimerAlgorithmPolygonPaintAnimation,
			&WinningSceneManager::TimerAlgorithmPaintedAllPolygon,


		};


		//参照渡しの引数で　
		//消去するというフラグが立ったら、リストの要素を削除する→登録されていた要素の行動（タイマーによる行動）は終了する
		//行動実行
		//メンバ変数に、現在のタイマーによる行動をEnum値で保存しているので、その行動を実行
		bool isKill = (this->*funcs[(int)((*itr).timerType)])((*itr));

		//上記のフラグに、
		//関数内にて、消去のフラグが立ったらTrueを返す
		//行動中である場合は、False

		if (isKill)
		{

			//削除前に、
			//構造体専用のカウントタイマークラスの削除
			//ゲームオブジェクトなので、KillMeにて削除
			(*itr).pCountTimer->KillMe();

			//イテレータをリストから削除
			itr = listCurrenttargetInfo_.erase(itr);
		}
		else
		{
			//削除をしない場合、イテレータを回す
			itr++;
		}

	}
}

//タグの更新処理
void WinningSceneManager::UpdateTags()
{
	//計測時間を過ぎている場合繰り返し
	//タグを読み込んだ際の、次のタグまでの実行時間を計測時間として、その計測時間を過ぎた場合、タグを実行し続ける

	//新しいタグの読み取りと処理実行
	//条件：シーンを終了していない
	//&&
	//　　：計測時間が計測終了を超えているか
	while (!endScene_ && pCountTimer_->EndOfTimerForCountUp((int)TIMER_TYPE::TAG_TIMER))
	{
		//タグから実行する処理を判別し、実行
		//x : 固定値０（メインタグ）（※固定値１：メインタグ２）
		//y : 現在の示されるセル値（0オリジン）
		ExecutionFunction((int)TAGS::MAIN_TAG, currentTagNumber_);




		//次のタグを読み込むまでの時間を取得
		//x : 固定値２（サブタグ（次のタグ読み込みまでの時間））
		//y : 現在の示されるセル値（0オリジン）
		float endTime = pCsvReader_->GetValueFloat((int)TAGS::SUB_TAG, currentTagNumber_);

		//上記で取得した時間を、カウントクラスの終了時間として更新
		//再計測開始
		//ハンドル：０
		//開始時間：0.f
		//終了時間：タグから読み込んだ次のタグまでの時間
		pCountTimer_->ReStartTimerForCountUp((int)TIMER_TYPE::TAG_TIMER, 0.f, endTime);


		//タグのセル値をカウントアップ
		currentTagNumber_++;

	}
}

//描画
void WinningSceneManager::Draw()
{
}

//解放
void WinningSceneManager::Release()
{
	//シーン内連携オブジェクトの解放
		//解放といっても、オブジェクトはGameObject型であるため、Deleteはしない。マネージャークラス内にて管理しているオブジェクトを登録する配列ポインタの解放
	DeleteArrayForCoopObjects();
	//UIオブジェクト群の解放
	DeleteArrayForUIGroups();
}
//UIGroup群の個数返す
int WinningSceneManager::GetUIGroupCount()
{
	return (int)WINNING_SCENE_UI_GROUP_TYPE::WINNING_UI_GROUP_MAX;
}

//タイマーへ情報追加
void WinningSceneManager::PushBackTimerInfo(TIMER_TYPE timerType, float endTime, XMVECTOR startPos, XMVECTOR targetPos, int num)
{
	//※現段階では、タイマーは共通して使うことができない
		//タイマーの行動による、それぞれのタイマーを用意することができない
		//そのため、今から初期化するタイマーを使用しているタイマーの行動は、削除する

		//そして、新しい行動をセット、タイマーもリセットする
	//※あるいは、タイマーをそれぞれ構造体の要素にそれぞれ持たせて、
		//構造体ごとに、タイマーを使用する？
		//→これであれば、別々に行動、計測が可能になる。



	//タイマーのリセット
	//上記で取得した時間を、カウントクラスの終了時間として更新
	//再計測開始
	//ハンドル：
	//開始時間：0.f
	//終了時間：タグから読み込んだ次のタグまでの時間
	//pCountTimer_->ReStartTimerForCountUp(timerType, 0.f, endTime);



	//新しい構造体要素の宣言
	CurrentTargetInfo currentTarget;
	currentTarget.timerType = timerType;
	currentTarget.num = num;
	currentTarget.startPos = startPos;
	currentTarget.targetPos = targetPos;

	//カウントタイマーの生成
	currentTarget.pCountTimer = (CountTimer*)Instantiate<CountTimer>(this);
	//カウントタイマーの初期化
	currentTarget.pCountTimer->StartTimerForCountUp(0.f, endTime);




	//要素をリストに追加
	listCurrenttargetInfo_.push_back(currentTarget);


}

/**タイマーによる行動関数
		 タイマーによる動的処理の実行関数******************************************************************************/

//時間計測による動的な　カメラの移動
bool  WinningSceneManager::TimerAlgorithmMoveCameraPostion(CurrentTargetInfo& targetInfo)
{
	//現在の経過時間をもとに、
		//目標地点へカメラを移動

	//開始地点から、終了地点までを一直線に結び（線形補完）
		//現在の経過時間における、地点を求め、
		//その地点へ移動させる

	//自身が所有するカウントタイマーの計測が
	//終了しているか
		//自身の所有しているタイマーは、計測タイマーを一つだけ所有しているので、
		//引数に、タイマーを示す、ハンドル番号は必要ない
	//if (pCountTimer_->EndOfTimerForCountUp(targetInfo.timerType))
	if (targetInfo.pCountTimer->EndOfTimerForCountUp())
	{
		//移動を終了させて、
			//タイマーの設定を初期化
		//InitTimer();

		//自身が削除されたことを伝える
		return true;

	}
	else
	{
		//移動
		if (targetInfo.pCountTimer->GetElpasedTime() == 0.0f)
		{
			//経過時間が０の場合、
				//割り算の計算では、解がinfになってしまうため、処理終了とする
				//その際の終了は、問題なく（自身を消去しない）
			return false;
		}


		//経過時間から、終了時間までの割合を求める
		//終了時間 / 経過時間　＝　現在の移動率割合
		float per = targetInfo.pCountTimer->GetElpasedTime() / targetInfo.pCountTimer->GetEndTimerForCountUp();

		//移動先
		XMVECTOR movePos = XMVectorLerp(targetInfo.startPos, targetInfo.targetPos, per);


		//移動地点を求める
		Camera::SetPosition(movePos + XMVectorSet(0.f, 2.f, 0.f, 0.f));

		//焦点を移動先にする
		//Camera::SetTarget(movePos + (currentTargetInfo_.targetPos - currentTargetInfo_.startPos));
		XMVECTOR vec = (targetInfo.targetPos - targetInfo.startPos);

		Camera::SetTarget(movePos + vec - XMVectorSet(0.f, -2.f, 0.f, 0.f));

	}

	//問題なく行動終了
		//自身を削除しない
	return false;

}
//時間計測による動的な　カメラの回転
bool  WinningSceneManager::TimerAlgorithmMoveCameraRotate(CurrentTargetInfo& targetInfo)
{
	//経過時間が０の場合、終了
	float elpasedTime = targetInfo.pCountTimer->GetElpasedTime();
	if (elpasedTime == 0.f)
	{
		return false;
	}


	//経過時間を取得
		//全体経過時間　ー　前回のフレーム時の時間
	elpasedTime = elpasedTime - targetInfo.targetPos.vecY;

	//前回のフレームの時間の更新
	targetInfo.targetPos.vecY = targetInfo.pCountTimer->GetElpasedTime();

	//経過時間　＊　スピード　にて
		//回転の度合いを求める
		//結果は、度（デグリー）にて求められる
	float moveAngle = elpasedTime * targetInfo.targetPos.vecX;

	/*if (moveAngle == 0.0f)
	{
		return false;
	}*/

	//回転の度合いをもとに、
		//カメラの回転
		//カメラの位置を変えずに、焦点の座標を変換する

	//ベクトルの回転
		//(0,0,1,0)
		//上記の座標をY軸を中心に９０度回転するとしたら、
			//→(1,0,0,0)となる。
			//→つまり、(0,0,0,0)を中心としたときに、その(0,0,0,0)にY軸があって、そこから回転を行っている。
	//上記を踏まえて、
		//�@焦点のベクトルに回転の度合いをラジアンに変更し、
		//�Aそれで回転行列を作る。
		//�B回転行列を焦点座標とかけて、移動後の座標を取得
		//�C移動位置をセット

	XMVECTOR camTarget = Camera::GetTarget();
	//�@�A
	XMMATRIX mat = XMMatrixTranslation(0.f , 0.f, 0.f);
	if (targetInfo.num == -1)
	{
		mat = XMMatrixRotationY(XMConvertToRadians(moveAngle));
	}
	else if(targetInfo.num == 0)
	{
		mat = XMMatrixRotationX(XMConvertToRadians(moveAngle));
	}

	//�B
	camTarget = XMVector3TransformCoord(camTarget, mat);
	//�C
	Camera::SetTarget(camTarget);

	/*
	//うまくいかなかったら、正規化されていないとダメパターン
	//急に移動したりする
		//→現在示している座標から、回転をするのではなくて、
		//→現在示している座標から、一度、０になって、から、回転を開始してしまう。

		//→原因不明

	//→原因解明
		//→タイマーのリセットをそのシーンで行ってしまっているため、
		//→タイマーは共通　＝　現在存在している、終了していない、カメラが（０，０を見るというタイマー行動
			//これが、再び実行されてしまっている。　→そのため、前回までの行動が、再び実行されてしまっているのでは？と考える。）
			//→つまり、前回のタイマー行動のタイマーの初期化が行われる。
			//→タイマー自体は共通なので、　現在行動している別の同じタイマーを使っている行動がやり直しになる。
	*/





		//終了しているか
		if (targetInfo.pCountTimer->EndOfTimerForCountUp())
		{

			//自身が削除されたことを伝える
			return true;

		}



	//問題なく行動終了
		//自身を削除しない
	return false;

}
/*
bool  WinningSceneManager::TimerAlgorithmMoveCameraTarget(CurrentTargetInfo& targetInfo)
{
	//問題なく行動終了
		//自身を削除しない
	return false;

}
*/
//時間計測による動的な　画像のα値設定
bool  WinningSceneManager::TimerAlgorithmTransformImageAlpha(CurrentTargetInfo& targetInfo)
{
	//タイマーによる変動計算方法
	//指定終了時間をかけて、α値を１〜０に変更する

	//�@終了時間を取得（カウントアップ）
	//�A現在の経過時間を取得（カウントアップ）
	//�B�A / �@を行う　（終了時間をもとに、現在経過した割合を求める）
	//�C�Bの結果をα値としてセット




	//�@
	float endTime = targetInfo.pCountTimer->GetEndTimerForCountUp();

	//�A
	float nowTime = targetInfo.pCountTimer->GetElpasedTime();

	//現在時刻が０の場合終了
		//０と割らせない
	if (nowTime == 0.f)
	{
		return false;
	}

	//�B
	float alpha = nowTime / endTime;
	//１〜０の間に切り詰める	//clamp(v, low, high)
		//1: max(alpha , 0.f) = 第一引数が第二引数より大きかったら　第一引数を。それ以外第二引数。
		//2: min(1: , 1.f) = 第一引数が第二引数より小さかったら、第一引数を。それ以外第二引数。
	alpha = min(max(alpha, 0.f), 1.f);


	//targetInfo.startPos.x に入っているｘ値により、
		//０〜１（増算）、１〜０（減算）かを判断。
		// 0.0f（増算）、1.0f（減算）
	//減算の場合
	if (targetInfo.startPos.vecX == 1.f)
	{
		//減算
		//α値を１〜０へ変動させるので、
			//�Bの値を反転させる
		alpha = 1.f - alpha;
	}

	//�C
	//targetInfoに格納しているnumに、α値をセットする画像への添え字、タイプが入っている。
		//そのタイプにて、アクセス
	this->SetAlphaSceneUIGroup(targetInfo.num, alpha);





	//終了しているか
	if (targetInfo.pCountTimer->EndOfTimerForCountUp())
	{
		//移動を終了させて、
			//タイマーの設定を初期化
		//InitTimer();

		//自身が削除されたことを伝える
		return true;
	}



	//問題なく行動終了
		//自身を削除しない
	return false;

}

//時間計測による動的な　ポリゴンを塗る
	//指定ポリゴンに作られたGrassの範囲を拡大
bool  WinningSceneManager::TimerAlgorithmPolygonPaintAnimation(CurrentTargetInfo& targetInfo)
{
	
	//★
	/**************************************************************************************/
	//０，０から
		//ポリゴンを拡大、拡張の関数を呼び込めばよい（既に存在しているので、難しいことはせずに、行えばよさそう）
		//だが、処理数、繰り返し回数が多くなるので、処理が重くなる可能性がある
			//それを避けるために、Grass事態の処理を改善する必要がありそう。
			//ゲーム本編のためにも、改善は必要そう


	//Grassを（０，０）に作成し
		//指定時間を超えたらGrassの拡張を行って、範囲を広げさせれば良い
	//指定時間について
		//地面ポリゴンの横のポリゴンを取得する

		//拡張は、その横ポリゴン回数分行えば、大体全ポリゴンを塗ることができそう
			//そのあと、濡れていないポリゴンは一気に塗ってしまえばよい

		//終了時間 / 横ポリゴン数　で出た値が
		//一回の拡張を行う時間間隔となる

		//その時間を確保しておいて、
		//その時間がたったら、新たに拡張を行う

	//保存
	//StartPaintの時点で
		//endPos の　ｘ　に、　拡張の時間間隔を保存

		//endPos の　ｙ　に、　前回の最終更新時間（拡張を行った時の時間）を保存
			//ｙと現在の時間の差分が　ｘ以上ならば、一回拡張とともに、現在時間をｙに更新

	//上記を、タイマーが終了するまで



	/**************************************************************************************/





	//現在時刻
	float nowTime = targetInfo.pCountTimer->GetElpasedTime();
	//前回更新した時間
	float lastUpdateTime = targetInfo.targetPos.vecY;



	//現在時間 - 前回の更新した時間
	float elpasedTime = nowTime - lastUpdateTime;
	//経過時間加算
	//targetInfo.targetPos.vecZ += elpasedTime;



	//その経過時間が
		//targetPos.xに格納されている値を超えていた場合
		//Grassの成長を宣言する
	if (elpasedTime >= targetInfo.targetPos.vecX)
	{
		//GrassLandObjectの成長宣言を呼び込む（強制的に成長させる）
		GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
		pGrassLand->ForciblyGrowGass();

		//前回の更新した時間を　現在時間に更新
		targetInfo.targetPos.vecY = nowTime;


	}


	//1フレームで拡張できる限界数のカウント
	//int count = 0;


	////経過時間 - 間隔　が０以上の間繰り返す
	//while (targetInfo.targetPos.vecZ - targetInfo.targetPos.vecX >= 0.f) //&&
	//	//count < 200)
	//{
	//	//減算する
	//	targetInfo.targetPos.vecZ -= targetInfo.targetPos.vecX;


	//	//GrassLandObjectの成長宣言を呼び込む（強制的に成長させる）
	//	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject(WINNING_COOP_GRASS_LAND);
	//	pGrassLand->ForciblyGrowGass();



	//	//次に見るポリゴン番号を進める(１オリジン)
	//	//targetInfo.num++;
	//	//１フレームに拡張できる限界数、そのカウント
	//	//count++;



	//	//前回の更新した時間を　現在時間に更新
	//	targetInfo.targetPos.vecY = nowTime;

	//}





	//終了しているか
	if (targetInfo.pCountTimer->EndOfTimerForCountUp())
	{
		//移動を終了させて、
			//タイマーの設定を初期化
		//InitTimer();

		//自身が削除されたことを伝える
		return true;
	}


		
		

	//問題なく行動終了
	//自身を削除しない
	return false;


}

//時間計測による動的な　全てのポリゴンを塗る
bool WinningSceneManager::TimerAlgorithmPaintedAllPolygon(CurrentTargetInfo& targetInfo)
{
	//指定時間で全ポリゴンを塗る
	//終了時間を受け取り、終了時間から、１ポリゴンを見る間隔。その時間を確保する。
		//その時間が経過したら、１ポリゴンの拡張を行う
		//→その際、指定ポリゴンがすでに、どのGrassかに、塗られていたら、拡張は行わない。
		//→誰にも塗られていないポリゴンであれば、拡張を行う


	//現在時刻
	float nowTime = targetInfo.pCountTimer->GetElpasedTime();
	//前回更新した時間
	float lastUpdateTime = targetInfo.targetPos.vecY;
	

	//現在時間 - 前回の更新した時間
	float elpasedTime = nowTime - lastUpdateTime;
	//経過時間加算
	targetInfo.targetPos.vecZ += elpasedTime;


	//1フレームで拡張できる限界数のカウント
	int count = 0;

	//経過時間 - 間隔　が０以上の間繰り返す
	while (targetInfo.targetPos.vecZ - targetInfo.targetPos.vecX >= 0.f &&
		count < 200)
	{
		//減算する
		targetInfo.targetPos.vecZ -= targetInfo.targetPos.vecX;


		//GrassLandObjectの成長宣言を呼び込む（強制的に成長させる）
		GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
		//pGrassLand->ForciblyGrowGass();
		pGrassLand->IfNotPaintedCreateOneGreenPolygon(targetInfo.num);




		//次に見るポリゴン番号を進める(１オリジン)
		targetInfo.num++;
		//１フレームに拡張できる限界数、そのカウント
		count++;



		//前回の更新した時間を　現在時間に更新
		targetInfo.targetPos.vecY = nowTime;
	
	}


	//終了しているか
	if (targetInfo.pCountTimer->EndOfTimerForCountUp())
	{
		//移動を終了させて、
			//タイマーの設定を初期化
		//InitTimer();

		//自身が削除されたことを伝える
		return true;
	}


	////その経過時間が
	//	//targetPos.xに格納されている値を超えていた場合
	//	//Grassの成長を宣言する
	//if (elpasedTime >= targetInfo.targetPos.vecX)
	//{
	//	//GrassLandObjectの成長宣言を呼び込む（強制的に成長させる）
	//	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject(WINNING_COOP_GRASS_LAND);
	//	//pGrassLand->ForciblyGrowGass();
	//	pGrassLand->IfNotPaintedCreateOneGreenPolygon(targetInfo.num);



	//	//次に見るポリゴン番号を進める(１オリジン)
	//	targetInfo.num++;


	//	//前回の更新した時間を　現在時間に更新
	//	targetInfo.targetPos.vecY = nowTime;


	//}








	return false;
}


//void WinningSceneManager::InitTimer()
//{
//	currentTargetInfo_.startPos = XMVectorSet(0.f, 0.f, 0.f, 0.f);
//	currentTargetInfo_.targetPos = XMVectorSet(0.f, 0.f, 0.f, 0.f);
//	currentTargetInfo_.num = -1;
//	currentTargetInfo_.timerType = MAX_TIMER;
//
//}



//タグを読み込み　タグによる行動実行
void WinningSceneManager::ExecutionFunction(int x, int y)
{
	//実行処理関数を判別する
		//引数座標を、CSVのセルとして使用し、タグを読み込む
		//タグから実行される処理関数を判別して、処理関数にアクセスするためのハンドル番号をEnum値で返してもらう
	//取得成功：Enum値（MAX前のEnum値）
	//取得失敗：MAX値
	WINNING_ANIMATION_TAG tag = GetAnimationTag(x, y);

	//条件：実行処理関数へのハンドルがMAXでないならば
	if (tag != WINNING_ANIMATION_TAG::WINNING_TAG_MAX)
	{
		//自身のインスタンスの
			//関数呼び出しを示すために、 this->　を付加
			//* を付加する理由は、関数ポインタを登録する際に、＆を付けて保存したため、pFunction自体は、ポインタになっているので、＆で実際の関数へのアドレスを参照させる
		//(this->*pFunction[tag])(x, y);
		//(this->**pFunction[tag])(x, y);
		//タグの処理関数呼び込み
		(this->*pTagList_[(int)tag].pFunction)(x, y);

	}


}

//引数の座標から示される、タグをから、実行処理関数を識別するEnum値を返す
WINNING_ANIMATION_TAG WinningSceneManager::GetAnimationTag(int x, int y)
{
	//引数の座標を
		//CSVにおけるセル値として使用して、
		//タグを読み込む
	//現在指しているのタグを読み込む
	//x : 固定値０（メインタグ）
	//y : 現在の示されるセル値（0オリジン）
	std::string tag = pCsvReader_->GetString(x, y);


	//CsvReaderにて読み込まれるのは、
		//"\0"が文字末についている。
	//しかし、string型へ代入した文字列には、 "\0"がついていないため、
		//＝＝の審議判断では、\0がないということで、偽になる



	//条件　：タグMAX（enum）
		//タグの文字列群から、
		//メンバに登録しているタグの文字列と比較して、同様のタグを探す
		//その文字列配列の登録されているタグの添え字をWINNING＿ANIMATION_TAG型で返す
	for (int i = 0; i < (int)WINNING_ANIMATION_TAG::WINNING_TAG_MAX; i++)
	{
		//文字末に、'\0'を結合して、タグ+'\0'で文字列の真偽判断を行う
		std::string target = pTagList_[i].tag + '\0';

		//条件：タグ文字列とタグ文字列群から取得した文字列が同じならば
		if (target == tag)
		{
			//タグ(enum)型にキャストして種類を返す
			return (WINNING_ANIMATION_TAG)i;
		}
	}
	//見つけられなかったとして、MAXを返す
	return WINNING_ANIMATION_TAG::WINNING_TAG_MAX;
}


//シーンを切り替える
void WinningSceneManager::ChangeScene(int x, int y)
{
	//シーン終了のフラグ
	endScene_ = true;

	//シーン切り替え
	SceneChanger* pSceneChanger = (SceneChanger*)FindObject("SceneChanger");
	pSceneChanger->ChangeScene(SCENE_ID::SCENE_ID_RESULT);
}

//タグセル値から座標を取得する
XMVECTOR WinningSceneManager::GetVector(int x, int y)
{
	//座標をCsvから読み込む
//x : 固定値１（メインタグ２）
//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);

	//繰り返しの終了条件値
	static constexpr int XYZ = 3;
	//座標を登録する配列
	float saveCoord[XYZ] = 
	{
		0.f ,0.f ,0.f 
	};



	//タグに座標を
		//0.0|0.0|0.0　と記入されている
		// | 間隔で座標をFloat値で取得し、座標系の型に代入する

	//文字列のサイズ分繰り返す

	int current = 0;	//現在の添え字
	int before = 0;		//前回の最終添え字
	int i = 0;			//格納先配列の添え字
	int length = (int)mainTag2.length();	//文字列の長さ

	//条件：現在参照中の文字数が文字の長さを超えない間
	while (current < length)
	{
		//タグから文字（1文字）を取得
		char c = mainTag2[current];

		//区切り文字を定義
		char delimiter = '|';

		//条件：現在参照している文字（１文字）が区切り文字であるかを調べる
		if (mainTag2[current] == delimiter)
		{
			//文字列を座標のfloat型に変換


			//前回の最終添え字から、現在の添え字までの間の文字を取得
				//substr
			//引数：読み込み添え字
			//引数：第一引数から、読み込む文字数
			std::string strCoord = mainTag2.substr(before, (unsigned long long)((current - before)));

			//文字列を数値に変換
			float coord = (float)atof(strCoord.c_str());

			//格納先配列へ格納
			saveCoord[i] = coord;

			//格納先配列への添え字のカウントアップ
			i++;

			//前回の区切り文字にて取得した際の最終添え字
				//最終添え字は、 区切り文字の　|の次の文字から対象とする
			before = current + 1;
		}

		//カウントアップ
		current++;
	}

	//座標をベクトル型にして返す
	return XMVectorSet(saveCoord[0], saveCoord[1], saveCoord[2] , 0.f);


}

//指定ポリゴンを緑ポリゴンへ
void WinningSceneManager::ChangeGreenPolygon(int polyNum)
{
	//GrassLandを取得
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
	//指定ポリゴンを緑ポリゴンにしてもらう
	pGrassLand->CreateOneGreenPolygon(polyNum);


}
/*
void WinningSceneManager::ChangeGreenPolygon(int x, int y)
{
	//指定セルから
		//緑ポリゴンを作成、切り替えるポリゴン番号を取得する
}
*/

//全ポリゴンを緑ポリゴンへ
void WinningSceneManager::AllChangeGreenPolygon(int x, int y)
{
	//GrassLandの取得
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);

	//全ポリゴンを拡張、塗る関数呼び込み
		//GrassLandに登録している複数のGrass（草）クラスの中から、
		//一番最初の要素を取得して、
		//その要素をもとに、拡張されていないポリゴンを拡張させる（全ポリゴンを、自身の所有ポリゴンとする（ポリゴンを確保））
		//拡張させるにあたって、全ポリゴンを参照することになるので、あらかじめ、処理時間がかかることを頭に入れておく。そのうえで拡張させる
	pGrassLand->CreateAllGreenPolygon();

}

//指定時間で全ポリゴンを緑ポリゴンへ
void WinningSceneManager::AllChangeGreenPolygonTheSpecifiedTime(int x, int y)
{
	//全ポリゴンを指定時間かけて、緑ポリゴンへ変化させる

	//指定時間
	//メインタグ２
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	float endTime = pCsvReader_->GetValueFloat((int)TAGS::MAIN_TAG_2, y);


	//タイマー終了時間
	//＝指定時間


	XMVECTOR targetPos = XMVectorSet(0.f, 0.f, 0.f, 0.f);
	//targetPos.vecX
	//１ポリゴンを　緑ポリゴンへ変化させる時間間隔
	//指定時間 / 全ポリゴン数　にて、１つのポリゴンを緑ポリゴンへ変化させる処理を行う、間隔の時間を取得。
		//経過時間を計測し、上記の間隔を過ぎたら、１つのポリゴンを緑ポリゴンへ変化
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
	int polyCount = pGrassLand->GetPolygonCount();
	targetPos.vecX = endTime / (float)polyCount;


	//targetPos.vecY
	//前回更新した時の時間
	//targetPos.vecY = 0.0f;

	//num
	//次に参照するポリゴン番号
	int num = 1;


	//タイマーによる　動的処理の開始宣言
	//必要要素を引き渡し
	//構造体にタイマーによる移動などの必要情報をセットする
	PushBackTimerInfo(
		TIMER_TYPE::PAINTED_ALL_POLYGON_TIMER, endTime, XMVectorSet(0.f, 0.f, 0.f, 0.f), targetPos , num);

}

//カメラ位置のセット
void WinningSceneManager::SetCameraPosition(int x, int y)
{
	//座標を取得
	XMVECTOR vec = GetVector(x,y);

	//座標をセット
	Camera::SetPosition(vec);
}

//ポリゴン番号のワールド座標を取得する
bool WinningSceneManager::GetWorldPosOfPolygonMakeGround(XMVECTOR* localToWorld, int polyNum)
{
	//ポリゴン番号から、ポリゴンの中心のローカル座標を取得
	//条件：ポリゴン番号を取得できていたら
	if (GetLocalPosOfPolygonMakeGround(localToWorld, polyNum))
	{
		//ポリゴン番号が存在し、
			//ローカル座標を受け取れたら
			//木オブジェクトを生成する

		//GrassLandのポリゴンを管理するクラス
		GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);

		//そのローカル座標を
		//そのポリゴンのTransformである、
		//pGrassLand_のTransformを使い、ワールド座標に変形させる
		TransformedInTheWorldMatrix(localToWorld, pGrassLand->transform_);

		return true;
	}


	return false;
}

//指定ポリゴン番号のローカル座標を取得
bool WinningSceneManager::GetLocalPosOfPolygonMakeGround(XMVECTOR* localPos, int polyNum)
{
	//GrassLandのポリゴンを管理するクラスに
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
	//ローカル座標を求める関数を呼び込み
	return pGrassLand->GetLocalPosOfPolygon(localPos, polyNum);
}
//ローカル座標を該当Transformにてワールド座標へ変形
void WinningSceneManager::TransformedInTheWorldMatrix(XMVECTOR* toPos, Transform& trans)
{
	//ワールド行列の計算
	trans.Calclation();

	//変換
	(*toPos) = XMVector3TransformCoord((*toPos), trans.GetWorldMatrix());

}

//Treeオブジェクトを作成するにあたってTransformを取得する
Transform WinningSceneManager::GetTreeTransform(XMVECTOR& localToWorld, float initSize)
{
	Transform trans;
	trans.position_ = localToWorld;
	//trans.scale_ = XMVectorSet(0.9f, 0.9f, 0.90f, 0.0f);	//デフォルトのサイズを取得する
	trans.scale_ = XMVectorSet(initSize, initSize, initSize, 0.0f);

	return trans;
}

//カメラ位置の移動（一定秒数を掛けて移動）
void WinningSceneManager::MoveCameraPosition(int x, int y)
{
	//現在値を移動開始位置として登録

	//移動先を取得し
	//移動先を登録
	MoveCameraPosition(Camera::GetPosition() , GetVector(x, y) , y);

}
//カメラ移動を動的処理として登録
void WinningSceneManager::MoveCameraPosition(XMVECTOR startPos , XMVECTOR targetPos , int y)
{
	/*
	//currentTargetInfo_.startPos = startPos;
	//移動先を取得
	//移動先を登録
	//currentTargetInfo_.targetPos = targetPos;


	//上記で取得した時間を、カウントクラスの終了時間として更新
		//再計測開始
	//ハンドル：CAM_MOVE_POS_TIMER
	//開始時間：0.f
	//終了時間：タグから読み込んだ次のタグまでの時間
	//pCountTimer_->ReStartTimerForCountUp(CAM_MOVE_POS_TIMER, 0.f, endTime);



	//移動開始
		//カメラ移動のタイマー開始を宣言
	//currentTargetInfo_.timerType = CAM_MOVE_POS_TIMER;
	*/
	//→上記のタイマー、動的処理のための情報格納は、PushBackTimerInfo（）にて



	//移動時間の初期化
		//次のタグを読み込むまでの時間を取得
		//タグ読み込みまでの時間を、移動時間とする
		//x : 固定値２（サブタグ（次のタグ読み込みまでの時間））
		//y : 現在の示されるセル値（0オリジン）
	float endTime = pCsvReader_->GetValueFloat((int)TAGS::SUB_TAG, y);

	//タイマーによる　動的処理の開始宣言
	//必要要素を引き渡し
		//構造体にタイマーによる移動などの必要情報をセットする
	PushBackTimerInfo(TIMER_TYPE::CAM_MOVE_POS_TIMER, endTime, startPos, targetPos);

}
//カメラの回転（一定秒数をかけて回転）
void WinningSceneManager::MoveCameraRotateY(int x, int y)
{
	//カメラを指定度数回転させる（指定時間をかけて）

	//次のタグまでの時間
	//（サブタグ）を読み込み、

	//終了時間の取得
		//x : 固定値２（サブタグ）
		//y : 現在の示されるセル値（0オリジン）
	float endTime = pCsvReader_->GetValueFloat((int)TAGS::SUB_TAG, y);

	//回転度数
	float angle = pCsvReader_->GetValueFloat((int)TAGS::MAIN_TAG_2, y);


	//時間 / 回転度数　にて、回転スピードを求める
	//上記を、タイマー情報に格納して、時間による回転を行う

	float SPEED = endTime / angle;

	//取得したスピードなど
	//を、タイマー情報のベクトルに登録
	XMVECTOR targetPos = XMVectorSet(SPEED, 0.f, 0.f, 0.f);

	//タイマーによる　動的処理の開始宣言
	//必要要素を引き渡し
	//構造体にタイマーによる移動などの必要情報をセットする
	//第五引数：区別：num = -1(y軸回転)
	PushBackTimerInfo(TIMER_TYPE::CAM_MOVE_ROTATE_TIMER, endTime, XMVectorSet(0.f, 0.f, 0.f, 0.f), targetPos);

}

//カメラの回転（一定秒数をかけて回転）
void WinningSceneManager::MoveCameraRotateX(int x, int y)
{
	//カメラを指定度数回転させる（指定時間をかけて）

//次のタグまでの時間
//（サブタグ）を読み込み、

//終了時間の取得
	//x : 固定値２（サブタグ）
	//y : 現在の示されるセル値（0オリジン）
	float endTime = pCsvReader_->GetValueFloat((int)TAGS::SUB_TAG, y);

	//回転度数
	float angle = pCsvReader_->GetValueFloat((int)TAGS::MAIN_TAG_2, y);


	//時間 / 回転度数　にて、回転スピードを求める
	//上記を、タイマー情報に格納して、時間による回転を行う

	float SPEED = endTime / angle;

	//取得したスピードなど
	//を、タイマー情報のベクトルに登録
	XMVECTOR targetPos = XMVectorSet(SPEED, 0.f, 0.f, 0.f);

	//タイマーによる　動的処理の開始宣言
	//必要要素を引き渡し
	//構造体にタイマーによる移動などの必要情報をセットする
	//第五引数：区別：num = 0(x軸回転)
	PushBackTimerInfo(TIMER_TYPE::CAM_MOVE_ROTATE_TIMER, endTime, XMVectorSet(0.f, 0.f, 0.f, 0.f), targetPos , 0);

}

/*
void WinningSceneManager::RotateCameraPosition(int x, int y)
{
}
*/
/*
void WinningSceneManager::MoveCameraPosOfTheObject(int x, int y)
{
	//オブジェクトの位置を取得して、
}
*/

//指定オブジェクトへ向けてカメラの回転開始 Y
void WinningSceneManager::MoveCameraRotateYOfTheObject(int x, int y)
{


	//対象のゲームオブジェクトの取得

	//取得ゲームオブジェクトの座標へ回転するタイマーのセット
		//ポリゴン番号１　（０，０）に向きたいならば、ゲームオブジェクトとして移動をしていない、Groundなどのゲームオブジェクトを取得して、その座標をもらえればよさそう。
		//敵オブジェクトなどに参照したい場合は、　専用関数でオブジェクトを取得する必要が出てくる
		//専用の行動関数　funcの作成

	//インスタンスを生成したオブジェクトごとの
		//シーンマネージャーが取得している連携オブジェクトを返す

		//メインタグにて示されるオブジェクトが確定したのち、そのオブジェクトのポインタを取得するときのための配列
	WINNING_SCENE_COOP_OBJECTS coopObj[MAX_MODELS] =
	{
		WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_ENEMY_SPAWN ,
		WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND,
		WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_MAX,
		WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_BACK_GROUND_TREE,
		WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER,
		WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER_OWNED_TREE,
		WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER_OWNED_TREE,
		WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_ENEMY_SPAWN ,

	};


	//対象となるオブジェクトを
	//メインタグ２から取得
		//作成するクラスを選択する
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);


	//連携オブジェクトを入れる型
	GameObject* pCoopObj = nullptr;

	//スコープ：�@
	//条件　：オブジェクトMAX値
	for (int i = 0; i < MAX_MODELS; i++)
	{
		//対象文字
		std::string target = instanceObjectInfos_[i].instanceObjectNames + '\0';

		//スコープ：�A
		//条件：取得タグと、目的のオブジェクトが等しい
		if (target == mainTag2)
		{
			//スコープ：�B
			//条件：MAX値でないならば
			//あらかじめ確保しておいた、配列にて示される、シーンの連携オブジェクトにアクセスする
			//連携先のオブジェクトがあるならば、
			if (coopObj[i] != WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_MAX)
			{
				//オブジェクト取得
				pCoopObj = GetCoopObject((int)coopObj[i]);

			}
			
			//処理終了
				//スコープ：�@を抜ける
			break;


		}
	
	}


	//条件：連携オブジェクトが取得できていなかったら
	if (pCoopObj == nullptr)
	{
		//処理終了
		return;
	}

		//現在のカメラの視点のベクトルと
			//現在のカメラTarget - 現在のカメラPosition　にて、視点方向を出す
		XMVECTOR camVec = Camera::GetTarget() - Camera::GetPosition();

		//連携オブジェクト座標取得のための準備
		//親のTransformの計算
		pCoopObj->pParent_->transform_.Calclation();


		//親のTransformにて示される、ワールド行列と、連携オブジェクト自身のローカル座標を計算し
			//ワールド座標にする
		XMVECTOR coopWorldPos = 
			XMVector3TransformCoord(pCoopObj->transform_.position_, 
				pCoopObj->pParent_->transform_.GetWorldMatrix());


		//現在のカメラの座標から、目的の位置（連携オブジェクトの座標）までの座標をつないだベクトル
		XMVECTOR targetVec = coopWorldPos - Camera::GetPosition();


		//計算ベクトルの正規化
		camVec = XMVector3Normalize(camVec);
		targetVec = XMVector3Normalize(targetVec);


		//２つのベクトルの内積のACosの値が、２つのベクトルのなす角となる
		//内積を求める（２つのベクトルによる、一番近い内積（小さい角度のほうの内積））
		float dot = XMVector3Dot(camVec, targetVec).vecX;


		//２つのベクトルのなす角
			//結果　＝　ラジアン
			//度に直す(計算の際に、最終的にラジアンに変化させるが、でバック時にわかりやすくするために、度にする)
		//float angle = acos(dot) * (180 / 3.14);	//←計算式
		float angle = XMConvertToDegrees((float)acos((double)dot));


		//タイマーへ登録する情報を定義
		{
			//終了時間の取得
			//x : 固定値２（サブタグ）
			//y : 現在の示されるセル値（0オリジン）
			float endTime = pCsvReader_->GetValueFloat((int)TAGS::SUB_TAG, y);

			//終了時間を　なす角で割って、
				//スピードを求める
				//フレームにおける経過時間を求めて、その経過時間＊スピードにて　回転の度合いを求める
			const float SPEED = endTime / angle;


			//取得したスピードなど
				//を、タイマー情報のベクトルに登録
			XMVECTOR targetPos = XMVectorSet(SPEED, 0.f, 0.f, 0.f);


			//タイマーによる　動的処理の開始宣言
			//必要要素を引き渡し
			//構造体にタイマーによる移動などの必要情報をセットする
			PushBackTimerInfo(TIMER_TYPE::CAM_MOVE_ROTATE_TIMER, endTime, XMVectorSet(0.f, 0.f, 0.f, 0.f), targetPos);
		}

}

//地面の始点ポリゴンから、終点ポリゴンへのカメラ視点移動を開始
void WinningSceneManager::MoveToScanGroundPolygon(int x , int y)
{
	//地面ポリゴンの合計ポリゴンを取得する
	//ポリゴン数の取得
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
	int polygonCount = pGrassLand->GetPolygonCount();


	//始点ポリゴンの座標取得
	XMVECTOR startPolyVec;
	//終点ポリゴンの座標取得
	XMVECTOR targetPolyVec;

	//座標取得
	GetWorldPosOfPolygonMakeGround(&startPolyVec, 1);
	GetWorldPosOfPolygonMakeGround(&targetPolyVec, polygonCount);

	//座標を上げる
		//見やすさのため
	startPolyVec += XMVectorSet(0.f, 2.f, 0.f, 0.f);
	targetPolyVec += XMVectorSet(0.f, 2.f, 0.f, 0.f);


	//移動設定
	MoveCameraPosition(startPolyVec, targetPolyVec, y);

}


//インスタンス生成
void WinningSceneManager::CreateInstance(int x, int y)
{
	//作成するクラスを選択する
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);

	//特殊関数ポインタ
	void(WinningSceneManager::* funcs[MAX_MODELS])(GameObject*) =
	{
		&WinningSceneManager::SpecialOfEnemy,
		&WinningSceneManager::SpecialOfGround,
		&WinningSceneManager::SpecialOfSkyBox,
		&WinningSceneManager::SpecialOfBackGroundTree,
		&WinningSceneManager::SpecialOfPlayer,
		&WinningSceneManager::SpecialOfPlayerOwnedTree,
		&WinningSceneManager::SpecialOfTree,
		&WinningSceneManager::SpecialOfEnemySpawn,

	};

	//カウンター
	int i = 0;
	//スコープ：�@
	//条件：モデルMAX値
	for (i = 0; i < MAX_MODELS; i++)
	{
		//対象文字
		std::string target = instanceObjectInfos_[i].instanceObjectNames + '\0';

		//スコープ：�A
		//条件：対象モデルを示す文字列と、タグから読み込んだ文字列が同じなら
		if (target == mainTag2)
		{
			GameObject* pModel = nullptr;
			//スコープ：�B
			//条件：ファイル名がNuLLでないなら
			if (instanceObjectInfos_[i].instanceFileNames != "NULL")
			{
				//インスタンス作成
				pModel = Instantiate<WinningDirectingModel>(this);
				
				//キャスト
					//ロードを読み込める型にキャスト
				WinningDirectingModel* pDModel = (WinningDirectingModel*)pModel;

				//ロード
				pDModel->LoadModel(instanceObjectInfos_[i].instanceFileNames);
			}

			//特殊関数（インスタンス生成における処理関数）呼び込み
			(this->*funcs[i])((GameObject*)pModel);

			//繰り返しの終了
				//スコープ：�@を抜ける
			break;
		}


	}




}
//インスタンス生成時の特殊行動　敵
void WinningSceneManager::SpecialOfEnemy(GameObject* pGameObject)
{
	//敵群に追加
		//専用のクラスに追加
	EnemySpawnForWinningScene* pEnemySpawn = (EnemySpawnForWinningScene*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_ENEMY_SPAWN);
	//追加
	pEnemySpawn->AddInstance(pGameObject);

	//敵の追加エフェクト,武器の作成
	Instantiate<EnemyWeapon>(pGameObject);
	Instantiate<EffectEnemy>(pGameObject);
}
//インスタンス生成時の特殊行動　地面
void WinningSceneManager::SpecialOfGround(GameObject* pGameObject)
{
	//地面クラスの作成
	GameObject* pGameObj = Instantiate<GrassLandObject>(this);
	//マネージャーに登録
	this->AddCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND, pGameObj);
		//本来であれば、作成クラスの中で、マネージャークラスへついかする処理をしたいが、
		//GrassLandObjectは、WinningScene専用のクラスではないため、外部から追加を行う
}
//インスタンス生成時の特殊行動　SkyBox
void WinningSceneManager::SpecialOfSkyBox(GameObject* pGameObject)
{
	//SkyBoxクラスの作成
	SkyBox* pSkyBox = (SkyBox*)Instantiate<SkyBox>(this);


	//地面オブジェクトを取得して
	GrassLandObject* pGrassLandObject = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);

	//地面オブジェクトから、ポリゴンサイズ、ポリゴン幅、ポリゴン高さの取得
	int width;
	int depth;
	pGrassLandObject->GetWidthAndDepth(&width, &depth);
	float size = pGrassLandObject->GetPolygonSize();



	//位置調整
	static constexpr float SKYBOX_SCALE_Y = 100.f;
	//拡大、移動の詳細は、GameSceneにて
	//拡大
	pSkyBox->SetSkyBoxScale(XMVectorSet((float)(size * width), SKYBOX_SCALE_Y, (float)(size * depth), 0.f));
	//移動
	pSkyBox->SetSkyBoxPosition(XMVectorSet(size * (width / 2.f), -(SKYBOX_SCALE_Y / 10.f), size * (depth / 2.f), 0.f));



	//pSkyBox->transform_.position_ += XMVectorSet(20.f , 0.f,0.f,0.f);
	//
	//pSkyBox->transform_.scale_ += XMVectorSet(10.f , 10.f , 10.f , 0.f);
	//pSkyBox->transform_.scale_.vecX *= 3.0f;
	//pSkyBox->transform_.scale_.vecY *= 3.0f;
	//pSkyBox->transform_.scale_.vecZ *= 3.0f;

	

}
//インスタンス生成時の特殊行動　背景木
void WinningSceneManager::SpecialOfBackGroundTree(GameObject* pGameObject)
{
	//背景クラスの木群クラスの作成
	GameObject* pGameObj = Instantiate<GroupOfTreeForWinningScene>(this);
	//マネージャーに登録
	this->AddCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_BACK_GROUND_TREE, pGameObj);
	////GroupOfTreeへ、だれのオブジェクトなのかを知らせる
	GroupOfTreeForWinningScene* pGroupOfTree = (GroupOfTreeForWinningScene*)pGameObj;
	pGroupOfTree->Initialize(WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_BACK_GROUND_TREE);

}
//インスタンス生成時の特殊行動　プレイヤー
void WinningSceneManager::SpecialOfPlayer(GameObject* pGameObject)
{
	//マネージャーに登録
	this->AddCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER, pGameObject);
	
	//ランダム配置
	//地面ポリゴンの合計ポリゴンを取得する
		//ポリゴン数の取得
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
	int polygonCount = pGrassLand->GetPolygonCount();

	//合計ポリゴン数を敵数で割り、
		//1人のポリゴンの範囲を決める
	int polyRange = polygonCount / 5;

	//乱数の範囲
	//（ポリゴン範囲　＊　現在の添え字）　〜　（ポリゴン範囲　＊　現在の添え字）＋　ポリゴンの範囲　ー　１
	int polyNum = (polyRange * 3) + rand() % polyRange;
	//(polyRange * i)　から、polyRange個分の乱数


//ポリゴン番号を、もとに、
	//地面ポリゴンの座標を求めて、移動させる
		//ポリゴンのローカル座標を入れる変数
	XMVECTOR localToWorld;

	//ポリゴン番号から
	//ローカル座標を取得し、ワールド座標を取得する
	//条件：ポリゴン番号取得できたか
	if (GetWorldPosOfPolygonMakeGround(&localToWorld, polyNum))
	{
		//移動
		pGameObject->transform_.position_ = localToWorld;

		//プレイヤーの高さを少し上げる
		pGameObject->transform_.position_.vecY += 1.0f;


		//ポリゴン番号を更新
		//必要型へキャスト
		WinningDirectingModel* pWinningModel = (WinningDirectingModel*)pGameObject;
		//自身が立っているポリゴン番号を伝える
		pWinningModel->SetStandPolyNum(polyNum);

	}

}
//インスタンス生成時の特殊行動　プレイヤー専用木群
void WinningSceneManager::SpecialOfPlayerOwnedTree(GameObject* pGameObject)
{
	//背景クラスの木群クラスの作成
	GameObject* pGameObj = Instantiate<GroupOfTreeForWinningScene>(this);
	//マネージャーに登録
	this->AddCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER_OWNED_TREE, pGameObj);
	////GroupOfTreeへ、だれのオブジェクトなのかを知らせる
	GroupOfTreeForWinningScene* pGroupOfTree = (GroupOfTreeForWinningScene*)pGameObj;
	
	//ここで注意が必要なのが、
		//GroupOfTreeとして登録するEnum値は、あくまでも、
		//背景用の木として作成する
			//背景用として作成するのは、演出のため、当たり判定なども必要ない、そのため、背景用の干渉を受けないものとして作成させる
	pGroupOfTree->Initialize(WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER_OWNED_TREE);

}
//インスタンス生成時の特殊行動　木
void WinningSceneManager::SpecialOfTree(GameObject* pGameObject)
{
	//木オブジェクト1つを作成して、
		//プレイヤー専用の木群に登録
		//配置場所は、ランダムに決める

	//ポリゴン数の取得
	//GrassLandのポリゴンを管理するクラス
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
	int polygonCount = pGrassLand->GetPolygonCount();

	//ポリゴン番号の乱数取得
		//0から、polygoncount個分
	int polyNum = 0 + rand() % polygonCount;

	//ポリゴンのローカル座標を入れる変数
	XMVECTOR localToWorld;

	//ポリゴン番号から
	//ローカル座標を取得し、ワールド座標を取得する
	//条件：ポリゴン番号を取得できたか
	if (GetWorldPosOfPolygonMakeGround(&localToWorld, polyNum))
	{

		//localToWorld = XMVectorSet(0.f, 0.f, 0.f, 0.f);

		//変形後の座標を新規確保のTransformにセットして、
		//そのTransformをもとに、
			//木の生成位置を確定する

		//木のTransformを取得する
		Transform trans = GetTreeTransform(localToWorld, 0.9f);
		trans.rotate_.vecY = 90.f * (rand() % 4);	//乱数で回転させる
		

		//プレイヤー専用の木オブジェクトに登録
		GroupOfTreeForWinningScene* pGroupOfTree = (GroupOfTreeForWinningScene*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER_OWNED_TREE);
		pGroupOfTree->AddTreeObject(trans, polyNum);
	}


}
//インスタンス生成時の特殊行動　敵スポーン
void WinningSceneManager::SpecialOfEnemySpawn(GameObject* pGameObject)
{

	//敵スポーンの作成	//Enemyオブジェクト群
	GameObject* pEnemySpawn = Instantiate<EnemySpawnForWinningScene>(this);
	//マネージャーに登録
	this->AddCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_ENEMY_SPAWN, pEnemySpawn);

}

/*
void WinningSceneManager::EnterObject(int x, int y)
{
}
*/
/*
void WinningSceneManager::LeaveObject(int x, int y)
{
}
*/
/*
void WinningSceneManager::SetTransformObject(int x, int y)
{
}


void WinningSceneManager::TransformObjectAlpha(int x, int y)
{
}

void WinningSceneManager::DeleteObject(int x, int y)
{
}
*/
//void WinningSceneManager::LoadImage(int x, int y)
//{
//}

//画像ロード
void WinningSceneManager::LoadImageForWinnning(int x, int y)
{
	//作成するクラスを選択する
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);

	//スコープ：�@
	//条件：UIGroupMAX(enum)まで
	for (int i = 0; i < (int)WINNING_SCENE_UI_GROUP_TYPE::WINNING_UI_GROUP_MAX; i++)
	{
		//対象文字
		std::string target = imageInfos_[i].imageName + '\0';

		//スコープ：�A
		//条件：対象文字とタグ読み込みの文字列が同じか
		if (target == mainTag2)
		{
			//UIGroup作成
			//インスタンス生成
			UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

			//UIGroupをSceneMnagerにセットする
			this->AddUIGroup(i, uiGroup);

			//UI画像
			{
				//ロードと追加
				uiGroup->AddStackUI(imageInfos_[i].imageFileName);

			}

			//UIGroupの非描画
			this->InvisibleSceneUIGroup(i);
		
			//繰り返しの終了
				//スコープ：�@を抜ける
			break;
		}


	}






}

//指定オブジェクトの座標にカメラを設置
void WinningSceneManager::SetCameraPosOfTheObject(int x, int y)
{
	/*
	//CSVから示される、オブジェクト名を取得
	//オブジェクトを探す
		//オブジェクトに対応した、COOPオブジェクトを呼び込み、そのオブジェクトを取得
		//取得した、オブジェクトの座標を参照するように、カメラを移動

	敵や木など
	ただオブジェクトのTransformから移動目的ベクトルを取得するのではなく、
	敵や木などの関数を読んで、適切な移動位置を示してもらう必要がある。
	そのために、下記で処理関数を分ける必要がある
	*/

	//特殊関数ポインタ登録
		//nullptrも登録するが、
		//nullptrの関数を呼び出そうとすれば、当然エラーになるので読み込まない、移動をしない前提
	void(WinningSceneManager:: * funcs[MAX_MODELS])() =
	{
		&WinningSceneManager::SetCameraEnemyPos,
		nullptr,
		nullptr,
		nullptr,
		&WinningSceneManager::SetCameraPlayerPos,
		nullptr,
		&WinningSceneManager::SetCameraTreePos,
		nullptr,

	};


	//作成するクラスを選択する
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);

	//カウンター
	int i = 0;
	//スコープ：�@
	//条件：モデルMAX値
	for (i = 0; i < MAX_MODELS; i++)
	{
		//対象文字
		std::string target = instanceObjectInfos_[i].instanceObjectNames + '\0';

		//スコープ：�A
		//条件：対象文字がタグから読み込んだ文字列と同じか
		if (target == mainTag2)
		{
			//スコープ：�B
			//条件：関数が存在していれば、
			if (funcs[i] != nullptr)
			{
				//特殊関数呼び込み
				(this->*funcs[i])();
			}

			//繰り返しの終了
				//スコープ：�@を抜ける
			break;
		}


	}

}
//プレイヤーオブジェクトのFPS視点にカメラを設置
void WinningSceneManager::SetCameraPosOfThePlayer(int x, int y)
{
	//プレイヤー取得
	GameObject* pPlayer = GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER);
	//プレイヤーの座標を取得（ワールド座標（親からのローカル座標ではないので注意））
	//プレイヤー自身の座標（ローカル）　＋　親までのワールド行列

	//視点の離れ具合
	XMVECTOR posDeparture = XMVectorSet(0.f, 1.f, -0.1f, 0.f);
	//焦点の離れ具合
	XMVECTOR tarDeparture = posDeparture + XMVectorSet(0.f, 0.f, -1.f, 0.f);
	//カメラの座標（離れ具合）のセット
	SetDeparture(pPlayer, posDeparture, tarDeparture);



	//プレイヤー視点にセットされた場合は、
		//プレイヤーの向いている方向にカメラを向ける。
		//つまり、プレイヤーモデルのFPS視点になるように座標をセットする

}
//カメラをプレイヤー位置（TPS）にセット
void WinningSceneManager::SetCameraPlayerPos()
{
	//プレイヤー取得
	GameObject* pPlayer = GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER);
	//プレイヤーの座標を取得（ワールド座標（親からのローカル座標ではないので注意））
	//プレイヤー自身の座標（ローカル）　＋　親までのワールド行列
	

	XMVECTOR posDeparture = XMVectorSet(2.f, 2.f, -5.f, 0.f);
	//XMVECTOR tarDeparture = posDeparture + XMVectorSet(0.f, 0.f, -1.f, 0.f);
	//カメラの座標（離れ具合）のセット
	SetDeparture(pPlayer, posDeparture);

}
//カメラを敵位置（TPS）にセット
void WinningSceneManager::SetCameraEnemyPos()
{
	//敵スポーンから、
		//1体目から敵のオブジェクトを貰う。
		//次に参照された際には、2体目の敵オブジェクトを貰えるように、順番で取得する
	EnemySpawnForWinningScene* pEnemySpawn = (EnemySpawnForWinningScene*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_ENEMY_SPAWN);

	//オブジェクトを取得
	GameObject* pObject = pEnemySpawn->GetInstance();

	//条件：取得出来ているか
	if (pObject != nullptr)
	{
		//カメラの座標（離れ具合）のセット
		SetDeparture(pObject, XMVectorSet(2.f, 2.f, -5.f, 0.f));
	}
}
//カメラを木位置（TPS）にセット
void WinningSceneManager::SetCameraTreePos()
{
	//木のオブジェクトの中から
	//ランダムオブジェクトを取得
	GroupOfTreeForWinningScene* pGroupOfTree = (GroupOfTreeForWinningScene*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER_OWNED_TREE);

	//ランダムオブジェクトの描画位置を取得
	XMVECTOR sourcePos = pGroupOfTree->GetRandomTreeObjectPosition();

	
	//カメラの座標（離れ具合）のセット
	//カメラの位置をセットする
	Camera::SetPosition(sourcePos + XMVectorSet(2.f, 10.f, -20.f, 0.f));
	Camera::SetTarget(sourcePos + XMVectorSet(0.f, 10.f, 0.f, 0.f));
	
}
//カメラの位置を離れ具合を考慮してセット（位置の離れ具合）
void WinningSceneManager::SetDeparture(GameObject* pObject, XMVECTOR departureVec)
{
	//焦点の離れ具合を０としてセット
	SetDeparture(pObject, departureVec, XMVectorSet(0.f, 0.f, 0.f, 0.f));
}
//カメラの位置を離れ具合を考慮してセット（位置の離れ具合　、焦点の離れ具合）
void WinningSceneManager::SetDeparture(GameObject* pObject, XMVECTOR posDepartureVec, XMVECTOR tarDepartureVec)
{
	//自身の座標
	XMVECTOR objPos = pObject->transform_.position_;
	//親までのワールド行列
	pObject->pParent_->transform_.Calclation();
	XMMATRIX parentMat = pObject->pParent_->transform_.GetWorldMatrix();


	//親の行列とVector配列を掛ける
	objPos = XMVector3TransformCoord(objPos, parentMat);

	//カメラの位置をセットする
	Camera::SetPosition(objPos + posDepartureVec);
	Camera::SetTarget(objPos + tarDepartureVec);

}

//画像ロード
void WinningSceneManager::VisibleImage(int x, int y)
{
	//作成するクラスを選択する
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);

	//スコープ：�@
	//条件：UIGroupMAX(enum)まで
	for (int i = 0; i < (int)WINNING_SCENE_UI_GROUP_TYPE::WINNING_UI_GROUP_MAX; i++)
	{
		//対象文字
		std::string target = imageInfos_[i].imageName + '\0';

		//スコープ：�A
		//条件：対象文字とタグの文字列が同じ場合
		if (target == mainTag2)
		{
			//UIGroupの描画
			this->VisibleSceneUIGroup(i);

			//初期値のα値：０
			//描画の段階で、時間をかけてα値を０〜１になるように変動させたい。
				//その場合、
				//描画時に１．０ｆの場合、一瞬フラッシュのように画像が移り、そのあとに、遷移が行われる。
				//上記を回避するには、初期値を０にするよりほかにない
			this->SetAlphaSceneUIGroup(i, 0.f);

			//繰り返しの終了
				//スコープ：�@を抜ける
			break;
		}

	}

}

//画像非描画
void WinningSceneManager::InvisibleImage(int x, int y)
{
	//作成するクラスを選択する
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);


	//スコープ：�@
	//条件：UIGroupMAX(enum)まで
	for (int i = 0; i < (int)WINNING_SCENE_UI_GROUP_TYPE::WINNING_UI_GROUP_MAX; i++)
	{
		//対象文字
		std::string target = imageInfos_[i].imageName + '\0';

		//スコープ：�A
		//条件：対象文字とタグの文字列が同じ場合
		if (target == mainTag2)
		{
			//UIGroupの非描画
			this->InvisibleSceneUIGroup(i);

			//繰り返しの終了
				//スコープ：�@を抜ける
			break;
		}

	}
}

//画像A値変動(一定時間をかけて変動)(増算)(減算)
void WinningSceneManager::AddOrSubOfImageAlpha(int x, int y)
{
	//タイマーによるα値変動を開始

	//タイマーによる変動計算方法
	//指定終了時間をかけて、α値を１〜０に変更する

	//�@終了時間を取得（カウントアップ）
	//�A現在の経過時間を取得（カウントアップ）
	//�B�@ / �Aを行う　
	//�C�Bの結果をα値としてセット


	//画像のタイプを文字列で取得
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);

	//画像データへの添え字
	int i;
	//スコープ：�@
	//条件：画像データ分回す
	for (i = 0; i < MAX_IMAGES; i++)
	{
		//対象文字
		std::string target = imageInfos_[i].imageName + '\0';

		//スコープ：�A
		//条件：対象文字とタグ文字列が同じ場合
		if (target == mainTag2)
		{
			
			//繰り返しの終了
				//現在のi添え字を残して、繰り返し終了
				//スコープ：�@抜ける
			break;
		}


	}

	//条件：添え字が最大値と等しい場合
	if (i == MAX_IMAGES)
	{
		//処理の終了
		return;
	}


	//�@
	//タイマー処理の終了時間の取得
		//x : 固定値２（サブタグ）
		//y : 現在の示されるセル値（0オリジン）
	float endTime = pCsvReader_->GetValueFloat((int)TAGS::SUB_TAG, y);


	//自身のタグを読み込み
		//タグにより、計算方法が
		//増算ー＞０、　減算ー＞１かを判別する
	WINNING_ANIMATION_TAG tag = GetAnimationTag((int)TAGS::MAIN_TAG, y);

	//タグにより、α値計算の増算か、減算かを判別
	//計算のためにfloat値にて判断
		//増算：0.f , 減算：1.f
	//初期値：WINNING_TAG_ADDITION_IMAGE_ALPHA（増算）:0.f
	float type = 0.f;
	//条件：タグが減算を示しているタグならば
	if (tag == WINNING_ANIMATION_TAG::WINNING_TAG_SUBTRACTION_IMAGE_ALPHA)
	{
		//WINNING_TAG_SUBTRACTION_IMAGE_ALPHA（減算）:1.f
		type = 1.f;
	}
	

	//タイマーの設定開始
	//必要要素を引き渡し
	//構造体にタイマーによる移動などの必要情報をセットする

	//引数：画像A値変動タイマー
	//引数：タイマー終了時間
	//引数：x->増算、減算かを識別する値（０−＞増算、１ー＞減算）
	//引数：ベクトル設定なし
	//引数：画像（UIGroup）へのタイプ（添え字）
	PushBackTimerInfo(
		TIMER_TYPE::TRANSFORM_IMAGE_ALPHA_TIMER, endTime,
		XMVectorSet(type, 0.f, 0.f, 0.f), XMVectorSet(0.f, 0.f, 0.f, 0.f) ,
		i);
}

//音楽のロード
void WinningSceneManager::LoadAudio(int x, int y)
{
	//ファイルをロード
		//ロードするAudioを選択
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);

	//スコープ：�@
	//条件：音楽データ分回す
	for (int i = 0; i < MAX_AUDIOS; i++)
	{
		//対象文字
		std::string target = audioInfos_[i].audioName + '\0';

		//スコープ：�A
		//条件：対象文字とタグ文字列が同じ場合
		if (target == mainTag2)
		{
			//ロードと返り値にて、ハンドル番号を格納
			audioInfos_[i].hAudio =  Audio::Load(audioInfos_[i].audioFileName);

			//繰り返しの終了
				//スコープ：�@抜ける
			break;
		}


	}

}

//音楽の再生
void WinningSceneManager::PlayAudio(int x, int y)
{
	//ファイルをロード
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);

	//スコープ：�@
	//条件：音楽データ分回す
	for (int i = 0; i < MAX_AUDIOS; i++)
	{
		//対象文字
		std::string target = audioInfos_[i].audioName + '\0';

		//スコープ：�A
		//条件：対象文字とタグ文字列が同じ場合
		//　　：&&
		//　　：ハンドル番号がー１でない
		if (target == mainTag2 && audioInfos_[i].hAudio != -1)
		{
			//音楽再生
			Audio::Play(audioInfos_[i].hAudio);

			//音量設定
			Audio::SetVolume(audioInfos_[i].hAudio, 0.8f);

			//繰り返しの終了
				//スコープ：�@抜ける
			break;
		}


	}
}

//音楽の停止
void WinningSceneManager::StopAudio(int x, int y)
{
	//ファイルをロード
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);

	//スコープ：�@
	//条件：音楽データ分回す
	for (int i = 0; i < MAX_AUDIOS; i++)
	{
		//対象文字
		std::string target = audioInfos_[i].audioName + '\0';

		//スコープ：�A
		//条件：対象文字とタグ文字列が同じ場合
		//　　：&&
		//　　：ハンドル番号がー１でない
		if (target == mainTag2 && audioInfos_[i].hAudio != -1)
		{
			//音楽停止
			Audio::Stop(audioInfos_[i].hAudio);

			//繰り返しの終了
				//スコープ：�@抜ける
			break;
		}

	}
}

//音楽のループ
void WinningSceneManager::LoopAudio(int x, int y)
{
	//ファイルをロード
	//x : 固定値１（メインタグ２）
	//y : 現在の示されるセル値（0オリジン）
	std::string mainTag2 = pCsvReader_->GetString((int)TAGS::MAIN_TAG_2, y);

	//スコープ：�@
	//条件：音楽データ分回す
	for (int i = 0; i < MAX_AUDIOS; i++)
	{
		//対象文字
		std::string target = audioInfos_[i].audioName + '\0';

		//スコープ：�A
		//条件：対象文字とタグ文字列が同じ場合
		//　　：&&
		//　　：ハンドル番号がー１でない
		if (target == mainTag2 && audioInfos_[i].hAudio != -1)
		{
			//音楽のループ設定
			Audio::InfiniteLoop(audioInfos_[i].hAudio);

			//繰り返しの終了
				//スコープ：�@抜ける
			break;
		}
	}
}

//地面に沿って、背景の木を生成
void WinningSceneManager::CreateBackGroundTree(int x, int y)
{
	//地面のオブジェクトの四方を木オブジェクトで囲ませる
/*

そのポリゴンの「４」ポリゴンごとに、
ポリゴン番号を取得し、

ポリゴンを作る3頂点の
中心の座標を求めて、
中心の座標を、
ローカル座標でもらう。

もらった、ローカル座標を、
Transformを使って、ワールド座標にする。

その座標に、
木オブジェクトを作成

これを、
地面の上辺
地面の下辺
地面の右辺
地面の左辺　で行う




	*/


	//ポリゴン数の取得
	//GrassLandのポリゴンを管理するクラス
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
	int polygonCount = pGrassLand->GetPolygonCount();

	//幅の頂点数、奥行きの頂点数の取得
	int width = -1;
	int depth = -1;
	pGrassLand->GetWidthAndDepth(&width, &depth);


	//木オブジェクトを管理するクラス
		//新しい木の描画先を指定して、描画してもらうクラス
	GroupOfTreeForWinningScene* pGroupOfTree = (GroupOfTreeForWinningScene*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_BACK_GROUND_TREE);


	//木オブジェクトの登録方向
	enum TREE_DIRECTION
	{
		DOWN_DIRECTION = 0,
		RIGHT_DIRECTION = 90,
		UP_DIRECTION = 180,
		LEFT_DIRECTION = 270,

	};

	//木を設置するための情報群
	struct polygonInfo
	{
		int polygonNumber;	//ポリゴン番号

		TREE_DIRECTION direction;	//回転方向として扱う値

	};


	//ポリゴン番号
	//一行のポリゴン数
		// width - 1
	std::vector<polygonInfo> polygonNumbers;
	polygonNumbers.clear();


	//木オブジェクトを生成する間隔
		//ポリゴンの間隔
		//幅
	static constexpr unsigned int POLYGON_WIDTH_DISTANCE = 4;
	//ポリゴンの間隔
	//奥行
	static constexpr unsigned int POLYGON_HEIGHT_DISTANCE = 8;




	//ポリゴン番号の登録

	//木の配置場所
	//地面オブジェクトの上辺
	//地面オブジェクトの下辺
	
	//ポリゴン１から
	//条件：width - 1になるまで繰り返す
	//増減：４ポリゴンごと
	for (int i = 1; i < (width - 1); i += POLYGON_WIDTH_DISTANCE)
	{
		//上辺
		{
			//登録構造体
			polygonInfo up;
			up.polygonNumber = i;
			up.direction = UP_DIRECTION;
			polygonNumbers.push_back(up);
		}

		//下辺
		{
			//登録構造体
			polygonInfo down;
			down.polygonNumber = polygonCount - width - 1 + i;
			down.direction = DOWN_DIRECTION;
			polygonNumbers.push_back(down);
		}
	}

	//木の配置場所
	//地面オブジェクトの右辺
	//地面オブジェクトの左辺
	
		//縦のポリゴン数は
		//一行に２つあるため、
		// depthの２倍のポリゴンが存在することになる
	//条件：depthの２倍の-1
	//増減：８ポリゴンごと
	for (int i = 1; i < (depth * 2 - 1); i += POLYGON_HEIGHT_DISTANCE)
	{
		//左辺
			//iは1から始まり、
			//仮に　width = 5;
			//1行目　１
			//2行目　５
			//3行目　９
			//4行目　１３

			//連続の法則性
				//１から width - 1ずつ増えていく
		int polyLeftNum = 1 + ((width - 1) * (i - 1));
		{
			//登録構造体
			polygonInfo left;
			left.polygonNumber = polyLeftNum;
			left.direction = LEFT_DIRECTION;
			polygonNumbers.push_back(left);
		}

		//右辺
			//連続の法則性
			//leftの　-1 
		int polyRightNum = polyLeftNum + (width - 2);
		{
			//登録構造体
			polygonInfo right;
			right.polygonNumber = polyRightNum;
			right.direction = RIGHT_DIRECTION;
			polygonNumbers.push_back(right);
		}

	}

	//木オブジェクトの生成
	//木配置ポリゴンが格納された可変長配列より
		//ポリゴン位置をワールド座標に変換し、その位置へ木オブジェクトを設置
	//条件：木配置ポリゴン群
	for (int i = 0; i < polygonNumbers.size(); i++)
	{

		//ポリゴンのローカル座標を入れる変数
		XMVECTOR localToWorld;

		//ポリゴン番号から
		//ローカル座標を取得し、ワールド座標を取得する
		//条件：ポリゴン番号を取得できたら
		if (GetWorldPosOfPolygonMakeGround(&localToWorld, polygonNumbers[i].polygonNumber))
		{
			//ポリゴン番号が存在し、
				//ローカル座標を受け取れたら
				//木オブジェクトを生成する


			//変形後の座標を新規確保のTransformにセットして、
			//そのTransformをもとに、
				//木の生成位置を確定する

			//木のTransformを取得する
			Transform trans = GetTreeTransform(localToWorld, 0.9f);
			//enumの値を回転値として登録
			trans.rotate_.vecY = (float)polygonNumbers[i].direction;	
			//木オブジェクトを追加
			pGroupOfTree->AddTreeObject(trans, polygonNumbers[i].polygonNumber);

		}
	}
}
/*
void WinningSceneManager::ExecutionLsystem(int x, int y)
{
}
*/

//地面のポリゴンを緑ポリゴンにするアニメーションの開始
void WinningSceneManager::StartPolygonPaintAnimation(int x, int y)
{
	//ポリゴン（x:０，y:０）にGrassを作成し、
		//拡張準備を行う。
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
	//pGrassLand->CreateOneGrassPolygon(1);
	//一番初めのポリゴンを始点として、Grassオブジェクトの作成
	pGrassLand->CreateGrassPolygon(1);

	//プレイヤーオブジェクトを取得し、
		//プレイヤーオブジェクトの立っているポリゴン番号に
		//Grassオブジェクトを作成する
	WinningDirectingModel* pPlayer = (WinningDirectingModel*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER);
	//ポリゴン番号を取得し、そのポリゴン番号へGrassの生成
	int polyNum = pPlayer->GetStandPolyNum();
	if (polyNum != -1)
	{
		pGrassLand->CreateGrassPolygon(polyNum);
	}


	/*
	//作成したGrassの時間による成長を止める
		//時間による、成長管理を行わせたいので、時間成長を止める
		//→上記の処理は必要なし→初めから、GrassクラスのUpdateにて、計算時間を渡し、その時間によって成長できるかの判定を行っている、
		//→Updateを行わなければ実行されることはない
	//×→Updateは、GrasｓLandが行うようにしているので、
		//→GrassLandObjectを作成してしまうと、　自動的にUpdateされるようになってしまう。
	*/


	//移動タイマー情報の定義
	{

		//移動時間の初期化
		//次のタグを読み込むまでの時間を取得
		//タグ読み込みまでの時間を、移動時間とする
			//x : 固定値１（メインタグ２）
			//y : 現在の示されるセル値（0オリジン）
		float endTime = pCsvReader_->GetValueFloat((int)TAGS::MAIN_TAG_2, y);

		/********************************************************/
		//保存
		//StartPaintの時点で
			//endPos の　ｘ　に、　拡張の時間間隔を保存

			//endPos の　ｙ　に、　前回の最終更新時間（拡張を行った時の時間）を保存
				//ｙと現在の時間の差分が　ｘ以上ならば、一回拡張とともに、現在時間をｙに更新

		//上記を、タイマーが終了するまで
		/********************************************************/


		//地面ポリゴンの横ポリゴン数を取得
		int width; int depth;
		pGrassLand->GetWidthAndDepth(&width, &depth);

		//目的位置
		XMVECTOR targetPos;
		//終了時間 / 横　似て、拡張を行う時間間隔のセット
		targetPos.vecX = endTime / width;
		//前回更新した時間の初期化
		targetPos.vecY = 0.0f;
		//初期化
		targetPos.vecZ = 0.0f;


		//タイマーの設定開始
		//必要要素を引き渡し
		//構造体にタイマーによる移動などの必要情報をセットする
		PushBackTimerInfo(
			TIMER_TYPE::DIAGONAL_POLYGON_PAINT_ANIMATION_TIMER,
			endTime, XMVectorSet(0.f, 0.f, 0.f, 0.f), targetPos);
	}

}

//敵の消去モーションを実行
void WinningSceneManager::StartEnemyEraseMotion(int x, int y)
{
	//終了時間（消去モーションを行う時間）を取得
	//x : 固定値２（サブタグ（次のタグまでの経過時間））
	//y : 現在の示されるセル値（0オリジン）
	float endTime = pCsvReader_->GetValueFloat((int)TAGS::SUB_TAG, y);

	//時間を渡し、
		//消去モーションを行わせる
	EnemySpawnForWinningScene* pEnemySpawn = (EnemySpawnForWinningScene*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_ENEMY_SPAWN);
	//敵オブジェクトに消去モーションを行わせる
		//敵オブジェクトは、呼び込むたびに消去モーションを行わせる対象を切り替える
	pEnemySpawn->StartEraseMotion(endTime);

}

//ダメージエフェクト（敵につけているエフェクト）をプレイヤーへ追加
void WinningSceneManager::AddEnemyEffect(int x, int y)
{
	
	//プレイヤーを取得し、
	//プレイヤーオブジェクトへダメージエフェクトをつける
	WinningDirectingModel* pPlayer = (WinningDirectingModel*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_PLAYER);
	
	//敵のダメージエフェクトをつける
		//つける相手、描画順を考慮してエフェクトをつける（ブレンドの技術を使用して、背景透過を再現しているため、
		//α値の設定をしているピクセルは、描画順が後であればあるほど、現在描画されているピクセル色を込みした、ピクセル処理とすることができる。逆に言うと、α値のピクセルを先に描画して、後で、背景透過の後ろのピクセルを塗っても、色は出ない。背景透過しない）
	GameObject* pEffectEnemy = Instantiate<EffectEnemy>(pPlayer);

	//微調整
	pEffectEnemy->transform_.position_.vecZ -= 0.3f;


	//メインタグから、
		//消去モーションの時間を取得
	//終了時間（消去モーションを行う時間）を取得
	//x : 固定値１
	//y : 現在の示されるセル値（0オリジン）
	float endTime = pCsvReader_->GetValueFloat((int)TAGS::MAIN_TAG_2, y);


	//EraseMotionも同時に開始する
		//消去モーションも開始
		//消去モーションは　シェーダー切り替えにより実現
		//消去モーション実行呼び込み
	pPlayer->StartEraseMotion(endTime);

	//消去スピードを変更
	pPlayer->ChangeEraseSpeed(0.2f);


}

//敵オブジェクトのランダム配置
void WinningSceneManager::SetRandomPosAllEnemy(int x, int y)
{
	//地面ポリゴンの合計ポリゴンを取得する
		//ポリゴン数の取得
	GrassLandObject* pGrassLand = (GrassLandObject*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_GRASS_LAND);
	int polygonCount = pGrassLand->GetPolygonCount();

	//敵スポーンの取得
	EnemySpawnForWinningScene* pEnemySpawn = (EnemySpawnForWinningScene*)GetCoopObject((int)WINNING_SCENE_COOP_OBJECTS::WINNING_COOP_ENEMY_SPAWN);
	//敵の合計数取得
	const unsigned int MAX = (unsigned int)pEnemySpawn->GetObjectCount();

	//合計数の中から
		//ランダムでポリゴン番号を取得し、
		//そのポリゴン番号へ、モデルを配置する
		//※ただし、敵オブジェクトが5体いる場合、　合計ポリゴン数を5で割り、(合計1000 / 5 = 200)商の数をひとつのオブジェクトのランダム配置先のポリゴン番号の範囲とする
			//※　つまり、　1体目は、 0~199。2体目は、200~399。と、一体、200の範囲でランダムの範囲を決めて、その範囲から、乱数を生成する
	//条件：敵の合計数まで
	for (unsigned int i = 0; i < MAX; i++)
	{
		//合計ポリゴン数を敵数で割り、
			//1人のポリゴンの範囲を決める
		int polyRange = polygonCount / MAX;

		//乱数の範囲
		//（ポリゴン範囲　＊　現在の添え字）　〜　（ポリゴン範囲　＊　現在の添え字）＋　ポリゴンの範囲　ー　１
		int polyNum = (polyRange * i) + rand() % polyRange;
			//(polyRange * i)　から、polyRange個分の乱数


		//ポリゴン番号を、もとに、
			//地面ポリゴンの座標を求めて、移動させる
				//ポリゴンのローカル座標を入れる変数
		XMVECTOR localToWorld;

		//ポリゴン番号から
		//ローカル座標を取得し、ワールド座標を取得する
		//条件：ポリゴン番号を取得できたら
		if (GetWorldPosOfPolygonMakeGround(&localToWorld, polyNum))
		{
			//敵の高さを少し上げる
				//描画の際に、地面に埋まってしまうため、
				//座標を上げる
			localToWorld.vecY += 1.0f;


			//移動
			pEnemySpawn->SetPosition(i , localToWorld);
		}

	}



}

