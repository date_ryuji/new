#pragma once
//システム内　標準ヘッダ(usr/)
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
//カレントディレクトリ以下　ヘッダー（カレントディレクトリ：プロジェクトフォルダ/）
#include "../../Engine/Scene/SceneManagerParent.h"	//シーンマネージャー　抽象クラス


/*
勝利演出を行うアニメーション実行シーンのマネージャー

勝利演出を動画で用意しない理由は、
動画で用意すると、
地面のテクスチャや、木のテクスチャを変えた時に、
それぞれのテクスチャすべてのパターンの動画を作る必要があり、リソース的に非効率だと思った。（提出する際に、圧縮しても、動画はほとんど圧縮の効果を持たない）



*/
/*
	演出流れ（実装）

	・CSV
	
	　　	�@処理の流れをCSVにて管理する。
	　		　→CSVには、各タグとなる文字列３文字と、タグを読み込んだ時に次のタグを読み込むまでの時間を一行に記述。

	・ソース
			�ACSVから各タグを読み込んだ時、
			　実行する処理を記述した関数を作成、実行処理も関数内へ記述。（１タグに１関数）

			�BCSVからタグを一つ取得
			�Cタグから、該当する処理関数を呼び込み、処理実行
			�D�Bの時に同時に読み込んだ、次のタグを読み込むまでの時間まで待機（処理によっては、左記時間を遷移時間として処理を行うものもある。（左記時間を使って画像のα値を０にするなど））
			�E�Dの待機が終了したら�Bへ戻る

*/

//クラスのプロトタイプ宣言
class CountTimer;
class CsvReader;


//ウィニングシーンにおけるUIGroup（UI群）のタイプ
	//詳細：ウィニングシーン内にて管理する常駐のUIGroup
	//　　：外部からUIGroupの登録する際に使用する
enum class WINNING_SCENE_UI_GROUP_TYPE
{
	WINNING_UI_GROUP_WHITE = 0,	//白背景画像グループ
	WINNING_UI_GROUP_CLEAR ,	//クリアロゴグループ

	WINNING_UI_GROUP_MAX,		//MAX
};



//ウィニングシーンにおける連携や仲介が必要なオブジェクトクラス
	//詳細：連携を行う際にアクセス先のオブジェクトクラス
	//　　：外部からオブジェクトの登録する際に使用する
enum class WINNING_SCENE_COOP_OBJECTS
{
	WINNING_COOP_GRASS_LAND = 0,	//草地面オブジェクト
	WINNING_COOP_ENEMY_SPAWN,		//敵スポーン
	WINNING_COOP_BACK_GROUND_TREE ,	//背景木
	WINNING_COOP_PLAYER,			//プレイヤー	
	WINNING_COOP_PLAYER_OWNED_TREE,	//プレイヤーが所有する木

	WINNING_COOP_MAX ,				//MAX

};

//タグ一覧
	//詳細：CSVファイルにて読み込み、処理を識別するためのタグ
enum class WINNING_ANIMATION_TAG
{
	WINNING_TAG_CHANGE_SCENE = 0,			//シーン切り替え
	
	//WINNING_TAG_CHANGE_GREEN_POLYGON ,		//指定ポリゴンを緑ポリゴンへ
	WINNING_TAG_ALL_CHANGE_GREEN_POLYGON,	//全ポリゴンを緑ポリゴンへ
	WINNING_TAG_ALL_CHANGE_SPECIFIED_TIME,	//全ポリゴンを指定時間かけて、緑ポリゴンへ変化させる

	WINNING_TAG_CREATE_BACKGROUND_TREE,		//背景木作成
	//WINNING_TAG_EXECUTION_LSYSTEM,			//プレイヤー所有木のLSYSTEM実行
	WINNING_TAG_START_PAINT_ANIMATION,		//ポリゴンを塗るアニメーションの開始（是面ポリゴンを塗る）
	WINNING_TAG_SET_RANDOM_POS_ALL_ENEMY,	//全ての敵の位置をランダム位置にセット

	WINNING_TAG_SET_CAMERA,					//カメラセット（任意位置にセット）
	WINNING_TAG_MOVE_CAMERA,				//カメラ移動（任意量移動）
	WINNING_TAG_MOVE_CAMERA_ROTATE_Y,		//カメラY回転（任意量回転）
	WINNING_TAG_MOVE_CAMERA_ROTATE_X,		//カメラX回転（任意量回転）
	//WINNING_TAG_ROTATE_CAMERA,				//カメラ回転（任意回転値セット）
	WINNING_TAG_SET_CAMERA_OF_THE_OBJ,		//任意オブジェクト位置にカメラをセット
	WINNING_TAG_SET_CAMERA_OF_THE_FPS,		//カメラをFPS視点にセット（プレイヤーとなるオブジェクト視点へ）
	WINNING_TAG_MOVE_CAMERA_ROTATE_OF_THE_OBJ,	//現在のカメラ視点から、任意オブジェクトに向けてカメラを回転する
	WINNING_TAG_MOVE_TO_SCAN_GROUND_POLYGON,	//地面ポリゴンの始点から終点に向けて移動の開始

	WINNING_TAG_CREATE_INSTANCE,			//任意オブジェクトインスタンス生成
	//WINNING_TAG_ENTER_OBJECT,				//任意オブジェクトのUpdate再開
	//WINNING_TAG_LEAVE_OBJECT,				//任意オブジェクトのUpdate停止
	//WINNING_TAG_SET_TRANSFORM_OBJECT,		//任意オブジェクトのTransformをセットする
	//WINNING_TAG_TRANSFORM_OBJECT_ALPHA,		//任意オブジェクトのα値をセット
	//WINNING_TAG_DELETE_OBJECT,				//任意オブジェクトを削除

	WINNING_TAG_LOAD_IMAGE,					//画像のロード
	WINNING_TAG_VISIBLE_IMAGE,				//画像の表示
	WINNING_TAG_INVISIBLE_IMAGE,			//画像の非表示
	WINNING_TAG_SUBTRACTION_IMAGE_ALPHA,	//画像のα値減衰
	WINNING_TAG_ADDITION_IMAGE_ALPHA,		//画像のα値増加

	WINNING_TAG_LOAD_AUDIO,					//音楽ロード
	WINNING_TAG_PLAY_AUDIO,					//音楽の再生
	WINNING_TAG_STOP_AUDIO,					//音楽の停止
	WINNING_TAG_LOOP_AUDIO,					//音楽のループ

	WINNING_TAG_PAINT_POLYGON_ANIMATION,	//地面ポリゴンの始点（０，０）にGrassの作成、拡張の開始
	WINNING_TAG_START_ERASE_MOTION_ENEMY,	//敵オブジェクトンの消去モーション開始
	WINNING_TAG_ADD_ENEMY_EFFECT,			//敵オブジェクトにエフェクト追加


	WINNING_TAG_MAX,						//MAX


};

//タグの種類を識別する
	//詳細：タグ読み込みのCSVの列を識別する
enum class TAGS
{
	MAIN_TAG = 0,	//Main
	MAIN_TAG_2 ,	//Main2
	SUB_TAG,		//Sub

	MAX_TAG,		//Max
};

//タグによる動的処理のタイマーの種別
	//詳細：タグによる動的処理（カメラの任意秒数での移動など）、アニメーションを行う際に、
	//　　：タイマーの種類、行動を識別するタイプ
enum class TIMER_TYPE
{
	TAG_TIMER = 0,			//タグ読み込み用
	CAM_MOVE_POS_TIMER ,	//カメラ移動
	CAM_MOVE_ROTATE_TIMER,	//カメラ回転
	//CAM_MOVE_TARGET_TIMER,	//カメラ焦点移動
	TRANSFORM_IMAGE_ALPHA_TIMER,	//画像　A値変動	
	DIAGONAL_POLYGON_PAINT_ANIMATION_TIMER,		//地面ポリゴンを斜めに染めていく
												//カメラのルート通りに、地面の始点（０，０）から、終点（最終ポリゴン）までのルートへのポリゴンを染めていく
	PAINTED_ALL_POLYGON_TIMER,		//全ポリゴンを時間で塗る

	MAX_TIMER,						//MAX

};


/*
	クラス詳細：ウィニングシーンのマネージャークラス（シーン内オブジェクトとの橋渡しオブジェクト）
	クラスレベル：サブクラス（スーパークラス（SceneManagerParent））
	使用クラス　：WinningScene
	クラス概要（詳しく）
				：マネージャークラス
				：シーン内の常駐UIGroupを登録し、表示、非表示
				：シーンの各オブジェクトの連携（橋渡し）を行うクラス
				：詳しくは、SceneManagerParentを参照

				：勝利演出を行うための演出の流れをCSVとして取得する。
				　読み込んだCSVからタグとなる文字列を取得して、タグごとの必要処理を実行

*/
class WinningSceneManager : public SceneManagerParent
{
//private メンバ定数　構造体
private : 
	//インスタンスオブジェクト情報（タグ読み込みにより生成されるオブジェクト数）
	static constexpr unsigned int MAX_MODELS = 8;
	/*
		構造体詳細	：インスタンス生成用のオブジェクト　情報群
		構造体概要（詳しく）
			：オブジェクト生成のために必要な情報群
	*/
	struct SceneInstanceObjectInfo
	{
		//インスタンスを生成するオブジェクトの名前
		std::string instanceObjectNames;
		//インスタンスを生成するオブジェクトのFbxモデルのファイル名
		std::string instanceFileNames;

		SceneInstanceObjectInfo(std::string objName , std::string fileName)
			:instanceObjectNames(objName),
			instanceFileNames(fileName)
		{};
	};
	//タグにて生成される可能性のある　インスタンスオブジェクト群
		//詳細：オブジェクトの名前を持たせておくことで、タグにて生成が呼び出されたとき、下記情報群との比較で、モデルのロードを行う
	SceneInstanceObjectInfo* instanceObjectInfos_;

	//音楽情報（タグ読み込みにより読み込まれる音楽数）
	static constexpr unsigned int MAX_AUDIOS = 3;
	/*
		構造体詳細	：ロード用音楽情報群
		構造体概要（詳しく）
			：音楽ロードのために必要な情報群
	*/	
	struct SceneAudioInfo
	{
		//ハンドル番号
		int hAudio;
		//生成するAudioの名前
		std::string audioName;
		//Audioのファイル名
		std::string audioFileName;

		SceneAudioInfo(std::string name, std::string fileName)
			:hAudio(-1),
			audioName(name),
			audioFileName(fileName)
		{};
	};
	//タグにてロードされる可能性のある　音楽ファイル群
		//詳細：音楽ファイルの名前を持たせておくことで、タグにてロードが呼び出されたとき、下記情報群との比較で、音楽のロードを行う
	SceneAudioInfo* audioInfos_;

	//画像情報（タグ読み込みにより読み込まれる画像数）
	static constexpr unsigned int MAX_IMAGES = (unsigned int)WINNING_SCENE_UI_GROUP_TYPE::WINNING_UI_GROUP_MAX;
	/*
		構造体詳細	：ロード用画像情報群
		構造体概要（詳しく）
			：画像ロードのために必要な情報群
	*/
	struct SceneImageInfo
	{
		//画像の名前
		std::string imageName;
		//画像のファイル名
		std::string imageFileName;

		SceneImageInfo(std::string name, std::string fileName)
			:imageName(name),
			imageFileName(fileName)
		{};
	};
	//タグにてロードされる可能性のある　画像ファイル群
		//詳細：画像ファイルの名前を持たせておくことで、タグにてロードが呼び出されたとき、下記情報群との比較で、画像のロードを行う
	SceneImageInfo* imageInfos_;

	/*
		構造体詳細	：現在動的行動中の処理の情報群　
		構造体概要（詳しく）
			：動的処理実行のための情報格納構造体
			：情報を格納することで、動的処理のための利用する動的処理対象の確定などに用いる
	*/
	struct CurrentTargetInfo
	{
		//現在行動するタイマーの種類
		TIMER_TYPE timerType;

		//ターゲット番号
			//モデルや、画像などのターゲットを示す番号
			//Infoなどの配列の添え字番号
		int num;

		//タイマークラスごとの移動先のベクターなどを格納しておく変数
			//移動の場合：目標ベクトル
			//回転の場合：回転度数
			//画像値A変動：なし
		XMVECTOR targetPos;
		//開始位置
		XMVECTOR startPos;

		//時間計測クラス
			//自身構造体限定、専用のタイマークラス
		CountTimer* pCountTimer;

		//コンストラクタ
		CurrentTargetInfo()
			:timerType(TIMER_TYPE::MAX_TIMER),
			num(-1),
			pCountTimer(nullptr),
			targetPos(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
			startPos(XMVectorSet(0.f, 0.f, 0.f, 0.f))
		{};
	};
	//現在動的処理実行中の処理群
	std::list<CurrentTargetInfo> listCurrenttargetInfo_;


	/*
	構造体詳細	：読み込むタグ　タグによる実行関数をまとめた構造体
	構造体概要（詳しく）
		：CSVにて読み取るタグ（文字3文字）の登録と、タグを読み込んだ際の実行関数を確保
	*/
	struct TagList
	{
		//タグ文字列
		std::string tag;
		//タグ読み込み時の実行関数へのポインタ
		void(WinningSceneManager::* pFunction)(int x, int y);
	};

	//タグリスト
		//詳細：読み込む予定のあるタグリストを登録しておき、
		//　　：タグが読み込まれたときの比較を行う
	TagList* pTagList_;

//priavte メンバ変数、ポインタ、配列
private :
	//シーン終了のフラグ
		//シーン終了のフラグが立ったら、タグ読み込みを中止する
	bool endScene_;
	//現在参照しているタグの番号
		//0オリジンから、タグを読み込んだ数
		//Csvのセルをを示す際に、Y座標のセル値として使用する
	int currentTagNumber_;

	//時間計測クラス
	CountTimer* pCountTimer_;

	//CSVファイルリーダー	
		//詳細：処理順タグが書かれたCSV読み取りクラス
	CsvReader* pCsvReader_;

//private メソッド
private : 

	//タグの更新処理
	void UpdateTags();

	//タイマーの更新処理
	void UpdateTimers();


	/**タイマーによる行動関数
		 タイマーによる動的処理の実行関数************************************************************/
	
	//時間計測による動的な　カメラの移動
		//引数：動的処理に必要な情報群
		//戻値：動的処理が終了したか
	bool TimerAlgorithmMoveCameraPostion(CurrentTargetInfo& targetInfo);
	//時間計測による動的な　カメラの回転
		//引数：動的処理に必要な情報群
		//戻値：動的処理が終了したか
	bool TimerAlgorithmMoveCameraRotate(CurrentTargetInfo& targetInfo);
	/*
	//時間計測による動的な　カメラの焦点移動
		//引数：動的処理に必要な情報群
		//戻値：動的処理が終了したか
	bool TimerAlgorithmMoveCameraTarget(CurrentTargetInfo& targetInfo);
	*/
	//時間計測による動的な　画像のα値設定
		//引数：動的処理に必要な情報群
		//戻値：動的処理が終了したか
	bool TimerAlgorithmTransformImageAlpha(CurrentTargetInfo& targetInfo);
	//時間計測による動的な　ポリゴンを塗る
		//引数：動的処理に必要な情報群
		//戻値：動的処理が終了したか
	bool TimerAlgorithmPolygonPaintAnimation(CurrentTargetInfo& targetInfo);
	//時間計測による動的な　全てのポリゴンを塗る
		//引数：動的処理に必要な情報群
		//戻値：動的処理が終了したか
	bool TimerAlgorithmPaintedAllPolygon(CurrentTargetInfo& targetInfo);

	//タイマーの要素を初期化とともに追加
		//引数：タイマータイプ
		//引数：終了時間
		//引数：スタート位置
		//引数：対象位置
		//引数：対象番号（オブジェクトの場合、オブジェクトへのハンドル（添え字））
		//戻値：なし
	void PushBackTimerInfo(TIMER_TYPE timerType, float endTime, XMVECTOR startPos, XMVECTOR targetPos, int num = -1);





//public ゲームループメソッド
public:
	//コンストラクタ
		//詳細：コンストラクタ　引数ありのGameObjectコンストラクタを呼び込む（名前設定）	
		//引数：親オブジェクト（GameObject）
		//戻値：なし
	WinningSceneManager(GameObject* parent);

	//デストラクタ
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	~WinningSceneManager() override;

	//初期化
		//詳細：ゲームオブジェクトのインスタン生成時、必ず呼ばれるオブジェクト初期化関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Initialize() override;

	//更新
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト更新関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Update() override;

	//描画
		//詳細：親が存在しているとき、ゲームループ時、必ず呼ばれるオブジェクト描画関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Draw() override;

	//解放
		//詳細：自身の解放時、必ず呼ばれるオブジェクト解放関数
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	void Release() override;


	//UIGroupの個数を取得
		//詳細：自身の所有しているUIGroup群（ppUIGroups_）内のUIGroupの個数を取得
		//  　：○○_SCENE_UI_GROUP_TYPE::○○_UI_GROUP_MAX(enum)を返す。（○○　＝　各シーン名）
		//レベル：オーバーライド
		//引数：なし
		//戻値：なし
	int GetUIGroupCount() override;

//private 
private:
	/*タグの初期化***************************************************************/

	//タグの初期化（pTagList_）
		//引数：なし
		//戻値：なし
	void InitTag();
	//インスタンスオブジェクト情報の初期化（instanceObjectInfos_）
		//引数：なし
		//戻値：なし
	void InitInstanceObjectString();
	//音楽情報の初期化（audioInfos_）
		//引数：なし
		//戻値：なし
	void InitAudio();
	//画像情報の初期化（imageInfos_）
		//引数：なし
		//戻値：なし
	void InitImage();


	/*タグ実行***********************************************************************/

	//タグを読み込み　タグによる行動実行
		//詳細：読み込んだタグから、行動処理関数を判別し、関数を呼び出す
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void ExecutionFunction(int x , int y);


	//引数の座標から示される、タグをから、実行処理関数を識別するEnum値を返す
		//詳細：ＣＳＶを読み込み、タグ文字列を読み込む。タグ文字列から該当のタグ識別子を返す
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：タグ識別子(WINNING_ANIMATION_TAG(enum))
	WINNING_ANIMATION_TAG GetAnimationTag(int x , int y);

	/*タグ処理内にて使用する処理メソッド********************************************/

	//ポリゴン番号のワールド座標を取得する
		//詳細：地面ポリゴンのポリゴン番号を取得し、そのポリゴン番号のワールド座標を取得する
		//引数：ワールド座標を入れる座標ポインタ
		//引数：ポリゴン番号
		//戻値：取得できた（取得できた：true , 取得できない：false）
	bool GetWorldPosOfPolygonMakeGround(XMVECTOR* localToWorld, int polyNum);
	
	//指定ポリゴン番号のローカル座標を取得
		//詳細：ポリゴンのローカル座標　３頂点の中心座標
		//引数：ローカル座標を入れるポインタ
		//引数：ポリゴン番号
		//戻値：取得できた（取得できた：true , 取得できない：false）
	bool GetLocalPosOfPolygonMakeGround(XMVECTOR* localPos, int polyNum);
	//ローカル座標を該当Transformにてワールド座標へ変形
		//引数：ワールド座標
		//引数：変形に使用するTransform 
		//戻値：なし
	void TransformedInTheWorldMatrix(XMVECTOR* toPos, Transform& trans);
	//Treeオブジェクトを作成するにあたってTransformを取得する
		//引数：ワールド座標
		//引数：初期サイズ
	Transform GetTreeTransform(XMVECTOR& localToWorld, float initSize);

	//タグセル値から座標を取得する
		//詳細：タグのセル　セルに格納されているベクトル値（文字列）　その文字列をXMVECTOR型に変換
		//　　：指定セル値にアクセスして、そこに書かれている、座標（ｘｙｚ）を取得し、XMVECTOR型に変換して返す（MainTag２，SubTag）
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：ベクトル
	XMVECTOR GetVector(int x, int y);


	/*タグごとの処理関数**********************************************************/
	
	/*シーン********************************************************/
	//シーンを切り替える
		//タグ：[scs]
		//詳細：タイトルシーンへ
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void ChangeScene(int x, int y);


	/*ポリゴン操作**************************************************/
	/*
	//指定ポリゴンを緑ポリゴンへ
		//タグ：[pgo]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void ChangeGreenPolygon(int x , int y);
	*/
	//指定ポリゴンを緑ポリゴンへ
		//引数：ポリゴン番号
		//戻値：なし
	void ChangeGreenPolygon(int polyNum);


	//全ポリゴンを緑ポリゴンへ
		//タグ：[pga]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし	
	void AllChangeGreenPolygon(int x, int y);

	//指定時間で全ポリゴンを緑ポリゴンへ
		//タグ：[pgt]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし	
	void AllChangeGreenPolygonTheSpecifiedTime(int x , int y);

	/*カメラ系*******************************************************/
	
	//カメラ位置のセット
		//タグ：[cms]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void SetCameraPosition(int x , int y);

	//カメラ位置の移動（一定秒数を掛けて移動）
		//詳細：カメラのナビゲーション
		//タグ：[cmm]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void MoveCameraPosition(int x, int y);
	//カメラ移動を動的処理として登録
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void MoveCameraPosition(XMVECTOR startPos, XMVECTOR targetPos , int y);
	//カメラの回転（一定秒数をかけて回転）
		//タグ：[cry]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void MoveCameraRotateY(int x, int y);
	//カメラの回転（一定秒数をかけて回転）
		//タグ：[crx]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void MoveCameraRotateX(int x, int y);

	/*
	//カメラ回転
		//タグ：[crp]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void RotateCameraPosition(int x, int y);
	*/

	//指定オブジェクトの座標にカメラを設置
		//タグ：[cmo]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void SetCameraPosOfTheObject(int x, int y);

	//プレイヤーオブジェクトのFPS視点にカメラを設置
		//タグ：[fps]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void SetCameraPosOfThePlayer(int x, int y);

	/*
	//指定オブジェクトへ向けてカメラの移動開始
	//タグ：[cmg]
	//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
	//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
	//戻値：なし
	void MoveCameraPosOfTheObject(int x, int y);
	*/

	//指定オブジェクトへ向けてカメラの回転開始 Y
		//タグ：[crg]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void MoveCameraRotateYOfTheObject(int x , int y);
	
	//地面の始点ポリゴンから、終点ポリゴンへのカメラ視点移動を開始
		//タグ：[grm]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void MoveToScanGroundPolygon(int x, int y);


	/*オブジェクトインスタンス　操作系******************************/
	
	//インスタンスクラスごとの特殊関数
	//インスタンスクラスごとのカメラ操作
	
	//カメラをプレイヤー位置（TPS）にセット
		//詳細：カメラをプレイヤー位置に沿わせる。プレイヤーのサイズや視点を具体的に沿わせるためにカメラ位置の微調整も行う
		//引数：なし
		//戻値：なし
	void SetCameraPlayerPos();
	//カメラを敵位置（TPS）にセット
		//詳細：カメラを敵位置に沿わせる。敵のサイズや視点を具体的に沿わせるためにカメラ位置の微調整も行う
		//　　：敵は複数存在するため、敵の登録順、生成順に関数を呼び出すたびに登録順にカメラを沿わせる敵を選択する
		//引数：なし
		//戻値：なし
	void SetCameraEnemyPos();
	//カメラを木位置（TPS）にセット
		//詳細：カメラを木位置に沿わせる。木のサイズや視点を具体的に沿わせるためにカメラ位置の微調整も行う
		//　　：木は複数存在するため、木の登録順、生成順に関数を呼び出すたびに登録順にカメラを沿わせる木を選択する
		//引数：なし
		//戻値：なし
	void SetCameraTreePos();
	//カメラの位置を離れ具合を考慮してセット
		//引数：カメラを沿わせるオブジェクト
		//引数：離れ具合（位置）
		//戻値：なし
	void SetDeparture(GameObject* pObject, XMVECTOR departureVec);
	//カメラの位置を離れ具合を考慮してセット
		//引数：カメラを沿わせるオブジェクト
		//引数：離れ具合（位置）
		//引数：離れ具合（焦点）
		//戻値：なし
	void SetDeparture(GameObject* pObject, XMVECTOR posDepartureVec, XMVECTOR tarDepartureVec);
	
	//インスタンスクラスごとの特殊関数
	//インスタンスクラスごとのインスタンス生成時の他操作の呼び出し

	//インスタンス生成時の特殊行動
		//引数：インスタンス生成されたオブジェクトポインタ
		//戻値：なし
	//敵
	void SpecialOfEnemy(GameObject* pGameObject);
	//地面
	void SpecialOfGround(GameObject* pGameObject);
	//背景
	void SpecialOfSkyBox(GameObject* pGameObject);
	//背景木
	void SpecialOfBackGroundTree(GameObject* pGameObject);
	//プレイヤー
	void SpecialOfPlayer(GameObject* pGameObject);
	//プレイヤー所有木
	void SpecialOfPlayerOwnedTree(GameObject* pGameObject);
	//木
	void SpecialOfTree(GameObject* pGameObject);
	//敵スポーン
	void SpecialOfEnemySpawn(GameObject* pGameObject);


	//インスタンス生成
		//タグ：[ins]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void CreateInstance(int x, int y);

	/*
	//オブジェクトのUpdate実行(Enter)
		//タグ：[obe]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void EnterObject(int x, int y);
	*/
	/*
	//オブジェクトのUpdate非実行(Leave)
		//タグ：[obl]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void LeaveObject(int x, int y);
	*/
	/*
	//オブジェクトのTransformセット
		//タグ：[obs]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void SetTransformObject(int x, int y);

	//オブジェクトのA値変動（一定時間で変動）
		//タグ：[oba]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void TransformObjectAlpha(int x, int y);

	//オブジェクトの削除（KillMe）
		//タグ：[obd]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void DeleteObject(int x, int y);
	*/

	/*画像　操作系***********************************************/

	//画像ロード
		//タグ：[iml]
		//詳細：関数名に、ForWinningがついている理由は、LoadImageという、関数がWindows.hなどに存在していて、そして、それがDefineによる宣言がされているため、スコープでの識別ができない状態になる（同じ関数名を付けると、Defineだと、スコープによる、識別が存在しないので、エラーになる）
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void LoadImageForWinnning(int x, int y);

	//画像描画
		//タグ：[imv]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void VisibleImage(int x, int y);

	//画像非描画
		//タグ：[imi]	
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void InvisibleImage(int x, int y);

	//画像A値変動(一定時間をかけて変動)(増算)(減算)
		//タグ：[ims][ima]
		//詳細：処理の違いは、減算するか、増算するかによる、処理の上で一行しか違う部分が存在しない
		//　　：そのため、関数を分ける必要性は感じない
		//　　：そのため、関数内にて、どちらのタグであるか、識別し、判別できるように、増算　＝　０，減算　＝　１など値を残しておく。
		//　　：上記により、計算の際に、増算、減算の処理を分ける。
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void AddOrSubOfImageAlpha(int x , int y);
	

	/*音楽　操作系**************************************************/

	//音楽のロード
		//タグ：[adl]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void LoadAudio(int x, int y);

	//音楽の再生
		//タグ：[adp]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void PlayAudio(int x, int y);

	//音楽の停止
		//タグ：[ads]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void StopAudio(int x, int y);

	//音楽のループ(Again = Loop(Loopをタグにする場合、lになるが、lはすでに、Loadにて使われている))
		//タグ：[ada]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void LoopAudio(int x, int y);


	/*特殊アルゴリズム　操作系*************************************/
		/*
			行動が決まっているため、（Load〜のように、生成するものをCSVファイルから個別に読み込む必要性がない）
			引数にて、座標を指定を必要としない
			だが、関数を関数ポインタとして配列にまとめるために、引数ありの関数とする

		*/
		
	//地面に沿って、背景の木を生成
		//タグ：[bgt]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void CreateBackGroundTree(int x , int y);
	/*
	//LSYSTEM実行
		//タグ：[lsy]
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void ExecutionLsystem(int x, int y);
	*/

	//敵オブジェクトのランダム配置
		//タグ：[eor]
		//詳細：地面ポリゴンの上に、無理のないように、ランダム配置を行う
		//　　：現在取得している、全敵オブジェクトをランダム配置を行う
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void SetRandomPosAllEnemy(int x , int y);

	//地面のポリゴンを緑ポリゴンにするアニメーションの開始
		//タグ：[poa]
		//詳細：地面ポリゴンの（０，０）ポリゴンから、最終ポリゴンまで緑ポリゴンへするアニメーション開始
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void StartPolygonPaintAnimation(int x, int y);

	//敵の消去モーションを実行
		//タグ：[sem]
		//詳細：呼び込み回数ごとに、スポーンで管理しているオブジェクトを回して、
		//　　：消去モーションを行うオブジェクトを選択する
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void StartEnemyEraseMotion(int x , int y);

	//ダメージエフェクト（敵につけているエフェクト）をプレイヤーへ追加
		//タグ：[aee]
		//詳細：本来は、対象を決めずに、インスタンス作成したオブジェクトにそれぞれアクセスできるように、連携オブジェクトとして取得し、そのオブジェクトへ、消滅エフェクトのゲームオブジェクトを子供として持たせる
		//　　：だが、今回はプレイヤーだけのため、プレイヤー専用の処理とする
		//引数：CSVファイルにおけるX座標（座標が書かれているCSVファイルのセル値）
		//引数：CSVファイルにおけるY座標（座標が書かれているCSVファイルのセル値）
		//戻値：なし
	void AddEnemyEffect(int x , int y);

};



