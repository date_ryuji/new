#include "GameDatas.h"

/*GameDatas*/
namespace GameDatas
{
	/*extern 初期化*/
	XMVECTOR gameBackGroundColor_ = BACKGROUND_COLOR_BLACK;
}


//ゲーム内背景（セッター）
void GameDatas::SetGameBackGroundColor(XMVECTOR gameBackGroundColor)
{
	gameBackGroundColor_ = gameBackGroundColor;
}

//ゲーム内背景（ゲッター）
XMVECTOR GameDatas::GetGameBackGroundColor()
{
	return gameBackGroundColor_;
}
