#pragma once
#include <string>


//シーン（ゲーム内シーン）の種類
	//詳細：シーン切り替え、シーンごとのオブジェクトクラスを切り替える際に使用する
	//　　：シーン切り替え対象のシーンは、以下に追加をすることで、切り替え対象と認識される。
	//　　：シーン切り替えの際に、切り替え先のIDを識別することで、シーン切り替え先を判別する
enum class SCENE_ID
{
	SCENE_ID_TEST = 0,	//テストシーン　仮シーン
	SCENE_ID_SPLASH,	//スプラッシュシーン　企業ロゴ、個人ロゴ、仕様エンジンを表示するシーン
	SCENE_ID_TITLE,		//タイトルシーン	　ゲームタイトルを表示するシーン
	SCENE_ID_GAME,		//ゲームシーン		　ゲームプレイシーン
	SCENE_ID_WINNING,	//ウィニングシーン　　勝利演出シーン
	SCENE_ID_GAMEOVER,  //ゲームオーバーシーン　敗北演出シーン
	SCENE_ID_RESULT,	//リザルトシーン	　リザルト表示シーン


	SCENE_ID_MAX,		//MAX
};



//シーン（ゲーム内シーン）のオブジェクト名
//詳細：シーンを探す際に、ゲームオブジェクトのFindObject関数の引数として与える、オブジェクト名の文字列
//　　：文字列での登録は、できるだけ、行いたくはないが、　SceneChangerにおいて、どうしても、文字列が必要になる場面があるため、所有しておく
//　　：SCENE_ID(enum)に登録したシーンごとに、各シーンを扱うシーンオブジェクトクラスのクラスメイと同様の名前で登録する
static const std::string SCENE_OBJECT_NAME[(int)SCENE_ID::SCENE_ID_MAX] =
{
	"TestScene",
	"SplashScene",
	"TitleScene",
	"GameScene",
	"WinningScene",
	"GameOverScene",
	"ResultScene",
};
/*
	文字列の定数　

	std::string型で、static constexpr(#defineのように、コンパイル時に定数とするもの（普通のconstは、実行時に定数生成される）)を定義するためには、
	constexprにすることで、定数値を、コンパイル時に生成するため、実行時には多少軽くなる
	std::string_view	//https://torutk.hatenablog.jp/entry/2020/08/10/015936	→存在しない（開発環境が、Ｃ＋＋１７以下のため、存在しない）
	staticにすることで、ファイル読み込み時、インクルード時に、毎回、メモリを消費することがなくなる

*/



//シーンマネージャー（ゲーム内シーンをマネジメントするクラス）の種類
	//詳細：各シーンにおけるシーン内に存在する、各オブジェクト連携の橋渡しとなるマネージャークラス
	//　　：各シーンに必ず存在するわけではなく、SCENE_IDと順番、連番が同じとは限らない
	//　　：マネージャーを使用する場合、以下に種類を必ずついかする。マネージャーを作成する際に、特別な方法を使用するため、そのためには、必ず以下の種類に追加されている必要がある
enum class SCENE_MANAGER_ID
{
	SCENE_MANAGER_ID_NONE = 0,	//マネージャーなし　シーンがマネージャーを所持していないときに使用する
	SCENE_MANAGER_ID_SPLASH,	//スプラッシュシーンマネージャー　
	SCENE_MANAGER_ID_TITLE,		//タイトルシーンマネージャー　
	SCENE_MANAGER_ID_GAME,		//ゲームシーンマネージャー　
	SCENE_MANAGER_ID_WINNING,	//ウィニングシーンマネージャー　
	SCENE_MANAGER_ID_GAMEOVER,	//ゲームオーバーシーンマネージャー　
	SCENE_MANAGER_ID_RESULT,	//リザルトシーンマネージャー　

	SCENE_MANAGER_ID_MAX ,		//MAX

};





