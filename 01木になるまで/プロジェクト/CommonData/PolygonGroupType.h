#pragma once

//ゲーム内３Dポリゴン（多角形）　ポリゴンの表示方法の種類
	//詳細：ゲーム内にて表示する3Dポリゴン、Fbxファイルから、平面のポリゴンなど。各ポリゴン事に初期化、表示方法、表示に使うデータが異なる。
	//　　：そのため、種類ごとに処理を分ける際に使用する
enum class POLYGON_GROUP_TYPE
{
	FBX_POLYGON_GROUP = 0,				//FBXモデルポリゴン

	HEIGHT_MAP_POLYGON_GROUP,			//ハイトマップポリゴン　ハイトマップ画像を読み取り、画像のRGB値からポリゴンの高さを決定し、地面を形成する


	HEIGHT_MAP_CREATER_POLYGON_GROUP,	//ハイトマップポリゴン　ハイトマップ画像を作成する

	GRASS_LAND_POLYGON_GROUP,			//ハイトマップポリゴン＋動的UV	シェーダーへ送るUV値を動的に変動し、グラフィックに変化を与え続ける

	BRANCHES_MAKE_TREE_POLYGON_GROUP,	//パーツ類組み合わせポリゴン　パーツとなる枝を組み合わせて一つの大きなポリゴンとする
										//LSYSTEMによって、生成する木オブジェクト（LSYSTEMとは細かいパーツを組み合わせることで、ひとつの木を表現する方法。その際に、パーツ組み合わせのポリゴンを使用する）
										//BrancheesMakeTree＝枝達によって作られる木
	
	BRANCH_POLYGON_GROUP,				//パーツ類組み合わせポリゴンの枝パーツ　パーツとなる枝のポリゴン

	
	PLANE_POLYGON_GROUP,				//平面ポリゴン　厚さを持たないｘｙ上に展開する四角形ポリゴン　


	POLYGON_GROUP_MAX,					//MAX


};






