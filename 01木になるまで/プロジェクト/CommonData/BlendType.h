#pragma once
//ブレンド　（ピクセルの混ぜ具合）の種類
	//詳細：透過を表現できるBlnedタイプ、ピクセル表示の際に、
	//　　：重なったピクセルをどのように表示するのか、表示方法の種類分けを行う際に使用する
enum class BLEND_TYPE
{
	BLEND_ALPHA_FOR_ALPHA = 0,				//標準のα値による透過を行う
	BLEND_SUBTRACT_FOR_GRAVITY_MAGIC,		//色を　減算　

	BLEND_MAX ,								//MAX 


};
