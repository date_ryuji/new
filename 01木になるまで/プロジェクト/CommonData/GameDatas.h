#pragma once
//システム内　標準ヘッダ(usr/)
#include <DirectXMath.h>

//名前空間　スコープ省略
using namespace DirectX;



//ゲーム世界の背景色（ゲーム内背景色）の固定値（RGBA）
//黄緑
#define BACKGROUND_COLOR_LIGHTGREEN XMVectorSet(0.f , 0.5f , 0.f, 1.f)
//黒
#define BACKGROUND_COLOR_BLACK XMVectorSet(0.f , 0.0f , 0.f, 1.f)



//ゲームを作る情報 名前空間
	//詳細：動的に変化する、ゲーム全体における情報を保存しておく領域
	//　　：Direct3Dなどにおけるゲーム更新の際に使用する情報への外部からのアクセスを置く名前空間。
	//　　：これまでは、Direct3Dにおけるゲーム内背景色を変更するために、Direct３Dへアクセスしていた。それでは、情報を与えすぎていたため、改良した。
	//制限：namespaceの実装部分をヘッダに書いてしまうと、多重定義になる（cppをインクルードしたときのようになる）ので注意
namespace GameDatas
{
	/*extern 前方宣言**************************/
	//ゲーム内背景色
	extern XMVECTOR gameBackGroundColor_;

	/*メソッド************************************/
	//ゲーム内背景　セット
		//引数：ゲーム内背景色（セット値）
		//戻値：なし
	void SetGameBackGroundColor(XMVECTOR gameBackGroundColor);
	//ゲーム内背景　ゲット
		//引数：なし
		//戻値：ゲーム内背景色（ゲット値）
	XMVECTOR GetGameBackGroundColor();

}


