#include "ShaderType.h"


//引数シェーダーが２D専用シェーダーであるかの判断を行う
bool ShaderType::Is2DShader(SHADER_TYPE shaderType)
{
	//条件：引数シェーダータイプごと
	switch (shaderType)
	{
	//2Dシェーダーであるか
	case SHADER_TYPE::SHADER_2D:
	case SHADER_TYPE::SHADER_KUROSAWA_MODE_2D:
	case SHADER_TYPE::SHADER_BLURRED_2D:
		return true;
		break;
	//その他
	default:
		return false;
	}

	return false;
};

