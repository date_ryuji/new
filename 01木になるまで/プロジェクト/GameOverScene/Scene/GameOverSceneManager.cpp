#include "GameOverSceneManager.h"				//ヘッダ
#include "../../Engine/GameObject/UIGroup.h"	//シーン内UI画像、ボタンをまとめたUIGroup群
#include "../../Engine/Scene/SceneChanger.h"	//シーン切り替えクラス
#include "../../CommonData/SceneType.h"			//シーン識別ID群
#include "../../GameScene/CountTimer/CountTimer.h"	//時間計測クラス

//コンストラクタ
GameOverSceneManager::GameOverSceneManager(GameObject* parent) :
	SceneManagerParent::SceneManagerParent(parent, "GameOverSceneManager"),
	pCountTimer_(nullptr)
{
}
//デストラクタ
GameOverSceneManager::~GameOverSceneManager()
{
}

//初期化
void GameOverSceneManager::Initialize()
{

	/*シーンマネージャー定義時の必須処理*******************************************************************/
		//詳細：マネージャーとしての機能を持たせるための前提処理、初期化
		//　　：（条件：リソースが存在する場合必須）
		//　　：リソースとは、この場合、連携オブジェクト群における連携オブジェクトが存在するか
		//　　：　　　　　　　　　　　　UIGroup軍におけるUIGroupが存在するか。
		//　　：上記のリソースが一つも存在しない場合に呼び込む必要はない


	//連携オブジェクトなし
	//連携オブジェクト群のポインタを保存しておく配列の初期化、生成
	//継承先である、自身のクラスで宣言した、連携オブジェクトを指定するEnum値をもとに、連携オブジェクトの数を引数として渡す
	//関数先にて、引数分の連携オブジェクトを入れる枠、要素を確保してくれる
	//NewArrayForCoopObjects(RESULT_COOP_MAX);
	
	//UIオブジェクト群のポインタを保存しておく配列の初期化、生成
	NewArrayForUIGroups((int)GAMEOVER_SCENE_UI_GROUP_TYPE::GAMEOVER_UI_GROUP_MAX);

	/****************************************************************************************************/


	//カウントタイマーのインスタンス生成
	pCountTimer_ = (CountTimer*)Instantiate<CountTimer>(this);
	//タイマー（カウントアップ）の開始
	static const float TIMER_END_TIME = 3.f;
	pCountTimer_->StartTimerForCountUp(0.0f, TIMER_END_TIME);

}

//更新
void GameOverSceneManager::Update()
{
	//条件：タイマーが計測を終了をしてしていたら
	if (pCountTimer_->EndOfTimerForCountUp())
	{
		//シーン切り替え
		ChangeScene();
	}

}

//描画
void GameOverSceneManager::Draw()
{
}

//解放
void GameOverSceneManager::Release()
{
	//シーン内連携オブジェクトの解放
		//解放といっても、オブジェクトはGameObject型であるため、Deleteはしない。マネージャークラス内にて管理しているオブジェクトを登録する配列ポインタの解放
	DeleteArrayForCoopObjects();
	//UIオブジェクト群の解放
	DeleteArrayForUIGroups();
}

//シーン管理の常駐UIGroupの個数を返す
int GameOverSceneManager::GetUIGroupCount()
{
	return (int)GAMEOVER_SCENE_UI_GROUP_TYPE::GAMEOVER_UI_GROUP_MAX;
}

//シーン切り替え
void GameOverSceneManager::ChangeScene()
{
	//シーン切り替え
	SceneChanger* pSceneChanger = (SceneChanger*)FindObject("SceneChanger");
	pSceneChanger->ChangeScene(SCENE_ID::SCENE_ID_RESULT);
}
