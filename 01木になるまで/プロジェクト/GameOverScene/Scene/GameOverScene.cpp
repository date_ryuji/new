#include "GameOverScene.h"						//ヘッダ
#include "../../Engine/GameObject/UIGroup.h"	//UIGroup　シーン内UI群
#include "../../Engine/Scene/SceneUIManager.h"	//シーンのUIマネージャー
#include "../../CommonData/SceneType.h"			//シーン識別IDクラス
#include "../../CommonData/GameDatas.h"			//ゲーム内情報定義
#include "GameOverSceneManager.h"				//マネージャークラス


//コンストラクタ
//SceneParentのコンストラクタ呼び込み
	//引数：親オブジェクト
	//引数：シーン名のオブジェクト名（シーン識別情報群(Project/CommonDatas/SceneType.h)よりシーンオブジェクト名を取得する（シーンをID識別しオブジェクト名取得））
GameOverScene::GameOverScene(GameObject* parent)
	: SceneParent::SceneParent(parent, SCENE_OBJECT_NAME[(int)SCENE_ID::SCENE_ID_GAMEOVER])
{
}

//初期化
void GameOverScene::Initialize()
{
	//ゲームの背景色を切り替える
		//背景色の変化
	//背景色取得
	GameDatas::SetGameBackGroundColor(BACKGROUND_COLOR_LIGHTGREEN);

	//ゲームオーバーシーンクラスのマネージャー
		//詳細：SceneParentを継承しているため、
		//　　：継承元関数から、シーンマネージャークラスを作成させる
		//制限　　：シーンマネージャークラスは、GameObject型継承のオブジェクト作成関数である、Instantiateでの作成は推奨されない。
		//制限理由：制限をかけるのは、シーンマネージャーが作成されたことを伝えなくてはいけないクラスが存在するためである。
	GameOverSceneManager* pSceneManager = 
		(GameOverSceneManager*)CreateSceneManager(SCENE_MANAGER_ID::SCENE_MANAGER_ID_GAMEOVER);

	//UI作成
	CreateSceneUIGroups(pSceneManager);


}

//更新
void GameOverScene::Update()
{
}

//描画
void GameOverScene::Draw()
{
}

//解放
void GameOverScene::Release()
{
}

//UIGroupへのUI画像の追加
int GameOverScene::AddStackUI(UIGroup* pUIGroup, const std::string& FILE_NAME)
{
	//引数UIGroupに追加する
	return pUIGroup->AddStackUI(FILE_NAME);
}

//シーン内常駐UIGroupの作成
void GameOverScene::CreateSceneUIGroups(GameOverSceneManager* pSceneManager)
{
	//UIGroupの作成

	//所属ロゴUI群
	{
		//UIGroup作成
		//インスタンス生成
		UIGroup* uiGroup = (UIGroup*)Instantiate<UIGroup>(this);

		//UIGroupをSceneMnagerにセットする
		//SPLASH_UI_GROUP_AFFILIATION
		//所属ロゴUI群
		pSceneManager->AddUIGroup((int)GAMEOVER_SCENE_UI_GROUP_TYPE::GAMEOVER_UI_GROUP_TITLE, uiGroup);

		//UI画像
		{
			
			{
				int hStart = AddStackUI(uiGroup, "Assets/Scene/Image/GameOverScene/GameOverLogo.png");

				//UIManagerを取得
				SceneUIManager* pSceneUIManager = uiGroup->GetSceneUIManager();
			}

		}
		//UIGroup描画
		pSceneManager->VisibleSceneUIGroup((int)GAMEOVER_SCENE_UI_GROUP_TYPE::GAMEOVER_UI_GROUP_TITLE);
	}

}
