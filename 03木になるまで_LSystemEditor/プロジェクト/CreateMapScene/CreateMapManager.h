#pragma once

/*


CreateMapSceneクラスにおける
メニューのすべてを管理するクラス
ダイアログのすべてを管理するクラス


*/

#pragma once
#include <Windows.h>
#include <commctrl.h>	//ダイアログのスクロールバーを操作するための関数、定数群を持つヘッダ
#include "../Engine/GameObject.h"


#pragma comment(lib, "ComCtl32.lib") //ライブラリのリンク







//■■シーンを管理するクラス
class CreateMapManager : public GameObject
{
private:

	//定数値
	//ビルド時に生成される定数値
			//＃defineと同様のもの
			//クラス内でのメンバとして定義して、
			//宣言時にしか、値は確定できないので、ヘッダに値を書くことになる
	//基準となる最高、最低値
	static constexpr int MAX_WIDTH_ = 64;
	static constexpr int MIN_WIDTH_ = 2;
	static constexpr int MAX_DEPTH_ = 64;
	static constexpr int MIN_DEPTH_ = 2;
	static constexpr float MAX_SIZE_ = 2.0f;
	static constexpr float MIN_SIZE_ = 0.01f;

	//以下　static constexprを使用している理由
	//値が絶対に変わらないのであれば、
		//const よりも、　static constexpr（constexpr static）を使用する方が良い↓
	////基準となる最高、最低値
	//const int MAX_WIDTH_;
	//const int MIN_WIDTH_;
	//const int MAX_DEPTH_;
	//const int MIN_DEPTH_;
	//const float MAX_SIZE_;
	//const float MIN_SIZE_;


	//クラス内のstatic 
		//staticにすることで、
		//クラス内で共通して値を持つことができる。
		//つまり、　共通したメモリを持つことになるので、メモリをクラスごとに確保しない
	//かつ、

	//static const expres という#defineと同じような定数がある
		//これは、ビルド時に値が確定するものなので、
		//普通のstatic const は、実行時に値が確定する。
		//値が変わらない定数であっても、生成されるタイミングが違う。
	//なので、
		//絶対に値が変わらないものであれば、
		//static const expresを使用する。（ビルド時に確定するので、絶対に変わらない）
	//そうすることで、
		//ビルドには少し時間がかかるが、
		//実行時には軽くなる。

		//今回のプロジェクトような、実行時に重くなってしまう可能性があるものは、
		//static constexpr
	/*
	#define と　static constexpr の違い
	※ここにおける名前空間のスコープは、クラスのスコープと同等と考える

	�@
	#defineは、自身のクラスに同様の名前のdefineがあった場合、
	それは、後に定義された方に上書きされる

	�A
	�@を防ぐために、名前空間などに、スコープわけをしたとしても、
	名前空間：：　といった、スコープでのアクセスが　defineはできない

	�B
	グローバル部分に、同様の名前が存在したら
	・#defineは、コンパイル時に、上書き処理がされて、エラーが起こらない
	・static constexpr は、　コンパイル時に、再定義としてエラーを起こす

	�C
	スコープにくくった場合、
	・#defienは、同様の定義として、上書きされる
	・static constexpr は、　スコープわけで分類されるので、スコープ：：　とアクセスすれば、別々の値として扱うことが可能

	
	*/
	//さらに、defineと違って、
	//スコープで、宣言することができるので、
		//仮に名前空間があったら、　その名前空間にconstexprで定数宣言して、　それをスコープで、 NameSpa::aaというように、設定できるので、
		//defineと違って、スコープごとの定数とできるので、
		//仮に名前がかぶっても使うことができる。
	//仮に＃Defineだと、
		//仮に名前空間などで囲んでいても、
		//勝手に上書きという形が取られてしまう
		//そして、名前空間に書き込んだものは、スコープで、アクセスすることはできない

	//なので、使いどころとしては、
		//名前空間に、Defineと同じように、ビルド時に生成して、スコープわけの定数を作りたいときに使える



//	static constexpr int aa = 10;
		//cppの中だけで使う定数ならば、
		//cppでも、同様に、static constexpr ~ と書けばいいし、
		//そうすれば、クラスの中で共通のメモリからとられるもので、かつ、ビルド時に生成されて、確定する




	

	//パラメータの保存
	//スクロールの動的可変によって取得できる、０〜１００の値を
	//パラメータでは、ゲーム内で管理したい値に線形補完する
		//スクロール０　→　パラメータ　２
		//スクロール100　→　パラメータ　６４　となるようにいい感じに補完する
	int width_;		//幅
	int depth_;		//高さ
	float size_;	//サイズ




	//現在のパラメータをファイルに保存する
	void ExeCreate();



	//シーンを切り替える
	void ChangeScene();


	//線形補完実行
	int LinearInterpolationInt(const  int MAX ,const  int MIN , float percent);
	float LinearInterpolationFloat(const  float MAX , const  float MIN, float percent);

	void SetDepth(HWND hDlg, const int SLIDER_ID, const int TEXT_ID, int* value, const int MAX, const int MIN);
	void SetSize(HWND hDlg, const int SLIDER_ID, const int TEXT_ID, float* value, const float MAX, const float MIN);

	//引数にて渡された値分
		//引数を階乗として、最低値に掛けて、乗数の値を求める。
		//その値を、メンバ変数のWidth二登録する
	void SetWidth(int factorial);


	//テキスト更新
		//ダイアログのリソースである、テキストボックスの更新を行う
	void UpdateText(HWND hDlg, const int TEXT_ID, const int text);
	void UpdateText(HWND hDlg, const int TEXT_ID, const float text);
	void UpdateText(HWND hDlg, const int TEXT_ID, const std::string text);




public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	CreateMapManager(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

public :
	//偽物
	//ダイアログプロシージャもどき
	//MainCppが本物
	//本物では、この偽物へ、メッセージを飛ばす処理を行う
	//そして、この偽物で、ゲームに影響の与える処理を書くことで、本物に、余計な処理、情報を与えない
	BOOL DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);

	



};

