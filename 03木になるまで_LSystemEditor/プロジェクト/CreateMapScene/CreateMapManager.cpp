#include "CreateMapManager.h"
#include <string>

#include "../Engine/SceneManager.h"
#include "../Engine/TextWriter.h"
#include "../Engine/Global.h"


#include "../resource.h"
#include "../Procedure/DialogProcedure.h"
#include "../Procedure/WindowProcedure.h"





//コンストラクタ
CreateMapManager::CreateMapManager(GameObject * parent)
	: GameObject(parent, "CreateMapManager"),


	//MAX_WIDTH_(64),MIN_WIDTH_(2),
	//MAX_DEPTH_(64), MIN_DEPTH_(2),
	//MAX_SIZE_(2.0f), MIN_SIZE_(0.01f),


	width_(MIN_WIDTH_), depth_(MIN_DEPTH_),size_(MIN_SIZE_)	//仮に順番を下に持ってきたとしても、値を代入されるのは、constの値とかよりも後になる
															//これは、ヘッダにて、
															//const の定数よりも、先に宣言されているので、一番最初に初期化される
	//仮に、順番をconst定数の後ろに持ってくると
	//きちんと、const の後に、初期化が呼ばれた。

	//なので、
	//コンストラクタの宣言部分に連続して、初期化を書くと、
		//書いた順番は関係なしに、
		//ヘッダでの宣言順番に書くことが望ましい
{


}

//初期化
void CreateMapManager::Initialize()
{

	//自身が使用するダイアログの初期化を行う
	DialogProcedure::ShowDialogLeavedItToClass(DIALOG_TYPE_NEW_FILE);




}

//更新
void CreateMapManager::Update()
{

}

//描画
void CreateMapManager::Draw()
{

}

//開放
void CreateMapManager::Release()
{
	//自身が使用するダイアログの非表示を行う
	DialogProcedure::CloseDialogLeavedItToClass(DIALOG_TYPE_NEW_FILE);


}

void CreateMapManager::ExeCreate()
{
	//ファイル作成
		//width_ , depth_ , size_をそれぞれファイルに書き込む
	TextWriter* pTextWriter_ = new TextWriter(DELIMITER_COMMA);
	//文字の追加
	pTextWriter_->PushBackString(std::to_string(width_) , true);
	pTextWriter_->PushBackString(std::to_string(depth_), true);
	pTextWriter_->PushBackString(std::to_string(size_), true);
	//書き込み終了
	pTextWriter_->PushEnd();
	//ファイル出力
	pTextWriter_->FileWriteExcecute("Assets/HeightMap/MapInfo/MapInformation.txt");
	//解放
	SAFE_DELETE(pTextWriter_);


	pTextWriter_ = new TextWriter(DELIMITER_COMMA);
	pTextWriter_->PushBackString(std::to_string(size_), true);
	//書き込み終了
	pTextWriter_->PushEnd();
	//ファイル出力
	pTextWriter_->FileWriteExcecute("Assets/HeightMap/MapEditorResult/PolygonSize.txt");

	SAFE_DELETE(pTextWriter_);








	//シーン切り替え
	ChangeScene();
}

void CreateMapManager::ChangeScene()
{
	//シーン切り替え
	SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
	pSceneManager->ChangeScene(SCENE_ID_MAP_EDITOR);


}

int CreateMapManager::LinearInterpolationInt(const int MAX, const int MIN, float percent)
{
	return (int)LinearInterpolationFloat((float)MAX , (float)MIN , percent);
}

float CreateMapManager::LinearInterpolationFloat(const float MAX, const float MIN, float percent)
{
	float between = MIN + percent * (MAX - MIN);

	return between;

}


BOOL CreateMapManager::DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	//msgに対する
	switch (msg)
	{
		//何かメッセージを受け取ったら、メッセージ全般
	case WM_COMMAND:
	{
		//引数のwpの、LOWORD（wp）にコントロールのIDが入ってくる
		//そのためこのコントロールのIDから、誰に変更が加えられたのか判断。処理を判断する。
		switch (LOWORD(wp))
		{
		case BUTTON_CREATE:
		{
			//作成実行
			ExeCreate();

			//ダイアログの情報を初期に戻す
			{
				//Depth , Size
				//スライダーを初期位置にセットしなおす
				//SendDlgItemMessage(hDlg, SLIDER_WIDTH, TBM_SETPOS, TRUE, 0);
				SendDlgItemMessage(hDlg, SLIDER_DEPTH, TBM_SETPOS, TRUE, 0);
				SendDlgItemMessage(hDlg, SLIDER_POLY_SIZE, TBM_SETPOS, TRUE, 0);

				//テキストの更新
				//消えるときは、
				//変数なども消してしまうので、値自体は変更の必要性がない
				{
					//std::string widthStr = std::to_string(MIN_WIDTH_);
					//SetDlgItemText(hDlg, TEXT_WIDTH, widthStr.c_str());
					//UpdateText(hDlg, TEXT_WIDTH, MIN_WIDTH_);
					UpdateText(hDlg, TEXT_DEPTH, MIN_DEPTH_);
					UpdateText(hDlg, TEXT_POLY_SIZE, MIN_SIZE_);
				}

				//Width
				SendMessage(GetDlgItem(hDlg, COMBO_WIDTH),
					CB_SETCURSEL,
					0, 0);
			}

		}break;
		case COMBO_WIDTH : 
		{
			//コンボボックスの選択番号を取得
				//返り値に一番小さい値として、０が返ってくるので、
				//最小値の値を返り値分だけ回して、その値をWidthとする
				//相すれば、　2の乗数として値を確定できる
			//選択されたコンボボックスの選択番号順を取得

			int num = SendMessage(GetDlgItem(hDlg, COMBO_WIDTH),
				CB_GETCURSEL,
				0, 0);

			//コンボボックスの選択番号を
			//最小値の階乗として用いて
				//widthを求める
			SetWidth(num);

		}break;

		






		////スタティックテキスト
		//	//テキスト内容の更新
		//SetDlgItemText(hDlg, TEXT_WIDTH, "2");
		//SetDlgItemText(hDlg, TEXT_DEPTH, "2");
		//SetDlgItemText(hDlg, TEXT_POLY_SIZE, "0.01");


		////任意の位置にセット
		//SendDlgItemMessage(hDlg, SLIDER_DEPTH, TBM_SETPOS, TRUE, 2);




		}
	}break;

	///ダイアログが再表示されたとき
	case WM_INITMENUPOPUP:
	{
		




	}break;

	//スクロールのバーが触られたとき
	case WM_HSCROLL:
	case WM_VSCROLL:
		switch (LOWORD(wp))
		{
		case SLIDER_DEPTH:
		{
			//現在の位置を取得	
					// 0 ~ 100
				//int を /100 して、0.00 ~ 1.00fのfloat型にする
					//ここで、 / 0になる可能性があるため、　*0.01fで代用
			float per = SendDlgItemMessage(hDlg, SLIDER_WIDTH, TBM_GETPOS, 0, 0) * 0.01f;

			//割合を使って、
				//Widthの最高と最低とで、線形補完をして、
				//最高と最低を線で繋いだ時に、perの割合の値を求める
			width_ = LinearInterpolationInt(MAX_WIDTH_, MIN_WIDTH_, per);


			std::string str = std::to_string(width_);

			//テキスト表示
				//第3引数にて、文字列のポインタ部分を与えることで、LPCSTR(文字列のポインタ)を通すことが可能
			SetDlgItemText(hDlg, TEXT_WIDTH, str.c_str());

		}break;







		}


		if ((HWND)lp == GetDlgItem(hDlg, SLIDER_DEPTH))
		{
			SetDepth(hDlg, SLIDER_DEPTH, TEXT_DEPTH, &depth_, MAX_DEPTH_, MIN_DEPTH_);
		}
	/*	if ((HWND)lp == GetDlgItem(hDlg, SLIDER_WIDTH))
		{
			SetDepth(hDlg, SLIDER_WIDTH, TEXT_WIDTH, &width_, MAX_WIDTH_, MIN_WIDTH_);
		}*/
		if ((HWND)lp == GetDlgItem(hDlg, SLIDER_POLY_SIZE))
		{
			SetSize(hDlg, SLIDER_POLY_SIZE, TEXT_POLY_SIZE, &size_, MAX_SIZE_, MIN_SIZE_);
		}





		break;


		//if (LOWORD(wp) == SLIDER_DEPTH)
		//{
		//	//現在の位置を取得	
		//			// 0 ~ 100
		//		//int を /100 して、0.00 ~ 1.00fのfloat型にする
		//			//ここで、 / 0になる可能性があるため、　*0.01fで代用
		//	float per = SendDlgItemMessage(hDlg, SLIDER_WIDTH, TBM_GETPOS, 0, 0) * 0.01f;

		//	//割合を使って、
		//		//Widthの最高と最低とで、線形補完をして、
		//		//最高と最低を線で繋いだ時に、perの割合の値を求める
		//	width_ = LinearInterpolationInt(MAX_WIDTH_, MIN_WIDTH_, per);


		//	std::string str = std::to_string(width_);

		//	//テキスト表示
		//		//第3引数にて、文字列のポインタ部分を与えることで、LPCSTR(文字列のポインタ)を通すことが可能
		//	SetDlgItemText(hDlg, TEXT_WIDTH, str.c_str());


		//}

		//switch ((HWND)lp)
		//{
		//case GetDlgItem(hDlg, SLIDER_WIDTH):
		//{
		//	//現在の位置を取得	
		//		// 0 ~ 100
		//	//int を /100 して、0.00 ~ 1.00fのfloat型にする
		//		//ここで、 / 0になる可能性があるため、　*0.01fで代用
		//	float per = SendDlgItemMessage(hDlg, SLIDER_WIDTH, TBM_GETPOS, 0, 0) * 0.01f;

		//	//割合を使って、
		//		//Widthの最高と最低とで、線形補完をして、
		//		//最高と最低を線で繋いだ時に、perの割合の値を求める
		//	width_ = LinearInterpolationInt(MAX_WIDTH_, MIN_WIDTH_, per);


		//	std::string str = std::to_string(width_);

		//	//テキスト表示
		//		//第3引数にて、文字列のポインタ部分を与えることで、LPCSTR(文字列のポインタ)を通すことが可能
		//	SetDlgItemText(hDlg, TEXT_WIDTH, str.c_str());




		//	
		//}break;
	
	

	
	}


	return FALSE;
}

void CreateMapManager::SetDepth(HWND hDlg , const int SLIDER_ID , const int TEXT_ID , int* value , const int MAX , const int MIN)
{
	//現在の位置を取得	
					// 0 ~ 100
				//int を /100 して、0.00 ~ 1.00fのfloat型にする
					//ここで、 / 0になる可能性があるため、　*0.01fで代用
	float per = SendDlgItemMessage(hDlg, SLIDER_ID, TBM_GETPOS, 0, 0) * 0.01f;

	//割合を使って、
		//Width(Depth)の最高と最低とで、線形補完をして、
		//最高と最低を線で繋いだ時に、perの割合の値を求める
	(*value) = LinearInterpolationInt(MAX, MIN, per);


	//テキスト更新
	UpdateText(hDlg, TEXT_ID, (*value));

}

void CreateMapManager::SetSize(HWND hDlg, const int SLIDER_ID, const int TEXT_ID, float * value, const float MAX, const float MIN)
{
	//現在の位置を取得	
				// 0 ~ 100
			//int を /100 して、0.00 ~ 1.00fのfloat型にする
				//ここで、 / 0になる可能性があるため、　*0.01fで代用
	float per = SendDlgItemMessage(hDlg, SLIDER_POLY_SIZE, TBM_GETPOS, 0, 0) * 0.01f;

	//割合を使って、
		//Widthの最高と最低とで、線形補完をして、
		//最高と最低を線で繋いだ時に、perの割合の値を求める
	(*value) = LinearInterpolationFloat(MAX_SIZE_, MIN_SIZE_, per);


	//テキスト更新
	UpdateText(hDlg, TEXT_ID, (*value));

}

void CreateMapManager::SetWidth(int factorial)
{
	int width = MIN_WIDTH_;
	for (int i = 0; i < factorial; i++)
	{
		//現在の値に、最小である、2をコンボボックスの選択番号回数分掛けることで、2の乗数の解を出す
		width *= MIN_WIDTH_;
	}
	//保存用のメンバ変数に登録
	width_ = width;

}

void CreateMapManager::UpdateText(HWND hDlg, const int TEXT_ID, const int text)
{
	std::string strText = std::to_string(text);
	UpdateText(hDlg, TEXT_ID, strText);
}

void CreateMapManager::UpdateText(HWND hDlg, const int TEXT_ID, const float text)
{
	std::string strText = std::to_string(text);
	UpdateText(hDlg, TEXT_ID, strText);
}

void CreateMapManager::UpdateText(HWND hDlg, const int TEXT_ID, const std::string text)
{
	SetDlgItemText(hDlg, TEXT_ID, text.c_str());
}

