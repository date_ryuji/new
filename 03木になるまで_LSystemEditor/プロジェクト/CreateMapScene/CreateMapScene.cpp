#include "CreateMapScene.h"

//シーン（メニュー）マネージャー
#include "CreateMapManager.h"



//コンストラクタ
CreateMapScene::CreateMapScene(GameObject * parent)
	: GameObject(parent, "CreateMapScene")
{
}

//初期化
void CreateMapScene::Initialize()
{
	//マネージャークラスの作成
	Instantiate<CreateMapManager>(this);


}

//更新
void CreateMapScene::Update()
{



}

//描画
void CreateMapScene::Draw()
{

}

//開放
void CreateMapScene::Release()
{
}