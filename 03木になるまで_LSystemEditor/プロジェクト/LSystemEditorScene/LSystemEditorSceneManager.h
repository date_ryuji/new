#pragma once

/*


CreateMapSceneクラスにおける
メニューのすべてを管理するクラス
ダイアログのすべてを管理するクラス


*/

#pragma once
#include <Windows.h>
#include <commctrl.h>	//ダイアログのスクロールバーを操作するための関数、定数群を持つヘッダ
#include "../Engine/GameObject.h"


#pragma comment(lib, "ComCtl32.lib") //ライブラリのリンク

//前方宣言
class Tree;


//■■シーンを管理するクラス
class LSystemEditorSceneManager : public GameObject
{
private:

	//テキスト出力文字列
	//パターン文字列
	std::string pattern_;

	//木クラス
		//木の描画、LSYSTEM実行を担う
	Tree* pTree_;



private : 

	//文字列結合
		//引数文字を、パターンに結合
	void AddChar(char c);

	//パターンの初期化
	void ClearPattern();

	//パターンをダイアログのテキストへ描画
	void DrawDialogText(HWND hDlg);



public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	LSystemEditorSceneManager(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

public:
	//偽物
	//ダイアログプロシージャもどき
	//MainCppが本物
	//本物では、この偽物へ、メッセージを飛ばす処理を行う
	//そして、この偽物で、ゲームに影響の与える処理を書くことで、本物に、余計な処理、情報を与えない
	BOOL DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);

	//パターンの作成
	void CreatePatternText();





};

