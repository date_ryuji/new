#pragma once
#include "../Engine/GameObject.h"

//LSYSTEM
#include "LSystemTree.h"

//木のタイプ
enum TREE_TYPES
{
	//通常（範囲を広げるだけ）
	TREE_TYPE_DEFAULT = 0, 
	//ダミー（偽物）
	TREE_TYPE_DUMMY , 
	//バインド（敵拘束）
	TREE_TYPE_BIND , 
	//イーター（敵捕食）
	TREE_TYPE_EATER,

	TREE_TYPE_MAX,

};



//■■シーンを管理するクラス
class Tree : public LSystemTree
{
protected : 

	//自身の木のタイプを保存
		//木のタイプごとに
		//敵に当たった際に、起こす自身の行動を決定する
	TREE_TYPES myTreeType_;


protected :

	//死亡判定
	void IsDead();

	//自身が消去されるかの判定
	void JudgeKillMe();

	//ルールをランダムで選択し返す
	std::string GetRandomRule();

	//引数ファイルパスにて示されたテキストを読み込み
		//ルールを返す
	std::string GetRule(const std::string fileName);

	//テクスチャファイルのパスを取得
	std::string GetTextureFilePath(TREE_TYPES type);
	std::string GetFallenLeavesTextureFilePath(TREE_TYPES type);







public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Tree(GameObject* parent);

	//初期化
	void Initialize() override;
	//木の種類を選択した初期化
	void Initialize(TREE_TYPES type);
	//ルール（LSYSTEMのパターンとなるもの）と木の種類を選択し初期化
	void Initialize(TREE_TYPES type , const std::string& pattern);


	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

public : 
	//消去モーション中
	bool IsErase();

};