#include "Tree.h"
#include "../Engine/CsvReader.h"
#include "../Engine/Global.h"



//コンストラクタ
Tree::Tree(GameObject * parent)
	: LSystemTree::LSystemTree(parent, "Tree")
{
}

//初期化
void Tree::Initialize()
{
}
void Tree::Initialize(TREE_TYPES type)
{
	//自身の木のタイプを設定
	myTreeType_ = type;

	//自身のタイプにおける
	//LSYSTEMの生成ルールと
	//使用テクスチャを選択

	//ルール群
	std::string ruleStr[TREE_TYPE_MAX] =
	{
		GetRandomRule() ,
		GetRule("Assets/Scene/InputText/LSystem/LSystemDummy.txt"),
		GetRule("Assets/Scene/InputText/LSystem/LSystemBind.txt"),
		GetRule("Assets/Scene/InputText/LSystem/LSystemEater.txt"),

	};



	//LSYSTEMのルール
	//const std::string RULE = "F[-F[*F][/ F]]";
	//テクスチャのファイルパス
	const std::string TEXTURE_FILE_NAME = GetTextureFilePath(myTreeType_);
	//ルールタイプをテキストから読み取る
	const std::string RULE = ruleStr[myTreeType_];



	//親のInitialize()
	//引数：LSYSTEMのルール
	//引数：ソースモデルのテクスチャ
	LSystemTree::Initialize(RULE, TEXTURE_FILE_NAME);


	//描画位置を変えているだけで
		//実際には、Transoformを動かさないので、
		//親のTransformから離れ具合を表現しているコライダーの方式ではできない
	//そのため、木を追加時に、
		//新たにコライダーを作成して、
		//その時に、原点からの離れ具合を登録して、コライダーを作る
		//そのため、コライダーが多くなるので注意


	//落ち葉クラスへのテクスチャロード
	this->LoadFallenLeaves(GetFallenLeavesTextureFilePath(myTreeType_));

}
void Tree::Initialize(TREE_TYPES type, const std::string& pattern)
{
	
	//自身の木のタイプを設定
	myTreeType_ = type;


	//LSYSTEMのルール
	//const std::string RULE = "F[-F[*F][/ F]]";
	//テクスチャのファイルパス
	const std::string TEXTURE_FILE_NAME = GetTextureFilePath(myTreeType_);
	//ルールタイプをテキストから読み取る
	const std::string RULE = pattern;


	//親のInitialize()
	//引数：LSYSTEMのルール
	//引数：ソースモデルのテクスチャ
	LSystemTree::Initialize(RULE, TEXTURE_FILE_NAME);


	//描画位置を変えているだけで
		//実際には、Transoformを動かさないので、
		//親のTransformから離れ具合を表現しているコライダーの方式ではできない
	//そのため、木を追加時に、
		//新たにコライダーを作成して、
		//その時に、原点からの離れ具合を登録して、コライダーを作る
		//そのため、コライダーが多くなるので注意


	//落ち葉クラスへのテクスチャロード
	this->LoadFallenLeaves(GetFallenLeavesTextureFilePath(myTreeType_));


}
std::string Tree::GetRandomRule()
{
	//読み取るファイル数
	static constexpr unsigned int FILE_COUNT = 7;

	//ファイルパスのルート
	static const std::string RULE_PATH_ROOT = "Assets/Scene/InputText/LSystem/LSystem";

	//今回の拡張で用いるルールの確定
	//1オリジンの　FILE_COUNTまで
	int random = 1 + rand() % (FILE_COUNT - 1);

	//CsvReaderを使用して、テキスト文字を受け取る
	CsvReader* pCsvReader = new CsvReader;


	//ファイルをロード
	pCsvReader->Load(RULE_PATH_ROOT + std::to_string(random) + ".txt");

	//ルールの文字列を受け取る
	const std::string RULE = pCsvReader->GetString(0, 0);

	//解放
	SAFE_DELETE(pCsvReader);

	return RULE;
}

std::string Tree::GetRule(const std::string fileName)
{
	//CsvReaderを使用して、テキスト文字を受け取る
	CsvReader* pCsvReader = new CsvReader;


	//ファイルをロード
	pCsvReader->Load(fileName);

	//ルールの文字列を受け取る
	const std::string RULE = pCsvReader->GetString(0, 0);

	//解放
	SAFE_DELETE(pCsvReader);

	return RULE;
}

std::string Tree::GetTextureFilePath(TREE_TYPES type)
{
	//テクスチャ群
	std::string texStr[TREE_TYPE_MAX] =
	{
		"Assets/Scene/Image/TreeTexture/Tree_Default.png",
		"Assets/Scene/Image/TreeTexture/Tree_Dummy.png",
		"Assets/Scene/Image/TreeTexture/Tree_Bind.png",
		"Assets/Scene/Image/TreeTexture/Tree_Eater.png",

	};

	return texStr[type];

}

std::string Tree::GetFallenLeavesTextureFilePath(TREE_TYPES type)
{
	//落ち葉のテクスチャ群
	std::string fallenLeavesTex[TREE_TYPE_MAX] =
	{
		"Assets/Scene/Image/TreeTexture/Leaf.png",
		"Assets/Scene/Image/TreeTexture/WhiteGreen.png",
		"Assets/Scene/Image/TreeTexture/Orange.png",
		"Assets/Scene/Image/TreeTexture/Red.png",

	};

	return fallenLeavesTex[type];
}

//更新
void Tree::Update()
{
	//親のUpdate()
	LSystemTree::Update();


	if (!(IsStopTime()))
	{
		//消去モーションが立っているか
		if (IsErase())
		{
			//自身が消去されるかの判定
			//消去モーションが終了したか
			JudgeKillMe();
		}
		else
		{
			//死亡判定
			IsDead();

		}
	}
}


void Tree::IsDead()
{
}

void Tree::JudgeKillMe()
{
	//消去モーションが終了したかのフラグを取得する
	if (EndEraseMotion())
	{
		KillMe();
	}

}



//描画
void Tree::Draw()
{

}

//開放
void Tree::Release()
{

	//親のRelease()
	LSystemTree::Release();
}

bool Tree::IsErase()
{
	return false;
}


