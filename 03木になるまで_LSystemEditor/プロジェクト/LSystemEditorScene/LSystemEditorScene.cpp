#include "LSystemEditorScene.h"

//シーンマネージャー
#include "LSystemEditorSceneManager.h"

#include "../Engine/SkyBox.h"




//コンストラクタ
LSystemEditorScene::LSystemEditorScene(GameObject* parent)
	: GameObject(parent, "LSystemEditorScene")
{
}

//初期化
void LSystemEditorScene::Initialize()
{
	//マネージャークラスの作成
	Instantiate<LSystemEditorSceneManager>(this);

	//SkyBox（背景）の生成
	SkyBox* pSkyBox =  (SkyBox*)Instantiate<SkyBox>(this);
	

}

//更新
void LSystemEditorScene::Update()
{



}

//描画
void LSystemEditorScene::Draw()
{

}

//開放
void LSystemEditorScene::Release()
{
}