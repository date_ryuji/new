#pragma once
#include <vector>
#include "../Engine/GameObject.h"
//時間の計測を請け負うクラス
	//Updateを行って、時間を加算する必要がある。
	//自身でUpdateを行って、そのタイミングで加算が出来れば、管理が楽なので、GameObject型を継承する



//■■シーンを管理するクラス
class CountTimer : public GameObject
{
private : 
	//計測に必要な情報構造体
	struct TimerInfo
	{
		bool permit;		//計測停止のフラグ（計測を止めている場合は、時間計測を行わない）//タイマー許可、非許可
		float elpasedTime;
		float remaingTime;
		float endTimeForCountUp;
		float endTimeForCountDown;

		//コンストラクタ
		TimerInfo();
	};

	//複数計測用
	std::vector<TimerInfo*> pTimers_;


	//経過時間（ｓ）
		//毎フレーム経過時間を計測する変数
	//タイマー：カウントアップ
	//float elpasedTime_;

	//残り時間
		//タイマー（カウントダウン）実行時の、残り時間（引数終了時間から、経過時間をカウントダウンした際の、残りのｓ）
	//タイマー：カウントダウン
	//float remaingTime_;



	//目標時間（カウントアップ、カウントダウンのタイマーにおける終了となる時間）
	//タイマー：カウントアップ
	//float endTimeForCountUp_;


	//タイマー：カウントダウン
	//float endTimeForCountDown_;








public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	CountTimer(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

public : 
	//タイマー（カウントダウン）の開始
	//タイマー：カウントダウン
	//引数：開始時間（最大値、開始値）
	//引数：終了時間（最小値、終了値）
	//戻値：ハンドル番号（このクラスにおける、計測タイマーの個数番目（０オリジン））
	int StartTimerForCountDown(float startTime , float endTime);
	//タイマー（カウントアップ）の開始
	//タイマー：カウントアップ
	//引数：開始時間（最小値、開始値）
	//引数：終了時間（最大値、終了値）
	//戻値：ハンドル番号（このクラスにおける、計測タイマーの個数番目（０オリジン））
	int StartTimerForCountUp(float startTime, float endTime);
	//計測しなおし
		//すでに確保している、タイマーを使い、計測を１から再開する
		//そのため、すでにあるタイマーにアクセスするために、ハンドル番号を第一引数にセットする
	//タイマー：カウントダウン
	//引数：ハンドル（タイマーの番号）
	//引数：開始時間（最小値、開始値）
	//引数：終了時間（最大値、終了値）
	void ReStartTimerForCountDown(int handle , float startTime, float endTime);
	//計測しなおし
		//すでに確保している、タイマーを使い、計測を１から再開する
		//そのため、すでにあるタイマーにアクセスするために、ハンドル番号を第一引数にセットする
	//タイマー：カウントアップ
	//引数：ハンドル（タイマーの番号）
	//引数：開始時間（最小値、開始値）
	//引数：終了時間（最大値、終了値）
	void ReStartTimerForCountUp(int handle, float startTime, float endTime);


	//現在の残り時間を取得
	//タイマー：カウントダウン
	//引数：ハンドル（指定なし＝０）
	//戻値：残り時間
	float GetRemaingTimer(int handle = 0);
	//現在の終了時間を取得
	//タイマー：カウントダウン
	//引数：ハンドル（指定なし＝０）
	//戻値：終了時間
	float GetEndTimerForCountDown(int handle = 0);
	//現在の経過時間を取得
	//タイマー：カウントアップ
	//戻値：経過時間
	float GetElpasedTime(int handle = 0);
	//現在の終了時間を取得
	//タイマー：カウントアップ
	//戻値：終了時間
	float GetEndTimerForCountUp(int handle = 0);


	//タイマーの計測の終了したかを判断
	//タイマー：カウントダウン
		//”残り時間”　が　”カウントダウン終了時間”　以下である
	//戻値：計測終了かのフラグ
	bool EndOfTimerForCountDown(int handle = 0);

	//タイマーの計測の終了したかを判断
	//タイマー：カウントアップ
		//”現在時間”　が　”カウントアップ終了時間”　以上である
	//戻値：計測終了かのフラグ
	bool EndOfTimerForCountUp(int handle = 0);


	

	//タイマーの計測禁止
	//タイマーのストップ
		//現在の計測の時間を残したまま、タイマーを停止する
	void NotPermittedTimer(int handle = 0);
	//タイマーの計測許可
	//タイマーの開始(ストップしたものを再開)
	void PermitTimer(int handle = 0);
	//タイマーがストップされているか
	bool IsPermitTimer(int handle = 0);


	//タイマーリセット
		//メンバ変数をすべてリセット
	void ResetTimer(int handle = 0);






};

