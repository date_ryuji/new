#include "FallenLeaves.h"

//経過時間を取得する
#include "../Engine/Timer.h"
//モデルクラス
#include "../Engine/Model.h"


//コンストラクタ
FallenLeaves::FallenLeaves(GameObject* parent)
	: GameObject(parent, "FallenLeaves"),
	hModel_(-1),
	totalElpasedTime_(0.f),

	isStopTime_(false)
{
	drawPoses_.clear();
}

//初期化
void FallenLeaves::Initialize()
{





}

void FallenLeaves::Load(const std::string& textureFileName)
{
	//落ち葉の画像となる画像ファイルパスを外部から指定して、
		//落ち葉のグラフィックを固定にしない


	//描画先となる平面ポリゴンのロード
	//引数：平面に張るテクスチャのファイルパス
	//引数：ポリゴンタイプ（平面）
	//引数：シェーダータイプ（平面）
	hModel_ = Model::Load(textureFileName , PLANE_POLYGON_GROUP , SHADER_PLANE);
	//ロードが出来たか
	assert(hModel_ != -1);

}

//更新
void FallenLeaves::Update()
{
	//時間停止されていない場合
	if (!(IsStopTimer()))
	{

		//新しい落ち葉を作成
		CanAddNewFallenLeave();

		//落下処理
		//往復処理
		FallenAndRound();

	}


}


//描画
void FallenLeaves::Draw()
{

	//全ての描画先を描画
	for (auto itr = drawPoses_.begin(); itr != drawPoses_.end(); itr++)
	{
		//座標セット
			//親のTreeの座標も含めた、ローカル座標として扱いたいため、
			//自身のメンバであるTransformにセット
		transform_.position_ = (*itr).pos;
		transform_.rotate_ = (*itr).rotate;


		Model::SetTransform(hModel_, transform_);

		//描画
		Model::Draw(hModel_);
	}

}

//開放
void FallenLeaves::Release()
{
}



void FallenLeaves::CanAddNewFallenLeave()
{
	//時間を計測し
	//生成間隔を超えていたら、新しい落ち葉の生成

	//フレームの経過時間を計測し、
	//時間を計測する
	totalElpasedTime_ += Timer::GetDelta();

	//生成間隔時間
	static constexpr float CREATE_INTARVAL = 0.5f;

	//生成間隔を超えていたら
	if (totalElpasedTime_ >= CREATE_INTARVAL)
	{
		//生成
		CreateFallenLeave();

		//合計経過時間初期化
		totalElpasedTime_ = 0.f;
	}





}

void FallenLeaves::CreateFallenLeave()
{
	//（Yのランダム値）生成位置高さの一定範囲内
	//（Xのランダム値）生成横幅　　の一定範囲内
	//（Zのランダム値）生成奥行き　の一定範囲内
	//※ローカル座標


	//の位置へランダム生成

	//生成最高値
	//X
	static constexpr float MAX_X = 10.f;
	//Y
	static constexpr float MAX_Y = 15.f;
	//Z
	static constexpr float MAX_Z = 10.f;
	//生成最低値
	//X
	static constexpr float MIN_X = -10.f;
	//Y
	static constexpr float MIN_Y = 10.f;
	//Z
	static constexpr float MIN_Z = -10.f;

	//小数第何位までの乱数を出すかによる
	//上記に掛ける10の倍数
	//100 : 小数第2位までの乱数
	static constexpr int POINT = 100;


	//乱数
	//最小値　＋　乱数生成　＋　（最高値 - 最低値）※最小値から（）個数分生成
	float RAND_X = (int)(MIN_X * POINT) + rand() % (int)((MAX_X * POINT) - (MIN_X * POINT));
	float RAND_Y = (int)(MIN_Y * POINT) + rand() % (int)((MAX_Y * POINT) - (MIN_Y * POINT));
	float RAND_Z = (int)(MIN_Z * POINT) + rand() % (int)((MAX_Z * POINT) - (MIN_Z * POINT));


	//桁を小数に直す
	//0は、割れないので避ける
	if (RAND_X != 0.f)
	{
		RAND_X /= POINT;
	}
	if (RAND_Y != 0.f)
	{
		RAND_Y /= POINT;
	}
	if (RAND_Z != 0.f)
	{
		RAND_Z /= POINT;
	}

	//上記の定数値をもとに生成位置をランダム生成
	//Float値を使ってランダム値を生成することはできないので、
		//小数第何位までの少数の乱数を出したいかによって、最高値、最低値を＊１０の倍数をして、結果を　/10の倍数（＊した値と同様）
	XMVECTOR randomPos = XMVectorSet(RAND_X, RAND_Y, RAND_Z, 0.f);

	//XZ方向生成方向のランダム生成


	

	//登録構造体の準備
	DrawInfo info(randomPos);

	//乱数位置を登録
	drawPoses_.push_back(info);



}

void FallenLeaves::FallenAndRound()
{
	//描画位置群に指定されている
	//描画位置をそれぞれ落下させる


	//落下スピード（Y軸への移動）
	static constexpr float FALLEN_SPEED = 5.f;
	//最高Y
	static constexpr float MAX_Y = 15.f;
	//消去座標（最低Y座標）（落下する際に使用する）
	static constexpr float MIN_Y = 0.f;
	//上記を踏まえた
		//Y座標へのおちるスピード
		//道のり　/　時間　＝　スピード
	static constexpr float SPEED_Y = (MAX_Y - MIN_Y) / FALLEN_SPEED;


	//往復スピード(X,Z軸への移動)
	static constexpr float ROUND_SPEED = 2.f;
	//最高X,Z座標（往復する際に使用する）
	static constexpr float MAX_XZ = 5.f;
	//最低X,Z座標（往復する際に使用する）
	static constexpr float MIN_XZ = -10.f;
	//上記を踏まえた
		//XZ座標へのおちるスピード
		//道のり　/　時間　＝　スピード
	static constexpr float SPEED_XZ = (MAX_XZ - MIN_XZ) / ROUND_SPEED;


	//上記の値を利用し、
	//現在の経過時間による、移動量を求める

	//経過時間
	float elpasedTime = Timer::GetDelta();

	//経過時間＊スピード　＝　移動量
	for (auto itr = drawPoses_.begin(); itr != drawPoses_.end();)
	{
		//座標の計算
		(*itr).pos += XMVectorSet(
			SPEED_XZ * elpasedTime * (*itr).cord.vecX, 
			SPEED_Y * elpasedTime * (*itr).cord.vecY,
			SPEED_XZ * elpasedTime * (*itr).cord.vecZ,
			0.f);

		(*itr).rotate += XMVectorSet(0.1f, 0.2f, 0.5f, 0.f);



		//XZ方向の移動方向を指定
		if ((*itr).pos.vecX <= MIN_XZ)
		{
			//移動方向＋
			(*itr).cord.vecX = 1.f;
		}
		else if ((*itr).pos.vecX >= MAX_XZ)
		{
			//移動方向-
			(*itr).cord.vecX = -1.f;
		}
		if ((*itr).pos.vecZ <= MIN_XZ)
		{
			//移動方向＋
			(*itr).cord.vecZ = 1.f;
		}
		else if ((*itr).pos.vecZ >= MAX_XZ)
		{
			//移動方向-
			(*itr).cord.vecZ = -1.f;
		}


		//消去座標に達していたら、削除
		if ((*itr).pos.vecY <= MIN_Y)
		{
			//削除
			itr = drawPoses_.erase(itr);
		}
		//達していなかったら
		else
		{
			//イテレータを進める
			itr++;
		}

	}

}

bool FallenLeaves::IsStopTimer()
{
	return isStopTime_;
}


void FallenLeaves::StopTimer()
{
	isStopTime_ = true;
}

void FallenLeaves::StartTimer()
{
	isStopTime_ = false;
}
