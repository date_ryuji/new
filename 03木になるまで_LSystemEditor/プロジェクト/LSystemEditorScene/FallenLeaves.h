#pragma once
#include <list>
#include "../Engine/GameObject.h"


//落ち葉クラス
	//動的に落ち葉となる平面ポリゴンを作成し、
	//時間とともに、下へ落下させる
		//その平面を木オブジェクトとともに描画させることで、
		//その木から発生する落ち葉と視覚的に見せる





class FallenLeaves : public GameObject
{
private : 
	//落ち葉の描画に用いる平面ポリゴンのモデル番号
	int hModel_;

	//基準位置
		//落ち葉の描画位置、
		//落ち葉の消去位置などなどを決める際の基準位置
			//だが、この位置は、親が木オブジェクトであるならば、その親の原点からのローカル座標を自身は所有しているため、
			//基準の位置を持たずとも、親からのローカルという時点で基準となる

	struct DrawInfo
	{
		//座標
		XMVECTOR pos;
		//回転値
		XMVECTOR rotate;

		//符号
			//+ : +1.f
			//- : -1.f
		XMVECTOR cord;

		//コンストラクタ
		DrawInfo(XMVECTOR setPos) :
			DrawInfo(setPos , XMVectorSet(0.f,0.f,0.f,0.f),XMVectorSet(1.f, -1.f, 1.f, 0.f))
		{
		}

		DrawInfo(XMVECTOR setPos , XMVECTOR setRotate, XMVECTOR setCord) :
			pos(setPos),
			rotate(setRotate),
			cord(setCord)
		{
		}
	};

	//描画位置群
	std::list<DrawInfo> drawPoses_;

	//時間停止のフラグ
		//他のオブジェクトが止まっているときに、あえて、動き続けるのも、演出としても良いが、
		//それは不自然なので、時間停止を実現する
	bool isStopTime_;


	//合計経過時間
		//新しい落ち葉を生成する際に使用する
	float totalElpasedTime_;




private : 
	void FallenAndRound();

	//タイマーが動いているか
	bool IsStopTimer();

	void CanAddNewFallenLeave();

	void CreateFallenLeave();



public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	FallenLeaves(GameObject* parent);

	//初期化
	void Initialize() override;
	//落ち葉のテクスチャを指定して描画先の平面のロード
	void Load(const std::string& textureFileName);

	//更新
	void Update() override;


	//描画
	void Draw() override;

	//開放
	void Release() override;

public : 
	//時間停止
	void StopTimer();
	//時間計測再開
	void StartTimer();

};
