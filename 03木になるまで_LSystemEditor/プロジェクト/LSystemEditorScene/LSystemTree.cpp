#include "LSystemTree.h"
#include "../Engine/Model.h"
#include "../Engine/Transform.h"


//木オブジェクト
#include "../Engine/BranchesMakeTree.h"
//枝オブジェクト
#include "../Engine/Branch.h"
//落ち葉オブジェクト
#include "FallenLeaves.h"


//自身のクラスにおいて
	//枝オブジェクトを任意の位置にセットし、
	//そのローカル座標を、木オブジェクトに登録することで、
	//３Dポリゴンを３Dポリゴンに結合する処理を請け負う

//1本の木オブジェクトを管理するクラス


//カウントタイマー
#include "CountTimer.h"
//消去モーション専用シェーダー
#include "../Shader/HeightMapEraseMotionShader.h"
//シェーダーインスタンス所持、取得クラス
#include "../Engine/Direct3D.h"





//コンストラクタ
LSystemTree::LSystemTree(GameObject * parent)
	: LSystemTree(parent , "LSystemTree")
{


}

LSystemTree::LSystemTree(GameObject* parent, std::string name)
	: GameObject(parent, name),
	isStopTime_(false),


	pBranchesMakeTree_(nullptr),
	pBranchSource_(nullptr),
	hBranchesMakeTree_(-1),

	isEraseMotion_(false),
	pCountTimer_(nullptr),
	pFallenLeaves_(nullptr),

	lsystemCounter_(0),

	length(-1),
	lastLength(-1),

	hCommonModel_(-1),


	//addVec_(XMVectorSet(0.f, 3.0f,0,0)),
	addVec_(XMVectorSet(0.0f, 6.0f, 0.0f, 0.0f)),
	addBranchVec_(XMVectorSet(0.0f, 3.0f, 0.0f, 0.0f)),
	modelScale_(XMVectorSet(2.0f, 3.0f, 2.0f, 0.0f)),


	pos(0)

{
	CreateRules();

}

void LSystemTree::CreateRules()
{
	/*
	//樹木生成�A
	//ルールの定義
	//ルール:
	//F : 直線に延ばす
	//+ : X軸　45度回転
	//- : X軸　-45度回転
	//* : X軸　90度回転
	// /: X軸　-90度回転
	//[ : 無し
	//] : 無し
	{
		rules[RULE_FIXED] = "F";
		//rules[1] = "F-FF*F[-F]+[/F]";	//12回から重くなる（なので、１０回ぐらいが限界では？）
		//樹木生成�Aにて：右斜め↑に延びて描画される
		//rules[1] = "FF[+F-F-F][-F+F+F]";	//６回から重くなる
		//樹木生成�Aにて：３つ又の槍のような形、右斜めに伸びていく

		//rules[1] = "FF+[+F-F-F]-[-F+F+F]";	//６回から重くなる
		//樹木生成�Aにて：線がクロスされて木感が増す、だが、生成後に横に延びた線が目立つ（なので木の上部がまっすぐな線で整地されたみたいな形に）
	}

	//樹木生成�B
	//[ : 分岐開始
	//] : 分岐終了
	{
		//rules[1] = "FF[+F-F-F][-F+F+F]";	//６回から重くなる
		//樹木生成�Bにて：横に広がらない、縦に延びる

		//rules[1] = "FF[+F][-F]";	
	}
	//樹木生成�C
	//角度４５度
	//＋　＝　+Z４５度回転
	// -　＝　-Z４５度回転
	// *　＝　+X４５度回転
	// /  ＝　-X４５度回転
	//moveVec(0,3,0,0)(移動量)
	{
		//rules[1] = "FF[+F-F-F][-F+F+F]";	//６回から重くなる
		//樹木生成�Cにて：横に広がらない、縦に延びる
		//rules[1] = "FF[+F][-F]";
		//樹木生成�Cにて：木感じは、増したが、中心の幹の部分が極端に太くなってしまう→枝分かれせずに、４５度回転した際に、枝が上に伸びていない状態で、４５度回転された枝を生成しようとするので、前回の枝の部分から少し上部に上がった位置に枝が生成されるようになる。（４５度回転した回転値を行列として移動するベクトルに掛けるので、少し上部に上がる）

		//rules[1] = "F[+F][+F][-F]";
		//樹木生成�Cにて：木というより、直線に延びる、草野のようなデザイン（上部に余計に延びる）

		//rules[1] = "F[+F]F[-F]";
		//樹木生成�Cにて：+X方向から風が吹いてなびいているような草のようなデザインになる


		//rules[1] = "FF[+F-F-F][-F+F+F]";
		//樹木生成�Cにて：文字数が多いために、枝と枝の間が埋まって、葉っぱの様になっている

		//★
		{
			//★
			//rules[1] = "F[-F+F-F][+F-F+F]";
			//樹木生成�Cにて：ボリュームが増えて、木の感じが増した。



			//★
			rules[RULE_B] = "F[-F][+F]";
			//樹木生成�Cにて：木に近い。
			//★
			rules[RULE_B] = "F[-F+F][+F-F]";
			//樹木生成�Cにて：木に近い。左右対称に広がり、緩い三角形のような形で枝が形作られているので、綺麗（ただ、枝の本数が少ないため、木としては物足りない）
		}

		// *  /含む
		//Z方向への傾きも行う
		{
			rules[RULE_B] = "F[-/F][+F][*F]";
			//樹木生成�Cにて：3Dで、立体感が増した（３回で十分）

			//★
			rules[RULE_B] = "F[/F*F][+F][-*F]";


			//rules[RULE_B] = "F[/F][+F[-F]][[F]-*F]";
		}

	}

	{
		//★
		rules[RULE_C] = "F[-/F][+F][*F]";


		//★
		rules[RULE_D] = "F[+F/F-F][*FF-F]";

		//★
		rules[RULE_E] = "F[-F[*F][/F]]";

	}
	

	//今回の拡張で用いるルールの確定
	int max = (int)RULE_END;
	int random = 1 + rand() % (max - 1);

	ruleType_ = (LSystemTree::RULE_TYPE)random;
	//ruleType_ = RULE_E;
	*/

	//仮のルール設定
	//RULE_ = "F[-F[*F][/F]]";


	//置換対象の文字列セット
	REPLACEMENT_TARGET_ = "F";


	//保存用文字列にルールの１番目の要素を代入
	///axiom = rules[RULE_FIXED];
	axiom = REPLACEMENT_TARGET_;

	//置換後の文字列(初期に、ルールの１番目の要素を入れる)
	sentence = axiom;


	//モデル番号配列（ベクター）のクリア
	hModel_.clear();


	//描画させるモデル番号リストのクリア
	drawModel_.clear();

	//トランスフォームの配列（ベクター）のクリア
	modelTransform_.clear();


	//ルールが複数ある場合の初期化
	/*
	//要素数を確保
	//ポインタのポインタなので、行要素をポインタで確保
	rules = new std::string*[ruleCount];

	//forで回して、行内の要素をポインタで取得
	for (int i = 0; i < ruleCount; i++)
	{
	//ポインタで示された行番目の要素に、
	//新たに要素を動的確保
	rules[i] = new std::string[2];
	}
	*/


	/*
	{
	rule1[0] = "A";
	rule1[1] = "AB";
	}

	{
	rule2[0] = "B";
	rule2[1] = "A";

	}
	*/
}


int hBranch;


//初期化
void LSystemTree::Initialize()
{

}

void LSystemTree::Initialize(const std::string& RULE, const std::string& TEXTURE_FILE_NAME)
{
	//文字列置換のルール作成
	//RULE_ = RULE;
	//からの文字列用意
	RULE_ = "";
	//ルールとして登録する前に
		//文字列終わりの\0を抜きにして考える必要がある
		//￥0がついていると、文字列の置換の際に、￥0を省く処理を行っていないため、想定通りに動かなくなる。
	//\0前まで、文字列をコピー
	for (int i = 0; i < RULE.size(); i++)
	{
		//一文字ずつ取得
		char c = RULE[i];

		//\0でないなら
		if (c != '\0')
		{
			//結合
			RULE_ += c;
		}
		//文字列なら
		else
		{
			//終了
			break;
		}
	}



	/*Transform trans;
trans.position_ = XMVectorSet(0, 0, 0, 0);

CreateTheFirstOne(trans);*/

//ソースとなるFbxファイルのファイルパス
	static const std::string fbxFileName = "Assets/Scene/Model/TreeSource/TreeSourceAndLeaves2.fbx";



	////hModel_ = Model::Load(fileName, BRANCH_POLYGON_GROUP , SHADER_HEIGHT_MAP);

	//Treeオブジェクトの生成
	pBranchesMakeTree_ = new BranchesMakeTree;
	//pBranchesMakeTree_->Load("" , SHADER_HEIGHT_MAP);	//ファイルが存在しないものを指すので、　Loadは使えない
	//引数：テクスチャのパス
	//引数：シェーダータイプ
	pBranchesMakeTree_->Initialize(TEXTURE_FILE_NAME, SHADER_HEIGHT_MAP);


	////Branchオブジェクトの生成
	//pBranchSource_ = new Branch;


	//データベースに追加する形に変形
	Model::ModelData modelData;
	modelData.fileName = "BranchesMakeTreeForLSystemTree.class";//ファイル名を他のBranchesMakeTreeと区別するために、BranchesMakeTreeとForだれのための、.class（クラスですよ）（ファイル名は、解放の時に、同様の名前が存在するかで、解放するかを判断する。なので、他とは被らないファイル名をセットする）
	modelData.pPolygonGroup = pBranchesMakeTree_;
	modelData.thisPolygonGroup = BRANCHES_MAKE_TREE_POLYGON_GROUP;
	modelData.thisShader = SHADER_HEIGHT_MAP;
	modelData.transform = transform_;

	//Model.cppのデータベース軍に追加
	hBranchesMakeTree_ = Model::AddModelData(modelData);
	assert(hBranchesMakeTree_ != -1);
	Model::SetTransform(hBranchesMakeTree_, transform_);



	//モデルをロードして、
	//後にポインタをもらう方式にする
	//Branch
	hBranch = Model::Load(fbxFileName, BRANCH_POLYGON_GROUP, SHADER_HEIGHT_MAP);
	assert(hBranch != -1);
	Model::SetTransform(hBranch, transform_);
	//3Dポリゴンのモデルポインターを取得
	pBranchSource_ = (Branch*)Model::GetPolygonGroupPointer(hBranch);




	////テスト
	//	//消去モーションの実行
	//StartEraseMotion();




	////木を作るソース（素材）となるFBXをロード
	////int modelNum = Model::Load("TreeSource/TreeSource2.fbx");
	//hCommonModel_ = Model::Load("TreeSource/TreeSourceAndDoubleLeaf.fbx");
	////きちんとロードできた＝０以上
	////ロードできていない＝-1以上
	//assert(hCommonModel_ > -1);


}

void LSystemTree::CreateTheFirstOne(Transform& trans)
{
	////モデルのTransfromを生成
	//Transform trans;
	//trans.position_ = XMVectorSet(0, 0, 0, 0);

	//引数Transformの
	//親Transformを、オブジェクトクラスの持っているTransformにする
	//trans.pParent_ = &transform_;
		//上記の処理によって、
		//オブジェクトクラスが、可変したら、木オブジェクトも、可変するようにする。



	//トランスフォームを仮のものへコピー
	Transform tranform = trans;
	//前回、直線にのばされた枝のTransformを保存（次回のLSYSTEMにて、枝を生成する位置の参考に使われる。）
	lastTreeTrans = trans;



	//木を一本表示させる
	//★モデル番号をModelcppにて複数個確保管理する
	//CreateTreeSource(trans);
	//★一つのモデルの頂点数を動的に増やし、	
		//モデルの３Dポリゴンを変化させていく
	Join3DPolygon(tranform);




	//XMVECTOR scale = lastTreeTrans.scale_;
	//scale = XMVectorSet(scale.vecX / 2.0f , )




	//一本作るだけなので、ここで前回の最終配置数をカウント
	length++;


	//Transform branchTrans = transform_;
	//branchTrans.scale_ = XMVectorSet(0.5f, 0.5f, 0.5f, 0.0f);

	////枝のサイズを小さくする
	//Model::SetTransform(hBranch, branchTrans);



	//モデルのサイズを縮小
	modelScale_ -= SUB_VEC;

}

void LSystemTree::CreateTheFirstOne()
{
	Transform trans;
	trans.position_ = XMVectorSet(0.f, 0.f, 0.f, 0.f);

	CreateTheFirstOne(trans);

}


#if 0
void LSystemTree::CreateTreeSource(Transform trans)
{
	////木を作るソース（素材）となるFBXをロード
	////int modelNum = Model::Load("TreeSource/TreeSource2.fbx");
	//int modelNum = Model::Load("TreeSource/TreeSourceAndDoubleLeaf.fbx");
	////きちんとロードできた＝０以上
	////ロードできていない＝-1以上
	//assert(modelNum > -1);




	//Vector
	//モデル番号配列に登録、追加
	hModel_.push_back(hCommonModel_);


	//追加した配列の最後の添え字を取得
	//今一番後ろに追加した要素数を取得することで、今追加された配列の要素を取得
	int modelSuffix = (int)hModel_.size() - 1;

	{


		//List
		//毎フレーム描画させるモデル番号の配列に登録された添え字
		//リストに登録(これで、登録されているモデル番号の配列と、Transformの配列で要素がずれなければ、（Vectorなので、途中で外すことは考えない）)
		//この番号でモデル番号と、Transformを取得できる
		//追加
		drawModel_.push_back(modelSuffix);
	}

	{
		//モデルのTransformを設定	
		//新たに作成した,
		//Transformであっても、Draw時には、Transformを送るだけなので、計算などは、送ったDraw側で行ってくれる。

		//引数Transformの
		//親Transformを、オブジェクトクラスの持っているTransformにする
		trans.pParent_ = &transform_;
		//上記の処理によって、
		//オブジェクトクラスが、可変したら、木オブジェクトも、可変するようにする。


		//Vector
		//登録したモデルのTransformを追加
		modelTransform_.push_back(trans);
		modelTransform_[modelSuffix].scale_ = modelScale_;


	}

	//次回の最終配置数をカウント
	lastLength++;

}
#endif



void LSystemTree::ExecutionLsystem()
{
	//LSYSTEM実行回数カウント
	lsystemCounter_++;




	//LSYSTEM試作段階�@
	{
		/*
		//文字列の置換をして、
		//LSYSTEMによる置換を行う
		StringReplacement();
		*/

		//置換された文字列通りに描画
		//ルールにの取って、素材の描画を行う
		/*
			ルール：

			'F'→　描画
			'+'→　＋回転
			'-'→　ー回転
			'['→　保存
			']'→　ポップ

		*/
		//CreateModel();
	}
	//LSYSTEM試作段階�A
	{
		//文字列置換の関数を呼ぶ
			//関数先にて、文字列の置換と、それに伴う、置換を終えてすぐに、素材の登録をする（その関数に飛ぶ）
		//StringReplacement_2();

	}
	//LSYSTEM試作段階�C
	{

		StringReplacement_4();

		CreateModelTransform4();
	}

	/*
	{
		//モデルのTransfromを生成
		Transform trans;
		trans.position_ = XMVectorSet(0, length, 0, 0);

		length += 2;

		//木を一本表示させる
		CreateTreeSource(trans);
	}
	*/

	//モデルのサイズを縮小
	modelScale_ -= SUB_VEC;



}
bool LSystemTree::IsStopTime()
{
	return isStopTime_;
}
void LSystemTree::StopTime()
{
	isStopTime_ = true;

	//タイマーの経過時間の計測を停止する
	if (pCountTimer_ != nullptr)
	{
		pCountTimer_->NotPermittedTimer(0);
	}
	//落ち葉クラスへ伝える
	if (pFallenLeaves_ != nullptr)
	{
		pFallenLeaves_->StopTimer();
	}
}
void LSystemTree::StartTime()
{
	isStopTime_ = false;

	//タイマーの経過時間の計測を再開する
	if (pCountTimer_ != nullptr)
	{
		pCountTimer_->PermitTimer(0);
	}
	//落ち葉クラスへ伝える
	if (pFallenLeaves_ != nullptr)
	{
		pFallenLeaves_->StartTimer();
	}
}


void LSystemTree::Join3DPolygon(Transform & trans)
{
	//引数Transformの
	//親Transformを、オブジェクトクラスの持っているTransformにする
	//trans.pParent_ = &transform_;
		//→引数のTransformは
		//→pBranchSourceの　”モデルの”原点からのローカル座標における行列を取得して、それらから、モデルの頂点を追加する。
			//→そのため、LSYSTEMのモデルのTransoformを親にすると、ワールドにおける座標の変化ができてしまうので、余計な計算をさせないようにする必要がある。




	//上記の処理によって、
	//オブジェクトクラスが、可変したら、木オブジェクトも、可変するようにする。
	trans.scale_ = modelScale_;



	//木オブジェクトの
	//枝オブジェクトの結合を行うクラスの呼び込み
	pBranchesMakeTree_->Join3DPolygon(pBranchSource_, trans);





	//次回の最終配置数をカウント
	lastLength++;

}




//更新
void LSystemTree::Update()
{
	//{
	////キーが１回押されたら、LSYSTEMの文字列置換を１回進ませる
	////かつ
	////上限回数より少ないとき実行
	//if (Input::IsKeyDown(DIK_SPACE) && lsystemCounter_ < LSYSTEM_COUNT_)
	//{
	//	//LSYSTEM実行
	//	ExecutionLsystem();

	//}

	//}



}

//毎フレーム特定値成長する（一定量ずつTransformのScale値をアップする）
//枝を生成したときに、初期スケール値は幼木として短い長さになる。→そこから成長する
	//★特定キーが押されたときに、LSYSTEMで枝の生成（初期スケール値にて）
	//★成長を行うためには、前回の枝のTransform値を持っておかないといけない。（または、アクセスできるようにしておかないといけない。）
	//★初期のスケール値を１．０として持っておき、毎フレーム一定量スケール値＋して、それぞれ前回のTransformのScaleにセット
	//★→スケール値が一定量足して、最大のスケール値に拡大が終了したときに、次のLSYSTEMの実行が可能になる。




//描画
	//毎フレーム描画させるもの
	//毎フレーム描画させる中に描画させるモデル番号（モデルをロードして返された値）を使って描画しないとゲーム内に表示されない
void LSystemTree::Draw()
{
	//３Dポリゴンの頂点数を動的に増やす
	//BranchMakeTreeクラスを用いる場合
	//DrawBranchesMakeTree(transform_);


	//モデルをLSYSTEMで持って、複数のモデルをModelcppに登録する場合
	//DrawBranch();
}

void LSystemTree::DrawBranchesMakeTree(Transform& trans)
{
	//Transformの更新を行う
	//transform_ = trans;

	//消去モーションのフラグが立っているとき
		//シェーダーを切り替える
	if (isEraseMotion_)
	{
		//シェーダーへの追加情報を与える
		//経過時間の取得
		float elpasedTime = pCountTimer_->GetElpasedTime();

		//経過時間をシェーダーに渡す

			//コンスタントバッファ2つ目に代入
		HeightMapEraseMotionShader* pHMEraseMotion = (HeightMapEraseMotionShader*)Direct3D::GetShaderClass(SHADER_HEIGHT_MAP_ERASE_MOTION);

		//コンスタントバッファにセット
		HeightMapEraseMotionShader::CONSTANT_BUFFER_1 cb1;
		cb1.elpasedTime = elpasedTime;

		//セット
		pHMEraseMotion->SetConstantBuffer1Data(&cb1);
	}


	Model::SetTransform(hBranchesMakeTree_, trans);
	Model::Draw(hBranchesMakeTree_);
}


void LSystemTree::DrawBranch()
{
	////イテレータでリストを回してリストの最後まで回す
	//for (auto itr = drawModel_.begin(); itr != drawModel_.end();
	//	itr++)
	//{
	//	//Transformの計算
	//	//modelTransform_[(*itr)].Calclation();



	//	//リストに、モデル番号とそのTransformの添え字を示す番号が入る
	//	//→そのため、その値をそれぞれの配列（Vector）の添え字として要素を取得
	//	//描画するモデル番号と、自身のTransformを送って描画
	//	Model::SetTransform(hModel_[(*itr)], modelTransform_[(*itr)]);

	//	//描画
	//	Model::Draw(hModel_[(*itr)]);
	//}
}

void LSystemTree::StartEraseMotion()
{
	//消去モーション開始のフラグを立てる
	isEraseMotion_ = true;


	//シェーダーの切替え
		//消去モーションを必要とするシェーダーに変換
	Model::ChangeShader(hBranchesMakeTree_ , SHADER_HEIGHT_MAP_ERASE_MOTION);

	//シェーダー切替えによって、
		//シェーダーに渡す情報として、
		//経過時間を必要とする
		//その経過時間の開始を宣言する
	pCountTimer_ = (CountTimer*)Instantiate<CountTimer>(this);
	//カウントアップ
	//開始：０．ｆ
	//終了：５．ｆ
	pCountTimer_->StartTimerForCountUp(0.f, 5.f);




}

bool LSystemTree::EndEraseMotion()
{
	if (pCountTimer_ != nullptr)
	{
		//現在の経過時間が、終了時間以上である
		if (pCountTimer_->GetElpasedTime() >= pCountTimer_->GetEndTimerForCountUp())
		{
			return true;
		}
	}
	return false;
}


//開放
void LSystemTree::Release()
{
	//pBranchesMakeTreeは、
		//Model.cppのモデルリストに追加しているので、
		//解放しない
	//pBranchSource_も同様に、ロードしてから、ポインタをもらう
	//SAFE_DELETE(pBranchSource_);
}


void LSystemTree::SetTransform(Transform * trans)
{
	transform_.position_ = trans->position_;
	transform_.rotate_ = trans->rotate_;
	transform_.scale_ = trans->scale_;

	//ベクターから
	//行列の作成
	transform_.Calclation();


}

bool LSystemTree::ExecutableLSystem()
{
	return lsystemCounter_ != LSYSTEM_COUNT_;
}

int LSystemTree::GetLSystemCounter()
{
	return lsystemCounter_;
}

#if 0 
void LSystemTree::StringReplacement()
{

	std::string nextSentence = "";

	//sentence（現在の文字列）の文字１文字１文字
	//をルールにのっとって、rulesの要素一番目は、要素２番目の値に置き換える
	//ルール："F"　→　"FF+[+F-F-F]-[-F+F+F]"
	for (int i = 0; i < sentence.size(); i++)
	{
		//文字列の頭(１文字目)を持って来る
			//文字列の1要素（char型の配列でstringは取得されている）を取得
		auto current = sentence[i];

		//見つかったかの判定
		bool found = false;

		//文字列の２次元配列なので、
			//文字列から１文字のcharを取得するときは、３次元配列の要素指定になる
		for (int k = 0; k < RULE_END; k++)
		{
			//２次元配列の
			//rulesの列番目、取得するchar要素位置(何番目のchar値をとるか)
			if (current == rules[RULE_A][0])
			{
				found = true;

				//文字列なのでそのまま代入
				nextSentence += rules[RULE_B];
				break;
			}
		}

		//変換されていないなら、
		//そのままの値を足す（rules１番目の要素以外の文字だった）
		if (!found)
		{
			nextSentence += current;
		}
	}

	sentence = nextSentence;


	//文字列の頭(１文字目)を持って来る
		//文字列の０番目（charの配列がstringなので、→その０番目を取得することで、文字列１文字目を取得）
		//文字列で扱うが、取得した値をstring方で扱うことは不可能
		//文字列から配列のように、要素一つ一つ取得すると、
			//→1バイトであれば、[0]要素一つを取得すると、そのままの文字として取得することが可能
			//→2バイトだと、char型の配列1つの要素に文字を表すデータが入っていない。2バイトなので、char型2マス分が文字列となる。
		//今回は、1バイトの文字のみ使用するので、配列1要素を指定する形にする。
	//auto current = sentence[0];	//auto型、あるいは＋で取得
	//cout << current;


}

#endif
#if 0 
void LSystemTree::CreateModel()
{
	//モデルに登録するTransformの準備
		//文字によっては、回転などが存在するので、Transformは、外部に持ってくる。
	Transform trans;

	//高さを決めてセットする。
	trans.position_ = XMVectorSet(0, length, 0, 0);

	//高さを上げる
	length += 2;

	//描画をするには、前回の描画した位置を覚えておかないといけない
		//枝のはやす位置、向きというのを考えないといけない。（そのままの向きで新たにまっすぐ上方向に向けるわけにはいかない（現在の方向から延びる枝として作成しないといけないので、））
	//枝分かれを起こしたら、その位置を、次の描画位置として移動が必要

	/*★★★★木生成方法案�@★★★★*/
	/*
		[が始まったところ内の枝は、同じ節から生える枝。
		]が終わったら、次からは、それ以降の枝から延びることに
		（まだ、ここでは、枝は上に移動することはない。）

		そして、次に生えるとき、
		→置換の時に、
			その置換するときに、置換に使う、Fが、置換されてFで描画されるものの、親となる。親の節となる。→ということは、その節のTransformを渡してやって、それをもとに、枝分かれの節を作る

		→親の位置をもらって、そこから、枝分かれの位置をとる。


		であれば、　文字列の置換時に
					描画のモデルの登録、を行わないといけない。
		（置換時に、Fを置換するときに、）



		だが、上記の方法では、
		ひとまず、置換した親の位置から枝分かれしていくようになったが。
		★だが、置換後に全体の描画を行うわけではないので、生え方が一パターンになる。（それでいいのか？？？）

		→置換後にFを描画してしまうと、とてつもない数になってしまうか？？？
		→なので、上記の方法でいいのでは？？



	*/


	//描画するライン（素材）の回転値（Transform）	
		//初期値は上に伸びてほしいので、＋Y方向に１

	//置換した後の文字列数分
	for (int i = 0; i < sentence.size(); i++)
	{
		//1バイト分、char1文字分取得
		auto current = sentence[i];



		//もし1バイトの文字がFだったら、
		//描画しろ命令
		if (current == 'F')
		{
			//現在のTransformで描画
			CreateTreeSource(trans);
			//Transformをリセット

			trans.rotate_ = XMVectorSet(0, 0, 0, 0);

		}
		//＋回転しろ（一定量回転）
		else if (current == '+')
		{
			//Transform値を＋回転
			trans.rotate_.vecZ += 45;
			//描画はしない。
				//描画はFが呼ばれたら、
				//なので→次に呼ばれたFは、上記の回転で描画されるので、
				//+回転された描画となる。

		}
		// - 回転しろ
		else if (current == '-')
		{
			//Transform値を - 回転
			trans.rotate_.vecZ -= 45;
		}
		// * +2倍回転しろ
		else if (current == '*')
		{
			//Transform値を + 回転
			trans.rotate_.vecZ += 90;
		}
		// / -2倍回転しろ
		else if (current == '/')
		{
			//Transform値を - 回転
			trans.rotate_.vecZ -= 90;
		}
		//保存（その階層は終了？次の階層へ？？）
		//[]に囲まれた部分は、枝分かれを意味する
		else if (current == '[')
		{

		}
		//pop　謎
		else if (current == ']')
		{

		}

	}


}
#endif


#if 0 
/*★★★★木生成方法案�A★★★★*/
/*
//文字を置換して、親を送るまでは良いが、
//それを頭から配列の要素と照らし合わせて、その要素で示したTransformに存在している要素を示していた。
//→だが、置換したものを新に置換の作業をしたら、頭から見たら、それは、０番目で、配列の０番目の要素を示してしまう。
	//→なので、前回の最終配置数を保存していくことで、（変数は分けなくてもいいか？？）

	//→置換を行うときに、前回の最終配置数をからひとつづつ要素を足していくことで、
		//→親の枝（前回置換にて作られたF）の要素を示すことができる→それでTransfromを示す

*/


void LSystemTree::StringReplacement_2()
{
	std::string nextSentence = "";

	//前回の最終配置数を計算用の変数に配置
	int modelCounter = length;
	//上記のmodelCounterをカウントして、前回に配置した素材のTransformだったりにアクセスする
	//終了条件を作らないと、配置していない要素にアクセスしてしまいそうだが、、
		//→下記の文字列の置換では、Fという、前回配置された


//sentence（現在の文字列）の文字１文字１文字
//をルールにのっとって、rulesの要素一番目は、要素２番目の値に置き換える
//ルール："F"　→　"FF+[+F-F-F]-[-F+F+F]"
	for (int i = 0; i < sentence.size(); i++)
	{
		//文字列の頭(１文字目)を持って来る
			//文字列の1要素（char型の配列でstringは取得されている）を取得
		auto current = sentence[i];

		//見つかったかの判定
		bool found = false;

		//文字列の２次元配列なので、
			//文字列から１文字のcharを取得するときは、３次元配列の要素指定になる
		for (int k = 0; k < RULE_END; k++)
		{
			//２次元配列の
			//rulesの列番目、取得するchar要素位置(何番目のchar値をとるか)
			//置換を行う文字であった場合、置換のルールにのっとって、置換
			if (current == rules[RULE_A][0])
			{
				found = true;

				//文字列なのでそのまま代入
				nextSentence += rules[RULE_B];

				//置換による、モデルの描画
					//引数にて、この先置換されて枝を描画するときの親とするモデルのTransformを送る
					//ここで、現在示しているmodelCounter（lengthを進めた値）
					//modelCounterにて、示す値は前回にて置換された素材が登録されているモデルの番号
					//この番号を添え字として配列にアクセスすれば、
						//→登録された素材にアクセスできる→これが、この関数にて置換がされる文字列が示されていたモデルとなる
				CreateModel3(modelTransform_[modelCounter]);


				//置換を完了したので、置換のモデルとなった素材を次に進める。
					//置換が完了したことで、その置換のもととなったモデルがこの先の枝の親となることはなくなったので、
				//カウントを進める
				modelCounter++;

				break;
			}
		}

		//変換されていないなら、
		//そのままの値を足す（rules１番目の要素以外の文字だった）
		if (!found)
		{
			nextSentence += current;
		}
	}

	//置換が完了した文字列を
	//置換完了後の文字列保存変数に代入
	sentence = nextSentence;

	//置換が完了したため、前回の最終配置数を
		//今回置換を完了した親の枝数分、（つまり置換を行った数分）
	//前回分にカウントした変数を代入
	length = modelCounter;

}
#endif


//★下記の文章の実装を実現するために、文字列の置換のみ行う
//樹木生成�C
/*
//★枝を伸ばしたら、次に伸ばす枝の開始位置は前回伸ばされた枝の終点から伸びる
	//★枝を伸ばしたら、伸ばした先のTransformが親の枝のTransformとなる。（サイズもこの先使用するかもしれないのでTransformで）
	//枝を発生させる位置（描画の際の枝のTransformは、）は、前回の枝の位置だけ覚えておけば十分だが、
		//描画の際には、枝のTransformは無いといけないので、保存しておく
	//[]で、枝を分岐させるものに関しては、
		//分岐中は、分岐開始の位置を保存しておき、それとは別に、分岐された枝先にて枝の成長を行っていく。
		//]で分岐が終了したら、→親のTransformの位置を保存しておいたTransformで上書きして、成長続ける
//★（前提）一度枝が分岐し、成長し、成長を終えた[]の枠を超えたとき
	//→それ以降で、その枝についかして枝が生えることは絶対にない
*/

void LSystemTree::StringReplacement_4()
{
	std::string nextSentence = "";



	//sentence（現在の文字列）の文字１文字１文字
	//をルールにのっとって、rulesの要素一番目は、要素２番目の値に置き換える
	//ルール："F"　→　"FF+[+F-F-F]-[-F+F+F]"などに置換
	for (int i = 0; i < sentence.size(); i++)
	{
		//文字列の頭(１文字目)を持って来る
			//文字列の1要素（char型の配列でstringは取得されている）を取得
		auto current = sentence[i];


		//置換対象の文字列
			//上記の文字列のchar要素位置(何番目のchar値をとるか)の0番目を取得
			//置換を行う文字であった場合、置換のルールにのっとって、置換後の文字列を結合
		if (current == REPLACEMENT_TARGET_[0])
		{


			//文字列なのでそのまま代入
			nextSentence += RULE_;

			//break;
		}
		//変換されていないなら、
		//そのままの値を足す（rules１番目の要素以外の文字だった）
				//現段階参照しているsentence全てを見終えるまで終了しない
		else
		{
			nextSentence += current;
		}
	}

	//置換が完了した文字列を
	//置換完了後の文字列保存変数に代入
	sentence = nextSentence;



}


#if 0 
//★Fで生やされた枝→それを、親としてその枝の終点から枝を生やすイメージ
//Transformで、自身が生えている枝を指定して、
//その位置から新しく枝を生やす。
//文字列の置換とは別に、もらったTransformで置換後の文字列に合わせて、枝を生成
//★今回の場合、置換後の文字列というのはわかっている
void LSystemTree::CreateModel(Transform trans)
{
	//引数のTransformを保存
	//モデルに登録するTransformの準備
	//文字によっては、回転などが存在するので、Transformは、外部に持ってくる。
	Transform setTrans = trans;

	setTrans.position_ += addVec_;



	//描画するライン（素材）の回転値（Transform）	
		//初期値は上に伸びてほしいので、＋Y方向に１

	//置換する文字は、決められているので、
	//置換のルールにて決められているものを使用する
	//置換した後の文字列数分
	for (int i = 0; i < rules[RULE_B].size(); i++)
	{
		//1バイト分、char1文字分取得
		auto current = rules[RULE_B][i];



		//もし1バイトの文字がFだったら、
		//描画しろ命令
		if (current == 'F')
		{
			//現在のTransformで描画
			CreateTreeSource(setTrans);

			//Transformをリセット
			setTrans = trans;
			setTrans.position_ += addVec_;
		}
		//＋回転しろ（一定量回転）
		else if (current == '+')
		{
			//Transform値を＋回転
			setTrans.rotate_.vecZ += 45;

			setTrans.position_ += addVec_;

			//描画はしない。
				//描画はFが呼ばれたら、
				//なので→次に呼ばれたFは、上記の回転で描画されるので、
				//+回転された描画となる。

		}
		// - 回転しろ
		else if (current == '-')
		{
			//Transform値を - 回転
			setTrans.rotate_.vecZ -= 45;

			//逆方向にベクトルを持ってきたいので、現段階ではX値だけ反転させる
			setTrans.position_ += addVec_ * XMVectorSet(-1.0f, 1.0f, 1.0f, 0.0f);
		}
		// * +2倍回転しろ
		else if (current == '*')
		{
			//Transform値を + 回転
			setTrans.rotate_.vecZ += 90;

			setTrans.position_ += addVec_;
		}
		// / -2倍回転しろ
		else if (current == '/')
		{
			//Transform値を - 回転
			setTrans.rotate_.vecZ -= 90;

			//足すベクトルを反転
			setTrans.position_ -= addVec_ * XMVectorSet(-1.0f, 1.0f, 1.0f, 0.0f);
		}
		//保存（その階層は終了？次の階層へ？？）
		//[]に囲まれた部分は、枝分かれを意味する
		else if (current == '[')
		{

		}
		//pop　謎
		else if (current == ']')
		{

		}

	}


}
#endif

#if 0
/*★★★★木生成方法案�B★★★★*/
/*
	親の位置を継承することは、できた、
	→なので、→回転などをした時の、
			→回転させたときの回転後の枝の先の位置にTransformを持ってくることができれば、

	木を左右対称に広がるものが作れるはず。



*/
//https://www.sidefx.com/ja/docs/houdini/nodes/sop/lsystem.html
//上記のサイトから、
//[が枝分かれの開始、→[]内は、その分かれた枝によって、その枝から各成長をしていく
	//]が終わったら、枝分かれを始めた位置へ戻るようにする
	//→次の文字列で、また[が始まったら、枝分かれが始まるので、木全体から見たら、一本線から２つの枝へ分岐したように見える
void LSystemTree::CreateModel3(Transform trans)
{
	//引数のTransformを保存
	//モデルに登録するTransformの準備
	//文字によっては、回転などが存在するので、Transformは、外部に持ってくる。
	Transform setTrans = trans;
	//回転値はリセット
	setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);


	//setTrans.position_ += addVec_;


	//分岐を示すフラグ
	bool branch = false;


	//描画するライン（素材）の回転値（Transform）	
		//初期値は上に伸びてほしいので、＋Y方向に１

	//置換する文字は、決められているので、
	//置換のルールにて決められているものを使用する
	//置換した後の文字列数分
	for (int i = 0; i < rules[RULE_B].size(); i++)
	{
		//1バイト分、char1文字分取得
		auto current = rules[RULE_B][i];



		//もし1バイトの文字がFだったら、
		//描画しろ命令
		if (current == 'F')
		{
			//現在のTransformで描画
			CreateTreeSource(setTrans);

			//分岐していないなら
			if (branch != true)
			{
				//Transformをリセット
				setTrans = trans;
				//回転値はリセット
				setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);
				//setTrans.position_ += addVec_;
			}
			//分岐しているなら
			else
			{
				//現在の位置を残したまま、
				//その位置から次の枝生成を行う

					//回転値はリセット
				setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);
			}
		}
		//＋回転しろ（一定量回転）
		else if (current == '+')
		{
			//Transform値を＋回転 (DirectXは、MAYAで作成したものと、逆になってしまうので注意)
			setTrans.rotate_.vecZ += 45 * (-1);

			setTrans.position_ += addVec_;

			//描画はしない。
				//描画はFが呼ばれたら、
				//なので→次に呼ばれたFは、上記の回転で描画されるので、
				//+回転された描画となる。

		}
		// - 回転しろ
		else if (current == '-')
		{
			//Transform値を - 回転 
			setTrans.rotate_.vecZ -= 45 * (-1);

			//逆方向にベクトルを持ってきたいので、現段階ではX値だけ反転させる
			setTrans.position_ += addVec_ * XMVectorSet(-1.0f, 1.0f, 1.0f, 0.0f);
		}
		// * +2倍回転しろ
		else if (current == '*')
		{
			//Transform値を + 回転
			setTrans.rotate_.vecZ += 90 * (-1);

			setTrans.position_ += addVec_;
		}
		// / -2倍回転しろ
		else if (current == '/')
		{
			//Transform値を - 回転
			setTrans.rotate_.vecZ -= 90 * (-1);

			//足すベクトルを反転
			setTrans.position_ -= addVec_ * XMVectorSet(-1.0f, 1.0f, 1.0f, 0.0f);
		}
		//枝分かれの分岐開始
		else if (current == '[')
		{
			//分岐開始
			//分岐中は枝を伸ばしても、枝の生える位置をリセットせずに、
			branch = true;
		}
		//枝分かれ終了、枝分かれを起こした分岐位置へ戻る
		else if (current == ']')
		{
			//分岐終了
			//分岐位置へ戻る
			setTrans = trans;

			//回転値はリセット
			setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);

			branch = false;
		}

	}


}

#endif


//このままでは、LSYSTEMとしては、未完成である。
//�C
//[]　枝分岐を使用すれば、
//枝が伸びたときに、その伸びた先の枝の先端が次の枝発生位置となる
	//→　＝　つまり、枝の発生、はやす位置は動的に常に変化し続けているということ。？？
//★枝を伸ばしたなら、次に延びる枝は、左記にて伸ばした枝の続きに書かなくてはならない。
	//→[]があるならば、→その中は、分岐された枝をもとに延ばす。分岐された枝の中で完結させる。


//★枝を上部を盛り上げるためには、
	//→枝を伸ばした枝から生えさせて、行かなければならない。（その時その時で、はやした枝の位置を覚えておく必要がない）

//考える必要がある。

/*
//★枝を伸ばしたら、次に伸ばす枝の開始位置は前回伸ばされた枝の終点から伸びる
	//★枝を伸ばしたら、伸ばした先のTransformが親の枝のTransformとなる。（サイズもこの先使用するかもしれないのでTransformで）
	//枝を発生させる位置（描画の際の枝のTransformは、）は、前回の枝の位置だけ覚えておけば十分だが、
		//描画の際には、枝のTransformは無いといけないので、保存しておく
	//[]で、枝を分岐させるものに関しては、
		//分岐中は、分岐開始の位置を保存しておき、それとは別に、分岐された枝先にて枝の成長を行っていく。
		//]で分岐が終了したら、→親のTransformの位置を保存しておいたTransformで上書きして、成長続ける
//★（前提）一度枝が分岐し、成長し、成長を終えた[]の枠を超えたとき
	//→それ以降で、その枝についかして枝が生えることは絶対にない
*/
void LSystemTree::CreateModelTransform4()
{
	//必要な要素
	//前回の枝のTransform（自身が枝をはやさせる親の枝のTransform）→常に更新
	//初期値に、前回のLSYSTEMによって、分岐せずに直線に延ばされた枝のTransformを代入
	//Transform parentTrans = lastTreeTrans;
	
	
	//分岐を起こした場合に分岐前の枝のTransformを保存する変数(分岐が終わった後に分岐した地点から枝を生やせるように)
	//分岐開始位置
	Transform branchTrans = lastTreeTrans;


	//sentence位置を示す要素位置（添え字）を初期化
	pos = 0;

	//★★★★★★★★★★
	/*
		!!
		再帰

		[が始まったら、まず�@分岐位置を持っておく

		その分岐位置を関数へ送って、その分岐位置をもとに枝を発生、
		再び[が来たら、再び現在の分岐位置を関数へ送って、その分岐位置をもとに枝を発生

		その関数の段階で、]が来たら、終了。関数を返す。（再帰）
		戻った先で、文字を進めて、]が来たら、終了。関数を返す。

		これを再帰的に続ける。。


		（注意）
		まっすぐに進むので、
		関数内の次に発生させる枝の位置、前回の枝の位置は別に持っておかないといけない。
		関数の引数の値は、変更せずに。



		（問題）
		共通の文字列である、sentenceを見るので、
		関数にて飛んだ先においても、sentenceを見ることになる。
		→これを、関数先、関数外でも参照するには、メンバ変数で現在見ている位置を持っておかないと（ただのint型で、LSYSTEM終了したら、０で初期化すればいい。）

	*/

	/*
	//引数のTransformを保存
	//モデルに登録するTransformの準備
	//文字によっては、回転などが存在するので、Transformは、外部に持ってくる。
	Transform setTrans = trans;
	//回転値はリセット
	//setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);


	//setTrans.position_ += addVec_;
	*/


	//セット用のTransform
	Transform setTrans = lastTreeTrans;
	//回転値はリセット
	//setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);

	//移動量ベクトル
	XMVECTOR moveVec = addVec_;



	//描画するライン（素材）の回転値（Transform）	
		//初期値は上に伸びてほしいので、＋Y方向に１

	//置換する文字は、決められているので、
	//置換のルールにて決められているものを使用する
	//置換した後の文字列数分
	for (pos = 0; pos < sentence.size(); pos++)
	{
		//1バイト分、char1文字分取得
		auto current = sentence[pos];

		//文字により行動変化
		switch (ReplacementJudgment(current))
		{
		case LSystemTree::TRANSFORM_F : 
			TransformF(setTrans, moveVec, branchTrans); break;
		case LSystemTree::TRANSFORM_PLUS:
			TransformSource(setTrans, XMVectorSet(0, 0, 45 * (-1), 0)); break;
		case LSystemTree::TRANSFORM_MINUS:
			TransformSource(setTrans, XMVectorSet(0, 0, -45 * (-1), 0)); break;
		case LSystemTree::TRANSFORM_MULTIPLY:
			TransformSource(setTrans, XMVectorSet(45 * (-1), 0, 0, 0)); break;
		case LSystemTree::TRANSFORM_DIVISION:
			TransformSource(setTrans, XMVectorSet(-45 * (-1), 0, 0, 0)); break;
		case LSystemTree::TRANSFORM_START_BRANCH:
			//関数へ飛んで、]が見つかるまで関数先にて、枝生成を行う
				//枝の生成を行う分岐位置を送ってもらう

				//sentenceを見る値を一つ進める（関数先で[の次から文字を見るように）
			pos++;
			BranchTree(branchTrans);
			break;
		case LSystemTree::TRANSFORM_END_BRANCH:
			//分岐終了
			//関数呼び出しもと（[を受けて、関数を呼び出したものへ）
			return;

		default : 
			return;

		}

		
		


		//	//もし1バイトの文字がFだったら、
		////描画しろ命令
		//	if (current == 'F')
		//	{


		//	}
		////＋回転しろ（一定量回転）
		//	else if (current == '+')
		//	{
		//		//Transform値を＋回転 (DirectXは、MAYAで作成したものと、逆になってしまうので注意)
		//		setTrans.rotate_.vecZ += 45 * (-1);

		//		//setTrans.position_ += addVec_;

		//		//描画はしない。
		//			//描画はFが呼ばれたら、
		//			//なので→次に呼ばれたFは、上記の回転で描画されるので、
		//			//+回転された描画となる。

		//	}
		//// - 回転しろ
		//	else if (current == '-')
		//	{
		//		//Transform値を - 回転 
		//		setTrans.rotate_.vecZ -= 45 * (-1);

		//		//逆方向にベクトルを持ってきたいので、現段階ではX値だけ反転させる
		//		//setTrans.position_ += addVec_ * XMVectorSet(-1, 1, 1, 0);
		//	}
		//// * +2倍回転しろ
		//	else if (current == '*')
		//	{
		//		//Transform値を + 回転
		//		setTrans.rotate_.vecX += 45 * (-1);

		//		//setTrans.position_ += addVec_;
		//	}
		//// / -2倍回転しろ
		//	else if (current == '/')
		//	{
		//		//Transform値を - 回転
		//		setTrans.rotate_.vecX -= 45 * (-1);

		//		//足すベクトルを反転
		//		//setTrans.position_ -= addVec_ * XMVectorSet(-1, 1, 1, 0);
		//	}
		////枝分かれの分岐開始
		//	else if (current == '[')
		//	{


		//		//関数へ飛んで、]が見つかるまで関数先にて、枝生成を行う
		//		//枝の生成を行う分岐位置を送ってもらう

		//		//sentenceを見る値を一つ進める（関数先で[の次から文字を見るように）
		//		pos++;
		//		BranchTree(branchTrans);
		//	}
		//	else
		//	{
		//		return;

		//	}





	}

	//LSYSTEMの枝生成が完了したので、
	//最後にまっすぐ伸ばした枝の位置を登録
	//LSYSTEmを開始した関数の、最後の分岐位置を登録させる。→つぎのLSYSTEMは、下記にて登録した位置から枝を生成させる
	lastTreeTrans = branchTrans;


}

void LSystemTree::TransformF(Transform& setTrans, XMVECTOR moveVec, Transform& branchTrans)
{
	//現在のTransform値に、移動ベクトルを足す
	//→ただ足すのではなく、現在の回転を行列として取得し、その回転分ベクトルを回転させる(ベクトルに回転行列を掛ける)
	//回転した方向に一定量移動する。
//setTrans.position_ += moveVec;



	//★回転方向における、moveVec文の移動を行う
////回転行列	//これに回転具合を入れる
	XMMATRIX matXZ;
//	//回転させた軸の回転具合を取得（ラジアンに直して、行列にする）
	matXZ = XMMatrixRotationZ(XMConvertToRadians(setTrans.rotate_.vecZ));
	matXZ *= XMMatrixRotationX(XMConvertToRadians(setTrans.rotate_.vecX));
	//回転行列と移動ベクトルを掛ける			
	setTrans.position_ += XMVector3TransformCoord(moveVec, matXZ);
	//setTrans.position_ += moveVec;
	setTrans.position_.vecW = 0.f;



	//現在のTransformで描画
		//木を一本表示させる
//★モデル番号をModelcppにて複数個確保管理する
//CreateTreeSource(trans);
//★一つのモデルの頂点数を動的に増やし、	
	//モデルの３Dポリゴンを変化させていく
	Join3DPolygon(setTrans);


	//自身のTransformを親の位置へ登録
//枝分かれは考えずに、一本で完結する場合を考えて
			//parentTrans = setTrans;


			//Transformをリセット
			//setTrans = parentTrans;
			//回転値はリセット
			//setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);
			//setTrans.position_ += addVec_;




	branchTrans = setTrans;
	
}

LSystemTree::TRANSFORM_TYPE LSystemTree::ReplacementJudgment(char current)
{
	//もし1バイトの文字がFだったら、
//描画しろ命令
	switch (current)
	{
	case 'F' : 
		return LSystemTree::TRANSFORM_F;
	case '+':
		return LSystemTree::TRANSFORM_PLUS;
	case '-':
		return LSystemTree::TRANSFORM_MINUS;
	case '*':
		return LSystemTree::TRANSFORM_MULTIPLY;
	case '/':
		return LSystemTree::TRANSFORM_DIVISION;
	case '[':
		return LSystemTree::TRANSFORM_START_BRANCH;
	case ']':
		return LSystemTree::TRANSFORM_END_BRANCH;

	default : 
		return LSystemTree::TRANSFORM_MAX;

	}



}


void LSystemTree::TransformSource(Transform& setTrans, XMVECTOR addRotateValue)
{
	setTrans.rotate_ += addRotateValue;
}

//LSYSTEMの枝生成と同じことをする
//[の文字を受けて、枝が分岐された後の枝生成
	//引数にてもらったTransform値から枝を生成していく（引数値を開始位置としてLSYSTEMを行う）
	//']'が見つかるまで、見つかったら帰る。
	//途中で[を見つけたら、さらに現在の分岐位置を送って再帰的に行う
//戻値は、なし＝分岐後は、その分岐先にて枝の生成を完結させたいので、その分岐先で最終的な分岐位置も必要なし（そこからさらに枝を生成することもないので。）
void LSystemTree::BranchTree(Transform trans)
{
	//必要な要素
	//前回の枝のTransform（自身が枝をはやさせる親の枝のTransform）→常に更新
	//初期値に、前回のLSYSTEMによって、分岐せずに直線に延ばされた枝のTransformを代入
	//Transform parentTrans = trans;
	//分岐を起こした場合に分岐前の枝のTransformを保存する変数(分岐が終わった後に分岐した地点から枝を生やせるように)
	//分岐開始位置
	Transform branchTrans = trans;




	//回転したかのフラグ（直線の枝なのか、回転して生成した枝なのか。）
	//回転しても、してなくても、分岐が起こらない時点で、それは、枝として正式に伸びたことを示す。→なので、枝が伸びたら、そのまま、branchTransに登録
	//分岐したら、関数を呼べば良い。
	//bool rotate = false;


	//セット用のTransform
	Transform setTrans = trans;
	//回転値はリセット
	//setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);

	//移動量ベクトル
	XMVECTOR moveVec = addBranchVec_;



	//描画するライン（素材）の回転値（Transform）	
		//初期値は上に伸びてほしいので、＋Y方向に１

	//置換する文字は、決められているので、
	//置換のルールにて決められているものを使用する
	//置換した後の文字列数分
	for (pos; pos < sentence.size(); pos++)
	{
		//1バイト分、char1文字分取得
		auto current = sentence[pos];


		//文字により行動変化
		switch (ReplacementJudgment(current))
		{
		case LSystemTree::TRANSFORM_F:
			TransformF(setTrans, moveVec, branchTrans); break;
		case LSystemTree::TRANSFORM_PLUS:
			TransformSource(setTrans, XMVectorSet(0, 0, 45 * (-1), 0)); break;
		case LSystemTree::TRANSFORM_MINUS:
			TransformSource(setTrans, XMVectorSet(0, 0, -45 * (-1), 0)); break;
		case LSystemTree::TRANSFORM_MULTIPLY:
			TransformSource(setTrans, XMVectorSet(45 * (-1), 0, 0, 0)); break;
		case LSystemTree::TRANSFORM_DIVISION:
			TransformSource(setTrans, XMVectorSet(-45 * (-1), 0, 0, 0)); break;
		case LSystemTree::TRANSFORM_START_BRANCH:
			//関数へ飛んで、]が見つかるまで関数先にて、枝生成を行う
				//枝の生成を行う分岐位置を送ってもらう

				//sentenceを見る値を一つ進める（関数先で[の次から文字を見るように）
			pos++;
			BranchTree(branchTrans);
			break;
		case LSystemTree::TRANSFORM_END_BRANCH:
			//分岐終了
			//関数呼び出しもと（[を受けて、関数を呼び出したものへ）
			return;

		default:
			return;

		}


	//	//もし1バイトの文字がFだったら、
	//	//描画しろ命令
	//	if (current == 'F')
	//	{
	//		//現在のTransform値に、移動ベクトルを足す
	//			//→ただ足すのではなく、現在の回転を行列として取得し、その回転分ベクトルを回転させる
	//			//回転した方向に一定量移動する。
	//		//setTrans.position_ += moveVec;
	//		//回転行列	//これに回転具合を入れる
	//		XMMATRIX matZ;
	//		//回転させた軸の回転具合を取得（ラジアンに直して、行列にする）
	//		matZ = XMMatrixRotationZ(XMConvertToRadians(setTrans.rotate_.vecZ));
	//		matZ *= XMMatrixRotationX(XMConvertToRadians(setTrans.rotate_.vecX));
	//		//回転行列と移動ベクトルを掛ける			
	//		setTrans.position_ += XMVector3TransformCoord(moveVec, matZ);



	//		//現在のTransformで描画
	//			//木を一本表示させる
	////★モデル番号をModelcppにて複数個確保管理する
	////CreateTreeSource(trans);
	////★一つのモデルの頂点数を動的に増やし、	
	//	//モデルの３Dポリゴンを変化させていく
	//		Join3DPolygon(setTrans);



	//		//自身のTransformを親の位置へ登録
	//			//枝分かれは考えずに、一本で完結する場合を考えて
	//		//parentTrans = setTrans;

	//		//回転値はリセット
	//		//setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);

	//	//分岐で自身の関数が呼ばれている場合、回転を行なっていても、
	//	//行なっていなくても、
	//	//それが正式に伸びた枝となる。→なので、これを分岐の位置に登録できる。（分岐が始まるときに、その位置から始める）
	//		branchTrans = setTrans;

	//	}
	//	//＋回転しろ（一定量回転）
	//	else if (current == '+')
	//	{
	//		//Transform値を＋回転 (DirectXは、MAYAで作成したものと、逆になってしまうので注意)
	//		setTrans.rotate_.vecZ += 45 * (-1);



	//	}
	//	// - 回転しろ
	//	else if (current == '-')
	//	{
	//		//Transform値を - 回転 
	//		setTrans.rotate_.vecZ -= 45 * (-1);

	//	}
	//	// * +2倍回転しろ
	//	else if (current == '*')
	//	{
	//		//Transform値を + 回転
	//		setTrans.rotate_.vecX += 45 * (-1);

	//	}
	//	// / -2倍回転しろ
	//	else if (current == '/')
	//	{
	//		//Transform値を - 回転
	//		setTrans.rotate_.vecX -= 45 * (-1);

	//	}
	//	//さらに
	//	//枝分かれの分岐開始
	//	else if (current == '[')
	//	{
	//		//分岐開始

	//		//関数へ飛んで、]が見つかるまで関数先にて、枝生成を行う
	//		//枝の生成を行う分岐位置を送ってもらう

	//		//sentenceを見る値を一つ進める（関数先で[の次から文字を見るように）
	//		pos++;
	//		BranchTree(branchTrans);
	//	}
	//	//枝分かれ終了、枝分かれを起こした分岐位置へ戻る
	//	else if (current == ']')
	//	{
	//		//分岐終了
	//		//関数呼び出しもと（[を受けて、関数を呼び出したものへ）
	//		return;
	//	}

	}
}

void LSystemTree::LoadFallenLeaves(const std::string& textureFileName)
{
	//落ち葉クラスへ
	//テクスチャを指定して渡す
	pFallenLeaves_ = (FallenLeaves*)Instantiate<FallenLeaves>(this);
	//テクスチャロード
	pFallenLeaves_->Load(textureFileName);

}


#if 0
//このままでは、LSYSTEMとしては、未完成である。
//�C
//[]　枝分岐を使用すれば、
//枝が伸びたときに、その伸びた先の枝の先端が次の枝発生位置となる
	//→　＝　つまり、枝の発生、はやす位置は動的に常に変化し続けているということ。？？
//★枝を伸ばしたなら、次に延びる枝は、左記にて伸ばした枝の続きに書かなくてはならない。
	//→[]があるならば、→その中は、分岐された枝をもとに延ばす。分岐された枝の中で完結させる。


//★枝を上部を盛り上げるためには、
	//→枝を伸ばした枝から生えさせて、行かなければならない。（その時その時で、はやした枝の位置を覚えておく必要がない）

//考える必要がある。

/*
//★枝を伸ばしたら、次に伸ばす枝の開始位置は前回伸ばされた枝の終点から伸びる
	//★枝を伸ばしたら、伸ばした先のTransformが親の枝のTransformとなる。（サイズもこの先使用するかもしれないのでTransformで）
	//枝を発生させる位置（描画の際の枝のTransformは、）は、前回の枝の位置だけ覚えておけば十分だが、
		//描画の際には、枝のTransformは無いといけないので、保存しておく
	//[]で、枝を分岐させるものに関しては、
		//分岐中は、分岐開始の位置を保存しておき、それとは別に、分岐された枝先にて枝の成長を行っていく。
		//]で分岐が終了したら、→親のTransformの位置を保存しておいたTransformで上書きして、成長続ける
//★（前提）一度枝が分岐し、成長し、成長を終えた[]の枠を超えたとき
	//→それ以降で、その枝についかして枝が生えることは絶対にない
*/
void LSystemTree::CreateModel4()
{
	//必要な要素
	//前回の枝のTransform（自身が枝をはやさせる親の枝のTransform）→常に更新
	//初期値に、前回のLSYSTEMによって、分岐せずに直線に延ばされた枝のTransformを代入
	Transform parentTrans = lastTreeTrans;
	//分岐を起こした場合に分岐前の枝のTransformを保存する変数(分岐が終わった後に分岐した地点から枝を生やせるように)
	//分岐開始位置
	Transform branchTrans = lastTreeTrans;


	//sentence位置を示す要素位置（添え字）を初期化
	pos = 0;

	//★★★★★★★★★★
	/*
		!!
		再帰

		[が始まったら、まず�@分岐位置を持っておく

		その分岐位置を関数へ送って、その分岐位置をもとに枝を発生、
		再び[が来たら、再び現在の分岐位置を関数へ送って、その分岐位置をもとに枝を発生

		その関数の段階で、]が来たら、終了。関数を返す。（再帰）
		戻った先で、文字を進めて、]が来たら、終了。関数を返す。

		これを再帰的に続ける。。


		（注意）
		まっすぐに進むので、
		関数内の次に発生させる枝の位置、前回の枝の位置は別に持っておかないといけない。
		関数の引数の値は、変更せずに。



		（問題）
		共通の文字列である、sentenceを見るので、
		関数にて飛んだ先においても、sentenceを見ることになる。
		→これを、関数先、関数外でも参照するには、メンバ変数で現在見ている位置を持っておかないと（ただのint型で、LSYSTEM終了したら、０で初期化すればいい。）

	*/

	/*
	//引数のTransformを保存
	//モデルに登録するTransformの準備
	//文字によっては、回転などが存在するので、Transformは、外部に持ってくる。
	Transform setTrans = trans;
	//回転値はリセット
	setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);


	//setTrans.position_ += addVec_;
	*/



	//分岐を示すフラグ([]されたら)
	bool branch = false;
	//回転したかのフラグ（直線の枝なのか、回転して生成した枝なのか。）
	bool rotate = false;


	//セット用のTransform
	Transform setTrans = parentTrans;
	//回転値はリセット
	setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);

	//移動量ベクトル
	XMVECTOR moveVec = XMVectorSet(0, 1, 0, 0);



	//描画するライン（素材）の回転値（Transform）	
		//初期値は上に伸びてほしいので、＋Y方向に１

	//置換する文字は、決められているので、
	//置換のルールにて決められているものを使用する
	//置換した後の文字列数分
	for (pos = 0; pos < sentence.size(); pos++)
	{
		//1バイト分、char1文字分取得
		auto current = sentence[pos];



		//もし1バイトの文字がFだったら、
		//描画しろ命令
		if (current == 'F')
		{
			//現在のTransform値に、移動ベクトルを足す
				//→ただ足すのではなく、現在の回転を行列として取得し、その回転分ベクトルを回転させる
				//回転した方向に一定量移動する。
			setTrans.position_ += moveVec;


			//現在のTransformで描画
			CreateTreeSource(setTrans);



			//分岐していないなら
			if (branch == false)
			{
				//自身のTransformを親の位置へ登録
					//枝分かれは考えずに、一本で完結する場合を考えて
				parentTrans = setTrans;


				//Transformをリセット
				//setTrans = parentTrans;
				//回転値はリセット
				setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);
				//setTrans.position_ += addVec_;


						//回転していない、まっすぐ伸びた枝ならば、
			//分岐位置に登録
				if (rotate == false)
				{
					branchTrans = parentTrans;

				}
			}
			//分岐しているなら
			else
			{
				//現在の位置を残したまま、
				//その位置から次の枝生成を行う

					//回転値はリセット
				setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);
			}
		}
		//＋回転しろ（一定量回転）
		else if (current == '+')
		{
			//Transform値を＋回転 (DirectXは、MAYAで作成したものと、逆になってしまうので注意)
			setTrans.rotate_.vecZ += 45 * (-1);

			//setTrans.position_ += addVec_;

			//描画はしない。
				//描画はFが呼ばれたら、
				//なので→次に呼ばれたFは、上記の回転で描画されるので、
				//+回転された描画となる。

		}
		// - 回転しろ
		else if (current == '-')
		{
			//Transform値を - 回転 
			setTrans.rotate_.vecZ -= 45 * (-1);

			//逆方向にベクトルを持ってきたいので、現段階ではX値だけ反転させる
			//setTrans.position_ += addVec_ * XMVectorSet(-1, 1, 1, 0);
		}
		// * +2倍回転しろ
		else if (current == '*')
		{
			//Transform値を + 回転
			setTrans.rotate_.vecZ += 90 * (-1);

			//setTrans.position_ += addVec_;
		}
		// / -2倍回転しろ
		else if (current == '/')
		{
			//Transform値を - 回転
			setTrans.rotate_.vecZ -= 90 * (-1);

			//足すベクトルを反転
			//setTrans.position_ -= addVec_ * XMVectorSet(-1, 1, 1, 0);
		}
		//枝分かれの分岐開始
		else if (current == '[')
		{
			//分岐開始
			//分岐中は枝を伸ばしても、枝の生える位置をリセットせずに、
			branch = true;

			//関数へ飛んで、]が見つかるまで関数先にて、枝生成を行う
			//枝の生成を行う分岐位置を送ってもらう
			BranchTree(branchTrans);
		}
		//枝分かれ終了、枝分かれを起こした分岐位置へ戻る
		else if (current == ']')
		{
			//分岐終了
			//分岐位置へ戻る
			setTrans = trans;

			//回転値はリセット
			setTrans.rotate_ = XMVectorSet(0, 0, 0, 0);

			branch = false;
		}

	}

	//LSYSTEMの枝生成が完了したので、
	//最後にまっすぐ伸ばした枝の位置を登録
	lastTreeTrans = branchTrans;


}

//LSYSTEMの枝生成と同じことをする
//[の文字を受けて、枝が分岐された後の枝生成
	//引数にてもらったTransform値から枝を生成していく（引数値を開始位置としてLSYSTEMを行う）
	//']'が見つかるまで、見つかったら帰る。
	//途中で[を見つけたら、さらに現在の分岐位置を送って再帰的に行う
//戻値は、なし＝分岐後は、その分岐先にて枝の生成を完結させたいので、その分岐先で最終的な分岐位置も必要なし（そこからさらに枝を生成することもないので。）
void LSystemTree::BranchTree(Transform trans)
{
	//必要な要素
	//前回の枝のTransform（自身が枝をはやさせる親の枝のTransform）→常に更新
	//初期値に、前回のLSYSTEMによって、分岐せずに直線に延ばされた枝のTransformを代入
	Transform parentTrans = trans;
	//分岐を起こした場合に分岐前の枝のTransformを保存する変数(分岐が終わった後に分岐した地点から枝を生やせるように)
	//分岐開始位置
	Transform branchTrans = trans;



}

#endif


//�D再帰の表現を行うのがLSYSTEMとも聞いた。→なので、このままでよいのか？？



