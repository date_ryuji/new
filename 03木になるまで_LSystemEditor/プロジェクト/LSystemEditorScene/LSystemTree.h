/*

	LSystemで、木を自動生成
	（こてこてで、メモリのことも考えていない）

	L_SYSTEMとは、
	文字列を変換していって、再帰的に分岐していくみたいな。


	https://www.youtube.com/watch?v=E1B4UoSQMFw&feature=youtu.be

	木生成のルール
	http://www.serenelight3d.com/blog55/?p=2126
*/


/*

	〇問題

	・文字列の増加によって、５，６回目あたりから極端に重くなる。
	  char*であるため、長さは問題ないとは思うが、常に持っておくには問題がある。



*/


#pragma once
#include <string>
#include <list>
#include <vector>
#include "../Engine/GameObject.h"


class Transform;
class BranchesMakeTree;
class Branch;
class CountTimer;
class FallenLeaves;


//■■シーンを管理するクラス
class LSystemTree : public GameObject
{
private : 
	//変形の判定Enum
	enum TRANSFORM_TYPE
	{
		TRANSFORM_F = 0  ,
		TRANSFORM_PLUS,
		TRANSFORM_MINUS,
		TRANSFORM_MULTIPLY , 
		TRANSFORM_DIVISION , 
		TRANSFORM_START_BRANCH,
		TRANSFORM_END_BRANCH,

		TRANSFORM_MAX,

	};

protected : 
	//枝オブジェクトを結合して作られる、木オブジェクト
	BranchesMakeTree* pBranchesMakeTree_;
	//木を作る枝オブジェクト
	Branch* pBranchSource_;

	//木オブジェクトのモデル番号
		//Model.cppに登録する際の、モデル番号を保存しておく
	int hBranchesMakeTree_;


	//消去モーション時のシェーダー切替えフラグ
	bool isEraseMotion_;
	//上記の時間計測を行うクラス
	CountTimer* pCountTimer_;

	//時間停止のフラグ
	//時間による行動、移動、攻撃、HP現象などなど様々な動的要因を停止する
	bool isStopTime_;

	//落ち葉クラス
	FallenLeaves* pFallenLeaves_;

protected : 
	//木オブジェクトに
	//新しい枝オブジェクトの登録
	void Join3DPolygon(Transform& trans);

	//消去モーションの実行を行う
		//そのフラグを立てる
		//シェーダーの切り替えも行う
	void StartEraseMotion();

	//消去モーションが終了したか
	bool EndEraseMotion();

	//時間が停止しているか
	bool IsStopTime();


protected:
	//LSYSTEM実行回数上限
	static const unsigned int LSYSTEM_COUNT_ = 2;

	//LSYSTEM実行回数
	unsigned int lsystemCounter_;

	//サイズの減少値
	static constexpr XMVECTOR SUB_VEC = { 0.5f,0.5f, 0.5f, 0.0f };



	//各階層で足されるベクトル値
	XMVECTOR addVec_;
	//枝分かれ時の足されるベクトル値
	XMVECTOR addBranchVec_;




	//枝のソースとなるオブジェクトの拡大率
	XMVECTOR modelScale_;


protected:
	//保存用文字列
	std::string axiom;
	//置換対象の文字列
	std::string REPLACEMENT_TARGET_;

	//置換後に文字列が代入される文字列
	std::string sentence;

	//自身のルールが複数ある場合
		//ルールの数（配列数）
		//const int ruleCount;

		/*
		//ルール数分のルールの確保
			//文字列置換のルールの文字列
		//エラー：静的でないメンバー参照は特定のオブジェクトを基準とする相対参照である必要があります
		//メンバ内で作成した定数で、要素数を指定して作成することができない
		//２次元配列のポインタとしたいので、ポインタのポインタで取得
		std::string** rules;
		*/
		//置換パターンのタイプ
	/*enum RULE_TYPE
	{
		RULE_FIXED = 0,
		RULE_B,
		RULE_C,
		RULE_D,
		RULE_E,


		RULE_END


	};*/



	//拡張のルール
	//LSystemTree::RULE_TYPE ruleType_;


	//文字列置換のルール
	//std::string rules[RULE_END];

	//文字列置換のルール
	std::string RULE_;




	//前回の階層の最終配置数
		//初期値はー１→０オリジンにするために、＋＋されたときにはじめて０にする形に
	int length;
	//次の階層の最終配置数
	int lastLength;

	//現在見ているsentenceの位置（要素位置）
	int pos;

	//前回の最終配置数から、順番に置換をしていくが、(最終配置数まで回す）
		//その時に、置換の時に回す値が、モデルに登録している番号
	//すべての置換が終了したとき、
		//次の階層の最終配置数を更新（今回登録した分を登録）

	//配置数の更新を行うタイミングを間違わないように





	//各モデル番号
		//消されない前提（なのでVector）
	std::vector<int> hModel_;

	//モデル番号（共通）
	int hCommonModel_;




	//描画するモデル番号（Drawにて回す）が入る配列の要素（添え字）
		//毎フレーム描画する番号を登録させておく
	std::list<int> drawModel_;

	//各Transform
		//消されない前提
		//描画を行う、枝のTransform値
	std::vector<Transform> modelTransform_;

	//分岐していない枝の現段階の最終地点のTransform(LSYSTEMが回った時に、一番初めに成長させるときにどこから始めるのかしめすTrans)
		//前回のLSYSTEMによる成長にて、最後に分岐せずにまっすぐ伸びた枝のTransform
		//★クラスはポインタではないので、すでに実態を持っている状態
	Transform lastTreeTrans;



protected:


	//LSYSTEMで、文字列置換を行う関数
	void StringReplacement();

	//樹木生成�A
	//樹木生成�B
	//LSYSTEMで、文字列置換を行い、
	//かつ、文字列を置換したのちに、その置換する文字列で作られた枝を親として、その位置から枝を制作する。
	void StringReplacement_2();

	//樹木生成�C
	void StringReplacement_4();


	//描画のデータを作成
	void CreateModel();

	//樹木生成�A
	//引数にてもらったTransformを枝を生やす節の枝として、
	//そのTransformを継承して、枝を作成
	void CreateModel(Transform trans);

	//樹木生成�B
	void CreateModel3(Transform trans);

	//樹木生成�C
	//lastTransformを使用して親の枝の位置を判断（LSYSTEMの枝生成の下記の関数内でも、lastTransformは更新される）
	void CreateModelTransform4();

	//条件による変形
	void TransformF(Transform& setTrans , XMVECTOR moveVec , Transform& branchTrans);
	void TransformSource(Transform& setTrans , XMVECTOR addRotateValue);



	TRANSFORM_TYPE ReplacementJudgment(char current);





	//枝分岐先の枝生成（再帰）
	void BranchTree(Transform trans);


	//木の枝、木を作る素材を作成
	//void CreateTreeSource(Transform trans);

	//落ち葉クラスのロード
	void LoadFallenLeaves(const std::string& textureFileName);





public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	LSystemTree(GameObject* parent);

	LSystemTree(GameObject* parent , std::string name);

	//初期化
	virtual void Initialize();
	//ルール指定、テクスチャ指定の初期化
	void Initialize(const std::string& RULE , const std::string& TEXTURE_FILE_NAME);
	//更新
	virtual void Update();
	//開放
	virtual void Release();

public : 
	//時間の停止（行動、移動、攻撃などの停止）
	void StopTime();
	//時間の再開（行動、移動、攻撃などの再開）
	void StartTime();



	//ルール作成
		//LSYSTEM実行の
	void CreateRules();

	//一番目の枝
		//木の幹となる枝の生成
	void CreateTheFirstOne(Transform& trans);
	void CreateTheFirstOne();

	//描画
	void Draw() override;
	void DrawBranchesMakeTree(Transform& trans);
	//枝の描画実行
	void DrawBranch();



	//Transformを変更
		//引数にてもらったTransformを、自身のTransformにコピーさせる
	void SetTransform(Transform* trans);

	//LSYSTEMを実行可能かどうかを調べる
		//LSYSTEM実行可能回数上限にたっていないか
		//上限に達している　FALSE、上限に達していない　TRUE
	bool ExecutableLSystem();

	//LSYSTEM実行回数を取得
	int GetLSystemCounter();



	//LSYSTEM実行
	void ExecutionLsystem();


};

