#include "LSystemEditorSceneManager.h"
#include <string>

#include "../Engine/SceneManager.h"
#include "../Engine/TextWriter.h"
#include "../Engine/Global.h"
#include "../Engine/Camera.h"


#include "../resource.h"
#include "../Procedure/DialogProcedure.h"
#include "../Procedure/WindowProcedure.h"

//マネージャー内にて生成、管理するオブジェクト
#include "Tree.h"



//コンストラクタ
LSystemEditorSceneManager::LSystemEditorSceneManager(GameObject* parent)
	: GameObject(parent, "LSystemEditorSceneManager"),
	pattern_("")
{


}

//初期化
void LSystemEditorSceneManager::Initialize()
{

	//自身が使用するダイアログの描画を行う
	DialogProcedure::ShowDialogLeavedItToClass(DIALOG_TYPE_LSYSTEM_EDITOR);


	//LSYSTEMクラスなどの作成

	//作成しなおす場合は、
		//一度KillMeして、
		//インスタンスを作り直した方がよさそう


	//カメラ位置セット
	//Camera::SetPosition(Camera::position_ + XMVectorSet(0.f, 0.f, -5.f, 0.f));

	Camera::SetPosition(XMVectorSet(0.f, 15.f, -50.f, 0.f));
	Camera::SetTarget(XMVectorSet(0.f, 15.f, 0.f, 0.f));

}

//更新
void LSystemEditorSceneManager::Update()
{

}

//描画
void LSystemEditorSceneManager::Draw()
{
	//Treeの描画
	//描画はTreeクラス自身で行っていないので描画
	if (pTree_ != nullptr)
	{
		pTree_->DrawBranchesMakeTree(transform_);
	}
}

//開放
void LSystemEditorSceneManager::Release()
{

}


BOOL LSystemEditorSceneManager::DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	//msgに対する
	switch (msg)
	{
		//何かメッセージを受け取ったら、メッセージ全般
	case WM_COMMAND:
	{
		//引数のwpの、LOWORD（wp）にコントロールのIDが入ってくる
		//そのためこのコントロールのIDから、誰に変更が加えられたのか判断。処理を判断する。
		switch (LOWORD(wp))
		{
			//パターン　ボタンが押下されたら
		case LSYSTEM_BUTTON_F:
		{
			AddChar('F');
			DrawDialogText(hDlg);	//テキスト更新
		}break;
		case LSYSTEM_BUTTON_PLUS:
		{
			AddChar('+');
			DrawDialogText(hDlg);	//テキスト更新
		}break;
		case LSYSTEM_BUTTON_MINUS:
		{
			AddChar('-');
			DrawDialogText(hDlg);	//テキスト更新
		}break;
		case LSYSTEM_BUTTON_MULTI:
		{
			AddChar('*');
			DrawDialogText(hDlg);	//テキスト更新
		}break;
		case LSYSTEM_BUTTON_DIVISOR:
		{
			AddChar('/');
			DrawDialogText(hDlg);	//テキスト更新
		}break;
		case LSYSTEM_BUTTON_BRANCH_START:
		{
			AddChar('[');
			DrawDialogText(hDlg);	//テキスト更新
		}break;
		case LSYSTEM_BUTTON_BRANCH_END:
		{
			AddChar(']');
			DrawDialogText(hDlg);	//テキスト更新
		}break;

		//クリアボタン
		case LSYSTEM_CLEAR:
		{
			//パターンの初期化
			ClearPattern();
			DrawDialogText(hDlg);	//テキスト更新
		}break;

		//LSYSTEM実行
			//LSYSTEMの作成
			//枝１本目の描画
		case LSYSTEM_EXE : 
		{
			//現在のインスタンス削除
			if (pTree_ != nullptr)
			{
				pTree_->KillMe();
			}

			//木のインスタンス生成
			//元のポインタのオブジェクトは、KillMeされて、次のフレームには存在しなくなる
			pTree_ = (Tree*)Instantiate<Tree>(this);




			//現在選択されている、
				//タイプを引数として、テクスチャを選択し生成する
			//現在のタイプ
				//コンボボックスの現在選択されているタイプを取得
			int num = SendMessage(GetDlgItem(hDlg, LSYSTEM_TEXTURE_TYPE),
				CB_GETCURSEL,
				0, 0);

			//現在のタイプと
			//パターンの文字列を渡す
			pTree_->Initialize((TREE_TYPES)num , pattern_);

			//枝を一つ生成
				//下記の関数を呼ばなければ、LSYSTEMを行うことができないので注意
			pTree_->CreateTheFirstOne();
		}break;


		//LSYSTEM進める
			//生成した枝のLSYSTEMを実行させる
		case LSYSTEM_ADVANCE : 
		{
			if (pTree_ != nullptr)
			{
				//実行可能回数より現在の
				// LSYSTEM回数が少ない。
				//実行可能なら
				if (pTree_->ExecutableLSystem())
				{
					//Lsystem一回実行
					pTree_->ExecutionLsystem();
				}
			}

		}break;



		}
	}

	//	case BUTTON_CREATE:
	//	{
	//		//作成実行
	//		ExeCreate();

	//		//ダイアログの情報を初期に戻す
	//		{
	//			//Depth , Size
	//			//スライダーを初期位置にセットしなおす
	//			//SendDlgItemMessage(hDlg, SLIDER_WIDTH, TBM_SETPOS, TRUE, 0);
	//			SendDlgItemMessage(hDlg, SLIDER_DEPTH, TBM_SETPOS, TRUE, 0);
	//			SendDlgItemMessage(hDlg, SLIDER_POLY_SIZE, TBM_SETPOS, TRUE, 0);

	//			//テキストの更新
	//			//消えるときは、
	//			//変数なども消してしまうので、値自体は変更の必要性がない
	//			{
	//				//std::string widthStr = std::to_string(MIN_WIDTH_);
	//				//SetDlgItemText(hDlg, TEXT_WIDTH, widthStr.c_str());
	//				//UpdateText(hDlg, TEXT_WIDTH, MIN_WIDTH_);
	//				UpdateText(hDlg, TEXT_DEPTH, MIN_DEPTH_);
	//				UpdateText(hDlg, TEXT_POLY_SIZE, MIN_SIZE_);
	//			}

	//			//Width
	//			SendMessage(GetDlgItem(hDlg, COMBO_WIDTH),
	//				CB_SETCURSEL,
	//				0, 0);
	//		}

	//	}break;
	//	case COMBO_WIDTH:
	//	{
	//		//コンボボックスの選択番号を取得
	//			//返り値に一番小さい値として、０が返ってくるので、
	//			//最小値の値を返り値分だけ回して、その値をWidthとする
	//			//相すれば、　2の乗数として値を確定できる
	//		//選択されたコンボボックスの選択番号順を取得

	//		int num = SendMessage(GetDlgItem(hDlg, COMBO_WIDTH),
	//			CB_GETCURSEL,
	//			0, 0);

	//		//コンボボックスの選択番号を
	//		//最小値の階乗として用いて
	//			//widthを求める
	//		SetWidth(num);

	//	}break;








	//	////スタティックテキスト
	//	//	//テキスト内容の更新
	//	//SetDlgItemText(hDlg, TEXT_WIDTH, "2");
	//	//SetDlgItemText(hDlg, TEXT_DEPTH, "2");
	//	//SetDlgItemText(hDlg, TEXT_POLY_SIZE, "0.01");


	//	////任意の位置にセット
	//	//SendDlgItemMessage(hDlg, SLIDER_DEPTH, TBM_SETPOS, TRUE, 2);




	//	}
	//}break;

	/////ダイアログが再表示されたとき
	//case WM_INITMENUPOPUP:
	//{





	//}break;

	////スクロールのバーが触られたとき
	//case WM_HSCROLL:
	//case WM_VSCROLL:
	//	switch (LOWORD(wp))
	//	{
	//	case SLIDER_DEPTH:
	//	{
	//		//現在の位置を取得	
	//				// 0 ~ 100
	//			//int を /100 して、0.00 ~ 1.00fのfloat型にする
	//				//ここで、 / 0になる可能性があるため、　*0.01fで代用
	//		float per = SendDlgItemMessage(hDlg, SLIDER_WIDTH, TBM_GETPOS, 0, 0) * 0.01f;

	//		//割合を使って、
	//			//Widthの最高と最低とで、線形補完をして、
	//			//最高と最低を線で繋いだ時に、perの割合の値を求める
	//		width_ = LinearInterpolationInt(MAX_WIDTH_, MIN_WIDTH_, per);


	//		std::string str = std::to_string(width_);

	//		//テキスト表示
	//			//第3引数にて、文字列のポインタ部分を与えることで、LPCSTR(文字列のポインタ)を通すことが可能
	//		SetDlgItemText(hDlg, TEXT_WIDTH, str.c_str());

	//	}break;







	//	}


	//	if ((HWND)lp == GetDlgItem(hDlg, SLIDER_DEPTH))
	//	{
	//		SetDepth(hDlg, SLIDER_DEPTH, TEXT_DEPTH, &depth_, MAX_DEPTH_, MIN_DEPTH_);
	//	}
	//	/*	if ((HWND)lp == GetDlgItem(hDlg, SLIDER_WIDTH))
	//		{
	//			SetDepth(hDlg, SLIDER_WIDTH, TEXT_WIDTH, &width_, MAX_WIDTH_, MIN_WIDTH_);
	//		}*/
	//	if ((HWND)lp == GetDlgItem(hDlg, SLIDER_POLY_SIZE))
	//	{
	//		SetSize(hDlg, SLIDER_POLY_SIZE, TEXT_POLY_SIZE, &size_, MAX_SIZE_, MIN_SIZE_);
	//	}





	//	break;


	//	//if (LOWORD(wp) == SLIDER_DEPTH)
	//	//{
	//	//	//現在の位置を取得	
	//	//			// 0 ~ 100
	//	//		//int を /100 して、0.00 ~ 1.00fのfloat型にする
	//	//			//ここで、 / 0になる可能性があるため、　*0.01fで代用
	//	//	float per = SendDlgItemMessage(hDlg, SLIDER_WIDTH, TBM_GETPOS, 0, 0) * 0.01f;

	//	//	//割合を使って、
	//	//		//Widthの最高と最低とで、線形補完をして、
	//	//		//最高と最低を線で繋いだ時に、perの割合の値を求める
	//	//	width_ = LinearInterpolationInt(MAX_WIDTH_, MIN_WIDTH_, per);


	//	//	std::string str = std::to_string(width_);

	//	//	//テキスト表示
	//	//		//第3引数にて、文字列のポインタ部分を与えることで、LPCSTR(文字列のポインタ)を通すことが可能
	//	//	SetDlgItemText(hDlg, TEXT_WIDTH, str.c_str());


	//	//}

	//	//switch ((HWND)lp)
	//	//{
	//	//case GetDlgItem(hDlg, SLIDER_WIDTH):
	//	//{
	//	//	//現在の位置を取得	
	//	//		// 0 ~ 100
	//	//	//int を /100 して、0.00 ~ 1.00fのfloat型にする
	//	//		//ここで、 / 0になる可能性があるため、　*0.01fで代用
	//	//	float per = SendDlgItemMessage(hDlg, SLIDER_WIDTH, TBM_GETPOS, 0, 0) * 0.01f;

	//	//	//割合を使って、
	//	//		//Widthの最高と最低とで、線形補完をして、
	//	//		//最高と最低を線で繋いだ時に、perの割合の値を求める
	//	//	width_ = LinearInterpolationInt(MAX_WIDTH_, MIN_WIDTH_, per);


	//	//	std::string str = std::to_string(width_);

	//	//	//テキスト表示
	//	//		//第3引数にて、文字列のポインタ部分を与えることで、LPCSTR(文字列のポインタ)を通すことが可能
	//	//	SetDlgItemText(hDlg, TEXT_WIDTH, str.c_str());




	//	//	
	//	//}break;




	}


	return FALSE;
}

void LSystemEditorSceneManager::CreatePatternText()
{
	//現在登録されているパターンをテキストに出力
	//書き込みクラスの作成

	//引数：区切り文字タイプ（改行）
	TextWriter* pTextWriter = new TextWriter(DELIMITER_NEW_LINE);

	//パターン文字追加
	pTextWriter->PushBackString(pattern_ , true);

	//テキスト追加終了
	pTextWriter->PushEnd();
	//ファイル書き出し
	pTextWriter->FileWriteExcecute("Assets/Scene/OutputText/LSystem.txt");



}


void LSystemEditorSceneManager::AddChar(char c)
{
	//引数文字を結合
	pattern_.push_back(c);
}

void LSystemEditorSceneManager::ClearPattern()
{
	pattern_ = "";
}

void LSystemEditorSceneManager::DrawDialogText(HWND hDlg)
{
	//テキスト更新
		//メンバにて管理している文字列を描画
	SetDlgItemText(hDlg, LSYSTEM_OUTPUT_TEXT, pattern_.c_str());
}


