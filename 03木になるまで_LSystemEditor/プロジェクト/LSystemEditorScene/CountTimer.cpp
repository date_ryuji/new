#include "CountTimer.h"
#include "../Engine/Timer.h"
#include "../Engine/Global.h"



CountTimer::CountTimer(GameObject * parent)
	//elpasedTime_(0.0f),
	//remaingTime_(0.0f),
	//endTimeForCountUp_(0.0f),
	//endTimeForCountDown_(0.0f)
{
	pTimers_.clear();
}

void CountTimer::Initialize()
{
	//タイマーを初期化
	//Timer::Reset();


}

void CountTimer::Update()
{
	//基本的にカウントアップ、カウントダウンのどちらも計算する
		//そのため、きちんと計算開始を行うには、まず、スタートの関数を呼び出す必要がある、

	//複数計測に対応しているため、
		//それぞれの計測を行う
	for (int i = 0; i < pTimers_.size(); i++)
	{
		//タイマーの計測許可されているか
		if (pTimers_[i]->permit)
		{



			//タイマー：カウントアップ
			//経過時間が
			//終了時間以下
			if (pTimers_[i]->endTimeForCountUp >= pTimers_[i]->elpasedTime)
			{
				//デルタタイム
					//フレームの経過時間を計測
				pTimers_[i]->elpasedTime += Timer::GetDelta();
			}
			else
			{
				//終了時間で更新
				pTimers_[i]->elpasedTime = pTimers_[i]->endTimeForCountUp;
			}



			//タイマー：カウントダウン
			//残り時間が
			//終了時間以上
			if (pTimers_[i]->endTimeForCountDown <= pTimers_[i]->remaingTime)
			{
				//デルタタイム
					//フレームの経過時間を計測
				pTimers_[i]->remaingTime -= Timer::GetDelta();
			}
			else
			{
				//終了時間で更新
				pTimers_[i]->remaingTime = pTimers_[i]->endTimeForCountDown;
			}

		}

	}



	////タイマー：カウントアップ
	////経過時間が
	////終了時間以下
	//if (endTimeForCountUp_ >= elpasedTime_)
	//{
	//	//デルタタイム
	//		//フレームの経過時間を計測
	//	elpasedTime_ += Timer::GetDelta();
	//}
	//else
	//{
	//	//終了時間で更新
	//	elpasedTime_ = endTimeForCountUp_;
	//}



	////タイマー：カウントダウン
	////残り時間が
	////終了時間以上
	//if (endTimeForCountDown_ <= remaingTime_)
	//{
	//	//デルタタイム
	//		//フレームの経過時間を計測
	//	remaingTime_ -= Timer::GetDelta();
	//}
	//else
	//{
	//	//終了時間で更新
	//	remaingTime_ = endTimeForCountDown_;
	//}

}

void CountTimer::Draw()
{
}

void CountTimer::Release()
{
	for (int i = 0; i < pTimers_.size(); i++)
	{
		//解放
		SAFE_DELETE(pTimers_[i]);
	}
}

int CountTimer::StartTimerForCountDown(float startTime, float endTime)
{
	//新規タイマーの作成
	TimerInfo* pNewTimer = new TimerInfo;

	pNewTimer->remaingTime = startTime;
	pNewTimer->endTimeForCountDown = endTime;


	//リストに登録して、
	pTimers_.push_back(pNewTimer);
	//リストの要素数 - 1 = にて、タイマーへのハンドルを返す
	return pTimers_.size() - 1;

}
int CountTimer::StartTimerForCountUp(float startTime, float endTime)
{
	//新規タイマーの作成
	TimerInfo* pNewTimer = new TimerInfo;

	pNewTimer->elpasedTime = startTime;
	pNewTimer->endTimeForCountUp = endTime;


	//リストに登録して、
	pTimers_.push_back(pNewTimer);
	//リストの要素数 - 1 = にて、タイマーへのハンドルを返す
	return pTimers_.size() - 1;
}

void CountTimer::ReStartTimerForCountDown(int handle, float startTime, float endTime)
{
	//ハンドル番号から、
		//時間をセットしなおす
		//リセットとは違い、リセットは、すべての要素を０にするが、
		//ReStartは、計測のし直しになる。（最初から、カウントアップ、カウントダウンを行う）

	pTimers_[handle]->remaingTime = startTime;
	pTimers_[handle]->endTimeForCountDown = endTime;

	//自フレームから、
		//タイマーをリスタートして、計測を開始することが可能となる

}

void CountTimer::ReStartTimerForCountUp(int handle, float startTime, float endTime)
{
	pTimers_[handle]->elpasedTime = startTime;
	pTimers_[handle]->endTimeForCountUp = endTime;
}

float CountTimer::GetRemaingTimer(int handle)
{
	return pTimers_[handle]->remaingTime;
}

float CountTimer::GetEndTimerForCountDown(int handle)
{
	return pTimers_[handle]->endTimeForCountDown;
}
float CountTimer::GetElpasedTime(int handle)
{
	return pTimers_[handle]->elpasedTime;
}

float CountTimer::GetEndTimerForCountUp(int handle)
{
	return pTimers_[handle]->endTimeForCountUp;
}

bool CountTimer::EndOfTimerForCountDown(int handle)
{
	return pTimers_[handle]->remaingTime <= pTimers_[handle]->endTimeForCountDown;
}

bool CountTimer::EndOfTimerForCountUp(int handle)
{
	return pTimers_[handle]->elpasedTime >= pTimers_[handle]->endTimeForCountUp;
}

void CountTimer::NotPermittedTimer(int handle)
{
	pTimers_[handle]->permit = false;
}

void CountTimer::PermitTimer(int handle)
{
	pTimers_[handle]->permit = true;
}

bool CountTimer::IsPermitTimer(int handle)
{
	return pTimers_[handle]->permit;
}


void CountTimer::ResetTimer(int handle)
{

	//セットしなおす専用の関数へ
		//スタート、エンドを０にセットする
	ReStartTimerForCountDown(handle, 0.0f, 0.0f);
	ReStartTimerForCountUp(handle, 0.0f, 0.0f);

}

CountTimer::TimerInfo::TimerInfo():
	permit(true),	//初期から、タイマーの計測を許可する
	elpasedTime(0.f),
	remaingTime(0.f),
	endTimeForCountUp(0.f),
	endTimeForCountDown(0.f)
{
}
