#pragma once
#include "../Engine/GameObject.h"

//自身クラスの、各要素
//シェーダーや、SkyBoxや、テキストなどなど
//その使い方


class Texts;
//class Controller;
class Player;

//■■シーンを管理するクラス
class SampleScene : public GameObject
{
	////コントローラークラス
	//Controller* pController_;
	//プレイヤー
	Player* pPlayer_;

	//テキスト群
	Texts* pTexts_;

	//画像ハンドル
	int hImage_;


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SampleScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

