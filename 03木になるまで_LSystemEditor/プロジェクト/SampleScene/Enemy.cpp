#include "Enemy.h"
#include "../Engine/Model.h"
#include "../Engine/SphereCollider.h"

#include "../Engine/BoxCollider.h"

#include "../Shader/CommonDatas.h"



//コンストラクタ
Enemy::Enemy(GameObject * parent)
	: GameObject(parent, "Enemy"),
	hModel_(-1)
{
}

//初期化
void Enemy::Initialize()
{
	hModel_ = Model::Load("Assets/DebugCollider/DebugBoxCollider.fbx" , FBX_POLYGON_GROUP, SHADER_NORMAL);
	assert(hModel_ != -1);

	//transform_.position_.vecZ = 3.0f;

	//球コライダーの生成
	SphereCollider* pColl = new SphereCollider(XMVectorSet(0.f, 0.f, 0.f, 0.f), 3.0f , true);

	//箱コライダーの作成
	//BoxCollider* pColl = new BoxCollider(XMVectorSet(0.f, 0.f, 0.f, 0.f), XMVectorSet(2.f, 2.f, 2.f, 0.f) , true);

	//コライダーを
	//オブジェクト自身のコライダーリストへ追加
	AddCollider(pColl);



}

//更新
void Enemy::Update()
{
	const float SPEED = 0.1f;
	static int code = 1;


	transform_.position_.vecX += SPEED * code;

	if (transform_.position_.vecX < -3.0f
		|| transform_.position_.vecX > 3.0f)
	{
		code *= -1;
	}

	
}

//描画
void Enemy::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

}

//開放
void Enemy::Release()
{
}

void Enemy::OnCollision(GameObject * pTarget)
{

}
