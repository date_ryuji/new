#include "Player.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "../Engine/Model.h"
//タマモデル
#include "../SampleScene/Bullet.h"

//コンストラクタ
Player::Player(GameObject * parent)
	: GameObject(parent, "Player"),
	hModel_(-1)
{
	
}

//初期化
void Player::Initialize()
{
	hModel_ = Model::Load("Assets/DebugCollider/DebugSphereCollider.fbx");
	assert(hModel_ != -1);



}

//更新
void Player::Update()
{
	//タマ発射
	if (Input::IsKeyDown(DIK_SPACE))
	{
		//ここにおける弾は、
			//親における（０，０，０，０）から座標を更新し続ける
				//子供のTransformは、親のTransformを考慮して、自身のTransform値分移動する。
		Instantiate<Bullet>(this);
		//Instantiate<Bullet>(pParent_);
	}

	const float SPEED = 0.1f;
	XMVECTOR move = XMVectorSet(
		Input::IsKey(DIK_D) - Input::IsKey(DIK_A),
		Input::IsKey(DIK_UP) - Input::IsKey(DIK_DOWN),
		Input::IsKey(DIK_W) - Input::IsKey(DIK_S), 0.f);
	XMVECTOR moveRotate = XMVectorSet(Input::IsKey(DIK_RETURN) - Input::IsKey(DIK_LSHIFT), Input::IsKey(DIK_RIGHT) - Input::IsKey(DIK_LEFT), 0.f, 0.f);

	move *= SPEED;
	moveRotate *= SPEED;



	//移動
	transform_.position_ = transform_.position_ + move;
	//回転
	transform_.rotate_ += moveRotate;
	//Y軸の回転行列
	XMMATRIX matY = XMMatrixRotationY(transform_.rotate_.vecY);
	{
		//移動方向
		XMVECTOR front = XMVectorSet(0, 5, -10, 0);
		
		transform_.Calclation();
		XMVECTOR worldVec = XMVector3TransformCoord(transform_.position_ , transform_.GetWorldMatrix());
		


		//回転させる
		front = XMVector3TransformCoord(front, matY);
		Camera::SetPosition(transform_.position_ + front);
	
	}
	
	Camera::SetTarget(transform_.position_);
	////焦点
	//{
	//	//焦点位置
	//	XMVECTOR targetDis = XMVectorSet(0.f, 0.f, 3.f, 0.f);
	//
	//	//回転後の位置
	//		//現在の回転量を求めて、
	//		//回転したときのベクトル位置を求める
	//	targetDis = XMVector3TransformCoord(targetDis, matY);

	//	Camera::SetTarget(transform_.position_ + targetDis);
	//}



}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

}

//開放
void Player::Release()
{
}