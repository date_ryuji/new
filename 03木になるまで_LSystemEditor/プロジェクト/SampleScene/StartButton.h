#pragma once
#include "../Engine/Button.h"

class StartButton : public Button
{
protected : 


	//接触時の処理
	//接触しているときに、毎フレーム呼ばれるので注意
	//継承先にてオーバーライドして、ボタンと接触しているときの処理を行う
	//例：接触しているときに、マウスのクリックが押されたときに、　ボタンをONとして、何か行動させる
	//例：接触しているときに、マウスのクリックが押されたときに、　ボタンをOFFとして、何か行動させる
	virtual void OnContactButton();
	
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	StartButton(GameObject* parent);

	//デストラクタ
	~StartButton() override;

	//初期化
	virtual void Initialize() override;


	//更新
	virtual void Update() override;


	//描画
	virtual void Draw() override;


	//開放
	virtual void Release() override;



};

