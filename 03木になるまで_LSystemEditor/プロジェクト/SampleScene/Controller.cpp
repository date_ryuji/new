#include "Controller.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "../Engine/Model.h"

//コンストラクタ
Controller::Controller(GameObject * parent)
	: GameObject(parent, "Controller")
{
	//回転度数
	SPEED_ = 15.0f;
	standardPixX_ = 100.0f;
	standardPixY_ = 100.0f;	//１００ピクセルで　0.01回転する
				//前回と今回とで、割合を取り、
				//割合＊SPEEDにて、　回転量とする

}

//初期化
void Controller::Initialize()
{
	//transform_.position_ = Camera::GetPosition();
	beforePixPos_ = Input::GetMouseCursorPosition();

	transform_.position_ = Camera::GetPosition();


}

//更新
void Controller::Update()
{




	XMVECTOR move = XMVectorSet(
		Input::IsKey(DIK_D) - Input::IsKey(DIK_A),
		Input::IsKey(DIK_UP) - Input::IsKey(DIK_DOWN),
		Input::IsKey(DIK_W) - Input::IsKey(DIK_S),0.f);
	const float SPEED = 0.1f;

	XMVECTOR moveRotate = XMVectorSet(Input::IsKey(DIK_RETURN) - Input::IsKey(DIK_LSHIFT), Input::IsKey(DIK_RIGHT) - Input::IsKey(DIK_LEFT), 0.f, 0.f);

	move *= SPEED;
	moveRotate *= SPEED;
	
	/*
	if (Input::IsKey(DIK_W))
	{
		move.vecZ += SPEED;
	}
	if (Input::IsKey(DIK_S))
	{
		move.vecZ -= SPEED;
	}
	if (Input::IsKey(DIK_A))
	{
		move.vecX -= SPEED;
	}
	if (Input::IsKey(DIK_D))
	{
		move.vecX += SPEED;
	}
*/



	{
		

		transform_.position_ = transform_.position_ + move;

//		XMVECTOR camVec = Camera::GetPosition();
		Camera::SetPosition(transform_.position_ - XMVectorSet(0,0,-2,0));

		//Camera::SetTarget(transform_.position_ + XMVectorSet(0, 0, 2, 0));
	}
	{
		//回転
		transform_.rotate_ += moveRotate;
		//回転行列
		XMMATRIX matY = XMMatrixRotationY(transform_.rotate_.vecY);

		XMVECTOR targetDis = XMVectorSet(0.f, -1.f, 5.f, 0.f);



		//回転後の位置
			//現在の回転量を求めて、
			//回転したときのベクトル位置を求める
		targetDis = XMVector3TransformCoord(targetDis , matY);

		Camera::SetTarget(transform_.position_ + targetDis);
	}

		/*Inputクラスからマウスの移動量を取得*/
		//移動量に伴って、回転

	//{
	//	//マウスの移動量を取得
	//	XMVECTOR moveMouseCursor = Input::GetMouseMove();



	//	//if (moveMouseCursor.vecX != 0 && moveMouseCursor.vecY != 0)
	//	{
	//		//カメラの回転
	//		//マウスの移動によって、回転
	//		//XMVECTOR sub = mouseCursor - beforePixPos_;
	//		//基準の移動ピクセルをもとに、
	//			//割合を求める
	//			//基準ピクセルをもとに、どの程度進むのか


	//		float perX;
	//		if (moveMouseCursor.vecX != 0)
	//		{
	//			//基準のピクセル値から
	//				//移動量のピクセルの割合を求める
	//				//どれだけ移動したのか
	//			perX = moveMouseCursor.vecX / standardPixX_;
	//			//０との除算をすると、
	//			//答えにinfという値が入ってしまいエラーの原因になるので、0の場合を除く計算とする
	//		}
	//		else
	//		{
	//			perX = 0;
	//		}
	//		float perY;
	//		if (moveMouseCursor.vecY != 0)
	//		{
	//			perY = moveMouseCursor.vecY / standardPixY_;
	//		}
	//		else
	//		{
	//			perY = 0;
	//		}


	//		transform_.rotate_.vecX += SPEED_ * (perY);
	//		//今回は移動量をもらうので、前回より下に言っていたらー　となるので、
	//		//ーを回転方向と合わせるために、変える必要がない


	//		transform_.rotate_.vecY += SPEED_ * (perX);


	//		//回転
	//		XMMATRIX matX = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX));
	//		XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));





	//		XMVECTOR targetDis = XMVectorSet(0.f, -1.f, 10.f, 0.f);

	//		//回転を掛ける
	//		targetDis = XMVector3TransformCoord(targetDis, matX * matY);

	//		Camera::SetTarget(transform_.position_ + targetDis);


	//	}
	//}



}

//描画
void Controller::Draw()
{
}

//開放
void Controller::Release()
{
}