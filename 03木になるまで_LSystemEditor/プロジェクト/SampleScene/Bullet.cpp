#include "Bullet.h"
#include "../Engine/Model.h"
#include "../Engine/SphereCollider.h"
#include "../Engine/BoxCollider.h"


//コンストラクタ
Bullet::Bullet(GameObject * parent)
	: GameObject(parent, "Bullet"),
	hModel_(-1),
	pSpColl_(nullptr),
	beforePos_(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}

//初期化
void Bullet::Initialize()
{
	hModel_ = Model::Load("Assets/DebugCollider/DebugSphereCollider.fbx");
	assert(hModel_ != -1);

	//transform_.position_ = pParent_->transform_.position_ - XMVectorSet(0.f, 0.f, 2.f, 0.f);
		//自身の所有するTransformというのは、
		//決して、ワールドにおける座標ではないということを理解する

		//Transformを計算するときに、
		//親のTransformを考慮して、自身の位置を計算する。つまり、親のTransformを原点として、自身のTransformの離れ具合を実装する。

		//自身のTransformが仮に（１，１，１，０）だったら、
			//ワールドにおける１，１，１０になるのではなくて、
		//親の原点から、（１，１，１，０）となる。

		//それを考慮した座標管理にする。

	//なので、
		//ワールド座標系で管理したいのならば、いろいろと座標に変化が必要。
	//transform_.position_.vecZ = -5;


	//今回においては特別で、
		//コントローラーで、カメラの位置を動的に切り替えている、
		//なので、
		//本来であれば、プレイヤーがいて、プレイヤーをカメラが見ているのであれば、
		//そのプレイヤーの原点から出現させるので、　Bulletクラスで位置を原点から始めるようにすればよい。
	//今回においては特別で、
		//カメラの位置と、見る位置を変えているので、
		//親の座標がカメラの位置と連携していない、
		//それをそろえるために、座標を変換している。

	//transform_.position_ = 








	//直径　＝　0.5f　となる
	//つまり、半径　＝　0.25f となる
	//つまり、コライダー第2引数の半径　＝　0.25f
	//transform_.scale_ = XMVectorSet(0.5f, 0.5f, 0.5f, 0.f);
	//transform_.position_.vecY = 0;


	//球コライダーの生成
	//コライダーの、オブジェクトからの離れ具合
	//半径　→半径なので、　半径0.5 = 直径1.0　となる。つまり、　MAYA上で１．０ｆの大きさで作った球は、直径１．０、半径0.5となる。コライダーを作成するときに、そのサイズを考慮して半径を設定する必要がある。
	
	//モデルサイズ　＝　球サイズ＝　1.0f = 半径 0.5f
	//第２引数：0.5f(半径)
	//SphereCollider* pColl = new SphereCollider(XMVectorSet(0.f, 0.f, 0.f, 0.f) , 0.5f);
	//モデルサイズ　＝　球サイズ＝　0.5f = 半径 0.25f
	//第２引数：0.25f(半径)
	//第3引数：コライダーを視認できるようにするか(現在は、視認できやすいように、サイズを、0.25上げている)//MAYA上での大きさと、あっていない。
	//SphereCollider* pColl = new SphereCollider(XMVectorSet(0.f, 0.f, 0.f, 0.f) , 2.5f , true);
	//pSpColl_ = pColl;
	


	//箱コライダーの作成
	BoxCollider* pColl = new BoxCollider(XMVectorSet(0.f, 0.f, 0.f, 0.f), XMVectorSet(3.0f, 3.0f, 3.0f, 0.f), true);
	pBoxColl_ = pColl;

	//コライダーを
	//オブジェクト自身のコライダーリストへ追加
	AddCollider(pColl);


	//コライダーの性能を
	//壁ずり実行のコライダーに変更
	pColl->SetColliderFunctionType(ALONG_THE_WALL_FUNCTION);

}

//更新
void Bullet::Update()
{
	beforePos_ = transform_.position_;

	transform_.position_.vecZ += 0.1f;

	if (transform_.position_.vecZ > 10.f)
	{
		KillMe();
	}


}

//描画
void Bullet::Draw()
{
 	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

}

//開放
void Bullet::Release()
{
}

void Bullet::OnCollision(GameObject * pTarget)
{
	


	//敵がEnemyであるとき
	if (pTarget->objectName_ == "Enemy")
	{
		/*int a = 0;
		pTarget->KillMe();
		KillMe();*/

		//壁ずりの前に
		//移動量を戻す
			//→前回の移動（衝突判定のタイミングから考えると、前回のフレームのUpdateの時の座標、→この処理が終了後に、今回のフレームのUpdateが呼ばれる）
		//transform_.position_ = beforePos_;


		//壁ずりの実行
		//pSpColl_->AlongTheWall();
		pBoxColl_->AlongTheWall();

	}

}
