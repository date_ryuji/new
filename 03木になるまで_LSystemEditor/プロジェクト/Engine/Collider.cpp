#include "Collider.h"
#include "GameObject.h"
#include "Global.h"
#include "SphereCollider.h"
#include "BoxCollider.h"
#include "DebugWireFrame.h"


Collider::Collider(XMVECTOR position ,COLLIDER_TYPE collType ,FUNCTION_TYPE funcType ,  bool drawWireFrame):
	myPos_(position),
	myColliderType_(collType),
	myFunctionType_(funcType),
	pDebugWireFrame_(new DebugWireFrame),
	pTarget_(nullptr),
	pGameObject_(nullptr)
{
	//ワイヤーフレームモデルのロード
	pDebugWireFrame_->Load(collType);
	//描画状態の設定
	pDebugWireFrame_->DrawWireFrame(drawWireFrame);
}

//Collider::Collider(XMVECTOR position, XMVECTOR size)
//{
//}
//
//Collider::Collider(XMVECTOR position, float size)
//{
//}

Collider::~Collider()
{
}

void Collider::SetMyGameObject(GameObject * pGameObject)
{
	pGameObject_ = pGameObject;
	pDebugWireFrame_->SetParentTransform(&pGameObject_->transform_);
}

DebugWireFrame * Collider::GetDebugWireFrame()
{
	return pDebugWireFrame_;
}

void Collider::DoJudge()
{
	doJudge_ = true;
}

void Collider::DoNotJudge()
{
	doJudge_ = false;
}

bool Collider::IsHit(Collider * target)
{
	return false;
}

void Collider::Draw()
{
	pDebugWireFrame_->Draw();
}

void Collider::DrawWireFrame(bool drawWireFrame)
{
	pDebugWireFrame_->DrawWireFrame(drawWireFrame);
}

//球同士の当たり判定
bool Collider::IsHitSphereToSphere(SphereCollider * pSphere1, SphereCollider * pSphere2)
{
	//今回採用する衝突判定の方法
	/*
		・ベクトル型を使用した衝突判定の計算

		自身のベクトルをA、引数のベクトルをBとする。
		�@２つのベクトルの差分をとって、AからB、あるいはBからAに延びるベクトルを求める。
		�Aベクトルの長さを求める（ベクトルで）

		�B求めたベクトルの、長さと、A・Bの半径の合計より
			・以下の時
			　→衝突しているので、trueを返す
			・大きいとき
			　→衝突していないので、falseを返す
	*/

	//�@
	//anotherCollPos = 自身と衝突判定を行うコライダーの位置
	//GetWorldPos() = 自身のコライダーのワールド位置を取得（外部に渡すための関数だったが、使えるなら使う）
	XMVECTOR length = pSphere1->GetWorldPos() - pSphere2->GetWorldPos();

	//�A
	length = XMVector3Length(length);

	/*球　サイズ********************************************************************************/
	//サイズの可変
		//詳しくは、BoxToSphere関数にて
	float radiusA = GetVariableSize(pSphere1);
	float radiusB = GetVariableSize(pSphere2);
	//２つのコライダーの半径の合計を出す
		//（２つの半径より、２コライダー間の距離が長いなら）　＝　衝突していない
		//（２つの半径より、２コライダー間の距離が短いなら）　＝　衝突している
	float sumRadius = radiusA + radiusB;


	//�B
		//×長さは、ベクトルで出てくるのでX、Y、Zで判断を行い、
		/*
			if (length.vecX <= sumRadius || length.vecY <= sumRadius ||
				length.vecZ <= sumRadius)
		*/
		//〇Lengthの時点で、→ｘｙｚすべてに共通の値が入る、xyzどの値を使っても問題なし
		/*
			if (length.vecX <= sumRadius)

		*/
		//半径の合計より小さければ、衝突していることになる
	if (length.vecX <= sumRadius)
	{
		//１つでも小さいものがあるなら、
		//衝突している
		JudgeTargetCollider((Collider*)pSphere1, (Collider*)pSphere2);
		return true;

	}
	/*
		・半径 1の球コライダー
		・半径 5の球コライダー

		上記の２つが衝突されているかを判定しろ

		・上記２つの直線距離は４である

		２つの半径の合計は６
		２つの直線の距離は４

		〇２つが衝突しているときの最高の距離は６（２つの半径の合計が６なので）
		〇２つの直線距離は４
		〇２つの半径合計より距離が小さいので

		★上記の２つの球は衝突している



	*/

	SetTargetCollider(nullptr);
	//衝突しなかった
	return false;


}

//球と六面体との当たり判定
bool Collider::IsHitBoxToSphere(BoxCollider * pBox1, SphereCollider * pSphere1)
{
	
	//球体のワールド座標
	XMVECTOR spherePosA = pSphere1->GetWorldPos();
	//箱の　ワールド座標
	XMVECTOR boxPosB = pBox1->GetWorldPos();


	/*球　サイズ********************************************************************************/
	//球の半径を求める
	//球の半径を、コライダーを所有しているオブジェクトのサイズで可変させる
	//↓上記の処理を関数で行う
	float radius = GetVariableSize(pSphere1);
	//球体のサイズは、
	//半径の長さを　それぞれ、ｘｙｚとしておく＝　半径が共通の球　のみ表現できるとする
	XMVECTOR sphereSizeA = XMVectorSet(radius, radius, radius, 0.f);

	/*箱　サイズ********************************************************************************/
	//球と同様
		//箱の場合は、ｘｙｚそれぞれを、それぞれのScale値で可変させる
	XMVECTOR boxSizeB = GetVariableSize(pBox1);



	//衝突判定
		//内外判定を行い、
		//ｘｙｚの座標がそれぞれ、　内外判定を行い、座標が内側にあると判断されたら、衝突しているとフラグを返す
	if (spherePosA.vecX > boxPosB.vecX - boxSizeB.vecX  / 2 - sphereSizeA.vecX &&
		spherePosA.vecX < boxPosB.vecX + boxSizeB.vecX / 2 + sphereSizeA.vecX &&
		spherePosA.vecY > boxPosB.vecY - boxSizeB.vecY / 2 - sphereSizeA.vecX &&
		spherePosA.vecY < boxPosB.vecY + boxSizeB.vecY / 2 + sphereSizeA.vecX &&
		spherePosA.vecZ > boxPosB.vecZ - boxSizeB.vecZ / 2 - sphereSizeA.vecX &&
		spherePosA.vecZ < boxPosB.vecZ + boxSizeB.vecZ / 2 + sphereSizeA.vecX)
	{
		JudgeTargetCollider((Collider*)pBox1, (Collider*)pSphere1);
		return true;
	}
	SetTargetCollider(nullptr);
	return false;
}

//六面体と六面体との当たり判定
bool Collider::IsHitBoxToBox(BoxCollider * pBox1, BoxCollider * pBox2)
{
	//ワールド座標を取得
		//コライダーをつけているオブジェクトのワールド＋　離れ具合
	XMVECTOR boxPosA = pBox1->GetWorldPos();
	XMVECTOR boxPosB = pBox2->GetWorldPos();

	
	/*箱　サイズ********************************************************************************/
	//サイズ
		//ワールド
		//ｘ方向のサイズ（ｍ）(横幅)
		//ｙ方向のサイズ（ｍ）（高さ）
		//ｚ方向のサイズ（ｍ）（奥行）
	//サイズの可変
	XMVECTOR boxSizeA = GetVariableSize(pBox1);
	XMVECTOR boxSizeB = GetVariableSize(pBox2);


	//衝突判定
	if ((boxPosA.vecX + boxSizeA.vecX / 2) > (boxPosB.vecX - boxSizeB.vecX / 2) &&
		(boxPosA.vecX - boxSizeA.vecX / 2) < (boxPosB.vecX + boxSizeB.vecX / 2) &&
		(boxPosA.vecY + boxSizeA.vecY / 2) > (boxPosB.vecY - boxSizeB.vecY / 2) &&
		(boxPosA.vecY - boxSizeA.vecY / 2) < (boxPosB.vecY + boxSizeB.vecY / 2) &&
		(boxPosA.vecZ + boxSizeA.vecZ / 2) > (boxPosB.vecZ - boxSizeB.vecZ / 2) &&
		(boxPosA.vecZ - boxSizeA.vecZ / 2) < (boxPosB.vecZ + boxSizeB.vecZ / 2))
	{
		JudgeTargetCollider((Collider*)pBox1, (Collider*)pBox2);
		return true;
	}

	//ターゲットがいなかったため、
	//nullptrを代入させる
	SetTargetCollider(nullptr);
	return false;


}

void Collider::AlongTheWall()
{
	//壁ずりの処理を行えるかの確認を行う
	if (CanColliderAlongTheWall())
	{
		/*
		http://marupeke296.com/COL_Basic_No5_WallVector.html
		
		�@自身のコライダーと
		　相手のコライダーで差ベクトルを取る
				→自身から相手へのベクトルを求める(進行ベクトル)
		�Aその方向ベクトルと、相手のコライダーの面（三角ポリゴン）とで、接触する面を求める
		�Bその接触した面の法線ベクトルを外積で求める
		�Cその法線ベクトルと、　�@のベクトルとで、壁ずりベクトルを求める
		�D壁ずりベクトル方向へコライダーを所有している　オブジェクトを移動させる
		
		*/

		//�@
		XMVECTOR progressVec = pTarget_->GetWorldPos() - this->GetWorldPos();

		//正規化
		XMVECTOR dir = XMVector3Normalize(progressVec);

		//スタート位置は、
		//自身のコライダー位置を、相手のコライダー方向へー10倍
		XMVECTOR start =  this->GetWorldPos() + (dir * -10.f);

		//�A，�B
			//ワイヤーフレームの当たり判定を呼び込み、
				//ここで判定を行うのは、
				//相手のコライダーでなければいけない。
			//その相手のコライダーと当たる、法線ベクトルもらう
		XMVECTOR normalVec = pTarget_->GetDebugWireFrame()->NormalVectorOfCollidingFace(start, dir);


		//�C
		XMVECTOR alongWallVec = CalcWallScratchVector(progressVec, normalVec);
		

		//�D
		//壁ずりベクトル方向へ一定量移動
			//壁ずりの方向を反転させる
			//コライダーの原点と、相手のコライダーとの原点で差ベクトルをとって、それを、進行方向としてしまっているので、
			//そのままだと、右に押し出されて、左に押し出されてと常に同じ位置にとどまり続けてしまう。
		pGameObject_->transform_.position_ -= alongWallVec * 0.1f;


	}
}

XMVECTOR Collider::CalcWallScratchVector(XMVECTOR & progressVec, XMVECTOR & normalVec)
{
	//法線ベクトルの正規化
	XMVECTOR normal = XMVector3Normalize(normalVec);

	//壁ずりベクトルを求める
	XMVECTOR alongWallVec = XMVector3Normalize(
		progressVec - 
		XMVector3Dot(progressVec, normal) *
		normal);

	return alongWallVec;

}

bool Collider::CanColliderAlongTheWall()
{
	//相手がnullptrでない
	//自身の機能タイプが　ALONG_THE_WALL_FUNCTIONである
	if (pTarget_ != nullptr && myFunctionType_ == ALONG_THE_WALL_FUNCTION)
	{
		return true;
	}
	return false;
}




XMVECTOR Collider::GetWorldPos()
{
	//オブジェクト位置＋コライダー位置
	//このコライダーを、
		//自身を導入しているワールドTransformで変形して、
		//座標を計算させる

	Transform trans = pGameObject_->transform_;
	trans.Calclation();
	//オブジェクトの離れ具合と、
	//コライダーを所有しているオブジェクトのワールド行列（つまり、所有オブジェクトの移動、回転、拡大を考慮したときの行列、これをかけることで、親の位置を原点とした座標を取得可能）
	XMVECTOR worldPos = XMVector3TransformCoord(myPos_ , trans.GetWorldMatrix()) ;

	return worldPos;
}

COLLIDER_TYPE Collider::GetMyType()
{
	//自身のタイプを返す
	return myColliderType_;
}

void Collider::SetObjectPos(XMVECTOR objectPosition)
{
	//オブジェクト位置の更新
	objectPos_ = objectPosition;



	//ワイヤーフレームのモデルに座標を送る
		//ここで考慮するのは、
		//コライダークラスが扱っている各情報が、どの座標なのか

		//描画において、
		//親を、仮に、ゲームオブジェクトのTransformとするならば、
			//その親を原点としての、座標となってしまう。
	//離れ具合と、オブジェクトの位置を考慮した座標を送る
		//SetObjectPosの座標に入ってくるのは、
		//親までのTransform,自身のTransformを考慮した位置となるので、
			//親に追尾する形で変わっている座標も考慮されたPositionとなる。
	//XMVECTOR worldPos = GetWorldPos();
	pDebugWireFrame_->SetPosition(myPos_);
}

void Collider::SetColliderFunctionType(FUNCTION_TYPE type)
{
	myFunctionType_ = type;
}


void Collider::SetMyType(COLLIDER_TYPE colliderType)
{
	myColliderType_ = colliderType;
}

void Collider::SetWireFrameSize()
{
	//箱
	//→　Vectorで、自身のサイズを送る

	//球
	//→　floatで、　自身の半径を送る

}

void Collider::SetWireFrameSize(float radius)
{
	pDebugWireFrame_->SetScale(radius);
}

void Collider::SetWireFrameSize(XMVECTOR & size)
{
	pDebugWireFrame_->SetScale(size);
}


void Collider::SetTargetCollider(Collider * pTarget)
{
	pTarget_ = pTarget;
}

void Collider::JudgeTargetCollider(Collider * pColl1, Collider * pColl2)
{
	if (pColl1 == this && pColl2 != this)
	{
		SetTargetCollider(pColl2);
	}
	else if(pColl1 != this && pColl2 == this)
	{
		SetTargetCollider(pColl1);
	}
}

float Collider::GetGameObjectScaleForSphere()
{
	return pGameObject_->transform_.scale_.vecX;
}

XMVECTOR Collider::GetGameObjectScaleForBox()
{
	return pGameObject_->transform_.scale_;
}

float Collider::GetVariableSize(SphereCollider * pSpColl)
{
	//サイズ
		//ワールド
		//ｘ方向のサイズ（ｍ）(横幅)
		//ｙ方向のサイズ（ｍ）（高さ）
		//ｚ方向のサイズ（ｍ）（奥行）
	float radius = pSpColl->GetRadiusSize();
	//ここにおけるサイズは、あくまでも、登録時でのサイズなので、
		//親のモデルが、仮に、サイズの可変をされていたら、それに合わせて拡大、縮小する必要がある。
		//コライダーを、コライダーを持っているモデルのサイズに合わせるやり方をすれば、
		//・デバック用のコライダーモデルの出力にサイズが合うし、
		//・のちに、持っているモデルのサイズを変えたときに、それに合わせて、サイズを可変するという手間を省ける。
	radius *= pSpColl->GetGameObjectScaleForSphere();


	return radius;
}

XMVECTOR Collider::GetVariableSize(BoxCollider * pBoxColl)
{
	XMVECTOR boxSize = pBoxColl->GetSize();
	//箱のサイズも同様に
		//コライダーを持っているモデルのサイズ分可変する
	XMVECTOR scale = pBoxColl->GetGameObjectScaleForBox();
	boxSize = XMVectorSet(
		boxSize.vecX * scale.vecX,
		boxSize.vecY * scale.vecY,
		boxSize.vecZ * scale.vecZ,
		0.f
	);

	return boxSize;


}

