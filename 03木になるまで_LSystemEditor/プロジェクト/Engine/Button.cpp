#include "Button.h"
#include "Image.h"
#include "Input.h"
#include "Transform.h"
#include "Global.h"

Button::Button(GameObject * parent):
	currentStatus_(OFF_BUTTON)	//ボタンOFFがデフォルト
{
	for (int i = 0; i < MAX_BUTTON_STATUS; i++)
	{
		hImage_[i] = -1;
		trans[i] = nullptr;
	}
}

Button::~Button()
{
}


/*
void Button::Initialize()
{
	//ボタンのロード
	Load("" , ON_BUTTON);
	Load("", OFF_BUTTON);
}
void Button::Update()
{
	//ボタンの接触判定
	UpdateButtonForOnContact();
}
void Button::Draw()
{
	//ボタン描画
	DrawButton();
}
void Button::Release()
{
	//ボタンクラスの動的確保した要素を解放
	ReleaseButton();
}
*/

void Button::OnContactButton()
{
	//ボタンの押下処理
		//マウスの左クリックを、押し続けている間　ON
		//マウスの左クリックを、離し続けている間　OFF
	ButtonPress();


}









void Button::Load(const std::string & fileName, BUTTON_STATUS status)
{
	//すでにロード済みなら戻す
	if (hImage_[status] != -1) { return; }

	//画像をロード
	hImage_[status] = Image::Load(fileName);
	assert(hImage_[status] != -1);
	//トランスフォームの作成
	trans[status] = new Transform;
	Image::SetTransform(hImage_[status], trans[status]);
}

void Button::UpdateButtonForOnContact()
{
	//接触判定
	XMVECTOR mouseCursorVec = Input::GetMouseCursorPosition();


	//画像の当たり判定
	//マウスカーソル位置と　画像の当たり判定のための情報を入れる構造体
	ImageCollision imageCollision;
	//マウスの位置を代入
	imageCollision.scrPointPos = mouseCursorVec;

	
	//ここで一度、現在のボタンがすでにロード済みであるかの確認をとる
	CheckIfLoaded(currentStatus_);
	//画像とマウスカーソル位置の接触判定
	Image::HitCollision(hImage_[currentStatus_], &imageCollision);

	//接触していた
	if (imageCollision.hit)
	{
		//接触時の処理を呼び込む
		OnContactButton();
	}
	
}



void Button::DrawButton()
{
	//今から描画をしようとする画像がロードされているかの確認
	CheckIfLoaded(currentStatus_);
	//描画のためのTransformセット
	//Image::SetTransform(hImage_[currentStatus_], trans[currentStatus_]);
		//Transformは、Image.cppにセットして登録しているので、
		//ここではセットしなおさない。
		//→ピクセル管理で動かしたりするときに、
		//→移動後のTransformなどを受け取りなおすのは酷なので、
			//→ここではセットせずに、Transformは、画像生成時にセットした、Transformを移動して扱うようにする。

	//描画実行
	Image::Draw(hImage_[currentStatus_]);
}



void Button::ReleaseButton()
{
	for (int i = 0; i < MAX_BUTTON_STATUS; i++)
	{
		SAFE_DELETE(trans[i]);
	}
	//SAFE_DELETE_ARRAY(trans);	//ポインタを配列として持っただけなので、
									//配列自体は実体。だが、中身の要素は動的確保したもの
								//要するに、ポインタを配列として持っただけ→　ポインタが示すのは、配列の先頭アドレス（実体を持つ配列と同様である。）（この辺りを混乱しないようにする）

}


//void Button::ChangeButtonTexture(BUTTON_STATUS changeStatus)
//{
//}

void Button::ChangeButtonStatus(BUTTON_STATUS changeStatus)
{
	
	//現在のステータスを切り替える必要がある

	//そうすると、描画の画像が変化する
	//伴って、描画をするための画像が用意されているかの確認
		//すでにロード済みならば、		：そのまま、その画像を描画のために用いる
		//まだロードされていないならば、：現在あるモデルを使用する。（hImage_にモデル番号をコピー）
	CheckIfLoaded(currentStatus_);


	//描画先の画像が確保できた（画像のハンドルを持つことができた）ら初めて
	//ステータスを切り替える
	currentStatus_ = changeStatus;

}

bool Button::Load(BUTTON_STATUS status)
{
	
	//ロードされているかの確認
	//ロードされているなら
	if (hImage_[status] != -1)
	{
		//関数終了
		return true;
	}
	else
	{
		//すでにロードされているものを探して、
		//その画像のモデルハンドルも自身で共有
		for (int i = 0; i < MAX_BUTTON_STATUS; i++)
		{
			if(hImage_[i] != -1 && i != (int)status)
			{
				//情報をコピーする
				CopyLoadedInformation(status, i);
				return true;
			}
		}
	}


	//すでにロードされている画像が、
	//一つもなかった
	return false;
}

void Button::CheckIfLoaded(BUTTON_STATUS status)
{
	if (!Load(status))
	{
		MessageBox(nullptr, "ボタンの画像を最低１つロードしてください", "エラー", MB_OK);
		assert(0);
	};

	
}


void Button::CopyLoadedInformation(int copyTo, int original)
{
	//画像ハンドルのコピー
	hImage_[copyTo] = hImage_[original];
	//Transformのコピー
	trans[copyTo] = new Transform;
	//中身をコピー
	(*trans[copyTo]) = (*trans[original]);

	//セット
	Image::SetTransform(hImage_[copyTo], trans[copyTo]);

}

void Button::ButtonON()
{
	ChangeButtonStatus(ON_BUTTON);
}

void Button::ButtonOFF()
{
	ChangeButtonStatus(OFF_BUTTON);
}

void Button::ButtonPress()
{
	//列挙型：BUTTON_STATUS
	//これが　０：OFF_BUTTON　、　１：ON_BUTTON　であるとき限定
		//接触時：（OFFである、かつ、ボタンの範囲内にいる）画像を変えるのであれば、更に、条件が必要

	//マウスの左クリック入力を受け取り、それが入力されているときはTrue＝ONBUTTON、False＝OFFBUTTON　になる
	ChangeButtonStatus((BUTTON_STATUS)Input::IsMouseButton(0));
}

void Button::ButtonPressOneDown()
{
	//入力1回されたときに、
	//ON,OFFを切り替える処理を行う
	//if (Input::IsMouseButtonDown(0))
	{
		//現在の状態を反転させて、切り替える
		//if文を使わないために
		//論理演算にて識別させる

		//　currentStatus_は何でもいいが、　IsMouseButtonDownは、１の時は、→ToggleButtonSwitchにて、currentStatusの反転したものを帰す。
																//0の時は、→同様にて、currentStatusそのままの値を返す。
		//つまり、1の時は、currentStatusをそのままの値を返して、
		//　　　　0の時は、currentStatusの反転の値を返して
		/*
		
		current , isMouse = 結果
		0		,		0 =  1
		1		,		0 =  0
		0		,		1 =  0
		1		,		1 =  1
		
		上記の論理演算結果を見ると
		・同じときは1
		・違うときは0

		→排他的論理和のNOT（！）

		*/
		/*ChangeButtonStatus(ToggleButtonSwitch(
			(BUTTON_STATUS)
			(!(currentStatus_ ^ Input::IsMouseButtonDown(0)))
		));*/
		//排他的論理和をつかえば、そもそもToggleButtonSwtichの関数いらない

		/*
		
		排他的論理和にして

		同じ値の時に　０、違うときに１にすれば、
		今回の処理は通る
		
		*/
		ChangeButtonStatus(
			(BUTTON_STATUS)
			((int)currentStatus_ ^ (int)Input::IsMouseButtonDown(0))
		);
	}
}

void Button::ButtonPressOneUp()
{
	//入力1回離されたとき、
	//ON,OFFを切り替える処理を行う
	//if (Input::IsMouseButtonUp(0))
	{
		//現在の状態を反転させて、切り替える
		//Down時と同様に、排他的論理和のNOT
		/*ChangeButtonStatus(ToggleButtonSwitch(
			(BUTTON_STATUS)
			(!(currentStatus_ ^ Input::IsMouseButtonUp(0)))
		));*/
		ChangeButtonStatus(
			(BUTTON_STATUS)
			((int)currentStatus_ ^ (int)Input::IsMouseButtonUp(0))
		);
	}
}

BUTTON_STATUS Button::ToggleButtonSwitch(BUTTON_STATUS status)
{
	//現在がONなら、 OFFへ
	//現在がOFFなら、ONへ　切り替える

	//単純に列挙型の値を整数として扱って、
	//OFF ＝　０
	//ON　＝　１

	//上記として扱い、
	//0を1へ、1を0へ切り替える処理を行う

	/*
	・引数を整数にする
	・引数の値を‐1する
	・その値を絶対値にする(絶対値→符号を＋にした整数値（-1を１に、　ー１００を１００に）)

	引数1の場合
		1−1＝０　→絶対値＝０

	引数0の場合
		0−1＝‐1　→絶対値＝１


	*/

	int switchValue = (int)status;
	switchValue -= 1;
	switchValue = abs(switchValue);


	return (BUTTON_STATUS)switchValue;

}


void Button::SetTransform(Transform & transform)
{
	for (int i = 0; i < MAX_BUTTON_STATUS; i++)
	{
		if (hImage_[i] != -1)
		{
			//自身が所有している画像をすべて同様に動かす
			Image::SetTransform(hImage_[i], transform);
		}
	}
}
void Button::SetTransform(Transform * transform)
{
	for (int i = 0; i < MAX_BUTTON_STATUS; i++)
	{
		if (hImage_[i] != -1)
		{
			Image::SetTransform(hImage_[i], transform);
		}
	}
}
void Button::MovePixelPosition(int x, int y)
{
	//移動のため、
	//仮に、hImage_に登録してある各画像が、共通のものを使っていた場合、
	//２回以上呼ばれて、２倍以上移動してしまう。
	//それを避けるために、一度移動したものは避けるようにする

	//移動したものを保存しておく一時保存先
	std::vector<int> moved;
	moved.clear();

	for (int i = 0; i < MAX_BUTTON_STATUS; i++)
	{
		if (hImage_[i] != -1 && IsExits(moved, hImage_[i]))
		{
			//移動
			Image::MovePixelPosition(hImage_[i], x, y);
			//移動済みに追加
			moved.push_back(hImage_[i]);

		}
	}
}

bool Button::IsExits(std::vector<int>& moved, int checkValue)
{
	for (int i = 0; i < moved.size(); i++)
	{
		if (checkValue == moved[i])
		{
			return true;
		}
	}

	return false;
}

void Button::SetPixelPosition(int x, int y)
{
	for (int i = 0; i < MAX_BUTTON_STATUS; i++)
	{
		if (hImage_[i] != -1)
		{
			Image::SetPixelPosition(hImage_[i], x, y);
		}
	}
	
}
void Button::ChangeShader(SHADER_TYPE shaderType)
{
	for (int i = 0; i < MAX_BUTTON_STATUS; i++)
	{
		if (hImage_[i] != -1)
		{
			Image::ChangeShader(hImage_[i], shaderType);
		}
	}
}

void Button::SetPixelScale(int x, int y)
{
	for (int i = 0; i < MAX_BUTTON_STATUS; i++)
	{
		if (hImage_[i] != -1)
		{
			Image::SetPixelScale(hImage_[i], x, y);
		}
	}
}

bool Button::IsButtonOFF()
{
	return !currentStatus_;
}

bool Button::IsButtonON()
{
	return currentStatus_;
}
