#pragma once
#include "PolygonGroup.h"


//頂点情報を持つクラスを結合して、
//一つの３Dポリゴンクラスとする
	//1つのポリゴンクラスの情報をもらって、
	//その情報を、自身の所有しているポリゴンの情報と結合

/*

結合情報
・頂点情報（ローカル座標、UV、法線など）
・インデックス情報（どの頂点番号で▲ポリゴンをつくるか）

結合によって変化する情報
・頂点バッファの更新
・インデックスバッファの更新



*/


//結合する３Dポリゴンクラス
class Branch;
class Plane;



class BranchesMakeTree : public PolygonGroup
{
protected:
	//コンスタントバッファ
	//シェーダーに渡す情報（シェーダー内のコンスタントバッファに変数として持っている情報に渡す情報を、構造体として取得しておく（渡す情報をまとめておく））
	//シェーダーのコンスタントバッファと同じ順番になるようにする。
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;	//ワールド行列＊ビュー行列＊プロジェクション行列
		XMMATRIX	matNormal;	//回転のためのワールド行列（移動行列なし）
		XMMATRIX	matWorld;	//移動＊回転＊拡大　行列

		XMFLOAT4 camPos;	//カメラポジション
							//シェーダーの反射のため

		BOOL isTexture;	//テクスチャが張られているか
	};

	//頂点情報
		//現段階ではポジションだけの頂点情報（のちに法線）
	struct VERTEX
	{
		XMVECTOR position;	//位置情報（頂点の位置）
		XMVECTOR uv;		//UV情報	（頂点のテクスチャの座標）
		XMVECTOR normal;		//法線情報	（頂点の陰）

		//コンストラクタ
		VERTEX();
	};



	//各頂点ごとの頂点情報を入れる配列
	VERTEX* pVertices_;

	//インデックス情報を登録しておく
	//動的確保した配列
	int* pIndex_;


	//頂点情報を受け渡し
	ID3D11Buffer *pVertexBuffer_;
	//頂点の順番→インデックス情報を持っておく情報
	ID3D11Buffer *pIndexBuffer_;
	//コンスタントバッファ→渡すため
	ID3D11Buffer *pConstantBuffer_;
	//上記の情報があれば、→FBX,頂点情報、インデックス情報を受け取れば、
		//→Quadクラスト同じように大量に三角形を作れば表示は可能なはず

	//FBXを読み込むので、あらかじめわからないので、
	//メンバとして宣言しておく
	int vertexCount_;	//頂点数（FBXファイルより取得）
	int polygonCount_;	//ポリゴン数（FBXファイルより取得）
	int materialCount_;	//マテリアルの個数（マテリアルのLambert１、２などを入れるための数）（ノードに存在するマテリアルの個数）

	//インデックス数
	int indexCountEachMaterial_;



protected:

	//頂点情報（Y座標を除く）作成
	//戻値：エラーの有無
	virtual HRESULT InitVertex();
	//頂点情報から頂点バッファーを作成
	virtual HRESULT CreateVertexBuffer();

	//インデックス情報の作成
	virtual HRESULT InitIndex();
	//インデックス情報からインデックスバッファーを作成
	virtual HRESULT CreateIndexBuffer();
	//コンスタントバッファの作成
	virtual HRESULT CreateConstantBuffer();

	//テクスチャのロード
	virtual HRESULT LoadTexture(std::string textureFile = "");
	Texture* pTexture_;

public:

	//コンストラクタ
	//引数：なし
	//戻値：なし
	BranchesMakeTree();

	//デストラクタ
	virtual ~BranchesMakeTree();


	//ロード
	//引数：fileName ファイル名(ハイトマップの画像ファイル)
	//戻値：なし
	virtual HRESULT Load(std::string fileName, SHADER_TYPE thisShader);

	//初期化
	//必要初期化
	//引数:テクスチャファイル名(素材モデルに共通して使うテクスチャ)
	//引数：シェーダー名
	HRESULT Initialize(std::string textureFileName, SHADER_TYPE thisShader);




	//モデルの描画を行う
	//引数：&transform	描画時の変形情報（移動、拡大、回転）
	//戻値：なし
	virtual HRESULT    Draw(Transform& transform);
	//ポインタの解放処理
	//引数：なし
	//戻値：なし
	virtual void    Release();


	//３Dポリゴンクラスを
		//自身のモデルに結合して、
		//1つのポリゴンクラスにする

		//引数にて渡された、３Dポリゴンクラスから、
		//頂点情報、インデックス情報を取得し、自身の情報に結合する
	//引数：３Dポリゴンクラス（Branch（枝））
	//引数：出現位置のTransform（ローカル座標 各頂点を引数Transformのワールド行列で変形させる）
	HRESULT Join3DPolygon(Branch* pBranch , Transform& trans);


	//３Dポリゴンクラスを
	//自身のモデルに結合して、
	//1つのポリゴンクラスにする

	//引数にて渡された、３Dポリゴンクラスから、
	//頂点情報、インデックス情報を取得し、自身の情報に結合する
//引数：３Dポリゴンクラス（Plane（平面））
//引数：出現位置のTransform（ローカル座標 各頂点を引数Transformのワールド行列で変形させる）
	HRESULT Join3DPolygon(Plane* pPlane, Transform& trans);





};



