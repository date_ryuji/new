#pragma once

//Direct2D
//DirectWriteを使用した、　テキストの描画などに用いる


#include <d2d1.h>	//Direct２D
#include <d2d1_1.h>	//Direct２D Ver1
#include <dwrite.h>	//DirectWrite
#include <wchar.h>	//wchar , swprintf()

//コンパイラディレクティブ
	//オブジェクトファイルを処理するときに、リンカーによって読み取られる
	//リンカ：機械語のプログラムの断片を結合し実行可能なプログラムを作成するプログラムのこと
		//上記によって、ソースファイルをコンパイルするとプログラムのオブジェクトファイルが出来る
#pragma comment( lib, "d2d1.lib" )
#pragma comment( lib, "dwrite.lib" )



namespace Direct2D
{
	//スクリーンサイズ
	extern int scrWidth;
	extern int scrHeight;


	//初期化
	//戻値：処理の成功、失敗
	HRESULT Initialize();

	//描画開始
	void BeginDrawDirect2D();

	//描画終了
	void EndDrawDirect2D();

	//解放
	void Release();

	//ブラシの取得
		//描画をするために必要
	//戻値：描画ブラシのポインタ
	ID2D1SolidColorBrush* GetBrush();

	////描画の橋渡し（ビュー）の取得
	//ID2D1RenderTarget* GetRenderTargetView();

	ID2D1DeviceContext* GetDeviceContext();


	//テキストフォーマット（標準）の取得
	IDWriteTextFormat* GetTextFormat();


	//Deviceの作成
	void CreateDevice();


	//Factoryの作成
	void CreateFactory();

	//DeviceContextの作成
	void CreateDeviceContext();





}
