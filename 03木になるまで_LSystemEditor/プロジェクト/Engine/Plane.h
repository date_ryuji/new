#pragma once
#include "PolygonGroup.h"



/*
//３D空間上に設置する
	//平面ポリゴン
	//あくまでポリゴンとしては、▲ポリゴン2つで作られる四角形のポリゴンである。
	//両面表示、裏面表示をするかは、シェーダーによって管理させる


//平面として描画できるようにするエンジンでもあるが、（初期化にて、平面のポリゴンを確保する）
//同時に、Engine/Branch.hのように、自身の頂点情報を外部に渡すことが可能なため、
//        Engine/BranchesMakeTreeの様に、他ポリゴンクラスと結合して、ポリゴンを作るクラスへ結合ができる


*/


class Plane : public PolygonGroup
{
public:
	//頂点情報
	//現段階ではポジションだけの頂点情報（のちに法線）
	struct VERTEX
	{
		XMVECTOR position;	//位置情報（頂点の位置）
		XMVECTOR uv;		//UV情報	（頂点のテクスチャの座標）

		//コンストラクタ
		VERTEX();

		VERTEX(XMVECTOR setPos , XMVECTOR setUv);

	};




protected:

	//コンスタントバッファ
	//シェーダーに渡す情報（シェーダー内のコンスタントバッファに変数として持っている情報に渡す情報を、構造体として取得しておく（渡す情報をまとめておく））
	//シェーダーのコンスタントバッファと同じ順番になるようにする。
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;	//ワールド行列＊ビュー行列＊プロジェクション行列
		//XMMATRIX	matNormal;	//回転のためのワールド行列（移動行列なし）
		//XMMATRIX	matWorld;	//移動＊回転＊拡大　行列

		//XMFLOAT4 camPos;	//カメラポジション
		//					//シェーダーの反射のため

		BOOL isTexture;	//テクスチャが張られているか
	};


	//各頂点ごとの頂点情報を入れる配列
	VERTEX* pVertices_;

	//インデックス情報を登録しておく
	//動的確保した配列
	int* pIndex_;


	//頂点バッファー
	//頂点情報を受け渡し
	ID3D11Buffer *pVertexBuffer_;
	//インデックスバッファー
	//頂点の順番→インデックス情報を持っておく情報
	ID3D11Buffer *pIndexBuffer_;
	//コンスタントバッファ→シェーダーへの情報
	ID3D11Buffer *pConstantBuffer_;	

	//FBXを読み込むので、あらかじめわからないので、
	//メンバとして宣言しておく
	int vertexCount_;	//頂点数（FBXファイルより取得）
	int polygonCount_;	//ポリゴン数（FBXファイルより取得）

	//インデックス数
	int indexCountEachMaterial_;

	//テクスチャ
	Texture*	pTexture_;

protected:


	//頂点バッファ作成
	//引数：FBXモデルのノードのメッシュ
	//戻値：エラーの有無
	HRESULT InitVertex();
	//インデックスバッファ作成
	//引数：FBXモデルのノードのメッシュ
	//戻値：エラーの有無
	HRESULT InitIndex();


	//頂点情報から頂点バッファーを作成
	virtual HRESULT CreateVertexBuffer();
	//インデックス情報からインデックスバッファーを作成
	virtual HRESULT CreateIndexBuffer();
	//コンスタントバッファの作成
	virtual HRESULT CreateConstantBuffer();

	//テクスチャのロード
	HRESULT LoadTexture(std::string fileName);


public:


	//コンストラクタ
	//引数：なし
	//戻値：なし
	Plane();

	//デストラクタ
	~Plane() override;

	//ロード
	//ハイトマップ画像からモデルの作成を行う（引数のファイル名より）
	//引数：textureFileName ファイル名(Planeに貼り付ける画像ファイル)
	//引数：シェーダータイプ（SHADER_PLANEが理想）
	//戻値：なし
	HRESULT Load(std::string textureFileName, SHADER_TYPE thisShader) override;

	



	//モデルの描画を行う
	//引数：&transform	描画時の変形情報（移動、拡大、回転）
	//戻値：なし
	HRESULT    Draw(Transform& transform) override;


	//ポインタの解放処理
	//引数：なし
	//戻値：なし
	void    Release() override;


	//頂点数を取得
	int GetVertexCount();
	//ポリゴン数を取得
	int GetPolygonCount();
	//インデックス数を取得
	int GetIndexCount();

	//頂点情報群を取得
	Plane::VERTEX* GetVertexes();
	//インデックス情報群を取得
	int* GetIndexes();


};


