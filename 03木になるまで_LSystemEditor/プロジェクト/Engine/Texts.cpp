#include "Texts.h"
#include <typeinfo>


void Texts::Initialize()
{
	//描画のための必要ポインタの取得
	pSolidBrush_ = Direct2D::GetBrush();
	pDeviceContext_ = Direct2D::GetDeviceContext();
	//pDeviceContext = Direct2D::GetRenderTargetView();
	pTextFormat_ = Direct2D::GetTextFormat();

}

////ヘッダとソースとでテンプレート関数を分けると
//	//エラー
//	//そのために、以下のサイトにあるように
//		//template int func<int>(int); //追加することで、　明示的に、使用可能な、リンク可能な関数とする
////https://qiita.com/i153/items/38f9688a9c80b2cb7da7
//template<class T>
//void Texts::DrawTex(T text)
//{
//}
//template void Texts::DrawTex<int>(int); //追加


template<class E>
DATA_TYPE Texts::GetDataType(E text)
{
	//type_info name;
	//name.name = typeid(int).name;

	const char i = typeid(int).name()[0];
	char f = typeid(float).name()[0];
	char d = typeid(double).name()[0];
	char c = typeid(char).name()[0];
	char s = typeid(std::string).name()[0];


	
	//引数の型を調べる
	char type = typeid(text).name()[0];


	DATA_TYPE dataType;


	if (i == type)
	{
		dataType = INT_TYPE;
	}
	else if(f == type
		|| d == type)
	{
		dataType = FLOAT_TYPE;
	}
	else if (c == type)
	{
		dataType = CHAR_TYPE;
	}
	else if (s == type)
	{
		dataType = STRING_TYPE;
	}
	else
	{
		dataType = MAX_DATA_TYPE;
	}


	/*if (typeid(int).name == typeid(text).name)
	{
		dataType = INT_TYPE; 
	}
	else if(typeid(float).name == typeid(text).name 
		|| typeid(double).name == typeid(text).name)
	{
		dataType = FLOAT_TYPE;
	}
	else if (typeid(char).name == typeid(text).name)
	{
		dataType = CHAR_TYPE; 
	}
	else if (typeid(std::string).name == typeid(text).name)
	{
		dataType = STRING_TYPE; 
	}
	else
	{
		dataType = MAX_DATA_TYPE;
	}*/


	//switch のcase文に、　変数を扱うことはできない
		//仮に、constとおいても、　constの値には、変数の値が入るっているようなものなので、　定数として確立しない
	/*switch (type)
	{
	case i : 
		dataType = INT_TYPE; break;
	case f:
	case d:
		dataType = FLOAT_TYPE; break;
	case c:
		dataType = CHAR_TYPE; break;
	case s:
		dataType = STRING_TYPE; break;



	default:
		dataType = MAX_DATA_TYPE; break;
	}*/








	return dataType;
}
//テンプレートの引数として渡されるであろう型を網羅させる
template DATA_TYPE Texts::GetDataType<int>(int); //追加
template DATA_TYPE Texts::GetDataType<double>(double);
template DATA_TYPE Texts::GetDataType<float>(float);
template DATA_TYPE Texts::GetDataType<char>(char);
template DATA_TYPE Texts::GetDataType<std::string>(std::string);


template<class T>
void Texts::DrawTex(const T text, D2D1::ColorF::Enum color, int posX, int posY)
{
	{
		//テストテキスト描画
		//--------------------------★変更↓--------------------------
		//画像とテキストの描画
		WCHAR wcText1[256] = { 0 };
		WCHAR wcText2[256] = { 0 };

		swprintf(wcText1, 256, L"%lf", 60.0);
		//wcharを　第４引数に入れることで、　フォーマットに合わせることが可能
			//https://www.k-cube.co.jp/wakaba/server/format.html
		swprintf(wcText2, 256, L"%ls", L"こんにちは");


		//関数DrawBitmap()
		//第1引数：レンダリングするビットマップ（pD2DBitmap）
		//第2引数：ビットマップの位置の座標（D2D1::RectF(左, 上, 右, 下)）
			//（テキスト原点ｘ、テキスト原点ｙ、　テキストの枠のサイズWidth , テキストの枠のサイズHeight）
		//第3引数：不透明度（0.0f〜1.0f）
		//第4引数：ビットマップが拡大縮小または回転される場合に使用する補間モード（D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR：ドット絵風[ギザギザ]
		//                                                                         D2D1_BITMAP_INTERPOLATION_MODE_LINEAR：写真風[なめらか]）
		//第5引数：トリミング（D2D1::RectF(左, 上, 右, 下), nullptr：イメージ全体の場合）
		//pRT->DrawBitmap(pD2DBitmap, D2D1::RectF(0, 0, 128, 128), 1.0f, D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR, nullptr);
		pSolidBrush_->SetColor(D2D1::ColorF(TEXT_COLOR_BLACK));
		pDeviceContext_->DrawTextA(wcText1, ARRAYSIZE(wcText1) - 1, pTextFormat_,
			D2D1::RectF(0, 0, Direct2D::scrWidth, Direct2D::scrHeight), pSolidBrush_, D2D1_DRAW_TEXT_OPTIONS_NONE);


		//ブラシの色を変える
		//pSolidBrush_->SetColor(D2D1::ColorF(D2D1::ColorF::Violet));
		pSolidBrush_->SetColor(D2D1::ColorF(TEXT_COLOR_VIOLET));
		//pSolidBrush_->Set
			

		pDeviceContext_->DrawTextA(wcText2, ARRAYSIZE(wcText2) - 1, pTextFormat_,
			D2D1::RectF(300, 300, Direct2D::scrWidth, Direct2D::scrHeight), pSolidBrush_, D2D1_DRAW_TEXT_OPTIONS_NONE);


	}


	DATA_TYPE dataType = GetDataType<T>(text);

	//データを取得
	//文字長
	const int maxDistance = 256;

	//文字配列の宣言
	WCHAR dispText[maxDistance] = {0};	// = new WCHAR[maxDistance];

	///フォーマット指定子
	///std::string parameter = { '%' , 'l' , ' ' , '\n' };
	////型文字
	//char type = ' ';
	////データタイプから
	////出力文字をWCHAR型に変更
	//switch (dataType)
	//{
	//case INT_TYPE : 
	//	type = 'd';

	//	//char i = 'd';	//int型のフォーマット
	//	//int型のフォーマットに指定する
	//		//https://bituse.info/c_func/56
	//	//memcpy(parameter[2].c_str(), &i, 1);
	//	break;

	//case FLOAT_TYPE:
	//	type = 'f'; break;
	//case CHAR_TYPE :
	//	type = 'c'; break;
	//case STRING_TYPE:
	//	type = 's'; break;

	//default:
	//	delete[] dispText;	return; break;
	//}
	////フォーマット指定子
	//std::wstring parameter = { '%' , 'l' , type , '\n' };
	//swprintf(dispText, maxDistance, parameter, text);
	////本来であれば、テキストフォーマット部分のみかえて、　
	//	//フォーマットを変数として持ち、その変数から、　文字列を作成したかった。


	//データタイプから
	//出力文字をWCHAR型に変更
		//フォーマット識別子を変更し、
		//文字列に変更
	switch (dataType)
	{
	case INT_TYPE:
		//int型のフォーマットを指定して、Wchar配列の文字に変換
		swprintf(dispText, maxDistance, L"%ld", text);	
		break;

	case FLOAT_TYPE:
		//float(double)型のフォーマットを指定して、Wchar配列の文字に変換
		swprintf(dispText, maxDistance, L"%lf", text);
		break;
	case CHAR_TYPE:
		//char型のフォーマットを指定して、Wchar配列の文字に変換
		swprintf(dispText, maxDistance, L"%lc", text);
		break;
	case STRING_TYPE:
		//string型のフォーマットを指定して、Wchar配列の文字に変換
		swprintf(dispText, maxDistance, L"%ls", text);
		break;

	default:
		//delete[] dispText;
		return; break;
	}



	//ブラシ色を変更
	pSolidBrush_->SetColor(D2D1::ColorF(color));
	//描画
	pDeviceContext_->DrawTextA(dispText, ARRAYSIZE(dispText) - 1, pTextFormat_,
		
		D2D1::RectF(posX, posY, Direct2D::scrWidth, Direct2D::scrHeight),
		
		pSolidBrush_, D2D1_DRAW_TEXT_OPTIONS_NONE);



	//delete[] dispText;

}
template void Texts::DrawTex<int>(int, 
	D2D1::ColorF::Enum color , int posX, int posY); //追加
	//テンプレート関数を
	//ヘッダとソースで分けた時のコンパイル時エラーをなくすために、　明示的に使用例のようなものを宣言させる
template void Texts::DrawTex<double>(double, 
	D2D1::ColorF::Enum color, int posX, int posY);
												//そして、　引数として渡されるであろう、型をすべて、ここで網羅しなければ、エラーになる
												//そのため、型が何が来るのか、わからない場合は、　ヘッダに宣下してしまうのが一番
template void Texts::DrawTex<float>(float, 
	D2D1::ColorF::Enum color, int posX, int posY);
template void Texts::DrawTex<char>(char, 
	D2D1::ColorF::Enum color, int posX, int posY);
template void Texts::DrawTex<std::string>(std::string, 
	D2D1::ColorF::Enum color, int posX, int posY);