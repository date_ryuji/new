#pragma once
#include "GameObject.h"

/*
	ゲームプログラムの構造

	WinMain(メッセージループ)　
	→　RootJob(GameObject継承)　
		→ SceneManager(GameObject継承)　
		→　各Scene(GameObject継承)　
		→　各シーン内オブジェクト(GameObject継承)

*/

//ゲームに登場するシーン
enum SCENE_ID
{
	SCENE_ID_TEST = 0,
	SCENE_ID_SAMPLE,
	SCENE_ID_MAP_EDITOR,
	SCENE_ID_CREATE_MAP,
	SCENE_ID_LSYSTEM_EDITOR,

};
//各シーンに名前を付けておいて、enumの値で管理
//シーン切り替えにおいて、上記のシーンIDから切り替えるシーンを選択


class SceneManager :
	public GameObject
{
private : 
	SCENE_ID currentSceneID_;	//現在のシーンID
	SCENE_ID nextSceneID_;		//次のシーンID
	
	//現在のシーンのインスタンス
	GameObject* pCurrentInstance_;



public : 

	//GameObjectクラスに書いても、コンストラクタなので、、結局クラスごとに書かないといけなくなる
	//親を受け取るコンストラクタ（Instantiateの関数にて、引数ありのコンストラクタで動的確保されるので、）
	SceneManager(GameObject* parent);
	//デストラクタ
	~SceneManager() override;


	// GameObject を介して継承されました
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;

	//シーン切り替えを宣言する関数（次のフレームにてシーンは切り替えられるようにする）
	//引数：切り替え後のシーンID
	//戻値：なし
	void ChangeScene(SCENE_ID changeScene);

};

