#include "Branch.h"

Branch::Branch():
PolygonGroup::PolygonGroup(),

pVertices_(nullptr),
pVertexBuffer_(nullptr),
pIndex_(nullptr),
pIndexBuffer_(nullptr),
vertexCount_(0), indexCountEachMaterial_(0), polygonCount_(0)
{
}

Branch::~Branch()
{
}

HRESULT Branch::Load(std::string fileName, SHADER_TYPE thisShader)
{
	//ファイルにて示されたFbxファイルをロードする


	thisShader_ = thisShader;


	//FBXをロードするときはこの形式で書けと言われている
	//FBXのトップをCreateで呼ぶ
	//マネージャを生成
	FbxManager* pFbxManager = FbxManager::Create();

	//FBXをロードをする機能を持っているクラスの作成
	//ロードする人
	//インポーター（輸入、ロードして持ってくる）する人を生成
	FbxImporter *fbxImporter = FbxImporter::Create(pFbxManager, "imp");

	//ファイル名でインポーターがインポート（輸入、ロード）している
	fbxImporter->Initialize(fileName.c_str(), -1, pFbxManager->GetIOSettings());

	//ゲーム内のシーンに読み込む
	//１つのシーンに入れるそれぞれのシーンに、取り込む

	//シーンオブジェクトにFBXファイルの情報を流し込む
	FbxScene*	pFbxScene = FbxScene::Create(pFbxManager, "fbxscene");
	fbxImporter->Import(pFbxScene);	//FBXのシーン、をインポートさせて、ゲームのシーン内にてインポート、使えるようにする？
	fbxImporter->Destroy();



	//メッシュ情報を取得
	//ノード→FBXを細分化すると、複数のノードという要素のつながりで作られている
		//ノードをたどり、一番根っこから３番目のノードには、
		//（複数のモデルをくっつけているならば、（モデルA,もでるB、モデルCの３つのモデルを１シーンにおいて作られたFBX））
		//３番目のノードには、モデルBの位置（Mesh）類が収まっているなど
		//情報がノードとして、一つのFBXが表現されている
	//一番根っこの１番目の子供には、→FBXに置かれているすべてのモデル（モデルA,モデルB、モデルC）の情報が入っていると考える（一つ一つ、モデルを分けてゲーム内で扱わないなら、１つのモデルとして扱うなら、左記の情報を使う）
	//一番根っこのノードを取得
	FbxNode* rootNode = pFbxScene->GetRootNode();
	//ルートノードの一番最初のノードを持ってくる
	//今から欲しい情報がすべて入っているノード
		//→おでんのモデルなら、こんにゃく、ちくわなど、すべてを結合したすべての情報
		//→各頂点、法線
			//→すべての情報を合わせたものをメッシュという
	FbxNode* pNode = rootNode->GetChild(0);
	//取得したノードの中の
	//頂点の情報、法線の情報、（ノードからメッシュを取得）
	FbxMesh* mesh = pNode->GetMesh();

	//各情報の個数を取得
	//頂点の情報がわからないとインデックス情報は作れない
	vertexCount_ = mesh->GetControlPointsCount();	//頂点の数
	//ないと描画できない
	polygonCount_ = mesh->GetPolygonCount();	//ポリゴンの数



	//ファイルパスから、自動でディレクトリ名、ファイル名を取得する関数
	//ディレクトリ名を取得する配列を用意して
	char dir[_MAX_DIR];         //ディレクトリ（フォルダ）名
								//MAX_DIRには、Windows側が、決めた最大はこのくらいだろうという標準のサイズが入っている


	//ファイル名の引数からディレクトリ名だけ取得する
	_splitpath_s(fileName.c_str(), nullptr, 0, dir, _MAX_DIR, nullptr, 0, nullptr, 0);
	//stringのファイル名をそのまま使えないので、普通のキャラ型に変更→ str.c_str()
	//ドライブ名、ディレクトリ名、ファイル名、ファイル識別子	をパスから自動で取得してくれる
	//必要のない部分はnullptr , 0　に
	//引数へcharの配列と、文字数サイズを送ったものに、その引数位置に適した文字列が帰ってくる



	//カレントディレクトリの変更前に
	//現在のカレントディレクトリを覚えておく（もとに戻すときのために）
	char defaultCurrentDir[MAX_PATH];						//MAX_PATHという、Window側の標準で決められた、最大文字数
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);		//現在のカレントディレクトリになっているパスを保存	


	//カレントディレクトリ＝現在見ているディレクトリ（標準でパスを見始めるディレクトリ）
	//★SetCurrentDirectoryをすることで、ファイルをロードするときに、それ以下から、呼びこむようになる（以降のファイルは、そのパス以下から読み込むようになる
	//Assetsとなるなら以降Assets以下から、のパスとなる
	SetCurrentDirectory(dir);
	//ロードするファイル名が引数にて取得できている（テクスチャが同様のパスに存在するならば、引数のファイル名からディレクトリパスをもらえればいい）
	//→ファイルパスの整形

	/*
		以下のバッファの作成で、
		失敗してしまうかもしれない
		→失敗したらSetCurrentDirectoryが変わった状態で、returnを返してしまう

		なので、SetCurrentなど、マネージャーの解放などは必ず行わなければいけない
		→失敗したら、その時々にSetCurrentDirectoryの処理を核でもいいし

		TryCatchでも
		※それらのエラー処理の書き方は、他の人のソースを見てみる
	*/

	//頂点バッファの作成を行う（引数のメッシュより）
	if (FAILED(InitVertex(mesh)))//頂点バッファ準備
	{
		MessageBox(nullptr, "頂点バッファの作成失敗", "エラー", MB_YESNO);

		//カレントディレクトリ
		//終わったら戻す
		SetCurrentDirectory(defaultCurrentDir);
		//マネージャ解放
		//マネージャーを解放することで、上記の諸々が解放されるようになっているので、マネージャーだけ解放
		pFbxManager->Destroy();
		return E_FAIL;
	};


	//インデックスバッファの作成
	if (FAILED(InitIndex(mesh)))		//インデックスバッファ準備
	{
		MessageBox(nullptr, "頂点バッファの作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};



	//カレントディレクトリを変更したので、戻したい
	//"Assets"というのが、カレントにすればよいが、、、、
		//現在のカレントが"Assets/Model"がカレントになっている.
		//そのうえで、Setに"Assets"と入れると、"Assets/Model/Assets"がカレントとなってしまう
		//→じゃあ、何個か上に戻せば、、→何個上なのかがわからない
	//単純に、変更を加える前のパスを戻しておけばいい
	//終わったら戻す
	SetCurrentDirectory(defaultCurrentDir);

	//マネージャ解放
	//マネージャーを解放することで、上記の諸々が解放されるようになっているので、マネージャーだけ解放
	pFbxManager->Destroy();

	//問題なく
	return S_OK;

}

void Branch::Release()
{
	//ポインタ宣言順と逆に解放
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_RELEASE(pVertexBuffer_);
	SAFE_DELETE_ARRAY(pVertices_);

}

int Branch::GetVertexCount()
{
	return vertexCount_;
}

int Branch::GetPolygonCount()
{
	return polygonCount_;
}

int Branch::GetIndexCount()
{
	return indexCountEachMaterial_;
}

Branch::VERTEX * Branch::GetVertexes()
{
	return pVertices_;
}

int * Branch::GetIndexes()
{
	return pIndex_;
}



//頂点バッファの作成
//引数においてメッシュを受け取り、FBXファイルから頂点情報を取得して、
//頂点バッファを作成する
HRESULT Branch::InitVertex(fbxsdk::FbxMesh * mesh)
{
	//頂点情報を入れる配列のポインタ
	//頂点数分

	//レイと、面との当たり判定のために、メンバに持っていくs
	pVertices_ = new VERTEX[vertexCount_];


	//全ポリゴン
	//ポリゴンの枚数分だけ回して
	//頂点情報をFBXから持ってくる
	for (DWORD poly = 0; poly < (DWORD)(polygonCount_); poly++)
	{
		//3頂点分（三角形のポリゴンなので、）
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//調べる頂点の番号
			//３番目のポリゴンの、何番目の頂点→とすれば、頂点の順番（頂点情報が存在するFBX情報から、全体の上から何番目の頂点だ）の位置が出てくる
			int index = mesh->GetPolygonVertex(poly, vertex);

			//頂点の位置
			//頂点の順番のその、ベクトルを取得
				//頂点が入っているFBXの情報から、index番目のベクトルを取得
			FbxVector4 pos = mesh->GetControlPointAt(index);
			//pVertices_[index].position = XMVectorSet((float)-pos[0], (float)pos[1], (float)pos[2], 0.0f);
				//FBXは左右反転になる前提なので、マイナスにして、左右反転の左右反転になる
				//だが、法線情報は、これにすることで、→法線情報を反転させなくてはいけなくなるので、
					//★左右反転が前提なのであれば、→ここでーにしないのも手
			pVertices_[index].position = XMVectorSet((float)pos[0], (float)pos[1], (float)pos[2], 0.0f);
			//接線の情報を取得するときに、
			//頂点の方向が逆になっていると、ややこしくなるので、　やめる
				//→これに伴って、インデックスの順番も逆にしていたものを、順番通りに

			////頂点のUV
			////位置と同様に、取得
			////１個目のレイヤー（UVも複数入れられるので、）を取得して専用の変数に
			//FbxVector2 uv = mesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			////UVの座標を登録
			////UVの座標は、Y座標が上下逆になっている→1からY座標を引けば、上下逆になる
			//pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0 - uv.mData[1]), 0, 0);
			//	//!注意!　ここで、Y座標における座標を登録するときに、逆に取得することで、


			//頂点のUV
			FbxLayerElementUV * pUV = mesh->GetLayer(0)->GetUVs();
			int uvIndex = mesh->GetTextureUVIndex(poly, vertex, FbxLayerElement::eTextureDiffuse);
			FbxVector2  uv = pUV->GetDirectArray().GetAt(uvIndex);
			pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0f - uv.mData[1]), 0.0f, 0.0f);



			//頂点の法線
			FbxVector4 Normal;
			mesh->GetPolygonVertexNormal(poly, vertex, Normal);	//ｉ番目のポリゴンの、ｊ番目の頂点の法線をゲット
			//pVertices_[index].normal = XMVectorSet((float)-Normal[0], (float)Normal[1], (float)Normal[2], 0.0f);
			pVertices_[index].normal = XMVectorSet((float)Normal[0], (float)Normal[1], (float)Normal[2], 0.0f);
		}
	}



	// 頂点バッファ作成
	if (FAILED(CreateVertexBuffer()))
	{
		MessageBox(nullptr, "頂点バッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};



	return S_OK;
}

//インデックスバッファ準備
//Quad(3Dモデル)と同じ要領で準備
HRESULT Branch::InitIndex(fbxsdk::FbxMesh * mesh)
{
	//インデックス数の初期化
	indexCountEachMaterial_ = 0;

	//ポリゴン数＊３　＝　インデックス数
	pIndex_ = new int[polygonCount_ * 3];


	int count = 0;

	//全ポリゴン
	//FBXからインデックス情報を取得する

	//ポリゴンごとに、頂点情報の取得を行う
	for (DWORD poly = 0; poly < (DWORD)(polygonCount_); poly++)
	{


			//★　頂点を反転しなかったので、　そのままのインデックスで登録
				//上記で反転していたのは、頂点を反時計回りに取得していたため、反転させていた
			for (int vertex = 0; vertex < 3; vertex++)
			{
				//インデックス情報に連番で確保
				//ポリゴンの頂点番号 0 , 1 , 2 の確保
				pIndex_[count] = mesh->GetPolygonVertex(poly, vertex);
				count++;	//配列のサイズを取得するために、要素数を取得するためのカウント
							//また、動的確保したインデックス情報を登録する配列の添え字としても使用する。

			}

	}


	//合計インデックス情報数の登録
	indexCountEachMaterial_ = count;


	// インデックスバッファを生成する
	if (FAILED(CreateIndexBuffer()))
	{
		MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};


	return S_OK;

}

HRESULT Branch::CreateVertexBuffer()
{
	//verticesはポインタで動的配列になったので、sizeof(vertices)では正しくサイズを取得することができない。
//	この場合はVERTEX型のサイズ*頂点数を指定すればよい。
	//頂点の数はわかっているので、→vertexの型でいくらのサイズをbyteで使うのか、＊頂点数→最終的に使用する全体サイズ

// 頂点データ用バッファの設定
//頂点情報へ代入
	D3D11_BUFFER_DESC bd_vertex;

	//頂点バッファの使用するバイト長を登録
	//頂点情報のサイズの登録
	//型名＊頂点数（頂点数だけ、VERTEXを取得している）（VERTEXの情報が何個必要なのか）
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	bd_vertex.CPUAccessFlags = 0;

	bd_vertex.MiscFlags = 0;

	bd_vertex.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA data_vertex;

	data_vertex.pSysMem = pVertices_;


	//Direct３DにおけるpDeviceにそれぞれのバッファを作成する
	//頂点バッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		MessageBox(nullptr, "頂点バッファの作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};

	return S_OK;
}

HRESULT Branch::CreateIndexBuffer()
{

	D3D11_BUFFER_DESC   bd;

	bd.Usage = D3D11_USAGE_DEFAULT;

	bd.ByteWidth = sizeof(int) * indexCountEachMaterial_;

	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

	bd.CPUAccessFlags = 0;

	bd.MiscFlags = 0;


	//バッファーへのアクセスポインタ
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex_;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	//インデックスバッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_YESNO);

		return E_FAIL;
	}
	return S_OK;

}


Branch::VERTEX::VERTEX() :
	position(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	uv(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	normal(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}
