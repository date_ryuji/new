#include "SkyBox.h"
#include "Model.h"
#include "Texture.h"
#include "Direct3D.h"
#include "Fbx.h"

#include "Camera.h"


//コンストラクタ
SkyBox::SkyBox(GameObject * parent)
	: GameObject(parent, "SkyBox"),
	hModel_(-1),
	//pTrackingObj_(nullptr),
	pSkyBoxTex_(nullptr)
{
}

SkyBox::~SkyBox()
{
	pSkyBoxTex_->Release();
	SAFE_DELETE(pSkyBoxTex_);
}

//初期化
void SkyBox::Initialize()
{
	//球体
	hModel_ = Model::Load("Assets/CubeMap/Boll.fbx", FBX_POLYGON_GROUP, SHADER_SKY_BOX);
	//箱
	//hModel_ = Model::Load("Assets/CubeMap/Cube.fbx", SHADER_SKY_BOX);
	assert(hModel_ > -1);

	//SkyBoxのテクスチャを写す、球体のサイズを決める
	transform_.scale_ = XMVectorSet(30.f , 20.f ,30.f , 0.f);	//球体の場合、この程度が一番自然
	//transform_.position_ = XMVectorSet(0.f, 10.f, 0.f, 0.f);


	//SkyBoxに写すテクスチャをロード
		//専用のテクスチャ＝CubeMap＝環境マップ用のテクスチャをロードする
		//テクスチャは、環境マップ用のキューブテクスチャなので、専用のロードを呼ぶ
	pSkyBoxTex_ = new Texture;
	//ロード
	if (FAILED(pSkyBoxTex_->LoadCube("Assets/CubeMap/HeighLight-Cube-Map/Cube_HeighLight_Red.dds")))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "SkyBox用のテクスチャ初期化の失敗", "エラー", MB_OK);
		assert(0);	//エラーを返す
	};
	//Release()による解放も忘れずに


}

//更新
void SkyBox::Update()
{

}

//描画
void SkyBox::Draw()
{
	Model::SetTransform(hModel_, transform_);


	//CubeMapの画像サンプラーを渡す
	//テクスチャの橋渡しを送る
					//hlsl : SkyBox.hlsl
	ID3D11ShaderResourceView* pSRV = pSkyBoxTex_->GetSRV();
	//シェーダーに作った変数に渡している
		//hlsl : g_normalTexture(t1)
		//第一引数に、(tn) のnを代入
	Direct3D::pContext->PSSetShaderResources(2, 1, &pSRV);
		//SkyBoxに使用する、CubeMapは、こちらで指定した別のファイルを使用したいので、
		//クラス内で、新たに橋渡しを行う



	//描画時　オブジェクトの回転などを考慮せずとも、背景に表示されているのは、
	//画像を反射させたものなので、
		//現在のカメラの視点によって、反射がされるようになっている。　つまり、現在見ている視点は、その視点方向の反射したときの周りの景色が映ることになる。
		//そのため、仮に親が回転していても、反射している画像自体は回転しないので、
		//→支店を回転させて、見る位置を変えたら、　その見ている視点における　反射が映る。　→つまり、360度の視点から背景を見ることができるということになる。

	Model::Draw(hModel_);

}

//開放
void SkyBox::Release()
{

}