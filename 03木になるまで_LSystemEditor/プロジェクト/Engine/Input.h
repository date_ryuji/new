#pragma once

#include <dInput.h>
#include <DirectXMath.h>	//XMVECTORが定義されている場所
#include "XInput.h"		//ゲームパッドの入力を受け付けるためのヘッダ
						//DirectInputのコントローラー入力を使用せずに、コントローラー入力を簡易的に入力受付を行えるようにした機能


//XMVECTORの型は、DirectXという名前空間に存在する型
	//→DirectXという名前空間を省略して使えるように→namespaceを定義して省略できるようにしておく（napespace std;と同様）
using namespace DirectX;

//リンクするライブラリファイルを宣言（教しえる）
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")
#pragma comment(lib,"Xinput.lib")	//XInputのライブラリ

//解放処理関数
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}





//全体でインプットするデバイスは一つなので、
//→inpuは一つで、何個もインスタンスは作らないので
//名前空間により宣言
	//名前空間なので、public , privateのアクセス修飾子は存在しない
namespace Input
{
	//ゲームパッドのキー（コントローラーのボタンを示すenumの値が長いので、短く）
		//Inputヘッダに、別の#defineで宣言するのも。
	enum PAD
	{
		A = XINPUT_GAMEPAD_A,
		B = XINPUT_GAMEPAD_B,
		X = XINPUT_GAMEPAD_X,
		Y = XINPUT_GAMEPAD_Y,
		DPAD_RIGHT = XINPUT_GAMEPAD_DPAD_RIGHT,
		DPAD_LEFT = XINPUT_GAMEPAD_DPAD_LEFT,
		DPAD_UP = XINPUT_GAMEPAD_DPAD_UP,
		DPAD_DOWN = XINPUT_GAMEPAD_DPAD_DOWN,
		START = XINPUT_GAMEPAD_START,
		BACK = XINPUT_GAMEPAD_BACK,
		LEFT_THUMB = XINPUT_GAMEPAD_LEFT_THUMB,			//左スティックの押し込み
		RIGHT_THUMB = XINPUT_GAMEPAD_RIGHT_THUMB,		//右スティックの押し込み
		LEFT_SHOULDER = XINPUT_GAMEPAD_LEFT_SHOULDER,	//左のL1ボタン
		RIGHT_SHOULDER = XINPUT_GAMEPAD_RIGHT_SHOULDER,	//右のR1ボタン


		/*
		#define XINPUT_GAMEPAD_DPAD_UP          0x0001
		#define XINPUT_GAMEPAD_DPAD_DOWN        0x0002
		#define XINPUT_GAMEPAD_DPAD_LEFT        0x0004
		#define XINPUT_GAMEPAD_DPAD_RIGHT       0x0008
		#define XINPUT_GAMEPAD_START            0x0010
		#define XINPUT_GAMEPAD_BACK             0x0020
		#define XINPUT_GAMEPAD_LEFT_THUMB       0x0040
		#define XINPUT_GAMEPAD_RIGHT_THUMB      0x0080
		#define XINPUT_GAMEPAD_LEFT_SHOULDER    0x0100
		#define XINPUT_GAMEPAD_RIGHT_SHOULDER   0x0200
		#define XINPUT_GAMEPAD_A                0x1000
		#define XINPUT_GAMEPAD_B                0x2000
		#define XINPUT_GAMEPAD_X                0x4000
		#define XINPUT_GAMEPAD_Y                0x8000

		
		*/

	};


	//初期化
	//引数：ウィンドウハンドル番号
	//戻値：なし
	HRESULT Initialize(HWND hWnd);
	//キーボードなどのデバイスの更新
	//引数：なし
	//戻値：なし
	HRESULT Update();	//Updateで、どのキーが押されているのか、を調べる
					//押されているものを配列にキー数以上に確保した登録
	
	///////////////////////////　キーボード　//////////////////////////////////
	//DirectInput

	//キーが押されているとき（常に）
	//引数：なし
	//戻値：関数結果のフラグ判断
	bool IsKey(int keyCode);	//KeyCodeに入った値が、押されているか、そのbool値を返す
								//Updateにて、配列に押されていることを示したものを調べて、押されているかの確認
								//★毎フレーム回って、押し続けている場合は、続けて更新し続けるので、trueを返す

	//今キーを押したか（一度だけ、押した判定を返してほしい→毎フレーム続けて押し続けても、trueの判定を返してほしくない）
	//引数：なし
	//戻値：関数結果のフラグ判断
	bool IsKeyDown(int keyCode);	//今キーを押したかを調べる＝前のキーが今押したキーと同じでないかを調べる
									//毎フレーム目では負えないスピードで回っているので、→そのフレームで、本来は１回押したいだけなのに、何回も回てしまう→これを避けるためにDownの機能が必要
									//Downによって、前のキーと違うキーならば、処理を行う、次のフレームにわたって、さらに押し続けられたとき
											//→前回と同じキーで、かつ、このDownの関数が呼ばれているならば、→同じ処理はしてほしくない
									//★前回のキーを保存しておき、その前回のキーが押していない、かつ、現在のUpdateにて受け取ったキーにて押している
									//★その場合は、trueを返す
	//キーが離されたとき（前回が押されていて、今回が０の時（IsKeyDownの逆））
	//引数：なし
	//戻値：関数結果のフラグ判断
	bool IsKeyUp(int keyCode);
	///////////////////////////　キーボード　//////////////////////////////////

	///////////////////////////　マウス　//////////////////////////////////
	//DirectInput

	//マウスのボタンは、キーボードと同様に押されているかを取得
		//ボタン→右、左、中ボタンの３つ
	//ボタンの現在の位置は、マウスのデバイスは情報を持っていないので、マウスからの情報でそのままもらうことは不可能→解決は
	
	//マウスのボタンが押されているか
		//引数：buttonCode	調べたいボタンの番号
	//戻値：押されていればtrue
	bool IsMouseButton(int buttonCode);

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	//引数：buttonCode	調べたいボタンの番号
	//戻値：押した瞬間だったらtrue
	bool IsMouseButtonDown(int buttonCode);

	//マウスのボタンを今放したか調べる
	//引数：buttonCode	調べたいボタンの番号
	//戻値：放した瞬間だったらtrue
	bool IsMouseButtonUp(int buttonCode);


	/*
		ボタンの位置はマウスのデバイスから取得することはできない
			→なので、Window側からのポジションをもらって、それぞベクトルへ変換し、返す関数とする
	*/
	//マウスカーソルの位置を取得
	//引数：なし
	//戻値：マウスカーソルの位置
	XMVECTOR GetMouseCursorPosition();

	//マウスのカーソル位置をセット
	//引数：ゲームウィンドウx値、ゲームウィンドウy値
	//戻値：なし
	void SetMousePosition(int  x, int y);

	/*
		マウスが、今回（１フレームでUpdateの更新がかかったのち）
		でどれだけxyz方向に移動したかの移動量は
		→マウスの押され具合を保存している構造体に、別の変数として保存されている

		/＊
		マウスの位置が、ウィンドウ内で、かつ
		マウスの左クリックが押されていて、かつ
		移動量分、が、一定の量移動されていたら、→その移動量分、線で軌跡を描けば、→移動の軌跡に線が描かれる
		＊/

	*/
	//そのフレームでのマウスの移動量を取得
	//引数：なし
	//戻値：X,Y マウスの移動量　　Z,ホイールの回転量
	XMVECTOR GetMouseMove();


	//マウスホイールが回転させられたか
		//マウスの構造体のZ方向がマウスのホイールが示すところ。
		//＋方向が奥に回された、-方向が手前に回された
	//bool 

	///////////////////////////　マウス　//////////////////////////////////
	

	///////////////////////////　コントローラー　//////////////////////////////////
	//XInput.h

	//コントローラーのボタンが押されているか調べる
	//DirectInputに、コントローラーの機能がある。
		//それとは別に、XInputという機能ができた（Xbox360ができて、Windowsでも、Xboxでもコントローラーが動く）
		//DirectInputよりも楽に、コントローラー操作、が実装できる
	//基本的には、Xboxしか保証しないとなっている。（他でも動作するが、）

	/*****コントローラーのボタン押下************************************/
	//コントローラーのボタンが押されているか、
	//引数：buttonCode	調べたいボタンの番号
	//引数：padID コントローラーの番号（コントローラーを指定する（何番目のコントローラー？））
			//padIDは、初期値で、０を入れている、→関数の引数を作成している時点での初期化
			//→そのため、padIDには、０が入っていて、
			//→呼び込み時に、引数にて、padIDを指定されて、値を入れたらpadIDを更新
			//→指定されなかったら、０
	//戻値：押されていればtrue
	bool IsPadButton(int buttonCode, int padID = 0);

	//コントローラーのボタンを今押したか調べる（押しっぱなしは無効）
	//引数：buttonCode	調べたいボタンの番号
	//引数：padID コントローラーの番号（コントローラーを指定する（何番目のコントローラー？））
	//戻値：押した瞬間だったらtrue
	bool IsPadButtonDown(int buttonCode, int padID = 0);

	//コントローラーのボタンを今放したか調べる
	//引数：buttonCode	調べたいボタンの番号
	//引数：padID コントローラーの番号（コントローラーを指定する（何番目のコントローラー？））
	//戻値：放した瞬間だったらtrue
	bool IsPadButtonUp(int buttonCode, int padID = 0);


	/*****アナログスティックごとの傾きを取得（傾きの大きさから移動量の計算に使用し、移動スピードの調整に用いる）************************************/
	//左スティックの傾きを取得
	//引数：padID コントローラーの番号（コントローラーを指定する（何番目のコントローラー？））
	//戻値:傾き具合（-1〜1）
	XMVECTOR GetPadStickL(int padID = 0);

	//右スティックの傾きを取得
	//引数：padID コントローラーの番号（コントローラーを指定する（何番目のコントローラー？））
	//戻値:傾き具合（-1〜1）
	XMVECTOR GetPadStickR(int padID = 0);

	/*****トリガーごとの押し込み具合を取得************************************/
			//トリガー　＝　R２,L２などなど		（深く押し込めるボタン）
	//左トリガーの押し込み具合を取得
	//引数：padID コントローラーの番号（コントローラーを指定する（何番目のコントローラー？））
	//戻値:押し込み具合（0〜1）
	float		GetPadTrrigerL(int padID = 0);

	//右トリガーの押し込み具合を取得
	//引数：padID コントローラーの番号（コントローラーを指定する（何番目のコントローラー？））
	//戻値:押し込み具合（0〜1）
	float		GetPadTrrigerR(int padID = 0);

	//振動させる
	//引数：l 左モーターの強さ（０〜６５５３５）
	//引数：r 右モーターの強さ（０〜６５５３５）
	//引数：padID コントローラーの番号（コントローラーを指定する（何番目のコントローラー？））
	void SetPadVibration(int l, int r, int padID = 0);


	//解放処理
	//引数：なし
	//戻値：なし
	void Release();
};
