#include "PolygonGroup.h"
#include "Texture.h"

PolygonGroup::PolygonGroup()
{
}

PolygonGroup::~PolygonGroup()
{
}

HRESULT PolygonGroup::Load(std::string fileName, SHADER_TYPE thisShader)
{
	return E_NOTIMPL;
}

HRESULT PolygonGroup::Draw(Transform & transform)
{
	return E_NOTIMPL;
}

void PolygonGroup::Release()
{
}

void PolygonGroup::RayCast(RayCastData * rayData)
{
}

XMVECTOR PolygonGroup::NormalVectorOfCollidingFace(XMVECTOR & start, XMVECTOR & dir)
{
	return XMVECTOR();
}

ID3D11Buffer * PolygonGroup::GetConstantBuffer()
{
	return nullptr;
}





RayCastData::RayCastData() :
	start(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	dir(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	normal(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	dist(99999.0f),
	hit(FALSE),
	polygonNum(-1)
{
}

