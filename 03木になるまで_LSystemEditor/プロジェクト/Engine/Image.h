#pragma once
//Spriteファイルを扱う
		//疑似フライウェイトパターンを扱う
#include <string>
#include <vector>
#include <Shlwapi.h>	//パスが存在するか//PathFileExists関数を使用するため
#include "Transform.h"
#include "Sprite.h"

#pragma comment(lib , "Shlwapi.lib")

//Model.h　、　Model.cppと同様に
//２Dオブジェクトのデータのロード、描画、Transformのセット
//１シーンにおける２Dオブジェクトの画像データを保存しておく動的配列を持つ
namespace Image
{
	//１シーンにて扱う画像データ
	//構造体の作成
			//使用する画像を保存しておくもの（ロードは１回しかしないとは別の話）
			//使用する画像１つ１つのデータを持っておくもの
	struct ImageData
	{
		Transform transform;
		std::string fileName;
		Sprite* pSprite;
		SHADER_TYPE thisShader;


		//構造体もコンストラクタが使用可能
		ImageData() : pSprite(nullptr)
		{}	//コンストラクタにて初期化
	};

	//ロード
	//画像のFBXをロードして、ロードを完了したら画像番号になる値を返して、
		//ロードできなかったらー１を返す
	//引数：fileName ロードする画像ファイル名
	//戻値：handle	画像を読み込んだら、その画像の番号。失敗したらー１
	int Load(std::string fileName, SHADER_TYPE thisShader = SHADER_2D);

	//指定された画像データのFbxの描画を行う
	//引数：handle 描画する画像番号
	//戻値：なし
	void Draw(int handle);

	//クラス全体の解放処理
	void Release();

	//画像データの全消去
	//画像データは、シーンが切り替えられたときに行えばよい（１っ回１っ回クラスオブジェクトが解放と同時にFbxファイルなどのデータが解放されても困る（せっかく画像データを保存しておいた意味がなくなる））
	//１つ１つの球をロードする際に、毎回ロードしたら困るので画像データを持っておく動的配列を宣言した。（１つの球が解放時に、画像データを動的配列から消したら。意味ない）
	void ReleaseAllImageData();

	//指定ハンドルのデータ解放
	void ReleaseOneData(int handle);


	//指定された画像データのTransfromをセットする
	//トランスフォームの位置、サイズ、拡大率を受け取る
	//引数：handle トランスフォームを変更する画像番号
	//引数：transform セットするトランスフォーム(&　基本型以外のデータを受け取るときは＆を使用)
	//戻値：なし
	void SetTransform(int handle, Transform& transform);

	//指定された画像データのTransfromをセットする
	//トランスフォームの位置、サイズ、拡大率を受け取る
	//引数：handle トランスフォームを変更する画像番号
	//引数：transform セットするトランスフォームのポインタ(中身をコピーするだけなので、ポインタでも問題なし、様々な受け渡し方に対応するために作った)
	//戻値：なし
	void SetTransform(int handle, Transform* transform);



	//指定された画像データのPositionをピクセル単位で動かす（現在位置から動かす）
		//画像が、　プロジェクション座標におけるZ０の位置にいる場合に限る（現段階）
		//現在のTransformに設置されているPositionの値は、ワールド座標のため、その座標を使用して、
			//スクリーン座標とする、
			//そして、そのスクリーン座標に値を足して、ワールドに直す
				//効率が悪いので、本来であれば、　画像は、Transformではなく、RectTransformなどで持っておく必要があるのかもしれない
				//RectTransform = 完全２D上でのTransformのようなもの
	//引数：位置をを変更する画像番号
	//引数：移動ピクセル位置　X
	//引数：移動ピクセル位置　Y
	void MovePixelPosition(int handle, int x, int y);


	//指定された画像データのPositionを引数ピクセル位置に設置する
		///使用されているTransformは、引数にて渡されたTransformのアドレスを使用している
		//そのため、Positionを変更するタイミングを考えなければいけない


	//引数ピクセル位置に移動
	//引数位置をワールド座標に変換し　その座標をTransformにする
	//引数：位置をを変更する画像番号
	//引数：セットピクセル位置　X
	//引数：セットピクセル位置　Y
	void SetPixelPosition(int handle, int x, int y);

	//画像サイズの変更
	//ピクセル感覚に縮小拡大
		//→画像を、引数ピクセルの大きさに変更
	//引数:画像番号
	//引数:サイズピクセル　X
	//引数:サイズピクセル　Y
	void SetPixelScale(int handle, int scaleX, int scaleY);

	//画像サイズの変更
	//ワールド感覚に縮小拡大
		//〇倍感覚
	//引数:画像番号
	//引数:拡大、縮小を入れたベクター
	void SetScale(int handle, XMVECTOR scale);




	//ワールド（プロジェクション）空間における移動量を求める
		//１ピクセル当たりの移動量から、引数ピクセルの移動量を求める
	//引数の移動ピクセルをワールド座標間隔に直した座標を返す
	//引数：セットピクセル位置　X
	//引数：セットピクセル位置　Y
	XMVECTOR MoveWorldToPixelConversion(int x, int y);


	//引数ピクセルから
		//ワールド（プロジェクションにおける）における座標を返す（Moveとは別）
	//引数：セットピクセル位置　X
	//引数：セットピクセル位置　Y
	XMVECTOR SetWorldToPixelConversion(int x, int y);


	//シェーダーを切り替える
	//引数：画像ハンドル
	//引数：切り替え先のシェーダータイプ
	void ChangeShader(int handle, SHADER_TYPE shaderType);

	//現在のシェーダーをもらう
	//引数：画像ハンドル
	SHADER_TYPE GetShader(int handle);


	//画像の当たり判定
		//渡されたマウスの位置と指定画像とで衝突しているかの判定
		//現段階：奥行きを考えない　ワールドのZ座標を変更した際の当たり判定には対応していない（Zが0の時のみ対応）
	//引数：位置をを変更する画像番号
	//引数：セットピクセル位置　X
	//引数：セットピクセル位置　Y
	void HitCollision(int handle, ImageCollision* imageCollision);


};

