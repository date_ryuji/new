#include "Fbx.h"
#include "Camera.h"
#include "Texture.h"
//#include "GameObject.h"		//Findを使いたいが、
							//Findは、自身がゲームオブジェクトであるとき限定に使えるもの
										//自身の親のポインタがルートジョブなら　そいつから、オブジェクトを探すことになるので、
											//自身がオブジェクトのポインタを持っていなければいけない。
							//→そして、Fbxのエンジンクラスをあまり、必要に汚したくない→シェーダークラスを作成し、そのクラスへ必要情報を持たせる

#include "Global.h"
#include "Math.h"
#include "../Shader/BaseShader.h"

Fbx::Fbx() :
	PolygonGroup::PolygonGroup()
{	
}

Fbx::~Fbx()
{
}

//引数のファイル名からFBXのロードを行う
//ロード関数
HRESULT Fbx::Load(std::string fileName, SHADER_TYPE thisShader)
{

	thisShader_ = thisShader;


	//FBXをロードするときはこの形式で書けと言われている

	//FBXのトップをCreateで呼ぶ
	//マネージャを生成
	FbxManager* pFbxManager = FbxManager::Create();

	//FBXをロードをする機能を持っているクラスの作成
	//ロードする人
	//インポーター（輸入、ロードして持ってくる）する人を生成
	FbxImporter *fbxImporter = FbxImporter::Create(pFbxManager, "imp");

	//ファイル名でインポーターがインポート（輸入、ロード）している
	fbxImporter->Initialize(fileName.c_str(), -1, pFbxManager->GetIOSettings());

	//ゲーム内のシーンに読み込む
	//１つのシーンに入れるそれぞれのシーンに、取り込む

	//シーンオブジェクトにFBXファイルの情報を流し込む
	FbxScene*	pFbxScene = FbxScene::Create(pFbxManager, "fbxscene");
	fbxImporter->Import(pFbxScene);	//FBXのシーン、をインポートさせて、ゲームのシーン内にてインポート、使えるようにする？
	fbxImporter->Destroy();



	//メッシュ情報を取得

	//ノード→FBXを細分化すると、複数のノードという要素のつながりで作られている
		//ノードをたどり、一番根っこから３番目のノードには、
		//（複数のモデルをくっつけているならば、（モデルA,もでるB、モデルCの３つのモデルを１シーンにおいて作られたFBX））
		//３番目のノードには、モデルBの位置（Mesh）類が収まっているなど
		//情報がノードとして、一つのFBXが表現されている
	//一番根っこの１番目の子供には、→FBXに置かれているすべてのモデル（モデルA,モデルB、モデルC）の情報が入っていると考える（一つ一つ、モデルを分けてゲーム内で扱わないなら、１つのモデルとして扱うなら、左記の情報を使う）
	//一番根っこのノードを取得
	FbxNode* rootNode = pFbxScene->GetRootNode();
	//ルートノードの一番最初のノードを持ってくる
	//今から欲しい情報がすべて入っているノード
		//→おでんのモデルなら、こんにゃく、ちくわなど、すべてを結合したすべての情報
		//→各頂点、法線
			//→すべての情報を合わせたものをメッシュという
	FbxNode* pNode = rootNode->GetChild(0);
	//取得したノードの中の
	//頂点の情報、法線の情報、（ノードからメッシュを取得）
	FbxMesh* mesh = pNode->GetMesh();

	//各情報の個数を取得
	//頂点の情報がわからないとインデックス情報は作れない
	vertexCount_ = mesh->GetControlPointsCount();	//頂点の数
	//ないと描画できない
	polygonCount_ = mesh->GetPolygonCount();	//ポリゴンの数


	//一つのノードの中に、マテリアル情報とメッシュ情報が入っている
		//→なので、
	materialCount_ = pNode->GetMaterialCount();


	//ファイルパスから、自動でディレクトリ名、ファイル名を取得する関数
	//ディレクトリ名を取得する配列を用意して
	char dir[_MAX_DIR];         //ディレクトリ（フォルダ）名
								//MAX_DIRには、Windows側が、決めた最大はこのくらいだろうという標準のサイズが入っている
			

	//ファイル名の引数からディレクトリ名だけ取得する
	_splitpath_s(fileName.c_str(), nullptr, 0, dir, _MAX_DIR, nullptr, 0, nullptr, 0);
	//stringのファイル名をそのまま使えないので、普通のキャラ型に変更→ str.c_str()
	//ドライブ名、ディレクトリ名、ファイル名、ファイル識別子	をパスから自動で取得してくれる
	//必要のない部分はnullptr , 0　に
	//引数へcharの配列と、文字数サイズを送ったものに、その引数位置に適した文字列が帰ってくる



	//カレントディレクトリの変更前に
	//現在のカレントディレクトリを覚えておく（もとに戻すときのために）
	char defaultCurrentDir[MAX_PATH];						//MAX_PATHという、Window側の標準で決められた、最大文字数
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);		//現在のカレントディレクトリになっているパスを保存	


	//カレントディレクトリ＝現在見ているディレクトリ（標準でパスを見始めるディレクトリ）
	//★SetCurrentDirectoryをすることで、ファイルをロードするときに、それ以下から、呼びこむようになる（以降のファイルは、そのパス以下から読み込むようになる
	//Assetsとなるなら以降Assets以下から、のパスとなる
	SetCurrentDirectory(dir);
	//ロードするファイル名が引数にて取得できている（テクスチャが同様のパスに存在するならば、引数のファイル名からディレクトリパスをもらえればいい）
	//→ファイルパスの整形

	/*
		以下のバッファの作成で、
		失敗してしまうかもしれない
		→失敗したらSetCurrentDirectoryが変わった状態で、returnを返してしまう

		なので、SetCurrentなど、マネージャーの解放などは必ず行わなければいけない
		→失敗したら、その時々にSetCurrentDirectoryの処理を核でもいいし
		 
		TryCatchでも
		※それらのエラー処理の書き方は、他の人のソースを見てみる
	*/

	//頂点バッファの作成を行う（引数のメッシュより）
	if (FAILED(InitVertex(mesh)))//頂点バッファ準備
	{
		MessageBox(nullptr, "頂点バッファの作成失敗", "エラー", MB_YESNO);
		
		//カレントディレクトリ
		//終わったら戻す
		SetCurrentDirectory(defaultCurrentDir);
		//マネージャ解放
		//マネージャーを解放することで、上記の諸々が解放されるようになっているので、マネージャーだけ解放
		pFbxManager->Destroy();
		return E_FAIL;
	};
			

	//インデックスバッファの作成
	if (FAILED(InitIndex(mesh)))		//インデックスバッファ準備
	{
		MessageBox(nullptr, "頂点バッファの作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};
	

	//コンスタントバッファの作成
	if (FAILED(IntConstantBuffer()))		//コンスタントバッファ準備
	{
		MessageBox(nullptr, "コンスタントバッファの作成失敗", "エラー", MB_YESNO);
		//カレントディレクトリ戻す
		SetCurrentDirectory(defaultCurrentDir);
		//マネージャ解放
		pFbxManager->Destroy();
		return E_FAIL;
	};


	//マテリアルの取得
		//マテリアルはノードに入っている→メッシュには入っていないのでノードを送ってその中から、取得する
	if (FAILED(InitMaterial(pNode)))
	{
		MessageBox(nullptr, "マテリアルの取得失敗", "エラー", MB_YESNO);
		//カレントディレクトリ戻す
		SetCurrentDirectory(defaultCurrentDir);
		//マネージャ解放
		pFbxManager->Destroy();
		return E_FAIL;
	};


	//カレントディレクトリを変更したので、戻したい
	//"Assets"というのが、カレントにすればよいが、、、、
		//現在のカレントが"Assets/Model"がカレントになっている.
		//そのうえで、Setに"Assets"と入れると、"Assets/Model/Assets"がカレントとなってしまう
		//→じゃあ、何個か上に戻せば、、→何個上なのかがわからない
	//単純に、変更を加える前のパスを戻しておけばいい
	//終わったら戻す
	SetCurrentDirectory(defaultCurrentDir);

	//マネージャ解放
	//マネージャーを解放することで、上記の諸々が解放されるようになっているので、マネージャーだけ解放
	pFbxManager->Destroy();

	//問題なく
	return S_OK;
}

//頂点バッファの作成
//引数においてメッシュを受け取り、FBXファイルから頂点情報を取得して、
//頂点バッファを作成する
HRESULT Fbx::InitVertex(fbxsdk::FbxMesh * mesh)
{
	//頂点情報を入れる配列のポインタ
	//頂点数分

	//レイと、面との当たり判定のために、メンバに持っていくs
	pVertices_ = new VERTEX[vertexCount_];


	//全ポリゴン
	//ポリゴンの枚数分だけ回して
	//頂点情報をFBXから持ってくる
	for (DWORD poly = 0; poly < (DWORD)(polygonCount_); poly++)
	{
		//3頂点分（三角形のポリゴンなので、）
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//調べる頂点の番号
			//３番目のポリゴンの、何番目の頂点→とすれば、頂点の順番（頂点情報が存在するFBX情報から、全体の上から何番目の頂点だ）の位置が出てくる
			int index = mesh->GetPolygonVertex(poly, vertex);

			//頂点の位置
			//頂点の順番のその、ベクトルを取得
				//頂点が入っているFBXの情報から、index番目のベクトルを取得
			FbxVector4 pos = mesh->GetControlPointAt(index);
			//pVertices_[index].position = XMVectorSet((float)-pos[0], (float)pos[1], (float)pos[2], 0.0f);
				//FBXは左右反転になる前提なので、マイナスにして、左右反転の左右反転になる
				//だが、法線情報は、これにすることで、→法線情報を反転させなくてはいけなくなるので、
					//★左右反転が前提なのであれば、→ここでーにしないのも手
			pVertices_[index].position = XMVectorSet((float)pos[0], (float)pos[1], (float)pos[2], 0.0f);
			//接線の情報を取得するときに、
			//頂点の方向が逆になっていると、ややこしくなるので、　やめる
				//→これに伴って、インデックスの順番も逆にしていたものを、順番通りに

			////頂点のUV
			////位置と同様に、取得
			////１個目のレイヤー（UVも複数入れられるので、）を取得して専用の変数に
			//FbxVector2 uv = mesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			////UVの座標を登録
			////UVの座標は、Y座標が上下逆になっている→1からY座標を引けば、上下逆になる
			//pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0 - uv.mData[1]), 0, 0);
			//	//!注意!　ここで、Y座標における座標を登録するときに、逆に取得することで、
		

			//頂点のUV
			FbxLayerElementUV * pUV = mesh->GetLayer(0)->GetUVs();
			int uvIndex = mesh->GetTextureUVIndex(poly, vertex, FbxLayerElement::eTextureDiffuse);
			FbxVector2  uv = pUV->GetDirectArray().GetAt(uvIndex);
			pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0f - uv.mData[1]), 0.0f, 0.0f);



			//頂点の法線
			FbxVector4 Normal;
			mesh->GetPolygonVertexNormal(poly, vertex, Normal);	//ｉ番目のポリゴンの、ｊ番目の頂点の法線をゲット
			//pVertices_[index].normal = XMVectorSet((float)-Normal[0], (float)Normal[1], (float)Normal[2], 0.0f);
			pVertices_[index].normal = XMVectorSet((float)Normal[0], (float)Normal[1], (float)Normal[2], 0.0f);
		}
	}

	//この方法で接線(tangent)を取得する
		//MAYAで作ったデータをマイナスにしていた。
		//だが、これをしていると、　ノーマルの情報もマイナスにしなくてはいけなくなる。
		//これはなしにする。

		//それに伴って、インデックスの登録順も逆にしていた。
		//これも　やめる。
			//頂点を逆にしないので、　インデックスも逆にする必要がない
	for (int i = 0; i < polygonCount_; i++)
	{
		int startIndex = mesh->GetPolygonVertexIndex(i);


		FbxGeometryElementTangent* t = mesh->GetElementTangent(0);

		//Tangentがない場合を除いて
		if (t != nullptr)
		{

			FbxVector4 tangent = t->GetDirectArray().GetAt(startIndex).mData;


			for (int j = 0; j < 3; j++)
			{
				int index = mesh->GetPolygonVertices()[startIndex + j];
				pVertices_[index].tangent = XMVectorSet((float)tangent[0], (float)tangent[1], (float)tangent[2], 0.0f);
			}
		}
	}


	// 頂点バッファ作成

	//verticesはポインタで動的配列になったので、sizeof(vertices)では正しくサイズを取得することができない。
	//	この場合はVERTEX型のサイズ*頂点数を指定すればよい。
		//頂点の数はわかっているので、→vertexの型でいくらのサイズをbyteで使うのか、＊頂点数→最終的に使用する全体サイズ

	// 頂点データ用バッファの設定
	//頂点情報へ代入
	D3D11_BUFFER_DESC bd_vertex;

	//頂点バッファの使用するバイト長を登録
	//頂点情報のサイズの登録
	//型名＊頂点数（頂点数だけ、VERTEXを取得している）（VERTEXの情報が何個必要なのか）
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	bd_vertex.CPUAccessFlags = 0;

	bd_vertex.MiscFlags = 0;

	bd_vertex.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA data_vertex;

	data_vertex.pSysMem = pVertices_;


	//Direct３DにおけるpDeviceにそれぞれのバッファを作成する
	//頂点バッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		MessageBox(nullptr, "頂点バッファの作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};

	return S_OK;
}



//インデックスバッファ準備
//Quad(3Dモデル)と同じ要領で準備
HRESULT Fbx::InitIndex(fbxsdk::FbxMesh * mesh)
{
	//ポインタのポインタ
	//なので、ポインタの配列を代入→ポインタのポインタ
	ppIndexBuffer_ = new ID3D11Buffer*[materialCount_];


	//インデックス情報を登録するint型配列の準備
	//indexには、どの頂点３つが、三角形になっているのかを頭から３つずつ登録していくもの
		//　＝　ポリゴンの枚数＊　３（３角化したため）（三角形を示す頂点を並べていくため、ポリゴン＊３で示す（頂点数よりも多くなる））
	
	//→★メンバ変数へ、かつ、マテリアルごとのポインタの２次元配列にしたいので、→ポインタのポインタへ
	//インデックス情報も、マテリアル分だけ欲しい
	//マテリアルごとに、そのマテリアルを適用されている、マテリアルごとにインデックスを分けていく
	//★インデックス情報に入れる頂点数を決める→マテリアルごとに分けるだけなので、→頂点数の最大を超えることは無いだろう→頂点数の最大を要素数とする
	//int* index = new int[polygonCount_ * 3];



	//それぞれの、マテリアルごとに要素数（最終的にそのマテリアルごとの、インデックス情報の要素　＝　マテリアルで使用されている頂点数）
	//登録するためのポインタ
	indexCountEachMaterial_ = new int[materialCount_];	//マテリアル数で要素を確保
	//++で要素を足していくので、初期化
	for (int i = 0; i < materialCount_; i++)
	{
		indexCountEachMaterial_[i] = 0;
	}
		
	//★マテリアルごとのインデックス情報
	//まずマテリアルの数分の、ポインタを確保。
		//配列を入れる配列の要素を確保
	ppIndex_ = new int*[materialCount_];

	//マテリアルの数分だけループ
	//複数のインデックスバッファ、それぞれ（マテリアルごとにインデックスバッファを分ける）
	for (int i = 0; i < materialCount_; i++)
	{
		//繰り返しの中で、
			//配列の要素に配列を確保して、要素をポリゴン数分確保
		ppIndex_[i] = new int[polygonCount_ * 3];

		int count = 0;

		//全ポリゴン
		//FBXからインデックス情報を取得する

		//マテリアルをマテリアルごとに、分けてインデックス情報を分ける
		for (DWORD poly = 0; poly < (DWORD)(polygonCount_); poly++)
		{
			//マテリアルの情報を見る
			FbxLayerElementMaterial *   mtl = mesh->GetLayer(0)->GetMaterials();
			//今見ているpoly番目は、何番目のマテリアルなのか、
			int mtlId = mtl->GetIndexArray().GetAt(poly);

			//今取得したマテリアルの番号が、青だったら、青を登録するように以下の処理に進む
			//変数のiは、マテリアル数分回っている→今欲しいマテリアル番号iと、取得したマテリアルの番号とが、同じなら、登録しなくてはいけないインデックス情報なので、

			if (mtlId == i)
			{
				//3頂点分

				//FBXに入っている、インデックスバッファの情報が逆に入っている


				//	本来は、時計回りにインデックス情報が入っていてほしい
				//	→だが、なぜか、インデックス情報が、逆になっている
				//	→これを、時計回りにインデックス情報を登録するようにしたい

				//	反時計に入っているなら、→時計回りに登録するように
				//	→インデックス情報を逆から登録するようにする
				//	→繰り返しの最大の終了条件から、 - 1になるまで繰り返す
				
				//０，１，２を２，１，０にしたい
				//であれば、０〜２を２〜０にすればいい→だが、DWORDはunsignedなので、−を扱うことができない→なので、−１になると、大きな値が入ってしまう（アンダーフロー）
					//DWORD＝unsignedint	→手っ取り早いのは、→変数をintとする
				//for (int vertex = 2; vertex >= 0; vertex--)
				//★　頂点を反転しなかったので、　そのままのインデックスで登録
					//上記で反転していたのは、頂点を反時計回りに取得していたため、反転させていた
				for (int vertex = 0; vertex < 3; vertex++)
				{
					//２次元配列にしたので、
						//現在見ている配列にアクセス
					ppIndex_[i][count] = mesh->GetPolygonVertex(poly, vertex);
					count++;	//配列のサイズを取得するために、要素数を取得するためのカウント
								//また、動的確保したインデックス情報を登録する配列の添え字としても使用する。
					
				}
			}
		}
		indexCountEachMaterial_[i] = count;


		//インデックスバッファの設定
		//byte vertexSize = sizeof(int) * (polygonCount_*3);	//バイトのサイズを取得したいので、→要素数分、配列の型で掛ける
				//要素数を数えることが可能なので、配列の型　＊　要素数　でサイズを指定

		// インデックスバッファを生成する

		D3D11_BUFFER_DESC   bd;

		bd.Usage = D3D11_USAGE_DEFAULT;

		bd.ByteWidth = sizeof(int) * count;	//インデックス情報に登録した数分だけサイズ取得（マテリアルごとに配列を回しているので）
											//サイズを、indexのサイズ分だけ、（インデックス情報の入っている配列のバイトサイズだけ）
											//for分内にて、カウントした変数をかけて、型＊要素数とする

		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

		bd.CPUAccessFlags = 0;

		bd.MiscFlags = 0;



		D3D11_SUBRESOURCE_DATA InitData;

		InitData.pSysMem = ppIndex_[i];	//Quadにて送る際に、（void*）でキャストを行わないとエラーになったが、
									//今回においては、動的確保したint配列においてそのまま送ってもエラーにはならなかった
									//�@マテリアルごとのインデックス情報を渡さないといけないので、今回のインデックス情報のみを渡してあげる
		InitData.SysMemPitch = 0;

		InitData.SysMemSlicePitch = 0;

		//インデックスバッファの作成
		if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &ppIndexBuffer_[i])))
		{
			//エラーを、どこでしたのかを知らせるためのメッセージボックス
			MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_YESNO);
			
			return E_FAIL;
		}

	}
	//完全に処理の終えた（バッファを作成し終えたのちに）消去
	//CreateBuffer時点でindex(ポインタ)を登録したInitDataを使用しているので、indexの領域は参照されている→その段階で解放されてしまっていたら、エラーが起こる可能性もある。
	//SAFE_DELETE(ppIndex_);//ポインタの解放
	return S_OK;

}

//コンスタントバッファの作成
//Quadと同様
HRESULT Fbx::IntConstantBuffer()
{
	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	UINT size = sizeof(CONSTANT_BUFFER);
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//コンスタントバッファを作成する
	//Deviceに引数としてバッファを作成してもらう
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	
	}
	return S_OK;

};

//マテリアルの確保
HRESULT Fbx::InitMaterial(fbxsdk::FbxNode* pNode)
{
	//マテリアルを入れる配列の宣言
	//マテリアルの数分だけ要素を取得→materialCountより
	pMaterialList_ = new MATERIAL[materialCount_];

	//マテリアル数分だけ繰り返す
	for (int i = 0; i < materialCount_; i++)
	{
		//マテリアル１つ１つを見て、
		//textureが張られているか、貼られていないかを見ていく
		
		// i番目のマテリアル情報を取得
		//１つのマテリアルの情報を持ってくる
			FbxSurfaceMaterial* pMaterial = pNode->GetMaterial(i);	


			//テクスチャの有り無し関係なく、
			//MAYA情報を取得する

			/*通常色（ディフューズ）*************************************/
			//マテリアルの色
			//マテリアルのポインタを取得
			//FbxSurfaceLambert* pMaterial = (FbxSurfaceLambert*)pNode->GetMaterial(i);
			//FbxSurfaceMaterial* pMaterial = pNode->GetMaterial(i);
			//フォンのマテリアルポインタの取得
				//フォンにキャスト
				//この時点ではフォンをMAYA上で使ったどうかは気にしていない
			FbxSurfacePhong* pPhong = (FbxSurfacePhong*)pMaterial;

			//マテリアルのポインタの中のDiffuse。そのものの色を取得
			FbxDouble3  diffuse = pPhong->Diffuse;
			//マテリアルの構造体変数へ、上記で取得した色を入れる。
			pMaterialList_[i].diffuseColor = XMFLOAT4((float)diffuse[0], (float)diffuse[1], (float)diffuse[2], 1.0f);






			/*環境光（アンビエント）******************************/
			//環境光のマテリアル情報取得
			FbxDouble3 ambient = pPhong->Ambient;
			//構造体変数へ　色情報を取得
			pMaterialList_[i].ambientColor = XMFLOAT4((float)ambient[0], (float)ambient[1], (float)ambient[2], 1.0f);

			/*鏡面反射情報の初期化********************************/
			pMaterialList_[i].specularColor = XMFLOAT4(0, 0, 0, 0);
			pMaterialList_[i].shininess = 1;


			/*鏡面反射（スペキュラー）******************************/
			//ラバートのマテリアルからスペキュラーを取得するとエラーになるので注意
			//フォンの場合
			//FbxSurfaceLambert* pMaterial = (FbxSurfaceLambert*)pNode->GetMaterial(i);
				//Lambertの部分をPhongにしなければいけない
			//フォンシェーディングだったら
				//フォンシェーディングを選んだのか、番号が帰って来る
					//フォンを使ったとなったならスペキュラーの情報を取得したい
			if (pMaterial->GetClassId().Is(FbxSurfacePhong::ClassId))
			{
				//フォンのマテリアルがあるなら
				//フォンの中のスペキュラーなどの情報を取得
				FbxDouble3 specular = pPhong->Specular;

				//シェーダー内にて計算していたスペキュラー情報を取得
				pMaterialList_[i].specularColor = XMFLOAT4((float)specular[0], (float)specular[1], (float)specular[2], 1.0f);

				pMaterialList_[i].shininess = (float)pPhong->Shininess;


			}

			//スペキュラーは、
			//テクスチャが張ってあっても、必要。
			//アンビエントも。

			//なので、共通部分で取得するようにしなくてはいけない。



			//鏡面反射
			//FbxDouble3 speclur = pMaterial->




		/*Diffuse（通常色）用のテクスチャのロード*/
			//Deffuse用のテクスチャを取得
			//テクスチャが何枚張られているか
			//張られていたら、テクスチャ名をロードする
				/*ノーマルマップでも、同じことをやって、テクスチャをロードすればよい*///必要部分をコピーすればよい
				//渡すときは、　これまでと同様に
			{

				if (FAILED(LoadDiffuseTexture(i, pMaterial)))
				{
					MessageBox(nullptr, "ディフューズテクスチャのロード失敗", "エラー", MB_YESNO);
					return E_FAIL;
				}
			}

			/*ノーマルマップ用(でこぼこ)のテクスチャのロード*/
			{
				if (FAILED(LoadNormalMapTexture(i, pMaterial)))
				{
					MessageBox(nullptr, "ノーマルマップテクスチャのロード失敗", "エラー", MB_YESNO);
					return E_FAIL;
				}
			}
	}

	return S_OK;
}


HRESULT Fbx::LoadNormalMapTexture(int i, FbxSurfaceMaterial* pMaterial)
{

	//テクスチャ情報
		//プロパティを探す
		//ノーマルマップ用のテクスチャを探すので、
		//::sNormalMap
	FbxProperty  lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sNormalMap);
	//ノーマルマップ＝RGBの色により、偽の凹凸をつけさせrう
		//白に近いところは明るく、黒に近いところは暗くさせるように描画させることで、疑似の凹凸をつける




	//テクスチャの数数
	//textureの貼り付けた枚数→普通１枚だが、高解像度で、顔と体を別テクスチャで、枚数を分けることがある（１つのモデルに対して）
	int fileTextureCount = lProperty.GetSrcObjectCount<FbxFileTexture>();



	//テクスチャあり
	//　＝　テクスチャの枚数が０以外であったら、
	//　＝　テクスチャあり　＝　１以上　＝　真
	//　＝　テクスチャなし　＝　０　＝　偽
	if (fileTextureCount)
	{
		//テクスチャをロード

		//テクスチャのファイル名を取得
		//テクスチャのソースから
		FbxFileTexture* textureInfo = lProperty.GetSrcObject<FbxFileTexture>(0);
		const char* textureFilePath = textureInfo->GetRelativeFileName();
		//標準では、パスが間違いでエラーになる
		//原因：示されているフォルダが違う（Assetsから読み込みたい）
		//解決：画像が入っているフォルダから読み込みたい（Assets下からとも限らない）
		//ルール：FBXと同じ位置にテクスチャファイルを置くとした
		//解決：LoadでFBXファイルの頭のフォルダと同様の位置がもらえればいい
				//SetCurrentDirectoryをすることで、ファイルをロードするときに、それ以下から、呼びこむようになる（以降のファイルは、そのパス以下から読み込むようになる）


		//★★プログラムにおいて、起きるかもしれない問題を考えた処理を行う★★
		//ファイル名+拡張だけにする（もしかしたら、取得したパスはフルパスかもしれない）
		//カレントディレクトリを変更して、（FBXファイルと同じファイルを置くディレクトリまでをカレントディレクトリとした）
		//→そのうえで、取得したFilePathを、きちんと、ファイル名＋拡張子だけとした（直前のディレクトリまでをカレントとしたならば、→ほしいのは、ファイルだけ）
			//直前のディレクトリまでの、フルパスで入ってしまうことがあり、その可能性も示唆しないといけないので、ファイル名と、拡張子だけにしたい
			//せっかくカレントディレクトリをファイル前のディレクトリとしても、フルパスで持ってこようとすると、まずいので、
		char name[_MAX_FNAME];	//ファイル名
		char ext[_MAX_EXT];	//拡張子
		_splitpath_s(textureFilePath, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
		//printfと同じ扱い方で、printfの結果を第一引数の変数へ代入する
		wsprintf(name, "%s%s", name, ext);	//ファイル名変数　＝　ファイル名＋拡張し


		//ファイルからテクスチャ作成
		//テクスチャのクラスに、画像ファイルを読み込むための初期化を書けばいい（使用されているテクスチャをテクスチャクラスにテクスチャを作成）

		//MATERIAL構造体内のTextureクラスを作成
		pMaterialList_[i].pNormalTexture = new Texture;
		//Textureクラスに画像ファイルをロードする（ファイル名を指定して）(ファイル名は上記にて取得した)
		if (FAILED(pMaterialList_[i].pNormalTexture->Load(name)))
		{
			//エラーを、どこでしたのかを知らせるためのメッセージボックス
			MessageBox(nullptr, "ファイル読み込み失敗", "エラー", MB_YESNO);
			return E_FAIL;
		}

	}

	//テクスチャ無し
	else
	{
		//テクスチャがない場合は、
		//MATERIALクラスのpTextureを nullにする
		pMaterialList_[i].pNormalTexture = nullptr;	//ポインタヌル

		//テクスチャが存在しないということは、
		//マテリアルそのものの色を取得させればよい
	}


	return S_OK;
}

HRESULT Fbx::LoadDiffuseTexture(int i, FbxSurfaceMaterial * pMaterial)
{
	//テクスチャ情報
					//プロパティを探す
	FbxProperty  lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sDiffuse);
	//ディフューズ→拡散反射光
	//みかんはオレンジ色→本当に？　オレンジでないんだって、→オレンジの光を全方向に反射する質感である（物そのものの色ということ）
							//つまり→色に設定した情報を持ってくる（Colorに設定した情報を持ってくる）





//テクスチャの数数
//textureの貼り付けた枚数→普通１枚だが、高解像度で、顔と体を別テクスチャで、枚数を分けることがある（１つのモデルに対して）
	int fileTextureCount = lProperty.GetSrcObjectCount<FbxFileTexture>();



	//テクスチャあり
	//　＝　テクスチャの枚数が０以外であったら、
	//　＝　テクスチャあり　＝　１以上　＝　真
	//　＝　テクスチャなし　＝　０　＝　偽
	if (fileTextureCount)
	{
		//テクスチャをロード

		//テクスチャのファイル名を取得
		//テクスチャのソースから
		FbxFileTexture* textureInfo = lProperty.GetSrcObject<FbxFileTexture>(0);
		const char* textureFilePath = textureInfo->GetRelativeFileName();
		//標準では、パスが間違いでエラーになる
		//原因：示されているフォルダが違う（Assetsから読み込みたい）
		//解決：画像が入っているフォルダから読み込みたい（Assets下からとも限らない）
		//ルール：FBXと同じ位置にテクスチャファイルを置くとした
		//解決：LoadでFBXファイルの頭のフォルダと同様の位置がもらえればいい
				//SetCurrentDirectoryをすることで、ファイルをロードするときに、それ以下から、呼びこむようになる（以降のファイルは、そのパス以下から読み込むようになる）


		//★★プログラムにおいて、起きるかもしれない問題を考えた処理を行う★★
		//ファイル名+拡張だけにする（もしかしたら、取得したパスはフルパスかもしれない）
		//カレントディレクトリを変更して、（FBXファイルと同じファイルを置くディレクトリまでをカレントディレクトリとした）
		//→そのうえで、取得したFilePathを、きちんと、ファイル名＋拡張子だけとした（直前のディレクトリまでをカレントとしたならば、→ほしいのは、ファイルだけ）
			//直前のディレクトリまでの、フルパスで入ってしまうことがあり、その可能性も示唆しないといけないので、ファイル名と、拡張子だけにしたい
			//せっかくカレントディレクトリをファイル前のディレクトリとしても、フルパスで持ってこようとすると、まずいので、
		char name[_MAX_FNAME];	//ファイル名
		char ext[_MAX_EXT];	//拡張子
		_splitpath_s(textureFilePath, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
		//printfと同じ扱い方で、printfの結果を第一引数の変数へ代入する
		wsprintf(name, "%s%s", name, ext);	//ファイル名変数　＝　ファイル名＋拡張し


		//ファイルからテクスチャ作成
		//テクスチャのクラスに、画像ファイルを読み込むための初期化を書けばいい（使用されているテクスチャをテクスチャクラスにテクスチャを作成）

		//MATERIAL構造体内のTextureクラスを作成
		pMaterialList_[i].pTexture = new Texture;
		//Textureクラスに画像ファイルをロードする（ファイル名を指定して）(ファイル名は上記にて取得した)
		if (FAILED(pMaterialList_[i].pTexture->Load(name)))
		{
			//エラーを、どこでしたのかを知らせるためのメッセージボックス
			MessageBox(nullptr, "ファイル読み込み失敗", "エラー", MB_YESNO);
			return E_FAIL;
		}

	}

	//テクスチャ無し
	else
	{
		//テクスチャがない場合は、
		//MATERIALクラスのpTextureを nullにする
		pMaterialList_[i].pTexture = nullptr;	//ポインタヌル

		//テクスチャが存在しないということは、
		//マテリアルそのものの色を取得させればよい





	}

	return S_OK;
}




//Quad(3Dモデル)と同様に描画
//描画
HRESULT Fbx::Draw(Transform & transform)
{

	


	//シェーダーを選択
	Direct3D::SetShaderBundle(thisShader_);
	//Direct3D::SetShaderBundle(SHADER_OUTLINE);
	//３Dのシェーダーを使用するために、Direct3D内の、シェーダーをセットする、関数へ、enumの値を扱ってあげる
	//→Direct3Dをインクルードしているので、　→enumの値で、セットしてあげた名前を使用することが可能


	//シェーダーの種類を選択して、
	//1回目　SHADER_OUTLINE
	//2回目　SHADER_TOONになるように選択
		//1っ回目は、OUTLINEで描画されて、
		//2回目は、TOONで描画される。
			//→そうすることで、
			//→2回描画で、かつシェーダーを変更して描画が可能になる。
	//for (int i = 0; i < 2; i++)
	{



		////コンスタントバッファを開く（マップ）
		//if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))
		//{
		//	//エラーを、どこでしたのかを知らせるためのメッセージボックス
		//	MessageBox(nullptr, "コンスタントバッファのマップの書き込み失敗", "エラー", MB_YESNO);

		//	return E_FAIL;	//エラーですと返す。
		//					//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

		//					//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
		//}

							//Mapとして、書き込むために呼び込む
		//配列の中身を丸っとコピーを行うmemcpyと同じように
		//丸っとコピーさせる
		//memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	// データを値を送る
						//配列にあるデータなどを、他の領域にまるっとコピー
						//memcpy_s (コピー先、コピー先のサイズ、コピー元　、コピー元のサイズ)
						//コンスタントバッファに先ほど作ったものを書き込む
							//コピー元の先頭アドレスを入れたい、void 型のアドレス→アドレスであって、特にどの型でもないので、voidの、アドレスの型にキャストしている
							//構造体で作った、cbのアドレスを、→入れる

		//テクスチャのサンプラーの取得
		//textureはUV情報を登録するときに使用する

		//テクスチャはそのFBXにテクスチャが貼り付けてないならば、サンプラーは必要ない
		//逆に、テクスチャがあるならば、サンプラーを作らないといけない
		//→じゃあ、それはどこで分かるのか、→マテリアルを作成するときに、テクスチャが存在するか、しないかを参照したはず、
		//そのテクスチャが存在しないnullptrでないならば→作成



		////全マテリアルを調べる
		////マテリアルの中で、テクスチャが存在するなら、テクスチャのサンプラー作成
		//for (int i = 0; i < materialCount_; i++)
		//{
		//	//マテリアルのテクスチャがnullptrでないならば
		//	if (pMaterialList_[i].pTexture != nullptr)
		//	{
		//		//「テクスチャの」サンプラー作成（それぞれのマテリアルごとに）
		//		ID3D11SamplerState* pSampler = pMaterialList_[i].pTexture->GetSampler();	//テクスチャクラスあkら、サンプラーを取得
		//		Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);
		//		//シェーダーへの橋渡しのビューを取得
		//		ID3D11ShaderResourceView* pSRV = pMaterialList_[i].pTexture->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
		//		Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);
		//	}
		//}



		////編集を行った、コンスタントバッファを閉じる（Unマップ）
		//Direct3D::pContext->Unmap(pConstantBuffer_, 0); 	//再開
		//					//書き込んだので、最後に、Unmapにて、　閉じる
		//					//コンスタントバッファに送る



							//頂点バッファ（頂点を持っておくための領域（確保場所））をセット
			//複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		UINT stride = sizeof(VERTEX);	//1頂点のサイズを指定する、→位置のみであれば、XMVECTORの構造体であったが、
									//位置とUVで構造体をとるので、その構造体のサイズで
		UINT offset = 0;
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);



		//コンスタントバッファ（登録（メンバ内のコンスタントバッファを））
		Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
		Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用



		//インデックスバッファは、ポインタのポインタなので、
		//ポインタの配列分回してセットする必要がある
		// インデックスバッファーをセット
		for (int i = 0; i < materialCount_; i++)
		{
			stride = sizeof(int);
			offset = 0;

			//それぞれのインデックスバッファを配列から添え字にて指定し、送る
			Direct3D::pContext->IASetIndexBuffer(ppIndexBuffer_[i], DXGI_FORMAT_R32_UINT, 0);


			//コンスタントバッファの受け渡し。
			D3D11_MAPPED_SUBRESOURCE pdata;
			//シェーダ内に渡すための、のコンスタントバッファを作成（メンバの構造体）
					//コンスタントバッファに渡す情報
			CONSTANT_BUFFER cb;
			{

				//ワールド、ビュー、プロジェクション行列を掛けたもの
				//cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() *Camera::GetProjectionMatrix());	//シェーダーに持っていくときに、行列のために、縦に読む行列と、横に読む行列があり、それのどちらかにしないといけないので　
											//★受け取った、Transformのワールド行列を取得
										//XMMatrixTranspose＝行列を、縦読みするか、横読みするか（その縦と横を入れ替える（））

				//シェーダーにワールド行列を渡す準備（自身の構造体変数に情報を入れて、のちに、シェーダーに入れる。）
				//カメラのビュー行列を掛けると。消えてしまった。
					//カメラからの座標になったので、
					//カメラから奥行き１ｍしか今は表示されていない。→それはプロジェクション座標は奥行きが１ｍなので、現段階だと。１ｍの位置にしか表示されていないので、オブジェクトは、１ｍ奥にいて、本当はあるのだが、見えていない。
				//そして、プロジェクション行列を掛けることで、
					//カメラの視推台を奥行き１ｍ　（２＊２＊１のサイズに）圧縮するので、
					//オブジェクトも、プロジェクションの範囲に圧縮されるので、見えるようになる。
				//スクリーンへの行列は、勝手に掛けられるので、ここですることはない。（）
				cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix()* Camera::GetViewMatrix() * Camera::GetProjectionMatrix());

				//ワールド行列
					//法線の回転のために
					//ライトは固定なのに、、陰も一緒に回っていた。
						//それを回転状況を考慮して、法線を回転させた。
				cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));	//

				//テスト段階
				//コンスタントバッファをシェーダーに送っている部分を見つけることができないため、
				//→同じコンスタントバッファの変数である、部分を変更し、反映されている部分に変更を与えてみる
				/*
				//変更が起こっていない→変更をさせることができていない。
					//必ずtrueが実行されてしまっている
				//→diffuseColorを変更して、isTextureがtrueの時にdiffuseColorを参照するとする→きちんと色が反映された（cb.diffuseColor = XMFLOAT4(1, 0, 0, 0);//赤色を表示するようにfloat4を変更した）
					//★つまり、diffuseColorは問題なし？？
				//if文、フラグにおいて、いろいろ試したが、どうやらif文におけるフラグが使用できていない



				//★★boolを読み込んでくれない。解決策。
				https://qastack.jp/gamedev/22600/why-cant-i-get-a-bool-packed-and-aligned-into-a-d3d-constant-buffer
				//ここによると、HLSLでのbool型は、１バイトしか使わなくても、４バイトで格納されているらしい
				//→そして、HLSLでは、boolの値を、４バイトのうち４番目の１バイトに登録している
				//読み込んでくるCPUは、bool値を、４バイトのうち１番目のバイトから持ってくる→ここで、見ることができてないらしい

				//解決策：boolを使用せずに、intを使った値にする（intにtrue,falseを入れて、判断に使用する）
				*/
				//cb.diffuseColor = XMFLOAT4(0, 0, 1, 0);
				//cb.isTexture = false;


				//ワールド行列を入れる
					//ワールド行列を渡して、
					//頂点からカメラへの向きというのを取得するために、頂点に掛けてやらないといけない（移動、回転、拡大を込みしたワールド位置にするために）
					//※だが、これなら、ワールド行列など、個別に渡すよりも、
						//移動行列、回転行列、拡大行列をそれぞれ渡してやって、頂点シェーダー内で計算したほうが良い気がする。
						//あれも、これもと用意するのであれば、、、
				cb.matWorld = XMMatrixTranspose(transform.GetWorldMatrix());


				//カメラの位置を入れる
					//XMVECTOR型をFLOAT4型に入れることはできない。
					//変換する関数があったが、忘れてしまったので、1つずつFLOAT４型に変換して代入
				cb.camPos = XMFLOAT4(Camera::position_.vecX, Camera::position_.vecY, Camera::position_.vecZ, 0);





				//描画の際に、
				//描画を、テクスチャを描画させるのか、マテリアルの色を描画させるのかで分けなければいけない
				//そのために、シェーダーに渡す情報である、コンスタントバッファの変数に情報を送らなければいけない（コンスタントバッファは、共通して使用するとして、そのマテリアルごとに、切り替える）
					//マテリアルごと描画を行うインデックスを判断しているのは、この繰り返し、→であれば、ここで、コンスタントバッファの情報を入れ替えて、描画をしてもらえばいい
					//マテリアルの色、フラグ
				//テクスチャあり
				if (pMaterialList_[i].pTexture != nullptr)
				{
					//nullptrでない　＝　テクスチャが存在する
					//「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
						//サンプラーは、どこに貼り付けますか？などの情報
					//hlsl : g_sampler(s0)
					ID3D11SamplerState* pSampler = pMaterialList_[i].pTexture->GetSampler();	//テクスチャクラスから、サンプラーを取得
					//ここにおける第一引数が何番目のサンプラーなどか、
					Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);
					//シェーダーへの橋渡しのビューを取得
					//hlsl : g_texture(t0)
					ID3D11ShaderResourceView* pSRV = pMaterialList_[i].pTexture->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
						//シェーダーに作った変数に渡している
					Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);


					cb.isTexture = TRUE;	//コンスタントバッファのテクスチャ有無にtrueを立てる
					//cb.isTexture = cb.isTexture | 128;

					//cb.diffuseColor	//マテリアルの色は使用しないため、登録しない
				}
				//テクスチャなし
				else
				{
					//nullptrである　＝　テクスチャが存在しない
					//　＝　マテリアルそのものの色を使用しないといけない
					cb.isTexture = FALSE;


				}
				//コンスタントバッファに送らなくてはいけない
				//送るのは、コンスタントバッファなのか、cbなのか、pConstantBufferなのか、pContextなのか

				//シェーダーをきちんと理解しているか、
				//シェーダーにどこで情報を渡しているのか

					//ノーマルマップ　用のテクスチャをシェーダーに渡す
				if (pMaterialList_[i].pNormalTexture != nullptr)
				{
					//テクスチャの橋渡しを送る
					//hlsl : NormalMap.hlsl
					ID3D11ShaderResourceView* pSRV = pMaterialList_[i].pNormalTexture->GetSRV();
					//シェーダーに作った変数に渡している
						//hlsl : g_normalTexture(t1)
						//第一引数に、(tn) のnを代入
					Direct3D::pContext->PSSetShaderResources(1, 1, &pSRV);


					//サンプラーは共通のものを使用するため、
						//ここでは何もしない


				}
				//テクスチャの有無関係なく
				//マテリアルのフォンの情報や、通常色の情報を
				//コンスタントバッファへ
				{
					//現在のマテリアルを見ているので、
					//
					cb.diffuseColor = pMaterialList_[i].diffuseColor;	//マテリアルリストに登録されている、diffuseのマテリアルの色を登録
																	//コンスタントバッファ側が同じ変数の型であることを確認

					cb.ambientColor = pMaterialList_[i].ambientColor;
					cb.supecularColor = pMaterialList_[i].specularColor;
					cb.shininess = pMaterialList_[i].shininess;



					//*******トゥーンシェーディング用のテクスチャをシェーダーファイルに渡す*******				
					//サンプラーは同様のものを使う。
					////「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
					////hlsl : Sampler
					//ID3D11SamplerState* pSampler = pMaterialList_[i].pTexture->GetSampler();	//テクスチャクラスから、サンプラーを取得
					////ここにおける第一引数が何番目のサンプラーなどか、
					//Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);


					////テクスチャの橋渡しを送る
					////hlsl : Texture
					//ID3D11ShaderResourceView* pSRV = Direct3D::pToonTex->GetSRV();
					////シェーダーに作った変数に渡している
					//	//hlsl : g_toonTex(t1)
					//	//第一引数に、(tn) のnを代入
					//Direct3D::pContext->PSSetShaderResources(1, 1, &pSRV);





				}

				//自身の使用する
			//シェーダー独自の
			//コンスタンバッファや、テクスチャの受け渡しを行う
				Direct3D::GetShaderClass(thisShader_)->ShaderConstantBuffer1View();



				//SHADER=WATER限定
					//本来はFindで探したいが、
				{
					//if (thisShader_ == SHADER_WATER)
					//{
					//	////水のクラスを探す
					//	//float static scroll = 0.0f;
					//	//float addValue = 0.001f;
					//	//
					//	//
					//	//scroll += 0.0005f;

					//	//cb.scroll = scroll;

					//	//テクスチャの橋渡しを送る
					//	//hlsl : NormalMap.hlsl
					//	ID3D11ShaderResourceView* pSRV = Direct3D::pCubeTex->GetSRV();
					//	//シェーダーに作った変数に渡している
					//		//hlsl : g_normalTexture(t1)
					//		//第一引数に、(tn) のnを代入
					//	Direct3D::pContext->PSSetShaderResources(2, 1, &pSRV);


					//
					//}

					//専用のシェーダークラスにてセット
						//ShaderConstantBuffer1View（）関数にて、それぞれセット
					////環境マップシェーダー時
					//	//キューブのテクスチャをセット　シェーダーに渡す
					//if (thisShader_ == SHADER_WATER || 
					//	thisShader_ == SHADER_ENVI || 
					//	thisShader_ == SHADER_NORMAL || 
					//	thisShader_ == SHADER_WETBALL || 
					//	thisShader_ == SHADER_FOG)
					//{
					//	//テクスチャの橋渡しを送る
					//	//hlsl : NormalMap.hlsl
					//	ID3D11ShaderResourceView* pSRV = Direct3D::pCubeTex->GetSRV();
					//	//シェーダーに作った変数に渡している
					//		//hlsl : g_normalTexture(t1)
					//		//第一引数に、(tn) のnを代入
					//	Direct3D::pContext->PSSetShaderResources(2, 1, &pSRV);

					//}


				}


			}



			//★コンスタントバッファに変更を加えたので、この時点での変更を保存するために開く(更新を送ってあげる)
			//★→コンスタントバッファで開いて、メモリに保存、書き込みを行う
			//コンスタントバッファを開く（マップ）
			if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))
			{
				//エラーを、どこでしたのかを知らせるためのメッセージボックス
				MessageBox(nullptr, "コンスタントバッファのマップの書き込み失敗", "エラー", MB_YESNO);

				return E_FAIL;	//エラーですと返す。
								//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

								//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
			}

			//Mapとして、書き込むために呼び込む（変更をした、コンスタントバッファを書き込む）
			//メモリーをコピー
			memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	// データを値を送る
							//配列にあるデータなどを、他の領域にまるっとコピー
							//memcpy_s (コピー先、コピー先のサイズ、コピー元　、コピー元のサイズ)
							//コンスタントバッファに先ほど作ったものを書き込む
								//コピー元の先頭アドレスを入れたい、void 型のアドレス→アドレスであって、特にどの型でもないので、voidの、アドレスの型にキャストしている
								//構造体で作った、cbのアドレスを、→入れる

			//最後にコンスタントバッファを閉じる
			//編集を行った、コンスタントバッファを閉じる（Unマップ）
			Direct3D::pContext->Unmap(pConstantBuffer_, 0); 	//再開
								//書き込んだので、最後に、Unmapにて、　閉じる
								//コンスタントバッファに送る


			//描画
			//★頂点の個数を表す、→ここの頂点の数を、最終的な三角形の頂点の数
			//★この情報を追加しないと、頂点による、三角形を描画してくれない（たとえ頂点情報、インデックス情報を変更しても。。。）

			//頂点数を調べたいが、→インデックスバッファを複数に分けたことにより、
			//それぞれのマテリアルごと、インデックス情報が違い、→頂点数が違くなる
			//→
			Direct3D::pContext->DrawIndexed(indexCountEachMaterial_[i], 0, 0);	//頂点が６つなら、６つ
		}



		////2回回して、2回目はトゥーン
		//Direct3D::SetShaderBundle(SHADER_TOON);
	}

	//成功
	return S_OK;

}

//解放処理
void Fbx::Release()
{

	/*
	//コンスタントバッファなどの
	//ID3D11は、DELETEはしてはいけない
	SAFE_RELEASEを使用する（その中で、ポインタのクラスのさらにRelease()関数を呼ぶ（＋ポインタのアドレスは解放=nullptr）（ポインタを使えなくする））
	*/

	//ポインタ宣言順と逆に解放
	SAFE_RELEASE(pConstantBuffer_);	
	SAFE_DELETE_ARRAY(indexCountEachMaterial_);


	
	//ポインタのポインタ（複数個確保したポインタの）解放
	for (int i = 0; i < materialCount_; i++)
	{
		//MaterialList内のポインタの解放
		if (pMaterialList_[i].pTexture != nullptr) { pMaterialList_[i].pTexture->Release(); SAFE_DELETE(pMaterialList_[i].pTexture);};
		if (pMaterialList_[i].pTexture != nullptr) { pMaterialList_[i].pNormalTexture->Release(); SAFE_DELETE(pMaterialList_[i].pNormalTexture); };

		//まず、ポインタのポインタの→中身のポインタをすべて解放
		SAFE_DELETE_ARRAY(ppIndex_[i]);

		SAFE_RELEASE(ppIndexBuffer_[i]);//ID3D11型なので、Release
	}
	SAFE_DELETE_ARRAY(pMaterialList_);	//配列のポインタを解放

	//そして最後に側のポインタを解放(ポインタのポインタ)
	SAFE_DELETE_ARRAY(ppIndex_);
	SAFE_DELETE_ARRAY(ppIndexBuffer_);	//ポインタのポインタなので、中の要素である、ID3D11の解放はSAFE_RELEASEにて行い、
									//そのポインタは、DELETEにて解放（側は、ただの実体のないポインタである。）

	SAFE_DELETE_ARRAY(pVertices_);

	SAFE_RELEASE(pVertexBuffer_);
	

}



void Fbx::RayCast(RayCastData* rayData)
{
	//マテリアル毎
	//インデックス情報を、マテリアルごとに　分けてあるので、
		//配列を指定する添え字
	for (DWORD i = 0; i < (DWORD)(materialCount_); i++)
	{
		//そのマテリアルのポリゴン毎
			//indexCountEachMaterial_ = マテリアルごとのインデックス情報の数。
			//　＝　３頂点ごと　＝　１ポリゴン（面）ごと
			//マテリアルごとのインデックス情報を / 3 = 面の枚数だけ回る
			//										 = jをうまく使って、連続するインデックス情報をもらわなければいけない。
		for (DWORD j = 0; j < (DWORD)(indexCountEachMaterial_[i]) / 3; j++)
		{
			//indexは、マテリアルごとに取得している
				//つまり、indexの行はi
			//列は、ポリゴン事取得するので、
			//インデックス情報は、三角形を構成する頂点が時計回りに順番に並んでいる。ということは０番め、１番目、２番目が１つの三角形を示す。→この流れがポリゴン数　毎　続く
			// j = 0 
				//０番目＝　j * 3
				//１番目＝　j * 3 +  1
				//２番目＝　j * 3 +  2
			//これでインデックス情報の頂点番号が取得できる
				//頂点番号を、頂点情報の引数にして、位置を取得
			// j = 1 
				//３番目＝　j * 3
				//４番目＝　j * 3 +  1
				//５番目＝　j * 3 +  2


			//3頂点の位置情報を取得
				//インデックス情報は、
				//あらかじめ、時計回りで入っている。
			XMVECTOR v0 = pVertices_[ppIndex_[i][(j * 3) + 0]].position;
			XMVECTOR v1 = pVertices_[ppIndex_[i][(j * 3) + 1]].position;
			XMVECTOR v2 = pVertices_[ppIndex_[i][(j * 3) + 2]].position;

			float dist = 0.0f;

			//その三角形との判定
				//レイの情報は引数にてもらっているので、
				//その情報と、
				//三角形の頂点の情報を与えて、接触判定を行う。
			bool hit = Math::Intersect(
				rayData->start ,		//レイ発射位置
				rayData->dir ,			//レイの方向
				v0 ,					//三角形頂点０
				v1 ,					//三角形頂点１ 
				v2 , 					//三角形頂点２
				&dist);		//レイと面の接触位置までの長さ
			

			//当たっていたら
			if (rayData->hit)
			{
				//当たったフラグを立てる
				rayData->hit = true;
				if (rayData->dist > dist)
				{
					//前回に当たった地点よりも短いとき更新
					rayData->dist = dist;

					//面の法線ベクトルを求める
					//�@ v1 - v0 = v0 から　v1 へのベクトル
					//�A v2 - v0 = v0 から　v2 へのベクトル
					//�B上記の2つの外積で、法線ベクトルを求める
					XMVECTOR vector1 = v1 - v0;
					XMVECTOR vector2 = v2 - v0;

					//rayData->normal = XMVector3Cross(vector1, vector2);
					rayData->normal = XMVector3Cross(vector2, vector1);
				}
				//return;
			}
		}
	}
}

XMVECTOR Fbx::NormalVectorOfCollidingFace(XMVECTOR & start, XMVECTOR & dir)
{
	RayCastData rayData;
	rayData.start = start;
	rayData.dir = dir;

	//当たり判定を実行
	RayCast(&rayData);

	//結果の法線ベクトルを返す
	return rayData.normal;
}



ID3D11Buffer * Fbx::GetConstantBuffer()
{
	return pConstantBuffer_;
}


Fbx::MATERIAL::MATERIAL() :
	pTexture(nullptr), pNormalTexture(nullptr),
	diffuseColor(XMFLOAT4(0.f, 0.f, 0.f, 0.f)),
	ambientColor(XMFLOAT4(0.f, 0.f, 0.f, 0.f)),
	specularColor(XMFLOAT4(0.f, 0.f, 0.f, 0.f)),
	shininess(0.f)
{
}

Fbx::VERTEX::VERTEX() :
	position(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	uv(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	normal(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	tangent(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}
