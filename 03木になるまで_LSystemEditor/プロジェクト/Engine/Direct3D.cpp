#include <wincodec.h>
#include <d3dcompiler.h>
#include "Direct3D.h"

#include "Direct2D.h"	//テキスト表示のためのDirect2Dクラス

#include "Camera.h"
#include "Texture.h"
#include "Sprite.h"	//レンダーターゲットにて書き込んだ画用紙を、３Dオブジェクトとして、書き込んで、それを描画させる

//シェーダー生成クラス
#include "../Shader/ShaderFactory.h"
//シェーダークラスの継承元クラス
#include "../Shader/BaseShader.h"

//ブレンド生成クラス
#include "../Blend/BlendFactory.h"
//ブレンドクラスの継承元クラス
#include "../Blend/BaseBlend.h"



#pragma comment( lib, "WindowsCodecs.lib" )


//変数
namespace Direct3D

{
	//クラスのオブジェクト作成
//オブジェクトを作成するときは、ポインタで作成すると決めているので、ポインタで
	ID3D11Device*           pDevice = nullptr;		//デバイス
													//監督	
	ID3D11DeviceContext*    pContext = nullptr;		//デバイスコンテキスト
													//絵を描く人
	//IDXGISwapChain*         pSwapChain1 = nullptr;		//スワップチェイン
														//裏で書いた紙と、表の紙を入れ替える
	ID3D11RenderTargetView* pRenderTargetView = nullptr;	//レンダーターゲットビュー
	
	ID3D11Texture2D*	pDepthStencil = nullptr;			//深度ステンシル（Zバッファ法（隠面消去を行う））
												//ウィンドウに表示する描画を、カメラから、前面にあるものは前面に塗り、後面にあるものは、塗らずに前面の色を残す処理（そのようにすれば、カメラから全面の色だけが残り、後面の色は棒がされなくなる）
	ID3D11DepthStencilView* pDepthStencilView = nullptr;		//深度ステンシルビュー（深度ステンシルとの仲人）
	
	ID3D11Texture2D*	pRenderTexture = nullptr;	//レンダーターゲット　（新しい書き込み先）
	//複数の画面の書き込み先を用意することで、
		//複数に書き込み、それを使い分けることで、　複数画面があるように見せる。
	ID3D11RenderTargetView* pRenderTargetView2 = nullptr;	//レンダーターゲットビュー

	//書き込み先のオブジェクト
	Sprite* pScreen = nullptr;

	/*Direct2Dと連携するための必要要素*/
	//Direct2Dを３Dと共用するために使用するスワップチェインなど
	//IDXGI = Direct3DとDirect2Dとで連携の橋渡しのようなことをしてくれる
	//デバイス
	IDXGIDevice* pGIDevice = nullptr;
	//デバイスコンテキスト　と
	//スワップチェーンの接続を担う
	IDXGISurface* pGISurface = nullptr;
	//Direct2Dにおける連携を可能とするバージョンのSwapChain
	//通常のスワップチェインは、Direct2Dとの連携に対応していない。
		//そのため、連携を対応しているSwapChain1（Ver.1）を使用する
	IDXGISwapChain1* pSwapChain1 = nullptr;



	//スクリーンサイズフルの描画範囲
	D3D11_VIEWPORT vpBig;
	//縮小サイズの描画範囲
		//複数画面にしたいときに、描画範囲を複数作り、それを切り替える。
	D3D11_VIEWPORT vpSmall;

															
//デバイスコンテキストとレンダーターゲットとの橋渡し		
	//→Main.cppにて、使うものを、この中で宣言

	//全体に応じて、使用するものなら、ヘッダに、（他クラスにおいても使用するものなどは、ヘッダで、）
	//この中でのみ使用するなら、cppにのみで使用するなら、ここで宣言

	//cpp内で、名前空間に、以上のポインタを追加している


	int scrWidth = 0;
	int scrHeight = 0;
	
	Texture* pToonTex = nullptr;
	Texture* pCubeTex = nullptr;


	//シェーダークラス（継承先）のインスタンスを入れておく配列
	/*シェーダー情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
	BaseShader* shaderBundle_[SHADER_MAX] = { nullptr };	//全要素をnullptrにて初期化
	/*シェーダー情報をDirect3Dに持たせるパターン***********************************************************************************************************************************/
	/*
	//シェーダー情報
	//シェーダーの情報を持たせるクラスを作成し、
	//そのクラスのインスタンスをDirect3Dに持たせておく
		//そして、シェーダー切り替え時に、それぞれのクラスの、SetShaderを呼びこんで、そのクラスのシェーダーをセットさせる

	//シェーダーの情報
	//コンパイル後のシェーダーの情報を保持しておく構造体
		//シェーダーを切り替えるときに、この情報をセットし、扱う
	//シェーダーをコンパイルする
	//コンパイルしたものを入れるためのポインタ
		//２D用、３D用のために配列を使用する
		//→だが、それだったら、一つの構造体にして、その配列にしたほうがきれいに見える
	struct ShaderBundle {

		ID3D11VertexShader*	pVertexShader = nullptr;	//頂点シェーダー
		ID3D11PixelShader*	pPixelShader = nullptr;		//ピクセルシェーダー
		ID3D11InputLayout*	pVertexLayout = nullptr;	//頂点インプットレイアウト
		ID3D11RasterizerState*	pRasterizerState = nullptr;	//ラスタライザー
	};
	ShaderBundle shaderBundle[SHADER_MAX];
			//構造体の変数を取得して、　shaderBundle[0]の頂点シェーダと複数取得ができた
				//→構造体にてまとまっていたほうが、それぞれ複数取得するのでまとまっていてわかりやすい
				//+名前で管理したいので、enumの名前で２D,3Dで指定できるように

	//構造体内のポインタを使用
			//構造体自体はぽいんたではないので　構造体-> メソッドではない
	//shaderBundle[0]内の ポインタpVertexShaderを　アドレスで渡したいときは、
			//
	*/


	//ブレンドクラス（継承先）のインスタンスを入れておく配列
	/*ブレンド情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
	BaseBlend* blendBundle_[BLEND_MAX] = { nullptr };
	/*ブレンド情報をDirect3Dに持たせるパターン***********************************************************************************************************************************/
	/*
	//ブレンド　をコンパイルするための構造体
		//シェーダーのように、必要な時にセットして切り替える
	struct BlendBundle
	{
		//ブレンドの基本情報
			//セットの段階では使用しない
			//初期化の段階で、Stateに情報を渡している
		//D3D11_BLEND_DESC* pBlendDesc;

		//透過
		ID3D11BlendState* pBlendState;
		//青の半透明の板があって、
		//赤いタマがあったら、　紫に見える。
			//つまり、色が混ざっているか、いないかで透明になっているように、見える。
		//透明なら、後ろの色を透過させた、　後ろの色と合わせた色にすれば、透過しているように見える

	};
	BlendBundle blendBundle[BLEND_MAX];
	*/



	//背景の色（色情報）
		//初期値＝{ 0.0f, 0.5f, 0.0f, 1.0f };　//黄緑　 //R,G,B,A（A=透明）
	float clearColor_[4] = { 0.0f, 0.5f, 0.0f, 1.0f };
	//色を識別するenum値
	enum RGBA
	{
		R = 0, G,B,A
	};





}

//Direct3D初期化
//Device・DeviceContext・SwapChainを同時作成
	//Direct2D非対応
#if 0
	HRESULT Direct3D::InitDirect3D(int winW, int winH , HWND hWnd)
	{
		//ウィンドウスクリーンのサイズを設定
		scrWidth = winW;
		scrHeight = winH;



		/*******ウィンドウ表示したものに、***************************************************************************/
	///////////////////////////いろいろ準備するための設定///////////////////////////////
	//いろいろな設定項目をまとめた構造体
		DXGI_SWAP_CHAIN_DESC scDesc;

		//とりあえず全部0
		ZeroMemory(&scDesc, sizeof(scDesc));

		//描画先のフォーマット（画面のサイズ→ウィンドウサイズを画面のサイズにするので、そのまま）
		//バックバッファのサイズを指定している。→これを小さくすると→描画のウィンドウサイズの画面の大きさに引き伸ばされる
		scDesc.BufferDesc.Width = winW;		//画面幅
		scDesc.BufferDesc.Height = winH;	//画面高さ
		scDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;	// 何色使えるか
									//８bit	＝　２５５　（RGBそれぞれ使える領域）
									//０〜２５５色（Rだけで、２５６色）→フルカラー

		//FPS（1/60秒に1回）
		scDesc.BufferDesc.RefreshRate.Numerator = 60;
		scDesc.BufferDesc.RefreshRate.Denominator = 1;

		//その他
		scDesc.Windowed = TRUE;			//ウィンドウモードかフルスクリーンか
										//Trueにすると、ウィンドウのサイズのみ、Falseにするとフルスクリーン
		scDesc.OutputWindow = hWnd;		//ウィンドウハンドル
		scDesc.BufferCount = 1;			//バックバッファの枚数（裏で絵を描くものの枚数　１枚で）
		scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	//バックバッファの使い道＝画面に描画するために
		scDesc.SampleDesc.Count = 1;		//MSAA（アンチエイリアス）の設定（ギザギザを滑らかに）
		scDesc.SampleDesc.Quality = 0;		//　〃


		////////////////上記設定をもとにデバイス、コンテキスト、スワップチェインを作成////////////////////////
		D3D_FEATURE_LEVEL level;

		//ポインタのクラスオブジェクトを生成しなくてはいけない
			//newの代わりに、DirectXで用意されているもの↓


		HRESULT hr;	//結果を入れる変数（HRESULT型）//if文のFAILE()の（）の中に入れるには、長すぎると思ったため、見やすく変数でとった
		hr =
			D3D11CreateDeviceAndSwapChain(
				nullptr,				// どのビデオアダプタを使用するか？既定ならばnullptrで
										//グラフィックボードどれを使うか→nullptrでデフォルト

				D3D_DRIVER_TYPE_HARDWARE,		// ドライバのタイプを渡す。ふつうはHARDWARE
												//ハードウェアに直接指定するが、→対応していないハードウェアにやろうとすると→、対応してなかったら、ソフトウェアにする
												//→ハードウェアで対応しなかったあ、ソフトウェアにすることができるが、→それだと遅い（なので、）

				nullptr,				// 上記をD3D_DRIVER_TYPE_SOFTWAREに設定しないかぎりnullptr
				D3DCOMPILE_DEBUG,					// 何らかのフラグを指定する。（デバッグ時はD3D11_CREATE_DEVICE_DEBUG？）
				nullptr,				// デバイス、コンテキストのレベルを設定。nullptrにしとけばOK
				0,					// 上の引数でレベルを何個指定したか
				D3D11_SDK_VERSION,			// SDKのバージョン。必ずこの値

				//出来上がったものを、オブジェクトに適用
				&scDesc,				// 上でいろいろ設定した構造体
				&pSwapChain1,				// 無事完成したSwapChainのアドレスが返ってくる
				&pDevice,				// 無事完成したDeviceアドレスが返ってくる
				&level,					// 無事完成したDevice、Contextのレベルが返ってくる
				&pContext);				// 無事完成したContextのアドレスが返ってくる

		if (FAILED(hr))
		{
			//エラーのメッセージボックスを返す（Windows機能）
			MessageBox(nullptr, "デバイス、コンテキスト、スワップチェイン作成失敗", "エラー", MB_OK);
			//上記のHRESULT型の戻り値が、失敗（E_~）を返したなら
			return E_FAIL;		//呼び出し元の関数へエラーを送る
		}


		///////////////////////////レンダーターゲットビュー作成///////////////////////////////
		//スワップチェーンからバックバッファを取得（バックバッファ ＝ レンダーターゲット）
		ID3D11Texture2D* pBackBuffer;

		if (FAILED(pSwapChain1->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
		{
			//エラーのメッセージボックスを返す（Windows機能）
			MessageBox(nullptr, "スワップチェインのバッファーの取得失敗", "エラー", MB_OK);
			//上記のHRESULT型の戻り値が、失敗（E_~）を返したなら
			return E_FAIL;		//呼び出し元の関数へエラーを送る

		}
		//pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

		//仮の紙を用意して、　対応するレンダーターゲットを設定して・・・
		//レンダーターゲットビューを作成
		if (FAILED(pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView)))
		{
			//エラーのメッセージボックスを返す（Windows機能）
			MessageBox(nullptr, "レンダーターゲットビュー作成失敗", "エラー", MB_OK);
			//上記のHRESULT型の戻り値が、失敗（E_~）を返したなら
			return E_FAIL;		//呼び出し元の関数へエラーを送る
		}
		//pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView);

		//一時的にバックバッファを取得しただけなので解放
		pBackBuffer->Release();


		///////////////////////////ビューポート（描画範囲）設定///////////////////////////////
		//遠くのもの小さく見えて、近くのもの大きく見える
		//視野で、見える範囲が、広がっていく
			//→それを広がっているものを、視点から、前に長方形に切り取ったものとしてみさせる
				//→なので、遠くのものは小さく見える

		//描画する範囲のため、
		//複数画面を表示するときは、
		//複数個用意する
			//表示画面ごと


		//レンダリング結果を表示する範囲
		{
			D3D11_VIEWPORT vp;
			vp.Width = (float)winW;	//幅
			vp.Height = (float)winH;//高さ
			vp.MinDepth = 0.0f;	//手前
			vp.MaxDepth = 1.0f;	//奥
								//長方形に変形したものを、１ｍの範囲にギュッとつぶした

			vp.TopLeftX = 0;	//左
			vp.TopLeftY = 0;	//上
		}
		//BeginDraw１にて使用するビューポートサイズ
		{

			//レンダリング結果を表示する範囲
			//vpSmall.Width = (float)winW / 10;	//幅
			//vpSmall.Height = (float)winH / 10;//高さ

			vpSmall.Width = (float)winW;	//幅
			vpSmall.Height = (float)winH;//高さ
			vpSmall.MinDepth = 0.0f;	//手前
			vpSmall.MaxDepth = 1.0f;	//奥
								//長方形に変形したものを、１ｍの範囲にギュッとつぶした

			vpSmall.TopLeftX = 0;	//左
			vpSmall.TopLeftY = 0;	//上

		}
		//拡大表示
		//Spriteとして表示するテクスチャのところの、サイズを10分の１にして
		//テクスチャ

		//1ドットが１０＊１０で表示された
			//モザイクのように見える

			//テクスチャのFilterが、POINTにすると、　ボケる。
			//だが、　LINEARにすると、ちかちかするような表現になる。


		//深度ステンシルビューの作成(Zバッファ（前のものは前、後ろのものは後ろに描画）)
		D3D11_TEXTURE2D_DESC descDepth;
		descDepth.Width = winW;
		descDepth.Height = winH;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_D32_FLOAT;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		pDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
		pDevice->CreateDepthStencilView(pDepthStencil, NULL, &pDepthStencilView);



		//データを画面に描画するための一通りの設定（パイプライン）
		pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);  // データの入力種類を指定
		pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);            // 描画先を設定
																//第３引数に深度ステンシルを入れて、Zバッファ（いんめん消去）を渡す
		pContext->RSSetViewports(1, &vpBig);



		//シェーダーの準備
		if (FAILED(InitShader()))
		{
			//エラー処理
			//エラー原因の　メッセージボックスを表示
			MessageBox(nullptr, "シェーダの初期化の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
		}
		//InitShader();
		//エラーが帰ってくるとする→すると、このInitShaderを呼び出している関数自体は、
			//→一番上の関数ではない→Direct3DのInitializeを読んでいる関数がいるはず→なので、　その呼び出し元に、またエラーと送る
			//呼び出し元でエラー処理してもらう（ここでは、とにかくエラーですと送る）

		//ブレンドの初期化
			//色の混ぜ方
		if (FAILED(InitBlend()))
		{
			MessageBox(nullptr, "ブレンドクラスの初期化失敗", "エラー", MB_OK);
			return E_FAIL;
		};

		//カメラの初期化
		Camera::Initialize();


		//カメラに位置、焦点を渡してやって、
		//それで、行列を作ってもらう
		//カメラの位置を送る
		//Camera::SetPosition(XMVectorSet(1, 0, 0, 0));	//カメラの位置をセット
		//この方式と、もう一つ、ｘ、ｙ、ｚだけを送り、それを受け取ってもらう関数も欲しい




		//トゥーンシェーディング用のテクスチャのロード
		pToonTex = new Texture;
		//ロード
		if (FAILED(pToonTex->Load("Assets/Toon/toon.png")))
		{
			//エラー処理
			//エラー原因の　メッセージボックスを表示
			MessageBox(nullptr, "トーンシェーダー用テクスチャ初期化の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す

		};
		//Release()による解放も忘れずに

			//環境マップ用のテクスチャのロード
			//テクスチャは、環境マップ用のキューブテクスチャなので、専用のロードを呼ぶ
		pCubeTex = new Texture;
		//ロード
		if (FAILED(pCubeTex->LoadCube("Assets/CubeMap/Cube.dds")))
		{
			//エラー処理
			//エラー原因の　メッセージボックスを表示
			MessageBox(nullptr, "キューブ用テクスチャ初期化の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す

		};
		//Release()による解放も忘れずに


		//描画先２の作成
		if (FAILED(CreateRenderTarget2()))
		{
			MessageBox(nullptr, "描画先２の作成失敗", "エラー", MB_OK);
			return E_FAIL;
		};






		return S_OK;
	}
#endif


	//Direct3D初期化
	//Device・DeviceContext　、　SwapChainを別々作成
		//Direct2D対応
	HRESULT Direct3D::InitDirect3DAndSupprtsDirect2D(int winW, int winH, HWND hWnd)
	{
		//Direct3Dと、Direct2Dの連携を前提とした
//Direct3DのDevice、DeviceContext,SwapChain1を作成
	//Direct2Dにおけるテキスト描画を参考にしたサイト
	//	//http://dioltista.blogspot.com/2019/04/c-directx11-direct2d-png.html

	//スマートポインタを使わずに、DirectXの型のインスタンスを取得して、Direct2Dを作成する方法
	//	//https://docs.microsoft.com/ja-jp/archive/msdn-magazine/2013/may/windows-with-c-introducing-direct2d-1-1
		HRESULT hr = S_OK;


		UINT createDeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;//DirectX11上でDirect2Dを使用するために必要	//この値をCreateDeviceの第4引数に入れなければ、　Direct2Dの描画、　レンダーターゲットビューを取得できない//逆に言えば、　これを第4引数に入れるだけで、　Direct2Dによって、　書き込まれる準備が整う
#ifdef _DEBUG
		//createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;	//デバック用フラグ
			//D3D型などのDirectXにおける　ポインタにおいて発生するDirectX側のエラー（例えば、処理終了後に、解放し忘れがある。など？）のエラーを返してくれる。
			//それらのエラーは、大抵、実行においては問題のないエラーなどで、　問題なく動くが、裏ではエラーをはいている　などというときに、ビルドの出力欄に、エラーを出力してくれる
#endif

			//ウィンドウスクリーンのサイズを設定
		scrWidth = winW;
		scrHeight = winH;

		hr = D3D11CreateDevice(
			nullptr,	// どのビデオアダプタを使用するか？既定ならばnullptrで
						//グラフィックボードどれを使うか→nullptrでデフォルト

			D3D_DRIVER_TYPE_HARDWARE,	// ドライバのタイプを渡す。ふつうはHARDWARE
												//ハードウェアに直接指定するが、→対応していないハードウェアにやろうとすると→、対応してなかったら、ソフトウェアにする
												//→ハードウェアで対応しなかったあ、ソフトウェアにすることができるが、→それだと遅い（なので、）

			nullptr,					// 上記をD3D_DRIVER_TYPE_SOFTWAREに設定しないかぎりnullptr
			createDeviceFlags,			//フラグ指定、（DEBUG, Direct2D仕様のためのサポート）
			nullptr,	// デバイス、コンテキストのレベルを設定。nullptrにしとけばOK
			0,			// 上の引数でレベルを何個指定したか
			D3D11_SDK_VERSION,			// SDKのバージョン。Direct３D11
			&pDevice,					// 無事完成したDeviceアドレスが返ってくる
			nullptr,					// level
			&pContext					// 無事完成したDeviceContextアドレスが返ってくる
		);
		//エラーチェック
		if (FAILED(hr))
		{
			//エラーのメッセージボックスを返す（Windows機能）
			MessageBox(nullptr, "デバイス、コンテキスト、スワップチェイン作成失敗", "エラー", MB_OK);
			//上記のHRESULT型の戻り値が、失敗（E_~）を返したなら
			return E_FAIL;		//呼び出し元の関数へエラーを送る
		}

		//DXGIのデバイスの作成
		//QueryInterface　＝　DirectXのスマートポインタであるComPtrの代用で、
			//dxgiDeviceにアドレスを取得させる
			//見えないところで、実態を持っているIDXGIDeviceのアドレスをもらう。
			//実体は一つで、そのアドレスを取得して、　取得により、見えないところにある、カウントが１アップされる（このカウントは、DirectX型のアドレスが受け渡されたら、カウントが１UPする。）

			//このカウントがあることによって、　カウントが０でない限りは、実態を持ち続けて、カウントが0になったら、見えないところで確保されている実体を解放される
			//自動でカウントされるが、　自動でカウントダウンはされない（ComPtrを使っていない限り、。）
				//なので、カウントダウンはこちらから指定して行う。　→　それがRelease（）である。
		hr = pDevice->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&pGIDevice));
		//エラーチェック
		if (FAILED(hr))
		{
			MessageBox(nullptr, "IDXGIDeviceの作成失敗", "エラー", MB_OK);return E_FAIL;
		}


		//Adapter
		//Direct3D,Direct2Dの相互間をつなげるケーブルのようなもの
		IDXGIAdapter* adapter = nullptr;
		//Deviceから、連携のためのアダプターを取得する
			//このスコープで宣言したポインタのアドレス格納部分に、アドレスを入れてもらいたいので、（代入するところは、このスコープのポインタアドレスに直接書き込ませたい）
			//そのため、　ポインタ（adapter）のアドレスを　参照渡しする。　すると、受け取り側は、ポインタのアドレス＝　ポインタのポインタで受け取るようになる
		hr = pGIDevice->GetAdapter(&adapter);
		//エラーチェック
		if (FAILED(hr))
		{
			MessageBox(nullptr, "IDXGIDeviceからAdapter（アダプター）の取得失敗", "エラー", MB_OK); 
			SAFE_RELEASE(adapter); return E_FAIL;
		}


		//Factory2の取得
		//factory = 工場
		//Direct2Dのリソース作成に用いるファクトリーオブジェクト（リソースを作る工場）
		IDXGIFactory2* dxgiFactory = nullptr;
		//アダプタに連携されているファクトリーをゲット
		hr = adapter->GetParent(__uuidof(IDXGIFactory2), reinterpret_cast<void**>(&dxgiFactory));
		//解放
			//参照カウンタのカウントダウン
		SAFE_RELEASE(adapter);

		//エラーチェック
		if (FAILED(hr))
		{
			MessageBox(nullptr, "Adapterからファクトリーオブジェクトの取得失敗", "エラー", MB_OK);
			return E_FAIL;
		}


		//スワップチェインに与える情報
		//ここにおける情報は、SwapChainにて使用していた構造体ではなく、
			//SwapChain1専用の、同様のバージョンの構造体を使用する
		DXGI_SWAP_CHAIN_DESC1 props = {};
		props.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		props.SampleDesc.Count = 1;
		props.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		props.BufferCount = 1;

		//スワップチェインの作成
		hr = dxgiFactory->CreateSwapChainForHwnd(
			pDevice,	//Direct3Dのデバイス
			hWnd,		//ウィンドウハンドル
			&props,		//スワップチェインの情報
			nullptr,
			nullptr,
			&pSwapChain1// 無事完成したSwapChainのアドレスが返ってくる
		);
		//解放
		SAFE_RELEASE(dxgiFactory);
		if (FAILED(hr))
		{
			MessageBox(nullptr, "SwapChain1の作成失敗", "エラー", MB_OK);
			SAFE_RELEASE(adapter); return E_FAIL;
		}

		/*2DDevice作成の準備**********************************************************************/
		//Direct2DのFactory（リソース作成工場）の作成
		Direct2D::CreateFactory();
		//Direct2DのDevice（全体監督）を作成
		Direct2D::CreateDevice();
		//Direct2DのDeviceContext（絵を描く人）を作成
		Direct2D::CreateDeviceContext();

		//デバイスコンテキストと
		//スワップチェーンの接続
		pSwapChain1->GetBuffer(0, __uuidof(pGISurface),
			reinterpret_cast<void **>(&pGISurface));





		///////////////////////////レンダーターゲットビュー作成///////////////////////////////
		//スワップチェーンからバックバッファを取得（バックバッファ ＝ レンダーターゲット）
		ID3D11Texture2D* pBackBuffer;

		if (FAILED(pSwapChain1->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
		{
			//エラーのメッセージボックスを返す（Windows機能）
			MessageBox(nullptr, "スワップチェインのバッファーの取得失敗", "エラー", MB_OK);
			//上記のHRESULT型の戻り値が、失敗（E_~）を返したなら
			return E_FAIL;		//呼び出し元の関数へエラーを送る

		}
		//pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

		//仮の紙を用意して、　対応するレンダーターゲットを設定して・・・
		//レンダーターゲットビューを作成
		if (FAILED(pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView)))
		{
			//エラーのメッセージボックスを返す（Windows機能）
			MessageBox(nullptr, "レンダーターゲットビュー作成失敗", "エラー", MB_OK);
			//上記のHRESULT型の戻り値が、失敗（E_~）を返したなら
			return E_FAIL;		//呼び出し元の関数へエラーを送る
		}
		//pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView);

		//一時的にバックバッファを取得しただけなので解放
		pBackBuffer->Release();


		///////////////////////////ビューポート（描画範囲）設定///////////////////////////////
		//遠くのもの小さく見えて、近くのもの大きく見える
		//視野で、見える範囲が、広がっていく
			//→それを広がっているものを、視点から、前に長方形に切り取ったものとしてみさせる
				//→なので、遠くのものは小さく見える

		//描画する範囲のため、
		//複数画面を表示するときは、
		//複数個用意する
			//表示画面ごと


		//レンダリング結果を表示する範囲
		{
			D3D11_VIEWPORT vp;
			vp.Width = (float)winW;	//幅
			vp.Height = (float)winH;//高さ
			vp.MinDepth = 0.0f;	//手前
			vp.MaxDepth = 1.0f;	//奥
								//長方形に変形したものを、１ｍの範囲にギュッとつぶした

			vp.TopLeftX = 0;	//左
			vp.TopLeftY = 0;	//上
		}
		//BeginDraw１にて使用するビューポートサイズ
		{

			//レンダリング結果を表示する範囲
			//vpSmall.Width = (float)winW / 10;	//幅
			//vpSmall.Height = (float)winH / 10;//高さ

			vpSmall.Width = (float)winW;	//幅
			vpSmall.Height = (float)winH;//高さ
			vpSmall.MinDepth = 0.0f;	//手前
			vpSmall.MaxDepth = 1.0f;	//奥
								//長方形に変形したものを、１ｍの範囲にギュッとつぶした

			vpSmall.TopLeftX = 0;	//左
			vpSmall.TopLeftY = 0;	//上

		}
		//拡大表示
		//Spriteとして表示するテクスチャのところの、サイズを10分の１にして
		//テクスチャ

		//1ドットが１０＊１０で表示された
			//モザイクのように見える

			//テクスチャのFilterが、POINTにすると、　ボケる。
			//だが、　LINEARにすると、ちかちかするような表現になる。


		//深度ステンシルビューの作成(Zバッファ（前のものは前、後ろのものは後ろに描画）)
		D3D11_TEXTURE2D_DESC descDepth;
		descDepth.Width = winW;
		descDepth.Height = winH;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_D32_FLOAT;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		pDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
		pDevice->CreateDepthStencilView(pDepthStencil, NULL, &pDepthStencilView);



		//データを画面に描画するための一通りの設定（パイプライン）
		pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);  // データの入力種類を指定
		pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);            // 描画先を設定
																//第３引数に深度ステンシルを入れて、Zバッファ（いんめん消去）を渡す
		pContext->RSSetViewports(1, &vpBig);



		//シェーダーの準備
		if (FAILED(InitShader()))
		{
			//エラー処理
			//エラー原因の　メッセージボックスを表示
			MessageBox(nullptr, "シェーダの初期化の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
		}
		//InitShader();
		//エラーが帰ってくるとする→すると、このInitShaderを呼び出している関数自体は、
			//→一番上の関数ではない→Direct3DのInitializeを読んでいる関数がいるはず→なので、　その呼び出し元に、またエラーと送る
			//呼び出し元でエラー処理してもらう（ここでは、とにかくエラーですと送る）

		//ブレンドの初期化
			//色の混ぜ方
		if (FAILED(InitBlend()))
		{
			MessageBox(nullptr, "ブレンドクラスの初期化失敗", "エラー", MB_OK);
			return E_FAIL;
		};

		//カメラの初期化
		Camera::Initialize();


		//カメラに位置、焦点を渡してやって、
		//それで、行列を作ってもらう
		//カメラの位置を送る
		//Camera::SetPosition(XMVectorSet(1, 0, 0, 0));	//カメラの位置をセット
		//この方式と、もう一つ、ｘ、ｙ、ｚだけを送り、それを受け取ってもらう関数も欲しい




		//トゥーンシェーディング用のテクスチャのロード
		pToonTex = new Texture;
		//ロード
		if (FAILED(pToonTex->Load("Assets/Toon/toon.png")))
		{
			//エラー処理
			//エラー原因の　メッセージボックスを表示
			MessageBox(nullptr, "トーンシェーダー用テクスチャ初期化の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す

		};
		//Release()による解放も忘れずに

			//環境マップ用のテクスチャのロード
			//テクスチャは、環境マップ用のキューブテクスチャなので、専用のロードを呼ぶ
		pCubeTex = new Texture;
		//ロード
		if (FAILED(pCubeTex->LoadCube("Assets/CubeMap/Cube.dds")))
		{
			//エラー処理
			//エラー原因の　メッセージボックスを表示
			MessageBox(nullptr, "キューブ用テクスチャ初期化の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す

		};
		//Release()による解放も忘れずに


		//描画先２の作成
		if (FAILED(CreateRenderTarget2()))
		{
			MessageBox(nullptr, "描画先２の作成失敗", "エラー", MB_OK);
			return E_FAIL;
		};






		return S_OK;

	}


//同じ名前で、使ってしまっているが、
//同じ名前で、ネームスペースをとれば、すでに存在するネームスペースに統合されるので、問題ない


//クラスとの違い
//→変数を宣言したときに、初期化
//コンストラクタ、デストラクタが存在しない


//いかに、namespaceの、関数の中身を書く→これらを見ると、クラスと変わらない
//Main.cppに書いていた、DirectX関係の処理をこの中にまとめる

//初期化
//★Main.cppによって、初期化でしていたもの、〜の設定して、（最初にしなくてはいけないもの）
HRESULT Direct3D::Initialize(int winW, int winH, HWND hWnd)

{
	//Direct3D初期化
	//Direct2D非対応
	//InitDirect3D(winW, winH, hWnd);

	
	//Direct3D初期化
	//Direct2D対応
	return InitDirect3DAndSupprtsDirect2D(winW, winH, hWnd);




	
}
BaseShader * Direct3D::GetShaderClass(SHADER_TYPE type)
{
	return shaderBundle_[type];
}

BaseBlend * Direct3D::GetBlendClass(BLEND_TYPE type)
{
	return blendBundle_[type];
}

HRESULT Direct3D::CreateRenderTarget2()
{



	//レンダリングするためのテクスチャの初期化(描画先を新たに用意)
	//書き込み先の新しい画用紙を用意する
	//テクスチャcppにあった初期化をそのまま使用する
	D3D11_TEXTURE2D_DESC texdec;	//DESC = 説明
	ZeroMemory(&texdec, sizeof(texdec));
	texdec.Width = scrWidth;	//画像の横幅
		//仮に、10分の１にすると、
		//サイズが、10分の１の位置だけ、描画範囲になるので、画面が小さくなるのではなくて、　描画位置が小さくなる。
		//描画する範囲→ビューポートが、描画する範囲になる。　なので、その範囲を複数作ってやれば、　描画先を決めることができる。

	texdec.Height = scrHeight;	//画像の縦幅
	texdec.MipLevels = 1;	//Mip　奥に行けば小さくなる→テクスチャ、ポリゴンを、近くにいるときは、高解像度のテクスチャを用意して、それを距離ごとに入れていけば、処理を見た目的によく見せて、軽く（遠くに同じきれいな、テクスチャを使ってはもったいない）
	texdec.ArraySize = 1;
	texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texdec.SampleDesc.Count = 1;
	texdec.SampleDesc.Quality = 0;
	texdec.Usage = D3D11_USAGE_DEFAULT;	//ここはDefault
		//リソースの使用方法を示す
		//DEFAULT＝　GPUからの読み取り、書き取りのみ可能
		//DYNAMIC＝　GPU,CPUのどちらからも読み取りをおこなえるようにする


	//このテクスチャを何に使用するのか
		//デフォルト＋　レンダーターゲットとして使用するとして（D3D11_BIND_RENDER_TARGET = プログラム側から絵を書き込めるようにする）
	texdec.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	texdec.CPUAccessFlags = 0;	//CPUへのアクセスを選択する。
								//WRITE＝書き込み
								//READ＝読み込み
								//０＝CPUアクセスが不要
		//Usageにて選択したリソースの管理方法によっても、どのように管理するのかを指定することができる
			//Usageにて、DEFAULT＝GPUのみ良い。CPUからの書き込みを制限した。
			//＝そのため、ここでは、CPUの書き込む余地をなくすため、０が良いとされると考える。

		//ココをWRITEにしていると、自宅のPCでは動かない。
		//しかし、動くPCも存在する
			//自宅のPCでは０にすることで動かす（テクスチャを作ることが可能）
	texdec.MiscFlags = 0;
	//texdec.MipLevels = 1;
	if (FAILED(pDevice->CreateTexture2D(&texdec, NULL, &pRenderTexture)))
	{
		//エラー処理
		MessageBox(nullptr, "描画先のテクスチャ作成失敗", "エラー", MB_OK);
		return E_FAIL;

	};
	//テクスチャを作成することができない
	//原因を調べる
		//CreateDeviceAndSwapChainにて、　デバックログ出力のフラグを入れて検証
		//→結果、　作成時に例外が投げられ、　エラーとして_com_errorとなっていた、なので、　COMの使用宣言がされていない段階なので、エラーが出ているのでは？と考える。
	//なので、一度、テクスチャ作成前に、COMの宣言を行う

	//→結果、治らず
//https://social.msdn.microsoft.com/Forums/SECURITY/ja-JP/70031db2-19d9-424d-b18c-462070e8cfce/directx1112288300641239412427124871249612452124733829112398124861?forum=vcgeneralja

	//このエラーが、
	//自宅のPC時にのみ、起こってしまう
	//こうゆうものを避けなければ、
		//提出先の企業でのPCで動かないという事態が起こってしまう可能性がある。






//レンダーターゲットを創ったら、
//もう一つ専用のレンダーターゲットビューを作る必要がある。（橋渡し＝　書き込みのプリンターのようなもの）

//レンダーターゲットを作る
//レンダーターゲットビューの設定を行う
	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	ZeroMemory(&renderTargetViewDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
	renderTargetViewDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;
	//レンダーターゲットの作成
	if (FAILED(Direct3D::pDevice->CreateRenderTargetView(
		pRenderTexture, &renderTargetViewDesc, &pRenderTargetView2)))
	{
		//エラー処理
		MessageBox(nullptr, "レンダーターゲットビューの作成失敗失敗", "エラー", MB_OK);
		return E_FAIL;

	};

	//書き込み先
	pScreen = new Sprite;
	if (FAILED(pScreen->Initialize(pRenderTexture, SHADER_2D)))
	{
		//エラー処理
		MessageBox(nullptr, "Spriteクラスの作成失敗失敗", "エラー", MB_OK);
		return E_FAIL;
	};


	return S_OK;

}


HRESULT Direct3D::InitBlend()
{

	//ブレンドクラス（継承先）のインスタンスの生成
	/*ブレンド情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
	//継承元のBaseBlend型にてキャスト

	//生成クラスの作成
	BlendFactory* pBlendFactory = new BlendFactory;


	//関数先にて、以下の処理を行う
	/*blendBundle_[BLEND_ALPHA ~] = new AlphaBlendForAlphaExpression;*/

	//それぞれのブレンドクラスの初期化を呼び出す
	for (int i = 0; i < BLEND_MAX; i++)
	{
		//ブレンドクラスの生成
			//関数先にて、引数にて渡した番号から、生成するクラスを識別して、クラスを生成、そのインスタンスを返してもらう
		blendBundle_[i] = pBlendFactory->CreateBlendClass((BLEND_TYPE)i);
		//初期化の呼び込み
		if (FAILED(blendBundle_[i]->Initialize()))
		{
			MessageBox(nullptr, "ブレンド（継承先）クラスの初期化失敗", "エラー", MB_OK); SAFE_DELETE(pBlendFactory); return E_FAIL;
		};
	}

	//解放
	SAFE_DELETE(pBlendFactory);


	//初期ブレンドをセット
	SetBlendBundle(BLEND_ALPHA_FOR_ALPHA);

	return S_OK;

	/*ブレンド情報をDirect3Dに持たせるパターン***********************************************************************************************************************************/
	/*//ブレンドステート
	//モデルの透過率の変化
	//通常のブレンド
		//通常透過
		//α値変更で、
			//後にあるピクセルを透過、（混ぜた色に）する
		//★画像のpng透過もこれで行うことが可能
	InitDefaultAlphaBlend();


	//減算
		//重量魔法、闇魔法に近い
	InitSubtractBlend();
	*/


}
void Direct3D::SetBlendBundle(BLEND_TYPE blendType)
{
	blendBundle_[blendType]->SetBlend();

	/*float blendFactor[4] = { D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO };
	pContext->OMSetBlendState(blendBundle[blendType].pBlendState , blendFactor, 0xffffffff);
*/
}



//シェーダー準備
//シェーダー３D,２D　どちらにも対応できるように
	//ラスタライザなど、が、２Dにおいても、特に設定はひつようないが、　
	//同じものでも、２D用３D用を作っておく（構造体）
HRESULT Direct3D::InitShader()

{
	//シェーダークラス（継承先）のインスタンスの生成
	/*シェーダー情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
	//継承元のBaseShader型にてキャスト

	//生成クラスの作成
	ShaderFactory* pShaderFactory = new ShaderFactory;


	//関数先にて、以下の処理を行う
	/*shaderBundle_[SHADER_CHOCO_FASHION] = new ChocolateFashionShader;
	shaderBundle_[SHADER_ENVI] = new EnvironmentShader;
	shaderBundle_[SHADER_FOG] = new FogShader;
	shaderBundle_[SHADER_NORMAL] = new NormalShader;
	shaderBundle_[SHADER_POINT_LIGHT_PLUS_WORLD_LIGHT] = new PointLightPlusWorldLightShader;
	shaderBundle_[SHADER_POINT_LIGHT] = new PointLightShader;
	shaderBundle_[SHADER_3D] = new Simple3DShader;
	shaderBundle_[SHADER_2D] = new Simple2DShader;
	shaderBundle_[SHADER_TEST] = new TestShader;
	shaderBundle_[SHADER_TOON] = new ToonShader;
	shaderBundle_[SHADER_WATER] = new WaterShader;
	shaderBundle_[SHADER_WETBALL] = new WetBallShader;*/


	//それぞれのシェーダーの初期化を呼び出す
	for (int i = 0; i < SHADER_MAX; i++)
	{
		//シェーダークラスの生成
			//関数先にて、引数にて渡した番号から、生成するクラスを識別して、クラスを生成、そのインスタンスを返してもらう
		shaderBundle_[i] = pShaderFactory->CreateShaderClass((SHADER_TYPE)i);
		//初期化の呼び込み
		if (FAILED(shaderBundle_[i]->Initialize()))
		{
			MessageBox(nullptr, "シェーダー（継承先）クラスの初期化失敗", "エラー", MB_OK); SAFE_DELETE(pShaderFactory); return E_FAIL;
		};
	}

	//解放
	SAFE_DELETE(pShaderFactory);


	/*シェーダー情報をDirect3Dに持たせるパターン***********************************************************************************************************************************/
	/*
	if (FAILED(InitShader3D())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL;};
	if (FAILED(InitShader2D())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;
	if (FAILED(InitShaderTest())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;

	if (FAILED(InitShaderToon())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;

	if (FAILED(InitShaderNormal())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;



	if (FAILED(InitShaderWater())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;

	if (FAILED(InitShaderEnvi())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;


	if (FAILED(InitShaderWetBall())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;

	if (FAILED(InitShaderChocolateFashion())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;

	if (FAILED(InitShaderFog())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;

	if (FAILED(InitShaderPointLight())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;

	if (FAILED(InitShaderPointLightPlusWorldLight())) { MessageBox(nullptr, "シェーダファイルのコンパイルの失敗", "エラー", MB_OK); return E_FAIL; };;

	//それぞれをデバイスコンテキストにセット
	//こうゆう、シェーダにしたよと送る
	//デバイスコンテキスト→書き込む人（バックバッファに）
	*/






	//使用するシェーダーを選択している
	//★シェーダを、２Dなら、２D、３Dなら、３Dを選択したい
	//シェーダは、あらかじめコンパイルしたい、しておきたい
		//その都度、以下の送るシェーダーのポインタで、使うものを２D,3Dで送ればいい
	//シェーダーを与えるとき、
		//どっちを使うかを、わからないので、　
		//→２Dを使うか、３Dを使うかを選べるように、２つの関数化にする
	//どちらを使うのかを、判断するために、
		//SHADER_3Dのところに、3Dか、２D化、を判断できる値を入れるようにしたい
	//SHADER_TYPE type;	//Typeという、SHADER_TYPEという、enumの値（unsigend int）の値を取得
						//値は、unsiged intだが、　enumの値であれば、何を表すunsiged intなのかが、わかりやすくする

	//３D,2Dを、決めるために、どちらを扱うかを送り、
		//使用するシェーダーを、コンテキストのポインタにセット
	SetShaderBundle(SHADER_3D);



	//エラー処理なし
	return S_OK;

}
//シェーダーポインタに、３D,２D　シェーダーを選んでセット
void Direct3D::SetShaderBundle(SHADER_TYPE type)
{
	/*シェーダー情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
	//クラスに持つ、
	//コンテキストに対して、情報を持たせる処理を行う関数　を呼びこみ、
		//それぞれのクラスのシェーダー情報を適用させる
	shaderBundle_[type]->SetShader();



	/*シェーダー情報をDirect3Dに持たせるパターン***********************************************************************************************************************************/
	/*
	pContext->VSSetShader(shaderBundle[type].pVertexShader, NULL, 0);	//頂点シェーダー
	pContext->PSSetShader(shaderBundle[type].pPixelShader, NULL, 0);	//ピクセルシェーダー
	pContext->IASetInputLayout(shaderBundle[type].pVertexLayout);	//頂点インプットレイアウト
	pContext->RSSetState(shaderBundle[type].pRasterizerState);		//ラスタライザー
	*/

}


//HRESULT Direct3D::InitShaderKurosawaMode()
//{
//	// 頂点シェーダの作成（コンパイル）
//	//シェーダーは、インクルードとは違い、ファイル別にプログラムを用意している（シェーダー専用のファイル）、コンパイルさせないといけないので、そのコンパイルのためのプログラム
//	ID3DBlob *pCompileVS = nullptr;	//コンパイル
//
//
//	//ファイルコンパイル
//	if (FAILED(D3DCompileFromFile(L"Toon.hlsl", nullptr, nullptr, 
//		"VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dシェーダファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//ファイルをコンパイルする　ファイル名				名前	グラボのバージョン		コンパイルするポインタ
//	assert(pCompileVS != nullptr);
//
//
//	//pVertexShaderを作成（頂点シェーダー）
//	//シェーダーバンドルのSHADER_TESTに入れてね
//	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL,
//		&shaderBundle[SHADER_KUROSAWA_MODE].pVertexShader)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dコンパイルしたシェーダを代入失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//コンパイルしたもの（pCompileVS）を入れる（クリエイト）
//
//	//★★頂点インプットレイアウト(3D用（光のための法線）)★★
//		//→頂点シェーダーに渡す情報群をここで設定
//	//頂点の情報
//	//1頂点に、位置、法線、色　の情報を持たせる
//	//Release前に、
//	D3D11_INPUT_ELEMENT_DESC layout[] = {
//
//		//TestShader.hlslファイルの
//		//頂点
//		//第５引数：メモリに書き込む位置（１つ目は０）
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
//
//		//UV
//		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
//
//		//法線
//		//第５引数：メモリに書き込む位置（３つ目はXMVECTORの構造体サイズの２つ分先から書き始める）
//		{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線
//
//	};
//
//	//インプットレイアウトの、情報が確定
//	if (FAILED(pDevice->CreateInputLayout(layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(),
//		&shaderBundle[SHADER_KUROSAWA_MODE].pVertexLayout)))
//		//頂点インプットレイアウトの情報を２つにしたので、　上記の数も２にしなければエラーになる
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "頂点インプットレイアウトの情報を確定失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//
//
//
//	//VSを開放　コンパイル完了したので
//	pCompileVS->Release();
//
//
//
//
//	// ピクセルシェーダの作成（コンパイル）
//	ID3DBlob *pCompilePS = nullptr;
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
//	if (FAILED(D3DCompileFromFile(L"Toon.hlsl", nullptr, nullptr, 
//		"PS_KUROSAWA", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "シェーダーファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	assert(pCompilePS != nullptr);
//	pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL,
//		&shaderBundle[SHADER_KUROSAWA_MODE].pPixelShader);
//	pCompilePS->Release();
//
//
//
//
//	//ラスタライザ作成
//	//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
//	//→そのピクセルごと、ピクセル化みたいなもの
//	D3D11_RASTERIZER_DESC rdc = {};
//
//	rdc.CullMode = D3D11_CULL_BACK;
//	//かリングモード　の略
//	//★裏のポリゴンを表示しますか、表示しませんか
//	//BACK　=裏のポリゴンはいらないよとしている
//	//FRONT =表のポリゴンいらないよ
//	//NONE  =いらないポリゴンないよ
//
//
//	rdc.FillMode = D3D11_FILL_SOLID;
//	//ソリッド→中身も埋めて　塗りつぶす
//	//ワイヤーフレーム→枠だけ、
//
//	rdc.FrontCounterClockwise = FALSE;
//	//カウンター　反撃
//	//時計
//	//FRONT　正面
//
//	//時計回りが、普通表、
//	//反時計回りが表にしますか？という設定
//	//どっちの回転を表面とするか（三角形があった時、頂点を時計回りに見るか？ということ）
//
//	//MAYAで、三角化しないでFBXにすると
//	//→長方形を作って、三角化しないと、FBXにすると
//	//→MAYAが、勝手に三角化をする（四角形を２つの▲の作りに変換→三角化）→すると、　面を三角化したとき、勝手に、一方を時計回り、もう一方を反時計回りにする
//	//→すると、一方を表面、もう一方を裏面とする→すると、透明になったり
//
//	if (FAILED(pDevice->CreateRasterizerState(&rdc, 
//		&shaderBundle[SHADER_KUROSAWA_MODE].pRasterizerState)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "ラスタライザの作成失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	//pDevice->CreateRasterizerState(&rdc, &pRasterizerState);
//
//	return S_OK;
//
//}
//
//HRESULT Direct3D::InitShaderOutline()
//{
//	// 頂点シェーダの作成（コンパイル）
//	//シェーダーは、インクルードとは違い、ファイル別にプログラムを用意している（シェーダー専用のファイル）、コンパイルさせないといけないので、そのコンパイルのためのプログラム
//	ID3DBlob *pCompileVS = nullptr;	//コンパイル
//
//
//	//ファイルコンパイル
//		//第４引数：頂点シェーダーの関数名
//		//ここを好きな名前にすれば(シェーダーにある関数)、　シェーダーの、その頂点シェーダーを呼んでくれる
//		//そのシェーダーファイルの頂点シェーダーをこちら側から指定できる。
//			//なので、　頂点シェーダーのプロパティにて指定した関数名とは別物もを頂点シェーダーとして扱うことができる。
//				//ピクセルシェーダーも、こちらから指定できる。
//			
//	if (FAILED(D3DCompileFromFile(L"Toon.hlsl", nullptr, nullptr, 
//		"PS_Big", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dシェーダファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//ファイルをコンパイルする　ファイル名				名前	グラボのバージョン		コンパイルするポインタ
//	assert(pCompileVS != nullptr);
//
//
//	
//
//
//	//pVertexShaderを作成（頂点シェーダー）
//	//シェーダーバンドルのSHADER_TESTに入れてね
//	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL,
//		&shaderBundle[SHADER_OUTLINE].pVertexShader)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dコンパイルしたシェーダを代入失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//コンパイルしたもの（pCompileVS）を入れる（クリエイト）
//
//	//★★頂点インプットレイアウト(3D用（光のための法線）)★★
//		//→頂点シェーダーに渡す情報群をここで設定
//	//頂点の情報
//	//1頂点に、位置、法線、色　の情報を持たせる
//	//Release前に、
//	D3D11_INPUT_ELEMENT_DESC layout[] = {
//
//		//TestShader.hlslファイルの
//		//頂点
//		//第５引数：メモリに書き込む位置（１つ目は０）
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
//
//		
//		//頂点シェーダーとピクセルシェーダーをここで指定する。
//			//関数名を上記や、下記で指定して、そいつを頂点シェーダーとして指定する。
//
//			//なので、ここで指定する頂点インプットは、
//				//その指定した頂点シェーダーにて扱う、引数数。を指定する。
//
//
//
//
//		//法線
//		//第５引数：メモリに書き込む位置（３つ目はXMVECTORの構造体サイズの２つ分先から書き始める）
//		{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線
//
//	};
//
//	//インプットレイアウトの、情報が確定
//	if (FAILED(pDevice->CreateInputLayout(layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(),
//		&shaderBundle[SHADER_OUTLINE].pVertexLayout)))
//		//頂点インプットレイアウトの情報を２つにしたので、　上記の数も２にしなければエラーになる
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "頂点インプットレイアウトの情報を確定失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//
//
//
//	//VSを開放　コンパイル完了したので
//	pCompileVS->Release();
//
//
//
//
//	// ピクセルシェーダの作成（コンパイル）
//	ID3DBlob *pCompilePS = nullptr;
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
//	if (FAILED(D3DCompileFromFile(L"Toon.hlsl", nullptr, nullptr, 
//		"PS_Black", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "シェーダーファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	assert(pCompilePS != nullptr);
//	pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL,
//		&shaderBundle[SHADER_OUTLINE].pPixelShader);
//	pCompilePS->Release();
//
//
//
//
//	//ラスタライザ作成
//	//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
//	//→そのピクセルごと、ピクセル化みたいなもの
//	D3D11_RASTERIZER_DESC rdc = {};
//
//	rdc.CullMode = D3D11_CULL_FRONT;
//	//かリングモード　の略
//	//★裏のポリゴンを表示しますか、表示しませんか
//	//BACK　=裏のポリゴンはいらないよとしている
//	//FRONT =表のポリゴンいらないよ
//	//NONE  =いらないポリゴンないよ
//
//
//	rdc.FillMode = D3D11_FILL_SOLID;
//	//ソリッド→中身も埋めて　塗りつぶす
//	//ワイヤーフレーム→枠だけ、
//
//	rdc.FrontCounterClockwise = FALSE;
//	//カウンター　反撃
//	//時計
//	//FRONT　正面
//
//	//時計回りが、普通表、
//	
//	if (FAILED(pDevice->CreateRasterizerState(&rdc,
//		&shaderBundle[SHADER_OUTLINE].pRasterizerState)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "ラスタライザの作成失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	//pDevice->CreateRasterizerState(&rdc, &pRasterizerState);
//
//	return S_OK;
//}
//
//HRESULT Direct3D::InitShaderToon()
//{
//	// 頂点シェーダの作成（コンパイル）
//	//シェーダーは、インクルードとは違い、ファイル別にプログラムを用意している（シェーダー専用のファイル）、コンパイルさせないといけないので、そのコンパイルのためのプログラム
//	ID3DBlob *pCompileVS = nullptr;	//コンパイル
//
//
//	//ファイルコンパイル
//	if (FAILED(D3DCompileFromFile(L"Toon.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dシェーダファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//ファイルをコンパイルする　ファイル名				名前	グラボのバージョン		コンパイルするポインタ
//	assert(pCompileVS != nullptr);
//
//
//	//pVertexShaderを作成（頂点シェーダー）
//	//シェーダーバンドルのSHADER_TESTに入れてね
//	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL,
//		&shaderBundle[SHADER_TOON].pVertexShader)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dコンパイルしたシェーダを代入失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//コンパイルしたもの（pCompileVS）を入れる（クリエイト）
//
//	//★★頂点インプットレイアウト(3D用（光のための法線）)★★
//		//→頂点シェーダーに渡す情報群をここで設定
//	//頂点の情報
//	//1頂点に、位置、法線、色　の情報を持たせる
//	//Release前に、
//	D3D11_INPUT_ELEMENT_DESC layout[] = {
//
//		//TestShader.hlslファイルの
//		//頂点
//		//第５引数：メモリに書き込む位置（１つ目は０）
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
//
//		//UV
//		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
//
//		//法線
//		//第５引数：メモリに書き込む位置（３つ目はXMVECTORの構造体サイズの２つ分先から書き始める）
//		{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線
//
//	};
//
//	//インプットレイアウトの、情報が確定
//	if (FAILED(pDevice->CreateInputLayout(layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(),
//		&shaderBundle[SHADER_TOON].pVertexLayout)))
//		//頂点インプットレイアウトの情報を２つにしたので、　上記の数も２にしなければエラーになる
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "頂点インプットレイアウトの情報を確定失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//
//
//
//	//VSを開放　コンパイル完了したので
//	pCompileVS->Release();
//
//
//
//
//	// ピクセルシェーダの作成（コンパイル）
//	ID3DBlob *pCompilePS = nullptr;
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
//	if (FAILED(D3DCompileFromFile(L"Toon.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "シェーダーファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	assert(pCompilePS != nullptr);
//	pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL,
//		&shaderBundle[SHADER_TOON].pPixelShader);
//	pCompilePS->Release();
//
//
//
//
//	//ラスタライザ作成
//	//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
//	//→そのピクセルごと、ピクセル化みたいなもの
//	D3D11_RASTERIZER_DESC rdc = {};
//
//	rdc.CullMode = D3D11_CULL_BACK;
//	//かリングモード　の略
//	//★裏のポリゴンを表示しますか、表示しませんか
//	//BACK　=裏のポリゴンはいらないよとしている
//	//FRONT =表のポリゴンいらないよ
//	//NONE  =いらないポリゴンないよ
//
//
//	rdc.FillMode = D3D11_FILL_SOLID;
//	//ソリッド→中身も埋めて　塗りつぶす
//	//ワイヤーフレーム→枠だけ、
//	
//	rdc.FrontCounterClockwise = FALSE;
//	//カウンター　反撃
//	//時計
//	//FRONT　正面
//
//	//時計回りが、普通表、
//	//反時計回りが表にしますか？という設定
//	//どっちの回転を表面とするか（三角形があった時、頂点を時計回りに見るか？ということ）
//
//	//MAYAで、三角化しないでFBXにすると
//	//→長方形を作って、三角化しないと、FBXにすると
//	//→MAYAが、勝手に三角化をする（四角形を２つの▲の作りに変換→三角化）→すると、　面を三角化したとき、勝手に、一方を時計回り、もう一方を反時計回りにする
//	//→すると、一方を表面、もう一方を裏面とする→すると、透明になったり
//
//	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_TOON].pRasterizerState)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "ラスタライザの作成失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	//pDevice->CreateRasterizerState(&rdc, &pRasterizerState);
//
//	return S_OK;
//}
//
//
//
////３Dシェーダーの初期化
//HRESULT Direct3D::InitShader3D()
//{
//	// 頂点シェーダの作成（コンパイル）
//	//シェーダーは、インクルードとは違い、ファイル別にプログラムを用意している（シェーダー専用のファイル）、コンパイルさせないといけないので、そのコンパイルのためのプログラム
//
//	ID3DBlob *pCompileVS = nullptr;	//コンパイル
//
//
//	if (FAILED(D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dシェーダファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
//	//ファイルをコンパイルする　ファイル名				名前	グラボのバージョン		コンパイルするポインタ
//	//グラボのバージョンは、特に難しいことをしないのであれば、小さい値でOK
//	//L"Simple~"	Lは？
//	//→メモにて
//	//
//	assert(pCompileVS != nullptr);
//	//上記のファイル読み込みも、HRESULTである
//	//→この時、HRESULTでも、エラーを返して、メッセージボックスが出て、
//	//あさーとでもエラーのボックスを出してくる
//
//	//アサーとは、デバック用と考えて、最終的にはなくしていく
//	//アサーとは、プログラマのミスで起こりえる、ファイル名チェックなどのものは、アサーと
//	//→プレイヤーが、ファイルを消してしまったなどの問題がある→それは、エラー処理で？
//	//エラー処理はif分なので、if文を入れれば入れるほど、重くなる
//	//だから、→すべてにエラー処理を入れればいいのかといえば、そうでもないとは思うが、
//	//→現段階では、マメなのかと思われるかも？→とにかく、HRESULT物は、エラー処理をする
//
//
//	//pVertexShaderを作成（頂点シェーダー）
//	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_3D].pVertexShader)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dコンパイルしたシェーダを代入失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &pVertexShader);
//	//コンパイルしたもの（pCompileVS）を入れる（クリエイト）
//
//	//★★頂点インプットレイアウト(3D用（光のための法線）)★★//２Dは法線いらない
//	//頂点の情報
//	//1頂点に、位置、法線、色　の情報を持たせる
//	//Release前に、
//	D3D11_INPUT_ELEMENT_DESC layout[] = {
//		//layoutを配列にしているのは、
//		//頂点に位置情報、法線などなどいろいろ情報を入れなくてはいけないので、
//		//法線などが増えるなら、この配列に増やしていく
//
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
//																								//RGBの領域にｘｙｚを入れる（フォーマットで位置を示す）
//	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
//																									  //テクスチャコーディネートの略
//																									  //UVはｘ、ｙの位置二つだけでよいので、RGの2つ
//																									  //先頭の情報、メモリの中に、頂点の位置、UVという情報が入っている　という頂点情報が連続している
//																									  //UVのサイズの位置はどこからとるの？→位置は、はじめからなので、　０から→UVは位置の次なので、位置のサイズ（XMVECTOR）の次の次からなので、XMVECTORサイズの後からサイズをとります　という意味
//																									  //これ以降にUVの下に書くのがあれば、サイズは、3つ目から〜
//																									  //頂点が、位置とUV座標の情報を持っている
//																									  //頂点に法線情報の追加
//	{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線
//
//	};
//
//	//要素数を取得したので、
//	//全体のサイズ /  要素の型（計算結果は、unsigned int）
//	//unsignedint size = sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC);
//	//（UINT）でキャスト
//
//	if (FAILED(pDevice->CreateInputLayout(layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_3D].pVertexLayout)))
//		//頂点インプットレイアウトの情報を２つにしたので、　上記の数も２にしなければエラーになる
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "頂点インプットレイアウトの情報を確定失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//pDevice->CreateInputLayout(layout, 1, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &pVertexLayout);
//	//インプットレイアウトの、情報が確定される
//	//ポジションだけなので１つ＝１										//ポインタを入れる
//
//
//	pCompileVS->Release();
//	//VSを開放　コンパイル完了したので
//
//
//
//	// ピクセルシェーダの作成（コンパイル）
//	ID3DBlob *pCompilePS = nullptr;
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
//	if (FAILED(D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "シェーダーファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	assert(pCompilePS != nullptr);
//	pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_3D].pPixelShader);
//	pCompilePS->Release();
//
//
//	//★２Dにおいて、らすたらいざ　
//	//ラスタライザ、頂点の置ける、その頂点を埋めるための、ものなので、塗りつぶすことができなくなる
//	//なので、ラスタライザは必要
//	//２Dにおいて、かリングモード→裏から見ることはないので、FRONTを消すようにしなければ、いい
//	//ラスタライザーに関しては、子リジョンの、ワイヤーフレームなどを使うとき以外で設定は必要ない
//
//
//	//ラスタライザ作成
//	//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
//	//→そのピクセルごと、ピクセル化みたいなもの
//	D3D11_RASTERIZER_DESC rdc = {};
//
//	rdc.CullMode = D3D11_CULL_BACK;
//	//かリングモード　の略
//	//★裏のポリゴンを表示しますか、表示しませんか
//	//→ポリゴンのいる、いらない
//	//→ポリゴンには裏があるはず、→さいころの１の面を見ているとき→６の面の裏面のポリゴンは見えないはず
//	//→なので、かリング＝　BACK　裏のポリゴンはいらないよとしている
//	//定義に移動
//	//FRONT(表いらない)BACK(裏いらない)NULL（いらないものなし）
//
//
//	//FRONTとすると、→表面が表示されないようになる
//	//→なので、中がくりぬかれたように見える（部屋の、正面の壁を追っ払ったような）
//	//半透明の場合は、両面表示するので、かリングにはどっちも入れない
//
//	rdc.FillMode = D3D11_FILL_SOLID;
//	//ソリッド→中身も埋めて　塗りつぶす
//	//ワイヤーフレーム→枠だけ、
//	//枠だけなので、三角形にて表現しているので、三角形のつながりとなる
//	//当たり判定表示、取り合えず表示されているのか知りたいときなど
//
//	rdc.FrontCounterClockwise = FALSE;
//	//カウンター　反撃
//	//時計
//	//FRONT　正面
//
//	//時計回りが、普通表、
//	//反時計回りが表にしますか？という設定
//	//どっちの回転を表面とするか（三角形があった時、頂点を時計回りに見るか？ということ）
//
//
//	//MAYAで、三角化しないでFBXにすると
//	//→長方形を作って、三角化しないと、FBXにすると
//	//→MAYAが、勝手に三角化をする（四角形を２つの▲の作りに変換→三角化）→すると、　面を三角化したとき、勝手に、一方を時計回り、もう一方を反時計回りにする
//	//→すると、一方を表面、もう一方を裏面とする→すると、透明になったり
//
//	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_3D].pRasterizerState)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "ラスタライザの作成失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	//pDevice->CreateRasterizerState(&rdc, &pRasterizerState);
//
//	return S_OK;
//}
////３Dシェーダーの初期化
//HRESULT Direct3D::InitShaderTest()
//{
//	// 頂点シェーダの作成（コンパイル）
//	//シェーダーは、インクルードとは違い、ファイル別にプログラムを用意している（シェーダー専用のファイル）、コンパイルさせないといけないので、そのコンパイルのためのプログラム
//
//	ID3DBlob *pCompileVS = nullptr;	//コンパイル
//
//
//	//
//	if (FAILED(D3DCompileFromFile(L"TestShader.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dシェーダファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
//	//ファイルをコンパイルする　ファイル名				名前	グラボのバージョン		コンパイルするポインタ
//	//グラボのバージョンは、特に難しいことをしないのであれば、小さい値でOK
//	//L"Simple~"	Lは？
//	//→メモにて
//	//
//	assert(pCompileVS != nullptr);
//
//
//	//pVertexShaderを作成（頂点シェーダー）
//	//シェーダーバンドルのSHADER_TESTに入れてね
//	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, 
//		&shaderBundle[SHADER_TEST].pVertexShader)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "３Dコンパイルしたシェーダを代入失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &pVertexShader);
//	//コンパイルしたもの（pCompileVS）を入れる（クリエイト）
//
//	//★★頂点インプットレイアウト(3D用（光のための法線）)★★
//		//→頂点シェーダーに渡す情報群をここで設定
//	//頂点の情報
//	//1頂点に、位置、法線、色　の情報を持たせる
//	//Release前に、
//	D3D11_INPUT_ELEMENT_DESC layout[] = {
//
//		//TestShader.hlslファイルの
//		//頂点
//		//第５引数：メモリに書き込む位置（１つ目は０）
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
//
//		//UV
//		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
//		
//		//法線
//		//第５引数：メモリに書き込む位置（３つ目はXMVECTORの構造体サイズの２つ分先から書き始める）
//		{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線
//
//	};
//
//	if (FAILED(pDevice->CreateInputLayout(layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), 
//		&shaderBundle[SHADER_TEST].pVertexLayout)))
//		//頂点インプットレイアウトの情報を２つにしたので、　上記の数も２にしなければエラーになる
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "頂点インプットレイアウトの情報を確定失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//pDevice->CreateInputLayout(layout, 1, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &pVertexLayout);
//	//インプットレイアウトの、情報が確定される
//	//ポジションだけなので１つ＝１										//ポインタを入れる
//
//
//	pCompileVS->Release();
//	//VSを開放　コンパイル完了したので
//
//
//
//	// ピクセルシェーダの作成（コンパイル）
//	ID3DBlob *pCompilePS = nullptr;
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
//	if (FAILED(D3DCompileFromFile(L"TestShader.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "シェーダーファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	assert(pCompilePS != nullptr);
//	pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, 
//		&shaderBundle[SHADER_TEST].pPixelShader);
//	pCompilePS->Release();
//
//
//	//★２Dにおいて、らすたらいざ　
//	//ラスタライザ、頂点の置ける、その頂点を埋めるための、ものなので、塗りつぶすことができなくなる
//	//なので、ラスタライザは必要
//	//２Dにおいて、かリングモード→裏から見ることはないので、FRONTを消すようにしなければ、いい
//	//ラスタライザーに関しては、子リジョンの、ワイヤーフレームなどを使うとき以外で設定は必要ない
//
//
//	//ラスタライザ作成
//	//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
//	//→そのピクセルごと、ピクセル化みたいなもの
//	D3D11_RASTERIZER_DESC rdc = {};
//
//	rdc.CullMode = D3D11_CULL_BACK;
//	//かリングモード　の略
//	//★裏のポリゴンを表示しますか、表示しませんか
//	//→ポリゴンのいる、いらない
//	//→ポリゴンには裏があるはず、→さいころの１の面を見ているとき→６の面の裏面のポリゴンは見えないはず
//	//→なので、かリング＝　BACK　裏のポリゴンはいらないよとしている
//	//定義に移動
//	//FRONT(表いらない)BACK(裏いらない)NULL（いらないものなし）
//
//
//	//FRONTとすると、→表面が表示されないようになる
//	//→なので、中がくりぬかれたように見える（部屋の、正面の壁を追っ払ったような）
//	//半透明の場合は、両面表示するので、かリングにはどっちも入れない
//
//	rdc.FillMode = D3D11_FILL_SOLID;
//	//ソリッド→中身も埋めて　塗りつぶす
//	//ワイヤーフレーム→枠だけ、
//	//枠だけなので、三角形にて表現しているので、三角形のつながりとなる
//	//当たり判定表示、取り合えず表示されているのか知りたいときなど
//
//	rdc.FrontCounterClockwise = FALSE;
//	//カウンター　反撃
//	//時計
//	//FRONT　正面
//
//	//時計回りが、普通表、
//	//反時計回りが表にしますか？という設定
//	//どっちの回転を表面とするか（三角形があった時、頂点を時計回りに見るか？ということ）
//
//
//	//MAYAで、三角化しないでFBXにすると
//	//→長方形を作って、三角化しないと、FBXにすると
//	//→MAYAが、勝手に三角化をする（四角形を２つの▲の作りに変換→三角化）→すると、　面を三角化したとき、勝手に、一方を時計回り、もう一方を反時計回りにする
//	//→すると、一方を表面、もう一方を裏面とする→すると、透明になったり
//
//	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_TEST].pRasterizerState)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "ラスタライザの作成失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	//pDevice->CreateRasterizerState(&rdc, &pRasterizerState);
//
//	return S_OK;
//}
//
//
//
//
//
////２Dシェーダーの初期化
////シェーダーを、２D用、シェーダーのポインタも２D用を使用する
//HRESULT Direct3D::InitShader2D()
//{
//	// 頂点シェーダの作成（コンパイル）
//	//シェーダーは、インクルードとは違い、ファイル別にプログラムを用意している（シェーダー専用のファイル）、コンパイルさせないといけないので、そのコンパイルのためのプログラム
//	ID3DBlob *pCompileVS = nullptr;	//コンパイル
//
//
//	if (FAILED(D3DCompileFromFile(L"Simple2D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "２Dシェーダファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
//	//ファイルをコンパイルする　ファイル名				名前	グラボのバージョン		コンパイルするポインタ
//	//グラボのバージョンは、特に難しいことをしないのであれば、小さい値でOK
//	//L"Simple~"	Lは？
//	//→メモにて
//	//
//	assert(pCompileVS != nullptr);
//	//上記のファイル読み込みも、HRESULTである
//	//→この時、HRESULTでも、エラーを返して、メッセージボックスが出て、
//	//あさーとでもエラーのボックスを出してくる
//
//	//アサーとは、デバック用と考えて、最終的にはなくしていく
//	//アサーとは、プログラマのミスで起こりえる、ファイル名チェックなどのものは、アサーと
//	//→プレイヤーが、ファイルを消してしまったなどの問題がある→それは、エラー処理で？
//	//エラー処理はif分なので、if文を入れれば入れるほど、重くなる
//	//だから、→すべてにエラー処理を入れればいいのかといえば、そうでもないとは思うが、
//	//→現段階では、マメなのかと思われるかも？→とにかく、HRESULT物は、エラー処理をする
//
//
//	//pVertexShaderを作成（頂点シェーダー）
//	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_2D].pVertexShader)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "コンパイルしたシェーダを代入失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &pVertexShader);
//	//コンパイルしたもの（pCompileVS）を入れる（クリエイト）
//
//	//★★頂点インプットレイアウト(3D用（光のための法線）)★★//２Dは法線いらない
//	//頂点の情報
//	//1頂点に、位置、法線、色　の情報を持たせる
//	//Release前に、
//	D3D11_INPUT_ELEMENT_DESC layout[] = {
//		//layoutを配列にしているのは、
//		//頂点に位置情報、法線などなどいろいろ情報を入れなくてはいけないので、
//		//法線などが増えるなら、この配列に増やしていく
//
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
//																								//RGBの領域にｘｙｚを入れる（フォーマットで位置を示す）
//	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
//																									  //テクスチャコーディネートの略
//																									  //UVはｘ、ｙの位置二つだけでよいので、RGの2つ
//																									  //先頭の情報、メモリの中に、頂点の位置、UVという情報が入っている　という頂点情報が連続している
//																									  //UVのサイズの位置はどこからとるの？→位置は、はじめからなので、　０から→UVは位置の次なので、位置のサイズ（XMVECTOR）の次の次からなので、XMVECTORサイズの後からサイズをとります　という意味
//																									  //これ以降にUVの下に書くのがあれば、サイズは、3つ目から〜
//																									  //頂点が、位置とUV座標の情報を持っている
//																									  //頂点に法線情報の追加
//	
//		//法線は２Dには必要なし（光関係ない）
//
//	};
//
//	//要素数を取得したので、
//	//全体のサイズ /  要素の型（計算結果は、unsigned int）
//	//unsignedint size = sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC);
//	//（UINT）でキャスト
//
//	if (FAILED(pDevice->CreateInputLayout(layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_2D].pVertexLayout)))
//		//頂点インプットレイアウトの情報を２つにしたので、　上記の数も２にしなければエラーになる
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "頂点インプットレイアウトの情報を確定失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//
//	}
//	//pDevice->CreateInputLayout(layout, 1, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &pVertexLayout);
//	//インプットレイアウトの、情報が確定される
//	//ポジションだけなので１つ＝１										//ポインタを入れる
//
//
//	pCompileVS->Release();
//	//VSを開放　コンパイル完了したので
//
//
//
//	// ピクセルシェーダの作成（コンパイル）
//	ID3DBlob *pCompilePS = nullptr;
//	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
//	if (FAILED(D3DCompileFromFile(L"Simple2D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "シェーダーファイルのコンパイル失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	assert(pCompilePS != nullptr);
//	pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_2D].pPixelShader);
//	pCompilePS->Release();
//
//
//	//★２Dにおいて、らすたらいざ　
//	//ラスタライザ、頂点の置ける、その頂点を埋めるための、ものなので、塗りつぶすことができなくなる
//	//なので、ラスタライザは必要
//	//２Dにおいて、かリングモード→裏から見ることはないので、FRONTを消すようにしなければ、いい
//	//ラスタライザーに関しては、子リジョンの、ワイヤーフレームなどを使うとき以外で設定は必要ない
//
//
//	//ラスタライザ作成
//	//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
//	//→そのピクセルごと、ピクセル化みたいなもの
//	D3D11_RASTERIZER_DESC rdc = {};
//
//	rdc.CullMode = D3D11_CULL_BACK;
//	//かリングモード　の略
//	//★裏のポリゴンを表示しますか、表示しませんか
//	//→ポリゴンのいる、いらない
//	//→ポリゴンには裏があるはず、→さいころの１の面を見ているとき→６の面の裏面のポリゴンは見えないはず
//	//→なので、かリング＝　BACK　裏のポリゴンはいらないよとしている
//	//定義に移動
//	//FRONT(表いらない)BACK(裏いらない)NULL（いらないものなし）
//
//
//	//FRONTとすると、→表面が表示されないようになる
//	//→なので、中がくりぬかれたように見える（部屋の、正面の壁を追っ払ったような）
//	//半透明の場合は、両面表示するので、かリングにはどっちも入れない
//
//	rdc.FillMode = D3D11_FILL_SOLID;
//	//ソリッド→中身も埋めて　塗りつぶす
//	//ワイヤーフレーム→枠だけ、
//	//枠だけなので、三角形にて表現しているので、三角形のつながりとなる
//	//当たり判定表示、取り合えず表示されているのか知りたいときなど
//
//	rdc.FrontCounterClockwise = FALSE;
//	//カウンター　反撃
//	//時計
//	//FRONT　正面
//
//	//時計回りが、普通表、
//	//反時計回りが表にしますか？という設定
//	//どっちの回転を表面とするか（三角形があった時、頂点を時計回りに見るか？ということ）
//
//
//	//MAYAで、三角化しないでFBXにすると
//	//→長方形を作って、三角化しないと、FBXにすると
//	//→MAYAが、勝手に三角化をする（四角形を２つの▲の作りに変換→三角化）→すると、　面を三角化したとき、勝手に、一方を時計回り、もう一方を反時計回りにする
//	//→すると、一方を表面、もう一方を裏面とする→すると、透明になったり
//
//	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_2D].pRasterizerState)))
//	{
//		//エラー処理
//		//エラー原因の　メッセージボックスを表示
//		MessageBox(nullptr, "ラスタライザの作成失敗", "エラー", MB_OK);
//		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
//	}
//	//pDevice->CreateRasterizerState(&rdc, &pRasterizerState);
//
//	return S_OK;
//}

void Direct3D::SetBackGroundColor(float r, float g, float b, float a)
{
	//引数にてもらった値を背景色を持つ配列に登録させる
	clearColor_[R] = r;
	clearColor_[G] = g;
	clearColor_[B] = b;
	clearColor_[A] = a;

}

void Direct3D::SetBackGroundColor(XMVECTOR& rgba)
{
	clearColor_[R] = rgba.vecX;
	clearColor_[G] = rgba.vecY;
	clearColor_[B] = rgba.vecZ;
	clearColor_[A] = rgba.vecW;

}

//描画開始
//描画のバックバッファの絵を用意
void  Direct3D::BeginDraw()

{
	//複数のレンダーターゲットへの書き込みを実現するために
	//マルチレンダーターゲット
	/*
	・レンダーターゲット、レンダーターゲットビューの用意
	・テクスチャクラスに、D3D11型のテクスチャ２Dから、サンプラーなどの情報をもらい、初期化するクラスの取得
	・Textureクラスを使って、Spriteクラスを作成、
	・Spriteクラスをスワップチェインで入れ替えている画像へ描画することで、新しく作成した、レンダーターゲットの描画が完了

	*/

	//描画範囲を決める
		//描画サイズを小さく
	pContext->RSSetViewports(1, &vpBig);
	pContext->RSSetViewports(1, &vpSmall);


	//ゲームの処理
			//DirectXは、OSを、飛ばすといったが、Windowで動いているので、
				//→Windowのメッセージを優先
				//m着ウィンドウを閉じろと言っているのに、ゲームが動いたら大変なので、
				//→ウィンドウがわのメッセージを優先

			//ウィンドウのメッセージがなかった時にゲームの描画

	//背景の色（色情報）
	//float clearColor[4] = { 0.0f, 0.5f, 0.0f, 1.0f };//R,G,B,A（A=透明）

	//茶色
	//float clearColor[4] = { 156.f / 256.f , 83.f / 256.f, 50.f / 256.f , 1.0f };//R,G,B,A（A=透明）
//	float clearColor[4] = { 0,0,0, 1.0f };//R,G,B,A（A=透明）


	//描画先を指定する
	//必要に応じて、
	//第二引数の、書き込むものを変化させ、書き込み先を指定する必要がある。
		//レンダーターゲットビューは、
			//書き込み先を、すでに決定している。
			//なので、この一行で、書き込み先を決定する。
	//引数：描画の橋渡しを行うレンダーターゲットビュー
	//引数：深度バッファ（Zバッファ）→テクスチャのサイズを変えるときは、深度バッファの設定も変える必要があるのだが、今回は使いまわす
	//pContext->OMSetRenderTargets(1, &pRenderTargetView2, pDepthStencilView);            // 描画先を設定
	pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);            // 描画先を設定
		//描画先を入れ替え、
		//現在書き込んでいるのは、　RenderTargetView2　
			//入れ替えを行っているのは、　RenderTargetView1　の書き込んだレンダーターゲット
			//何も書かれていないものを入れ替え続けても、描画はされない。
			//→書き込んだものを、どのようにかして、描画をする必要がある。

		//Spriteクラスに書き込んで、　
		//それを、　画面に貼り付けるようなやり方で再現。
		//貼り付けられたものを、スワップチェインで入れ替えればよい。

		//だが、画像ファイルをロードして、それをロードするような形にしている。
		//→もうすでにテクスチャをつくっているので、　画像のロードなどは必要ないはず。
			//→Sprite→Texture　　TextureのLoadを見ると、　テクスチャ部分は行っているが、　サンプラー部分はいまだに終わっていない。→作成したD3D型のテクスチャを




	//コンテキスト、　絵を描く人→バックバッファを塗りつぶす
	//画面をクリア
	//pContext->ClearRenderTargetView(pRenderTargetView2, clearColor_);
	pContext->ClearRenderTargetView(pRenderTargetView, clearColor_);


	//↓本来なら以下で、キャラの描画などを行って。。。



	//カメラのUpdate
	//毎フレーム呼ばれるようにしたい（カメラは、ゲームの中のことなので、Direct3Dに、入れる(CameraのInitializeも、ゲームに関することなので、)）
	//毎フレーム呼ばれているのは、Beginなので、
	//画面を切り替えた後に、カメラを更新すれば、移動の後を見ることができる
	Camera::Update();		//EndDrawで、入れ替えるので、その前にカメラを更新


	//深度バッファクリア
	//初期化
	//カメラの距離を登録させるZバッファの塗った配列の初期化
	//→９９９９でカメラからの距離を登録する配列初期化をする、クリアを行わなければいけない
	//	→だが、→９９９９というのは、この距離より前だったら、書き込みとするやつなので、→この値を、カメラの見える最大距離にしておけば、
	//	→それよりも遠いものは、描画しないといった、形で描画をすることが可能
	pContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);


}

//描画開始
//描画のバックバッファの絵を用意
//画面に持ってくるほうの描画
	//レンダーターゲットを複数用意して、
	//それを、画面に書き込んで、それをスワップチェインで交換して、描画を完了としたい。
void  Direct3D::BeginDraw2()

{
	pContext->RSSetViewports(1, &vpBig);


	//ゲームの処理
			//DirectXは、OSを、飛ばすといったが、Windowで動いているので、
				//→Windowのメッセージを優先
				//m着ウィンドウを閉じろと言っているのに、ゲームが動いたら大変なので、
				//→ウィンドウがわのメッセージを優先

			//ウィンドウのメッセージがなかった時にゲームの描画

	//背景の色（色情報）
	//float clearColor[4] = { 0.0f, 0.5f, 0.0f, 1.0f };//R,G,B,A（A=透明）

	//茶色
	float clearColor2[4] = { 156.f / 256.f , 83.f / 256.f, 50.f / 256.f , 1.0f };//R,G,B,A（A=透明）
	//float clearColor2[4] = { 0,0,0, 1.0f };//R,G,B,A（A=透明）


	//描画先を指定する
	//必要に応じて、
	//第二引数の、書き込むものを変化させ、書き込み先を指定する必要がある。
		//レンダーターゲットビューは、
			//書き込み先を、すでに決定している。
			//なので、この一行で、書き込み先を決定する。
	//引数：描画の橋渡しを行うレンダーターゲットビュー
	//引数：深度バッファ（Zバッファ）→テクスチャのサイズを変えるときは、深度バッファの設定も変える必要があるのだが、今回は使いまわす
	pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);            // 描画先を設定
		//描画先を入れ替え、
		//現在書き込んでいるのは、　RenderTargetView2　
			//入れ替えを行っているのは、　RenderTargetView1　の書き込んだレンダーターゲット
			//何も書かれていないものを入れ替え続けても、描画はされない。
			//→書き込んだものを、どのようにかして、描画をする必要がある。





	//コンテキスト、　絵を描く人→バックバッファを塗りつぶす
	//画面をクリア
	pContext->ClearRenderTargetView(pRenderTargetView, clearColor2);


	//↓本来なら以下で、キャラの描画などを行って。。。



	//カメラのUpdate
	//毎フレーム呼ばれるようにしたい（カメラは、ゲームの中のことなので、Direct3Dに、入れる(CameraのInitializeも、ゲームに関することなので、)）
	//毎フレーム呼ばれているのは、Beginなので、
	//画面を切り替えた後に、カメラを更新すれば、移動の後を見ることができる
	Camera::Update();		//EndDrawで、入れ替えるので、その前にカメラを更新


	//深度バッファクリア
	//初期化
	//カメラの距離を登録させるZバッファの塗った配列の初期化
	//→９９９９でカメラからの距離を登録する配列初期化をする、クリアを行わなければいけない
	//	→だが、→９９９９というのは、この距離より前だったら、書き込みとするやつなので、→この値を、カメラの見える最大距離にしておけば、
	//	→それよりも遠いものは、描画しないといった、形で描画をすることが可能
	pContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);


	//バックバッファをテクスチャとして取得し、
	//描画
	Transform trans;
	//trans.position_.vecX = -0.5f;
	//trans.position_.vecY = 0.5f;

	////レンダーターゲットを4枚使いまわせば、
	//	//むずかしくなく、描画することが可能。
	//	//カメラの位置を変えて、レンダーターゲットに情報を渡して、　描画して、、とやれば、可能

	//trans.scale_.vecX = 0.5f;
	//trans.scale_.vecY = 0.5f;


	//Direct３D内のInitializeの
	//テクスチャの生成の時に、
	//

	trans.Calclation();
	pScreen->Draw(trans);


}





//描画終了
//スワップチェインによって、バックバッファを入れ替える
//（描画されたものと、前回の画面を入れ替える→描画）
HRESULT Direct3D::EndDraw()

{
	//描画処理
	//ここで、キャラなどの描画

	//テスト
	//テクスチャのバッファーから、１ピクセルずつ、データを取得したい
	/*
	
	
	*/
	{
		//////コピー先のテクスチャのバッファーを準備
		////ID3D11Texture2D* tex2 = nullptr;


		////バッファーを取得する
		////ID3D11Texture2D* pBackBuffer;

		//ID3D11Buffer* pBackBuffer;

		//if (FAILED(pSwapChain1->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
		//{
		//	//エラーのメッセージボックスを返す（Windows機能）
		//	MessageBox(nullptr, "スワップチェインのバッファーの取得失敗", "エラー", MB_OK);
		//	//上記のHRESULT型の戻り値が、失敗（E_~）を返したなら
		//	return E_FAIL;		//呼び出し元の関数へエラーを送る

		//}



	
		////CPUからピクセルにアクセスするためのもの
		//	//MAP＝　テクスチャなどに、ピクセルから、アクセスするようにする
		//HRESULT hr;
		////Fbxの描画時に、コンスタントバッファにデータを与えるときにも用いた
		////受け渡しのために使う情報群
		//	//受け渡し方など？
		//D3D11_MAPPED_SUBRESOURCE mapd;
		////引数：バッファー
		////引数：バッファー番目
		////引数：マップタイプ（つまり、CPUで何をしますか？＝CPUで読み込み可能とするのが、MAPなので）
		////引数：マップフラグ
		////引数：受け渡しのために使う情報群
		//hr = pContext->Map(pBackBuffer, 0, D3D11_MAP_READ, 0, &mapd);
		////hr = pContext->Map(pBackBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapd);

		//if (FAILED(hr))
		//{
		//	return hr;
		//}



		//DWORD color;
		//LPDWORD pDst;
		//UINT pitch = mapd.RowPitch / sizeof(DWORD);

		//for (int v = 0; v < scrHeight; v++)
		//{
		//	pDst = (LPDWORD)mapd.pData + v * pitch;

		//	for (int u = 0; u < scrWidth; u++)
		//	{
		//		color = (DWORD)*pDst;


		//	}
		//
		//}
	

		//pContext->Unmap(pBackBuffer, 0);

		//SAFE_RELEASE(pBackBuffer);

	
	}



	//スワップチェインが、入れ替える
	//スワップ（バックバッファを表に表示する）
	if (FAILED(pSwapChain1->Present(0, 0)))
	{
		MessageBox(nullptr, "スワップチェイン失敗", "エラー", MB_OK);
		//Presentという処理をおこなって、　その処理が成功したら、HRESULTの型で、S_OK 失敗したら、E_~と帰ってくる
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
	
	}
	//pSwapChain->Present(0, 0);

	//→これでDirectXの広大な世界が、広がっている状態
	//これを１秒間のうちに、裏で描いて、入れ替えてと６０回繰り返している（FPS６０）


	return S_OK;	//問題なくHRESULTの型の関数が成功したとき→OKですと返す（OKなら、問題なく次に進める）
}

//Zバッファへの書き込みのON/OFF
	//奥行き設定のON,OFFの設定
void Direct3D::SetDepthBafferWriteEnable(bool isWrite)
{
	//ON
	if (isWrite)
	{
		//Zバッファ（デプスステンシルを指定する）
		pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);
	}

	//OFF
	else
	{
		pContext->OMSetRenderTargets(1, &pRenderTargetView, nullptr);
	}

}

//XMVECTOR&　を返すとして、
	//XMVECTOR color = XMVectorSet(clearColor_[R], clearColor_[G], clearColor_[B], clearColor_[A]);
	//return color;
//このやり方だと、スコープ外に行ったら、
//元の変数、配列は解放されてしまうので、
	//適切でない。
	//→スコープを外れたら解放されてしまうため、
XMVECTOR Direct3D::GetBackGroundColor()
{
	XMVECTOR color = XMVectorSet(clearColor_[R], clearColor_[G], clearColor_[B], clearColor_[A]);
	return color;
}

IDXGISwapChain1* Direct3D::GetSwapChain1()
{
	return pSwapChain1;
}

IDXGIDevice * Direct3D::GetIDXGIDevice()
{
	return pGIDevice;
}

IDXGISurface * Direct3D::GetIDXGISurface()
{
	return pGISurface;
}


//解放処理
void Direct3D::Release()

{
	//後に、確保したものを、先に開放
	pScreen->Release();
	SAFE_DELETE(pScreen);
	SAFE_RELEASE(pRenderTargetView2);
	//SAFE_RELEASE(pRenderTexture);	->ｐScreenに渡し、Textureにて渡されている、そして、すでに解放されているので、解放しない

	//ブレンドクラスの解放
	for (int i = 0; i < BLEND_MAX; i++)
	{
		/*ブレンド情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
		//インスタンスの解放
		SAFE_DELETE(blendBundle_[i]);
	}

	//シェーダークラスの解放
	//構造体配列ポインタの解放
	//０から
	//シェーダーの個数　enumの最後まで
	//１ずつ
	for (int i = 0; i < SHADER_MAX; i++)
	{
		/*シェーダー情報を独自のクラスに持たせるパターン***********************************************************************************************************************************/
		//インスタンスの解放
		SAFE_DELETE(shaderBundle_[i]);

		/*シェーダー情報をDirect3Dに持たせるパターン***********************************************************************************************************************************/
		//持たせたシェーダーの情報を解放
		//ID3D11の参照カウンタ方式のため、生成した、カウント分をReleaseでカウントダウン
		//SAFE_RELEASE(shaderBundle[i].pRasterizerState);
		//SAFE_RELEASE(shaderBundle[i].pVertexLayout);
		//SAFE_RELEASE(shaderBundle[i].pPixelShader);
		//SAFE_RELEASE(shaderBundle[i].pVertexShader);
	}

	//クラスのオブジェクトを動的生成するために
	//newの代わりに〜Deviceを使用した、→なので、Deleteするときも、専用の開放関数が存在する

	//解放処理　Relese()
	pToonTex->Release();	//デストラクタで、Release呼ぶようにすれば、ここで呼ぶこともない。
	SAFE_DELETE(pToonTex);
	pCubeTex->Release();
	SAFE_DELETE(pCubeTex);
	SAFE_RELEASE(pDepthStencilView);
	SAFE_RELEASE(pDepthStencil);
	SAFE_RELEASE(pRenderTargetView);
	SAFE_RELEASE(pGIDevice);
	SAFE_RELEASE(pGISurface);
	SAFE_RELEASE(pSwapChain1);
	SAFE_RELEASE(pContext);
	SAFE_RELEASE(pDevice);


	//作った順と逆に消していく→作るときは偉い人（監督）から

}