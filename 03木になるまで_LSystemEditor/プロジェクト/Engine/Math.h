#pragma once
//レイ発射のためのベクトルの計算を行う
//#include <DirectXMath.h>	//ベクトル、行列を使用するため
//using namespace DirectX

#include "Transform.h"	//ベクトル、行列を使用しているので、このヘッダをインクルードしてしまえば一発


namespace Math
{
	//行列式
	float Det(XMVECTOR a, XMVECTOR b, XMVECTOR c);

	//三角形と線分の交差判定
	//引数：
	bool Intersect(XMVECTOR origin, XMVECTOR ray, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2, float* t);
};

