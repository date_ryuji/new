#include "Camera.h"

#include "Direct3D.h"


//変数
namespace Camera
{
	//変数
	XMVECTOR position_;	//カメラの位置（視点）
	XMVECTOR target_;	//見る位置（焦点）
	XMMATRIX viewMatrix_;	//ビュー行列
	XMMATRIX projMatrix_;	//プロジェクション行列

	//画角角度
	float angle = XM_PIDIV4;

	//クラスの時は、ヘッダのメンバ変数の時は、＿をつけていた。
		//→クラスにもするかもしれないので、＿つけてもいいし、
		//→クラスと区別化をするために、＿つけないという方法もあり。

	//★名前の変更から、一気に対応の変数の名前を変えることが可能（置き換えだと、その名前をすべて変えてしまうので注意）
	//→この時、他のnamespaceでは、つけていないなど、ばらばらにしないようにする
}


//初期化
void Camera::Initialize()
{
	//カメラの初期位置を指定
	position_ = XMVectorSet(0, 3, -10, 0);	//カメラの位置
	target_ = XMVectorSet(0, 0, 0, 0);	//カメラの焦点

	//プロジェクション行列(初期値)
	projMatrix_ = XMMatrixPerspectiveFovLH(angle, (FLOAT)Direct3D::scrWidth / (FLOAT)Direct3D::scrHeight, 0.1f, 500.0f);
											//４５度	サイズに関しては、ウィンドウ側で決めているので、そこから持ってくるのが理想
											//１０ｃｍ手前から、　１００ｍまで(描画距離)
}

//プロジェクション行列は、変わらない→プロジェクトしょんは、ズームなどの時に使う
	//→視野の角度などを狭めて、ズームをするとき、

	//ウィンドウサイズ、→ゲームの場合は、固定なので、変わらないが、
	//ツールなどで、ウィンドウサイズが変わり、アスペクト比など考えなくてはいけないときは、→Updateに
//更新
void Camera::Update()
{
	//ビュー行列の作成
	viewMatrix_ = XMMatrixLookAtLH(position_, target_, XMVectorSet(0, 1, 0, 0));
									//どこから、（position）どこを見て（target）,カメラの上がどっちなのか


							//カメラの上は、カメラの本当の真上を示さなくても、ワールドの↑を指していれば問題は内
							//だが、カメラが、　真下を見たときに、真上を指していると問題が起こるので、　正確にとりたいときは、→カメラの本当の真上、（傾いているときのそのカメラの↑）

}

//ポジションを受け取るセッター
//ターゲットを受け取るセッター

//位置を設定
void Camera::SetPosition(const XMVECTOR& position)
{
	//Camera::position = position;	//引数のポジションをメンバ変数に受け取る
									//メンバ変数に＿がついていないと、ここで、どうするか苦労する
	position_ = position;
}
void Camera::SetPosition(float x, float y, float z)
{
	//オーバーロードをするとき
	//仮に、ｚは０以下になってほしくない
		//→その時、両方の関数で、→if文をかましたりして、→もう一方の関数でやり残しがあるとなってしまう

	//→この時、メインは、XMVECTORで引数をとっているほうの関数
	//→そんな時は、受け取ったfloat方で、→XMVECTORのほうの関数を読んで、細かい設定は、そっちの関数でやってもらえばいい

	SetPosition(XMVectorSet(x, y, z, 0));
}

XMVECTOR Camera::GetPosition()
{
	return position_;
}

XMVECTOR Camera::GetTarget()
{
	return target_;
}

//焦点を設定
void Camera::SetTarget(XMVECTOR target)
{
	target_ = target;	//引数のターゲットを、namespaceCameraのpositionに
}
void Camera::SetTarget(float x, float y , float z)
{
	SetTarget(XMVectorSet(x, y, z, 0));
}


//ビュー行列を取得
XMMATRIX Camera::GetViewMatrix()
{
	return viewMatrix_;

}

//プロジェクション行列を取得
XMMATRIX Camera::GetProjectionMatrix()
{
	return projMatrix_;
}

//画角（視野の広さ）を設定
void Camera::SetAngle(float angleValue)
{
	//アングルを変更するために
	//プロジェクション行列の画角を変更させる

	//プロジェクション行列(初期値)
	projMatrix_ = XMMatrixPerspectiveFovLH(XM_PIDIV4, (FLOAT)Direct3D::scrWidth / (FLOAT)Direct3D::scrHeight, 0.1f, 100.0f);
		//第一引数：画角角度

};

void Camera::AddAngleForZoomOut(float addValue)
{
	angle += addValue;
	//プロジェクション行列(初期値)
	//行列の作り直し
	projMatrix_ = XMMatrixPerspectiveFovLH(angle, (FLOAT)Direct3D::scrWidth / (FLOAT)Direct3D::scrHeight, 0.1f, 100.0f);
	//第一引数：画角角度
};

void Camera::SubAngleForZoomIn(float subValue)
{
	angle -= subValue;

	//プロジェクション行列(初期値)
	projMatrix_ = XMMatrixPerspectiveFovLH(angle, (FLOAT)Direct3D::scrWidth / (FLOAT)Direct3D::scrHeight, 0.1f, 100.0f);
	//第一引数：画角角度
};

