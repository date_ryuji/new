#pragma once
#include "PolygonGroup.h"
#include <fbxsdk.h>	//FbxSDK

//ライブラリのリンク
#pragma comment(lib, "LibFbxSDK-MT.lib")
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")



/*
・枝クラス

・３Dポリゴンとして、描画を行わないクラス
・３Dポリゴンとして、BranchesMakeTreeに結合のための素材クラス。
	・自身では描画せず、BranchesMakeTreeを作る、一つのソースの役割を示す。


*/


class Branch : public PolygonGroup
{
public : 
	//頂点情報
	//現段階ではポジションだけの頂点情報（のちに法線）
	struct VERTEX
	{
		XMVECTOR position;	//位置情報（頂点の位置）
		XMVECTOR uv;		//UV情報	（頂点のテクスチャの座標）
		XMVECTOR normal;		//法線情報	（頂点の陰）

		//コンストラクタ
		VERTEX();
	};



protected:
	//描画をしないため、
	//コンスタントバッファは必要なし



	//各頂点ごとの頂点情報を入れる配列
	VERTEX* pVertices_;

	//インデックス情報を登録しておく
	//動的確保した配列
	int* pIndex_;


	//頂点情報を受け渡し
	ID3D11Buffer *pVertexBuffer_;
	//頂点の順番→インデックス情報を持っておく情報
	ID3D11Buffer *pIndexBuffer_;

	//FBXを読み込むので、あらかじめわからないので、
	//メンバとして宣言しておく
	int vertexCount_;	//頂点数（FBXファイルより取得）
	int polygonCount_;	//ポリゴン数（FBXファイルより取得）

	//インデックス数
	int indexCountEachMaterial_;



protected:


	//頂点バッファ作成
	//引数：FBXモデルのノードのメッシュ
	//戻値：エラーの有無
	HRESULT InitVertex(fbxsdk::FbxMesh * mesh);
	//インデックスバッファ作成
	//引数：FBXモデルのノードのメッシュ
	//戻値：エラーの有無
	HRESULT InitIndex(fbxsdk::FbxMesh * mesh);

	
	//頂点情報から頂点バッファーを作成
	virtual HRESULT CreateVertexBuffer();
	//インデックス情報からインデックスバッファーを作成
	virtual HRESULT CreateIndexBuffer();


public:


	//コンストラクタ
	//引数：なし
	//戻値：なし
	Branch();

	//デストラクタ
	~Branch() override;

	//ロード
	//ハイトマップ画像からモデルの作成を行う（引数のファイル名より）
	//引数：fileName ファイル名(ハイトマップの画像ファイル)
	//戻値：なし
	HRESULT Load(std::string fileName, SHADER_TYPE thisShader) override;


	//ポインタの解放処理
	//引数：なし
	//戻値：なし
	void    Release() override;


	//頂点数を取得
	int GetVertexCount();
	//ポリゴン数を取得
	int GetPolygonCount();
	//インデックス数を取得
	int GetIndexCount();

	//頂点情報群を取得
	Branch::VERTEX* GetVertexes();
	//インデックス情報群を取得
	int* GetIndexes();


};

