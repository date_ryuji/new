#include "SceneManager.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"
#include "Timer.h"	//シーンの経過時間・デルタタイムを管理するクラス

//SceneManagerにて、シーンをインクルードして親子付け

#include "../TestScene.h"
#include "../SampleScene/SampleScene.h"
#include "../MapEditorScene/MapEditorScene.h"
#include "../CreateMapScene/CreateMapScene.h"

#include "../LSystemEditorScene/LSystemEditorScene.h"



//親のInstantiateの関数から
	//親のポインタをもらうコンストラクタをもらって、（継承元）GameObjectクラスの引数２つのコンストラクタを呼ぶ
SceneManager::SceneManager(GameObject * parent)
	:GameObject(parent, "SceneManager"),
	pCurrentInstance_(nullptr)
{
}

SceneManager::~SceneManager()
{
	Model::Release();
	Image::Release();
	//Audioクラスの解放処理
	//Audio::Release();	//Main.cppにて

}

void SceneManager::Initialize()
{
	//Audioクラスの初期化
	//Audio::Initialize();	//Main.cppにて



	//最初のシーンを準備
	currentSceneID_ = SCENE_ID_LSYSTEM_EDITOR;
	//次のシーンも現在のシーンに、→これで、Updateが呼ばれてもシーンが切り替わることはない
	nextSceneID_ = currentSceneID_;
	//インスタンスを生成（SceneManagerの子供に親子付）
	pCurrentInstance_ = Instantiate<LSystemEditorScene>(this);


}

//シーン切り替えの条件がそろっていたら
//シーン切り替えが行われる
void SceneManager::Update()
{
	//シーンIDが違うならシーンを切り替える
	if (currentSceneID_ != nextSceneID_)
	{
		//子供の解放
		this->ReleaseSub();

		////そのシーンのオブジェクトを全削除
		////子供以下の解放
		//pCurrentInstance_->ReleaseSub();
		////シーンの解放
		//SAFE_DELETE(pCurrentInstance_);


		//ロードしたデータを全削除
		Model::ReleaseAllModelData();
		Image::ReleaseAllImageData();
		Audio::ReleaseAllAudioData();

		


		//次のシーンを作成
		//次フレームからプレイされるシーン
		switch (nextSceneID_)
		{
		case SCENE_ID_TEST: pCurrentInstance_ = Instantiate<TestScene>(this); break;
		case SCENE_ID_SAMPLE: pCurrentInstance_ = Instantiate<SampleScene>(this); break;
		case SCENE_ID_MAP_EDITOR: pCurrentInstance_ = Instantiate<MapEditorScene>(this); break;
		case SCENE_ID_CREATE_MAP: pCurrentInstance_ = Instantiate<CreateMapScene>(this); break;
		case SCENE_ID_LSYSTEM_EDITOR: pCurrentInstance_ = Instantiate<LSystemEditorScene>(this); break;

		}

		//現在のシーンを次のシーンに代入
			//次フレームからは、ChangeSceneが呼ばれない限り
			//現在のシーンと次のシーンが同じなので、Updateが呼ばれても、シーンが切り替わることはない。
		currentSceneID_ = nextSceneID_;



		//シーンタイマーリセット
		Timer::Reset();
	}

}

void SceneManager::Draw()
{
}

void SceneManager::Release()
{
	
}

void SceneManager::ChangeScene(SCENE_ID changeSceneID)
{
	//次のシーンIDに引数の値を代入
		//次のフレームのUpdateにてシーン切り替えの実行
	nextSceneID_ = changeSceneID;

}
