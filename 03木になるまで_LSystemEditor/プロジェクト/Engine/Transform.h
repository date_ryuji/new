#pragma once
#include <DirectXMath.h>

//★外部でも使用が可能なように、ヘッダ部分に書いておく
//本来であれば、positionのベクター型のｘ成分にアクセスするには、
	//m128_f32[0]を指定しなくてはいけない→だが、１年生のように名称で管理したい
	//m128_32f[0]を名称で管理できるように名前で作っておく→#defineの定数でvecXとして取得
#define vecX m128_f32[0]
#define vecY m128_f32[1]
#define vecZ m128_f32[2]
/*
	XMVECTOR型の中の、要素のm128_f32[0]なので、
	position_ , rotate_ , scale_において、同じ型を使用しているなら、共通して使用することが可能である。

*/


using namespace DirectX;

//位置、向き、拡大率などを管理するクラス
//
class Transform
{
	
public:
	//行列
	XMMATRIX matTranslate_;	//移動行列（positionのｘｙｚで移動行列を作る）
	XMMATRIX matRotate_;	//回転行列	
	XMMATRIX matScale_;	//拡大行列

	//ベクトル
	//publicにすることで、外部から、~.xと指定できるようにした
	XMVECTOR position_;	//位置
	XMVECTOR rotate_;	//向き
	XMVECTOR scale_;	//拡大率

	//自身の親オブジェクトのTransform（親のワールド行列を取得したい）
	//親のワールドがわかれば、自分のワールドに掛ければ自身のTransが親基準になる
		//これは解放してはいけない→なぜなら、すでに宣言されているものを入れるだけのポインタなので、
		//親のオブジェクトを指定するときに、いつもらえばいいのか？？
	Transform* pParent_;


	//コンストラクタ
	Transform();

	//デストラクタ
	~Transform();

	//各行列の計算
	void Calclation();	//各positionなどから、移動行列を作る関数




	//ワールド行列を取得
	XMMATRIX GetWorldMatrix();	//すべての行列を掛けて送る
};