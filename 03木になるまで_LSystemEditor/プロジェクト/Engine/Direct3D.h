#pragma once
#include <d3d11.h>	//Direct３Dの略
#include <d3d11_1.h>	//Device1,SwapChain1 ヴァージョンが違うSwapChainを使用できるようにする
						//２Dを使用するために、SwapChain1を使用する
#include <d3d11_2.h>
#include <assert.h>
#include <DirectXMath.h>
#include "Global.h"
#include "../Shader/CommonDatas.h"


//リンカ
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")	//シェーダーをコンパイルするために、追加
	//ビルドされて、アプリにするながれ、プログラムをコンパイルされて、
	//リン化して、コンパイルをしたものに、あらかじめ用意されているものをくっつける（先にコンパイル済みのものを、くっつける）
	//インクルード	は、コンパイル前にやっている
//★→これはどこのソースに書いても一つ書いてしまえばいい
	//ソース内でなくても、プロジェクトの中に書いてしまうこともできるが、→ソースに書いたほうが忘れないので、
//これをすれば、DirectXSDKの、ライブラリが追加される


//#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
//マクロ
//→deleteの際に、ポインタが、領域のアドレスを覚えているときに、そのアドレスを忘れさせる
	//→これによって、deleteした後に、

//#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}
//Releseも同じく
//ポインタに使用されているポインタのReleaseを呼んであげる。



using namespace DirectX;
class Texture;
class BaseShader;
class BaseBlend;



//外部からも、読み込めるように、enumは、外に宣言しておく
namespace Direct3D

{
	//関数を創ったら、必ず引数と、戻値を書く
	//コメントを書けば、→呼び出すとき、つながるコメントを表示してくれる
	//→そこに、引数などを何かを書いておけば、いちいちヘッダを開く必要もない


	extern ID3D11Device*   pDevice;		//デバイス
		//extern　にしないと、これを、インクルードしたときに、仮に、pDevice単体であった時に、→Direct3Dをインクルードしたところで、新しく領域がとられる
		//なので、気共通のものを使いたいときは、→externでプロトタイプ宣言のようなもの、→後でどこかで宣言するので、インクルード先では、その宣言したものをきょうつうしてつかいます　


	extern ID3D11DeviceContext*    pContext;
				//コンテキストを、他ソースからも見れる、形に
				//cppにて、宣言し、インクルード先で、その宣言したものを使えるように

	//externは、これを作っておくよという、宣言のようなもの
		//cppにて、値の初期化を行わなければいけない（どこかで宣言）
	extern int scrWidth;	//ウィンドウの横幅
	extern int scrHeight;	//ウィンドウの縦幅


	//トゥーンシェーディング用の
	//テクスチャの用意(全モデルがこのテクスチャを用いて、トゥーンシェーディングを行わせるようにするため、Direct3Dにて用意)
	extern Texture* pToonTex;
	//環境マップ用の
	//テクスチャの用意
	//モデルに、環境の映り込みを再現させる。
	//箱型のテクスチャを用意し、　そのテクスチャの色を反映させる
	extern Texture* pCubeTex;

	//テクスチャをロードさせたいが、
	//DDSクラスなので、　Cubeテクスチャをロードする機能を、まだTexutreクラスに作っていない。　それを作り、それでロードさせる。



	//初期化
	//引数：winW ウィンドウ幅
	//引数：winH ウィンドウ高さ
	//引数：hWnd ウィンドウハンドル
	//戻値：なし
	HRESULT Initialize(int winW, int winH, HWND hWnd);
	//Direct3D初期化
//Device・DeviceContext・SwapChainを同時作成
	//Direct2D非対応
	HRESULT InitDirect3D(int winW, int winH, HWND hWnd);

	//Direct3D初期化
	//Device・DeviceContext　、　SwapChainを別々作成
		//Direct2D対応
	HRESULT InitDirect3DAndSupprtsDirect2D(int winW, int winH, HWND hWnd);

	//シェーダークラスのポインタを渡す
	//引数：シェーダータイプ
	//戻値：シェーダークラスのポインタ
	BaseShader* GetShaderClass(SHADER_TYPE type);
	//ブレンドクラスのポインタを渡す
	//引数：ブレンドタイプ
	//戻値：ブレンドクラスのポインタ
	BaseBlend* GetBlendClass(BLEND_TYPE type);
	//シェーダーの初期化(準備)
	//
	//戻値：エラー処理結果（エラーなし：S_OK、エラーあり：E_FAIL）
	HRESULT InitShader();
	//ブレンド（色の混合）の初期化
	HRESULT InitBlend();
	void SetShaderBundle(SHADER_TYPE type);
	void SetBlendBundle(BLEND_TYPE blendType);


	//HRESULT InitShaderKurosawaMode();
	//HRESULT InitShaderOutline();
	//HRESULT InitShaderToon();
	////３Dシェーダーの初期化　
	//HRESULT InitShader3D();
	////２Dシェーダーの初期化
	//HRESULT InitShader2D();
	//HRESULT InitShaderTest();


	//ゲーム全体の背景色の変更
	//引数：R値
	//引数：G値
	//引数：B値
	//引数：A値
	void SetBackGroundColor(float r, float g, float b, float a);
	//引数：RGBA値
	void SetBackGroundColor(XMVECTOR& rgba);
	//背景色を取得
	XMVECTOR GetBackGroundColor();

	//レンダー先を追加
	HRESULT CreateRenderTarget2();



	//描画開始
	//引数：なし
	//戻値：なし
	void  BeginDraw();

	void BeginDraw2();


	//描画終了
	//引数：なし
	//戻値：処理が成功か（HRESULT型の関数が成功した場合、最終的にS_OK、失敗の時その都度E_FAIL（どんなエラー化は、気にせずエラーです）
	HRESULT EndDraw();


	void SetDepthBafferWriteEnable(bool isWrite);


	//解放
	//引数：なし
	//戻値：なし
	void Release();


	//スワップチェイン１を渡す
		//Direct2D初期化の際に使用するSwapChainのポインタを渡す
	//戻値：スワップチェイン１のポインタ
	IDXGISwapChain1* GetSwapChain1();

	//DXGIDeviceを渡す
		//Direct2D初期化の際に使用するDeviceのポインタを渡す
	//戻値：デバイスのポインタ
	IDXGIDevice* GetIDXGIDevice();

	//デバイスコンテキスト　と
	//スワップチェーンの接続を担う
	//戻値：Surfaceポインタ
	IDXGISurface* GetIDXGISurface();


};

//Direct3Dに必要なものを、namespaceでまとめた
//→書き方としては、クラスと変わらない


