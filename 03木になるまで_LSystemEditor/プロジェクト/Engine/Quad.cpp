#include "Quad.h"
#include "Camera.h"
#include "Transform.h"	//Transformクラスの、移動、回転、拡大行列を使用するため
#include "Global.h"
//#include "Direct3D.h"
//cppでインクルードしたが、ヘッダでインクルードしないと
//頂点バッファの変数を作るために


Quad::Quad()
	
{
	//初期化（ポインタの）
	pVertexBuffer_ = nullptr;	//頂点バッファ
	pIndexBuffer_ = nullptr;

	vertex_ = 9;
}

//初期化して、　表示する、
//おなじみの関数を作っておく
	//Updateは今回なし

Quad::~Quad()

{
	//ポインタで取得していても、newで動的確保はしていないので、　→解放もなし
	

}


//頂点情報の初期化（頂点バッファー作成まで）
HRESULT Quad::InitVertex()
{
	// 頂点情報
	//構造体で、　位置、UV
	VERTEX vertices[] =

	{


		{XMVectorSet(-1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 0.5f, 0.0f, 0.0f)},	// 四角形の頂点（左上）

	{XMVectorSet(1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 0.5f, 0.0f, 0.0f)},	// 四角形の頂点（右上）

	{XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f)},	// 四角形の頂点（右下）

	{XMVectorSet(-1.0f, -1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)},	// 四角形の頂点（左下）		


		//頂点追加
		//四角形の上に、頂点を一つ追加して、→それをほかの頂点とつなげることで、→家の形に
			//→頂点の座標、ｘｙｚの位置を調整
	{XMVectorSet(0.0f, 2.0f, 0.0f, 0.0f),XMVectorSet(0.5f, 0.0f, 0.0f, 0.0f)},	//家の頂点を表す



		/*
		//★	星形
		XMVectorSet(-1.0f,  -1.0f, 0.0f, 0.0f),
		XMVectorSet(1.0f,  -1.0f, 0.0f, 0.0f),
		XMVectorSet(0.0f,  2.0f, 0.0f, 0.0f),			//2,1,0(時計回り)


		XMVectorSet(-1.0f,  1.0f, 0.0f, 0.0f),
		XMVectorSet(1.0f,  1.0f, 0.0f, 0.0f),
		XMVectorSet(0.0f,  -2.0f, 0.0f, 0.0f),			//3,4,5(時計回り)

		//*/

	};
	//DirectXの名前空間のXMVECTORを使い、ベクター型で頂点情報を入れる、配列を取得


	//ここの、頂点１番目に書かれているものが、頂点０として扱われる
		//→これをインデックス情報に入れるときに、どの頂点でさんかっけいが作られているかを表すときの順番にナウr

	// 頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;

	bd_vertex.ByteWidth = sizeof(vertices);

	bd_vertex.Usage = D3D11_USAGE_DEFAULT;

	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	bd_vertex.CPUAccessFlags = 0;

	bd_vertex.MiscFlags = 0;

	bd_vertex.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA data_vertex;

	data_vertex.pSysMem = vertices;

	/*
	HRESULT hr;
	//HRESUTLの型に、→バッファーを作成したときにエラーを返さないか、その結果をもらう
		//HRESULTに、成功と、失敗とともに、どんな失敗をしたのか、　
	hr =
	Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_);
			//頂点バッファを作ります
			*/


			//HRESULT型で、結果を受けるCreateBufferという関数などを使用したとき
			//→それにより、正しいエラー処理
			//失敗したかの判断
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		//エラーがわからないので、→エラーをWindowsの処理で、表示させる
		//メッセージボックス（上にウィンドウが出てくる）
		MessageBox(nullptr, "頂点バッファ作成失敗", "エラー", MB_YESNO);
		//メインのメッセージ、タブ、　OKボタン(他にも種類がある)
		//→★→これにより、失敗したところで、メッセージを出してもらう

		return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//→エラーの時、呼び出し先がそのエラーにより、帰ってきたエラーという表示により、プロジェクトを終了させるなど、処理を行わせる
						//呼び出し先では、一切責任は負わない（あくまで、Mainの一番上で、終了などの、そのエラーによる処理を行う）

	}
	//関数を作るとき、HRESULT型にしておいたほうが、結果を知れてよいことが多い
	//HRESULT型は、以上のように、エラー判断受け取り→それにより、呼び出し元でエラーを受けたときの処理


	//pDeviceを使いたいのだが、このpDeviceは、DirectX.cppに書かれているものなので、
		//取得できない（ヘッダしかインクルードしていないので、）
	//その時に、使うのが、extern
		//Direct３Dにて、pDeviceというポインタをしゅとくしている　（監督）
		//→そこで、初期などをしているものを、このクラスの中でほしい
		//→だが、.cppにて、初期化しているポインタが最終的には欲しい、
		//→ヘッダだけをインクルードしても、インクルードするというのは、あくまで、そこに書いてあることを、インクルード先のソースでコピーして貼り付けているに過ぎない
			//→となると、.cppにて初期化しているものは適用されない

		//★→cppにて、初期化したものを使いたいのだ、共通したものを使いたいのだ
		//★→インクルードするヘッダにて、extern Class *class;	と、externとして定義しておく

		//→するとどうなるのか・→externとして定義したものは、どこかで宣言するよ→となる、その宣言したものが、ヘッダの中のclassに入る。
		//→他クラスで、ヘッダをインクルードして、classを呼び込むと、→どこかで宣言したclassが、（つまり、初期化されて作られたものが）対象になる
		//→★★共通の変数、クラスを使うことが可能になる



	//インデックス情報
	//メンバのインデックス情報を宣言
	//→インデックス情報では、各モデルごとに頂点は違うため、オーバーライドの関数内でそれぞれ作成してもらう
	index = new int[vertex_] { 0, 2, 3, 0, 1, 2, 0, 4, 1 };	//時計回りに、頂点をとる

	return S_OK;
}

//ここで、初期化の処理内で、CreateBufferなどの、HRESULT型の処理がある。その時、エラーが出たとき

HRESULT Quad::Initialize()

{
	//配列要素をはじめに宣言し、それを後に編集することは、できない
	//→そのため、あらかじめ、宣言しておいた形での更新ではなく、
		//→かつ、関数の戻り値で取得する形でもなく（indexの一つの配列のために新しいオーバーライドの関数を作るわけにもいかないので、InitVertexの関数に入れてしまおうと考えた（しかし、InitVertexの戻り値は、すでに決まっているので、））
	//


	/**
	int *index1[9];
	int *index2;
	index2 = new int[2];
	*/



	/*	配列の要素を後に宣言させる�@
	int index1[9];
	int index2[9] = { 0,0,0,0,0,0,0,0,0 };	//index2で、頂点情報を取得し、先に宣言した配列に回して代入
											//だが、効率が悪いように思える
	for (int i = 0; i < 9; i++)
	{
		index1[i] = index2[i];
	}
	*/

	/*
	
	int index[vertex_];	//配列の要素数を、変数で取得することはd系内が、
	index = new int[vertex_]{ 0,2,3, 0,1,2, 0,4,1 };	//配列を動的に確保するときに、要素数を配列で取得することができてしまう

	*/

	if (FAILED(InitVertex()))
	{
		MessageBox(nullptr, "モデルの頂点情報の初期化失敗", "エラー", MB_YESNO);
		return E_FAIL;
	}
	

	
	//int index[] = { 0,2,3, 0,1,2, 0,4,1 };
									//頂点、三角形の頂点のつながりを書く（どの頂点と、どの頂点と、どの頂点で、三角形ができているのか）
									//最初は、２つの三角形だkえ{ 0,2,3, 0,1,2 }
	//星形　int index[] = { 2,1,0, 3,4,5 };
				//三角形にした時の最終頂点数　＝６

	//以下の、メモリのサイズを指定しているので、
		//→配列におけるメモリのサイズを取得しなくてはいけない
	//→ここでindexの配列のサイズを取得するとき
		//→配列の型は　int →　そして、sizeofというのは、intの型のサイズまで取得できる
	//→となれば、int を　頂点数分取得
	//sizeof(int) * vertex_
	//intのサイズ　＊　頂点数分	→　intの配列の要素数分のサイズ

	//配列の要素数を取得したい
		//インデックス情報を取得した後に、その数分だけ、要素数を頂点数として取得できれば良いが、、、

	//ポインタをそのまま、Sizeofでメモリの容量を受け取るときは、
		//→ポインタを入れると、→pointerのサイズが出てくる
	byte vertexSize = (byte)(sizeof(int) * (vertex_));


	//三角形を一つ増やす
		///→頂点を必要なだけ増やして、
		//→その頂点のつながりで、三角形を作る→三角形の頂点のつながりを、上記のindexに入れているため→そのindexに、もう一つの三角形の頂点順番を入れる
	//頂点情報と、インデックス情報を変更しても、三角形は表示されない
		//→Drawの描画時にも、最終的な三角形の頂点集を指定しないと描画はできない


	// インデックスバッファを生成する

	D3D11_BUFFER_DESC   bd;

	bd.Usage = D3D11_USAGE_DEFAULT;

	bd.ByteWidth = vertexSize;	//サイズを、indexのサイズ分だけ、（インデックス情報の入っているサイズだけ）

	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

	bd.CPUAccessFlags = 0;

	bd.MiscFlags = 0;



	D3D11_SUBRESOURCE_DATA InitData;

	InitData.pSysMem = (void*)index;	//作成したインデックス情報を渡す
										//インデックスのint型のポインタを、メモリーに渡す？
										//送るときに、（void*）出ないと送れないとエラーを返されるため、(void*)でキャスト
	InitData.SysMemPitch = 0;

	InitData.SysMemSlicePitch = 0;

	//成功したかの判断
	//失敗したら、呼び出し元の関数にエラーを返す
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
	}
	//Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
		//インデックスバッファ（領域）を作成



	//あとは、カメラが動くかもしれないので、
		//→後から、コンスタントバッファと、合わせ得られるように
	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//成功したかの判断
	//失敗したら、呼び出し元の関数にエラーを返す
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
	}
	// コンスタントバッファの作成
	//Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_);


	//テクスチャのロード
	TextureLoad();

	
	return S_OK;	//S_　HRESULTの結果が、成功したとき、→最後まで来るので、
				//最後まで来たら、OKですよと、返す（エラーの時、HRESULTの型のところで、エラーですと返している→その時、このInitializeを読んでいる関数に、送られる→それにより、その関数が、処理を行う）
}

HRESULT Quad::TextureLoad() {
	//ファイルのロード
	//テクスチャのロード
	pTexture_ = new Texture;
	if (FAILED(pTexture_->Load("Assets\\Texture1.jpg")))
	{
		MessageBox(nullptr, "ファイルの読み込みを失敗", "エラー", MB_OK);

		SAFE_DELETE(pTexture_);
		return E_FAIL;
	}
	//ロード時

	return S_OK;
}


HRESULT Quad::Draw(Transform& transform)
					//回転の行列を受け取る（ワールド行列→ベクトルに行列をかけて、回転の度合いをかけたベクトルにしたときのように）
					//ワールド行列で、移動、回転、拡大などなど　それぞれの行列を受け取る（→ワールド行列を、回転、移動２つの行列を送りたいなら、→あらかじめ２つの行列をかけてそれを送れば、２つの行列の変化を受けた行列になるので、（２つあるときは、→あらかじめ２つで掛け算をしてしまい、それを送るう））

{
	//シェーダーを選択
	Direct3D::SetShaderBundle(SHADER_3D);
		//３Dのシェーダーを使用するために、Direct3D内の、シェーダーをセットする、関数へ、enumの値を扱ってあげる
		//→Direct3Dをインクルードしているので、　→enumの値で、セットしてあげた名前を使用することが可能
		




	//カメラのヘッダから、直接指定する

	/*
	//コンスタントバッファに渡す情報
	XMVECTOR position = { 0, 3, -10, 0 };	//カメラの位置
		//世界の中心から、
	XMVECTOR target = { 0, 0, 0, 0 };	//カメラの焦点

	XMMATRIX view = XMMatrixLookAtLH(position, target, XMVectorSet(0, 1, 0, 0));	//ビュー行列
										//位置、　焦点　、　カメラの傾き
	//位置が、かわるかもしれないので、　カメラの支店などを、変数で持っておく																				//ビュー行列を作っている
	


	XMMATRIX proj = XMMatrixPerspectiveFovLH(XM_PIDIV4, 800.0f / 600.0f, 0.1f, 100.0f);//射影行列
							//				画各視野角の指定４５度、　パステクと比（１に近いほど）　	、０．１（１０ｃｍ）から、１００ｍ映すという範囲（範囲は狭いほうがいい→広すぎると、前後計算に誤差が出てくる）		
							//サイズの表示するものの、ギリギリが移るぐらいの大きさにする

							//なぜ、カメラの０から見ないのか→それは、プロジェクト座標にするために、
							//→後々圧縮するために、０だと、→視野が、完全に三角錐になってしまう→すると、伸ばすことができなくなる
							*/

	
	//シェーダー内のcbufferの変数にアクセスするために、ヘッダにて取得した、構造体の中の変数に　行列の登録
	//コンスタントバッファに渡すための構造体

	CONSTANT_BUFFER cb;
	cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() *Camera::GetProjectionMatrix());	//シェーダーに持っていくときに、行列のために、縦に読む行列と、横に読む行列があり、それのどちらかにしないといけないので　
								//★受け取った、Transformのワールド行列を取得
																														//XMMatrixTranspose＝行列を、縦読みするか、横読みするか（その縦と横を入れ替える（））
				//コンスタントバッファの型に、構造体の中の、行列に入れる
				//カメラから、送った、位置、焦点で、行列を作ってもらい、それを受け取る
	//コンスタントバッファに書き込みたいが、
		//コンスタントバッファは特殊な位置に存在する
		//普段は、書き込むことができない

	//法線を回転させるため、回転行列を送るために、worldMatrixを送る×（Mainから、回転行列を受け取っているので、）
		//Transformのワールド行列を送ってしまうと、エラーになる
		//→なぜ？
		//matWというのは、法線を回転させるために、送っていた
		//★★そして、欲しい行列というのは、→回転が欲しい、移動いらない、拡大の逆行列(元の動きとは逆の動きをする行列)
			//Transform型から行列を受け取って渡す
	cb.matW = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr , transform.matScale_));	//



	D3D11_MAPPED_SUBRESOURCE pdata;

	//成功したかの判断（エラーチェック）
	//失敗したら、呼び出し元の関数にエラーを返す
	if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "コンスタントバッファのマップの書き込み失敗", "エラー", MB_YESNO);

		return E_FAIL;	//エラーですと返す。
						//親の呼び出し元の関数に、HRESULTの型で、エラーなら、エラーですと返す

						//呼び出し元が、エラーによる処理を行う（その関数にも呼び出し元があるなら、それにまたエラーですとお来る（最終的な親で、プロジェクト終了など））
	}
	//Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);	// GPUからのデータアクセスを止める
						//Mapとして、書き込むために呼び込む
	memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	// データを値を送る
					//配列にあるデータなどを、他の領域に●っとコピー
					//memcpy_s (コピー先、コピー先のサイズ、コピー元　、コピー元のサイズ)
					//コンスタントバッファに先ほど作ったものを書き込む
						//コピー元の先頭アドレスを入れたい、void 型のアドレス→アドレスであって、特にどの型でもないので、voidの、アドレスの型にキャストしている
						//構造体で作った、cbのアドレスを、→入れる
	
	//テクスチャのサンプラーの取得
	ID3D11SamplerState* pSampler = pTexture_->GetSampler();	//テクスチャクラスあkら、サンプラーを取得
	Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);
	//シェーダーへの橋渡しのビューを取得
	ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
	Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);


	Direct3D::pContext->Unmap(pConstantBuffer_, 0); 	//再開
						//書き込んだので、最後に、Unmapにて、　音汁
						//コンスタントバッファに送る



						//頂点バッファ（頂点を持っておくための領域（確保場所））
		//複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
	UINT stride = sizeof(VERTEX);	//1頂点のサイズを指定する、→位置のみであれば、XMVECTORの構造体であったが、
								//位置とUVで構造体をとるので、その構造体のサイズで
	UINT offset = 0;
	Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

	// インデックスバッファーをセット
	stride = sizeof(int);
	offset = 0;
	Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	//コンスタントバッファ
	Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
	Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用


	//描画
	//★頂点の個数を表す、→ここの頂点の数を、最終的な三角形の頂点の数
	//★この情報を追加しないと、頂点による、三角形を描画してくれない（たとえ頂点情報、インデックス情報を変更しても。。。）
	Direct3D::pContext->DrawIndexed(vertex_, 0, 0);	//頂点が６つなので、６つ


	//成功
	return S_OK;

}
//


void Quad::Release()

{

	pTexture_->Release();
	SAFE_DELETE(pTexture_);
	SAFE_DELETE_ARRAY(index);	//インデックス情報を使用する、関数内にて、宣言し、使い終わった後に開放してしまうと、エラーになってしまったので、すべての作業が押さった後に、呼び出すようにめんばへんすうにした
	//ヘッダにて、確保したポインタを、逆順にリリース
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);

	

	//ID3D11から始まるものは、解放処理を呼ばずとも、ID3D11→Releaseが用意されているので、→それで解放してくれる
		//１回呼ぶだけで大丈夫？？？（スマートポインタ？？？）
	//同じコンスタントバッファなどID3D11の型を使用している方を他クラスにて、解放しようとすると→エラーが起こる（すでに解放されてしまっている？など？）
		//なので、　１回だけしか呼ぶことができない。あるいは呼ぶ必要がない？ID3D11の型で、Releaseを呼ぶ？？
	//頂点バッファなどを、確保するために、使った、型を開放


}