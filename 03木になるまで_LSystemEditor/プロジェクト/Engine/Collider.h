#pragma once
#include <DirectXMath.h>


using namespace DirectX;


//プロトタイプ宣言
class GameObject;
class BoxCollider;
class SphereCollider;
class DebugWireFrame;	//デバック用の、ワイヤーフレームのサイズを視認できるようにする、ワイヤーフレームモデル


//コライダークラス




/*
コライダーの種類
・スフィア　球形
・ボックス　六面体（四角形）

*/

/*
当たり判定の種類
・スフィア　スフィア
・ボックス　スフィア
・ボックス　ボックス

*/

//継承関係
	//コライダーをオブジェクトから呼び込む
		//・Shere　Boxクラスから　オブジェクトを作成
	//GameObjectに、Colliderクラスとしてオブジェクトを、持たせる

	//GameObjectから　毎フレーム衝突判定の計算を呼び込む

/*
//当たり判定の機能の種類
・機能なし（通常のあたり判定のみ）
・壁ずり（当たり判定を持つオブジェクトと衝突時、侵入した分だけ押し出される（自身だけ）接触したオブジェクトの面に沿って、押し出される）
・押し出し（当たったオブジェクトを押し出す（相手を動かす、自身は動かない））

*/

//コライダーの種類
enum COLLIDER_TYPE
{
	SPHERE_COLLIDER = 0,	//球形
	BOX_COLLIDER,			//四角形

	MAX_COLLIDER_TYPE,


};
//当たり判定の機能の種類
enum FUNCTION_TYPE
{
	NO_FUNCTION = 0,			//機能なし
	ALONG_THE_WALL_FUNCTION ,	//壁ずり（壁に沿う）
	EXTRUDE_FUNCTION,			//押し出す（壁ずりが、機能なしのコライダーとも壁ずりを行うようになるので、機能なしとほとんど機能は変わらない）

	MAX_FUNCTION_TYPE,

};

//GameObjectに持たせる
	//当たり判定コライダーの親クラス
class Collider
{
protected :
	COLLIDER_TYPE myColliderType_;	//自身のコライダータイプ
	FUNCTION_TYPE myFunctionType_;	//自身のコライダー機能タイプ

	bool doJudge_;	//判定するか、しないかのフラグ


	/*毎フレーム更新*/
	//オブジェクトからのコライダーの位置（ワールドではない）
		//オブジェクトの原点からのコライダーの原点の位置、離れ具合
	XMVECTOR myPos_;

	/*毎フレーム更新*/
	//コライダーを付けている
	//オブジェクトの位置
	XMVECTOR objectPos_;


	//自身のコライダータイプをセットする
	void SetMyType(COLLIDER_TYPE colliderType);

	//コライダーサイズを視認させるモデルクラス
	DebugWireFrame* pDebugWireFrame_;

	//自身と衝突したコライダーを保存しておく
		//衝突判定後の、
		//押し出しの処理時に使用する。
	Collider* pTarget_;


	//自身のコライダーを所有しているオブジェクト
	GameObject* pGameObject_;



protected:
	//サイズをワイヤーフレームモデルクラスに送る
	virtual void SetWireFrameSize();

	//float にて示される半径からサイズを変更
	void SetWireFrameSize(float radius);

	//XMVECTOR にて示される半径からサイズを変更
	void SetWireFrameSize(XMVECTOR& size);

	//保存用のコライダーにセットする
	void SetTargetCollider(Collider* pTarget);
	//ターゲットのコライダーと、自身のコライダーを判別して、
	//ターゲットのコライダーを上記の関数で登録する
	void JudgeTargetCollider(Collider* pColl1, Collider* pColl2);

	//コライダーを所有しているモデルのScale値を取得
		//これをもとに
		//Sphereなら、半径を可変
		//Boxなら、　　サイズをそれぞれ可変
			//して、そのサイズをもとに、当たり判定を行わせる
	//球専用：ｘｙｚともに等しく可変するために、サイズを取得する
		//この時、vecXのScale値を取得するようにするため、ｘｙｚそれぞれが違うと、想定通りの可変にならない可能性もある。
	float GetGameObjectScaleForSphere();
	//箱専用：ｘｙｚそれぞれを可変するために、それぞれのサイズを取得する
		//ｘｙｚそれぞれのScale分、箱のサイズのｘｙｚを可変
	XMVECTOR GetGameObjectScaleForBox();

	//上記の関数を呼び出し、
	//引数に手渡されるコライダーからサイズを取得し、サイズを自身を所有しているコライダーのサイズ分可変させる
	//引数：コライダーのポインタ(球専用)
	float GetVariableSize(SphereCollider* pSpColl);
	//上記の関数を呼び出し、
	//引数に手渡されるコライダーからサイズを取得し、サイズを自身を所有しているコライダーのサイズ分可変させる
	//引数：コライダーのポインタ(箱専用)
	XMVECTOR GetVariableSize(BoxCollider* pBoxColl);


public : 
	////コンストラクタ
	//	//各コライダークラスから、
	//		//メンバとして持たせる情報以外、objectPosなどをもらい、　親のプロテクテッド変数に渡す
	////引数：コライダーのオブジェクトからの離れ具合
	//引数：自身のコライダータイプ
	//引数：自身のコライダーの機能のタイプ
	//引数：ワイヤーフレームを描画するか
	Collider(XMVECTOR position , COLLIDER_TYPE collType , FUNCTION_TYPE funcType, bool drawWireFrame);

	/*Collider(XMVECTOR position , XMVECTOR size);

	Collider(XMVECTOR position , float size);
	*/


	//デストラクタ
		//仮想関数
	virtual ~Collider();


	//コライダーをセットしているゲームオブジェクトをセット
	//引数：自身をセットしているゲームオブジェクトのポインタ
	void SetMyGameObject(GameObject* pGameObject);


	//コライダーのモデルのポインタをもらう
	DebugWireFrame* GetDebugWireFrame();



	//判定する、しないのフラグを管理
	//判定のフラグを立てる
	void DoJudge();

	//判定のフラグを下す
	void DoNotJudge();

	//衝突判定を行う
		//継承先にてそれぞれ、衝突判定式が行える関数を呼び込む
		//自身が球なら、SphereColliderのIsHitが呼ばれるようにし、　IsHitから、球と　相手のコライダーを調べて、そのタイプから、自身と相手の関係性（球と球なのかなど）を調べて、その関係性から計算関数を呼ぶコム
	//引数：相手のコライダー情報　ポインタ
	virtual bool IsHit(Collider* target) = 0;

	//デバック用のワイヤーフレーム描画
	void Draw();
	//デバック用のワイヤーフレーム描画の許可、非許可
	void DrawWireFrame(bool drawWireFrame);


	//コライダーをつけた親の位置をセットする（毎フレーム）
		//毎フレーム衝突判定を行う前に、
		//コライダーを所有しているオブジェクトの位置をセットしてもらう（コライダーのワールド座標は、オブジェクトの位置＋オブジェクトからのコライダーの位置）
	//引数：コライダーを付加しているオブジェクトのワールド座標
	//戻値：なし
	void SetObjectPos(XMVECTOR objectPosition);

	


public :
	/*衝突判定*/
	/*衝突判定の相手の以下の関数へアクセスするとき、protectedだと、アクセスが出来なくなるので、注意（相手は、おなじColliderを継承していても、　実際の親子関係はない、ただの兄弟なので、　アクセスは当然できない）*/
	
	//コライダーのワールド座標を返す
	//戻値は、コライダーをつけたオブジェクト位置＋オブジェクトからのコライダーの位置（つまりコライダーのワールド座標が出せる）
	//引数：なし
	//戻値：自身のコライダーのワールド座標
	XMVECTOR GetWorldPos();

	//自身のコライダータイプをもらう、渡す
	COLLIDER_TYPE GetMyType();



	//球と球の当たり判定
		//SphereColliderより呼ばれる
			//計算に必要な項目を、SphereColliderのIsHitから呼ぶときに、関数の引数として渡す
	//引数：判定を行う　SphereColliderのポインタ
	//引数：判定を行う　SphereColliderのポインタ
	//戻値：衝突したか
	bool IsHitSphereToSphere(SphereCollider* pSphere1, SphereCollider* pSphere2);

	//球と六面体（四角形）の当たり判定
	//引数：判定を行う　BoxColliderのポインタ
	//引数：判定を行う　SphereColliderのポインタ
	//戻値：衝突したか
	bool IsHitBoxToSphere(BoxCollider* pBox1, SphereCollider* pSphere1);


	//四角形と四角形の当たり判定
	//引数：判定を行う　BoxColliderのポインタ
	//引数：判定を行う　BoxColliderのポインタ
	//戻値：衝突したか
	bool IsHitBoxToBox(BoxCollider* pBox1, BoxCollider* pBox2);

public : 
	/*壁ずり判定*/

	//自身のコライダーの機能タイプを選択
	//引数：コライダー機能のタイプ
	void SetColliderFunctionType(FUNCTION_TYPE type);


	//壁ずり判定
		//自身が壁ずりの時、
		//相手のコライダータイプと、コライダー機能タイプによって、処理を行わせる。
		//基本的には、　相手のコライダーに沿わせて、壁ずりで移動させる
		//ここにおける相手とは、Updateの処理の前に行わせた、衝突判定で、衝突した相手である。
			//常に最新の相手が入り、相手がいないときはnullptrが入る。
	void AlongTheWall();

	//自身が壁ずりができるかの確認
	//戻値：壁ずり実行可能
	bool CanColliderAlongTheWall();

	//壁ずりベクトルを求める
	//引数：進行ベクトル
	//引数：法線ベクトル
	//戻値：壁ずりベクトル
	XMVECTOR CalcWallScratchVector(XMVECTOR& progressVec, XMVECTOR& normalVec);
		
	

	
};

