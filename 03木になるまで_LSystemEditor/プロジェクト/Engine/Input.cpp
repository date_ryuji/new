#include "Input.h"

namespace Input
{
	//８＝バージョン→DirectInputは８のまま
		//→キーボードのインプットがされているかの確認なので、
		//→それは、→これ以上バージョンUPすることもないので、そのまま
	//Direct3DでのID11のようなもの（DirectX側の変数）
		//ID3D〜は、RELEASEにて、解放を行わなければいけなかった
		//LP〜もRELEASEにて解放。→たどると→ID8~型のポインタを別名で、ポインタである→なので、RELEASEにて解放することになる
		//書いていないけれど、ポインタなので、→変数の頭にpがついている
	LPDIRECTINPUT8   pDInput_ = nullptr;

	///////////////////////////　キーボード　//////////////////////////////////
	//デバイス
	//ここでいうところのキーボードなどが、デバイス
	//＝キーボードを入れる変数＝キーボードとやり取りする
	//＝Direct3Dにおけるデバイス＝グラフィックボードのこと

	//同じように元をたどればポインタなのでRelease
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;

	//今押されているかを入れる配列（Updateにて、押されていたら、）
	//バイト型の配列＝１バイト＝８ビット（２進数８桁）＝１１１１１１１１（２進数）＝＋１したら１００００００００（10進の２５６）＝０〜２５５まで
	//今まで、1バイト型を扱いたいときは＝char型を使っていた
	//Byteのもとをたどると＝ただのunsigned intである＝それを別の名前で使っているだけ
	//押した、押して内なので、bool型でよさそうだが、仕様なので
		//→おおよそは、キーの数は、１００何個か、→だが、２５６個を超えないだろう→なので、この数分取得
	//〇キーが何番だ→というのは決まっているので、→DIK＿〜　それを使用して、配列に対応する要素に登録してもらう
	BYTE keyState[256] = { 0 };

	//前回押されていたキーを保存しておく配列
	BYTE prevKeyState[256];    //前フレームでの各キーの状態
								//前回のキーと、現在のキーの押し具合（フラグ）を比較して今、初めて押されたか、続けて押されているのかを判断
///////////////////////////　キーボード　//////////////////////////////////

	///////////////////////////　マウス　//////////////////////////////////
	//デバイスを入れるポインタ
	LPDIRECTINPUTDEVICE8	pMouseDevice_;	//デバイスオブジェクト
	
	/*
		構造体なので、
		・マウスのどのボタンが押されているか
		・マウスがxyz方向にどれだけの移動量が移動量しているか、
	
	*/
	//今押されている情報を入れておく構造体（押されている、押されていないの情報をほじんしておく）
	DIMOUSESTATE mouseState_;				//マウスの状態
	//前回の情報
	DIMOUSESTATE prevMouseState_;			//前フレームのマウスの状態

	//マウスのウィンドウにおける現在地を渡すために必要な変数
	XMVECTOR mousePos;						//マウスのポジションを入れておく変数
						
	///////////////////////////　マウス　//////////////////////////////////
	
	///////////////////////////　コントローラー　//////////////////////////////////
	//コントローラーの数
	const int MAX_PAD_NUM = 4;
	/*
	構造体なので、
	マウスと同様に
	・ボタンの押下
	以外にも情報を保持している

	それをコントローラーを複数所有する場合、
	→コントローラー数分、押下状態を保存する構造体を取得する
	*/
	//コントローラーの押下状態を保存しておく構造体
		//コントローラーが複数あるなら、複数の入力状況
	XINPUT_STATE controllerState_[MAX_PAD_NUM];
	//保存用
	XINPUT_STATE prevControllerState_[MAX_PAD_NUM];
	///////////////////////////　コントローラー　//////////////////////////////////
	
	
	//初期化
	//エラー処理が存在するのでHRESULTの返り値を返す
	//引数にてウィンドウハンドル番号を受け取る（Main.cppからInitializeを呼ぶので、その時に、Windowの番号を引数として渡せばよい）
	HRESULT Initialize(HWND hWnd)
	{

		HRESULT hr;	//エラー処理の結果を入れるHRESULT型変数

		//Createなので、本来はHRESULT型なので、エラー処理を行う必要あり、
		hr = DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput_, nullptr);
		//エラー処理
		if (FAILED(hr)) { MessageBox(nullptr, "DirectInputの作成失敗", "エラー", MB_OK); return E_FAIL; }

		//GUID_SysKeyboard=キーボードのデバイスだよ
		//上２行は、pKeyDeviceという変数には、キーボードだよと宣言させた
		hr = pDInput_->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);
		if (FAILED(hr)) { MessageBox(nullptr, "キーボード情報１の作成失敗", "エラー", MB_OK); return E_FAIL; }
		hr = pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
		if (FAILED(hr)) { MessageBox(nullptr, "キーボード情報２の作成失敗", "エラー", MB_OK); return E_FAIL; }
		//ゲーム画面の前に、別のウィンドウがあるときに（ゲームウィンドウが現在のクリックされているウィンドウ化）
		//キーボード入力、動かすか→その判断
		//ウィンドウハンドル、＝ウィンドウの番号＝ゲームに使用しているウィンドウ番号を示す（Windowハンドルの番号はHWND型の変数）
		hr = pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
		if (FAILED(hr)) { MessageBox(nullptr, "キーボード情報３の作成失敗", "エラー", MB_OK); return E_FAIL; }

		//マウス
			//キーボードと同様に、マウスのデバイスの作成を行う
			//キーボードがマウスになっただけ
		hr = pDInput_->CreateDevice(GUID_SysMouse, &pMouseDevice_, nullptr);
		if (FAILED(hr)) { MessageBox(nullptr, "マウス情報１の作成失敗", "エラー", MB_OK); return E_FAIL; }
		hr = pMouseDevice_->SetDataFormat(&c_dfDIMouse);
		if (FAILED(hr)) { MessageBox(nullptr, "マウス情報２の作成失敗", "エラー", MB_OK); return E_FAIL; }
		hr = pMouseDevice_->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
		if (FAILED(hr)) { MessageBox(nullptr, "マウス情報３の作成失敗", "エラー", MB_OK); return E_FAIL; }

		//エラーがなかった時に、OKと返す
		return S_OK;


	}

	//更新
	HRESULT Update()
	{
		//エラー処理結果を入れるHRESULT型変数
		HRESULT hr;

		/***各デバイスの記憶（毎フレーム忘れるので、入手）************************************************/
		/***各デバイスの入力押下フラグの取得*************************/

		//ゲットする、入手する（キーボードを見失うので、また、探すみたいな）
		hr = pKeyDevice->Acquire();
		if (FAILED(hr)) { MessageBox(nullptr, "キーボードの取得の失敗", "エラー", MB_OK); return E_FAIL; }

		//キーボード
		//IsKeyDown（Uｐ）のために、
		//前回のキーを丸っと別の保存用配列に保存しておく
		//GetDeviceStateにて、キー入力のフラグを更新する前に、前回の配列をコピー→更新後には、前回と、今回とで２つの配列が存在することになる
		memcpy(prevKeyState , keyState , sizeof(keyState));
			//memcpy  =　配列の中身を丸っとコピーする関数
			//第一引数：コピー先、第二引数：コピー元、第三引数：コピー元の配列サイズ(コピー元の配列を入れれば、配列全体のサイズが入る)
			//第三引数→配列を入れることで、int a[100]; int value = sizeof(a);		valueには、配列のサイズが入るので、sizeof(int)*100;と同じ

		//すべてのキーをゲットして、（毎フレーム行う（毎フレームUpdateを呼ぶ））
		//そのキーが押されているのか、押されていないのかを取得→すべてを配列に登録
			//キーごとに配列に登録する番号は決まっているので、→その要素へ押してますよ登録（何も押されていないなら、オール０）
			//★押されているキーの要素に１
			//★押されてないキーの要素に０
		hr = pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);
		if (FAILED(hr)) { MessageBox(nullptr, "キーボードの押下フラグの取得失敗", "エラー", MB_OK); return E_FAIL; }


		//マウス
			//キーボードと同様
		//マウスの取得は、ゲームウィンドウ以外の部分をクリックすると、エラーになってしまう
			//→そのため、マウスのデバイスをゲットする部分はHRESULTのエラー処理を行わない
		pMouseDevice_->Acquire();	//マウスのデバイスを再びゲット→１フレームずつ忘れるから
		//if (FAILED(hr)) { MessageBox(nullptr, "マウス取得の失敗", "エラー", MB_OK); return E_FAIL; }
		memcpy(&prevMouseState_, &mouseState_, sizeof(mouseState_));	//前回の押された情報を保存
		pMouseDevice_->GetDeviceState(sizeof(mouseState_), &mouseState_);	//今回の押されたボタンの情報取得
		//if (FAILED(hr)) { MessageBox(nullptr, "マウスの押下フラグの取得失敗", "エラー", MB_OK); return E_FAIL; }


		//コントローラー
			//コントローラーの台数分（押下状態を保存する配列の取得分だけ、）確保
			//複数コントローラー使うなら、→その時に、第一引数にて、コントローラーの番号を指定する
		for (int i = 0; i < MAX_PAD_NUM; i++)
		{
			//前回の押下フラグを前回保存配列の領域に丸っとコピー
			memcpy(&prevControllerState_[i], &controllerState_[i], sizeof(controllerState_[i]));
			
			//XInput専用の、コントローラーの入力を取得する関数にて入力のフラグを取得
				//コントローラー１台に、１つの押下フラグの領域配列を持っているので、
				//第１引数：コントローラー番号
				//第２引数：コントローラーの押下フラグを入れる配列（ボタンごとキーボードのようにフラグが存在している、そのフラグと、関数呼ばれたときのキーコードで論理演算を行う）
			XInputGetState(i, &controllerState_[i]);
		}

		return S_OK;

	}

	/////////////////////////////　キーボード情報取得　//////////////////////////////////

	//引数のキー番号が押されていれば、true,押されていなければfalse
	bool IsKey(int keyCode)
	{
		//キーが押されていたら、trueを返す
		//keyStateは
		//Byte型なので、８ビットのどこかに、→０、１のフラグをUpdatにて押されているかのフラグを立てた
			//8ビットの先頭の1ビットだけに、０、１かのフラグが立っている→その先頭だけ欲しい
			//他7ビットには、何が入っているかがわからない
		//先頭のフラグを登録した位置のビットを取得するにはどうしよう？？？

		/*	そもそもフラグは


			boolで、フラグを立ててもいいが、、、
				→０OR　1なので、→BYTE型で、フラグの型を取得すれば、→BYTEは８ビットなので、０，１のフラグが「８個」登録させることが可能！！

			BYTE flag;
			先頭から7番目のフラグを立てたい→○○○○○○○○

			現在６→0000 0100
			7番目なので、＋２すれば、フラグは立つよね！
			＋２→0000 0110


			もう一回＋２→000 1000


			登録されている値が何であっても、
				→１のフラグでフラグが立つようにしたい
			００００　００００
			００００　００１０
			００００　００１０	→OR　→論理和（＋）（どっちかが、１なら1にする）


			C言語での論理和って？？
			flag = flag | 2;	//2で論理和をする（2桁目で、2桁目が１＝２）
								//１０進との論理和を行ってもらう→１０進を計算で２進との論理和を行う


			〇じゃあ、4桁目で論理和を行いたい
			↓
			〇4桁目を持ってきたい（０桁目から３桁目にある）（１０進の８に当たるところ＝それを８と指定せずに持ってきたい）
			１＜＜３（１（最初の桁）からの３ビット”シフト”）


			〇持ってくる→フラグが立っているなら、
			1 << 3 (持ってくるならさっきの桁目を指定するものは使える)

			０，１＝１を持ってくるなら、そこが１が立っているなら１を返してほしい（両方１なら１を返してほしい）
			だが、そのまま、ANDを使たとしたら、→全体のflagとしての２進と、１＜＜３にて、８（００００　０１００）でのANDになるので、
				//→８のところ＝今回でいうところの３桁目だけが１が立っている
					//→つまり＝flag側の３桁目が１なら帰ってくるのは、０以外の値
						//＝flag側の３桁目が０なら帰ってくるのは、「絶対に０」＝なぜなら、３桁目だけが１とのANDなので、flag側が３桁目が１出ないなら、０以外になるはずがない

			1<<3で、欲しい桁の値を出してもらって

			if(flag & 1<<3 != 0)
			//
		
		
		*/

		//つまり、ここでは、引数のKeyCode番目のByteの中の
		//先頭１ビットがフラグを持っている＝そのフラグの値を持ってきて、
			//キーのBYTEと　先頭１ビット（１０進における１２８、１６進の0x80）での論理積を行う（AND）
			//→先頭１ビットだけが１の値0x80との論理積なので、
			//★keyState側が、先頭１ビットが１でないなら０なら→必ず０になる
			//★逆に、先頭１ビットが１なら　→絶対に０にはならない
		//Updateにて、フラグを立てて、→そのフラグが立っているなら、先頭１ビットは１になっているはず


		//対応キーのBYTEから、先頭１ビット→キーが押されているかの登録場所
		//を確認
		//if (keyState[keyCode] & 128)
		if (keyState[keyCode] & 0x80)
		{
			return true;
		}

		return false;
	}

	//今初めて押されたとき、
	bool IsKeyDown(int keyCode)
	{

		//今は押してて、前回は押してない
		//前回は押していなく、今回は押している
			//配列の中から、IsKeyのようにBYTEの型から、フラグのビットが格納されている位置を指定し、
			//それぞれのキーが押されているフラグを受け取る
			//それを、比較して、前回が０で、今回が初めて１ならtrueを返す（BYTEから、フラグが立っているかの確認を行ったように論理演算にて確認できるはず）
		//
		if (keyState[keyCode] & 0x80
			 && !(prevKeyState[keyCode] & 0x80))
		{
			return true;
		}
		//まず→keyState[keyCode] & 0x80にて、「”今”押されているか、フラグを取得」
		//    →prevKeyState[keyCode] & 0x80 　「”前回”押されているか、フラグを取得」
		//そして→!(prevKeyState[keyCode] & 0x80) 前回は押していない＝前回のフラグが０である。
		//　　　→keyState[keyCode] & 0x80　　　　今回は押している　＝今回ののフラグが１である。
		//それの→＆＆

		/*
		keyState[keyCode] & 0x80
		　＝　前半をIsKey(KeyCode)と呼んでしまえば、
			IsKeyで、押されていることを呼んでいれば、
			→すでに実装されているものを書くならば、
				→すでにある関数を呼んでしまえばよい
						→仕様変更になったときに便利
		
		*/

		return false;
			//前回が１だったら→押し続けられている→false(IsKeyDownの条件には反する)
	
	}

	//キーが離されたとき
	bool IsKeyUp(int keyCode)
	{

		//今は押していなくて、前回は押している
		//前回は押していなく、今回は押している
			//IsKeyDownの逆
		if (!(keyState[keyCode] & 0x80)
			&& prevKeyState[keyCode] & 0x80)
		{
			return true;
		}
	

		return false;


	}

	/////////////////////////////　マウス情報取得　//////////////////////////////////
	//キーボードと同じことを行っている
		//→キーボードがByteで取得していたものを
		//構造体のマウスのボタンが押されたかを保存している要素内から、引数のCodeで判断を行って、
		//→押されているかのBYTEを取得してきて、それを同様に演算→押されているかのフラグが立っているかの確認
		//構造体には、マウスの押され具合とは別に、マウスがどれだけxyz方向に移動されかの移動量が入っている

	//マウスのボタンが押されているか調べる
	bool IsMouseButton(int buttonCode)
	{
		//押してる
		if (mouseState_.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		if (IsMouseButton(buttonCode) && !(prevMouseState_.rgbButtons[buttonCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今放したか調べる
	bool IsMouseButtonUp(int buttonCode)
	{
		//今押してなくて、前回は押してる
		if (!IsMouseButton(buttonCode) && prevMouseState_.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスのデバイスとは、別にマウスの現在地（ウィンドウにおける位置（左上を０，０としたときの））を取得
	//マウスカーソルの位置を取得
	XMVECTOR GetMouseCursorPosition()
	{
		/*
		POINT mousePos;	//LONGのｘ、ｙの構造体でできている
		GetCursorPos(&mousePos);	//現在のマウスの位置をWindow側から、取得する関数を呼ぶ
	
		//マウスの位置をもらい、それをXMVECTORのベクトルに変換し、返す
		XMVECTOR result = XMVectorSet((float)mousePos.x, (float)mousePos.y, 0, 0);
		return result;
		*/
		//このままでは、Windowのスクリーンからの左上を０としたときの
		//ウィンドウにおけるカーソル位置を取得
		//ゲームウィンドウの（０，０）からは取得できない

		//正しいやり方
			//ただ、メンバに保存しているベクトルを返すだけ、
			//→どのように座標をセットしているかは、Setにて、
		return mousePos;
	}
	
	//マウスの現在地をベクトルへ保存
	/*
		★マウスデバイスから、マウスのウィンドウにおける位置が取得できないのを前提として

		★Windowプロシージャーにて、（ゲームのウィンドウに何かが起こったときに呼ばれるやつ）

		マウスが動かされた→という、何かが起こったときの条件を書いて
		→マウスが動かされたら、→ウィンドウ側で、現在のマウスの位置をInputの名前空間の以下のSetを呼んでもらって、引数としてその位置を送ってもらう処理をする

	*/
	void SetMousePosition(int x, int y)
	{
		//引数をマウスの現在地のベクトルへ保存
		mousePos = XMVectorSet((float)x, (float)y, 0 , 0);
	}

	//マウスのデバイスから、どれだけ、xyzにマウスを動かされたかの取得
	//そのフレームでのマウスの移動量を取得
	XMVECTOR GetMouseMove()
	{
			//マウスの現在の状態を保存している構造体内から、
			//マウスの現在のxyz方向にどれだけ移動させたかを取得（構造体に、同様に現在マウスのどのボタンが押されたかも入っている）
		XMVECTOR result = XMVectorSet((float)mouseState_.lX, (float)mouseState_.lY, (float)mouseState_.lZ, 0);
		return result;
	}


	
	/////////////////////////////　コントローラー情報取得　//////////////////////////////////

	//コントローラーのボタンが押されているか調べる
		//キーボードと、同様に、ボタンのコード(どこかでenumで指定されているやつ)を取得し、呼び込み時に、引数として与える
	bool IsPadButton(int buttonCode, int padID)
	{
		//押下フラグを保存しているU short型のフラグと
			//キーコードにて指定したコードとの論理和
			//押されているフラグが立っていたら１が帰ってくるはず
		if (controllerState_[padID].Gamepad.wButtons & buttonCode)
		{
			return true; //押してる
		}
		return false; //押してない
	}

	//コントローラーのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsPadButtonDown(int buttonCode, int padID)
	{
		//今は押してて、前回は押してない
		//前回は押していなくて、今回は押している
			//今回押しているかが欲しいので、IsPadButtonにコードとパッドIDを送ればそのコードのフラグが立っているかが取得できる
			//前回の押下を保存している構造体からフラグとの論理和（！でfalseであることを確認）
		if (IsPadButton(buttonCode, padID) && !(prevControllerState_[padID].Gamepad.wButtons & buttonCode))
		{
			return true;
		}
		return false;
	}

	//コントローラーのボタンを今放したか調べる
	bool IsPadButtonUp(int buttonCode, int padID)
	{
		//今押してなくて、前回は押してる
		//前回押していて、今回離す
		if (!IsPadButton(buttonCode, padID) && prevControllerState_[padID].Gamepad.wButtons & buttonCode)
		{
			return true;
		}
		return false;
	}

	//アナログスティックのデッドゾーンの計算
	//トリガーのデッドゾーンの計算
	/*
		デッドゾーン
			：　傾きを０（傾けていない）〜１（最大に傾けている）とで分けたとき
				０〜１の範囲で、「ここからここまでは、傾けたとみなしません」という領域

		標準でのデッドゾーンの領域
			：０〜１の２０％はデッドゾーンとする
			　（０〜０．２　まではデッドゾーンとなり、その領域は、傾けた値として返さない０を返す。
				移動に用いるならば、移動していないということになる）
	
	*/
		//raw 引数により、スティックの傾きの量　＝　−３２７６７　〜　３２７６７
	//デッドゾーンにより、デッドゾーンに満たない値は０、デッドゾーンを超える値はデッドゾーンを０として、MAXの最大値を１とする（スティックならデッドゾーンを０から１（３２７６７まで））
		//max その傾きなどにおける最大の量（最大に傾けたときの値）
		//deadZone その傾きにおける、デッドゾーンまでの値（５０００となっていたら、１〜５０００は入力されたとみなさない＝０を返す）
	float GetAnalogValue(int raw, int max, int deadZone)
	{
		/**以下のコメントは、スティックの「傾き」が欲しい時に、関数が呼ばれたとき**************************/
		/***本来は、スティック、トリガー、両方の場合に呼ばれる****/


		//傾き、押し具合の値を入れる（コントローラーのそのままの値を入れる（構造体に保存している要素から取得したそのままの傾きなど））
		float result = (float)raw;

		//０より大きい
		if (result > 0)
		{
			//デッドゾーン
			if (result < deadZone)
			{
				//コントローラーの傾きなどの状態の値が、
				//デッドゾーンを超えないとき
				//　＝　傾きとみなさない（スティックなら、スティックを動かしていない判定）
				//　＝　０
				result = 0;
			}
			else
			{
				//コントローラーの傾きなどの状態の値が、
				//デッドゾーンを超えたとき
				//　＝　傾きとみなす（スティックなら、スティックを動かした判定）
				//　＝　傾きの大きさを代入（０〜１の値で返す（もともと、if文で０より大きいときに呼ぶものなので、０〜１の範囲でしか答えは出ない（割合を出しているので、）））
				result = (result - deadZone) / (max - deadZone);
					//傾きの値を返すといっても。。。。。。
					/*
						０〜１で傾きを表現したとき

						デッドゾーンによって、消されるのは０　　　〜０．２
						デッドゾーンによって、生き残るのは０．２１〜１
					
						であれば、resultのそのままの値を仮に０〜１のわかりやすい値で表現したとしても、
						移動になる値は０．２１〜の値になってしまう

						〇欲しいのは、入力初めで０〜の値が欲しい（０．２０が０になるようにして値をとりたい）

						〇解決方法
							：result - deadZone (傾きそのまま - デッドゾーンの値)　	
													＝　デッドゾーンの値を除いた傾きの量（単純にdeadZoneの値分はそもそもなかったとして、0.21 - 0.2 = 0.01→あくまで傾きは0.01だ）
							　max - deadZone (傾き最大 - デッドゾーン値)
													＝　デッドゾーンの値の抜いたときの最大量（deadZone量分は初めからなかった時の最大値）

							：部分 / 全体　＝　割合（これを傾きの０〜１の値とする）
							：deadを抜いた全体においてdeadを抜いた部分はどれだけ占めているのか
							　を取得


					*/
			}
		}
		//０より小さい
		else
		{
			//処理は、０より大きい場合と同様
			//結果の値が、＋か、-かの違い

			//デッドゾーン
			if (result > -deadZone)
			{
				result = 0;
			}
			else
			{
				//−１〜０
				result = (result + deadZone) / (max - deadZone);
			}
		}
		//初めのifの段階で、０ならば、傾きがされていないということ

		return result;
	}


	//左スティックの傾きを取得
	XMVECTOR GetPadStickL(int padID)
	{
		//スティックの傾き具合を値として取得
		//X方向への傾き具合
		float x = GetAnalogValue(controllerState_[padID].Gamepad.sThumbLX, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
			//傾き具合は、−３２７６７　〜　３２７６７の値から取得できる
			//・右への傾きいっぱいに傾けたなら３２７６７がsThumbLX、に入っている
			//・左への傾きいっぱいに傾けたならー３２７６７がsThumbLX　に入る
			//★その値を、決められたデッドゾーン（ここまでの傾き具合は入力とみなしませんよ）とともに関数へ
			//★スティックの傾きがデッドゾーン以下ならそれは、値０として（傾けていない）として帰ってくる

		//Y方向への傾き具合
		float y = GetAnalogValue(controllerState_[padID].Gamepad.sThumbLY, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		
		//傾き具合をxyのベクトルへ入れて返す
			//戻り値側で、傾きの値による、移動量の計算などに使用できる
		return XMVectorSet(x, y, 0, 0);
	}

	//右スティックの傾きを取得
		//左スティックと同様に計算を行う
		//引数として送る、傾き具合を保存している構造体の要素がLeftから、Rightになっただけ。
	//右と、左で関数が分かれているほうが呼び込み側も楽かも
	XMVECTOR GetPadStickR(int padID)
	{
		float x = GetAnalogValue(controllerState_[padID].Gamepad.sThumbRX, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		float y = GetAnalogValue(controllerState_[padID].Gamepad.sThumbRY, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		return XMVectorSet(x, y, 0, 0);
	}


	//左トリガーの押し込み具合を取得
	float GetPadTrrigerL(int padID)
	{
		return GetAnalogValue(controllerState_[padID].Gamepad.bLeftTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
			//スティックと同様の関数へ送り
			//そのボタンの押し込み具合の値と、具合の最大値、デッド値とを渡す
			//★関数先で、押し込み具合と、最大、デッドから→ー１〜１の値を返してもらう
			//★−１から１の値で戻り値側で、処理を行なわせる
	}

	//右トリガーの押し込み具合を取得
		//左トリガーと同様
		//LeftからRightに変えた
	float GetPadTrrigerR(int padID)
	{
		return GetAnalogValue(controllerState_[padID].Gamepad.bRightTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
	}

	//振動させる
		//左モーターの振動値、右モーターの振動値をもらい、その強さ分でモーターを動かす
		//！注意！長時間のモーターによる振動は人体に悪影響を与えることがある（使用を終えたら、強さを０にする）
	void SetPadVibration(int l, int r, int padID)
	{
		//バイブレーションの変数
		XINPUT_VIBRATION vibration;
		//メモリを初期化
		ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
		//モーターの強さを引数の値で調整
		vibration.wLeftMotorSpeed = l; // 左モーターの強さ
		vibration.wRightMotorSpeed = r;// 右モーターの強さ
		//調整した値をセットして、バイブレーションを再生させる
		XInputSetState(padID, &vibration);
	}


	//解放処理
	void Release()
	{
		//宣言と逆に解放

		SAFE_RELEASE(pMouseDevice_);
		SAFE_RELEASE(pKeyDevice);

		//ID3D〜は、RELEASEにて、解放を行わなければいけなかった
		//LP8〜もRELEASEにて解放。→たどると→ID8~型のポインタを別名で、ポインタである→なので、RELEASEにて解放することになる
		SAFE_RELEASE(pDInput_);
	}
}