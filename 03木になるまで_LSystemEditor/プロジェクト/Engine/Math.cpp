#include "Math.h"

//行列式
float Math::Det(XMVECTOR a, XMVECTOR b, XMVECTOR c)
{
	//DirectXにも、専用の関数がある
	//XMMatrixDetarminentみたいな
		//だが、帰り地がベクトルだったりするので、
		//計算式を書いて対応
	
	return	(a.vecX * b.vecY * c.vecZ) +
		(a.vecY * b.vecZ * c.vecX) +
		(a.vecZ * b.vecX * c.vecY) -
		(a.vecX * b.vecZ * c.vecY) -
		(a.vecY * b.vecX * c.vecZ) -
		(a.vecZ * b.vecY * c.vecX);
}

//レイとの衝突判定計算
//線分と三角形の交差
//https://qiita.com/edo_m18/items/2bd885b13bd74803a368
bool Math::Intersect(XMVECTOR origin, XMVECTOR ray, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2 , float* t)
{
	//線分と三角形の交差の計算のため必要な
	//U,V,Tの値を求める

	//https://sites.google.com/view/jc21dx11/%E3%83%AC%E3%82%A4%E3%82%AD%E3%83%A3%E3%82%B9%E3%83%88/%E7%B7%9A%E5%88%86%E3%81%A8%E4%B8%89%E8%A7%92%E5%BD%A2%E3%81%AE%E4%BA%A4%E5%B7%AE%E5%88%A4%E5%AE%9A
	//線分と三角形の交差は
	//edge1*u + edge2*v - ray*t = origin - v0
	//で求めることができると、計算で求めることができた。

	//三角形を作るベクトル３つ
	//V0,V1,V2が存在している
	//V０,V1,V2は、それぞれ、三角形を表す、それぞれの頂点である。


	//上記のedge1から始まる式は、
	//ax + by + cz = dと当てはめることができる
	// a = edge1
	// x = u
	// b = edge2
	// y = v
	// c = dir
	// z = t
	// d = start - v0

	//クラメールの公式では、
	//各abcdのベクトルを行列式に渡して出た値を
	//割り算して、ｘｙｚの値を出すことが可能である。（なぜそうなるのかは今考えずに）


	//であれば、

	//クラメールの公式に当てはめて、
	//XYZは、UVTと当てはめることが可能なので
	//UVTを求めることができるはず。

	/*
		行列式を使い値を返してもらう関数も作ってあるので、
	
		各ベクトル（abcd）のベクトルをその関数に渡し、
		その返り値で、
		ｘｙｚを求める公式に当てはめて、　ｘｙｚ（ここにおけるuvt）を求める
	
	*/


	//欲しい情報は　ｖ０からｖ１までのedge1
					//ｖ０からｖ２までのedge2
	//そのedgeはベクトルの引き算で求められる　(ベクトルの引き算をすれば、ベクトルからベクトルまでの差ベクトルを出せる)
	//edge1 (a)
	XMVECTOR edge1 = v1 - v0;
	//edge2 (b)
	XMVECTOR edge2 = v2 - v0;
	//ray   (c)

	//origin - v0 (d)
	XMVECTOR d = origin - v0;

	/*
		x + y + z = 9
		2x + 3y - 2Z = 5
		3x - y + z = 7

		からｘｙｚを求めるときには、
		a =	Det(１．x,２．x,３．x)	とベクトルを作成していた
	*/
	//ベクトルの場合は、
	//すでにベクトルを持っているので、
	//それぞれのX値をaに渡してベクトルとし、行列式によって、値を出す
		//それをY,Z分

	//そして、dになる、それぞれのxyzの解となる部分を集めたベクトルは
		//= origin - v0

	//クラメールの公式に当てはめるための
	//abcdベクトルを使用して、行列式に当てはめた返り値を求める
	//これらの返り値の値を使用して、ｘｙｚ（uvt）を求める
	
	//XYZを求める際に分母となる値
	float denom = Det(edge1, edge2, -ray);

	//平行の場合だと、
	//レイと三角形が当たらない。
	//そして、この平行の場合だと、denomの値はマイナスになるらしい
	if (denom < 0)
	{
		//レイと三角形の面が平行だったら、
		//当たっていない
		return false;
	}

	//Xを求める際の分子
	float numX = Det(d, edge2, -ray);
	//Yを求める際の分子
	float numY = Det(edge1, d, -ray);
	//Zを求める際の分子
	float numZ = Det(edge1, edge2, d);

	//行列式の返り値による、
	//クラメールの公式に当てはめた
	//ｘｙｚ（uvt）の値を求める(分子/分母)
	//U
	float u = numX / denom;
	//V
	float v = numY / denom;
	//T
	(*t) = numZ / denom;
		//衝突点までの距離（奥行き）を入れる（これが、レイ発射位置から、レイ衝突点までの距離が入る）


	//衝突判定
	/*
		線分と三角形の交差において、
		//edge1*u + edge2*v - ray*t = origin - v0　の式を求めたが
		
		線分と三角形の交点を仮にPと置いたとき、
		Pは、
		V0からV1への線分中にあるU(ｖ０を０、ｖ１を１という割合で求めたときのU位置は少数になる)（v0からUまでのベクトル）という点、
		V0からV2への線分中にあるV（v0からVまでのベクトル）という点、

		この２つのベクトルの足し算で出したベクトルが
		Pである。

		
		U　　　P	
		↑　／
		↑／
		└→→V


		となると、UVが、三角形の辺であるならば、
		UVTという点は以下でないといけない。

		上の式からuとvとtの値を求め

		0 ≦ u ≦ 1

		0 ≦ v ≦ 1

		0 ≦ u+v ≦ 1

		t  ≧ 0

		となっていれば、線分と三角形はぶつかってる（Pは三角形の内部にある）と言える。
		（写真メモを確認、１を超えたときに衝突しない）

	*/

	//必要なくなる条件の削除
	//※なくしても絶対に、誤作動は起きないとわかっているうえで
	//＆＆は、上から順番に条件が流れていくので、（見ていくので、）
	//※上から順番でなくても、＆＆になるので、どこから見ても結果は同じになる。
		//順番は関係ない。（一個でもfalseなら＆＆なら通らない。）

	//�@U、Vが０以上であれば、（ U >= 0 (YES), V >= 0 (YES)ときたら）
		//U＋Vが０より小さくなることはない。（(U+V) >= 0 は必ずYESとなるのはわかっている）
	//�AU＋Vが１より大きいという条件があるなら
		//U、Vが１より小さいという条件いらない。（→Uが１よりおおきかったら、　U+Vの条件false, Vが１より大きかったら、　U+Vの条件false
												//であれば、どちらにしても、Uが１より小さいと聞くのは、意味がない）
	if (u >= 0 && 
		v >= 0 && 
		(u + v) <= 1 && 
		t >= 0)
	{

		return true;
	}


	

	return false;


	//発射位置から、当たったところまでの距離
	//今回における奥行き　T　がそれにあたる
		//これも返したいので、ポインタで渡すように
}


//UVTを求めて、
//UVTそれぞれに判断を掛けることになるが、

//Uを求めて、
//その時点で、Uに判断を掛けてしまって、当たっていなかったら当たってないと返してしまったほうがいいし
//（ハイポリゴン）

//だが、
//if分多くしてしまうので、
//重くなるかも
//（ローポリゴン）


//どっちとも言えないかも
