#include "Direct2D.h"

//Direct3Dの所有しているスワップチェインに、２Dのデータを渡せるようにする
	//上記のようにすることで、
	//Direct3Dにて描画されたバッファかつ、Direct2Dにて描画するバッファを共通して扱う、（おそらく。
		//hr = Direct3D::GetSwapChain()->GetBuffer(0, IID_PPV_ARGS(&pDXGISurface));ここで、第一引数にてバッファの番号を指定していて、　その番号が、3Dと同様のため、そのように考えられる）
	//３D描画、かつ、２D描画を行なわせる
#include "Direct3D.h"



//メンバとして外に見せたくないものを宣言
	//ヘッダには、逆に見せてもよいものを宣言
namespace Direct2D
{
	/*Direct2Dを描画するために必要*/
	//ID2D1Factory* pD2DFactory = nullptr;
	IDWriteFactory* pDWriteFactory = nullptr;

	//追加
	ID2D1Factory1* pD2DFactory1 = nullptr;
	ID2D1DeviceContext* pD2DDeviceContext = nullptr;
	ID2D1Device* pD2DDevice = nullptr;
	ID2D1Bitmap1* pBitmap = nullptr;


	IDWriteTextFormat* pTextFormat = nullptr;
	//ID2D1RenderTarget* pRT = nullptr;
	ID2D1SolidColorBrush* pSolidBrush = nullptr;
	//IDXGISurface* pDXGISurface = nullptr;


	int scrWidth = 0;
	int scrHeight = 0;


}



//Direct2Dの初期化
	//Direct3Dの初期化が終わり、
	//スワップチェインが作成されている前提
HRESULT Direct2D::Initialize()
{
	////Direct２Dの初期化
	//if (FAILED(Init2D()))
	//{
	//	//エラー処理
	//		//エラー原因の　メッセージボックスを表示
	//	MessageBox(nullptr, "Direct2Dの初期化失敗", "エラー", MB_OK);
	//	return E_FAIL;	//エラーを返す

	//};

	//スクリーンサイズの取得
	scrWidth = Direct3D::scrWidth;
	scrHeight = Direct3D::scrHeight;




	HRESULT hr;
	// Direct2D,DirectWriteの初期化
	/*hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &pD2DFactory);
	if (FAILED(hr))
		return hr;
*/

	//hr = Direct3D::GetSwapChain1()->GetBuffer(0, IID_PPV_ARGS(&pDXGISurface));

	//hr = Direct3D::GetSwapChain()->GetBuffer(0, IID_PPV_ARGS(&pDXGISurface));
	/*if (FAILED(hr))
		return hr;*/

	auto propsBit = D2D1::BitmapProperties1(
		D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
		D2D1::PixelFormat(
			DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE));

	
	pD2DDeviceContext->CreateBitmapFromDxgiSurface(Direct3D::GetIDXGISurface(),
		propsBit,
		&pBitmap);
	pD2DDeviceContext->SetTarget(pBitmap);


	FLOAT dpiX;
	FLOAT dpiY;
	//pD2DFactory1->GetDesktopDpi(&dpiX, &dpiY);
	//VisualStudio2019において、上記にてDPIを取得することはできないため、以下の関数で取得する
		//https://docs.microsoft.com/en-us/answers/questions/170411/creating-a-simple-direct2d-application-39id2d1fact.html
	dpiX = (FLOAT)GetDpiForWindow(GetDesktopWindow());
	dpiY = dpiX;
	pD2DDeviceContext->SetDpi(dpiX, dpiY);


	D2D1_RENDER_TARGET_PROPERTIES props =
		D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT,
			D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), dpiX, dpiY);

	////Direct3D.cppにおける　CreateDevice~　の第４引数へ、　
	//	//Direct2Dを使用するというフラグを立てないと、
	////ここでRenderTargetViewに　ポインタが入らない
	//hr = pD2DFactory->CreateDxgiSurfaceRenderTarget(Direct3D::GetIDXGISurface(),
	//	&props, &pRT);
	//if (FAILED(hr))
	//	return hr;



	hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown * *>(&pDWriteFactory));
	if (FAILED(hr))
		return hr;

	//関数CreateTextFormat()
	//第1引数：フォント名（L"メイリオ", L"Arial", L"Meiryo UI"等）
	//第2引数：フォントコレクション（nullptr）
	//第3引数：フォントの太さ（DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_WEIGHT_BOLD等）
	//第4引数：フォントスタイル（DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STYLE_OBLIQUE, DWRITE_FONT_STYLE_ITALIC）
	//第5引数：フォントの幅（DWRITE_FONT_STRETCH_NORMAL,DWRITE_FONT_STRETCH_EXTRA_EXPANDED等）
	//第6引数：フォントサイズ（20, 30等）
	//第7引数：ロケール名（L""）
	//第8引数：テキストフォーマット（&g_pTextFormat）
	hr = pDWriteFactory->CreateTextFormat(L"メイリオ", nullptr, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 20, L"", &pTextFormat);
	if (FAILED(hr))
		return hr;

	//関数SetTextAlignment()
	//第1引数：テキストの配置（DWRITE_TEXT_ALIGNMENT_LEADING：前, DWRITE_TEXT_ALIGNMENT_TRAILING：後, DWRITE_TEXT_ALIGNMENT_CENTER：中央）
	hr = pTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
	if (FAILED(hr))
		return hr;

	//関数CreateSolidColorBrush()
	//第1引数：フォント色（D2D1::ColorF(D2D1::ColorF::Black)：黒, D2D1::ColorF(D2D1::ColorF(0.0f, 0.2f, 0.9f, 1.0f))：RGBA指定）
	hr = pD2DDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), &pSolidBrush);
	if (FAILED(hr))
		return hr;

	
	



	return hr;


}


//Direct2Dの描画開始
	//Direct３DのBeginDrawの後に呼び込む
void Direct2D::BeginDrawDirect2D()
{

	/*テキスト描画*/
	//Direct2D , DirectWrite
		//２Dのレンダーターゲットビューの書き込み準備
	

//	pRT->BeginDraw();

	pD2DDeviceContext->BeginDraw();

}



//Direct2Dの描画終了
	//Direct３DのEndDrawの前に呼び込む
	//画面を切り替えてしまう前に呼び込む
void Direct2D::EndDrawDirect2D()
{
	//{
	//	//テストテキスト描画
	//	//--------------------------★変更↓--------------------------
	//	//画像とテキストの描画
	//	WCHAR wcText1[256] = { 0 };
	//	WCHAR wcText2[256] = { 0 };

	//	swprintf(wcText1, 256, L"%lf", 60.0);
	//	//wcharを　第４引数に入れることで、　パーミッション？に合わせることが可能
	//	swprintf(wcText2, 256, L"%ls", L"こんにちは");


	//	//関数DrawBitmap()
	//	//第1引数：レンダリングするビットマップ（pD2DBitmap）
	//	//第2引数：ビットマップの位置の座標（D2D1::RectF(左, 上, 右, 下)）
	//		//（テキスト原点ｘ、テキスト原点ｙ、　テキストの枠のサイズWidth , テキストの枠のサイズHeight）
	//	//第3引数：不透明度（0.0f〜1.0f）
	//	//第4引数：ビットマップが拡大縮小または回転される場合に使用する補間モード（D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR：ドット絵風[ギザギザ]
	//	//                                                                         D2D1_BITMAP_INTERPOLATION_MODE_LINEAR：写真風[なめらか]）
	//	//第5引数：トリミング（D2D1::RectF(左, 上, 右, 下), nullptr：イメージ全体の場合）
	//	//pRT->DrawBitmap(pD2DBitmap, D2D1::RectF(0, 0, 128, 128), 1.0f, D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR, nullptr);
	//	pSolidBrush->SetColor(D2D1::ColorF(D2D1::ColorF::Black));
	//	pRT->DrawText(wcText1, ARRAYSIZE(wcText1) - 1, pTextFormat, D2D1::RectF(0, 0, scrWidth, scrHeight), pSolidBrush, D2D1_DRAW_TEXT_OPTIONS_NONE);
	//	
	//	
	//	//ブラシの色を変える
	//	pSolidBrush->SetColor(D2D1::ColorF(D2D1::ColorF::Violet));
	//	pRT->DrawText(wcText2, ARRAYSIZE(wcText2) - 1, pTextFormat, D2D1::RectF(300, 300, scrWidth, scrHeight), pSolidBrush, D2D1_DRAW_TEXT_OPTIONS_NONE);


	//}


	//描画終了
//	pRT->EndDraw();


	pD2DDeviceContext->EndDraw();

}

void Direct2D::Release()
{

	//SAFE_RELEASE(pDXGISurface);
	SAFE_RELEASE(pSolidBrush);
	//SAFE_RELEASE(pRT);
	SAFE_RELEASE(pTextFormat);
	SAFE_RELEASE(pDWriteFactory);
	//SAFE_RELEASE(pD2DFactory);
	SAFE_RELEASE(pBitmap);
	SAFE_RELEASE(pD2DFactory1);
	SAFE_RELEASE(pD2DDeviceContext);
	SAFE_RELEASE(pD2DDevice);

}

ID2D1SolidColorBrush * Direct2D::GetBrush()
{
	return pSolidBrush;
}

//ID2D1RenderTarget * Direct2D::GetRenderTargetView()
//{
//	return pRT;
//}

ID2D1DeviceContext * Direct2D::GetDeviceContext()
{
	return pD2DDeviceContext;
}

IDWriteTextFormat * Direct2D::GetTextFormat()
{
	return pTextFormat;
}


void Direct2D::CreateDevice()
{

	pD2DFactory1->CreateDevice(Direct3D::GetIDXGIDevice(),
		&pD2DDevice);
}

void Direct2D::CreateFactory()
{
	D2D1_FACTORY_OPTIONS fo = {};
#ifdef _DEBUG
	fo.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
#endif

	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED,
		fo,
		&pD2DFactory1);

}

void Direct2D::CreateDeviceContext()
{
	pD2DDevice->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE,
		&pD2DDeviceContext);

}
