

#pragma once

//Quad = ３D
///ポリゴン表示
//ポリゴンは複数呼ばれることになるので、
	//→名前空間ではなく、クラスとして持っておく

#include <DirectXMath.h>
#include "Direct3D.h"
#include "Texture.h"
#include "Transform.h"	//Transformクラスの、移動、回転、拡大行列を使用するため
using namespace DirectX;

//頂点情報（ｘ、ｙ、ｚ）を使う
//それは、Vectorにて、欲しい、

//→Vectorは、今まで、XMVECTORにて、取得していた、→だが、それは、<DirectXMath.h>の中の名前空間である
	//→DirectXの中のXMVECTORを使う

//→となると、DirectX::XMVECTORと本来書くべき
//だが、DirectXのXMVECTORとわかっているならば、省略したいと思う
	//その時に使うのが、using namespace std;　のような存在
	//これ以降のソース、このソース内では、std::を省略できるようにしますよ

//XMVECTORが存在する（正しくはXMVECTORの構造体が存在する、名前空間）ヘッダをインクルードして
	//→その中の名前空間のDirectXを、以降では省略できます（→DirectXのXMVECTORと、一つしかないので、→DirectX::というのは、省略しても別にかまわない（ほかのXMVECTORとでてこないので　））

//★→だが、using として、名前空間の指定を自ら書かないのであれば、→名前空間として定義している意味なくならないか？
	//→となりえるので、一つ一つDirectX::と書く人も、グループもある（会社次第）


//コンスタントバッファー


#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}
//解放


//class Transform;

class Quad
{
protected : 
		//継承先で使用
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;	//コンスタントバッファに送る、行列の情報を取得しておく
		XMMATRIX	matW;
	};
	//シェーダーにて、受け取るもののために、構造体で取得
		//現在は、一つしかもっていないが、

	//頂点情報
	//頂点に、位置と、UVがひつようなので、　構造体で取得

	struct VERTEX
	{
		XMVECTOR position;
		XMVECTOR uv;

		XMVECTOR normal;	//法線情報
	};



	ID3D11Buffer *pVertexBuffer_;	//頂点バッファ
	ID3D11Buffer *pIndexBuffer_;	//インデックスバッファ
	ID3D11Buffer *pConstantBuffer_;	//コンスタントバッファ

		//ポインタのアスタリスクの場所
		//Class *class1;
		//Class* class2;		//どちらも同じ、だが、どっちかにするなら、書き方は共通した書き方をする
	Texture*	pTexture_;


	int vertex_;	//	頂点数
					//継承先ごとで、違う頂点数
	int *index;

public:

	Quad();

	~Quad();

	//継承先でオーバーライドする関数
	HRESULT Initialize();//Quadの初期化（モデル描画のための初期化）
	virtual HRESULT InitVertex() = 0;//頂点情報の初期化

	virtual HRESULT TextureLoad();

	//オーバーライドの必要はないため、Virtualなし
	//HRESULT Draw(XMMATRIX& worldMatrix);	//モデル描画
	HRESULT Draw(Transform& transform);
	void Release();
};

