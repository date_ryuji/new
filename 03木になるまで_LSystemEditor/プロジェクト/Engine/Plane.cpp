#include "Plane.h"
#include "Texture.h"
#include "Camera.h"
#include "../Shader/BaseShader.h"



Plane::Plane() :
PolygonGroup::PolygonGroup(),

pVertices_(nullptr),
pVertexBuffer_(nullptr),
pIndex_(nullptr),
pIndexBuffer_(nullptr),
pConstantBuffer_(nullptr),
vertexCount_(0), indexCountEachMaterial_(0), polygonCount_(0)
{
}

Plane::~Plane()
{
}

HRESULT Plane::Load(std::string textureFileName, SHADER_TYPE thisShader)
{

	thisShader_ = thisShader;


	if (FAILED(InitVertex()))
	{
		MessageBox(nullptr, "頂点情報の生成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	if (FAILED(CreateVertexBuffer()))
	{
		MessageBox(nullptr, "頂点バッファーの生成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	if (FAILED(InitIndex()))
	{
		MessageBox(nullptr, "インデックス情報の生成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	if (FAILED(CreateIndexBuffer()))
	{
		MessageBox(nullptr, "インデックスバッファーの生成失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	if (FAILED(CreateConstantBuffer()))
	{
		MessageBox(nullptr, "コンスタントバッファーの生成失敗", "エラー", MB_OK);
		return E_FAIL;
	}






	//テクスチャのロード
	if (FAILED(LoadTexture(textureFileName)))
	{
		MessageBox(nullptr, "テクスチャの生成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};

	return S_OK;
}

HRESULT Plane::LoadTexture(std::string fileName)
{
	//ファイルのロード
	//テクスチャのロード
	pTexture_ = new Texture;
	if (FAILED(pTexture_->Load(fileName)))
	{
		MessageBox(nullptr, "ファイルの読み込みを失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	//ロード時
	return S_OK;

}

HRESULT Plane::Draw(Transform & transform)
{

	//シェーダーを選択
	Direct3D::SetShaderBundle(thisShader_);

	//シェーダーファイルに渡すための情報をまとめる
	CONSTANT_BUFFER cb;
	cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix()* Camera::GetViewMatrix() * Camera::GetProjectionMatrix());

	////ワールド行列
	//	//法線の回転のために
	//	//ライトは固定なのに、、陰も一緒に回っていた。
	//		//それを回転状況を考慮して、法線を回転させた。
	//cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));	//



	////ワールド行列を入れる
	//	//ワールド行列を渡して、
	//	//頂点からカメラへの向きというのを取得するために、頂点に掛けてやらないといけない（移動、回転、拡大を込みしたワールド位置にするために）
	//cb.matWorld = XMMatrixTranspose(transform.GetWorldMatrix());


	////カメラの位置を入れる
	//				//XMVECTOR型をFLOAT4型に入れることはできない。
	//				//変換する関数があったが、忘れてしまったので、1つずつFLOAT４型に変換して代入
	//cb.camPos = XMFLOAT4(Camera::position_.vecX, Camera::position_.vecY, Camera::position_.vecZ, 0);


	//テクスチャあり
	if (pTexture_ != nullptr)
	{
		//nullptrでない　＝　テクスチャが存在する
		//「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
			//サンプラーは、どこに貼り付けますか？などの情報
		//hlsl : g_sampler(s0)
		ID3D11SamplerState* pSampler = pTexture_->GetSampler();	//テクスチャクラスから、サンプラーを取得
		//ここにおける第一引数が何番目のサンプラーなどか、
		Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);
		//シェーダーへの橋渡しのビューを取得
		//hlsl : g_texture(t0)
		ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
			//シェーダーに作った変数に渡している
		Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);


		cb.isTexture = TRUE;	//コンスタントバッファのテクスチャ有無にtrueを立てる
	}
	//テクスチャなし
	else
	{
		//nullptrである　＝　テクスチャが存在しない
		//　＝　マテリアルそのものの色を使用しないといけない
		cb.isTexture = FALSE;

	}


	//シェーダーのコンスタントバッファ１のマップ
	if (FAILED(Direct3D::GetShaderClass(thisShader_)->MapShaderConstantBuffer1()))
	{
		MessageBox(nullptr, "コンスタントバッファMap失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};


	//自身の使用する
	//シェーダー独自の
	//コンスタンバッファや、テクスチャの受け渡しを行う
	Direct3D::GetShaderClass(thisShader_)->ShaderConstantBuffer1View();


	//シェーダーのコンスタントバッファ１のアンマップ
	if (FAILED(Direct3D::GetShaderClass(thisShader_)->UnMapShaderConstantBuffer1()))
	{
		MessageBox(nullptr, "コンスタントバッファUnMap失敗", "エラー", MB_YESNO);
		return E_FAIL;

	};



	D3D11_MAPPED_SUBRESOURCE pdata;
	//シェーダーなどのGPUでしか、触れることのできないものに
	//CPUから触れる準備
	if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))
	{
		MessageBox(nullptr, "コンスタントバッファのマップの書き込み失敗", "エラー", MB_YESNO);

		return E_FAIL;	//エラーですと返す。
	}
	//メモリにコピー
	memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	// データを値を送る


	{
		//頂点バッファをセット（頂点を持っておくための領域（確保場所））
			//複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		UINT stride = sizeof(Plane::VERTEX);	//1頂点のサイズを指定する、→位置のみであれば、XMVECTORの構造体であったが、
									//位置とUVで構造体をとるので、その構造体のサイズで
		UINT offset = 0;
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

		// インデックスバッファーをセット
		stride = sizeof(int);
		offset = 0;
		Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

		//コンスタントバッファをセット
		Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
		Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用

	}

	Direct3D::pContext->Unmap(pConstantBuffer_, 0); 	//再開
						//書き込んだので、最後に、Unmapにて、　閉じる
						//コンスタントバッファに送る


	//描画
	//★頂点の個数を表す、→ここの頂点の数を、最終的な三角形の頂点の数
	//★つまり、インデックス情報の頂点の数
	Direct3D::pContext->DrawIndexed(indexCountEachMaterial_, 0, 0);




	return S_OK;

}

void Plane::Release()
{
	//ポインタ宣言順と逆に解放
	if (pTexture_ != nullptr)
	{
		pTexture_->Release();
	}
	SAFE_DELETE(pTexture_);
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_RELEASE(pVertexBuffer_);
	SAFE_DELETE_ARRAY(pVertices_);
}


HRESULT Plane::InitVertex()
{
	static constexpr unsigned int VERTEX_COUNT = 4;

	vertexCount_ = VERTEX_COUNT;


	// 頂点情報
	Plane::VERTEX vertices[VERTEX_COUNT] =

	{

		{XMVectorSet(-1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 0.5f, 0.0f, 0.0f)},	// 四角形の頂点（左上）

		{XMVectorSet(1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 0.5f, 0.0f, 0.0f)},	// 四角形の頂点（右上）

		{XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f)},	// 四角形の頂点（右下）

		{XMVectorSet(-1.0f, -1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)},	// 四角形の頂点（左下）		

	};

	//メンバの頂点情報を動的確保
	pVertices_ = new VERTEX[VERTEX_COUNT];



	//メンバの頂点情報にコピーする
	memcpy(pVertices_, vertices, sizeof(VERTEX) * VERTEX_COUNT);

	return S_OK;
}

HRESULT Plane::InitIndex()
{
	//インデックス数
	//頂点数 - 1 * ポリゴン数

	polygonCount_ = 2;
	//indexCountEachMaterial_ = (vertexCount_ - 1) * polygonCount_;
	indexCountEachMaterial_ = 6;


	pIndex_ = new int[indexCountEachMaterial_] {0, 1, 3, 1, 2, 3};



	return S_OK;
}

HRESULT Plane::CreateVertexBuffer()
{
	//verticesはポインタで動的配列になったので、sizeof(vertices)では正しくサイズを取得することができない。
//	この場合はVERTEX型のサイズ*頂点数を指定すればよい。
	//頂点の数はわかっているので、→vertexの型でいくらのサイズをbyteで使うのか、＊頂点数→最終的に使用する全体サイズ

// 頂点データ用バッファの設定
//頂点情報へ代入
	D3D11_BUFFER_DESC bd_vertex;

	//頂点バッファの使用するバイト長を登録
	//頂点情報のサイズの登録
	//型名＊頂点数（頂点数だけ、VERTEXを取得している）（VERTEXの情報が何個必要なのか）
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	bd_vertex.CPUAccessFlags = 0;

	bd_vertex.MiscFlags = 0;

	bd_vertex.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA data_vertex;

	data_vertex.pSysMem = pVertices_;


	//Direct３DにおけるpDeviceにそれぞれのバッファを作成する
	//頂点バッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		MessageBox(nullptr, "頂点バッファの作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};

	return S_OK;

}

HRESULT Plane::CreateIndexBuffer()
{
	D3D11_BUFFER_DESC   bd;

	bd.Usage = D3D11_USAGE_DEFAULT;

	bd.ByteWidth = sizeof(int) * indexCountEachMaterial_;

	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

	bd.CPUAccessFlags = 0;

	bd.MiscFlags = 0;


	//バッファーへのアクセスポインタ
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex_;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	//インデックスバッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_YESNO);

		return E_FAIL;
	}
	return S_OK;

}

HRESULT Plane::CreateConstantBuffer()
{
	//コンスタントバッファ情報構造体
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(Plane::CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//成功したかの判断
	//失敗したら、呼び出し元の関数にエラーを返す
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		//エラーを、どこでしたのかを知らせるためのメッセージボックス
		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	}
	return S_OK;

}



int Plane::GetVertexCount()
{
	return vertexCount_;
}

int Plane::GetPolygonCount()
{
	return polygonCount_;
}

int Plane::GetIndexCount()
{
	return indexCountEachMaterial_;
}

Plane::VERTEX * Plane::GetVertexes()
{
	return pVertices_;
}

int * Plane::GetIndexes()
{
	return pIndex_;
}

Plane::VERTEX::VERTEX() :
	position(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	uv(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}

//頂点情報の初期化の時、
//専用のコンストラクタ（要素のベクトル3つを初期値としてセットして宣言する形）がないと、
	//宣言できないので専用のコンストラクタをセットする
Plane::VERTEX::VERTEX(XMVECTOR setPos, XMVECTOR setUv):
	position(setPos),
	uv(setUv)
{
}
