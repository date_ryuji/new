#include "BranchesMakeTree.h"
#include "Texture.h"
#include "Branch.h"
#include "Plane.h"
#include "Camera.h"
#include "../Shader/BaseShader.h"




BranchesMakeTree::BranchesMakeTree() :
	PolygonGroup::PolygonGroup(),

	pVertices_(nullptr),
	pVertexBuffer_(nullptr),
	pIndex_(nullptr),
	pIndexBuffer_(nullptr),
	pConstantBuffer_(nullptr),
	pTexture_(nullptr),
	vertexCount_(0), indexCountEachMaterial_(0), polygonCount_(0), materialCount_(0)
{
}


BranchesMakeTree::~BranchesMakeTree()
{
}

HRESULT BranchesMakeTree::Load(std::string fileName, SHADER_TYPE thisShader)
{

	return S_OK;
	
}

HRESULT BranchesMakeTree::Initialize(std::string textureFileName, SHADER_TYPE thisShader)
{
	thisShader_ = thisShader;

	//コンスタントバッファー作成
	if (FAILED(CreateConstantBuffer()))
	{
		MessageBox(nullptr, "コンスタントバッファーの失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};

	//テクスチャのロード
	if (FAILED(LoadTexture(textureFileName)))
	{
		MessageBox(nullptr, "テクスチャの生成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};



	return S_OK;

}

HRESULT BranchesMakeTree::Draw(Transform & transform)
{

	//シェーダーを選択
	Direct3D::SetShaderBundle(thisShader_);

	//シェーダーファイルに渡すための情報をまとめる
	CONSTANT_BUFFER cb;
	cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix()* Camera::GetViewMatrix() * Camera::GetProjectionMatrix());

	//ワールド行列
		//法線の回転のために
		//ライトは固定なのに、、陰も一緒に回っていた。
			//それを回転状況を考慮して、法線を回転させた。
	cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));	//



	//ワールド行列を入れる
		//ワールド行列を渡して、
		//頂点からカメラへの向きというのを取得するために、頂点に掛けてやらないといけない（移動、回転、拡大を込みしたワールド位置にするために）
	cb.matWorld = XMMatrixTranspose(transform.GetWorldMatrix());


	//カメラの位置を入れる
					//XMVECTOR型をFLOAT4型に入れることはできない。
					//変換する関数があったが、忘れてしまったので、1つずつFLOAT４型に変換して代入
	cb.camPos = XMFLOAT4(Camera::position_.vecX, Camera::position_.vecY, Camera::position_.vecZ, 0);


	//テクスチャあり
	if (pTexture_ != nullptr)
	{
		//nullptrでない　＝　テクスチャが存在する
		//「テクスチャクラスの」サンプラー作成（それぞれのマテリアルごとに）
			//サンプラーは、どこに貼り付けますか？などの情報
		//hlsl : g_sampler(s0)
		ID3D11SamplerState* pSampler = pTexture_->GetSampler();	//テクスチャクラスから、サンプラーを取得
		//ここにおける第一引数が何番目のサンプラーなどか、
		Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);
		//シェーダーへの橋渡しのビューを取得
		//hlsl : g_texture(t0)
		ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
			//シェーダーに作った変数に渡している
		Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);


		cb.isTexture = TRUE;	//コンスタントバッファのテクスチャ有無にtrueを立てる
	}
	//テクスチャなし
	else
	{
		//nullptrである　＝　テクスチャが存在しない
		//　＝　マテリアルそのものの色を使用しないといけない
		cb.isTexture = FALSE;

	}


	//シェーダーのコンスタントバッファ１のマップ
	if (FAILED(Direct3D::GetShaderClass(thisShader_)->MapShaderConstantBuffer1()))
	{
		MessageBox(nullptr, "コンスタントバッファMap失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};


	//自身の使用する
	//シェーダー独自の
	//コンスタンバッファや、テクスチャの受け渡しを行う
	Direct3D::GetShaderClass(thisShader_)->ShaderConstantBuffer1View();


	//シェーダーのコンスタントバッファ１のアンマップ
	if (FAILED(Direct3D::GetShaderClass(thisShader_)->UnMapShaderConstantBuffer1()))
	{
		MessageBox(nullptr, "コンスタントバッファUnMap失敗", "エラー", MB_YESNO);
		return E_FAIL;

	};



	D3D11_MAPPED_SUBRESOURCE pdata;
	//シェーダーなどのGPUでしか、触れることのできないものに
	//CPUから触れる準備
	if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))
	{
		MessageBox(nullptr, "コンスタントバッファのマップの書き込み失敗", "エラー", MB_YESNO);

		return E_FAIL;	//エラーですと返す。
	}
	//メモリにコピー
	memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	// データを値を送る


	{
		//頂点バッファをセット（頂点を持っておくための領域（確保場所））
			//複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
		UINT stride = sizeof(VERTEX);	//1頂点のサイズを指定する、→位置のみであれば、XMVECTORの構造体であったが、
									//位置とUVで構造体をとるので、その構造体のサイズで
		UINT offset = 0;
		Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

		// インデックスバッファーをセット
		stride = sizeof(int);
		offset = 0;
		Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

		//コンスタントバッファをセット
		Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
		Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用

	}

	Direct3D::pContext->Unmap(pConstantBuffer_, 0); 	//再開
						//書き込んだので、最後に、Unmapにて、　閉じる
						//コンスタントバッファに送る


	//描画
	//★頂点の個数を表す、→ここの頂点の数を、最終的な三角形の頂点の数
	//★つまり、インデックス情報の頂点の数
	Direct3D::pContext->DrawIndexed(indexCountEachMaterial_, 0, 0);




	return S_OK;

}

void BranchesMakeTree::Release()
{
	//ポインタ宣言順と逆に解放
	if (pTexture_ != nullptr)
	{
		pTexture_->Release();
	}
	SAFE_DELETE(pTexture_);
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_RELEASE(pVertexBuffer_);
	SAFE_DELETE_ARRAY(pVertices_);

}


HRESULT BranchesMakeTree::Join3DPolygon(Branch * pBranch, Transform& trans)
{
	//Transformによって、ワールド行列を取得し、
	//各頂点のローカル座標を変形。

	//頂点数を取得
	int toVertexCount = pBranch->GetVertexCount();

	//頂点数分
		//変換先の頂点情報を入れる要素を確保
		//頂点情報は、　構造体の内容は同じだが、
		//コピー先の構造体は、BranchesMakeTreeのほうの構造体だと示す必要がある（なぜならば、BranchのほうのVERTEXはpublicになっているので、混合してしまう。ので、明記）
	BranchesMakeTree::VERTEX* pToVertex = new BranchesMakeTree::VERTEX[toVertexCount];

	//引数３Dポリゴンモデルの頂点情報の取得
	Branch::VERTEX* originalVertex = pBranch->GetVertexes();


	//ワールド行列の計算
	trans.Calclation();


	//引数３Dポリゴンを
		//ワールド行列にて、変換
		//座標をワールド行列（拡大＊回転＊移動）を掛けて、変形
	for (int i = 0; i < toVertexCount; i++)
	{
		//頂点座標の変換
		pToVertex[i].position = XMVector3TransformCoord(originalVertex[i].position, trans.GetWorldMatrix());

		//UVの登録（UV情報はそのまま）
		pToVertex[i].uv = originalVertex[i].uv;

		//法線情報の変換
		pToVertex[i].normal = XMVector3TransformCoord(originalVertex[i].normal, trans.GetWorldMatrix());

	}


	//結合
	//現在ある自身のクラスの頂点情報に(後述：Tree)
	//引数にてもらった、今から結合するクラス(後述：Branch)の頂点情報を結合
	/*
	Branchの頂点数を調べて、
		�@Treeの頂点数＋Branch頂点数の数分、Vertexを確保、
		�ATreeの頂点をコピーして、�BBranchの頂点情報をコピー
	*/

	//�@
	BranchesMakeTree::VERTEX* pNextVertex = new BranchesMakeTree::VERTEX[vertexCount_ + toVertexCount];
	//�A
	memcpy(pNextVertex, pVertices_, sizeof(BranchesMakeTree::VERTEX) * vertexCount_);


	//�B
		//要素数 [vertexCount_ - 1]まで、要素を確保しているので、
		//新しい頂点情報は、[vertexCount]から、toVertexCount分確保
	memcpy(&pNextVertex[vertexCount_], pToVertex, sizeof(BranchesMakeTree::VERTEX) * toVertexCount);

	//古い頂点情報の削除
	SAFE_DELETE_ARRAY(pVertices_);
	SAFE_DELETE_ARRAY(pToVertex);


	//新しい頂点情報の登録
	pVertices_ = pNextVertex;

	//頂点数の更新
	vertexCount_ = vertexCount_ + toVertexCount;

	//頂点バッファー作成
	if (FAILED(CreateVertexBuffer()))
	{
		MessageBox(nullptr, "頂点バッファーの失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};





	//インデックス情報
	//インデックス数を取得
	int toIndexCount = pBranch->GetIndexCount();
	//新しい頂点情報の格納先
	int* pToIndex = new int[toIndexCount];

	//引数３Dポリゴンのインデックス情報の取得
	int* pOriginalIndex = pBranch->GetIndexes();

	
	//結合
	//インデックス情報の登録
	for (int i = 0; i < toIndexCount; i++)
	{
		//インデックス情報は
			//引数ポリゴンのBranchの元のインデックス情報に、＋元の頂点数　を足すことで、頂点上番号を取得できる
			//下記の計算をすることで、
			//登録先の頂点番号と番号を合わせることが可能である
		pToIndex[i] = (vertexCount_ - toVertexCount) + pOriginalIndex[i];

	}
	//結合
	//現在ある自身のクラスのインデックス情報に
	//引数にてもらった、今から結合するクラスのインデックス情報を結合
	/*
	Branchのインデックス数を調べて、
		�@Treeのインデックス数＋Branchインデックス数の数分、intを確保、
		�ATreeのインデックスをコピーして、�BBranchのインデックス情報をコピー
	*/

	//�@
	int* pNextIndex = new int[indexCountEachMaterial_ + toIndexCount];

	//�A
	memcpy(pNextIndex, pIndex_, sizeof(int) * indexCountEachMaterial_);

	//�B
	//要素数 [indexCountEachMaterial_ - 1]まで、要素を確保しているので、
	//新しい頂点情報は、[indexCountEachMaterial_]から、toIndexCount分確保
	memcpy(&pNextIndex[indexCountEachMaterial_], pToIndex, sizeof(int) * toIndexCount);

	//古い頂点情報の削除
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_DELETE_ARRAY(pToIndex);


	//新しい頂点情報の登録
	pIndex_= pNextIndex;

	//頂点数の更新
	indexCountEachMaterial_= indexCountEachMaterial_+ toIndexCount;


	//インデックスバッファー作成
	if (FAILED(CreateIndexBuffer()))
	{
		MessageBox(nullptr, "インデックスバッファーの失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};









	return S_OK;
}

HRESULT BranchesMakeTree::Join3DPolygon(Plane * pPlane, Transform & trans)
{


	//Transformによって、ワールド行列を取得し、
//各頂点のローカル座標を変形。

//頂点数を取得
	int toVertexCount = pPlane->GetVertexCount();

	//頂点数分
		//変換先の頂点情報を入れる要素を確保
		//頂点情報は、　構造体の内容は同じだが、
		//コピー先の構造体は、BranchesMakeTreeのほうの構造体だと示す必要がある（なぜならば、BranchのほうのVERTEXはpublicになっているので、混合してしまう。ので、明記）
	BranchesMakeTree::VERTEX* pToVertex = new BranchesMakeTree::VERTEX[toVertexCount];

	//引数３Dポリゴンモデルの頂点情報の取得
	Plane::VERTEX* originalVertex = pPlane->GetVertexes();


	//ワールド行列の計算
	trans.Calclation();


	//引数３Dポリゴンを
		//ワールド行列にて、変換
		//座標をワールド行列（拡大＊回転＊移動）を掛けて、変形
	for (int i = 0; i < toVertexCount; i++)
	{
		//頂点座標の変換
		pToVertex[i].position = XMVector3TransformCoord(originalVertex[i].position, trans.GetWorldMatrix());

		//UVの登録（UV情報はそのまま）
		pToVertex[i].uv = originalVertex[i].uv;

		//法線情報の変換
		pToVertex[i].normal = XMVectorSet(0.f, 0.f, 0.f, 0.f);

	}


	//結合
	//現在ある自身のクラスの頂点情報に(後述：Tree)
	//引数にてもらった、今から結合するクラス(後述：Branch)の頂点情報を結合
	/*
	Branchの頂点数を調べて、
		�@Treeの頂点数＋Branch頂点数の数分、Vertexを確保、
		�ATreeの頂点をコピーして、�BBranchの頂点情報をコピー
	*/

	//�@
	BranchesMakeTree::VERTEX* pNextVertex = new BranchesMakeTree::VERTEX[vertexCount_ + toVertexCount];
	//�A
	memcpy(pNextVertex, pVertices_, sizeof(BranchesMakeTree::VERTEX) * vertexCount_);


	//�B
		//要素数 [vertexCount_ - 1]まで、要素を確保しているので、
		//新しい頂点情報は、[vertexCount]から、toVertexCount分確保
	memcpy(&pNextVertex[vertexCount_], pToVertex, sizeof(BranchesMakeTree::VERTEX) * toVertexCount);

	//古い頂点情報の削除
	SAFE_DELETE_ARRAY(pVertices_);
	SAFE_DELETE_ARRAY(pToVertex);


	//新しい頂点情報の登録
	pVertices_ = pNextVertex;

	//頂点数の更新
	vertexCount_ = vertexCount_ + toVertexCount;

	//頂点バッファー作成
	if (FAILED(CreateVertexBuffer()))
	{
		MessageBox(nullptr, "頂点バッファーの失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};





	//インデックス情報
	//インデックス数を取得
	int toIndexCount = pPlane->GetIndexCount();
	//新しい頂点情報の格納先
	int* pToIndex = new int[toIndexCount];

	//引数３Dポリゴンのインデックス情報の取得
	int* pOriginalIndex = pPlane->GetIndexes();


	//結合
	//インデックス情報の登録
	for (int i = 0; i < toIndexCount; i++)
	{
		//インデックス情報は
			//引数ポリゴンのBranchの元のインデックス情報に、＋元の頂点数　を足すことで、頂点上番号を取得できる
			//下記の計算をすることで、
			//登録先の頂点番号と番号を合わせることが可能である
		pToIndex[i] = (vertexCount_ - toVertexCount) + pOriginalIndex[i];

	}
	//結合
	//現在ある自身のクラスのインデックス情報に
	//引数にてもらった、今から結合するクラスのインデックス情報を結合
	/*
	Branchのインデックス数を調べて、
		�@Treeのインデックス数＋Branchインデックス数の数分、intを確保、
		�ATreeのインデックスをコピーして、�BBranchのインデックス情報をコピー
	*/

	//�@
	int* pNextIndex = new int[indexCountEachMaterial_ + toIndexCount];

	//�A
	memcpy(pNextIndex, pIndex_, sizeof(int) * indexCountEachMaterial_);

	//�B
	//要素数 [indexCountEachMaterial_ - 1]まで、要素を確保しているので、
	//新しい頂点情報は、[indexCountEachMaterial_]から、toIndexCount分確保
	memcpy(&pNextIndex[indexCountEachMaterial_], pToIndex, sizeof(int) * toIndexCount);

	//古い頂点情報の削除
	SAFE_DELETE_ARRAY(pIndex_);
	SAFE_DELETE_ARRAY(pToIndex);


	//新しい頂点情報の登録
	pIndex_ = pNextIndex;

	//頂点数の更新
	indexCountEachMaterial_ = indexCountEachMaterial_ + toIndexCount;


	//インデックスバッファー作成
	if (FAILED(CreateIndexBuffer()))
	{
		MessageBox(nullptr, "インデックスバッファーの失敗", "エラー", MB_YESNO);
		return E_FAIL;
	};









	return S_OK;

}

HRESULT BranchesMakeTree::InitVertex()
{
	return S_OK;
}

HRESULT BranchesMakeTree::CreateVertexBuffer()
{
	//元の頂点バッファーの解放
	SAFE_RELEASE(pVertexBuffer_);



	//頂点情報を
//頂点バッファに登録（バッファに登録することになるので、ここで、頂点情報は、いらなくなっているのだが、、（頂点情報が変わらない限り。））
// 頂点データ用バッファの設定
//バッファに渡す情報の取得
	D3D11_BUFFER_DESC bd_vertex;


	//bd_vertex.ByteWidth = sizeof(vertices);
	//VERTEXという、構造体を、頂点数分サイズの確保
	bd_vertex.ByteWidth = sizeof(BranchesMakeTree::VERTEX) * vertexCount_;

	bd_vertex.Usage = D3D11_USAGE_DEFAULT;

	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	bd_vertex.CPUAccessFlags = 0;

	bd_vertex.MiscFlags = 0;

	bd_vertex.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA data_vertex;

	data_vertex.pSysMem = pVertices_;

	////メンバ変数に保存
	//data_vertex_ = new D3D11_SUBRESOURCE_DATA;
	////ここに保存を行って、更新の時は取り出し、更新するというやり方を行う。
	//	//だが、これだとメンバ変数として持っておくことと変わらない。（どっかに動的確保したもののアドレスを持っているだけなので、、、、　これなら、メンバ変数として持っておくほうが幾分見やすい）
	//data_vertex_->pSysMem = vertices;

	//頂点バッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		//エラーの原因のウィンドウ表示
		MessageBox(nullptr, "頂点バッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーですと返す。
	}

	return S_OK;

}

HRESULT BranchesMakeTree::InitIndex()
{
	return S_OK;
}

HRESULT BranchesMakeTree::CreateIndexBuffer()
{
	//元のインデックスバッファーの解放
	SAFE_RELEASE(pIndexBuffer_);


	// インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(int) * (indexCountEachMaterial_);	//サイズを、indexのサイズ分だけ、（インデックス情報の入っているサイズだけ）
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;


	//バッファーの格納情報
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex_;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	//インデックスバッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。
	}

	return S_OK;
}

HRESULT BranchesMakeTree::CreateConstantBuffer()
{
	//コンスタントバッファ作成
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	//コンスタントバッファの作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。
	}

	return S_OK;

}

HRESULT BranchesMakeTree::LoadTexture(std::string textureFile)
{
	//ファイルのロード
	//テクスチャのロード
	pTexture_ = new Texture;
	if (FAILED(pTexture_->Load(textureFile)))
	{
		MessageBox(nullptr, "ファイルの読み込みを失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	//ロード時
	return S_OK;

}


BranchesMakeTree::VERTEX::VERTEX() :
	position(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	uv(XMVectorSet(0.f, 0.f, 0.f, 0.f)),
	normal(XMVectorSet(0.f, 0.f, 0.f, 0.f))
{
}
