#include "BoxCollider.h"
#include "SphereCollider.h"	//相手のコライダーが球である場合を考えてインクルード


//BoxCollider::BoxCollider()
//{
//
//
//}

BoxCollider::BoxCollider(XMVECTOR position, XMVECTOR size, bool drawWireFrame):
	Collider::Collider(position , BOX_COLLIDER , NO_FUNCTION ,  drawWireFrame),
	size_(size)
{
	//コライダータイプをセットする
	//SetMyType(BOX_COLLIDER);
		//コンストラクタにて定義



	SetWireFrameSize();
}

BoxCollider::~BoxCollider()
{
}

bool BoxCollider::IsHit(Collider * target)
{
	bool hit = false;

	//相手のコライダータイプをもらう

	//そのタイプにより
	//衝突判定の計算式を持つ関数を呼び分ける
	switch (target->GetMyType())
	{
	case SPHERE_COLLIDER:
		//衝突判定
		//箱と球
		//自身とターゲット
		hit = IsHitBoxToSphere(this , (SphereCollider*)target);

		//ターゲットは継承の親クラスのCollider方なので、
			//相手が球だとわかったら、
			//球型にキャストする

		break;

	case BOX_COLLIDER :
		//衝突判定
		//箱と箱
		//自身とターゲット
		hit = IsHitBoxToBox(this, (BoxCollider*)target);
		break;


	default:
		hit = false;
		break;
	}
	

	return hit;
}

XMVECTOR BoxCollider::GetSize()
{
	return size_;
}

void BoxCollider::SetWireFrameSize()
{
	//サイズからワイヤーフレームに使用するサイズを変更
		//親のColliderクラスにある関数から、
		//XMVECTOR型から、サイズを変更する関数を呼び出す
	Collider::SetWireFrameSize(size_);
}
