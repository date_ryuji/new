#include <wincodec.h>
//#include <C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\Include\D3DX11.h>	//波線が出てしまう
//どこにあるのか、わかっていない
		//FbxSdkの時に、ファイルの場所を指定した。

		//場所はわかっている
		//だが、その場所を指定すると、
				//余分なファイルをロードしてしまうので、2重ロードになってしまう。

		//今ファイルを指定したが、
			//そこを優先的に探してくる、　そっちを優先的に見て、今まで標準で見ていたところを見なくなる。

				//場所がわからないとなっているので、
				//フルパスで指定してあげる

//上記をインクルードすると
//Direct２Dに使用するための、　#include <d3d11_1.h>をインクルードしたときにエラーを発生する。
	//これは、インクルードする順番によって回避できる
	//今回においては、
	//すでにD3DX11がある上に、　d3d11_1.hをオーバーライドしようとして、書き換えようとしてエラーが起こった。
	
	//だが、すでにd3d11_1.h　がある中で、 D3DX11をインクルードすれば、
	//エラーは回避できる。


//　lerpを使用するため
//https://cpprefjp.github.io/reference/cmath/lerp.html
//#include <iostream>
//#include <cmath>
	//使用できなかったため、
	//力ずくで計算させる


#include "Texture.h"
#include "Direct3D.h"

#include "../Direct3D11/D3DX11.h"

#include "Global.h"


#pragma comment( lib, "WindowsCodecs.lib" )
//#include 同様に、
//D3DX11が見つからないので、
	//現在はフルパスで指定
#pragma comment( lib, "Direct3D11/d3dx11.lib" )



	Texture::Texture()
	{
		//初期化
		pSampler_ = nullptr;	
		pSRV_ = nullptr;
		imgWidth_ = 0;
		imgHeight_ = 0;

	}

	Texture::~Texture()
	{
	}

	/*
	HRESULT Texture::Load(std::string fileName)
	{
		return S_OK;	//HRESULT型で、とりあえず、OK返しておく
	}
	*/

	


	HRESULT Texture::Load(std::string fileName)
	{
		//引数の画像ファイル名から
		//画像情報を、バッファーにコピーさせる
		ID3D11Texture2D* pTexture;

		//ファイル名にて示される画像ファイルを
		//バッファーにコピー
			//第二引数：バッファーのポインタのアドレス
		if (FAILED(GetTextureBuffer(fileName, &pTexture)))
		{
			MessageBox(nullptr, "画像のバッファーの作成失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		};




		////stringで受け取ったものを、ワイド文字で受け取る
		//wchar_t wtext[FILENAME_MAX];
		//				//ワイドのキャラ型、FILENAME_MAXは、ただの２６０という値→（２６０文字以上にファイル名が多くなることは内だろうという）
		//size_t ret;
		//mbstowcs_s(&ret, wtext, fileName.c_str(), fileName.length());
		//	//マルチバイトをワイド文字に変換する関数
		//	//multi byte string to wide ~ ？
		//		//第一引数は必要なもの　fileNameを　wtextに、入れて、どのぐらいの長さになるのか


		////COM使うため(WIC)
		//CoInitialize(nullptr);	//COMを使うなら、これが必要

		//IWICImagingFactory *pFactory = nullptr;	//読み込んで、
		//IWICBitmapDecoder *pDecoder = nullptr;	//デコーダーがあって、
		//IWICBitmapFrameDecode* pFrame = nullptr;
		//IWICFormatConverter* pFormatConverter = nullptr;
		//if (FAILED(CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, reinterpret_cast<void **>(&pFactory))))
		//{
		//	//失敗時の処理
		//	MessageBox(nullptr, "WICのインスタンス生成の失敗", "エラー", MB_OK);
		//	return E_FAIL;	//エラーを返す
		//
		//}
		//
		//
		//HRESULT hr = pFactory->CreateDecoderFromFilename(wtext, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder);
		////ファイル名を、自分で好きなものを受け取ったものを入れる		
		//	//だが、ファイル名の型は→、ワイド文字でないといけない（ワイド文字→どの文字も２バイトで受け取る）
		//	//関数の頭で、Stringのファイル名をワイド文字にする　wtext そのワイド文字を使用
		//if (FAILED(hr))
		//{
		//	MessageBox(nullptr, "画像ファイルの読み込み失敗", "エラー", MB_OK);
		//	//ここにおける、エラーというのは、ファイルを読み込めなかった、ファイルが存在しなかった可能性が高い
		//	return E_FAIL;	//エラーを返す
		//}


		//if (FAILED(pDecoder->GetFrame(0, &pFrame)))
		//{
		//	//失敗時の処理
		//	MessageBox(nullptr, "フレーム取得の失敗", "エラー", MB_OK);
		//	return E_FAIL;	//エラーを返す
		//}
		//if (FAILED(pFactory->CreateFormatConverter(&pFormatConverter)))
		//{
		//	//失敗時の処理
		//	MessageBox(nullptr, "コンバーターの初期化失敗", "エラー", MB_OK);
		//	return E_FAIL;	//エラーを返す
		//}
		//if (FAILED(pFormatConverter->Initialize(pFrame, GUID_WICPixelFormat32bppRGBA, WICBitmapDitherTypeNone, NULL, 1.0f, WICBitmapPaletteTypeMedianCut)))
		//{
		//	//失敗時の処理
		//	MessageBox(nullptr, "WIC初期化の失敗", "エラー", MB_OK);
		//	return E_FAIL;	//エラーを返す
		//
		//}
		//
		//CoUninitialize();	//最後に、COMを使うなら、これを使う


		////画像のサイズを調べる
		////UINT　unsigned int の略
		////UINT imgWidth;		//ヘッダにて、画像サイズを、送るために、メンバで取得しておく
		////UINT imgHeight;
		//							//メンバの変数に、画像のサイズを取得
		//if (FAILED(pFormatConverter->GetSize(&imgWidth_, &imgHeight_))) 
		//{
		//	//失敗時の処理
		//	MessageBox(nullptr, "画面サイズ取得失敗", "エラー", MB_OK);
		//	return E_FAIL;	//エラーを返す
		//}
		//

		//

		////テクスチャ生成
		//ID3D11Texture2D*	pTexture;	//テクスチャを入れる型
		//D3D11_TEXTURE2D_DESC texdec;
		//texdec.Width = imgWidth_;	//画像の横幅
		//texdec.Height = imgHeight_;	//画像の縦幅
		//texdec.MipLevels = 1;	//Mip　奥に行けば小さくなる→テクスチャ、ポリゴンを、近くにいるときは、高解像度のテクスチャを用意して、それを距離ごとに入れていけば、処理を見た目的によく見せて、軽く（遠くに同じきれいな、テクスチャを使ってはもったいない）
		//texdec.ArraySize = 1;
		//texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		//texdec.SampleDesc.Count = 1;
		//texdec.SampleDesc.Quality = 0;
		//texdec.Usage = D3D11_USAGE_DYNAMIC;
		//texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		//texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		//texdec.MiscFlags = 0;

		//if (FAILED(Direct3D::pDevice->CreateTexture2D(&texdec, nullptr, &pTexture)))
		//{
		//	//失敗時の処理
		//	MessageBox(nullptr, "テクスチャの生成失敗", "エラー", MB_OK);
		//	return E_FAIL;	//エラーを返す
		//}
		//


		////コンテキスト（絵を描く人）に渡す準備
		//D3D11_MAPPED_SUBRESOURCE hMappedres;
		//if (FAILED(Direct3D::pContext->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &hMappedres)))
		//{
		//	//失敗時の処理
		//	MessageBox(nullptr, "コンテキストのマップ失敗", "エラー", MB_OK);
		//	return E_FAIL;	//エラーを返す
		//}



		//if (FAILED(pFormatConverter->CopyPixels(nullptr, imgWidth_ * 4, imgWidth_ * imgHeight_ * 4, (BYTE*)hMappedres.pData)))
		//	//横一列で何ピクセルか、何バイトか、幅＊RGBAのための＊４
		//{

		//	//失敗時の処理
		//	MessageBox(nullptr, "ピクセル数（列）サイズ取得失敗", "エラー", MB_OK);
		//	return E_FAIL;	//エラーを返す
		//}			


		//Direct3D::pContext->Unmap(pTexture, 0);




		//サンプラー
		//テクスチャを貼れてから確認するとわかりやすい
		D3D11_SAMPLER_DESC  SamDesc;
		ZeroMemory(&SamDesc, sizeof(D3D11_SAMPLER_DESC));

		//LINEAR
		//POINT			違いは、分かりずらいと思うが、
			//3Dの物体は、きょりによって、サイズが違ってくるので、テクスチャも拡大縮小される
			//その時に、リアレストネイバーが使われる　＝　ぼかすか、ぼかさないか
		//２＊２を　4*4に拡大したときに、は、問題なく2倍すれば済む
			//だが、５＊５に拡大したときに、
			//空ができてしまう。
			//→存在している色を使って、一番違い色で塗ってしまう。（存在している色から、近くの塗られているべきピクセルを染める）
		//LINEAR
			//は、近くの色はこれだから、ここのポリゴンはこの色だろうと、保管してくれる。白と黒の間なら、灰色など

		//ふつうは、ぼかすべきなので、LINEAR
		//ポリゴンゲームなどは、POINTで
		SamDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;


		//テクスチャのアドレスコード
		//UVが０〜１より小さいときに、繰り返すのか、端っこを伸ばすのか、

		//U、横のテクスチャは伸ばすのか、伸ばさないのか　
			//CLAMP = 伸ばす
			//WRAP = 伸ばさない
		SamDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;	//CLAMP　UVで端っこの色が延ばされる（縦にだけ並ぶ、横は伸びる）
															//MAYAでUVを細かくした								
														//WRAP	横にだけ並ぶ（縦は伸びる（Vをやらなかったら））
														
		//大きいテクスチャを用意すると、解像度が荒くなるので、
			//小さいテクスチャを用意して、
			//それを並べると、めっちゃ大きく、引き伸ばされたものになる。
			//小さいテクスチャを並べて、そして、上記をNONE?などにすれば、縦横延ばされずに、テクスチャのVUを反映できる。
		//MAYA→UV→平面→　これにて、　縮小すると、UVを細かく何個も貼り付けることが可能


		//地面だったら、つなぎ目が見えてしまう。
			//MIRRERミラーにすれば、自然につながる。（繰り返しになっていることはばれるが、）
			//表、裏みたいに張っていく


		//テクスチャの状態に合わせて設定できるようにすればよい。

	
		//VもWRAPにすれば、UVが繰り返されていても表示できる
		SamDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;

		//Wはわかりません
		SamDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		/*
		if (FAILED(Direct3D::pDevice->CreateSamplerState(&SamDesc, &pSampler_)))
		{
			//失敗時の処理
			MessageBox(nullptr, "サンプラーの生成失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		*/
		Direct3D::pDevice->CreateSamplerState(&SamDesc, &pSampler_);


		//シェーダーリソースビュー
			//シェーダーへの橋渡し（〜ビューは橋渡し）
		D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
		srv.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srv.Texture2D.MipLevels = 1;
		if (FAILED(Direct3D::pDevice->CreateShaderResourceView(pTexture, &srv, &pSRV_)))
		{
			//失敗時の処理
			MessageBox(nullptr, "シェーダー生成失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}


		pTexture->Release();

		return S_OK;
	}


	//Direct3Dにて作成した
	//テクスチャを、テクスチャクラスで必要情報を持たせる。
		//この情報を使い、　Direct３Dで作られたID3D11型のテクスチャを、ゲームオブジェクトとして描画できるテクスチャに変更する。
	HRESULT Texture::Load(ID3D11Texture2D* pTexture)
	{
		//すでにテクスチャ部分は作成されているので、
		//テクスチャ作成部分は削り、
		//テクスチャクラスとして持っておくべき、　サンプラーの情報を作成


		//サンプラー
		//テクスチャを貼れてから確認するとわかりやすい
		D3D11_SAMPLER_DESC  SamDesc;
		ZeroMemory(&SamDesc, sizeof(D3D11_SAMPLER_DESC));

		//LINEAR
		//POINT			違いは、分かりずらいと思うが、
			//3Dの物体は、きょりによって、サイズが違ってくるので、テクスチャも拡大縮小される
			//その時に、リアレストネイバーが使われる　＝　ぼかすか、ぼかさないか
		//２＊２を　4*4に拡大したときに、は、問題なく2倍すれば済む
			//だが、５＊５に拡大したときに、
			//空ができてしまう。
			//→存在している色を使って、一番違い色で塗ってしまう。（存在している色から、近くの塗られているべきピクセルを染める）
		//LINEAR
			//は、近くの色はこれだから、ここのポリゴンはこの色だろうと、保管してくれる。白と黒の間なら、灰色など

		//ふつうは、ぼかすべきなので、LINEAR
		//ポリゴンゲームなどは、POINTで
		SamDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;


		//テクスチャのアドレスコード
		//UVが０〜１より小さいときに、繰り返すのか、端っこを伸ばすのか、

		//U、横のテクスチャは伸ばすのか、伸ばさないのか　
			//CLAMP = 伸ばす
			//WRAP = 伸ばさない
		SamDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;	//CLAMP　UVで端っこの色が延ばされる（縦にだけ並ぶ、横は伸びる）
															//MAYAでUVを細かくした								
														//WRAP	横にだけ並ぶ（縦は伸びる（Vをやらなかったら））

		//大きいテクスチャを用意すると、解像度が荒くなるので、
			//小さいテクスチャを用意して、
			//それを並べると、めっちゃ大きく、引き伸ばされたものになる。
			//小さいテクスチャを並べて、そして、上記をNONE?などにすれば、縦横延ばされずに、テクスチャのVUを反映できる。
		//MAYA→UV→平面→　これにて、　縮小すると、UVを細かく何個も貼り付けることが可能


		//地面だったら、つなぎ目が見えてしまう。
			//MIRRERミラーにすれば、自然につながる。（繰り返しになっていることはばれるが、）
			//表、裏みたいに張っていく


		//テクスチャの状態に合わせて設定できるようにすればよい。


		//VもWRAPにすれば、UVが繰り返されていても表示できる
		SamDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;

		//Wはわかりません
		SamDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		/*
		if (FAILED(Direct3D::pDevice->CreateSamplerState(&SamDesc, &pSampler_)))
		{
			//失敗時の処理
			MessageBox(nullptr, "サンプラーの生成失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		*/
		Direct3D::pDevice->CreateSamplerState(&SamDesc, &pSampler_);


		//シェーダーリソースビュー
			//シェーダーへの橋渡し（〜ビューは橋渡し）
		D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
		srv.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srv.Texture2D.MipLevels = 1;
		if (FAILED(Direct3D::pDevice->CreateShaderResourceView(pTexture, &srv, &pSRV_)))
		{
			//失敗時の処理
			MessageBox(nullptr, "シェーダー生成失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		//pTexture->Release();
			//Releaseを呼んでしまうと、　ID3D11で作成したテクスチャをリリースしてしまう、　テクスチャクラスのポインタ自体は、ここで解放したくない。

		//とりあえず、手打ち
			//本来は、正確に取得する必要がある。
		imgWidth_ = Direct3D::scrWidth;
		imgHeight_ = Direct3D::scrHeight;



		return S_OK;
	}

	HRESULT Texture::LoadCube(std::string fileName)
	{



		//stringで受け取ったものを、ワイド文字で受け取る
		//ファイル名を全部を2バイトとする、ワイド文字に変換
		wchar_t wtext[FILENAME_MAX];
		//ワイドのキャラ型、FILENAME_MAXは、ただの２６０という値→（２６０文字以上にファイル名が多くなることは内だろうという）
		size_t ret;
		mbstowcs_s(&ret, wtext, fileName.c_str(), fileName.length());


		//インクルードが必要で、
			//ライブラリも必要
			//ググれば、必要なインクルードファイル、ライブラリもわかる
		//#include <D3DX11.h>
		D3DX11_IMAGE_LOAD_INFO loadSMInfo;
		loadSMInfo.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;

		ID3D11Texture2D* SMTexture = 0;
		D3DX11CreateTextureFromFile(Direct3D::pDevice, fileName.c_str(),
			&loadSMInfo, 0, (ID3D11Resource**)&SMTexture, 0);

		D3D11_TEXTURE2D_DESC SMTextureDesc;
		SMTexture->GetDesc(&SMTextureDesc);

		D3D11_SHADER_RESOURCE_VIEW_DESC SMViewDesc;
		SMViewDesc.Format = SMTextureDesc.Format;

		//CubeMapと指定
		SMViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
		SMViewDesc.TextureCube.MipLevels = SMTextureDesc.MipLevels;
		SMViewDesc.TextureCube.MostDetailedMip = 0;

		//シェーダーリソースビューに渡している
		Direct3D::pDevice->CreateShaderResourceView(SMTexture, &SMViewDesc, &pSRV_);



		return S_OK;
	}




	void Texture::Release()
	{
		//逆で解放
		//ID3D11型なので、RELEASEにて解放
		SAFE_RELEASE(pSRV_);
		SAFE_RELEASE(pSampler_);

	}

	HRESULT Texture::GetTextureBuffer(std::string fileName ,ID3D11Texture2D ** pBuffer)
	{
		D3D11_MAPPED_SUBRESOURCE hMappedres;

		if (FAILED(GetTextureBuffer(fileName, pBuffer, &hMappedres)))
		{
			MessageBox(nullptr, "画像のバッファーの作成失敗", "エラー", MB_OK);
			return E_FAIL;	
		};
		return S_OK;

	}

	HRESULT Texture::GetTextureBuffer(std::string fileName, ID3D11Texture2D ** pBuffer, D3D11_MAPPED_SUBRESOURCE* hMappedres)
	{
		//stringで受け取ったものを、ワイド文字で受け取る
		wchar_t wtext[FILENAME_MAX];
		//ワイドのキャラ型、FILENAME_MAXは、ただの２６０という値→（２６０文字以上にファイル名が多くなることは内だろうという）
		size_t ret;
		mbstowcs_s(&ret, wtext, fileName.c_str(), fileName.length());
		//マルチバイトをワイド文字に変換する関数
		//multi byte string to wide ~ ？
			//第一引数は必要なもの　fileNameを　wtextに、入れて、どのぐらいの長さになるのか


		//COM使うため(WIC)
		CoInitialize(nullptr);	//COMを使うなら、これが必要

		IWICImagingFactory *pFactory = nullptr;	//読み込んで、
		IWICBitmapDecoder *pDecoder = nullptr;	//デコーダーがあって、
		IWICBitmapFrameDecode* pFrame = nullptr;
		IWICFormatConverter* pFormatConverter = nullptr;
		if (FAILED(CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, reinterpret_cast<void **>(&pFactory))))
		{
			//失敗時の処理
			MessageBox(nullptr, "WICのインスタンス生成の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す

		}


		HRESULT hr = pFactory->CreateDecoderFromFilename(wtext, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder);
		//ファイル名を、自分で好きなものを受け取ったものを入れる		
			//だが、ファイル名の型は→、ワイド文字でないといけない（ワイド文字→どの文字も２バイトで受け取る）
			//関数の頭で、Stringのファイル名をワイド文字にする　wtext そのワイド文字を使用
		if (FAILED(hr))
		{
			MessageBox(nullptr, "画像ファイルの読み込み失敗", "エラー", MB_OK);
			//ここにおける、エラーというのは、ファイルを読み込めなかった、ファイルが存在しなかった可能性が高い
			return E_FAIL;	//エラーを返す
		}


		if (FAILED(pDecoder->GetFrame(0, &pFrame)))
		{
			//失敗時の処理
			MessageBox(nullptr, "フレーム取得の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		if (FAILED(pFactory->CreateFormatConverter(&pFormatConverter)))
		{
			//失敗時の処理
			MessageBox(nullptr, "コンバーターの初期化失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		if (FAILED(pFormatConverter->Initialize(pFrame, GUID_WICPixelFormat32bppRGBA, WICBitmapDitherTypeNone, NULL, 1.0f, WICBitmapPaletteTypeMedianCut)))
		{
			//失敗時の処理
			MessageBox(nullptr, "WIC初期化の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す

		}

		CoUninitialize();	//最後に、COMを使うなら、これを使う


		//画像のサイズを調べる
		//UINT　unsigned int の略
		//UINT imgWidth;		//ヘッダにて、画像サイズを、送るために、メンバで取得しておく
		//UINT imgHeight;
									//メンバの変数に、画像のサイズを取得
		if (FAILED(pFormatConverter->GetSize(&imgWidth_, &imgHeight_)))
		{
			//失敗時の処理
			MessageBox(nullptr, "画面サイズ取得失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}




		//テクスチャ生成
		//ID3D11Texture2D*	pTexture;	//テクスチャを入れる型
		D3D11_TEXTURE2D_DESC texdec;
		texdec.Width = imgWidth_;	//画像の横幅
		texdec.Height = imgHeight_;	//画像の縦幅
		texdec.MipLevels = 1;	//Mip　奥に行けば小さくなる→テクスチャ、ポリゴンを、近くにいるときは、高解像度のテクスチャを用意して、それを距離ごとに入れていけば、処理を見た目的によく見せて、軽く（遠くに同じきれいな、テクスチャを使ってはもったいない）
		texdec.ArraySize = 1;
		texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		texdec.SampleDesc.Count = 1;
		texdec.SampleDesc.Quality = 0;
		texdec.Usage = D3D11_USAGE_DYNAMIC;
		texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		texdec.MiscFlags = 0;

		if (FAILED(Direct3D::pDevice->CreateTexture2D(&texdec, nullptr, pBuffer)))
		{
			//失敗時の処理
			MessageBox(nullptr, "テクスチャの生成失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}



		//コンテキスト（絵を描く人）に渡す準備
		//D3D11_MAPPED_SUBRESOURCE hMappedres;
		if (FAILED(Direct3D::pContext->Map((*pBuffer), 0, D3D11_MAP_WRITE_DISCARD, 0, hMappedres)))
		{
			//失敗時の処理
			MessageBox(nullptr, "コンテキストのマップ失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}



		if (FAILED(pFormatConverter->CopyPixels(nullptr, imgWidth_ * 4, imgWidth_ * imgHeight_ * 4, (BYTE*)hMappedres->pData)))
			//横一列で何ピクセルか、何バイトか、幅＊RGBAのための＊４
		{

			//失敗時の処理
			MessageBox(nullptr, "ピクセル数（列）サイズ取得失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}


		Direct3D::pContext->Unmap((*pBuffer), 0);


		return S_OK;

	}




