#include "Audio.h"
#include <xaudio2.h>
#include <vector>
#include <stdlib.h>


#include "Global.h"


#define SAFE_DESTROY_VOICE(p) if(p != nullptr){p->DestroyVoice(); p = nullptr;}

//プロパティにおいて、
//コード生成→ランタイムライブラリ→　DLL付きにすると、　pdbが読み込めないというエラーは出ない
	//だが、DirectXを使う上で、DLLなしにしないとエラーになるので、pdbのエラーが出ないようにしなければいけない。


//或いは、
//Microsoft〜サーバーから、　シンボルを読み込むとpdbをインストールするので、エラーは通らなくなる




namespace Audio
{
	struct Chunk
	{
		char	id[4]; 		// ID（IDはchar４つの4バイトと決まっている）
		unsigned int	size;	// サイズ（サイズがマイナスはない）
	};

	//ステージ（XAudio２自体）
	IXAudio2* pXAudio = nullptr;
	//指揮者（楽譜の作成者みたいな、これを演奏してという人）
	IXAudio2MasteringVoice* pMasteringVoice = nullptr;




	//演奏者（ソースボイス）が複数必要なので、
		//楽譜であるバッファと、演奏者を構造体で取得する
	struct AudioData
	{
		//オーディオファイルのバッファ
			//データ部の情報
		XAUDIO2_BUFFER buf = {};


		//オーディオファイルの演奏者
		//ソースボイスを一度に短い時間で再生したいとき（SE）とか
			//そうゆうときには、
			//ソースボイスを1つのファイルに複数持たせておく。
				//→そのソースボイスを回して、一人目が再生しているなら二人目にお願い。。。と回す
			//何個作るのかは、Load時の引数にてもらえばよい。
		IXAudio2SourceVoice** ppSourceVoice;


		//フォーマットチャンク
		WAVEFORMATEX	fmt;


		//ソースボイスの作成個数
		int createNum;

		//ファイル名
		std::string fileName;

		//コンストラクタ
		AudioData() :
			ppSourceVoice(nullptr)
		{
		}

	};
	//上記の楽譜、演奏者、の情報をベクター配列で取得し、追加していく
	std::vector<AudioData*>	audioDatas;


	////データ群の中から指定のデータを解放
	////引数：解放させたいデータを示す添え字
	void ReleaseOneData(int handle);

	//データの共有部分をコピーさせる処理
	//引数：コピー元の添え字番号（Vector配列におけるコピー元の添え字）
	//引数：ソースボイスの作成数
	//引数：ファイル名
	//戻値：ハンドル番号
	int ShareData(int original, int create, std::string fileName);


}




	void Audio::Initialize()
	{
		//COMの宣言
			//テクスチャクラスなどで行ったもの
			//プロジェクト内で一回やれば、2回やる必要ないので、
			//テクスチャクラスで作成した記憶があるので、エンジンに組み込むときは、これはいらない。
		//GameEngine上に一つあれば十分
		//CoInitializeEx(0, COINIT_MULTITHREADED);


		//ステージ作成,初期化
		XAudio2Create(&pXAudio);	//演奏者など、指揮者を置くためのステージ

		//指揮者作成,初期化
		pXAudio->CreateMasteringVoice(&pMasteringVoice);

		//ソースボイスは音ごとに作るのでのちに
			//ソースボイス＝演奏者なので、音楽ファイルごと、個別に発生させる演奏者分作成


		//演奏者群（Vector）を初期化
		audioDatas.clear();


	}

	int Audio::Load(std::string fileName , int create)
	{
		//ロードするデータが、既存データ内に存在するか
		{
			//引数にてもらったファイル名がすでにロードされているものなのかを調べる
				//ロード済み：ロードしてあるファイルを共通して使う
							//この時オーディオ構造体のbufに、オーディオのバッファ（データ）が入っているので、それをコピーする
							//ソースボイスは改めて作らなければいけない、なぜなら、演奏者なので、仮にオーディオデータが同じだとしても、演奏者を元居るソースボイスにお願いすることになると、
							//元居るソースボイスが自分自身のオーディオファイルを鳴らすときに、現在再生中のため再生できないとなってしまう、そのため、演奏者であるソースボイスは改めて作らなければいけない
				//ロード未　：新たにロード


			//同様のファイル名を探す
			//同様のファイルがあった場合、共有部分をコピーし（コピーというよりポインタを取得）ハンドル番号を返す
			for (int i = 0; i < audioDatas.size(); i++)
			{
				//すでに同一のファイルが存在していたらロードしない
					//同一ファイルの判断は、ファイル名から判断
				if (audioDatas[i]->fileName == fileName)
				{
					//データの共有部分をコピーさせる処理
						//関数内にて、データをVector配列に登録し、
						//そして、登録時のハンドル番号を返す
						//戻値をLoadの関数の戻値として処理を終了させる
					return ShareData(i, create, fileName);


				}

			}
		}




		//新規にオーディオデータをロード
			//上記の繰り返し分にて、既存のデータ内に同様のデータがなかった。
		{

			//テキストエディタを開くように
			//ファイルのハンドルを取得する
			HANDLE hFile;
			hFile = CreateFile(
				fileName.c_str(),                 //ファイル名(文字列型のポインタは、関数で専用の関数があるのでそれを使用してポインタを返す)
				GENERIC_READ,           //アクセスモード（読み込み）
				0,                      //共有（なし）
				NULL,                   //セキュリティ属性（継承しない）
				OPEN_EXISTING,           //読み込み方法
				FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
				NULL);                  //拡張属性（なし）


			//上記では、ファイルを開いているだけ
			//ここからファイル情報のチャンクを読み込む


			//データのIDとデータサイズ

			DWORD dwBytes = 0;

			//チャンクの構造体
			Chunk riffChunk;
			//ファイルハンドル
			//チャンクのアドレス
			//8バイト分取得（IDとサイズ部分）
			//今回読み込んだサイズ
				//Chunkの構造体に書き込む
			ReadFile(hFile, &riffChunk, 8, &dwBytes, NULL);
			//"RIFF"
			//size = ~~~

			char checkT[4] = { 'R' ,'I' , 'F' , 'F' };
			//if (riffChunk.id != checkT)
			//{
			//	//このAudioクラスで再生できるものではないのでー１（エラー）を返す
			//	return -1;
			//}

			//＝＝にはならない
				//なぜなら配列の添え字ナシは、→その配列の先頭アドレスだから(配列の添え字ナシを関数の引数に渡したとき、関数受け取り側でarray[]で受け取ればポインタ受け取りのようになるのは、それが理由)
				//先頭アドレスと、別の配列の先頭アドレスでは、当然＝＝にはならない。

				//中身が同じかどうかを判断しないといけない。
			for (int i = 0; i < 4; i++)
			{
				if (riffChunk.id[i] != checkT[i])
				{
					//	//このAudioクラスで再生できるものではないのでー１（エラー）を返す
					return -1;
				}
			}



			//dwBytesには、今回読み込んだデータのサイズが入ってくる。
				//それでもReadFileに何回も読み込ませると、読み込み位置を移動してくれる。
				//それは、hFileのハンドルの読み込み位置をReadFileが移動しているから。
				//dwBytesの値は、あくまで、今回読み込んだバイト数


			//データが
			//24718バイトのデータサイズ

			//構造体のsizeには、
			//24710が入ってくる　これはつまり、　IDとサイズの8バイトを抜いたサイズがデータ部分だということ。


			//上記でデータID,サイズを受け取った後に、そのあとのデータから、
			//８バイト後から読み込む。
			//ファイルの種類を取得
				//WAVEと入ってくるか、
				//WAVEとなれば、WAVEファイルの再生を行うようにすればよい
				//ここでWAVEでなければ、再生読み込みしないように。
			char wave[4];
			ReadFile(hFile, &wave, 4, &dwBytes, NULL);
			//"WAVE"

			char checkW[4] = { 'W' ,'A' , 'V' , 'E' };

			//＝＝にはならない
				//なぜなら配列の添え字ナシは、→その配列の先頭アドレスだから(配列の添え字ナシを関数の引数に渡したとき、関数受け取り側でarray[]で受け取ればポインタ受け取りのようになるのは、それが理由)
				//先頭アドレスと、別の配列の先頭アドレスでは、当然＝＝にはならない。

				//中身が同じかどうかを判断しないといけない。
			//if (wave != checkW)
			//{
			//	//このAudioクラスで再生できるものではないのでー１（エラー）を返す
			//	return -1;
			//}

			for (int i = 0; i < 4; i++)
			{
				if (wave[i] != checkW[i])
				{
					//	//このAudioクラスで再生できるものではないのでー１（エラー）を返す
					return -1;
				}
			}



			//フォーマットチャンク
			//に8バイト分（8バイトは　フォーマットチャンクのID,サイズが入っている）
			Chunk formatChunk;
			//Chunkの構造体に書き込む
			ReadFile(hFile, &formatChunk, 8, &dwBytes, NULL);

			//フォーマットチャンクから
			WAVEFORMATEX	fmt;
			//取得したフォーマットチャンクのデータ部のサイズ
				//そのサイズ分だけ、フォーマットチャンク
			ReadFile(hFile, &fmt, formatChunk.size, &dwBytes, NULL);

			//nChannels １モノナル、0ステレオ
			//〜〜　＝　1秒間に何回書き換えているか
			//＝＝　＝　1個１６Bit


			//データチャンク
			Chunk data;
			//これまでと同じように8バイト分でID,サイズを取得し
			//Chunkの構造体に書き込む
			ReadFile(hFile, &data, 8, &dwBytes, NULL);

			//サイズ分データを読み込む
			//今回は読み込んだデータをchar*に突っ込む
			//サイズ分のデータが欲しいので、
				//ポインタ配列の要素数は、データサイズ分取得
				//動的確保でないと変数で配列の要素数を宣言できないので注意
			char* pBuffer = new char[data.size];
			//char* pBuffer = (char*)malloc(data.size);
			//ZeroMemory(pBuffer, data.size);


			//pBufferはポインタなので＆つけない
			ReadFile(hFile, pBuffer, data.size, &dwBytes, NULL);



			//ハンドル閉じる
			//データは取得したので閉じる
			CloseHandle(hFile);



			//演奏者の情報を
			//構造体で取得する
			AudioData* ad = new AudioData;
			{
				//ソースボイス作成数を登録
				ad->createNum = create;
				//ファイル名を登録
				ad->fileName = fileName;

				//ソースボイス（演奏者）を複数作成
					//引数でもらったソースボイス作成数分ポインタの複数個確保
				ad->ppSourceVoice = new IXAudio2SourceVoice*[create];

				//フォーマットチャンクの登録
					//ロード済みのデータをさらにロードすることを防ぐために、
					//すでにあるデータを共通して使うようにする、
					//そのためには、ソースボイスを作成するときに、フォーマットチャンクが必要になる、そのために所有
				ad->fmt = fmt;


				//指定数分、ソースボイスの作成
				for (int i = 0; i < create; i++)
				{
					//実体を持つのではなく、
					//ポインタを渡して、そのアドレスに要素を入れてくれる。
					//ad.pSourceVoice[i] = new IXAudio2SourceVoice;


					//演奏者の作成,初期化
					//データファイルごと、ソースボイスを作るようにする
					//引数にて、フォーマットチャンクを使用する
					pXAudio->CreateSourceVoice(&ad->ppSourceVoice[i], &fmt);


				}
				//オーディオのバッファ部分
					//データ部分
				ad->buf.pAudioData = (BYTE*)pBuffer;	//Byte型のポインタ。サウンドデータのデータ部を表している。すでにロード済みのデータは、このポインタを共通して扱うようにする
				ad->buf.Flags = XAUDIO2_END_OF_STREAM;
				ad->buf.AudioBytes = data.size;




				//データリストに、
				//新規のデータとして追加する。
				audioDatas.push_back(ad);
				//以上の作業をファイルごとに行うことで、
					//複数ファイルを扱うことができる。
			}



			//演奏者の作成,初期化
			//データファイルごと、ソースボイスを作るようにする
			//引数にて、フォーマットチャンクを使用する
			//pXAudio->CreateSourceVoice(&ad.pSourceVoice), &fmt);



			//ベクター配列の一番後ろに追加したので、
			//現在のベクター配列の要素の数ー１　で、ベクター配列にデータを追加したときの添え字を返すことができる
			return (int)(audioDatas.size()) - 1;

			//以上の番号でModel.cppのように
				//ソースボイスを番号で管理することが可能になるので、
				//Play（）の時に、ソースボイス番号を渡してやれば、その番号によって再生するソースボイスを変えればよい

		}


		////楽譜に相当するもの
		////楽譜の作成＝＞この楽譜を使って演奏者に演奏させる
		//buf.pAudioData = (BYTE*)pBuffer;
		//buf.Flags = XAUDIO2_END_OF_STREAM;
		//buf.AudioBytes = data.size;
		////様々なメンバ変数がある。
		////Loopbegin
		//	//ループ開始
		//	//イントロ部分は一回だけ流して、メインの部分をループするというやり方もできるのではないだろうか？
		//	//途中でループ再生ができるということも可能かも？


		//{



		//}

		////演奏者の作成,初期化
		////データファイルごと、ソースボイスを作るようにする
		////引数にて、フォーマットチャンクを使用する
		//pXAudio->CreateSourceVoice(&pSourceVoice, &fmt);

	}
	int Audio::ShareData(int original , int create , std::string fileName)
	{
		//演奏者の情報を
			//構造体で取得する
		AudioData* ad = new AudioData;
		{
			//ソースボイス作成数を登録
			ad->createNum = create;
			//ファイル名を登録
			ad->fileName = fileName;

			//ソースボイス（演奏者）を複数作成
			//ソースボイスはコピー元データを共通して使うわけにはいかない。
					//あくまで共通して使いたいのは、ロードをしなくてはいけないオーディオデータ部分。演奏者を共通して使ってしまうと、コピー元のオーディオデータを再生するときに不具合が起こる（ほかの人のオーディオを鳴らしている状態になる）
				//引数でもらったソースボイス作成数分ポインタの複数個確保
			ad->ppSourceVoice = new IXAudio2SourceVoice*[create];

			//フォーマットチャンクの登録
				//ロード済みのデータをさらにロードすることを防ぐために、
				//すでにあるデータを共通して使うようにする、
				//そのためには、ソースボイスを作成するときに、フォーマットチャンクが必要になる、そのために所有
			ad->fmt = audioDatas[original]->fmt;


			//指定数分、ソースボイスの作成
			for (int i = 0; i < create; i++)
			{
				//実体を持つのではなく、
				//ポインタを渡して、そのアドレスに要素を入れてくれる。
				//ad.pSourceVoice[i] = new IXAudio2SourceVoice;


				//演奏者の作成,初期化
				//データファイルごと、ソースボイスを作るようにする
				//引数にて、フォーマットチャンクを使用する
				pXAudio->CreateSourceVoice(&ad->ppSourceVoice[i], &ad->fmt);


			}
			//オーディオのバッファ部分
				//データ部分
			//データ部のコピー(コピー元のオーディオデータからコピー)
			ad->buf.pAudioData = audioDatas[original]->buf.pAudioData;	//Byte型のポインタ。サウンドデータのデータ部を表している。すでにロード済みのデータは、このポインタを共通して扱うようにする
			ad->buf.Flags = audioDatas[original]->buf.Flags;
			ad->buf.AudioBytes = audioDatas[original]->buf.AudioBytes;




			//データリストに、
			//新規のデータとして追加する。
			audioDatas.push_back(ad);
			//以上の作業をファイルごとに行うことで、
				//複数ファイルを扱うことができる。


			
		}


		//ベクター配列の一番後ろに追加したので、
		//現在のベクター配列の要素の数ー１　で、ベクター配列にデータを追加したときの添え字を返すことができる
		return (int)(audioDatas.size()) - 1;
	}

	void Audio::Play(int hundle)
	{
		//楽譜ごとに演奏者を作らないといけない
		//楽譜ごとにソースボイスを作る


		//ソースボイス個数分
		//繰り返して
		//連続して同じソースボイスに再生が呼ばれたとき
			//→一人のソースボイスは、音を再生中の時、再生が終わるまで次の再生を行うことができない（どんどんスタックがたまっていく感じ）
		//なので、複数個ソースボイスを持たせているならば、
			//ソースボイスを頭から見ていって、
			//そいつが現在再生中のソースボイスなら、次のソースボイスに移って、
			//再生中でないソースボイスを見つけて、そいつに再生させる。→そうすれば、連続して再生することが可能になる。
		for (int i = 0; i < audioDatas[hundle]->createNum; i++)
		{
			//複数のソースボイスを　ベクター配列で取得しているので、
			//ハンドル番号により、指定する
			XAUDIO2_VOICE_STATE state;
			audioDatas[hundle]->ppSourceVoice[i]->GetState(&state);

			//下記の値が１なら、絶賛音を鳴らし中
			//0なら、音を鳴らしていない
			//なので次のソースボイスに音を鳴らしてもらう
			if (state.BuffersQueued == 0)
			{
				//鳴らす
				
				//再生データ専用の変数に渡した、
				//楽譜を演奏者に渡す
				audioDatas[hundle]->ppSourceVoice[i]->SubmitSourceBuffer(&audioDatas[hundle]->buf);
				//再生
				audioDatas[hundle]->ppSourceVoice[i]->Start();

				break;

			}
		}

		


	}
	void Audio::Stop(int hundle)
	{
		//ソースボイス分停止
		for (int i = 0; i < audioDatas[hundle]->createNum; i++)
		{
			{
				//停止
				audioDatas[hundle]->ppSourceVoice[i]->Stop();
				//ソースボイスに関連付けた、バッファを解放
				audioDatas[hundle]->ppSourceVoice[i]->FlushSourceBuffers();

			}
		}

	}
	void Audio::InfiniteLoop(int handle)
	{
		//http://dvdm.blog134.fc2.com/blog-entry-50.html

		//ループ回数
		//ループカウントをInfinite=無限にする
		audioDatas[handle]->buf.LoopCount = XAUDIO2_LOOP_INFINITE;


	}
	void Audio::NotInfiniteLoop(int handle)
	{
		//http://dvdm.blog134.fc2.com/blog-entry-50.html

		//ループ回数
		//ループをしない
		audioDatas[handle]->buf.LoopCount = 0;
		audioDatas[handle]->buf.LoopBegin = 0;
		audioDatas[handle]->buf.LoopLength = 0;


	}
	HRESULT Audio::SetVolume(int handle , float volume)
	{
		//http://dvdm.blog134.fc2.com/blog-entry-46.html

		/*
		SourceVoiceのメンバ

		HRESULT SetVolume(
			float Volume,
			UINT32 OperationSet = XAUDIO2_COMMIT_NOW
		);
		
		*/
		//引数の値を０〜１．０ｆの値に切り詰める
		if (volume < 0.0f)
		{
			volume = 0.0f;
		}
		else if(volume > 1.0f)
		{
			volume = 1.0f;
		}

		//ソースボイス全体に音量を変更する
		for (int i = 0; i < audioDatas[handle]->createNum; i++)
		{
			//第一引数：ボリュームの割合（0.0f ~ 1.0f）
			//第二引数：〜〜〜〜〜〜〜〜（書かなかった時には、= XAUDIO2_COMMIT_NOWの値が自動で入るため入れなくともよい）
			if (FAILED(audioDatas[handle]->ppSourceVoice[i]->SetVolume(volume)))
			{
				//エラーメッセージ
				//MessageBox()
				return E_FAIL;
			}
		
		}

		return S_OK;
	}
	void Audio::Release()
	{
		//https://brain.cc.kogakuin.ac.jp/~kanamaru/lecture/C++2/09/09-02.html


		//audioDatas自体は、実体（解放必要なし）
		//vectorなので、sizeにて要素数を調べてしまう
		/*for (auto itr = audioDatas.begin();
			itr != audioDatas.end();
			itr++)*/
		//オーディオデータの解放
		ReleaseAllAudioData();



		//解放
		//COMの開放も一回でいいので、GameEngineの一番上にでも
		//CoUninitialize();

		//マスターボイス
			//専用の解放関数を呼びこむだけでよい。
			//さらにDeleteしようとするとエラー
		
		//pMasteringVoice->DestroyVoice();
		SAFE_DESTROY_VOICE(pMasteringVoice);
		//××　delete pMasteringVoice;

		//		本体
		SAFE_RELEASE(pXAudio);
		


	}
	
	//オーディオデータの解放
	void Audio::ReleaseAllAudioData()
	{
		//ソースボイスの解放
			//ロードしたオーディオデータの解放
		for (int i = 0;
			i < audioDatas.size();
			i++)
		{

			//まだ解放されていなければ
			if (audioDatas[i] != nullptr)
			{
				//データ配列の動的確保したポインタの解放
				//まだ、他で使用されているpAudioDataデータなどは、解放しないようにする（以下の関数にて）
				ReleaseOneData(i);
			
			}



			

		}
		//演奏者群（Vector）を初期化
		audioDatas.clear();
	}
	void Audio::ReleaseOneData(int handle)
	{
		//考えられるエラーの際に解放を行わずに帰る
		if (handle < 0 || handle >= audioDatas.size() || audioDatas[handle] == nullptr)
		{
			return;
		}


		//停止
			//音を止めて、
			//かつ、ソースボイスからバッファを解放してから、
			//解放処理を行わなければ、　(再生中に解放処理を行うと、)開放中にエラーが起こる

		for (int i = 0; i < audioDatas[handle]->createNum; i++)
		{
			
			audioDatas[handle]->ppSourceVoice[i]->Stop();
			audioDatas[handle]->ppSourceVoice[i]->FlushSourceBuffers();
			
		}




		//解放されていない中に、共通のポインタが存在しない
		bool isExist = false;

		//まだ解放されていない中に、
		//自身の所有しているデータ部のポインタが、ある場合、それは開放しない。
			//解放されていない中になければ、初めて解放する
		for (int i = 0; i < audioDatas.size(); i++)
		{
			
			if (audioDatas[i] != nullptr &&	//まだ解放されていない
				i != handle &&			//自身と同じでない
				audioDatas[i]->buf.pAudioData == audioDatas[handle]->buf.pAudioData)	//pAudioDataに登録されたポインタが同じ
			{
				//解放しないとする
					//→まだ、解放されていないデータの中で共通ポインタを所有している場合消さない
					//→データの中で、配列の一番最後に使われている共通のポインタを　解放　行うようにする。
						//→自分のデータよりも要素が前、後ろのデータの中で同じデータを持っている場合は×。

				//共通データがまだ存在するので解放しない
				isExist = true;
				break;
			}
			//最後まで全データを回して、
			//フラグにtrueが入らなかった場合、
			//まだ、解放されていない中で同じFBXポインタを持つものがいない　＝　FBXポインタを解放
		}
		//解放されていない、他データ内で
		//共通ポインタを使ってなければモデル解放
		if (isExist == false)
		{
			SAFE_DELETE_ARRAY(audioDatas[handle]->buf.pAudioData);	
			/*
			ここで注意が必要なのは、
			バッファの解放をするときに、
			ソースボイスが音楽を再生中であるとき、　解放中にエラーが起こる可能性が高い

			そのため、解放処理の前に、必ず Stop() そして、ソースバイスからバッファの解放であるFlash~~~を呼び込むこと！！
			
			*/
			//https://dixq.net/forum/viewtopic.php?t=4330

			/*const BYTE* pByte = audioDatas[handle]->buf.pAudioData;
			SAFE_DELETE_ARRAY(pByte);*/

			//free((void*)audioDatas[handle]->buf.pAudioData);




			//delete[] audioDatas[handle]->buf.pAudioData;


			//audioDatas[handle]->buf.pAudioData = nullptr;
				//解放時に、nullptrを入れるとエラーになる
				//なので、SAFE_DELETE_ARRAYなどは使えない
					//nullptrにしないと、間違ってアクセスしてしまうことになりかねない
					//だが、構造体のポインタ自体を解放してしまうので、audioDatasの段階でエラーになるのでどちらにしてもアクセスできない
		}
		//ソースボイス内のオーディオデータの解放（newしたものの、ポインタを受け取っている、ここ以外で解放できるポインタを持っている人はほかにいない）
			//すでにロード済みなのであれば、ロード済みのバッファのポインタを受け取るようにしている
			//そして、そのデータが仮に解放されていたとしても、
			//そのポインタにはnullptrを入れるようにしている、SAFE_DELETEは、nullptrのポインタは２重解放しないように条件を付けているため、ここでエラーが起こることはない
		//SAFE_DELETE_ARRAY(audioDatas[i].buf.pAudioData);



		//ソースボイスの解放
				//まず中身のポインタ一つ一つの解放
			//それぞれ確保分
		for (int i = 0; i < audioDatas[handle]->createNum; i++)
		{
			//ポインタの解放
				//解放はマスターボイスと同様の解放関数

			//audioDatas[handle]->ppSourceVoice[i]->DestroyVoice();
			SAFE_DESTROY_VOICE(audioDatas[handle]->ppSourceVoice[i]);

			//audioDatas[i].ppSourceVoice[j]は、ポインタの枠を確保しただけで、実際に動的確保しているわけではない
			//SAFE_DELETE(audioDatas[i].ppSourceVoice[j]);
		}
		//側のポインタ
			//ポインタのポインタは、実際にはポインタのアドレス部分のみ持っている、
			//つまり、側の部分は実態を持っていない。
		//これはただのdeleteで、動的確保の解放
		SAFE_DELETE_ARRAY(audioDatas[handle]->ppSourceVoice);
		


		//ソースボイス内のオーディオデータの解放（newしたものの、ポインタを受け取っている、ここ以外で解放できるポインタを持っている人はほかにいない）
			//すでにロード済みなのであれば、ロード済みのバッファのポインタを受け取るようにしている
			//そして、そのデータが仮に解放されていたとしても、
			//そのポインタにはnullptrを入れるようにしている、SAFE_DELETEは、nullptrのポインタは２重解放しないように条件を付けているため、ここでエラーが起こることはない
		//SAFE_DELETE_ARRAY(audioDatas[i].buf.pAudioData);
		/*if (audioDatas[i]->buf.pAudioData != nullptr)
		{
			delete[] audioDatas[i]->buf.pAudioData;
			audioDatas[i]->buf.pAudioData = nullptr;
		}*/

		SAFE_DELETE(audioDatas[handle]);


	}

