#pragma once

//画像背景を360度にわたって描画するオブジェクト
	//ゲームオブジェクトとしてCubeMapを読み込むオブジェクトを作成し、
	//それを必要な時にインスタンスを作成するようにさせる

#pragma once
#include <d3d11.h>	//専用のコンスタントバッファを使うため
//ID3D11Buffer型

#include "GameObject.h"

class Texture;

//■■シーンを管理するクラス
class SkyBox : public GameObject
{
	int hModel_;

	//親とは別に、
	//SkyBoxが追尾する（つまり、追尾対象の周囲に一定の景色を表示させる対象の）オブジェクトを所有しようとした
	//（実装しなかった理由）一番の問題は、追尾対象が削除されたときに、既に存在しないオブジェクトを指してしまう可能性があるから。
		//→そのため、常に親がいて、その親についていくようにする（Transformの計算の時に、相するようにしているので、問題ない。）	
	//GameObject* pTrackingObj_;

	//環境マップ用の
	//テクスチャの用意
		//モデルに、環境の映り込みを再現させる。
		//箱型のテクスチャを用意し、　そのテクスチャの色を反射し反映させる
		//それにより、背景画像を写すことが可能となり、
			//背景を表示することができる
	Texture* pSkyBoxTex_;



public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SkyBox(GameObject* parent);
	//デストラクタ
	~SkyBox() override;

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
