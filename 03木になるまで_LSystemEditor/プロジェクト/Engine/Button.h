#pragma once
#include <vector>				//ボタン画像移動時に、同様の画像を所有しているとき、すでに移動した画像を移動させないため、一時保存先として持って置く
#include "GameObject.h"

//マウスカーソルと接触判定を行う
//ボタンクラス
	//使い方によっては、ON,OFFを切り替える、スイッチとしても使うことが可能

//ボタンクラスを継承するクラスが、Model.cppを一切触れることのないように、
//ボタンとの接触を行えるようにする。




//ボタンの状態
enum BUTTON_STATUS
{
	OFF_BUTTON = 0,	//OFF , 触れられていない
	ON_BUTTON ,		//ON  , 押された
	//TOUCH,		//	　, 触れられている
	
	MAX_BUTTON_STATUS
};


//前方宣言
class Transform;


//ボタンクラス
class Button : public GameObject
{
private :
	//画像ハンドル
	int hImage_[MAX_BUTTON_STATUS];
		//ボタンの状態それぞれに、画像を用意し、その画像ハンドルを持っておく

	//現在のボタンの状態
	BUTTON_STATUS currentStatus_;

	//ボタンに接触しているか
		//→接触しているときは、必ずOnContactButtonを呼ぶようにするが、
		//→外部から、ボタンに触れているのか、欲しいかもしれないので、フラグとして持っておく
	bool isContact_;

	//各Transform
	Transform* trans[MAX_BUTTON_STATUS];



protected : 
	/*移動関連********************************************************************************************/
	//どの画像を移動させるのかは、関数呼び出し元からはわからない。→つまり、一つのクラスで所有している画像全部を動かすイメージ

	//指定された画像データのTransfromをセットする
	//トランスフォームの位置、サイズ、拡大率を受け取る
	//引数：transform セットするトランスフォーム(&　基本型以外のデータを受け取るときは＆を使用)
	//戻値：なし
	void SetTransform(Transform& transform);
	//指定された画像データのTransfromをセットする
	//トランスフォームの位置、サイズ、拡大率を受け取る
	//引数：transform セットするトランスフォームのポインタ(中身をコピーするだけなので、ポインタでも問題なし、様々な受け渡し方に対応するために作った)
	//戻値：なし
	void SetTransform(Transform* transform);
	//指定された画像データのPositionをピクセル単位で動かす（現在位置から動かす）
	//引数：移動ピクセル位置　X
	//引数：移動ピクセル位置　Y
	void MovePixelPosition(int x, int y);
	//指定された画像データのPositionを引数ピクセル位置に設置する
	//引数ピクセル位置に移動
	//引数位置をワールド座標に変換し　その座標をTransformにする
	//引数：セットピクセル位置　X
	//引数：セットピクセル位置　Y
	void SetPixelPosition(int x, int y);

	//リストの中に引数値が存在するか
	bool IsExits(std::vector<int>& moved, int checkValue);
	
	/*シェーダー関連*/
	//シェーダーを切り替える
	//引数：切り替え先のシェーダータイプ
	void ChangeShader(SHADER_TYPE shaderType);

	/*サイズの可変*/
	void SetPixelScale(int x, int y);

protected :
	/*状態関連********************************************************************************************/
	//現在の状態がOFFであるか、
	bool IsButtonOFF();
	//現在の状態がONであるか、
	bool IsButtonON();



	/*接触関連********************************************************************************************/

	//ボタンとマウスカーソル接触時
		//マウスのテクスチャを変更させる
	//void ChangeButtonTexture(BUTTON_STATUS changeStatus);

	//ボタンのステータス変更
	void ChangeButtonStatus(BUTTON_STATUS changeStatus);




	//ボタンをONにする
		//あくまでステータスをONにするだけ
		//ステータスON,描画するボタン画像ON専用に
	void ButtonON();

	//ボタンをOFFにする
		//あくまでステータスをOFFにするだけ
		//ステータスON,描画するボタン画像OFF専用に
	void ButtonOFF();

	//ボタン押下
		//押し続けているときON,離したときOFF
	void ButtonPress();
	//ボタン押下　押したとき一回
		//現在がONならOFF,　現在がOFFならON
	void ButtonPressOneDown();
	//ボタン押下　離したとき一回
		//現在がONならOFF,　現在がOFFならON
	void ButtonPressOneUp();

	//ボタンのON,OFFのスイッチを切り替える
	//引数:切り替えるもととなる状態
	//戻値:切り替え後の状態
	BUTTON_STATUS ToggleButtonSwitch(BUTTON_STATUS status);


protected : 
	/*Update , Draw , Release実行関連********************************************************************************************/
	//ボタン画像のロード
		//必ず一枚は画像をロードさせ、（OFFの時）
		//そのほかの、ON,TOUCHなどの画像は、自ら、ロードさせるか、使わない場合は、上記のOFF時の画像を使用する
	//引数:テクスチャファイル名
	//引数:ロードした画像のハンドルを登録する番号（配列の添え字）（指定がない場合はOFF＝０）
	void Load(const std::string& fileName, BUTTON_STATUS status = OFF_BUTTON);
	//ロード
	//すでにロードされている場合は何もしない。
	//まだロードされていない場合はすでにロードされている画像を共通して扱う
	//引数:ロードした画像のハンドルを登録する番号（配列の添え字）
	bool Load(BUTTON_STATUS status);

	//引数の状態にて示されるボタン画像がすでにロード済みかを調べる
	//引数:画像のハンドルを登録する番号（配列の添え字）
	void CheckIfLoaded(BUTTON_STATUS status);


	//ロード済みでなかった配列要素に、ロード済みの要素をコピーさせる
	//引数:ロード済みでなかった配列要素の添え字（コピー先）
	//引数:ロード済みであった配列要素の添え字（コピー元）
	void CopyLoadedInformation(int copyTo , int original);




	//ボタンの更新処理
		//ボタンと、現在のカーソル位置との当たり判定を行う
		//当たっていない場合：何もしない
		//当たっている場合　：OnCollision関数を呼びこむ
	void UpdateButtonForOnContact();

	//ボタンを描画
	void DrawButton();

	//ボタンクラスの動的確保情報解放
	void ReleaseButton();

	//接触時の処理
	//接触しているときに、毎フレーム呼ばれるので注意
	//継承先にてオーバーライドして、ボタンと接触しているときの処理を行う
		//例：接触しているときに、マウスのクリックが押されたときに、　ボタンをONとして、何か行動させる
		//例：接触しているときに、マウスのクリックが押されたときに、　ボタンをOFFとして、何か行動させる
	virtual void OnContactButton();
	//ボタンに触れているときには、毎フレーム呼ばれるので、
	//継承先のOnContactButton()のオーバーライドにて、ボタンを押し続けている間、ONにして、離したとき、OFFにするという、処理を書けば、自動でスイッチがOFFになるボタンも作れる。

	//あるいは、ボタンの機能で、スイッチ式か、自動戻りスイッチ式かで、選択して、OnContactButtonとは別に、押したときの処理関数も作る？


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Button(GameObject* parent);

	//デストラクタ
	virtual ~Button();

	//初期化
	virtual void Initialize() = 0;
	

	//更新
	virtual void Update() = 0;
	

	//描画
	virtual void Draw() = 0;


	//開放
	virtual void Release() = 0;

	


};