#pragma once
#include <DirectXMath.h>

using namespace DirectX;

class Transform;

//デバック用のコライダーの範囲を示す
//ワイヤーフレームオブジェクト

//其のモデルを読み込み、管理するオブジェクト
class DebugWireFrame
{
private : 
	//ワイヤーフレームのモデルのモデル番号
	int hModel_;
	//描画、非描画を示す、フラグ
	bool enable_;

	//自身のTransformを保存しておくクラス
		//Pos,Scaleなどを別々で、取得し、管理するため、Transformを所有して起き、描画の時に用いる
	Transform* transform_;


public :
	DebugWireFrame();
	~DebugWireFrame();

	//親をセット
	void SetParentTransform(Transform* pTrans);


	//ロード
	//引数：モデルタイプ（コライダータイプ）
	void Load(int type);

	//サイズ可変
	void SetPosition(XMVECTOR& position);
	//スケール可変
		//ベクトルで取得
		//箱型　コライダー時のスケール可変に用いる
	void SetScale(XMVECTOR& scale);
		//半径を表すflaotで取得
		//球型　コライダー時のスケール可変に用いる
	void SetScale(float radius);


	//描画許可,非許可
	void DrawWireFrame(bool drawWireFrame);
	//描画
	void Draw();



	//引数ベクトルと、Fbxと衝突した面の法線ベクトルを帰す
	//引数：面との衝突判定を行うベクトルのスタート地点
	//引数：面との衝突判定を行うベクトルの方向
	//戻値：衝突した面の法線ベクトル
	XMVECTOR NormalVectorOfCollidingFace(XMVECTOR& start, XMVECTOR& dir);


};

