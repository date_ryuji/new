#pragma once
#include <d3d11.h>
#include "string"

class Texture
{
	ID3D11SamplerState*	pSampler_;	//サンプラー　テクスチャを入れる〜
	ID3D11ShaderResourceView*	pSRV_;	//~ビュー　絵描きが、紙に書くための橋渡し、（プリンターのような人）
										//テクスチャとシェーダーの間を持つ人
											//ピクセルシェーダーに「橋渡しをする人」



	//画像のサイズ保存
	UINT imgWidth_;	//画像の横幅
	UINT imgHeight_;	//画像の縦幅

public:
	Texture();
	~Texture();
	//png , jpeg
	HRESULT Load(std::string fileName);
	//string :標準を使うためにstd
	HRESULT Load(ID3D11Texture2D* pTexture);

	//DDS(cubeのテクスチャ)
	HRESULT LoadCube(std::string fileName);

	void Release();

	//ゲッター
	ID3D11SamplerState* GetSampler() { return pSampler_; };	//サンプラーを送る
	ID3D11ShaderResourceView* GetSRV() { return pSRV_; };	//サンプラーの橋渡しを送る

	//画像の横幅を送る
	UINT GetWidth() { return imgWidth_; };	
	//画像の縦幅を送る
	UINT GetHeight() { return imgHeight_; };	


	//テクスチャのバッファーを取得
		//引数にて渡されたテクスチャに、
		//自身のテクスチャのバッファー部分を入れて返す
	//引数：ファイル名
	//引数：バッファーのポインタのアドレス
	HRESULT GetTextureBuffer(std::string fileName  , ID3D11Texture2D** pBuffer);

	HRESULT GetTextureBuffer(std::string fileName, 
		ID3D11Texture2D** pBuffer , D3D11_MAPPED_SUBRESOURCE* hMappedres);


};