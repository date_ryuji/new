#pragma once
#include "../Shader/CommonDatas.h"

//FactoryMethodパターンを使用して、
//派生クラスのインスタンス生成を請け負って、それを渡すクラス
	//Direct3Dにて使用するシェーダークラスのインスタンスを生成し、返す
	//Direct3Dにて、インクルードを多く記入したくないので、
	//シェーダーを作るクラスを作っておき、それにより、派生クラスの依存性をなくす。
	//Direct3Dは、ただ、作られたものを受け取るだけでよくする


//継承元クラスの前方宣言
class PolygonGroup;


//シェーダーを作るクラス
	//シェーダーを作る工場
class PolygonGroupFactory
{
public:
	//PolygonGroupのインスタンスを作成し、
		//インスタンスを返す
	PolygonGroup* CreatePolygonGroupClass(POLYGON_GROUP_TYPE type);
};

