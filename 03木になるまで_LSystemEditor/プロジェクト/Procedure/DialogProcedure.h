#pragma once
#include <Windows.h>
#include <commctrl.h>	//ダイアログのスクロールバーを操作するための関数、定数群を持つヘッダ
#include <string>
//GameObjectクラスを継承したクラス
//すべてのObjectの一番上の親となるオブジェクト（オブジェクトは親をたどるとRootObjectに必ずたどり着く→たどり着かないオブジェクトは、GameObjectの家系にオブジェクトが作られていない）
#include "../Engine/RootJob.h"
//ダイアログ表示のための、ID番号を持っているもの
#include "../resource.h"



#pragma comment(lib, "ComCtl32.lib") //ライブラリのリンク


//最終的にシングルトンクラスにしたいクラス
//現段階では名前空間にしておく
	//シングルトン作成において参考サイト
	//https://qiita.com/kikuuuty/items/fcf5f7df2f0493c437dc
//インスタンスは、一つだけ生成し
	//そのインスタンスを共通して使うようにしたい
	//そのインスタンスから、とあるダイアログの表示を管理したり、ダイアログ番号を取得したりする。




//シングルトンを使うにあたって
	//メンバ、などの明示的表現
	//https://cpprefjp.github.io/lang/cpp11/defaulted_and_deleted_functions.html



//ダイアログのプロシージャ
BOOL CALLBACK DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);

//ダイアログ（CreateMap用）
	//初期化の段階で、
	//ダイアログのハンドルの段階で、いろいろとハンドルの違いでアクセスができなくなることがある
	//そのため、関数を分けて、処理をするようにする
BOOL CALLBACK DialogProcForCreateMap(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);
//ダイアログ（LSystemEditor用）
BOOL CALLBACK DialogProcForLSystemEditor(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);



#define AA 200
namespace ttt
{
#define BB 200

	static constexpr int MAX_WIDTH_ = 900;


}



enum DIALOG_TYPE
{
	//NEW_FILE
	DIALOG_TYPE_NEW_FILE = 0,
	//VERTEX_CHANGER
	DIALOG_TYPE_VERTEX_CHANGER ,
	//LSYSTEM
	DIALOG_TYPE_LSYSTEM_EDITOR,


	DIALOG_MAX,

};


namespace DialogProcedure
{
	//初期化
	void Initialize(const int nCmdShow);

	//解放
	void Release();

	//RootJobをセット
	void SetRootJob(RootJob* pRootJob);


	//ダイアログ作成
		//共通のダイアログプロシージャを使用する場合、
		//下記の関数にてダイアログプロシージャを指定し使用する
		//ただし、ダイアログごとにダイアログプロシージャを分けている方が、管理がしやすく、呼ぶ関数もダイアログごとになるため、分別できる。
			//そのため、ダイアログプロシージャの関数ごとに関数を分けているのが、以下のCreateDialog〜から、始まる関数である。			
	void CreateDialogLeavedItToClass(DIALOG_TYPE type, HINSTANCE hInstance, HWND hWnd);

	//CreateMap専用のダイアログの作成
	void CreateDialogForCreateMapLeavedItToClass(DIALOG_TYPE type, HINSTANCE hInstance, HWND hWnd);

	//LSYSTEM専用のダイアログの作成
	void CreateDialogForLSystemEditor(DIALOG_TYPE type, HINSTANCE hInstance, HWND hWnd);


	//ダイアログ表示
	void ShowDialogLeavedItToClass(DIALOG_TYPE type);

	//ダイアログ非表示
	void CloseDialogLeavedItToClass(DIALOG_TYPE type);



}

//class DialogProcedure
//{
//	//ダイアログハンドル群
//	HWND hDialogs_[DIALOG_MAX];
//
//	//ダイアログのリソース番号
//	int resourceHandle_[DIALOG_MAX];
//
//
//
//	//WinMainの第4引数
//		//ウィンドウを表示するときに必要
//	const int nCmdShow_;
//
//private : 
//	////デフォルトコンストラクタ
//	//DialogProcedure() = default;
//	////デフォルトデストラクタ
//	//~DialogProcedure() = default;
//
//	//自身のインスタンス
//	static DialogProcedure* instance;
//
//
//
//private : 
//	//ダイアログの
//	//resource.hにおける番号を、配列に登録する
//	void SetResourceHandle();
//
//
//public:
//	//コンストラクタ
//	DialogProcedure(const int nCmdShow);
//	//コンストラクタ
//	DialogProcedure();
//
//
//	//デストラクタ
//	~DialogProcedure();
//
//	//解放
//	void Release();
//
//
//	//RootJobをセット
//	void SetRootJob(RootJob* pRootJob);
//
//
//
//	//ダイアログ作成
//	void CreateDialogLeavedItToClass(DIALOG_TYPE type , HINSTANCE hInstance , HWND hWnd);
//
//	//ダイアログ表示
//	void ShowDialogLeavedItToClass(DIALOG_TYPE type);
//
//	//ダイアログ非表示
//	void CloseDialogLeavedItToClass(DIALOG_TYPE type);
//
//public : 
//
//	DialogProcedure(const DialogProcedure&) = delete;
//	DialogProcedure& operator=(const DialogProcedure&) = delete;
//	
//	DialogProcedure(DialogProcedure&&) = delete;
//	DialogProcedure& operator=(DialogProcedure&&) = delete;
//
//
//	static DialogProcedure& GetSingletonInstance()
//	{
//		return *instance;
//	}
//
//	static void CreateInstance(const int nCmdShow)
//	{
//		if (!instance)
//		{
//			instance = new DialogProcedure(nCmdShow);
//		}
//	}
//	static void destroy()
//	{
//		delete instance;
//		instance = nullptr;
//	}
//
//
//};
//
