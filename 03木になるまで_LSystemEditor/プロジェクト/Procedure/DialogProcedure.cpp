#include "DialogProcedure.h"

//ダイアログ操作を望むクラス
//#include "../MapEditorScene/MapEditManager.h"
//#include "../CreateMapScene/CreateMapManager.h"
#include "../LSystemEditorScene/LSystemEditorSceneManager.h"




namespace DialogProcedure
{
/*変数・配列*/
	//ダイアログハンドル群
	HWND hDialogs_[DIALOG_MAX];

	//ダイアログのリソース番号
	int resourceHandle_[DIALOG_MAX];
	//WinMainの第4引数
		//ウィンドウを表示するときに必要
	int nCmdShow_;


	//全てのゲームオブジェクトの親
	RootJob* pRootJob_ = nullptr;




/*関数*/
	//ダイアログの
	//resource.hにおける番号を、配列に登録する
	void SetResourceHandle();

	//コンボボックスを設定するためのSendMessageを送る関数
		//処理を共通化
	void CreateCOMBOBOXInfo(const HWND &hDlg, const char* text, const int ID);

}


void DialogProcedure::SetResourceHandle()
{
	resourceHandle_[DIALOG_TYPE_NEW_FILE] = DIALOG_NEW_FILE;
	resourceHandle_[DIALOG_TYPE_VERTEX_CHANGER] = DIALOG_VERTEX_CHANGER;
	resourceHandle_[DIALOG_TYPE_LSYSTEM_EDITOR] = DIALOG_LSYSTEM;

}


void DialogProcedure::Initialize(const int nCmdShow)
{
	nCmdShow_ = nCmdShow;


	for (int i = 0; i < DIALOG_MAX; i++)
	{
		hDialogs_[i] = NULL;
	}
	SetResourceHandle();

}

void DialogProcedure::Release()
{
	pRootJob_ = nullptr;
}

void DialogProcedure::SetRootJob(RootJob * pRootJob)
{
	pRootJob_ = pRootJob;
}

void DialogProcedure::CreateDialogLeavedItToClass(DIALOG_TYPE type, HINSTANCE hInstance, HWND hWnd)
{

	//ダイアログ作成関数を呼びこみ、返り値のダイアログハンドルを登録
	//引数：WinMainの第1引数
	//引数：ダイアログのリソースハンドル(resource.hにおけるハンドル番号)
	//引数：ウィンドウハンドル
	//引数：ウィンドウプロシージャの関数ポインタ
	hDialogs_[type] = 
	CreateDialog(hInstance, MAKEINTRESOURCE(resourceHandle_[type]), hWnd, 
		(DLGPROC)DialogProc);

}

void DialogProcedure::CreateDialogForCreateMapLeavedItToClass(DIALOG_TYPE type, HINSTANCE hInstance, HWND hWnd)
{
	//ダイアログ作成関数を呼びこみ、返り値のダイアログハンドルを登録
	//引数：WinMainの第1引数
	//引数：ダイアログのリソースハンドル(resource.hにおけるハンドル番号)
	//引数：ウィンドウハンドル
	//引数：ウィンドウプロシージャの関数ポインタ
	hDialogs_[type] =
		CreateDialog(hInstance, MAKEINTRESOURCE(resourceHandle_[type]), hWnd, 
			(DLGPROC)DialogProcForCreateMap);

}

void DialogProcedure::CreateDialogForLSystemEditor(DIALOG_TYPE type, HINSTANCE hInstance, HWND hWnd)
{
	//ダイアログ作成関数を呼びこみ、返り値のダイアログハンドルを登録
//引数：WinMainの第1引数
//引数：ダイアログのリソースハンドル(resource.hにおけるハンドル番号)（ダイアログ自体のハンドル）
//引数：ウィンドウハンドル
//引数：ウィンドウプロシージャの関数ポインタ
	hDialogs_[type] =
		CreateDialog(hInstance, MAKEINTRESOURCE(resourceHandle_[type]), hWnd, 
			(DLGPROC)DialogProcForLSystemEditor);

}


void DialogProcedure::ShowDialogLeavedItToClass(DIALOG_TYPE type)
{
	//ダイアログもウィンドウの1つなので、
	//ShowWindowにて、表示を管理
	//引数：ダイアログのハンドル
	//引数：WinMainの第4引数（共通）
	if (hDialogs_[type] != NULL)
	{
		ShowWindow(hDialogs_[type], nCmdShow_);
	}
	

}

void DialogProcedure::CloseDialogLeavedItToClass(DIALOG_TYPE type)
{
	//表示の逆で
	//Closeを呼べばよい
	if (hDialogs_[type] != NULL)
	{
		CloseWindow(hDialogs_[type]);
	}
}


#if 0
//本物
//ダイアログのプロシージャ
	//メインウィンドウではない、他のウィンドウ。ボタンなどなど。
	//ウィンドウプロシージャのように、ダイアログに何かされたら、そのメッセージと、された内容を引数に入れて、呼ばれる
//引数は、ウィンドウプロシージャと同じ
	//引数：ダイアログハンドル
	//引数：メッセージ内容
	//引数：メッセージ内容に対する、ダイアログにされた動作などのいろいろが入っている（マウスが動かされたなら、ｘ座標が入る。みたいに、メッセージごとに別々の内容が入ってくる）
	//引数：メッセージ内容に対する、ダイアログにされた動作などのいろいろが入っている（マウスが動かされたなら、ｙ座標が入る。みたいに、）
//コントロール＝ダイアログのボタンなど
//だが、ダイアログのメッセージは、trueか、falseを返すだけ。
	//そこから、各ダイアログのコントロールに処理を送りたいときは、SendMessageにて、コントロールを指定して処理を行う
BOOL DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	//Mapにて操作したいことが多いので、
//実際の操作は、Mapの関数にて行うようにする。

//そのために、本物の、プロシージャにメッセージが来たら、Mapのほうの偽物の関数を呼ぶ

//だが、マップの関数をどのように呼ぶか？
	//�@一番上からマップに辿っていく

	//�AMapという名前のオブジェクトを探す。

//�@一番上がいて、その間に中間がいるならば、その人を使わない手はないが、
//�A間を介するのも大変なので、一気に探すのも楽でよい

//何かを処理したら、trueを返すだけ
//msgに対する
	switch (msg)
	{
		/*
		WM_INITDIALOG・・・ダイアログが表示された（最初にやりたいことを書く）


		WM_COMMAND・・・ダイアログ上のパーツや、メニューの項目に触れた（LOWORD(wp)に触ったパーツのIDが入っている）


		*/

		//初期化は、ゲームオブジェクト外
		//最初にダイアログが表示されたとき
	case WM_INITDIALOG:
		//メッセージを強制定期に送る
			//見えない、ラジオボタンに対してのダイアログプロシージャに対してのメッセージを送る
			//コントロールの相手を指定されて、その相手にメッセージを強制的に送り、処理をしてもらう
		//第１引数：（コントロールハンドル）ラジオボタンのコントロールハンドル（それぞれボタンごとの番号）
		//第２引数：（メッセージ）ボタンにチェック
		//第３引数：（情報A）チェック
		//第４引数：（情報B）なし
	/*	SendMessage(GetDlgItem(hDlg, RADIO_UP),
			BM_SETCHECK,
			BST_CHECKED, 0);
	*/



		CreateCOMBOBOXInfo(hDlg, "カスミアジ", TYPE_COMBO);
		CreateCOMBOBOXInfo(hDlg, "アオマスク", TYPE_COMBO);
		CreateCOMBOBOXInfo(hDlg, "ナンヨウハギ", TYPE_COMBO);
		CreateCOMBOBOXInfo(hDlg, "チョウチョウウオ", TYPE_COMBO);
		CreateCOMBOBOXInfo(hDlg, "カクレクマノミ", TYPE_COMBO);
		CreateCOMBOBOXInfo(hDlg, "ツノダシ", TYPE_COMBO);
		CreateCOMBOBOXInfo(hDlg, "エイ", TYPE_COMBO);
		CreateCOMBOBOXInfo(hDlg, "フタスジタマガシラ", TYPE_COMBO);
		CreateCOMBOBOXInfo(hDlg, "チンアナゴ", TYPE_COMBO);
		CreateCOMBOBOXInfo(hDlg, "ジンベイザメ", TYPE_COMBO);

		//コントロールの選択。ボタンの選択の強制選択。
		//第１引数：（コントロールハンドル）コンボボックスのコントロールハンドル（それぞれボタンごとの番号）
		//第２引数：（メッセージ）コンボボックスの初期値を選択。
					//コンボボックスを開いたときに、あらかじめ選択されているもの
		//第３引数：（情報A）初期値とする、（選択している状態にする）値の番号（追加順で０から）
		//第４引数：（情報B）なし
		SendMessage(GetDlgItem(hDlg, TYPE_COMBO),
			CB_SETCURSEL,
			0, 0);


		CreateCOMBOBOXInfo(hDlg, "S", PRIORITY_COMBO);
		CreateCOMBOBOXInfo(hDlg, "A", PRIORITY_COMBO);
		CreateCOMBOBOXInfo(hDlg, "B", PRIORITY_COMBO);
		CreateCOMBOBOXInfo(hDlg, "C", PRIORITY_COMBO);
		CreateCOMBOBOXInfo(hDlg, "D", PRIORITY_COMBO);
		SendMessage(GetDlgItem(hDlg, PRIORITY_COMBO),
			CB_SETCURSEL,
			0, 0);

		CreateCOMBOBOXInfo(hDlg, "0.0f　　海面に最も近い", HEIGHT_COMBO);
		CreateCOMBOBOXInfo(hDlg, "0.1f ", HEIGHT_COMBO);
		CreateCOMBOBOXInfo(hDlg, "0.2f　 　海中上部", HEIGHT_COMBO);
		CreateCOMBOBOXInfo(hDlg, "0.3f ", HEIGHT_COMBO);
		CreateCOMBOBOXInfo(hDlg, "0.4f ", HEIGHT_COMBO);
		CreateCOMBOBOXInfo(hDlg, "0.5f　　海中中部", HEIGHT_COMBO);
		CreateCOMBOBOXInfo(hDlg, "0.6f ", HEIGHT_COMBO);
		CreateCOMBOBOXInfo(hDlg, "0.7f　　海中下部", HEIGHT_COMBO);
		CreateCOMBOBOXInfo(hDlg, "0.8f ", HEIGHT_COMBO);
		CreateCOMBOBOXInfo(hDlg, "0.9f ", HEIGHT_COMBO);
		CreateCOMBOBOXInfo(hDlg, "1.0f　　地面に沿う", HEIGHT_COMBO);
		SendMessage(GetDlgItem(hDlg, HEIGHT_COMBO),
			CB_SETCURSEL,
			0, 0);




		return TRUE;

		//何かメッセージを受け取ったら、メッセージ全般
	case WM_COMMAND:
		//ダイアログ作成時点で、
		//Mapはまだ、できていない可能性もある、

		//ゲームオブジェクトから探すとして、
		//RootJobを呼ばないといけない。
		if (pRootJob_)//ルートジョブが確保されていれば、（nullptrでなければ）
		{

			FishMetaAI* pFishMetaAI = (FishMetaAI*)pRootJob_->FindObject("FishMetaAI");
			if (pFishMetaAI)
			{
				return pFishMetaAI->DialogProc(hDlg, msg, wp, lp);
			}


			/*
			//マップオブジェクトを取得
			Map* map = (Map*)pRootJob->FindObject("Map");
			//マップも見つかったなら
			if (map)
			{
				//偽物のMapの所有するダイアログプロシージャを呼ぶ（引数同じ）
				//返り値は、bool型なので
				return map->DialogProc(hDlg, msg, wp, lp);
			}
			*/

		}

		return TRUE;

	}



	//自分でメッセージを作ることもできるので、
	//初期化というフラグのメッセージを作っておいて、そのメッセージなら初期化をするという処理でもいいし。

	//初期化は、最初に表示された時点で呼んでほしい。
	//RootJobなどの条件を付ける段階では、表示の命令、case文をしないといけないときに、ifを通ることができない。


	return FALSE;


}
BOOL DialogProcForCreateMap(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	return 0;
}
#endif



//本物
//ダイアログのプロシージャ
	//メインウィンドウではない、他のウィンドウ。ボタンなどなど。
	//ウィンドウプロシージャのように、ダイアログに何かされたら、そのメッセージと、された内容を引数に入れて、呼ばれる
//引数は、ウィンドウプロシージャと同じ
	//引数：ダイアログハンドル
	//引数：メッセージ内容
	//引数：メッセージ内容に対する、ダイアログにされた動作などのいろいろが入っている（マウスが動かされたなら、ｘ座標が入る。みたいに、メッセージごとに別々の内容が入ってくる）
	//引数：メッセージ内容に対する、ダイアログにされた動作などのいろいろが入っている（マウスが動かされたなら、ｙ座標が入る。みたいに、）
//コントロール＝ダイアログのボタンなど
//だが、ダイアログのメッセージは、trueか、falseを返すだけ。
	//そこから、各ダイアログのコントロールに処理を送りたいときは、SendMessageにて、コントロールを指定して処理を行う
BOOL CALLBACK DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	//Mapにて操作したいことが多いので、
	//実際の操作は、Mapの関数にて行うようにする。

	//そのために、本物の、プロシージャにメッセージが来たら、Mapのほうの偽物の関数を呼ぶ

	//だが、マップの関数をどのように呼ぶか？
		//�@一番上からマップに辿っていく

		//�AMapという名前のオブジェクトを探す。

	//�@一番上がいて、その間に中間がいるならば、その人を使わない手はないが、
	//�A間を介するのも大変なので、一気に探すのも楽でよい

	//何かを処理したら、trueを返すだけ
	//msgに対する
	switch (msg)
	{
		/*
		WM_INITDIALOG・・・ダイアログが表示された（最初にやりたいことを書く）


		WM_COMMAND・・・ダイアログ上のパーツや、メニューの項目に触れた（LOWORD(wp)に触ったパーツのIDが入っている）


		*/


	//初期化
	//初期化は、ゲームオブジェクト外
	//最初にダイアログが表示されたとき
	case WM_INITDIALOG:
		///*DIALOG_VERTEX_CHANGER*********************************************************************/
		//	//コンボボックスは、作成段階で、SortをFalseにしているので、追加したものから並ぶ
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "降下", COMBO_BLUSH_TYPE);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg , "上昇" , COMBO_BLUSH_TYPE);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "スムーズ", COMBO_BLUSH_TYPE);

		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "１倍", COMBO_BLUSH_RANGE);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "２倍", COMBO_BLUSH_RANGE);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "３倍", COMBO_BLUSH_RANGE);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "４倍", COMBO_BLUSH_RANGE);

		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "0.01", COMBO_BLUSH_SIZE);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "0.02", COMBO_BLUSH_SIZE);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "0.03", COMBO_BLUSH_SIZE);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "0.04", COMBO_BLUSH_SIZE);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "0.05", COMBO_BLUSH_SIZE);




		////強制的に
		////カーソルをセットさせる
		////コントロールの選択。ボタンの選択の強制選択。
		////第１引数：（コントロールハンドル）コンボボックスのコントロールハンドル（それぞれボタンごとの番号）
		////第２引数：（メッセージ）コンボボックスの初期値を選択。
		//			//コンボボックスを開いたときに、あらかじめ選択されているもの
		////第３引数：（情報A）初期値とする（選択している状態にする）値の番号（追加順で０から）
		////第４引数：（情報B）なし
		//SendMessage(GetDlgItem(hDlg, COMBO_BLUSH_TYPE),
		//	CB_SETCURSEL,
		//	0, 0);
		//SendMessage(GetDlgItem(hDlg, COMBO_BLUSH_RANGE),
		//	CB_SETCURSEL,
		//	0, 0);
		//SendMessage(GetDlgItem(hDlg, COMBO_BLUSH_SIZE),
		//	CB_SETCURSEL,
		//	0, 0);

		break;


		//何かメッセージを受け取ったら、メッセージ全般
	case WM_COMMAND:
		/*int depth = SendDlgItemMessage((HWND)lp, SLIDER_DEPTH, TBM_GETPOS, 0, 0);
		std::string str = std::to_string(depth);
		SetDlgItemText(hDlg, TEXT_DEPTH, str.c_str());
*/






		//if (DialogProcedure::pRootJob_)//ルートジョブが確保されていれば、（nullptrでなければ）
		//{
		//	//メッセージの受取先のクラスを確保して
		//	MapEditManager* pMapEditmanager = (MapEditManager*)DialogProcedure::pRootJob_->FindObject("MapEditManager");
		//	if (pMapEditmanager)
		//	{
		//		//クラスの持っている、偽のダイアログプロシージャにメッセージを送る
		//			//そのため、ゲームの中身に関する処理は、そのクラス内にて行わせる
		//		return pMapEditmanager->DialogProc(hDlg, msg, wp, lp);
		//	}
		//	
		//}




		
		break;


	}



	//自分でメッセージを作ることもできるので、
	//初期化というフラグのメッセージを作っておいて、そのメッセージなら初期化をするという処理でもいいし。

	//初期化は、最初に表示された時点で呼んでほしい。
	//RootJobなどの条件を付ける段階では、表示の命令、case文をしないといけないときに、ifを通ることができない。


	return FALSE;

}

//本物のプロシージャ
//CreateMapの偽物のダイアログプロシージャにアクセスするためのプロシージャ
//	→本物のプロシージャーにダイアログが操作された時の呼ばれて、そこから、CreateMapクラスにある、偽物のダイアログプロシージャへアクセスする
//CreateMap専用のダイアログプロシージャ
	//ダイアログごとに、プロシージャーを分ける
BOOL CALLBACK DialogProcForCreateMap(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (msg)
	{
		//初期化
		//初期化は、ゲームオブジェクト外(ゲームオブジェクトでやろうとすると、　初めて表示されたときに、ゲームオブジェクトがなくて呼ぶことができないかもしれない)
		//最初にダイアログが表示されたとき
	case WM_INITDIALOG:
	{

		///*NEW_FILE**********************************************************************/
		////スタティックテキスト
		//	//テキスト内容の更新
		////SetDlgItemText(hDlg, TEXT_WIDTH, "2");
		//SetDlgItemText(hDlg, TEXT_DEPTH, "2");
		//SetDlgItemText(hDlg, TEXT_POLY_SIZE, "0.01");

		////コンボボックスに追加
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "2", COMBO_WIDTH);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "4", COMBO_WIDTH);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "8", COMBO_WIDTH);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "16", COMBO_WIDTH);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "32", COMBO_WIDTH);
		//DialogProcedure::CreateCOMBOBOXInfo(hDlg, "64", COMBO_WIDTH);

		////強制的に
		////カーソルをセットさせる
		//SendMessage(GetDlgItem(hDlg, COMBO_WIDTH),
		//	CB_SETCURSEL,
		//	0, 0);


		////任意の位置にセット
		////SendDlgItemMessage(hDlg, SLIDER_DEPTH, TBM_SETPOS, TRUE, 2);



	}break;


	//何かメッセージを受け取ったら、メッセージ全般
	case WM_COMMAND:
	//スクロールのバーが触られたとき	//★スライダーは、//WMCOMMANDで返してくれない	//専用のコマンドを必要とする
	case WM_HSCROLL:
	case WM_VSCROLL:
	{
		//if (DialogProcedure::pRootJob_)//ルートジョブが確保されていれば、（nullptrでなければ）
		//{
		//	//メッセージの受取先のクラスを確保して
		//	CreateMapManager* pCreateMap = (CreateMapManager*)DialogProcedure::pRootJob_->FindObject("CreateMapManager");
		//	if (pCreateMap)
		//	{
		//		//クラスの持っている、偽のダイアログプロシージャにメッセージを送る
		//			//そのため、ゲームの中身に関する処理は、そのクラス内にて行わせる
		//		return pCreateMap->DialogProc(hDlg, msg, wp, lp);
		//	}
		//}
	
	}break;


	}

	return FALSE;



}

BOOL CALLBACK DialogProcForLSystemEditor(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (msg)
	{
		//初期化
		//初期化は、ゲームオブジェクト外(ゲームオブジェクトでやろうとすると、　初めて表示されたときに、ゲームオブジェクトがなくて呼ぶことができないかもしれない)
		//最初にダイアログが表示されたとき
	case WM_INITDIALOG:
	{
		//テキスト更新
		SetDlgItemText(hDlg, LSYSTEM_OUTPUT_TEXT, "");

		//コンボボックスの値の追加
			//追加順でコンボボックスに並ぶ
		DialogProcedure::CreateCOMBOBOXInfo(hDlg, "通常（緑）", LSYSTEM_TEXTURE_TYPE);
		DialogProcedure::CreateCOMBOBOXInfo(hDlg, "ダミー（薄緑）", LSYSTEM_TEXTURE_TYPE);
		DialogProcedure::CreateCOMBOBOXInfo(hDlg, "バインド（紫）", LSYSTEM_TEXTURE_TYPE);
		DialogProcedure::CreateCOMBOBOXInfo(hDlg, "イーター（赤）", LSYSTEM_TEXTURE_TYPE);

		//強制的に
		//カーソルをセットさせる
		//０
		SendMessage(GetDlgItem(hDlg, LSYSTEM_TEXTURE_TYPE),
			CB_SETCURSEL,
			0, 0);


	}break;


	//何かメッセージを受け取ったら、メッセージ全般
	case WM_COMMAND:
	{
		if (DialogProcedure::pRootJob_)//ルートジョブが確保されていれば、（nullptrでなければ）
		{
			//メッセージの受取先のクラスを確保して
			LSystemEditorSceneManager* pLSystemEditor = (LSystemEditorSceneManager*)DialogProcedure::pRootJob_->FindObject("LSystemEditorSceneManager");
			if (pLSystemEditor)
			{
				//クラスの持っている、偽のダイアログプロシージャにメッセージを送る
					//そのため、ゲームの中身に関する処理は、そのクラス内にて行わせる
				return pLSystemEditor->DialogProc(hDlg, msg, wp, lp);
			}
		}

	}break;


	}

	return FALSE;



}


void DialogProcedure::CreateCOMBOBOXInfo(const HWND & hDlg, const char * text, const int ID)
{
	{
		//メッセージを強制定期に送る
			//見えない、ラジオボタンに対してのダイアログプロシージャに対してのメッセージを送る
			//コントロールの相手を指定されて、その相手にメッセージを強制的に送り、処理をしてもらう
		//第１引数：（コントロールハンドル）ラジオボタンのコントロールハンドル（それぞれボタンごとの番号）
		//第２引数：（メッセージ）ボタンにチェック
		//第３引数：（情報A）チェック
		//第４引数：（情報B）なし
		/*	
			SendMessage(GetDlgItem(hDlg, RADIO_UP),
			BM_SETCHECK,
			BST_CHECKED, 0);
		*/	



		//コンボボックス
			//選択肢の追加
			//第１引数：（コントロールハンドル）コンボボックスのコントロールハンドル（それぞれボタンごとの番号）
			//第２引数：（メッセージ）文字列の追加
			//第３引数：（情報A）なし
			//第４引数：（情報B）出力文字列
		SendMessage(GetDlgItem(hDlg, ID),
			CB_ADDSTRING,
			0, (LPARAM)text);
	}
}
