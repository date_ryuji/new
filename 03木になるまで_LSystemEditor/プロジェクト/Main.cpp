/*
	ゲームプログラムの構造

	WinMain(メッセージループ)　
	→　RootJob(GameObject継承)　
		→ SceneManager(GameObject継承)　
		→　各Scene(GameObject継承)　
		→　各シーン内オブジェクト(GameObject継承)


	※（オブジェクトは親をたどるとRootObjectに必ずたどり着く→たどり着かないオブジェクトは、GameObjectの家系にオブジェクトが作られていない）
*/



//インクルード
#include <Windows.h>
#include <stdlib.h>	//時間を計測する

#include "Engine/Direct3D.h"	//Direct３D
#include "Engine/Direct2D.h"	//Direct２D
#include "Engine/Input.h"
#include "Engine/Audio.h"


//GameObjectクラスを継承したクラス
//すべてのObjectの一番上の親となるオブジェクト（オブジェクトは親をたどるとRootObjectに必ずたどり着く→たどり着かないオブジェクトは、GameObjectの家系にオブジェクトが作られていない）
#include "Engine/RootJob.h"

#include "Engine/Global.h"

#include "Engine/Timer.h"	//シーン経過時間を計測



//ウィンドウプロシージャー所有の名前空間
	//ウィンドウに何かあったときなどに呼ばれる関数と、その時の処理群を書いたクラス
#include "Procedure/WindowProcedure.h"
//ダイアログプロシージャー所有の名前空間
	//ダイアログに何かあったときに呼ばれる関数と、その時の処理群を書いたクラス
#include "Procedure/DialogProcedure.h"

//ダイアログやメニューなどのリソース
#include "resource.h"

//ダイアログをゲームループ内で生成したオブジェクトで管理するためのクラス
#include "MapEditorScene/MapEditManager.h"




#pragma comment(lib, "winmm.lib")	//stdlib
									//Windowsマルチメディアの略

//★DirectX関連は、Direct３Dに入れるので、そっちのソースにて
//DirectXを使うものを、名前空間として確保しておく
//→名前空間は、イメージはクラスと同じ


//teisuuu（値そのままは恥ずかしいので、定数化）
const char* WIN_CLASS_NAME = "LSystemEditor";  //ウィンドウクラス名
const int WINDOW_WIDTH = 720;  //ウィンドウの幅
const int WINDOW_HEIGHT = 720; //ウィンドウの高さ



///プロトタイプ宣言


//ダイアログプロシージャーを持たせたクラスのポインタ
//DialogProcedure* pDialogProc = nullptr;
//GameObjectを継承したクラスのポインタ
RootJob* pRootJob = nullptr;


//エントリーポイント

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
	//あるルール（型）にのっとって作られた実体　＝　インスタンス
{
	////ダイアログプロシージャーのインスタンスの生成
	//pDialogProc = new DialogProcedure(nCmdShow);
	//

	//ダイアログプロシージャーの初期化
	DialogProcedure::Initialize(nCmdShow);




/*****構造体の生成*****************************************************************************/
	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;	//ウィンドウの設定項目を構造体に分けられている
					//wcに、設定して、設定をたくさん入れていって、
					//→それを、最終的にレジスターで入れる

	//------------★wc（構造体）の中身を変更、★------------
	wc.cbSize = sizeof(WNDCLASSEX);             //この構造体のサイズ
										//~sizeときたら、このサイズを入れておく

/******ウィンドウを選択**************************************************************************/
	wc.hInstance = hInstance;                   //インスタンスハンドル
									//hI＝インスタンスハンドル（HW=ウィンドウハンドル）
									//プログラムでいうところのインスタンス、→オブジェクトと同じものとして考えられる
		//Mainに、hInstaceとして、どのアプリからのウィンドウなのかを判断

	//*******************以下説明***********************************
	//int a;	→intという数値を入れられる枠組み、ルールにのっとって、aをとってくれる
	/*class play
	{
		int hp;
	};
	play p;
	//プレイヤーというものは、int hpというものをもっていますよ
		//→その情報、ルールを持ったpを得た

		//クラスというものが、作られていても、使うもの画作られなければ動かない
		//p を得た
	
	*/
	//あるルールにのっとって、作られたもの→インスタンス


	//player p も　int a の　p,aはどちらもインスタンス（ルール（型）にのっとって作られたもの、）
	//何らかのルールにのっとって、そのルールにより作られたもの
		//クラスや、変数にとらわれずに、
		//VisualStudioで、ウィンドウを開いているのも、これもインスタンス

	//プログラム、ウィンドウを出すというルールのもと作っていて、
		//→実行して出てくる、ウィンドウ１個１個に番号を割り振り、出てくるアプリがインスタンス
		//→複数のアプリ、を出すこともできるはず、　その番号が入ってくる

	//同時に複数のアプリを起動するときに、どのアプリからのウィンドウ化をはんだするための番号
	//*****************************************************************
/*******ウィンドウ詳細設定***************************************************************************/
	wc.lpszClassName = WIN_CLASS_NAME;            //ウィンドウクラス名
		//設計図を作っているので、その設計図の名前
		//タスクマネージャーを開くと→今実行中のアプリの時
			//→exeの下に、サンプルゲームとして出てくる→なので、アプリの名前と共通にしておくことをお勧め


	wc.lpfnWndProc = WndProc;                   //ウィンドウプロシージャ
					//ウィンドウプロシージャーとして、作った、何かあった時に呼ぶ関数と同じ名前を入れる（その名前を書く）
					//関数ポインタ→関数名の後ろの（）をなくすと、→関数のアドレスを入れる（）

	wc.style = CS_VREDRAW | CS_HREDRAW;         //スタイル（デフォルト）

	wc.hIcon = LoadIcon(NULL, IDI_QUESTION); //アイコン
			//このhもハンドルのh
	//定義に移動してみる
	//→実行中のアプリのタブのところのアイコンが変わる（アプリを起動時の、下のバーに出てくるアイコン）
	//自作ができるため、自分の好きなアイコンを

	wc.hIconSm = LoadIcon(NULL, IDI_ASTERISK);   //小さいアイコン
		//こっちのアイコンは、→起動時のアプリウィンドウの左上のアイコン


	wc.hCursor = LoadCursor(NULL, IDC_ARROW);   //マウスカーソル
			//マウスカーソル
			//→定義へ移動→ウィンドウ上でのマウスカーソルの変更	→そこで、マウスカーソルの種類を選べる
			//ゲームだと、マウスカーソルの画像を、自分で設定したものにして置き、その画像を貼り付ける（マウスカーソルの位置に）


	wc.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);       //メニュー（あり　<ファイル、編集>　）(メニューとは、ファイル、編集、表示などのウィンドウについているタブ)


	wc.cbClsExtra = 0;

	wc.cbWndExtra = 0;

	wc.hbrBackground = (HBRUSH)GetStockObject(DKGRAY_BRUSH); //背景（白）
		//背景色
		//ゲームであると、背景画像を出すので、ここは何色でも。


/*******ウィンドウ適用***************************************************************************/
	//常軌にて、wcで、一気に入れる（変更を加えた構造体を）
	//複数のアプリを開くこともあるので、そのたび違う設定を行い（作ることもできる）
		//SampleGameの、設計図の名前を変える必要がある
	RegisterClassEx(&wc);  //クラスを登録


/*******ウィンドウサイズの計算***************************************************************************/
	RECT winRect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };	//Rectという構造体
					//Window側で用意されている構造体
					//RECT　枠のような
					//left top light bottom
					//left０の位置から８００＊６００という計算
					//タイトルバーを抜いた領域→それをクライアント領域
						//そのクライアント領域がleft0（左上からの値）から８００＊６００にしてほしい
					
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, TRUE);
					//left0を、８００＊６００にしたいということは、
					//タイトルバーの-8 , -31にすれば、→クライアント領域が、left0,0から８００＊６００になる
					//枠の右端が、８０８，６０８になると結果には出てくる（タイトルバーなど、ウィンドウの枠組みを入れた大きさの端がここに来るよ）

					//WS=WindowSｔｙぇ→ウィンドウスタイルによって、枠の右下端が変わってくるので、その設定
					//第三引数：メニューウィンドウの有り無し、→メニューウィンドウがあかないかで、下に下がったりと変わってくる
						//ゲームの場合、ウィンドウサイズがメニューによって小さくなるのは避けたい。


	//つまり、それによりウィンドウサイズ、を計算
		//クライアント領域　０　−　８００
		//タイトルバー入れて　-8 - 808	の領域（どこから、クライアント領域をとるのか、→それにより、枠がどの大きさなのか、→それにより枠を含めたサイズ（どの位置からどの位置まで））
		//その差、	タイトルなどの含めた -8 - -800
		//その二つの差を求めたい→どのくらいの大きさになるのかを求める
		//右　−　左端	を差額を出して幅
		//下　−　上	を差額を出して幅
	int winW = winRect.right - winRect.left;     //ウィンドウ幅
	int winH = winRect.bottom - winRect.top;     //ウィンドウ高さ


	//これによりタイトルバーの大きさを含めた
	//ウィンドウサイズになる（クライアント領域が、実際に使える領域が、指定した領域になる８００＊６００にする）
	//ゲームの場合は、ウィンドウサイズが重要になってくるので、きちんと指定してやる

/*******ウィンドウの作成***************************************************************************/
	//この段階では透明
	HWND hWnd = CreateWindow(

		WIN_CLASS_NAME,         //ウィンドウクラス名（設計図の名前→この設計図を基に作ってね）
								//ウィンドウが一つなので、→定数一つで

		WIN_CLASS_NAME,     //タイトルバーに表示する内容
								//setupにて、変更していた名前
								//上の、SampleGameというウィンドウクラスの名前と一緒でもいい。

		WS_OVERLAPPEDWINDOW, //スタイル（普通のウィンドウ）
			//WS=WindowStyle
			//POPUP→VisualStudio起動時に、→画面中央に、出てくるもの
			//→標準などだとフルスクリーンにしても、→画面の見えないところに×ボタンがあるので、
			//フルスクリーンの時は、→このPOPUPに
			//これらは複数指定ができる
				//→タイトルバーだけ、×ボタンだけ、→とこれらを組み合わせることができる
				//→これらを組み合わせて作ったのが、WS_OVERLAPPEDWINDOW（定義に移動を見ればわかる）

		//どこに表示されてもいいなら、デフォルトに
			//子ウィンドウの時は重要（親に対して、この位置なので、）
		CW_USEDEFAULT,       //表示位置左（おまかせ）
								//ディスプレイに対して、どの位置にウィンドウを表示するか
		CW_USEDEFAULT,       //表示位置上（おまかせ）

		//ゲームだと、ウィンドウの大きい差は重要になる
		//単純なウィンドウサイズ
		winW,                 //ウィンドウ幅

		winH,                 //ウィンドウ高さ
		//幅と高さは２の乗数でなくても
		//計算をして、タイトルバーを含めた大きさに

		NULL,                //親ウインドウ（なし）
							//ウィンドウの中に小さいウィンドウのような、
							//ゲームでは使わないかもね

		NULL,                //メニュー（なし）
								//タブなどの作成

		hInstance,           //インスタンス
								//さっきの、どのアプリに対してのウィンドウなのか、
								//アプリをクリックしたとき→そのアプリには、クラスのように、このルールにのっとったものがある
								//そのルールにより、クラスのオブジェクトのように、ルールにのっとったものが作成されたもの（１つのオブジェクト）

		NULL                 //パラメータ（なし）
	);




/*******ウィンドウ表示***************************************************************************/
	ShowWindow(hWnd, nCmdShow);
				//デフォルトのままだと、→一瞬出てくる
				//ウィンドウを作成したイウィンドウ引数	ともう一つの謎の（メインの最後の）
	//ウィンドウはデフォルトだと透明

	//DirectXの処理は、DirectXとして、ほかのソースとしてまとめたい
	//→そして、この時、クラスではなく、namespace を使う（インスタンスを１つしか作らないものは、）



	//ウィンドウバーに表示するテキストを設定
	SetWindowText(hWnd, WIN_CLASS_NAME);


/*******ダイアログ表示***************************************************************************/
	//ダイアログというのは、一つのウィンドウのようなもので、そのダイアログの中に、複数のボタンなどが設定されている（自身で作成した。）
		//追加→リソース→Dialog　


//メインのウィンドウが作られて、表示されたら
	//インスタンスハンドル
	//ダイアログのID	それぞれのダイアログの１つ１つのID
	//親ウィンドウのハンドル メインのウィンドウハンドル		必ず指定しなくても、いいが、これを親ウィンドウとして、自身を子ウィンドウとする。なので、親を閉じると、子も閉じることになる。（プログラムを終えたら、問題なく閉じるが、）
			//なので、ダイアログだけでプログラムを作ることも可能である。
	//ダイアログプロシージャの関数ポインタ
	//HWND hDlg1 = CreateDialog(hInstance, MAKEINTRESOURCE(DIALOG_NEW_FILE),hWnd , (DLGPROC)DialogProc);
	//HWND hDlg2 = CreateDialog(hInstance, MAKEINTRESOURCE(DIALOG_VERTEX_CHANGER), hWnd, (DLGPROC)DialogProc);

	//DLGPROC rcb = (DLGPROC)DialogProc;
	//DLGPROC bbb = (DLGPROC)DialogProcForCreateMap;

	////各々のダイアログを作成
	//	///関数を分けているのは、
	//	//ダイアログプロシージャーの関数が違うため、その関数を分けるために関数分けをしている。
	////CreateMap専用のダイアログ作成と
	//	//ダイアログプロシージャの設定
	//DialogProcedure::CreateDialogForCreateMapLeavedItToClass(DIALOG_TYPE_NEW_FILE, hInstance, hWnd);
	////VertexChangerダイアログ作成と
	//	//共通ダイアログプロシージャの設定
	//DialogProcedure::CreateDialogLeavedItToClass(DIALOG_TYPE_VERTEX_CHANGER, hInstance, hWnd);

	//LSYSTEMのダイアログ作成と
		//そのダイアログの専用ダイアログプロシージャの設定
	DialogProcedure::CreateDialogForLSystemEditor(DIALOG_TYPE_LSYSTEM_EDITOR, hInstance, hWnd);


	/*DialogProcedure::ShowDialogLeavedItToClass(DIALOG_TYPE_NEW_FILE);
	DialogProcedure::ShowDialogLeavedItToClass(DIALOG_TYPE_VERTEX_CHANGER);
	*/
	
/*
	pDialogProc->CreateDialogLeavedItToClass(DIALOG_TYPE_NEW_FILE, hInstance, hWnd);
	pDialogProc->CreateDialogLeavedItToClass(DIALOG_TYPE_VERTEX_CHANGER, hInstance, hWnd);
*/
	/*pDialogProc->ShowDialogLeavedItToClass(DIALOG_TYPE_NEW_FILE);
	pDialogProc->ShowDialogLeavedItToClass(DIALOG_TYPE_VERTEX_CHANGER);*/



	/*
	ShowWindow(hDlg1, nCmdShow);
	CloseWindow(hDlg1);
	*/





	//ShowWindowのように表示宣言をしないといけないのだが、
		//★リソースビューのDialogフォルダ下にある、ダイアロぐ編集から、ダイアログの動作のViableをTrueにすると表示されるので、そちらでもよい


/*******Direct３Dの初期化************************/
//以下で、DirectXの処理を行っていた、
	//→そのさまざまな初期化の処理をDirect３DのInitializeに持って行ったので、それを呼べばいい
	
	if (FAILED(Direct3D::Initialize(winW, winH, hWnd)))
	{
		 MessageBox(nullptr, "Direct３Dの初期化失敗", "エラー", MB_OK);
		return 0;	//エラーを返してくる　エラーなら、プロジェクト終了
	}
	//Direct3D::Initialize(winW, winH, hWnd);	//引数：ウィンドウ幅、ウィンドウ高さ、ウィンドウハンドル（ウィンドウの番号）
/********Direct2Dの初期化***************************************/

	if (FAILED(Direct2D::Initialize()))
	{
		MessageBox(nullptr, "Direct2Dの初期化失敗", "エラー", MB_OK);
		return 0;	//エラーを返してくる　エラーなら、プロジェクト終了
	}

/*******DirectInputの初期化******************************************/
	//Inputのnamespaceにハンドルを送り、初期化
	if (FAILED(Input::Initialize(hWnd)))
	{
		MessageBox(nullptr, "DirectInputの初期化失敗", "エラー", MB_OK);
		return 0;
	};
	
	//if (FAILED(Input::Initialize(winW)))
	//{
	//	return 0;	//エラーを返してくる　エラーなら、プロジェクト終了
	//}

/*******XAudio2の初期化******************************************/
	Audio::Initialize();


/***GameObjectの初期化*****************************************************/
	pRootJob = new RootJob();	//宣言、動的確保（コンストラクタは引数なし）
								//コンストラクタがpublicでないと、怒られる。
							
	pRootJob->Initialize();		//初期化を呼ぶ
								//大本の親→そいつのInitializeが呼ばれて、その次に、子供のInitializeがよばれて、と、なる

	//RootJobを
	//ウィンドウプロシージャにセットする
	WindowProcedure::SetRootJob(pRootJob);
	//RootJobを
	//ダイアログプロシージャにセットする
	DialogProcedure::SetRootJob(pRootJob);


/********************************************************/




/*******メッセージ受け取り***************************************************************************/
   //メッセージループ（何か起きるのを待つ）
	//Windows側のメッセージがあるなら、そちらを優先する
	MSG msg;	//ウィンドウメッセージを入れるための、ウィンドウプロシージャの時と同じ
				//→

	ZeroMemory(&msg, sizeof(msg));//ゼロメモリー


	//終わるというメッセージが来るまで続ける
	while (msg.message != WM_QUIT)
		//メッセージが、終わりにしたら、抜けて、→return0へ
		//メッセージ、→たまっている→古いのから処理している（PeekMeseage）
		//メッセージというものは、マウスを動かしたら、→その都度動かした動かした、→とたくさん呼ばれる→なので、そのメッセージをどんどんためていく
		//ウィンドが終わり→いわゆる×ボタンが押されれば、→メッセージに終わりが入るので、抜けて→終了
	{

		

		//メッセージあり

		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
			//メッセージのたまりが、あるならばtrueと帰ってくる

		{

			//ここで、何かあったら、メッセージがあれば、trueが帰ってきたら、ウィンドウプロシージャを呼ぶよという処理をしている
			TranslateMessage(&msg);

			DispatchMessage(&msg);

		}
		//メッセージなし
		else
		{
			//FPS計測
			{
				timeBeginPeriod(1);	//これ以降は、正確に１ｍｓを取得（時間を計って）してくださいという宣言

				//何回更新されたか、（フレーム数）カウント
				//static DWORD countFps = 0;


				//staticなので、１回しか更新されない（宣言の分が再び呼ばれたとしても、２回は宣言されない）
				//初めに、呼ばれたときに計測される
				static DWORD startTime = timeGetTime();

				//時間をゲット
				//時間をunsigned int
				DWORD nowTime = timeGetTime();
					//ウィンドウ立ち上がってからの時間、●ｍｓたったか、
					//1s = 1000ms

				//ゲームを更新したときの時間を持っておく
				static DWORD lastUpdateTime = nowTime;


				//６０ｆｐｓにしたい
				//１秒で６０回
				//１０００ｍｓで６０回
				//　＝　１６．６６６〜ｍｓ１回で立てばいい


				/*
				//１秒立ったら
					//カウントを０にする
				if (nowTime - startTime >= 1000)
				{
					//ワイド文字　文字列変換
					char str[16];
					//unsigned の u
					wsprintf(str, "%u", countFps);
					//nowTime - startTime) 現在の時間 - 開始時間　＝　ゲーム開始から●ｍｓたったか、
					// / 1000 = ●ｓたったか



					//ウィンドウのタイトルバーにテキストが書かれる
						//現段階では、
					//表示したいウィンドウのハンドル
					//表示したいテキスト
					SetWindowText(hWnd, str);

					/*
					//ウィンドウのタブに→毎フレーム更新させる→これで、FPSが３０００いかない
					//→これを１秒ごとにSetWindowTextにすると、（ウィンドウのプログラム）→４０００以上になる
						//ウィンドウプログラムだけだと重いことがわかる
						//→なので、ゲームを行うのに、ウィンドウプログラムだけでは難しい→だからDirectXなどが使われる
					//表示自体は、ディスプレイの描画回数が決まっているので、
						//計算自体は、４０００回ぐらいやっていても、描画は、ディスプレイの描画の回数しか、描画していない（なので、とびとびに見えたり）
					


					countFps = 0;
					startTime = nowTime;
						//現在の時間を開始の時間に
				}
				//０に切り替わる直前で
				*/

				//現在の時間から最後にUpdateされたときの時間を引いて、
					//それを１０００ｍｓ/６０で６０回　６０ｆｐｓとしたい
					//６０ｆｐｓにて経過する時間（１回の更新にかかる時間）よりも小さいなら
				if ((nowTime - lastUpdateTime)*60 <= 1000)
				{
				

					//６０ｍｓにて経過する時間よりも小さいなら
						//やり直す(もう一度、ウィンドウループ、ウィンドウの受付あるか、ないならゲーム処理のこのループに回って、経過時間を更新)
					continue;
				}

				lastUpdateTime = nowTime;

				//だが、これだと、正確でない
					//なぜなら、ちょっと、ずれてしまう

				//それに１０００/６０は、割り切れない
					//ならば、両方に６０を掛けると/60消せることになる
					//→左辺に６０＊　右辺に６０＊（そうすることで、１０００/60*60なので、６０消える）　

				//fpsの部分を、
				//ファイルから読み込んで、変えられるようにして、見たり

				//音ゲーや、格ゲーならば、ｆｐｓ上げて、６０固定にしたり
				//RPGでは、３０ｆｐｓにして、他をエフェクトを豪華にしたり
				//オンラインなどであれば、→あらかじめ３０に固定しておいて、３０フレームにしたことで、動きの量を大きくしたりなど


					//FPSをカウント//処理１回呼ばれるごとにカウントして、１ｓ間に何回カウントされたか＝フレーム数（１ｓのフレーム数）
				//countFps++;


				{
					/*
					//ウィンドウのタイトルバーにテキストを更新
					//ワイド文字　文字列変換
					char str[16];
					//unsigned の u
					wsprintf(str, "%u", countFps);
					SetWindowText(hWnd, str);
					*/

				}

				//countFps = 0;
			}
			//シーンタイマーに時間計測を行わせる
			//デルタタイムの更新
			Timer::UpdateFrameDelta();


			

			
			

			


			//ゲーム側の処理
			//ウィンドウ側の処理がないときに、ゲーム側の処理を行う
			//あくまで、ウィンドウ側の処理優先に



			//毎フレーム更新処理
			//キーボードの登録のためのUpdateを読んであげないと、
				//キーボードの登録を行ってくれない→Updateにて、キーボードの登録を毎フレーム行わないと→DirectInputは、毎フレームで、キーボードのことを忘れてしまう
				//→キーボードを使う前に、キーボードの存在をまた認識させることが必要
				//Updateを呼ぶところはほかに、Windowsのメッセージルーぷ以外に存在しないので、メッセージループの、ゲームの描画などを行う処理の前に書いた
			if (FAILED(Input::Update()))
			{
				MessageBox(nullptr, "DirectInputの更新失敗", "エラー", MB_OK);
				return 0;
			}	//毎フレーム更新（キーボードを再認識させる）
			
			
			//Inputで、キー情報を受け取ってから、プレイヤーを動かしたりの、Update処理
			/**GameObjectの更新*****************************************************/
			//pRootJob->Update();	//GameObjectを継承したクラスの更新
				//RootJobのUpdateがよばれたら、その子供のUpdateが呼ばれる。そのさらに、子供がいたら、さらに呼ぶ

			pRootJob->UpdateSub();
				//★大本がUpdateではなくUpdateSubを読んであげることで、
				//RootJobでは、自身のUpdateの呼び込みと、その子供のUpdateSubを呼び込み→そうすると、子供のUpdateSubでは、自身のUpdateと、子供のUpdateSub〜〜〜〜

			/**Direct3Dの描画開始********************************************************************/
			//描画開始
			Direct3D::BeginDraw();
			
			/**Direct2Dの描画開始********************************************************************/
			//描画開始（２D）
			//Direct3DのBeginDrawの後に呼び込む、３DのEndDrawが終わる前であれば、どこでも問題はない
			//だが、　ゲームオブジェクト内で、　DrawTextを呼ぶ可能性が高いので、　RootJobのDrawSubの前にBeginしておくのが良い
			Direct2D::BeginDrawDirect2D();

			
			/**GameObjectの描画********************************************************************/
			//pRootJob->Draw();	//GameObjectを継承したクラスの描画
								//描画は、Direct3Dの、描画開始が終わった後に、行うべき。（描画開始で、キャンバスを白色に塗って、今から塗りますよ。という宣言をする）
									//開始前に描画を行っても、それは、BeginDrawで消されてしまう
			pRootJob->DrawSub();
				//Drawではなく、自身のDrawを呼んで子供のDrawSubを呼ぶ
				//すると子供も、DrawSubが呼ばれるので、自身のDrawと、子供のDrawSubが呼ばれる
			

			/**Direct2Dの描画終了********************************************************************/
			//描画終了（２D）
				//スワップチェインで、画用紙を切り替える前に描画を終了することを宣言
			Direct2D::EndDrawDirect2D();

			/**Direct3Dの描画終了********************************************************************/
			//描画終了
			if (FAILED(Direct3D::EndDraw()))
			{
				//エラーチェック
				//エラーだったら
				return 0;	//終了
			}
			//Direct3D::EndDraw();
					//EndDrawを見てみる
					//→BeginDrawで、バックバッファにて、裏の紙に書き込んでいる(紙を真っ白にする)

					//EndDrawで、スワップチェインが、バックバッファと、現在のバッファを入れ替える
					//→なので、仮に、EndDrawの後に、→描画の画面に、いくら描画しても、次に回るときは、BeginDrawで、新しいものを用意して、それをEndDrawで入れ替えているので
						//→書き込んでも見えないような状態が続く

			////描画開始
			//Direct3D::BeginDraw2();
			////描画終了
			//Direct3D::EndDraw();


			
			//ウィンドウは、そんな早く動けない
				//→１ｍｓなどの細かいｍｓなど見ていない
			//なので、１ｍｓとしても、ざっくりとなってしまう

			
			Sleep(1);	//休ませる、休ませて、他のアプリのことをやっていいよ。みたいな
						//このプログラムを（１）なら１ｍｓ休んでいいよとする

			timeEndPeriod(1);	//正確に１ｍｓの時間を計測するのを終了する
								//これ以降はざっくりと時間計測を行う
								//１ｍｓとしても、これ以降は、１ｍｓとしても、ざっくりとなので、正確に１ｍｓにならない


		}

	}

	/*************************************************************/
	//★解放での注意★
	//→Direct3DのID3D11の型や、DirectInputの〜〜の型など、
	//は、Releaseの時に、DELETEではなく、RELEASEの関数を読んであげないといけない
		//ID3D11などの型は→RELEASEの中で、その型のポインタで、さらにメソッドのRelease()を呼ばなくてはいけないポインタそのものをdeleteできない(ポインタのアドレスはどこを示さないようにnullptrを入れる)
		//Deleteするのは、自分で作成したID3D11など以外のクラス、型のポインタ
	//ここにおけるQuadなどのポインタでのReleaseをSAFE_RELEASEで呼んではいけない→なぜなら、SAFE_RELEASEでは、ポインタのdeleteを行わない
		//→RELEASEでは、ポインタのRelease()を呼んでポインタにnullptrを入れるので、そのあとにdeleteするために、SAFE_DELETEを呼ぶと、すでにポインタのアドレスにはnullptrを入れているので、参照できなくなる
		//→SAFE_RELEASEは、ID3D11など、ポインタを、作成先でdelete解放を行ってはいけないものに使用するときに限る（ただ、Releaseを呼びたいときには使用しない）


	//★★解放処理(クラス内の、動的確保した領域の開放)
		//確保とは逆に呼び出す→Direct３Dは名前空間だが、宣言したのは先なので、一応最後に解放する


	//★★Main内で動的確保した領域の解放(newして、作成したもの)


	/*GameObjectの解放**/
	pRootJob->ReleaseSub();		//ReleaseSubを呼ぶことで全子供を再帰的にReleaseSubを呼ぶ
								//そして、ReleaseSub終了後に、
									//自身の子供リストの子供だけを解放するようにしている
									//そのため、一番上の親だけは解放されていない状態で終わる。→なので、Mainにて、一番上である、RootJobの解放
		//GameObjectを継承したクラスの解放
	//ポインタなので、Releaseを呼んだ後に、そのポインタ解放	（専用のマクロあるので（作ったので））						
	SAFE_DELETE(pRootJob);	//delete（newしたら、必ず、Delete）


	//namespaceの名前空間のRelease処理
	//Audioの解放
	Audio::Release();
	
	//DirectInputの解放
	Input::Release();

	//Direct2Dの解放
	Direct2D::Release();	//Direct２DのRelease関数を呼ぶ
	//Direct3Dの解放
	Direct3D::Release();	//Direct３DのRelease関数を呼ぶ

	
	//ダイアログプロシージャーのクラスの解放
	DialogProcedure::Release();
	//SAFE_DELETE(pDialogProc);



	/*************************************************************/

	//メッセージルーぷされないとき
		//そのままreturn0でプログラムが終了している
	return 0;

}

