#pragma once
#include "../Shader/CommonDatas.h"

//FactoryMethodパターンを使用して、
//派生クラスのインスタンス生成を請け負って、それを渡すクラス
	//Direct3Dにて使用するブレンド（混ぜ具合）クラスのインスタンスを生成し、返す
	//Direct3Dにて、インクルードを多く記入したくないので、
	//ブレンド（混ぜ具合）を作るクラスを作っておき、それにより、派生クラスの依存性をなくす。
	//Direct3Dは、ただ、作られたものを受け取るだけでよくする


//継承元クラスの前方宣言
class BaseBlend;

//ブレンド（混ぜ具合）を作るクラス
	//ブレンド（混ぜ具合）を作る工場
class BlendFactory
{
public:
	//ブレンド（混ぜ具合）のインスタンスを作成し、
		//インスタンスを返す
	BaseBlend* CreateBlendClass(BLEND_TYPE type);
};

