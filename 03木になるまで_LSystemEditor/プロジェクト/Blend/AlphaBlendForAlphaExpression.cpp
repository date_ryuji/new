#include "AlphaBlendForAlphaExpression.h"

AlphaBlendForAlphaExpression::AlphaBlendForAlphaExpression():
	BaseBlend::BaseBlend()
{
}

AlphaBlendForAlphaExpression::~AlphaBlendForAlphaExpression()
{
	BaseBlend::~BaseBlend();
}

HRESULT AlphaBlendForAlphaExpression::Initialize()
{

	//透過処理を実現するために、色の混ぜ具合を登録
	//ブレンドステート
	//以下のコードを追加するだけで、　透過する
	//要素を変えることで、エフェクトに適した透過にできる　
	D3D11_BLEND_DESC BlendDesc;
	ZeroMemory(&BlendDesc, sizeof(BlendDesc));
	BlendDesc.AlphaToCoverageEnable = FALSE;
	BlendDesc.IndependentBlendEnable = FALSE;
	BlendDesc.RenderTarget[0].BlendEnable = TRUE;	//半透明を許可する、色を混ぜる処理をするか、　TRUE
	
	//今から書き込む色の対処（混ぜ具合の方法）
	BlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	//D3D11_BLEND_SRC_ALPHA;	//現在	表示されているピクセルの色を込みした、今から書き込もうとしているピクセルの色とで最終的な色を計算する
								//src （= 今から表示しようとしているものは、）　	今から書き込む　色　をα値分使います
									//Src = ALPHA , DEST = INV_SRC_ALPHA ならば　（赤でα７０％の色を、青ピクセルの上に書き込むとき　＝今ある色青　３０％と　今から書き込む色赤　をα７０％分足した色を最終的な色とします。　＝結果　色は赤紫のような色になる）
	//D3D11_BLEND_ONE;	//100%　＝　そいつの自体のαを関係なく、　→　背景のRGBを足していく　→不透明のオブジェクトも、後ろの色の色を自身に透過させる
						//つまり、背景の色が移る	//この場合、透明のシェーダーにしているものは、後ろにどの色があるかは関係なく、世界の背景の色を移すので、
						//透過のオブジェクトの下にオブジェクトの色があっても、それを透過しなくなる

	//元ある色の対処（混ぜ具合の方法）
	BlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		//D3D11_BLEND_INV_SRC_ALPHA;	//Dest=すでに使われているものは、　今から表示しようとしているものの逆INV　３０％を使う。　青を３０％

	//何算の計算をするのかするのか
	BlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
			//D3D11_BLEND_OP_SUBTRACT;	//減産する、　背景の色（scr ONE Dest INV）を引くことになる。＝なので、黒くなる。　闇の魔法や、重力魔法などに使える
			//D3D11_BLEND_OP_ADD;	//上記を足す	//単純な透過

	BlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	BlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	BlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	BlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	//ブレンド状態を登録
	if (FAILED(CreateBlendState(&BlendDesc)))
	{
		MessageBox(nullptr, "ブレンド設定をもとにブレンド状態のポインタ作成失敗", "エラー", MB_OK); return E_FAIL;
	};

	return S_OK;
}
