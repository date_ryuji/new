#include "HeightMapObject.h"

#include "../Engine/Input.h"
#include "../Engine/Model.h"
#include "../Engine/HeightMapCreater.h"
#include "../Shader/CommonDatas.h"

#include "../Engine/CsvReader.h"


//コンストラクタ
HeightMapObject::HeightMapObject(GameObject * parent)
	: GameObject(parent, "HeightMapObject"),
	STANDARD_(128)
{
}

//初期化
void HeightMapObject::Initialize()
{
	//ハイトマップの初期化
	pHeightMapCreater_ = new HeightMapCreater;


	//マップ作製のための
	//必要情報軍の確保
		//ファイルから読み込んで、
		//情報として取得
	//Csvとして読み込む
		//書き込みの段階で、 ","感覚で書き込んでいる
	CsvReader* pCsvReader = new CsvReader;
	//ロード
	int width = 0; 
	int depth = 0; 
	float size = 0;
	if (pCsvReader->Load("Assets/HeightMap/MapInfo/MapInformation.txt"))
	{
		//ファイルのロードできたら
			//ロードしたファイルの値をセットする
		width = pCsvReader->GetValue(0, 0);	//Width
		depth = pCsvReader->GetValue(1, 0);	//Depth
		size = pCsvReader->GetValueFloat(2, 0);		//Size

	}
	else
	{
		//ファイルのロードができなかったら
			//基準値をセットする
		width = 64;
		depth = 32;
		size = 0.5f;
	}
	
	//テクスチャを作る場合でも、
	//２の乗数でないとうまく反応しない
		//なので、
		//作成の段階で、２の乗数で作ってもらうようにする。
		//そういったダイアログ操作を行う






	if (FAILED(
		pHeightMapCreater_->Initialize(
			width, depth, size, SHADER_HEIGHT_MAP, "Assets/HeightMap/Grass256.png")))
	{
		MessageBox(nullptr, "ハイトマップクリエイター作成失敗", "エラー", MB_OK);
		assert(0);
	}
	

	//追加
	Model::ModelData modelData;
	modelData.fileName = "";
	modelData.pPolygonGroup = pHeightMapCreater_;
	modelData.thisPolygonGroup = HEIGHT_MAP_CREATER_POLYGON_GROUP;
	modelData.thisShader = SHADER_HEIGHT_MAP;
	modelData.transform = transform_;

	hModel_ = Model::AddModelData(modelData);
	assert(hModel_ != -1);


	transform_.position_.vecY = -1;





}

//更新
void HeightMapObject::Update()
{

}

//描画
void HeightMapObject::Draw()
{

	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

}

//開放
void HeightMapObject::Release()
{
}

bool HeightMapObject::OverWrite(int width, int depth)
{
	//同時に、値が大きすぎないことを確認（基準（１２８以下）
	if (!CheckIfStandardIsNotOver(STANDARD_, width, depth)) { return false; }


	//2つの値が、
	//2の乗数であることを確認
	if(!CheckIfMultiplyOfTwo(STANDARD_, width, depth)) { return false; }


	//上書き
	if (FAILED(pHeightMapCreater_->OverWriteVertex(width, depth)))
	{
		MessageBox(nullptr, "ハイトマップ頂点情報更新失敗", "エラー", MB_OK);
		assert(0);
		return false;
	}


	return true;
}

bool HeightMapObject::CheckIfStandardIsNotOver(int standard, int width, int depth)
{
	return (standard <= width && standard <= depth);
}

bool HeightMapObject::CheckIfMultiplyOfTwo(int standard, int width, int depth)
{
	bool isExistsWidth = false;
	bool isExistsDepth = false;

	for (int i = 2; i <= standard; i *= 2)
	{
		if (width == i)
		{
			isExistsWidth = true;
		}
		if (depth == i)
		{
			isExistsDepth = true;
		}

	}


	return (isExistsWidth && isExistsDepth);
}


HRESULT HeightMapObject::CreateHeightMapImage()
{
	//ヘイトマップの作成
	if (FAILED(pHeightMapCreater_->CreateHeightMapImage()))
	{
		MessageBox(nullptr, "ハイトマップ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	};

	return S_OK;
}

void HeightMapObject::RiseOrFall(UP_DOWN_TYPE type, int polygonNum, const float SPEED , const int RANGE)
{
	//頂点情報のY座標の上下を行う
	pHeightMapCreater_->UpDownCoodeY(type, polygonNum, SPEED , RANGE);
}

int HeightMapObject::RayCast(RayCastData * pData)
{
	Model::RayCast(hModel_, pData);

	if (pData->hit)
	{
		return pData->polygonNum;
	}
	
	//衝突しなかった時
	// -1を帰す
	return -1;
}
