#include "MapEditManager.h"
#include "../Engine/PolygonGroup.h"
#include "../Engine/Input.h"


#include "HeightMapObject.h"
#include "../Engine/HeightMapCreater.h"


#include "../resource.h"
#include "../Procedure/DialogProcedure.h"


//コンストラクタ
MapEditManager::MapEditManager(GameObject * parent)
	: GameObject(parent, "MapEditManager"),
	upDownType_(DOWN_TYPE),
	upDownBrushSize_(0.01f),
	upDownBrushRange_(1),

	standardBrushSize_(0.01f)
{
}

//初期化
void MapEditManager::Initialize()
{
	
	//自身が使用するダイアログの初期化を行う
	DialogProcedure::ShowDialogLeavedItToClass(DIALOG_TYPE_VERTEX_CHANGER);



}

//更新
void MapEditManager::Update()
{
	/*if (Input::IsKeyDown(DIK_RSHIFT))
	{	
		CreateHeightMapImage();
	}*/



}

//描画
void MapEditManager::Draw()
{

}

//開放
void MapEditManager::Release()
{
	//自身が使用するダイアログの非表示
	DialogProcedure::CloseDialogLeavedItToClass(DIALOG_TYPE_VERTEX_CHANGER);


}

void MapEditManager::RiseOrFall(int polygonNum)
{
	if (pHeightMapObject_ == nullptr || polygonNum == -1)
	{
		return;

	}

	//引数：上下タイプ（ダイアログから取得）
	//引数：ポリゴン番号（引数から取得）
	//引数：上下量（ダイアログから取得）
	pHeightMapObject_->RiseOrFall(upDownType_, polygonNum, upDownBrushSize_ , upDownBrushRange_);
}

void MapEditManager::RayCast(RayCastData * pData)
{
	//レイキャスト実行
	int polygonNum = pHeightMapObject_->RayCast(pData);

	//成功時
	//ポリゴン番号を取得
	RiseOrFall(polygonNum);


}


void MapEditManager::CreateHeightMapImage()
{
	//ヘイトマップの作成
	if (FAILED(pHeightMapObject_->CreateHeightMapImage()))
	{
		MessageBox(nullptr, "ハイトマップ作成失敗", "エラー", MB_OK);
		assert(0);
	};

}


BOOL MapEditManager::DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	//msgに対する
	switch (msg)
	{
		//何かメッセージを受け取ったら、メッセージ全般
	case WM_COMMAND:
		//引数のwpの、LOWORD（wp）にコントロールのIDが入ってくる
		//そのためこのコントロールのIDから、誰に変更が加えられたのか判断。処理を判断する。
		switch (LOWORD(wp))
		{
		//上下のタイプ
			//ブラシのタイプ
		case COMBO_BLUSH_TYPE : 
		{
			//タイプのセット
			upDownType_ =
				(UP_DOWN_TYPE)
				SendMessage(GetDlgItem(hDlg, COMBO_BLUSH_TYPE),
					CB_GETCURSEL,
					0, 0);

			
		}break;

		case COMBO_BLUSH_RANGE:
		{
			// 0 ~ (int)
			int brushRange = SendMessage(GetDlgItem(hDlg, COMBO_BLUSH_RANGE),
				CB_GETCURSEL,
				0, 0);
			//1オリジンにしてセット
			upDownBrushRange_ = brushRange + 1;


		}break;

		//上下の変動値
			//ブラシのサイズ
		case COMBO_BLUSH_SIZE:
		{
			// 0 ~ (int)
			int brushSize = SendMessage(GetDlgItem(hDlg, COMBO_BLUSH_SIZE),
				CB_GETCURSEL,
				0, 0);

			//取得したコンボボックスにおける連番
				//それを1オリジンに変更し、標準のサイズとかけて、
				//サイズとする
			upDownBrushSize_ = (brushSize + 1) * standardBrushSize_;


		}break;


		case SLIDER_WIDTH:
			break;
		case SLIDER_DEPTH:
			break;
		case SLIDER_POLY_SIZE:
			break;

		

		
		}



	//	//初期化
	//	//初期化は、ゲームオブジェクト外
	//	//最初にダイアログが表示されたとき
	//case WM_INITDIALOG:


	//	//強制的に
	//	//カーソルをセットさせる
	//	//コントロールの選択。ボタンの選択の強制選択。
	//	//第１引数：（コントロールハンドル）コンボボックスのコントロールハンドル（それぞれボタンごとの番号）
	//	//第２引数：（メッセージ）コンボボックスの初期値を選択。
	//				//コンボボックスを開いたときに、あらかじめ選択されているもの
	//	//第３引数：（情報A）初期値とする（選択している状態にする）値の番号（追加順で０から）
	//	//第４引数：（情報B）なし
	//	SendMessage(GetDlgItem(hDlg, COMBO_BLUSH_TYPE),
	//		CB_SETCURSEL,
	//		0, 0);
	//	SendMessage(GetDlgItem(hDlg, COMBO_BLUSH_SIZE),
	//		CB_SETCURSEL,
	//		0, 0);



	//	//スタティックテキスト
	//		//テキスト内容の更新
	//	SetDlgItemText(hDlg, 1015, "1");


	//	/*NEW_FILE**********************************************************************/
	//	//スタティックテキスト
	//		//テキスト内容の更新
	//	SetDlgItemText(hDlg, TEXT_WIDTH, "2");
	//	SetDlgItemText(hDlg, TEXT_DEPTH, "2");
	//	SetDlgItemText(hDlg, TEXT_POLY_SIZE, "0.01");


	//	//任意の位置にセット
	//	SendDlgItemMessage(hDlg, SLIDER_DEPTH, TBM_SETPOS, TRUE, 2);



	//	break;


	//case WM_HSCROLL:
	//	//コントロールによって場合わけ
	//	if ((HWND)lp == GetDlgItem(hDlg, SLIDER_DEPTH)) {
	//		SendDlgItemMessage(hDlg, SLIDER_DEPTH, TBM_SETTHUMBLENGTH, 50, 0);

	//		// 0 ~ 100
	//		int depth = SendMessage((HWND)lp, TBM_GETPOS, 0, 0);
	//		std::string str = std::to_string(depth);
	//		SetDlgItemText(hDlg, TEXT_DEPTH, str.c_str());
	//	}

	//	break;


	//





	//	break;


	}


	return FALSE;

}

