#pragma once
#include <windows.h>
#include "../Engine/GameObject.h"

enum UP_DOWN_TYPE;

struct RayCastData;
class HeightMapCreater;



//■■シーンを管理するクラス
class HeightMapObject : public GameObject
{
private : 
	int hModel_;

	const int STANDARD_;

	//ヘイトマップクリエイター
	HeightMapCreater* pHeightMapCreater_;

private : 

	//基準を超えていないかの確認
	bool CheckIfStandardIsNotOver(int standard , int width , int depth);

	//2の乗数であるかの確認
	bool CheckIfMultiplyOfTwo(int standard, int width, int depth);


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	HeightMapObject(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//2の乗数のsizeに変更
		//引数にて、Width,Depth比を取得
		//そのサイズが2の乗数でなかった場合　OR　最高基準よりも大きい場合失敗と返す
		//2の乗数にする理由は、DirectXの画像読み込みの段階で、2の乗数でなければうまくいかない（UVとしてシェーダーで使うときだけ？）場合があるため、それを回避するため
	//引数：幅
	//引数：奥行き
	bool OverWrite(int width , int depth);



	//ハイトマップ書き込み
		//同時に、Y座標の最高値、最低値をテキストファイルに書き込み
	HRESULT CreateHeightMapImage();


	//指定ポリゴンを作る頂点のY座標を規定量上げる
	//引数：上下を示すタイプ
	//引数：ポリゴン番号
	//引数：移動値
	void RiseOrFall(UP_DOWN_TYPE type , int polygonNum , const float SPEED , const int RANGE);

	//所有しているハイトマップと当たり判定を行なわせる
		//引数にて
		//衝突したポリゴン番号を帰してもらう（衝突判定して、すぐに処理を行わせずに、一度、衝突が完了したことだけを帰す）
		//そして、必要な情報を付加した上で、衝突判定したことによる、全処理を実行させる
	int RayCast(RayCastData* pData);



};