#pragma once
#include "../Engine/GameObject.h"

//■■シーンを管理するクラス
class CameraController : public GameObject
{
	//基準移動スピード
		//カメラの回転量
	float SPEED_;

	//基準移動ピクセル
	float standardPixX_;
	float standardPixY_;


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	CameraController(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	void MoveCamera();

	void TruncatePosition();

	void TruncateRotate();


};