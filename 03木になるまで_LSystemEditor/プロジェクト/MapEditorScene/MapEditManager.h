#pragma once

/*


MapEditSceneクラスにおける
ダイアログのすべてを管理するクラス

ハイトマップなどのクラスへの情報を送るさいに、
その橋渡しとなるオブジェクト


*/

#pragma once
#include <Windows.h>
#include "../Engine/GameObject.h"


class HeightMapObject;
struct RayCastData;
enum UP_DOWN_TYPE;


//■■シーンを管理するクラス
class MapEditManager : public GameObject
{
	//ヘイトマップオブジェクト
	HeightMapObject* pHeightMapObject_;

private :
	//マップエディット情報群
	
	//上下タイプ
	UP_DOWN_TYPE upDownType_;
	//上下変動サイズ
	float upDownBrushSize_;
	//ブラシサイズ（対象のマスの範囲）
	int upDownBrushRange_;



	//標準上下変動サイズ
	const float standardBrushSize_;



public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	MapEditManager(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

public : 
	void SetHeightMapObject(HeightMapObject* pObj) { pHeightMapObject_ = pObj; };

public : 
	//橋渡し
	//Y座標の上下
	void RiseOrFall(int polygonNum);

	//レイキャスト
	void RayCast(RayCastData* pData);

public :
	//メニュー操作
	//ダイアログ操作
	
	
	//偽物
	//ダイアログプロシージャもどき
	//MainCppが本物
		//本物では、この偽物へ、メッセージを飛ばす処理を行う
		//そして、この偽物で、ゲームに影響の与える処理を書くことで、本物に、余計な処理、情報を与えない
	BOOL DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);

	//ハイトマップ画像の生成
	void CreateHeightMapImage();




};

