#include "ControllerAndRayCast.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "../Engine/Model.h"

#include "MapEditManager.h"

#include "CameraController.h"


//コンストラクタ
ControllerAndRayCast::ControllerAndRayCast(GameObject * parent)
	: GameObject(parent, "ControllerAndRayCast")
{

}

//初期化
void ControllerAndRayCast::Initialize()
{
	transform_.position_ = Camera::GetPosition();
	transform_.position_.vecY = 0.0f;


	//カメラのY座標を０に、
	Camera::SetPosition(transform_.position_);


	Instantiate<CameraController>(this);


}

//更新
void ControllerAndRayCast::Update()
{


	//レイキャストによる、
	//ヘイトマップの地面のオブジェクトの接触したポリゴンを選定
	if (Input::IsMouseButton(0))
	{
		RayCast();
	}


}

void ControllerAndRayCast::RayCast()
{
	//２次元から、３次元へ直すための行列を取得する。

	//ビューポート行列
	//ビューポート行列は、これまで、Direct３Dに作ってもらっていたため、
	//どこにも存在していない状態なので、作成する。
	float w = Direct3D::scrWidth / 2.0f;
	float h = Direct3D::scrHeight / 2.0f;
	XMMATRIX vp = {
		w, 0, 0, 0,
		0, -h, 0, 0,
		0, 0, 1, 0,
		w, h, 0, 1
	};
	//プロジェクション行列、ビューはカメラから

	//逆行列を作成
	//ビューポート行列の逆行列
	XMMATRIX invVp = XMMatrixInverse(nullptr, vp);
	//プロジェクション行列の逆行列
	XMMATRIX invPro = XMMatrixInverse(nullptr, Camera::GetProjectionMatrix());
	//ビュー行列の逆行列
	XMMATRIX invView = XMMatrixInverse(nullptr, Camera::GetViewMatrix());

	//ベクトル
	XMVECTOR mousePosFront = Input::GetMouseCursorPosition();
	mousePosFront.vecZ = 0;
	XMVECTOR mousePosBack = Input::GetMouseCursorPosition();
	mousePosBack.vecZ = 1;

	//３つの行列で変換することで、
	//ワールドの座標とする。
		//ビューポートからワールドへ
	mousePosFront = XMVector3TransformCoord(
		mousePosFront,
		invVp * invPro * invView
	);

	mousePosBack = XMVector3TransformCoord(
		mousePosBack,
		invVp * invPro * invView
	);


	//ワールドにおける座標が出せたので、
		//レイの方向を、
		//マウス開始位置から終了位置までに延ばす。
	XMVECTOR rayDir = mousePosBack - mousePosFront;
	//XMVECTOR rayDir = mousePosFront - mousePosBack;
	//正規化
	rayDir = XMVector3Normalize(rayDir);


	RayCastData rayData;

	//当たる判定	
	//スタート
	rayData.start = mousePosFront;	//マウス開始位置
	rayData.dir = rayDir;	//マウスクリック方向へ、（マウス開始位置から、クリック方向へ（世界の終了まで））


	pMapEditManager_->RayCast(&rayData);



}



//描画
void ControllerAndRayCast::Draw()
{
}

//開放
void ControllerAndRayCast::Release()
{
}