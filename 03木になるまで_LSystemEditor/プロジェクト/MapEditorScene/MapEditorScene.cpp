#include "MapEditorScene.h"


//360度背景
#include "../Engine/SkyBox.h"


#include "HeightMapObject.h"
#include "../SampleScene/Ground.h"

#include "ControllerAndRayCast.h"
#include "MapEditManager.h"


//コンストラクタ
MapEditorScene::MapEditorScene(GameObject * parent)
	: GameObject(parent, "MapEditorScene")
{
}

//初期化
void MapEditorScene::Initialize()
{
	//Instantiate<Ground>(this);


	//ハイトマップオブジェクト（ハイトマップのポリゴン群を管理するクラス）
	HeightMapObject* pHeightMapObj =
		(HeightMapObject*)Instantiate<HeightMapObject>(this);
	
	//ダイアログ情報など、リソースを共有するクラス
	MapEditManager* pMapEditManager =
		(MapEditManager*)Instantiate<MapEditManager>(this);
	pMapEditManager->SetHeightMapObject(pHeightMapObj);


	//コントローラー
	ControllerAndRayCast * pController =
		(ControllerAndRayCast*)Instantiate<ControllerAndRayCast>(this);
	pController->SetResourceManager(pMapEditManager);

	

	
	
	
	
	//SkyBox
	Instantiate<SkyBox>((GameObject*)pController);


}

//更新
void MapEditorScene::Update()
{



}

//描画
void MapEditorScene::Draw()
{

}

//開放
void MapEditorScene::Release()
{
}