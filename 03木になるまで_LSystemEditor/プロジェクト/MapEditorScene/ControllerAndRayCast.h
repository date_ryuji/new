#pragma once
#include "../Engine/GameObject.h"


class MapEditManager;

//■■シーンを管理するクラス
class ControllerAndRayCast : public GameObject
{


	//ダイアログや、ハイトマップクラスト情報の連携を行うクラス
	MapEditManager* pMapEditManager_;


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ControllerAndRayCast(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void RayCast();


	//描画
	void Draw() override;

	//開放
	void Release() override;

	//セット
	void SetResourceManager(MapEditManager* pMapEditManager) { pMapEditManager_ = pMapEditManager; };


};