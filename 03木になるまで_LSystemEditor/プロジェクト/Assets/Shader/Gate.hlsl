//───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
Texture2D	g_texture : register(t0);	//テクスチャー
										//テクスチャを複数使うときに、ｔ０の0の番号が変化する
Texture2D	g_normalTexture : register(t1);

SamplerState	g_sampler : register(s0);	//サンプラー
											//どのように貼り付けますか

//───────────────────────────────────────
 // コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────
//シェーダー内にて、グローバルに呼び込める構造体のようなもの
cbuffer global

{

	//行列
	float4x4	matWVP;			// ワールド・ビュー・プロジェクションの合成行列
								//ここで、行列を受け取っているので、これとは別に、
	//法線を回転させるために、回転行列を受け取らなければいけない
	float4x4	matW;	//ワールド行列を受け取り、回転行列を受け取る

	float4x4	matNormal;

	float4		camPos;

	//マテリアルの情報において、テクスチャを貼り付ける場合と、テクスチャなしのマテリアルの色を使用する場合とで分けるための変数
	//ベクトル
	float4		diffuseColor;		// ディフューズカラー（マテリアルの色）

	//フォンシェーディングのためのマテリアル情報
	float4		ambientColor;	//環境光色
	float4		supecularColor;	//ハイライト色
	float		shininess;

	bool		isTexture;		// テクスチャ貼ってあるかどうか



};

cbuffer power : register(b1)
{
	float MoveUVPower;
}



//───────────────────────────────────────

// 頂点シェーダー出力＆ピクセルシェーダー入力データ構造体

//───────────────────────────────────────

struct VS_OUT

{
	float4 pos		: SV_POSITION;	//位置
	float2 uv		: TEXCOORD;//UV
	float4 color	: COLOR;	//色（明るさ）
									//光が当たっていれば、色は明るいし、当たらなければ暗い色に
};
//VS_OUTというのが、ピクセルシェーダーに対して、情報を与える構造体である
	//→戻り値として渡す、VS＿OUT　UVを入れてやって



//───────────────────────────────────────

// 頂点シェーダ

//───────────────────────────────────────
//VS = V~ Shader

VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD, float4 normal : NORMAL)
//各頂点ごとの頂点情報
{

	//ピクセルシェーダーへ渡す情報
	VS_OUT outData;

	outData.pos = mul(pos, matWVP);
	//座標　WVPまでの行列を掛けて、プロジェクションまで求めたポジション



	//回転行列をコンスタントバッファの行列で受け取り（global）
		//その行列を法線に掛けて、法線を回転させる
	//法線を回転
	normal = mul(normal, matW);	//行列を掛けるやり方



	//仮のライトを追加
	//本来なら、ライトのクラスも追加しなくてはいけないが、
	//float4 light = float4(-1, 0.5, -0.7, 0);	//float4 light = float4(-1, 0.5, -0.7, 0);	//左上
	float light = float4(1, 0, 0, 0);
	//正規化
	light = normalize(light);

	//出力カラー
	outData.color = clamp(dot(normal, light), 0, 1);



	//UV位置
	outData.uv = uv;


	//まとめて出力
	return outData;

}



//★１ピクセルずつ呼ばれる（１ピクセルずつ、テクスチャの色、マテリアルの色に陰つけ）
//───────────────────────────────────────

// ピクセルシェーダ
	//★ピクセルの色などを管理する。
	//→そのため、テクスチャを張ったときの、UV情報なども、どのテクスチャのUV位置を貼り付けるのかも持つ
//───────────────────────────────────────

float4 PS(VS_OUT inData) : SV_Target
{
if (isTexture)
//テクスチャが張られているなら、テクスチャの色
{
	inData.uv.x *= (MoveUVPower / 3.0f);
	inData.uv.y /= (MoveUVPower / 2.0f);




	float4 diffuse = g_texture.Sample(g_sampler, inData.uv);// * inData.color;	//テクスチャのそのものの色（光の影計算込み（inData.colorに光の計算結果が入っている→計算に用いることで、陰の表現を可能とする））
float4 ambient = g_texture.Sample(g_sampler, inData.uv) * float4(0.2, 0.2, 0.2, 1);	//環境光として、足したい色（このままだと、暗い色）
	
//足す色の値を、どの色かを強くとれば、→その色に染まったテクスチャになる
	//２つの明るさを足して、→テクスチャの明るさそのまま、かつ、明るさを足す→その物の色を残したまま、全体の明るさを上げている
float4 color = diffuse + ambient; //マテリアルそのものの色＋環境光

//color.a = 0.5f;



return color;	

}
else
{
	//テクスチャが張られていないなら、マテリアルそのものの色
	//コンスタントバッファ内の色から取得して、登録


	//灰色
	//マテリアルそのものの色として用いる
	float4 diffuse = float4(0.5f , 0.7f , 0.8f , 1.0f);

	//環境光(マテリアルのの色に、一定数の明るさを掛けたもの→マテリアルの色で、一定数暗くなった色が出る)→その色を最後にマテリアルそのものに、足すことで、→明るさの値が「増える」ので、環境光の値分明るくなる
	float4 ambient = diffuse * float4(0.2, 0.2, 0.2, 1);	//環境光として、足したい色（このままだと、暗い色）


	float4 color = diffuse + ambient; //マテリアルそのものの色＋環境光

	color.a = 0.5f;

	return color;	
}
}



//テクスチャは動的に動かし
//ディフューズ部分は動かない
	//ディフューズで作成されたモデルの、テクスチャ部分だけ、動的に動かす処理を作成
	//★テクスチャ付きの、一部分のテクスチャを動かすという処理は、別途関数、シェーダーが必要
float4 PS_Scroll(VS_OUT inData) : SV_Target
{
if (isTexture)
//テクスチャが張られているなら、テクスチャの色
{
	//UVのX座標を狭める	
		//X座標を所定より小さくする（＊０．１などにすると、）と、描画時のテクスチャは、横に広がったようになる
		//X座標を所定より大きくする（＊2.0などにすると、）と、描画時のテクスチャは横に狭める
	inData.uv.x *= 1.5f;


	//UVのY座標のみ、
		//コンスタントバッファとして取得した、
		//値を減算して、動かす。
		//→上記により、基底のUVのY座標よりも、小さな座標になるため、Y座標が小さくなる＝　上にUVが上に移動するエフェクトを作ることができる
	inData.uv.y = inData.uv.y - MoveUVPower;
	

	float4 diffuse = g_texture.Sample(g_sampler, inData.uv);// * inData.color;	//テクスチャのそのものの色（光の影計算込み（inData.colorに光の計算結果が入っている→計算に用いることで、陰の表現を可能とする））
float4 ambient = g_texture.Sample(g_sampler, inData.uv) * float4(0.2, 0.2, 0.2, 1);	//環境光として、足したい色（このままだと、暗い色）

//足す色の値を、どの色かを強くとれば、→その色に染まったテクスチャになる
	//２つの明るさを足して、→テクスチャの明るさそのまま、かつ、明るさを足す→その物の色を残したまま、全体の明るさを上げている
float4 color = diffuse + ambient; //マテリアルそのものの色＋環境光

//color.a = 0.5f;



return color;

}
else
{
	{
		/*
		//テクスチャが張られていないなら、マテリアルそのものの色
		//コンスタントバッファ内の色から取得して、登録


		//灰色
		//マテリアルそのものの色として用いる
		//float4 diffuse = float4(0.5f , 0.7f , 0.8f , 1.0f);
		float4 diffuse = diffuseColor;

		//環境光(マテリアルのの色に、一定数の明るさを掛けたもの→マテリアルの色で、一定数暗くなった色が出る)→その色を最後にマテリアルそのものに、足すことで、→明るさの値が「増える」ので、環境光の値分明るくなる
		float4 ambient = diffuse * float4(0.2, 0.2, 0.2, 1);	//環境光として、足したい色（このままだと、暗い色）


		float4 color = diffuse + ambient; //マテリアルそのものの色＋環境光

		//color.a = 0.5f;

		return color;
		*/
	}

	{

			//inData.uv.x *= 1.5f;
			//inData.uv.y = inData.uv.y - MoveUVPower;

	
		//テクスチャのRGB値を足して、
				//マテリアルの色を加算して、出力する
				//マテリアルの色と、テクスチャの色を加算したRGB値を出力させる
		//float4 diffuse = g_texture.Sample(g_sampler, inData.uv) + diffuseColor;
		//テクスチャが存在しないのに、テクスチャを参照しようとしている、
			//上記を実装するならば、テクスチャは、Fbxのモデルなどとは別に用意して、バッファーに渡す必要がある。
		float4 diffuse = diffuseColor;
		//環境光として、足したい色（このままだと、暗い色）
		float4 ambient = diffuse * float4(0.2, 0.2, 0.2, 1);	




		return diffuse + ambient;


	
	}

}
}


