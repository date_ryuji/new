//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ で生成されたインクルード ファイル。
// Resource.rc で使用
//
#define DIALOG_NEW_FILE                 9
#define DIALOG_VERTEX_CHANGER           101
#define IDR_MENU1                       105
#define DIALOG_LSYSTEM                  106
#define COMBO_BLUSH_TYPE                1001
#define COMBO_BLUSH_RANGE               1002
#define COMBO_BLUSH_SIZE2               1003
#define COMBO_BLUSH_SIZE                1003
#define SLIDER_DEPTH                    1005
#define SLIDER_DEPTH_                   1005
#define SLIDER_POLY_SIZE                1006
#define SLIDER_POLY_SIZE_               1006
#define GROUP_WIDTH                     1007
#define GROUP_DEPTH                     1008
#define GROUP_POLYGON_SIZE              1009
#define TEXT_WIDTH                      1010
#define TEXT_DEPTH                      1011
#define TEXT_POLY_SIZE                  1012
#define GROUP_BLUSH_TYPE                1013
#define SLIDER_DEPTH_2                  1013
#define SLIDER_WIDTH                    1013
#define GROUP_BLUSH_SIZE                1014
#define GROUP_BLUSH_RANGE               1014
#define TEXT_BLUSH_SIZE                 1015
#define GROUP_BLUSH_SIZE2               1015
#define IDC_SPIN1                       1016
#define SPIN_BLUSH_SIZE                 1016
#define BUTTON_CREATE                   1016
#define IDC_BUTTON1                     1018
#define LSYSTEM_BUTTON_F                1018
#define LSYSTEM_BUTTON_PLUS             1019
#define LSYSTEM_BUTTON_MINUS            1020
#define LSYSTEM_BUTTON_MULTI            1021
#define LSYSTEM_BUTTON_DIVISOR          1022
#define IDC_COMBO2                      1023
#define COMBO_WIDTH                     1023
#define LSYSTEM_BUTTON_BRANCH_START     1023
#define LSYSTEM_BUTTON_BRANCH_END       1024
#define LSYSTEM_GROUP_PATTERN           1025
#define LSYSTEM_EXE                     1026
#define LSYSTEM_ADVANCE                 1027
#define LSYSTEM_ADVANCE2                1028
#define LSYSTEM_CLEAR                   1028
#define LSYSTEM_OUTPUT_TEXT             1029
#define LSYSTEM_TEXTURE_TYPE            1030
#define LSYSTEM_GROUP_PATTERN2          1032
#define ID_40001                        40001
#define ID_40002                        40002
#define MENU_NEW                        40003
#define MENU_SAVE                       40004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40005
#define _APS_NEXT_CONTROL_VALUE         1031
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
