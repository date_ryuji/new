#pragma once


//シェーダーの種類
enum SHADER_TYPE
{
	SHADER_3D, SHADER_2D, SHADER_TEST,
	SHADER_TOON, SHADER_NORMAL, SHADER_WATER, SHADER_ENVI,

	SHADER_WETBALL,

	SHADER_CHOCO_FASHION,

	SHADER_FOG,



	/*ポイントライトなので注意！！（スポットライトではない）*/
	SHADER_POINT_LIGHT,	//ポイントライト（周りを強制的に暗くする）
		//→シェーダーで強制的に暗くする処理を入れている
			//だが、ホラーゲームなどで考えると、　周りは、明るいまま、そのままの明るさに、ライトの明るさが加わって、明るくなる。
				//そのため、　シェーダーで暗くするという処理を入れるよりは、　世界の明るさを、（本来はドロップシャドウなども考慮して）面光源などで作り、その上に、、ライトの明るさがつくと考えるのが自然。

	//上記のことから
	SHADER_POINT_LIGHT_PLUS_WORLD_LIGHT,	//ポイントライト(+面光源)


	SHADER_SKY_BOX,	//SkyBOX

	SHADER_BLURRED_2D , 

	SHADER_OUTLINE,

	SHADER_KUROSAWA_MODE,		//黒澤モード（白黒）
	SHADER_KUROSAWA_MODE_2D,	//黒澤モード（白黒）２D

	SHADER_DEBUG_WIRE_FRAME,	//デバック用のワイヤーフレーム

	SHADER_HEIGHT_MAP,			//ヘイトマップ専用シェーダー

	//SHADER_HEIGHT_MAP,			//ヘイトマップ専用シェーダー

	SHADER_HEIGHT_MAP_ERASE_MOTION,	//消去モーション

	SHADER_PLANE,


	SHADER_MAX
};	//SHADER_ENDという名前の要素を、enumの最後につけておくことで、
	//配列の要素数をとるときに、SHADER_ENDにすれば、必要分のEND前までの要素数を取得できる




//ブレンド　（ピクセルの混ぜ具合）の種類
	//透過を表現できる
enum BLEND_TYPE
{
	BLEND_ALPHA_FOR_ALPHA,		//標準のα値による透過を行う
	BLEND_SUBTRACT_FOR_GRAVITY_MAGIC,		//色を　減算　

	BLEND_MAX


};



//Model名前空間にて
//扱われるポリゴン群のタイプ
enum POLYGON_GROUP_TYPE
{
	FBX_POLYGON_GROUP = 0,	//FBXモデル

	HEIGHT_MAP_POLYGON_GROUP , //ハイトマップ画像による、ポリゴンの高さを決定する


	HEIGHT_MAP_CREATER_POLYGON_GROUP, //ハイトマップ画像を作成する

	BRANCH_POLYGON_GROUP,

	BRANCHES_MAKE_TREE_POLYGON_GROUP,

	PLANE_POLYGON_GROUP,






	MAX_POLYGON_GROUP,


};




