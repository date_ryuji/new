
#include "DebugWireFrameShader.h"


DebugWireFrameShader::DebugWireFrameShader() :
	BaseShader::BaseShader()	//継承元の親のコンストラクタを呼び出す
{
}

DebugWireFrameShader::~DebugWireFrameShader()
{
	BaseShader::~BaseShader();
}

HRESULT DebugWireFrameShader::Initialize()
{
	//シェーダーファイル名
	//ワイド文字（全文字　2バイト）
	const wchar_t shaderFileName[] = L"Assets/Shader/NormalMap.hlsl";



	//頂点シェーダーの作成（コンパイル）
	{
		//頂点シェーダーへのコンパイルデータへのポインタ
		ID3DBlob *pCompileVS = nullptr;	//コンパイルデータへのポインタ


		//頂点シェーダーのために、シェーダーファイルをコンパイル
			//この時、ポインタである、pCompleVSは、nullptrなので、　何も入っていないからのポインタを渡している
			//つまり、からのポインタをコピーして渡していると考える。

			//となると、関数先で、仮に、ポインタへアドレスが入っても、
				//それは、コピー先のポインタに、アドレスが入っているだけ、　引数にて渡したもの自体に入るものではない。
			//つまり、引数にて渡したポインタに対して、コピーではなくて、ポインタ自体を参照したい。
			//＝参照渡しにてポインタを渡してやって、　ポインタを、外部からも直接メモリへ参照できるようにする。（D3DCompileFromFile()に渡している引数のポインタに＆をつけている理由は、同様の理由。　ポインタそのままを渡しても、ポインタがコピーされるだけで、元の「ポインタの」アドレスを指していないので、ポインタのアドレス部が変わっても、関数呼び出し元のポインタには影響を及ぼさなくなる）
		if (FAILED(VSCompileFromShaderFile(&pCompileVS, shaderFileName)))
		{
			MessageBox(nullptr, "頂点シェーダーへのシェーダーファイルコンパイル失敗", "エラー", MB_OK); return E_FAIL;
		};
		//頂点シェーダーの作成
		if (FAILED(CreateVertexShader(pCompileVS)))
		{
			MessageBox(nullptr, "頂点シェーダーへの作成失敗", "エラー", MB_OK); return E_FAIL;
		};


		//★★頂点インプットレイアウト(3D用（光のための法線）)★★
			//→頂点シェーダーに渡す情報群をここで設定
		//頂点の情報
		//1頂点に、位置、法線、色（etc...）　の情報を持たせる
		//Release前に、
		D3D11_INPUT_ELEMENT_DESC layout[] = {

			//layoutを配列にしているのは、
		//頂点に位置情報、法線などなどいろいろ情報を入れなくてはいけないので、
		//法線などが増えるなら、この配列に増やしていく

			//位置
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	
	
		};


		//配列をSizeofして、配列のサイズを求めることができるのは、
		//同スコープ内に限られる、
			//関数先にて、ポインタを使って、Sizeofをして、サイズを受け取ることができない
		//size_t base = sizeof(layout);
			//そのため、サイズを調べて、そのサイズを直接引数に渡す



		//頂点インプットレイアウト情報の確定
		//頂点インプットの情報群をもとに、
		//頂点インプットに渡す情報の確定
		//第３引数：頂点インプットレイアウトにおける引数の個数
		if (FAILED(CreateInputLayout(pCompileVS, layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)))))
		{
			MessageBox(nullptr, "頂点インプットレイアウトの情報確定失敗", "エラー", MB_OK); return E_FAIL;
		};



		//コンパイラ用のデータを解放
		SAFE_RELEASE(pCompileVS);

	}

	// ピクセルシェーダの作成（コンパイル）
	{
		//ピクセルシェーダーへのコンパイルデータへのポインタ
		ID3DBlob *pCompilePS = nullptr;

		//ピクセルシェーダーのために、シェーダーファイルをコンパイル
		if (FAILED(PSCompileFromShaderFile(&pCompilePS, shaderFileName)))
		{
			MessageBox(nullptr, "ピクセルシェーダーへのシェーダーファイルコンパイル失敗", "エラー", MB_OK); return E_FAIL;
		};
		//ピクセルシェーダーの作成
		if (FAILED(CreatePixelShader(pCompilePS)))
		{
			MessageBox(nullptr, "ピクセルシェーダーの作成失敗", "エラー", MB_OK); return E_FAIL;
		}

		//コンパイラ用のデータを解放
		SAFE_RELEASE(pCompilePS);

		//ラスタライザ作成
			//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
			//→そのピクセルごと、ピクセル化みたいなもの
		D3D11_RASTERIZER_DESC rdc = {};

		rdc.CullMode = D3D11_CULL_NONE;
		//カリングモード　の略
		//★裏のポリゴンを表示しますか、表示しませんか
			//FRONT:　表いらない
			//BACK :　裏いらない
			//NONE :　いらないもの　なし（両面表示）

		rdc.FillMode = D3D11_FILL_WIREFRAME;
		//★中身を塗りつぶすか
			//SOLID		：中身を塗りつぶす
			//WIER_FRAME：ワイヤーフレーム		//枠だけなので、三角形にて表現しているので、三角形のつながりとなる
												//当たり判定表示、取り合えず表示されているのか知りたいときなど

		rdc.FrontCounterClockwise = TRUE;
		//カウンター　反撃
		//時計回りを表とするか、反時計回りを表とするのか（FBXのインデックス情報に用いる）
			//TRUE  : 反時計回りを　表　とします
			//FALSE : 時計回りを　　表　とします
				/*
					FBX作成時に、面を三角化して、
					△ポリゴンだけで面を表現できる、その面を描画する際に、時計回りで頂点を読み込むのかなどを決める

				*/

				//ラスタライザ情報の確定
							//作成した情報を渡し、シェーダーに対するラスタライザの確定
		if (FAILED(CreateRasterizerState(&rdc)))
		{
			MessageBox(nullptr, "ラスタライザの情報確定失敗", "エラー", MB_OK); return E_FAIL;
		};


	}


	return S_OK;
}


HRESULT DebugWireFrameShader::VSCompileFromShaderFile(ID3DBlob** pCompileVS, const wchar_t * shaderFileName)
{
	//return BaseShader::VSCompileFromShaderFile(pCompileVS, shaderFileName);
	return BaseShader::CompileFromShaderFile(pCompileVS, shaderFileName , "VS_WIRE_FRAME" , FUNCTION_VERTEX);
}

HRESULT DebugWireFrameShader::PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t * shaderFileName)
{
	return BaseShader::CompileFromShaderFile(pCompilePS, shaderFileName, "PS_WIRE_FRAME", FUNCTION_PIXEL);
}
