#pragma once
#include "BaseShader.h"


class WaterShader : public BaseShader
{
protected:
	//頂点シェーダーファイルのコンパイル
		//仮想関数にしているのは、シェーダーごとに、関数が違う可能性があるため
	HRESULT VSCompileFromShaderFile(ID3DBlob** pCompileVS, const wchar_t* shaderFileName) override;
	//ピクセルシェーダーファイルのコンパイル
	HRESULT PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t* shaderFileName) override;


	//コンスタントバッファを確保しておくポインタ
	ID3D11Buffer* pConstantBuffer_;

public:
	struct CONSTANT_BUFFER_1
	{
		//濡れ具合の割合（０．ｆ〜１．ｆ）
		float scroll;
		//コンストラクタ
		CONSTANT_BUFFER_1();
	}ConstantBuffer1_;


	//コンストラクタ
	WaterShader();

	//デストラクタ
	~WaterShader() override;

	//シェーダーの初期化
		//頂点バッファの引数を決めたり、シェーダーファイルのコンパイル部分を書く。
	//純粋仮想関数
		//継承先にて、それぞれシェーダーの頂点インプットレイアウトを切り替えたりする
	HRESULT Initialize()override;



	//データをセット
		//オブジェクトクラス（GameObjectを継承した、シェーダーを使用するFbxなどを描画するクラス、）
		//そのクラスからデータの構造体をセットしてもらう）
	//引数：コンスタントバッファのポインタ
	void SetConstantBuffer1Data(CONSTANT_BUFFER_1* pConstantBuffer1);

	//コンスタントバッファ１をシェーダーに渡す
		//シェーダーのコンスタントバッファー１への橋渡し
	void ShaderConstantBuffer1View() override;


};

