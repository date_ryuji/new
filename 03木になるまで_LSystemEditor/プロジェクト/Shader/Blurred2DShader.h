#pragma once
#include "BaseShader.h"


class Blurred2DShader : public BaseShader
{
protected:
	//頂点シェーダーファイルのコンパイル
		//仮想関数にしているのは、シェーダーごとに、関数が違う可能性があるため
	HRESULT VSCompileFromShaderFile(ID3DBlob** pCompileVS, const wchar_t* shaderFileName) override;
	//ピクセルシェーダーファイルのコンパイル
	HRESULT PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t* shaderFileName) override;



public:

	//コンストラクタ
	Blurred2DShader();

	//デストラクタ
	~Blurred2DShader() override;

	//シェーダーの初期化
		//頂点バッファの引数を決めたり、シェーダーファイルのコンパイル部分を書く。
	//純粋仮想関数
		//継承先にて、それぞれシェーダーの頂点インプットレイアウトを切り替えたりする
	HRESULT Initialize()override;




};

