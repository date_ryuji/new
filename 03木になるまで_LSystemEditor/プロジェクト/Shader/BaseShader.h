#pragma once
//#include <d3d11.h>	//Direct３Dの略
#include <assert.h>
#include <string>
#include <d3dcompiler.h>
#include "../Engine/Direct3D.h"

#pragma comment(lib, "d3dcompiler.lib")	//シェーダーをコンパイルするために、追加

class BaseShader
{
protected :
	//シェーダーの情報
	//コンパイル後のシェーダーの情報を保持しておく構造体
		//シェーダーを切り替えるときに、この情報をセットし、扱う
	struct ShaderInfo {

		ID3D11VertexShader*	pVertexShader;	//頂点シェーダー
		ID3D11PixelShader*	pPixelShader;		//ピクセルシェーダー
		ID3D11InputLayout*	pVertexLayout;	//頂点インプットレイアウト	
		ID3D11RasterizerState*	pRasterizerState;	//ラスタライザー


	}shaderInfo_;

	//ピクセルシェーダーか頂点シェーダーか
	enum FUNCTION_TYPE
	{
		FUNCTION_VERTEX = 0, 
		FUNCTION_PIXEL,
		MAX_FUNCTION,
	};


	//シェーダー情報の初期化
	void InitShaderInfo();

	//頂点シェーダーファイルのコンパイル
		//仮想関数にしているのは、シェーダーごとに、関数が違う可能性があるため
	//引数：ポインタのアドレス（ポインタを持っているアドレス部を参照し、関数呼び出し元のデータを直接書き換える、参照渡しを行う）
	//引数：シェーダーファイル名
	virtual HRESULT VSCompileFromShaderFile(ID3DBlob** pCompileVS ,const wchar_t* shaderFileName);
	//ピクセルシェーダーファイルのコンパイル
	//引数：ポインタのアドレス（ポインタを持っているアドレス部を参照し、関数呼び出し元のデータを直接書き換える、参照渡しを行う）
	//引数：シェーダーファイル名
	virtual HRESULT PSCompileFromShaderFile(ID3DBlob** pCompilePS, const wchar_t* shaderFileName);

	//頂点シェーダーファイルのコンパイル
	//ピクセルシェーダーファイルのコンパイル
		//引数にて、ピクセルシェーダーの関数名を指定する
	//引数：ポインタのアドレス（ポインタを持っているアドレス部を参照し、関数呼び出し元のデータを直接書き換える、参照渡しを行う）
	//引数：シェーダーファイル名
	//引数：ピクセルシェーダー関数名　OR　頂点シェーダーの関数名
	//引数：エラー時の出力文字列
	HRESULT CompileFromShaderFile(ID3DBlob** pCompilePS, 
		const wchar_t* shaderFileName , 
		const std::string functionName , 
		FUNCTION_TYPE type = MAX_FUNCTION);





	//頂点シェーダーの作成
	//引数：コンパイル後のデータへのポインタ
	HRESULT CreateVertexShader(ID3DBlob *pCompileVS);

	//頂点インプットレイアウトの作成
		//頂点シェーダーの引数部分
	//引数：コンパイル後のデータへのポインタ
	HRESULT CreateInputLayout(ID3DBlob *pCompileVS , D3D11_INPUT_ELEMENT_DESC* layout , size_t size);

	//ピクセルシェーダーの作成
	//引数：コンパイル後のデータへのポインタ
	HRESULT CreatePixelShader(ID3DBlob *pCompilePS);

	//ラスタライザの作成
	//引数：ラスタライザ情報の構造体ポインタ
	HRESULT CreateRasterizerState(D3D11_RASTERIZER_DESC* rdc);

	//コンスタントバッファのポインタを作成
		//シェーダークラスに渡す、コンスタントバッファ2つ目、専用のコンスタントバッファへのポインタを作成
	//引数：コンスタントバッファのポインタ
	//引数：シェーダーのコンスタントバッファへとわたす構造体のサイズ
	HRESULT CreateConstantBuffer1(ID3D11Buffer** ppConstantBuffer1 , UINT size);

	//シェーダーのコンスタントバッファ2つ目にデータを渡す
	//引数：コンスタントバッファのポインタ
	//引数：シェーダーのコンスタントバッファへとわたす構造体のアドレス
	//引数：シェーダーのコンスタントバッファへとわたす構造体のサイズ
	void ShaderConstantBuffer1View(ID3D11Buffer** ppConstantBuffer1, void* cb1, UINT size);

	//キューブマップ（環境マップ）用のテクスチャをシェーダーに渡す
	//引数：シェーダー内のテクスチャ番号（キューブマップのテクスチャを登録するテクスチャ番目を指定する）
			//PSSetShaderResourcesの第一引数の要素を示す
	void SetCubeMapTexture(UINT suffix);

	//シェーダーのコンスタントバッファのマップ
	//CPUからの書き込みなどを許可させる
//引数：コンスタントバッファのポインタ
//引数：シェーダーのコンスタントバッファへとわたす構造体のアドレス
//引数：シェーダーのコンスタントバッファへとわたす構造体のサイズ
	HRESULT MapShaderConstantBuffer1(ID3D11Buffer** ppConstantBuffer1, void* cb1, UINT size);
	//シェーダーのコンスタントバッファのアンマップ
		//CPUからの書き込みなどを非許可（終了）させる
	//引数：コンスタントバッファのポインタ
	HRESULT UnMapShaderConstantBuffer1(ID3D11Buffer** ppConstantBuffer1);




	//グラフィックボードのバージョン名を取得
		//頂点シェーダー、ピクセルシェーダーそれぞれのバージョンを文字列で取得
	//引数：関数タイプ（VERTEX　OR　PIXEL）
	const std::string GetGraphicVersion(FUNCTION_TYPE type);
	//シェーダーファイルコンパイル時のエラーメッセージの取得
	//引数：関数タイプ（VERTEX　OR　PIXEL）
	const std::string GetErrorMessage(FUNCTION_TYPE type);


public : 
	//コンストラクタ
	BaseShader();

	//デストラクタ
	virtual ~BaseShader();

	//シェーダーの初期化
		//頂点バッファの引数を決めたり、シェーダーファイルのコンパイル部分を書く。
		//継承先にて、それぞれシェーダーの頂点インプットレイアウトを切り替えたりする
	virtual HRESULT Initialize();

	


	//自身のシェーダーをセットする
	void SetShader();

	//シェーダー独自の情報受け渡し実行
		//シェーダーの持つ、コンスタントバッファ2つ目に情報を渡す処理を実行
		//テクスチャの受け渡しを実行
	virtual void ShaderConstantBuffer1View();


	//シェーダーのコンスタントバッファのマップ
	//CPUからの書き込みなどを許可させる
	virtual HRESULT MapShaderConstantBuffer1();
	//シェーダーのコンスタントバッファのアンマップ
		//CPUからの書き込みなどを非許可（終了）させる
	virtual HRESULT UnMapShaderConstantBuffer1();



};

